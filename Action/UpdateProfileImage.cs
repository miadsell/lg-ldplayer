﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class UpdateProfileImage : IAction
    {
        public IGDevice IGDevice;

        string imagepath, deviceID;

        public UpdateProfileImage(IGDevice iGDevice, string imagepath)
        {
            IGDevice = iGDevice;
            this.imagepath = imagepath;
            deviceID = IGDevice.deviceID;
        }

        public UpdateProfileImage(string deviceID, string imagepath)
        {
            this.imagepath = imagepath;
            this.deviceID = deviceID;
        }

        public void DoAction()
        {
            ////Push image to sdcard

            //ADBSupport.ADBHelpers.PushFileToPhone(deviceID, imagepath, "/sdcard/Pictures");

            ////Restart

            //ADBSupport.LDHelpers.CloseDevice(IGDevice.ld_devicename);

            //ADBSupport.ADBHelpers.Delay(2, 3);

            ////Launch

            //ADBSupport.LDHelpers.LaunchLDPlayer(IGDevice.ld_devicename, false);

            ADBSupport.ADBHelpers.PushImageandScanGallery(deviceID, imagepath);


            List<string> screen_nav = new List<string>() { };

            ////Navigate to EditProfile

            //ADBSupport.IGScreen.GoToScreen(IGDevice, screen_nav);

            //Goto profile
            int loadprofile = 0;
            GotoProfile:
            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "instagram://profile", "com.instagram.android");


            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_Goto_EditProfileScreen, 3, 5, 5);
            }
            catch
            {
                KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_BACK);
                loadprofile++;
                if (loadprofile > 5)
                    throw new System.ArgumentException("Can't load profile");
                goto GotoProfile;
            }

            //Hit editprofile

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.Global_Goto_EditProfileScreen, ADBSupport.BMPSource.ClickChangeProfilePhoto, 1);

            //Click change profile

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.ClickChangeProfilePhoto, ADBSupport.BMPSource.ClickNewProfilePhoto);

            //Click new profile photo

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.ClickNewProfilePhoto, ADBSupport.BMPSource.WaitGallery); ADBSupport.ADBHelpers.Delay();

            //Swipe down image

            //KAutoHelper.ADBHelper.SwipeByPercent(deviceID, 48.6, 18.4, 45.2, 54.3);

            var random_y = (new Random()).Next(16, 25);
            var dest_y = random_y + 35;

            ADBSupport.ADBHelpers.SwipeByPercent_RandomTouch(deviceID, "doc", "27-75", random_y, dest_y, 100);


            ADBSupport.ADBHelpers.Delay(3, 8);

            //Click next

            KAutoHelper.ADBHelper.TapByPercent(deviceID, 92.4, 3.3); 

            //ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.UpdateProfileImage.UpdateProfileImage_NextBtn);

            ADBSupport.ADBHelpers.Delay(5, 10);

            //Click next

            KAutoHelper.ADBHelper.TapByPercent(deviceID, 92.4, 3.3);

            //ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.UpdateProfileImage.UpdateProfileImage_NextBtn);

            ADBSupport.ADBHelpers.Delay(5,10);

            //Wait EditProfileScreen

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_Goto_EditProfileScreen, 5, 10, 5);
        }
    }
}
