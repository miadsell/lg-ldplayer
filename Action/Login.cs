﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class Login : IAction
    {
        string username, password;
        string deviceID;

        public Login(string deviceID, string username, string password)
        {
            this.username = username;
            this.password = password;
            this.deviceID = deviceID;
        }

        public void DoAction()
        {
            LoginToAccount();
        }


        private void LoginToAccount()
        {

            ADBSupport.ADBHelpers.ClearAPPData(deviceID, "com.instagram.android");
            OpenIGAPP:
            try
            {
                //Open ig app
                ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.Global_IconApp, ADBSupport.BMPSource.Login_LoginBtn);
            }
            catch 
            {

                //can't open ig app => reinstall

                //remove it
                ADBSupport.ADBHelpers.UnInstallApp(deviceID, "com.instagram.android");

                var ig_path = AppDomain.CurrentDomain.BaseDirectory + "Temp\\" + deviceID.Replace(".","").Replace(":","") + "\\instagram.apk";
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "Temp\\" + deviceID.Replace(".", "").Replace(":", ""));
                File.Copy("instagram.apk", ig_path, true);

                //reinstall
                ADBSupport.LDHelpers.InstallAPK(deviceID, ig_path);

                Thread.Sleep(5000);

                //go home
                KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_HOME);

                //wait iconapp
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_IconApp);

                goto OpenIGAPP;
            }

            //Wait login

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Login_LoginBtn);

            //Click login

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.Login_LoginBtn, ADBSupport.BMPSource.Login_PhoneEmailUser, 1, "Login_LoginBtn");

            //Tap phoneusermail box

            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Login_PhoneEmailUser, "Login_PhoneEmailUser");

            ADBSupport.ADBHelpers.Delay();

            ADBSupport.ADBHelpers.InputTextWithDelay(deviceID, username); ADBSupport.ADBHelpers.Delay(1, 3);

            //Tap password

            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Login_Password, "Login_Password");

            ADBSupport.ADBHelpers.Delay();

            ADBSupport.ADBHelpers.InputTextWithDelay(deviceID, password); ADBSupport.ADBHelpers.Delay(1, 3);

            //Hit login

            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Login_LoginBtnBlue);

            //Wait home

            //ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Login_Disabled, 1, 1, 1);
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_WaitHome, 5, 10, 3);
            }
            catch
            {
                var screen = ADBSupport.ADBHelpers.ScreenShoot(deviceID, true);
                //Check if disable
                Bitmap temp_bmp = null;

                lock (ADBSupport.BMPSource.Login_Disabled)
                {
                    temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.Login_Disabled);
                }

                if (KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp) != null)
                {
                    throw new System.ArgumentException("Account Is Disabled");
                }

                lock (BMResource.LogIn.LogIn_AccountIsDisabled)
                {
                    temp_bmp = new Bitmap((Image)BMResource.LogIn.LogIn_AccountIsDisabled);
                }

                if (KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp) != null)
                {
                    throw new System.ArgumentException("Account Is Disabled");
                }

                lock (ADBSupport.BMPSource.ErrorScreen_UnsualActivity)
                {
                    temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.ErrorScreen_UnsualActivity);
                }

                if (KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp) != null)
                {
                    throw new System.ArgumentException("UnsualActivity");
                }

                //Check if unsual activity
                lock (BMResource.ErrorScreen.UnsualActivity_1)
                {
                    temp_bmp = new Bitmap((Image)BMResource.ErrorScreen.UnsualActivity_1);
                }

                if (KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp) != null)
                {
                    throw new System.ArgumentException("UnsualActivity");
                }

                lock (ADBSupport.BMPSource.Login_ErrorRequest)
                {
                    temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.Login_ErrorRequest);
                }

                if (KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp) != null)
                {
                    throw new System.ArgumentException("ErrorRequest");
                }


                throw;
            }
        }
    }
}
