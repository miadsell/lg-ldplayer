﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class ReadNotifcation : IAction
    {
        IGDevice iGDevice;
        string deviceID;

        public ReadNotifcation(IGDevice iGDevice)
        {
            this.iGDevice = iGDevice;
            this.deviceID = this.iGDevice.deviceID;
        }

        public ReadNotifcation(string deviceID)
        {
            this.deviceID = deviceID;
        }

        public void DoAction()
        {
            //Check if has notification

            bool has_notification = false;

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.ReadNotice_NoticationAlert, 1, 3, 0, 0.95);
                has_notification = true;
            }
            catch
            {

            }

            //If yes, click

            if (has_notification)
            {
                var point_heart = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.ReadNotice_HeartBtn);
                //click notification
                ADBSupport.ADBHelpers.RandomTapImageByPercentage(deviceID, point_heart.x, point_heart.y, "ReadNotice_HeartBtn");

                //wait Activity show

                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.ReadNotice_Activity, 3, 4, 2);
            }
            else 
                return;



        //Decide click notification or not
        ClickNotification:
            List<bool> choices = new List<bool>() { true, false };

            var choice = choices[(new Random(Guid.NewGuid().GetHashCode())).Next(choices.Count)];

            if (choice)
            {
                //If click, choose notice to click

                //set rectangle you can click
                ClickAgain:
                Class.RecPercentage recPercentage = new Class.RecPercentage(new { x = 10, y = 17.2 }, 70, 23.7);

                //random click rectangle

                ADBSupport.ADBHelpers.TapRecTanglePercentage(deviceID, recPercentage);

                //wait Activity disappear 

                try
                {

                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.ReadNotice_Activity, 1,2, 2);
                    goto ClickAgain;
                }
                catch
                {

                }
            }
            else
            {
                goto EndAction;
            }

            //make some scroll

            //Random scroll some times and delay
            int scroll_time = 0;
            while (true)
            {
                ADBSupport.ADBHelpers.RandomScrollScreen(deviceID, 1, 0, 78.7, 38.4, (new Random()).Next(1000, 2000));
                ADBSupport.ADBHelpers.Delay(4, 10);
                scroll_time++;

                if (scroll_time > 3)
                {
                    List<bool> choices_scroll = new List<bool>() { false, true, false };

                    var choice_scroll = choices_scroll[(new Random(Guid.NewGuid().GetHashCode())).Next(choices_scroll.Count)];

                    if (!choice_scroll)
                        goto EndAction;
                    else
                        break;
                }
            }

            //hit back

            KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_BACK);

            goto ClickNotification;

        EndAction:
            //go home

            Class.RecPercentage home_recPercentage = new Class.RecPercentage(new { x = 7.3, y = 96.1 }, 4.8, 1.6); //home button rec

            ADBSupport.ADBHelpers.TapRecTanglePercentage(deviceID, home_recPercentage);


            IGHelpers.DBHelpers.AddActionLog(iGDevice.ld_devicename, "Read Notifications");
        }
    }
}
