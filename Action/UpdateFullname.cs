﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class UpdateFullname:IAction
    {

        public IGDevice IGDevice;

        public string fullname, deviceID, deviceName;

        public UpdateFullname(IGDevice iGDevice, string fullname)
        {
            IGDevice = iGDevice;
            this.fullname = fullname;
            this.deviceID = iGDevice.deviceID;
            this.deviceName = iGDevice.ld_devicename;
        }

        public void DoAction()
        {
            //UPDATE CONTENT TO BIO
            //Goto profile

            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "instagram://profile", "com.instagram.android");


            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_Goto_EditProfileScreen, 5, 5, 5);

            //Hit editprofile

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.Global_Goto_EditProfileScreen, ADBSupport.BMPSource.SwitchAccount, 1);

            //Click name box

            //demoTag: 5.9,37 --demoA: 6.6,41.3 --demoC: 43.1,43

            ADBSupport.ADBHelpers.ClickRelative(deviceID, ADBSupport.BMPSource.UpdateFullname_TagTitleName, new { x = 5.9, y = 37 }, new { x = 6.6, y = 41.3 }, new { x = 43.1, y = 43 });

        CheckBoxEmpty:
            bool isempty = false;

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.UpdateFullname_EmptyFullname, 1, 5, 0, 0.7);
                isempty = true;
            }
            catch
            {

            }

            //Clear box
            if (!isempty)
            {
                while (true)
                {
                    //long press to show control
                    ADBSupport.ADBHelpers.LongPressRelative(deviceID, ADBSupport.BMPSource.UpdateFullname_TagTitleName, new { x = 5.9, y = 37 }, new { x = 6.6, y = 41.3 }, new { x = 43.1, y = 43 }, 1000);

                    try
                    {
                        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.BioSelectAll, 1, 5, 0, 0.8);
                        break;
                    }
                    catch { }
                }


                //Click select all
                ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.UpdateFullname_SelectAllBtn);

                //Send key delete

                ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell input keyevent 67"); Thread.Sleep(1000);

                goto CheckBoxEmpty;
            }

            //send new name

            ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID, deviceName, fullname);

            //Click update

            //ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.UpdateFullname_UpdateProfileTick, "UpdateFullname_UpdateProfileTick");

            int try_updatename = 0;

            UpdateName:

            if(ADBSupport.ADBHelpers.IsImageExisted(deviceID,ADBSupport.BMPSource.UpdateFullname_UpdateProfileTick,1,2,2))
                ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.UpdateFullname_UpdateProfileTick);


            //Wait editprofile
            if(!ADBSupport.ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.Global_Goto_EditProfileScreen, 5, 5, 2))
            {
                try_updatename++;
                if (try_updatename > 1)
                    throw new System.ArgumentException("Can't update name! Unknown reason");
                goto UpdateName;
            }
            

        }
    }
}
