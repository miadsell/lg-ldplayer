﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class SwipeHorizontal : EngagePostAction
    {
        public SwipeHorizontal(IGDevice iGDevice) : base(iGDevice)
        {
        }

        public SwipeHorizontal(string deviceID) : base(deviceID)
        {
        }

        public override void DoAction()
        {

            this.UpdateEngageButtonPos();

            SwipeMedia();
        }

        private void SwipeMedia()
        {
            bool canswipe = false;
            //Check if post has multiple media

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.SwipeStartDot, 2, 1, 1);
                canswipe = true;
            }
            catch { }

            if (!canswipe)
                return;
            //Set swipe position

            var swipe_y = rec_media.point.y + rec_media.height / 2;


            //Swipe

            List<string> vectors = new List<string>() { "left", "right" };

            int swipe_left = 0;

            var choice_vector = "";

            Stopwatch watch_swipe = new Stopwatch();
            watch_swipe.Start();


            while(true)
            {
                if (watch_swipe.Elapsed.TotalMinutes > 2)
                    break;
                if (swipe_left <= 1)
                {
                    choice_vector = "left";
                }
                else choice_vector = vectors[(new Random()).Next(vectors.Count)];

                switch (choice_vector)
                {
                    case "left":
                        swipe_left++;

                        var start_x_left = ADBSupport.ADBHelpers.RandomDoubleValue(70, 80);

                        KAutoHelper.ADBHelper.SwipeByPercent(deviceID, start_x_left, swipe_y, start_x_left - 52, swipe_y, (new Random()).Next(200, 250));

                        break;
                    case "right":
                        swipe_left--;

                        var start_x_right = ADBSupport.ADBHelpers.RandomDoubleValue(18, 28);

                        KAutoHelper.ADBHelper.SwipeByPercent(deviceID, start_x_right, swipe_y, start_x_right + 52, swipe_y, (new Random()).Next(200, 250));

                        break;
                }
                //Thread some seconds

                ADBSupport.ADBHelpers.Delay(0, 1);

                //Check if last media

                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.SwipeLastDot, 2, 1, 1);
                    break;
                }
                catch { }

            }
        }
    }
}
