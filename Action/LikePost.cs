﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class LikePost : EngagePostAction
    {
        Class.IGDevice IGDevice;

        

        

        public LikePost(IGDevice iGDevice) : base(iGDevice)
        {
        }

        public LikePost(string deviceID) : base(deviceID)
        {
        }

        public override void DoAction()
        {
            this.UpdateEngageButtonPos(true);

            if (likebtn_point == null)
                return;

            ClickLike();
        }


        private void ClickLike()
        {
            //KAutoHelper.ADBHelper.TapByPercent(deviceID, likebtn_point.x, likebtn_point.y);
            ADBSupport.ADBHelpers.RandomTapImageByPercentage(deviceID, likebtn_point.x, likebtn_point.y, "LikeButton");
            ADBSupport.ADBHelpers.Delay(1, 3);
        }

    }
}
