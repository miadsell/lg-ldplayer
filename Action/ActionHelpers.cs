﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    static class ActionHelpers
    {

        #region --Check Message--

        static public void CheckMessage(Class.IGDevice iGDevice, string accountId)
        {
            if (!IGHelpers.DBHelpers.IsCheckRequestMessageInLatest8Hours(accountId))
            {
                string base64_screenshoot = "";

                string deviceID = iGDevice.deviceID;


                //go home
                ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "https://instagram.com", "com.instagram.android");

                //wait home

                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_WaitHome);

                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Message_TagCameraIcon, 3, 5, 5);
                }
                catch
                {
                    return;
                }

                //check if has message
                bool has_message = false;

                Image crop_message_icon_region = null;

                crop_message_icon_region = ADBSupport.ADBHelpers.CropScreenshotRelative(deviceID, ADBSupport.BMPSource.Message_TagCameraIcon, new { x = 3.1, y = 6 }, new { x = 71, y = 5.2 }, new { x = 98.3, y = 9.3 });



                var message_icon_point = KAutoHelper.ImageScanOpenCV.FindOutPoint((Bitmap)crop_message_icon_region, ADBSupport.BMPSource.Message_NoMessage);

                if (message_icon_point == null)
                    has_message = true;

                //click message icon
                //demoTag: 3.5,6  -- tagA: 92.4,6.2 --tagC: 95.8,7.7
                Stopwatch watch_message = new Stopwatch();
                watch_message.Start();
                while (true)
                {
                    try
                    {
                        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Message_TagCameraIcon, 1, 1, 1);
                    }
                    catch
                    {
                        //may be show some popup -> back
                        KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_BACK);
                    }

                    ADBSupport.ADBHelpers.ClickRelative(deviceID, ADBSupport.BMPSource.Message_TagCameraIcon, new { x = 3.5, y = 6 }, new { x = 92.4, y = 6.2 }, new { x = 95.8, y = 7.7 });

                    //Wait direct page

                    try
                    {
                        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Message_DirectMessage, 2, 5, 5);
                        break;
                    }
                    catch
                    { }

                    if (watch_message.Elapsed.TotalMinutes > 1)
                        throw new System.ArgumentException("Can't open message");
                }

                bool has_request = false;
                //check if suitable for check request message (just check every 12 hours)

                //check request message



                //Check if has new request


                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Message_HasRequestMessage, 1, 5, 1);

                    has_request = true;
                }
                catch
                {

                }

                //add log

                IGHelpers.DBHelpers.AddActionLogByID(accountId, "check_request_message");




                if (has_message) //capture screenshoot
                {
                    var screen_message_base64 = SaveBitmapToBase64Str(ADBSupport.ADBHelpers.ScreenShoot(deviceID));

                    base64_screenshoot = screen_message_base64 + "|||";

                    //compare message

                    //crop screen for check

                    //--demoTag: 3.5, 21.1 --demoA: 21.7, 27.9 --demoC 65.2, 33.9

                    var crop_message = ADBSupport.ADBHelpers.CropScreenshotRelative(deviceID, ADBSupport.BMPSource.Message_TagMessage, new { x = 3.5, y = 22.1 }, new { x = 21.7, y = 27.9 }, new { x = 65.2, y = 33.9 });

                    if (!IGHelpers.DBHelpers.IsReallyNewMessage(accountId, (Bitmap)crop_message, "latest_message_screen"))
                        has_message = false;
                }

                if (has_request) //capture request screenshoot
                {
                    //Click request to goto request page

                    ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Message_HasRequestMessage, "Message_HasRequestMessage");

                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Message_TagRequests, 3, 5, 5);

                    var screen_request_base64 = SaveBitmapToBase64Str(ADBSupport.ADBHelpers.ScreenShoot(deviceID));

                    base64_screenshoot += screen_request_base64;

                    //compare with latest requests
                    var crop_request = ADBSupport.ADBHelpers.CropScreenshotRelative(deviceID, ADBSupport.BMPSource.Message_TagRequests, new { x = 13.5, y = 5.2 }, new { x = 20.4, y = 18.2 }, new { x = 74.5, y = 25.2 });

                    if (!IGHelpers.DBHelpers.IsReallyNewMessage(accountId, (Bitmap)crop_request, "latest_request_screen"))
                        has_request = false;

                }

                //--demoTag: 13.5,5.2 --demoA:20.4,18.2  --demoC: 74.5, 25.2


                if (has_message || has_request)
                {
                    //update to db
                    IGHelpers.DBHelpers.UpdateHasMessage(accountId, "yes", base64_screenshoot);


                }
            }

        }

        static public string SaveBitmapToBase64Str(Bitmap bitmap)
        {
            string SigBase64 = "";

            using (var ms = new MemoryStream())
            {
                bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                SigBase64 = Convert.ToBase64String(ms.GetBuffer()); //Get Base64
            }

            return SigBase64;
        }

        static public Bitmap BitmapFromBase64Str(string SigBase64)
        {
            var ms_from = Convert.FromBase64String(SigBase64);

            Bitmap bitmap = new Bitmap(new MemoryStream(ms_from));

            return bitmap;
        }

        #endregion
    }
}
