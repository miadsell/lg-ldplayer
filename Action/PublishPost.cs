﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class PublishPost : IAction
    {
        public IGDevice IGDevice;

        public string content, deviceID;

        public List<string> imagepaths = new List<string>();

        public PublishPost(IGDevice iGDevice, string content, List<string> imagepaths)
        {
            IGDevice = iGDevice;
            this.content = content;
            this.imagepaths = imagepaths;
            this.deviceID = this.IGDevice.deviceID;
        }

        /// <summary>
        /// testing
        /// </summary>
        /// <param name="deviceID"></param>
        public PublishPost(string deviceID)
        {
            this.deviceID = deviceID;
        }

        public void AddMulti_Images()
        {
            //scan image first

            ADBSupport.ADBHelpers.ScannGallery(deviceID);

            //Click relative multi image option

            //anh multiimage option - demoTag: 85.5, 1.9 --demoA: 88.6, 56.8 --demoC: 92.4, 58.9 

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Publish_NextBtn, 5, 5, 2);

            ADBSupport.ADBHelpers.ClickRelative(deviceID, ADBSupport.BMPSource.Publish_NextBtn, new { x = 85.5, y = 1.9 }, new { x = 88.6, y = 56.8 }, new { x = 92.4, y = 58.9 });

            //wait option clicked
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.MultiClicked);

            List<dynamic> images_pos = new List<dynamic>();


            //anh 1 - demoTag: 85.5, 1.9 --demoA: 3.5,66.9 --demoC: 18.3,73.5
            images_pos.Add(new
            {
                demoA = new { x = 3.5, y = 66.9 },
                demoC = new { x = 18.3, y = 73.5 }
            });

            //anh 2 - demoTag: 85.5, 1.9 --demoA: 31.7,66.9 --demoC: 44.8,73.5
            images_pos.Add(new
            {
                demoA = new { x = 31.7, y = 66.9 },
                demoC = new { x = 44.8, y = 73.5 }
            });

            //anh 3 - demoTag: 85.5, 1.9 --demoA: 56.2,66.9 --demoC: 66.5,73.5
            images_pos.Add(new
            {
                demoA = new { x = 56.2, y = 66.9 },
                demoC = new { x = 66.5, y = 73.5 }
            });

            //anh 4 - demoTag: 85.5, 1.9 --demoA: 81,66.9 --demoC: 95.5,73.5
            images_pos.Add(new
            {
                demoA = new { x = 81, y = 66.9 },
                demoC = new { x = 95.5, y = 73.5 }
            });


            //Click image -- currently - max image = 4

            var max_image = 4;

            if (imagepaths.Count < 4)
                max_image = imagepaths.Count;

            for (int i = 0; i < max_image; i++)
            {
                if (i == 0) //first image is too automatically
                    continue;
                ADBSupport.ADBHelpers.ClickRelative(deviceID, ADBSupport.BMPSource.Publish_NextBtn, new { x = 85.5, y = 1.9 }, images_pos[i].demoA, images_pos[i].demoC);
                //Swipe down image

                //KAutoHelper.ADBHelper.SwipeByPercent(deviceID, 48.6, 18.4, 45.2, 54.3);

                var random_y = (new Random()).Next(16, 25);
                var dest_y = random_y + 35;

                ADBSupport.ADBHelpers.SwipeByPercent_RandomTouch(deviceID, "doc", "27-75", random_y, dest_y, 100);


                ADBSupport.ADBHelpers.Delay();
            }


        }

        public void DoAction()
        {

            //ADBSupport.ADBHelpers.DownloadImage(IGDevice, image_link);

            //ADBSupport.ADBHelpers.PushImageandScanGallery(deviceID, imagepath);

            imagepaths.Reverse();

            foreach (var item in imagepaths)
            {
                ADBSupport.ADBHelpers.PushImageandScanGallery(deviceID, item);
            }

            //Go to home instagram

            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "https://instagram.com", "com.instagram.android");

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.AddPost, 5, 5, 5);

            ADBSupport.ADBHelpers.Delay(3, 8);


            //Click add post
            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.AddPost, ADBSupport.BMPSource.WaitGallery, 3); ADBSupport.ADBHelpers.Delay(3, 8);

            //Add images

            AddMulti_Images();

            while (true)
            {
                //Hit next

                ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Publish_NextBtn);

                //Wait next screen
                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PublishPost_BackBtn, 1, 0, 2);
                    break;
                }
                catch
                { }


            }

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.Publish_NextBtn, ADBSupport.BMPSource.WaitNewPostEdit, 3);

            //Wait write a caption
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.WriteACaption, 2, 4, 2);

            //Click write a caption
            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.WriteACaption);

            //Send unicode text

            ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID, IGDevice.ld_devicename, content);

            ////Copy and paste a caption

            //if (File.ReadAllText(contentpath) != "")
            //{
            //    try
            //    {
            //        ADBSupport.ADBHelpers.CopyTextFromEsEditor(deviceID, contentpath);
            //    }
            //    catch
            //    {
            //        ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell monkey -p com.instagram.android -c android.intent.category.LAUNCHER 1", 30);
            //        goto ClickShare;
            //    }

            //    //Reopen ig

            //    ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell monkey -p com.instagram.android -c android.intent.category.LAUNCHER 1", 30);

            //    //Paste
            //    while (true)
            //    {
            //        KAutoHelper.ADBHelper.LongPress(deviceID, 68, 78, 1000);

            //        try
            //        {
            //            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PasteBtn, 1, 0, 0);
            //            break;
            //        }
            //        catch
            //        { }
            //    }

            //    //Click paste

            //    ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.PasteBtn); ADBSupport.ADBHelpers.Delay(5, 10);
            //}
            ClickShare:
            //Click share

            bool press_back = false;

            WaitShareBtn:
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.ShareBtn, 5, 1, 2);
            }
            catch
            {
                if (!press_back)
                {
                    KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_BACK);
                    goto WaitShareBtn;
                }
                else
                    throw new System.ArgumentException("Can't wait ShareBtn");
            }





            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.ShareBtn);

            //wait finish adding show

            if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.WaitFinishingUp, 20, 1, 1))
            {
                //wait finish adding disappear

                while (true)
                {
                    try
                    {
                        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.WaitFinishingUp, 1, 0, 0);
                    }
                    catch
                    {
                        break;

                    }
                    Thread.Sleep(5000);
                }
            }
            else
            {
                //neu sau 20s ma chua hien thi thi mac dinh la da post thanh cong
            }
        }
    }
}
