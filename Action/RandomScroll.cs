﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class RandomScroll : IAction
    {
        public string vector; //up or down
        string deviceID;
        public int duration;

        public RandomScroll(string vector, string deviceID, int duration = -1)
        {

            this.vector = vector ?? throw new ArgumentNullException(nameof(vector));
            this.deviceID = deviceID ?? throw new ArgumentNullException(nameof(deviceID));
            this.duration = duration;
        }

        public void DoAction()
        {
            switch (vector)
            {
                case "up":
                    ScrollUp();
                    break;
                case "down":
                    ScrollDown();
                    break;
            }
        }

        

        private void ScrollUp()
        {
            var y_start= ((new Random()).NextDouble()) * (45.7 - 14.5) + 14.5;

            var y_des = y_start + (((new Random()).NextDouble()) * (40 - 32) + 32);


            int temp_dur = duration;

            if(temp_dur==-1)
                temp_dur = (new Random()).Next(200, 3000);


            ADBSupport.ADBHelpers.RandomScrollScreen(deviceID, 1, (new Random()).Next(1, 5), y_start, y_des, temp_dur);
        }


        private void ScrollDown()
        {
            var y_start = ((new Random()).NextDouble()) * (79.7 - 55.6) + 55.6;

            var y_des = y_start - (((new Random()).NextDouble()) * (40 - 32) + 32);


            int temp_dur = duration;

            if (temp_dur == -1)
                temp_dur = (new Random()).Next(200, 3000);

            ADBSupport.ADBHelpers.RandomScrollScreen(deviceID, 1, (new Random()).Next(1, 5), y_start, y_des, temp_dur);

        }
    }
}
