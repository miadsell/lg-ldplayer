﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class ViewReplyComment : EngagePostAction
    {

        public ViewReplyComment(IGDevice iGDevice) : base(iGDevice)
        {
        }

        public ViewReplyComment(string deviceID) : base(deviceID)
        {
        }

        public override void DoAction()
        {
            int retry = (new Random()).Next(10);

            int current_retry = 0;

            bool has_viewreplies = false;
            while (true)
            {

                if (!has_viewreplies)
                {

                    //Scroll

                    (new RandomScroll("down", deviceID)).DoAction();

                    try
                    {
                        //Check if ViewReplies
                        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.ViewReplies, 1, 5, 1);

                        ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.ViewReplies);

                        ADBSupport.ADBHelpers.Delay(2, 5);

                        has_viewreplies = true;
                    }
                    catch { }


                    current_retry++;

                    if (current_retry > retry)
                        break;
                }
                else
                {
                    //View previous reply
                    try
                    {
                        //Check if ViewReplies
                        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.ViewPreviousReplies, 1, 1, 1);


                        ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.ViewPreviousReplies);

                        ADBSupport.ADBHelpers.Delay(2, 5);
                    }
                    catch
                    {
                        break;
                    }

                }

            }
        }
    }
}
