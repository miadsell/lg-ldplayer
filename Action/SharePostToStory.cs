﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class SharePostToStory : IAction
    {
        public IGDevice IGDevice;

        string deviceID, post_url;

        public SharePostToStory(IGDevice iGDevice, string post_url)
        {
            IGDevice = iGDevice;
            this.deviceID = this.IGDevice.deviceID;
            this.post_url = post_url;
        }

        public void DoAction()
        {
            //open media

            ADBSupport.LDHelpers.OpenIGMediaByID(IGDevice.ld_devicename, post_url);

            //wait share btn
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, BMResource.SharePostToStory.SharePostToStory_ShareBtn, 5, 2, 1);

            //hit share btn
            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.SharePostToStory.SharePostToStory_ShareBtn);

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, BMResource.SharePostToStory.SharePostToStory_AddPostToYourStory, 5, 2, 1);

            //hit addpost to your story
            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.SharePostToStory.SharePostToStory_AddPostToYourStory);


            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, BMResource.SharePostToStory.SharePostToStory_SendToTag, 5, 2, 1);


            //hit your story by relative 
            //demoTag: 71.4,92.4 --demoA:3.9,96.5 --demoC: 16.6,97.3
            ADBSupport.ADBHelpers.ClickRelative(deviceID, BMResource.SharePostToStory.SharePostToStory_SendToTag, new { x = 71.4, y = 92.4 }, new { x = 3.9, y = 96.5 }, new { x = 16.6, y = 97.3 });

            //delay

            ADBSupport.ADBHelpers.Delay(10, 15);

            //check if ask stories archive

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PublishStories_AskStoriesArchive, 1, 3);
                ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.PublishStories_OK_StoriesArchive);
            }
            catch
            {

            }
        }
    }
}
