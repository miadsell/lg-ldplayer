﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class FollowPeople : EngagePostAction
    {
        public FollowPeople(IGDevice iGDevice) : base(iGDevice)
        {
        }

        public FollowPeople(string deviceID) : base(deviceID)
        {
        }

        public override void DoAction()
        {
            bool isfollowing = false;
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Follow1Btn, 1, 1, 1);
                ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Follow1Btn);
                isfollowing = true;
            }
            catch
            { }

            if (!isfollowing)
            {
                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Follow2Btn, 1, 1, 1);

                    ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Follow2Btn);

                    isfollowing = true;
                }
                catch
                { }
            }
        }
    }
}
