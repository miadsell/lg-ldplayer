﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class SearchPeople : IAction
    {
        string deviceID;
        string tab_search;
        string keyword;
        Class.IGDevice IGDevice;

        public SearchPeople(string deviceID, string keyword, string tab_search = null)
        {
            this.deviceID = deviceID;
            this.tab_search = tab_search == null ? "ACCOUNTS" : tab_search;
            this.keyword = keyword;
        }

        public SearchPeople(IGDevice iGDevice, string keyword,  string tab_search=null)
        {
            this.tab_search = tab_search == null ? "ACCOUNTS" : tab_search;
            this.keyword = keyword;
            this.IGDevice = iGDevice;
            this.deviceID = this.IGDevice.deviceID;
        }

        public void DoAction()
        {
            //Go to search tab
            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "instagram://explore", "com.instagram.android");

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_WaitSearchTab, 5, 5, 3);

            //Hit search box

            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.SearchBox);
            ADBSupport.ADBHelpers.Delay(2, 4);

            //Type keyword

            ADBSupport.ADBHelpers.InputTextWithDelay(deviceID, keyword);

            //Choose tab

            switch(tab_search)
            {
                case "TOP":
                    ClickTab(ADBSupport.BMPSource.Tab_Top);
                    break;
                case "ACCOUNTS":
                    ClickTab(ADBSupport.BMPSource.Tab_Accounts);
                    break;
                case "TAGS":
                    ClickTab(ADBSupport.BMPSource.Tab_Tags);
                    break;
                case "PLACES":
                    ClickTab(ADBSupport.BMPSource.Tab_Places);
                    break;
                default:
                    throw new System.ArgumentException("Not found tab for search");
            }

            //Click results
            var screen = ADBSupport.ADBHelpers.ScreenShoot(deviceID);

            //First priority, choose account has blut tick
            var blue_tickes= KAutoHelper.ImageScanOpenCV.FindOutPoints(screen, ADBSupport.BMPSource.BlueTick);

            if(blue_tickes.Count>0)
            {
                //click first blue tick
                KAutoHelper.ADBHelper.Tap(deviceID, blue_tickes[0].X, blue_tickes[0].Y);
            }
            else
            {
                //click random account
                var point_x = 14;
                var point_ys = new List<int>() { 34, 45, 56, 68, 80 };
                var point_y = point_ys[(new Random()).Next(point_ys.Count)];
                //click random account

                KAutoHelper.ADBHelper.TapByPercent(deviceID, point_x, point_y);
            }

            //Follow

            var follow = new Action.FollowPeople(IGDevice);
            follow.DoAction();



        }

        private void ClickTab(Bitmap click_bmp)
        {
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, click_bmp, 1, 2, 0);

                ADBSupport.ADBHelpers.ClickByImage(deviceID, click_bmp);

                ADBSupport.ADBHelpers.Delay(2, 4);
            }
            catch { }
        }
    }
}
