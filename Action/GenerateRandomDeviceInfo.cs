﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class GenerateRandomDeviceInfo : IAction
    {
        public string deviceID, gmail, deviceName, dim_path;

        public GenerateRandomDeviceInfo(string deviceID, string deviceName, string gmail, string dim_path = null)
        {
            this.deviceID = deviceID ?? throw new ArgumentNullException(nameof(deviceID));
            this.gmail = gmail;
            this.deviceName = deviceName;
            this.dim_path = dim_path;
        }

        public void DoAction()
        {
            string file_path = "";
            if (dim_path == null)
            {
                //Create DIM Setting Text File

                var lines = File.ReadAllLines("dim.txt").ToList<string>();

                for (int i = 0; i < lines.Count; i++)
                {
                    if (lines[i].Contains("<string name=\"gemail\"></string>"))
                    {
                        lines[i] = "<string name=\"gemail\">" + gmail + "</string>";
                    }
                }
                //ADBSupport.ADBHelpers.CreatePath(ADBSupport.LDHelpers.LDPlayerDATA_path + "\\" + deviceName + "\\system\\");
                //file_path = ADBSupport.LDHelpers.LDPlayerDATA_path + "\\" + deviceName + "\\system\\" + deviceName + ".txt";

                file_path = "Temp\\" + deviceName + ".txt";
                File.WriteAllLines(file_path, lines.ToArray<string>());


                IGHelpers.Helpers.FTP_CreateDirectory(ADBSupport.LDHelpers.ftp_dnplayerdata + deviceName + "/system/");
                IGHelpers.Helpers.FTP_UploadFile(file_path, ADBSupport.LDHelpers.ftp_dnplayerdata + deviceName + "/system/");

            }
            else file_path = dim_path;

            //Push DIM Settings to Device

            //ADBSupport.LDHelpers.PushFile(deviceName, file_path, "/sdcard/DIM/DIM_Settings.txt");

            ADBSupport.ADBHelpers.PushFileToPhone(deviceID, file_path, "/sdcard/DIM/DIM_Settings.txt");

            //Go home

            KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_HOME);

            ADBSupport.ADBHelpers.Delay(5);

            //Click device id masker

            KAutoHelper.ADBHelper.TapByPercent(deviceID, 49.0, 18.4);

            ADBSupport.ADBHelpers.Delay(1,3);

            ////Scroll to network country

            //ADBSupport.ADBHelpers.ScrollDownToImage(deviceID, 76.8, ADBSupport.BMPSource.CurrentNetworkCountry);

            ////Scroll CurrentNetwork Country to pos

            //ADBSupport.ADBHelpers.ScrollImageToPosByPercent(deviceID, ADBSupport.BMPSource.CurrentNetworkCountry, 53.9);

            ////Tap input

            //var point_percentage = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.CurrentNetworkCountry);

            //KAutoHelper.ADBHelper.TapByPercent(deviceID, point_percentage.x + 5, point_percentage.y + 6);

            ////Send vn

            //KAutoHelper.ADBHelper.InputText(deviceID, "vn");

            ////Scroll to network type

            //ADBSupport.ADBHelpers.ScrollDownToImage(deviceID, 76.8, ADBSupport.BMPSource.CurrentNetworkType);

            ////Scroll CurrentNetwork Country to pos

            //ADBSupport.ADBHelpers.ScrollImageToPosByPercent(deviceID, ADBSupport.BMPSource.CurrentNetworkType, 53.9);

            ////Tap input

            //point_percentage = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.CurrentNetworkType);

            //KAutoHelper.ADBHelper.TapByPercent(deviceID, point_percentage.x + 5, point_percentage.y + 6);

            ////Send 4G

            //KAutoHelper.ADBHelper.InputText(deviceID, "4G");            ////Scroll to network country


            ////Scroll to gmail

            //ADBSupport.ADBHelpers.ScrollDownToImage(deviceID, 76.8, ADBSupport.BMPSource.CurrentGoogleEmail);

            ////Scroll CurrentNetwork Country to pos

            //ADBSupport.ADBHelpers.ScrollImageToPosByPercent(deviceID, ADBSupport.BMPSource.CurrentGoogleEmail, 53.9);

            ////Tap input

            //var point_percentage = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.CurrentGoogleEmail);

            //KAutoHelper.ADBHelper.TapByPercent(deviceID, point_percentage.x + 5, point_percentage.y + 6);

            ////Tap Random

            ////ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.RandomBtn);

            ////Send gmail

            //KAutoHelper.ADBHelper.InputText(deviceID, gmail);

            ////Tap apply all

            //ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.ApplyAll);

            //Click setting

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.ClickSettingButton);
            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.ClickSettingButton, ADBSupport.BMPSource.ClickSettings);


            //Click settings and wait

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.ClickSettings, ADBSupport.BMPSource.WaitSettingsPage);

            //Scroll to bottom

            ADBSupport.ADBHelpers.ScrollDownToImage(deviceID, 72.7, ADBSupport.BMPSource.ImportSettings);

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.ImportSettings, ADBSupport.BMPSource.DeviceIDMaskerLite);

            //Click setting
            ADBSupport.ADBHelpers.Delay();
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.ClickSettingButton);
            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.ClickSettingButton, ADBSupport.BMPSource.ClickSettings);

            //Click reboot now

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Reboot);
            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Reboot);

            //Remove panel

           //IGHelpers.ViewDeviceHelpers.RemovePanel(deviceName, true);

        }
    }
}
