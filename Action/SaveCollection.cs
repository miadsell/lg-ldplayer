﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class SaveCollection : EngagePostAction
    {
        public SaveCollection(IGDevice iGDevice) : base(iGDevice)
        {
        }

        public SaveCollection(string deviceID) : base(deviceID)
        {
        }

        public override void DoAction()
        {
            this.UpdateEngageButtonPos(true);

            ClickAddSaveCollection();

        }


        private void ClickAddSaveCollection()
        {
            if (savecollection_point == null)
                return;
            KAutoHelper.ADBHelper.TapByPercent(deviceID, savecollection_point.x, savecollection_point.y);
        }
    }
}
