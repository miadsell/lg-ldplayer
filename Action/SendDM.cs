﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class SendDM : IAction
    {
        public IGDevice IGDevice;
        string spin_message, image_path;
        string deviceID;
        bool send_if_nomessage_before;

       /// <summary>
       /// must open profile first (important: image_path must be modify to unique before send to this func or it can lead to banned bulk instagram accounts)
       /// </summary>
       /// <param name="iGDevice"></param>
       /// <param name="spin_message"></param>
       /// <param name="image_path">just support 1 image, and if message was sent successful, action marked as success even image can't be sent, make sure you remember it when set message</param>
       /// <param name="send_if_nomessage_before"></param>
        public SendDM(IGDevice iGDevice, string spin_message, string image_path=null, bool send_if_nomessage_before = false)
        {
            IGDevice = iGDevice;
            this.spin_message = spin_message;
            this.image_path = image_path;
            this.deviceID = IGDevice.deviceID;
            this.send_if_nomessage_before = send_if_nomessage_before;
        }

        public void DoAction()
        {

            //click message
            if(ADBSupport.ADBHelpers.IsImageExisted(deviceID, BMResource.SendDM.DM_MessageBtn,5,1,1))
                ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.SendDM.DM_MessageBtn);
            else
            {
                //click top right menu

                for(int try_click_menu=0;try_click_menu<3;try_click_menu++)
                {
                    ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.SendDM.SendDM_TopRightMenu);

                    if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, BMResource.SendDM.SendDM_SendMessage_MenuItem, 5, 1, 1))
                        break;
                }

                ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.SendDM.SendDM_SendMessage_MenuItem);
            }

            //wait messagebox
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, BMResource.SendDM.DM_MessageBox, 2, 5, 3);

            if(send_if_nomessage_before)
            {
                //check neu da send message truoc do thi return
                if (!ADBSupport.ADBHelpers.IsImageExisted(deviceID, BMResource.SendDM.DM_NoMessageBefore, 1, 2, 1))
                    return;
            }

            string message = SharpSpin.Spinner.Spin(spin_message);

            //type message
            ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID, IGDevice.ld_devicename, message);

            ADBSupport.ADBHelpers.Delay(1, 3);

            //hit send
            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.SendDM.DM_SendBtn);

            //wait send btn invisible
            if (!ADBSupport.ADBHelpers.IsImageExisted(deviceID, BMResource.SendDM.DM_SendBtn, 5, 3, 2))
            {

            }
            else
            {
                throw new System.ArgumentException("SendDM|Failed to send message");
            }

            //vi send image bị blurry do đó tạm thời disable tính năng này, sẽ enable khi ok
            ////send images if has
            //if (image_path != null && image_path != "")
            //{
            //    ADBSupport.ADBHelpers.PushImageandScanGallery(deviceID, image_path);
            //    //click image option
            //    ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.SendDM.SendDM_ChooseImage);

            //    //wait gallery show
            //    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, BMResource.SendDM.SendDM_Gallery, 5, 2, 1);

            //    //click first image based on relative
            //    //demoTag: 16.9,38.6 --demoA: 5.4,46.8 --demoC: 25.6,57.3

            //    ADBSupport.ADBHelpers.ClickRelative(deviceID, BMResource.SendDM.SendDM_Gallery, new { x = 16.9, y = 38.6 }, new { x = 5.4, y = 46.8 }, new { x = 25.6, y = 57.3 });

            //    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, BMResource.SendDM.SendDM_SendImageBtn, 5, 2, 1);

            //    //Click sendimagebtn
            //    ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.SendDM.SendDM_SendImageBtn);

            //    //verify sendimagebtn is hide
            //    ADBSupport.ADBHelpers.WaitImageInsivible(deviceID, BMResource.SendDM.SendDM_SendImageBtn, 20);
            //}
        }
    }
}
