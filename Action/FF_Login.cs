﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class FF_Login : IAction
    {
        string deviceID;

        string username, password;

        public FF_Login(string deviceID, string username, string password)
        {
            this.deviceID = deviceID;
            this.username = username;
            this.password = password;
        }

        public void DoAction()
        {
            Login();
        }

        private void Login()
        {
            //clear firefox lite data
            ADBSupport.ADBHelpers.ClearAPPData(this.deviceID, "org.mozilla.rocket");

            //open firefox
            var command_launch = ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell monkey -p org.mozilla.rocket -c android.intent.category.LAUNCHER 1");

            // and wait ff open
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, BMResource.FF_Login.FFLite_BrowseTheWeb, 5, 5, 1);

            //click blue btn
            ADBSupport.ADBHelpers.ClickandWaitRandom(deviceID, BMResource.FF_Login.FFLite_BlueBtn, BMResource.FF_Login.FFLite_Done, 1);

            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.FF_Login.FFLite_Done);

            //wait and click later
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, BMResource.FF_Login.FFLite_Later, 5, 5, 1);

            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.FF_Login.FFLite_Later);

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, BMResource.FF_Login.FFLite_InstagramIcon);


            //hit open instagram
            ADBSupport.ADBHelpers.ClickandWaitRandom(deviceID, BMResource.FF_Login.FFLite_InstagramIcon, BMResource.FF_Login.FFLite_IG_Login, 1);

            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.FF_Login.FFLite_IG_Login);

            //fill username and password
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, BMResource.FF_Login.FFLite_IG_Username, 5, 5, 1);

            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.FF_Login.FFLite_IG_Username);

            ADBSupport.ADBHelpers.InputTextWithDelay(deviceID, username);

            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.FF_Login.FFLite_IG_Password);

            ADBSupport.ADBHelpers.InputTextWithDelay(deviceID, password);


            //click login
            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.FF_Login.FFLite_IG_LoginBtn);

            //wait if ask close resource
            if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, BMResource.FF_Login.FFLite_IG_CloseResources, 5, 5, 2))
            {
                ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.FF_Login.FFLite_IG_CloseResources);
            }

            //wait if ask not now
            if (ADBSupport.ADBHelpers.IsImageExisted(deviceID,BMResource.FF_Login.FFLite_IG_NotNow,5,5,2))
            {
                ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.FF_Login.FFLite_IG_NotNow);
            }

            //wait if ask change password
            //if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, BMResource.FF_Login.FFLite_IG_ChangePassword, 5, 5, 2))
            //{
            //    ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.FF_Login.FFLite_IG_ChangePassword);

            //    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, BMResource.FF_Login.FFLite_IG_OldPassBox, 5, 5, 1);

            //    //fill pass
            //    ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.FF_Login.FFLite_IG_OldPassBox);

            //    ADBSupport.ADBHelpers.InputTextWithDelay(deviceID, password);

            //    string newpass = CreatePassword(11);

            //    //fill newpass
            //    ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.FF_Login.FFLite_IG_NewPassBox);

            //    ADBSupport.ADBHelpers.InputTextWithDelay(deviceID, newpass);

            //    //fill newpass
            //    ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.FF_Login.FFLite_IG_ConfirmNewPassBox);

            //    ADBSupport.ADBHelpers.InputTextWithDelay(deviceID, newpass);
            //}

            //wait instagram
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, BMResource.FF_Login.FFLite_IG_TopCamera, 5, 5, 1);

            //hit use app
            //while(true)
            //{
            //    ADBSupport.ADBHelpers.RandomScrollScreen(deviceID, 1, 5, 78.7, 38.4);

            //    if(ADBSupport.ADBHelpers.IsImageExisted(deviceID,BMResource.FF_Login.FFLite_IG_UseTheAppBtn,1,2))
            //    {
            //        ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.FF_Login.FFLite_IG_UseTheAppBtn);
            //        Thread.Sleep(10000);
            //        break;
            //    }

            //}
            //launch instagram app
            ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell monkey -p com.instagram.android -c android.intent.category.LAUNCHER 1", 15);
        }

        public string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }
    }
}
