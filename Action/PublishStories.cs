﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class PublishStories : IAction
    {

        public IGDevice IGDevice;

        string deviceID;

        string imagepath;

        public PublishStories(IGDevice iGDevice, string imagepath)
        {
            IGDevice = iGDevice;
            this.imagepath = imagepath;
            this.deviceID = this.IGDevice.deviceID;
        }

        public void DoAction()
        {
            //ADBSupport.ADBHelpers.DownloadImage(IGDevice, image_link);

            ADBSupport.ADBHelpers.PushImageandScanGallery(deviceID, imagepath);

            //Go to home instagram

            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "https://instagram.com", "com.instagram.android");

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.AddPost, 5, 5, 5);

            ADBSupport.ADBHelpers.Delay(3, 8);

            //Click add stories

            Stopwatch watch_addstory = new Stopwatch();
            watch_addstory.Start();

            while (true)
            {

                ADBSupport.ADBHelpers.LongPressRelative(deviceID, ADBSupport.BMPSource.PublishStories_TagCameraBtn, new { x = 5.1, y = 5.8 }, new { x = 9, y = 14.9 }, new { x = 14.8, y = 19.2 }, 2000);

                //check if show add to your story
                if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.PublishStories_AddToYourStory, 5, 3, 1))
                {
                    //Click add to your story

                    ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.PublishStories_AddToYourStory, ADBSupport.BMPSource.PublishStories_ShootBtn, 2);
                    break;
                }
                else if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.PublishStories_ShootBtn, 1, 1))
                {
                    //khong hien popup add to your story => co the vao phan add story luon
                    break;
                }
                else if (!ADBSupport.ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.AddPost, 1, 1))
                {
                    //check if on home, if not => go home
                    ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "https://instagram.com", "com.instagram.android");

                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.AddPost, 5, 1, 2);

                }

                //try
                //{
                //    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PublishStories_AddToYourStory, 1, 5);
                //    break;
                //}
                //catch 
                //{ 
                //}

                if (watch_addstory.Elapsed.TotalMinutes > 2)
                    throw new System.ArgumentException("Can't wait show add to your story");

            }



            //ClickRelative Gallery
            ClickRelative:
            if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.PublishStories_ShootBtn, 1, 2, 1))
            {
                ADBSupport.ADBHelpers.ClickRelative(deviceID, ADBSupport.BMPSource.PublishStories_ShootBtn, new { x = 39.7, y = 76 }, new { x = 7.3, y = 94.2 }, new { x = 11.4, y = 95.4 });
                Thread.Sleep(5000);
                goto ClickRelative;
            }



            ////check if shootbtn still show
            //try
            //{
            //    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PublishStories_ShootBtn, 5, 1, 1);
            //    goto ClickRelative;
            //}
            //catch { }


            //Wait gallery

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PublishStories_Gallery, 2, 5, 3);


            ADBSupport.ADBHelpers.ScannGallery(deviceID);

            //Choose latest image

            ADBSupport.ADBHelpers.ClickRelative(deviceID, ADBSupport.BMPSource.PublishStories_Gallery, new { x = 4.2, y = 9.1 }, new { x = 6.9, y = 18.6 }, new { x = 28.6, y = 31.8 });

            //Hit relative your stories

            //wait tag send to btn

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PublishStories_TagSendToBtn, 2, 5, 2);

            ADBSupport.ADBHelpers.ClickRelative(deviceID, ADBSupport.BMPSource.PublishStories_TagSendToBtn, new { x = 71.4, y = 92.1 }, new { x = 8.6, y = 91.1 }, new { x = 12.8, y = 93 });


            //delay

            ADBSupport.ADBHelpers.Delay(10, 15);

            //check if ask stories archive

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PublishStories_AskStoriesArchive, 1, 3);
                ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.PublishStories_OK_StoriesArchive);
            }
            catch
            {

            }
        }
    }
}
