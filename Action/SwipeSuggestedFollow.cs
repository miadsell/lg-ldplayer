﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class SwipeSuggestedFollow : IAction
    {
        int min_time = 20;
        int max_time = 60;
        string deviceID;

        public SwipeSuggestedFollow(string deviceID)
        {
            this.deviceID = deviceID;
        }

        public void DoAction()
        {
            ScrollSuggestToViewFull();
        }


        /// <summary>
        /// Scroll To View Full Suggested For You Element
        /// </summary>
        private void ScrollSuggestToViewFull()
        {
            ///height: 40 percentage
            ///suggested label: nằm từ 10 -> 50 percentage
            ///

            double destination_y = (new Random()).NextDouble() * (50 - 10) + 10;

            ADBSupport.ADBHelpers.ScrollImageToPosByPercent(deviceID, ADBSupport.BMPSource.SuggestedForYou, destination_y);

            //Swipe

            double swipe_y = destination_y + 40 / 2; //40 la height cua suggested element

            List<string> vectors = new List<string>() { "left", "right" };

            int swipe_left = 0;

            var choice_vector = "";

            int target_watch_time = (new Random()).Next(min_time, max_time);


            Stopwatch watch_suggested = new Stopwatch();
            watch_suggested.Start();

            while (true)
            {

                if (watch_suggested.Elapsed.TotalSeconds > target_watch_time)
                    break;

                if (swipe_left <= 1)
                {
                    choice_vector = "left";
                }
                else choice_vector = vectors[(new Random()).Next(vectors.Count)];

                switch (choice_vector)
                {
                    case "left":
                        swipe_left++;

                        //KAutoHelper.ADBHelper.SwipeByPercent(deviceID, 75, swipe_y, 23, swipe_y, 400);

                        var start_x_left = ADBSupport.ADBHelpers.RandomDoubleValue(70, 80);

                        KAutoHelper.ADBHelper.SwipeByPercent(deviceID, start_x_left, swipe_y, start_x_left - 52, swipe_y, (new Random()).Next(400, 450));

                        break;
                    case "right":
                        swipe_left--;

                        //KAutoHelper.ADBHelper.SwipeByPercent(deviceID, 23, swipe_y, 75, swipe_y, 400);

                        var start_x_right = ADBSupport.ADBHelpers.RandomDoubleValue(18, 28);

                        KAutoHelper.ADBHelper.SwipeByPercent(deviceID, start_x_right, swipe_y, start_x_right + 52, swipe_y, (new Random()).Next(400, 450));

                        break;
                }
                //Thread some seconds

                ADBSupport.ADBHelpers.Delay(1, 2);

                //Decide to follow or not

                List<string> actions = new List<string>() { "continue", "follow", "continue", "continue" };

                var choice_action = actions[(new Random()).Next(actions.Count)];

                switch (choice_action)
                {
                    case "follow":
                        var screen = ADBSupport.ADBHelpers.ScreenShoot(deviceID);
                        var follow_btn = KAutoHelper.ImageScanOpenCV.FindOutPoints(screen, ADBSupport.BMPSource.Suggested_Follow);
                        //KAutoHelper.ADBHelper.Tap(deviceID, follow_btn[0].X, follow_btn[0].Y);
                        ADBSupport.ADBHelpers.RandomTapImage(deviceID, follow_btn[0].X, follow_btn[0].Y, "Suggested_Follow");
                        break;
                    case "continue":
                        continue;
                }

                ADBSupport.ADBHelpers.Delay(1, 3);
            }
        }
    }
}
