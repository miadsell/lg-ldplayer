﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class SetPrivacy : IAction
    {

        public IGDevice IGDevice;

        string deviceID;

        bool seton;

        public SetPrivacy(IGDevice iGDevice, bool seton)
        {
            IGDevice = iGDevice;
            this.seton = seton;
            this.deviceID = iGDevice.deviceID;
        }

        public void DoAction()
        {
            //Goto profile

            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "instagram://profile", "com.instagram.android");


            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_Goto_EditProfileScreen, 5, 5, 5);

            //Click setting

            ADBSupport.ADBHelpers.ClickandWait(deviceID, BMResource.AccountPrivacy.AccountPrivacy_SettingMenu, ADBSupport.BMPSource.SettingsText, 1);

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.SettingsText, ADBSupport.BMPSource.PrivacyMenu, 1);

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.PrivacyMenu, ADBSupport.BMPSource.AccountPrivacy, 1);

            //wait setting

            //Click Account Privacy

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.AccountPrivacy, ADBSupport.BMPSource.WaitPrivateAccount, 1);

            //seton

            if (seton)
            {
                if (isPrivateOn(deviceID))
                    return;

                //Click setting button
                KAutoHelper.ADBHelper.TapByPercent(deviceID, 89.3, 16.3);

                //Wait popup ok

                if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.OkPopup, 1, 5, 1))
                {

                    //Click ok

                    ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.OkPopup);
                }

                //ask review followers
                if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, BMResource.AccountPrivacy.AccountPrivacy_AskReviewFollowers, 1, 5, 1))
                {

                    //Click ok

                    ADBSupport.ADBHelpers.ClickByImage(deviceID, BMResource.AccountPrivacy.AccountPrivacy_Cancel);
                }


            }
            else
            {
                if (isPrivateOff(deviceID))
                {
                    return;
                }

                //Click setting button
                KAutoHelper.ADBHelper.TapByPercent(deviceID, 89.3, 16.3);

                //Wait popup ok

                if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.OkPopup, 1, 5, 1))
                {

                    //Click ok

                    ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.OkPopup);
                }
            }


            ADBSupport.ADBHelpers.Delay(5, 10);
        }

        private bool isPrivateOn(string deviceID)
        {

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PrivateOn, 1, 5, 0);
                //if already on -> return

                return true;
            }
            catch
            {

            }

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PrivateOn_1, 1, 5, 0);
                //if already on -> return

                return true;
            }
            catch
            {

            }

            return false;
        }

        private bool isPrivateOff(string deviceID)
        {
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PrivateOff, 1, 5, 0);
                //if already on -> return

                return true;
            }
            catch
            {

            }

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PrivateOff_1, 1, 5, 0);
                //if already on -> return

                return true;
            }
            catch
            {

            }

            return false;
        }
    }
}
