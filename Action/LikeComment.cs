﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class LikeComment : EngagePostAction
    {
        public LikeComment(IGDevice iGDevice) : base(iGDevice)
        {
        }

        public LikeComment(string deviceID) : base(deviceID)
        {
        }

        public override void DoAction()
        {
            //Get list comment like btn

            var screen = ADBSupport.ADBHelpers.ScreenShoot(deviceID);

            List<Point> likebtn = new List<Point>();

            for (int i = 0; i < 2; i++)
            {

                likebtn = KAutoHelper.ImageScanOpenCV.FindOutPoints(screen, ADBSupport.BMPSource.LikeCommentBtn);

                if (likebtn.Count > 0)
                    break;
            }

            if (likebtn.Count == 0)
                return;

            var choiced_comment = likebtn[(new Random()).Next(likebtn.Count)];

            //Hit like

            //KAutoHelper.ADBHelper.Tap(deviceID, choiced_comment.X, choiced_comment.Y);

            ADBSupport.ADBHelpers.RandomTapImage(deviceID, choiced_comment.X, choiced_comment.Y, "LikeCommentBtn");
        }
    }
}
