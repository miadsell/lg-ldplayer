﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class ViewPost : EngagePostAction
    {
        public ViewPost(IGDevice iGDevice) : base(iGDevice)
        {
        }

        public ViewPost(string deviceID) : base(deviceID)
        {
        }

        public override void DoAction()
        {
            this.UpdateEngageButtonPos(true);

            

            var choiced = (new Random()).Next(2);

            if (rec_viewcomment == null) //tuc la khong co comment
            {
                choiced = 1;
            }

            switch (choiced)
            {
                case 0:
                    ClickViewCommentsText();
                    break;
                case 1:
                    ClickCommentIcon();
                    break;
            }

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.WaitViewPostPage);
        }

        private void ClickViewCommentsText()
        {
            try
            {
                ADBSupport.ADBHelpers.TapRecTanglePercentage(deviceID, rec_viewcomment, ADBSupport.BMPSource.WaitViewPostPage, 1, 0.25);
            }
            catch
            {
                ClickCommentIcon();
            }
        }

        private void ClickCommentIcon()
        {
            ADBSupport.ADBHelpers.TapByPercentage(deviceID, viewcomment_icon_point.x, viewcomment_icon_point.y, ADBSupport.BMPSource.WaitViewPostPage);
        }
    }
}
