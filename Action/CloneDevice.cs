﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class CloneDevice : IAction
    {
        string device_name;
        string fullname;

        public CloneDevice(string device_name, string fullname)
        {
            this.device_name = device_name ?? throw new ArgumentNullException(nameof(device_name));
            this.fullname = fullname ?? throw new ArgumentNullException(nameof(fullname));
        }

        public void DoAction()
        {
            ADBSupport.LDHelpers.CloneDevice(device_name);
            ADBSupport.LDHelpers.GenerateDeviceInfo(device_name);

            //Open and Get deviceID

            string deviceID = ADBSupport.LDHelpers.LaunchLDPlayer(device_name, false);

            //Set locale region

            ADBSupport.ADBHelpers.SetLocaleRegion(deviceID);

            ////Set gmail in device id masker

            //var imei = ADBSupport.LDHelpers.GetIMEI(device_name);
            //var google_account = fullname.Replace(" ", "").Replace("_", "") + imei.Substring(imei.Length - 3) + "@gmail.com";

            //var generate_device_info = new Action.GenerateRandomDeviceInfo(deviceID, device_name, google_account);
            //generate_device_info.DoAction();


        }
    }
}
