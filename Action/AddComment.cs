﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.Action
{
    class AddComment : EngagePostAction
    {
        string comment_content;

        public AddComment(IGDevice iGDevice) : base(iGDevice)
        {
        }

        public AddComment(string deviceID) : base(deviceID)
        {
        }

        public override void DoAction()
        {
            this.UpdateEngageButtonPos(true);


            //get post_link

            string post_link = null;

            //scroll to dropdown of current post to get link

            //if (this.addcommentbtn_point != null)
            //{
            //    while (true)
            //    {
            //        ADBSupport.ADBHelpers.ScrollUpToImage(deviceID, 58.3, ADBSupport.BMPSource.Post_DropDownMenu, 10, 800);
            //        var dropdown_settings_point_temp = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.Post_DropDownMenu);

            //        if (dropdown_settings_point_temp != null)
            //        {
            //            if (dropdown_settings_point_temp.y < this.addcommentbtn_point.y)
            //                break;
            //        }
            //    }
            //}
            //else
            //{
            //    while (true)
            //    {
            //        ADBSupport.ADBHelpers.ScrollUpToImage(deviceID, 58.3, ADBSupport.BMPSource.Post_DropDownMenu, 10, 800);
            //        var dropdown_settings_point_temp = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.Post_DropDownMenu);

            //        if (dropdown_settings_point_temp != null)
            //        {
            //            break;
            //        }
            //    }
            //}

            var dropdown_settings_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.Post_DropDownMenu);

            //Copy link
            //Mutex m = new Mutex(false, "copylink");
            //m.WaitOne();
            lock (ADBSupport.LDHelpers.lock_copy)
            {
            CopyLink:
                ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.Post_DropDownMenu, ADBSupport.BMPSource.CopyLink);

                ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.CopyLink);

                //Wait loading is hide

                while (true)
                {
                    try
                    {
                        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.WaitLoading_CopyLink, 1, 1, 3);
                    }
                    catch
                    {
                        break;
                    }
                }

                post_link = ADBSupport.ADBHelpers.ContentFromClipBoard(deviceID);

                if (!post_link.Contains("instagram.com"))
                {
                    goto CopyLink;
                }
                Clipboard.Clear();
            }
            
            //Longpress to show Paste btn
            //while (true)
            //{
            //    //Long press

            //    KAutoHelper.ADBHelper.LongPress(deviceID, ADBSupport.ADBHelpers.ConvertPercentageToPoint(deviceID, "X", 25.5), ADBSupport.ADBHelpers.ConvertPercentageToPoint(deviceID, "Y", 96.1), 2000);

            //    //Wait paste button show
            //    try
            //    {
            //        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PasteBtn, 1, 0, 1);
            //        break;
            //    }
            //    catch { }

            //}
            ////Paste text from clipboard

            //ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.PasteBtn); Thread.Sleep(1000);

            ////Hit back

            //ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.BackEsEditor, ADBSupport.BMPSource.ConfirmYesEsEditor, 1);

            //ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.ConfirmYesEsEditor);

            //var post_link_path = ADBSupport.LDHelpers.LDPlayerDATA_path + "\\" + deviceName + "\\temp_img\\";

            ////Pull to local

            //ADBSupport.ADBHelpers.PullFileToLocal(deviceID, "sdcard/Download/Comment/post_link.txt", post_link_path);

            //post_link = File.ReadAllText(post_link_path + "post_link.txt");

            ////Hit yes to save


            if (comment_content == null)
            {
                var dyn = IGHelpers.Helpers.APIObJWithProxy();

                comment_content = IGAPIHelper.Scraper.ScrapeComment(dyn.instaApi, post_link);

                comment_content = comment_content.Replace("@", "");
            }


            //Scroll to box

            this.UpdateEngageButtonPos(true);

            var choiced = (new Random()).Next(2);

            //if (addcommentbtn_point == null && rec_addcommentbox == null)
            //    return;


            switch (choiced)
            {
                case 0:
                    ClickAddCommentBox();
                    break;
                case 1:
                    ClickAddCommentIcon();
                    break;
            }


            //Send unicode base64 text
            ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID, deviceName, comment_content);


            ////Back to instagram
            ///
            //ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell am start -n com.instagram.android/com.instagram.android.activity.MainTabActivity");


            //Stopwatch watch_longpress = new Stopwatch();
            //watch_longpress.Start();

            //while (true)
            //{
            //    if (watch_longpress.Elapsed.TotalMinutes > 1)
            //        throw new System.ArgumentException("Can't paste text");

            //    //Long press

            //    KAutoHelper.ADBHelper.LongPress(deviceID, ADBSupport.ADBHelpers.ConvertPercentageToPoint(deviceID, "X", 25.5), ADBSupport.ADBHelpers.ConvertPercentageToPoint(deviceID, "Y", 96.1), 1000);

            //    //Wait paste button show
            //    try
            //    {
            //        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PasteBtn, 1, 0, 1);
            //        break;
            //    }
            //    catch { }

            //}
            ////Paste text from clipboard

            //ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.PasteBtn);

            //Click post

            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.ClickPost);
        }

        private void ClickAddCommentBox()
        {
            ADBSupport.ADBHelpers.TapRecTanglePercentage(deviceID, rec_addcommentbox);
        }

        private void ClickAddCommentIcon()
        {
            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.AddCommentButton);    
        }
    }
}
