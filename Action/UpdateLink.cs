﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class UpdateLink : IAction
    {
        public IGDevice IGDevice;
        public string biolink, deviceID;

        public UpdateLink(IGDevice iGDevice, string biolink)
        {
            this.IGDevice = iGDevice;
            this.biolink = biolink;
            this.deviceID = IGDevice.deviceID;
        }

        public void DoAction()
        {
            //UPDATE CONTENT TO BIO
            //Goto profile

            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "instagram://profile", "com.instagram.android");


            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_Goto_EditProfileScreen, 5, 5, 5);

            //Hit editprofile

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.Global_Goto_EditProfileScreen, ADBSupport.BMPSource.SwitchAccount, 1);

            //CLICK link textbox
            //demoTag: 4.5,70.4 --A: 6.9,64.5 --C: 17.9,65.9
            ADBSupport.ADBHelpers.ClickRelative(deviceID, BMResource.UpdateBioLink.UpdateBioLink_BioTag, new { x = 4.5, y = 70.4 }, new { x = 6.9, y = 64.5 }, new { x = 17.9, y = 65.9 });

            Thread.Sleep(2000);

            //Check if EmptyLink

            if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, BMResource.UpdateBioLink.UpdateBioLink_WebsiteEmpty, 1))
            {
                //empty link
                //click websitebox
                ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.UpdateBioLink.UpdateBioLink_WebsiteEmpty);

                //send link
                ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID, IGDevice.ld_devicename, biolink);
            }
            else
            {
                //need to clear first

                while (true)
                {
                    //Clear link textbox XClickRelative

                    //demoTag: 5.2, 59.5 --demoA: 7.3, 63.8 --demoC: 24.5, 64.3
                    ADBSupport.ADBHelpers.LongPressRelative(deviceID, BMResource.UpdateBioLink.UpdateBioLink_WebsiteTitle_Tag, new { x = 5.5, y = 59.5 }, new { x = 7.3, y = 63.8 }, new { x = 24.5, y = 64.3 }, 2000);

                    if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.BioSelectAll, 1, 5, 5, 0.8))
                    {

                        //Click select all
                        ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.BioSelectAll);

                        //Send key delete

                        ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell input keyevent 67"); Thread.Sleep(1000);

                        //Check if clear

                        if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, BMResource.UpdateBioLink.UpdateBioLink_WebsiteEmpty, 1))
                        {
                            break;
                        }
                    }

                }

                //Send link
                ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID, IGDevice.ld_devicename, biolink);


            }

            Thread.Sleep(2000);

            //Hit update
            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.UpdateBioLink.UpdateBioLink_UpdateTickBtn);

            //Wait editprofile
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_Goto_EditProfileScreen, 5, 5, 5);
        }
    }
}
