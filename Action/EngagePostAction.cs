﻿
using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    abstract class EngagePostAction : IAction
    {

        public Class.IGDevice IGDevice;
        protected string deviceID, deviceName;

        #region --Button Pos--

        /// <summary>
        /// point in percentage
        /// </summary>
        protected dynamic likebtn_point, liked_btn_point, viewcomment_icon_point, savecollection_point, more_point;//, addcommentbtn_point;
        protected Class.RecPercentage rec_addcommentbox, rec_viewcomment;

        /// <summary>
        /// Rectangle of post (image or video)
        /// </summary>
        protected Class.RecPercentage rec_media;

        protected EngagePostAction(IGDevice iGDevice)
        {
            IGDevice = iGDevice ?? throw new ArgumentNullException(nameof(iGDevice));
            this.deviceID = this.IGDevice.deviceID;
            this.deviceName = this.IGDevice.ld_devicename;
        }

        //for testing
        protected EngagePostAction(string deviceID)
        {
            this.deviceID = deviceID ?? throw new ArgumentNullException(nameof(deviceID));
        }




        #endregion

        abstract public void DoAction();



        /// <summary>
        /// scroll post to show full engage button
        /// </summary>
        /// 
        protected void ScrollPostToViewFull()
        {
            var destination_point_y = PositionToViewFullPost();

            //Scroll post to position (scroll dropdown menu to top screen

            ADBSupport.ADBHelpers.ScrollImageToPosByPercent(deviceID, ADBSupport.BMPSource.Post_DropDownMenu, destination_point_y);

            //Check if se viewcomment_icon, if not scroll to see

            Stopwatch watch_scroll = new Stopwatch();
            watch_scroll.Start();

            while(true)
            {
                var temp_likebtn_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.LikeButton);
                var temp_liked_btn_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.Liked_Button);

                if (temp_likebtn_point != null || temp_liked_btn_point != null)
                    break;

                //KAutoHelper.ADBHelper.SwipeByPercent(deviceID, 52, 58.3, 52.6, 58.3 - 10, 800);

                var random_y = (new Random()).Next(58, 79);
                var dest_y = random_y - 10;

                ADBSupport.ADBHelpers.SwipeByPercent_RandomTouch(deviceID, "doc", "21-87", random_y, dest_y, (new Random()).Next(800, 1000));

                if (watch_scroll.Elapsed.TotalMinutes > 2)
                    throw new System.ArgumentException("Cant sroll to view full post ScrollPostToViewFull");
            }


            ADBSupport.ADBHelpers.ScrollDownToImage(deviceID, 58.3, ADBSupport.BMPSource.CommentButton, 10, 800);
        }

        protected void UpdateEngageButtonPos(bool isneedfull=false) //need full information or not
        {
            try
            {
                ScrollPostToViewFull();
            }
            catch
            {
                ////k co dropdown_menu tren man hinh
            }

            if (isneedfull)
            {
                //Scroll until see AddACommentBox or Next DropDown //first show, take first
                System.Diagnostics.Stopwatch stopwatch_scrollfull = new System.Diagnostics.Stopwatch();

                stopwatch_scrollfull.Start();
                
                while (true)
                {
                    var temp_timeago_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.Post_TimeAgo);
                    var temp_nextdropdown_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.NextDropDown);

                    if (temp_timeago_point != null)
                        break;

                    if (temp_nextdropdown_point != null)
                    {
                        //toa do y phai lon hon dropdown of current pos (25)
                        if (temp_nextdropdown_point.y > 25)
                            break;
                    }

                    //KAutoHelper.ADBHelper.SwipeByPercent(deviceID, 52, 58.3, 52.6, 58.3 - 10, 800);

                    var random_y = (new Random()).Next(58, 79);
                    var dest_y = random_y - 10;

                    ADBSupport.ADBHelpers.SwipeByPercent_RandomTouch(deviceID, "doc", "21-87", random_y, dest_y, (new Random()).Next(800, 1000));

                    if (stopwatch_scrollfull.Elapsed.TotalMinutes > 2)
                    {
                        throw new System.ArgumentException("Can't scroll to view full post");
                    }
                }

            }


            likebtn_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.LikeButton);
            liked_btn_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.Liked_Button);

            //use for find dropdown
            var temp_likebtn_point = likebtn_point == null ? liked_btn_point : likebtn_point;

            viewcomment_icon_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.CommentButton);
            savecollection_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.SaveCollectionButton);
            more_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.MoreCaption);

            //Stopwatch watch_addcommentbtn = new Stopwatch();
            //watch_addcommentbtn.Start();

            //while (addcommentbtn_point == null)
            //{
            //    addcommentbtn_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.AddCommentButton);


            //    if(watch_addcommentbtn.Elapsed.TotalSeconds>60)
            //    {
            //        break;
            //    }

            //    Thread.Sleep(10000);
            //}

            try
            {
                rec_addcommentbox = new Class.RecPercentage(ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.AddACommentBox), 52.4, 1);
                rec_viewcomment = new Class.RecPercentage(ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.ViewCommentLink), 18, 1);
            }
            catch { }

            var dropdown_menu = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.Post_DropDownMenu);

            dynamic media_point_percentage = null;



            if (dropdown_menu != null)
            {
                if (dropdown_menu.y < temp_likebtn_point.y) //dropdown menu y phải < viewcomment_icon_point y
                {
                    media_point_percentage = new
                    {
                        x = 0,
                        y = dropdown_menu.y + 6.4
                    };
                }
                else dropdown_menu = null;
            }

            if( dropdown_menu==null)
                media_point_percentage = new { x = 0, y = 15 };

            var media_height = temp_likebtn_point.y - 4 - media_point_percentage.y; //height = (vi tri button like) - (khoang cach tu button like tới mep duoi media) - (vi tri mép trên media)

            rec_media = new Class.RecPercentage(media_point_percentage, 95, media_height);
        }


        static private double PositionToViewFullPost()
        {
            double point_y_percentage_from = 12.4;
            double point_y_percentage_to = 22.9;

            double desination_point_y = (new Random().NextDouble()) * (point_y_percentage_to - point_y_percentage_from) + point_y_percentage_from;


            return desination_point_y;
        }
    }
}
