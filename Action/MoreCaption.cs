﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class MoreCaption : EngagePostAction
    {
        public MoreCaption(IGDevice iGDevice) : base(iGDevice)
        {
        }

        public MoreCaption(string deviceID) : base(deviceID)
        {
        }

        public override void DoAction()
        {
            this.UpdateEngageButtonPos(true);
            if (more_point == null)
                return;
            //KAutoHelper.ADBHelper.TapByPercent(deviceID, more_point.x, more_point.y);
            ADBSupport.ADBHelpers.RandomTapImageByPercentage(deviceID, more_point.x, more_point.y, "MoreCaption");
        }
    }
}
