﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class SwipeViewStories : EngagePostAction
    {
        string deviceID;

        public override void DoAction()
        {
            ScrollToTopStories();
            SimulateStories();
        }


        private void ScrollToTopStories()
        {
            //Go home
            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "https://instagram.com", "com.instagram.android");

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_WaitHome, 2, 5, 2);

            //Click home 2 times to scroll to top and swipe stories to first

            for (int i = 0; i < 2; i++)
            {
                ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Global_WaitHome);
                ADBSupport.ADBHelpers.Delay(1, 3);
            }
        }

        int min_time = 15;
        int max_time = 60;

        public SwipeViewStories(string deviceID) : base(deviceID)
        {
        }

        public SwipeViewStories(IGDevice iGDevice) : base(iGDevice)
        {
            this.deviceID = this.IGDevice.deviceID;
        }

        private void SimulateStories()
        {
            //Swipe

            double swipe_y = 17.4; //40 la height cua suggested element

            List<string> vectors = new List<string>() { "left", "right" };

            int swipe_left = 0;

            var choice_vector = "";

            int target_watch_time = (new Random()).Next(min_time, max_time);


            Stopwatch watch_stories = new Stopwatch();
            watch_stories.Start();

            while (true)
            {
                //Swipe
                if (watch_stories.Elapsed.TotalSeconds > target_watch_time)
                    break;

                if (swipe_left <= 1)
                {
                    choice_vector = "left";
                }
                else choice_vector = vectors[(new Random()).Next(vectors.Count)];

                switch (choice_vector)
                {
                    case "left":
                        swipe_left++;

                        //KAutoHelper.ADBHelper.SwipeByPercent(deviceID, 75, swipe_y, 23, swipe_y, 400);

                        var start_x_left = ADBSupport.ADBHelpers.RandomDoubleValue(70, 80);

                        KAutoHelper.ADBHelper.SwipeByPercent(deviceID, start_x_left, swipe_y, start_x_left - 52, swipe_y, (new Random()).Next(400, 450));

                        //Check if swipe to direct message

                        //If yes, press back and finish function

                        break;
                    case "right":
                        swipe_left--;

                        //KAutoHelper.ADBHelper.SwipeByPercent(deviceID, 23, swipe_y, 75, swipe_y, 400);

                        var start_x_right = ADBSupport.ADBHelpers.RandomDoubleValue(18, 28);

                        KAutoHelper.ADBHelper.SwipeByPercent(deviceID, start_x_right, swipe_y, start_x_right + 52, swipe_y, (new Random()).Next(400, 450));

                        //Check to see if swipe to DirectMessage
                        try
                        {
                            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.DirectMess, 1, 3, 0);
                            return;
                        }
                        catch { }

                        break;
                }
                //Thread some seconds

                ADBSupport.ADBHelpers.Delay(1, 2);

                //decide to click story or not

                //Decide to follow or not

                List<string> actions = new List<string>() { "view", "continue" };

                var choice_action = actions[(new Random()).Next(actions.Count)];

                switch (choice_action)
                {
                    case "view":
                        var screen = ADBSupport.ADBHelpers.ScreenShoot(deviceID);
                        List<Point> story_elements = new List<Point>();

                        story_elements = KAutoHelper.ImageScanOpenCV.FindOutPoints(screen, ADBSupport.BMPSource.StoryElement);
                        if (story_elements.Count == 0)
                            story_elements = KAutoHelper.ImageScanOpenCV.FindOutPoints(screen, ADBSupport.BMPSource.StoryElementViewed);

                        if (story_elements.Count == 0)
                            return;
                        //+20 ra vi trí gần giữa vòng tròn story
                        //-4 lấy vị tri gần trên vong tron
                        // => random lấy 
                        ADBSupport.ADBHelpers.RandomTapImage(deviceID, story_elements[0].X + 16, story_elements[0].Y - 20, "StoryElement");

                        EngageStory();
                        break;
                    case "continue":
                        continue;
                }

                //Hit next, hit previous, longpress
            }
        }

        private void EngageStory()
        {
            int target_engage = (new Random()).Next(3, 15);
            int swipe_right = 0;

            var choice_vector = "";

            List<string> vectors = new List<string>() { "left|70-90", "right|1-70", "hold|90-100" };

            for (int t = 0; t < target_engage; t++)
            {
                //Swipe
                if (swipe_right <= 1)
                {
                    choice_vector = "right";
                }
                else choice_vector = IGHelpers.Helpers.RandomActionByPercentage(vectors);

                switch (choice_vector)
                {
                    case "left":
                        swipe_right--;

                        KAutoHelper.ADBHelper.TapByPercent(deviceID, 3.5, 55.8);

                        break;
                    case "right":
                        swipe_right++;

                        KAutoHelper.ADBHelper.TapByPercent(deviceID, 91.4, 55.8);

                        break;
                    case "hold":
                        //Hold

                        int hold_dur = (new Random()).Next(2000, 10000);

                        KAutoHelper.ADBHelper.LongPress(deviceID, 47, 56, hold_dur);

                        break;
                }

                ADBSupport.ADBHelpers.Delay(1, 3);
            }

            //Press back
            KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_BACK);
        }
    }
}
