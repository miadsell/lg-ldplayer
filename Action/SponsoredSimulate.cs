﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class SponsoredSimulate : EngagePostAction
    {
        public SponsoredSimulate(IGDevice iGDevice) : base(iGDevice)
        {
        }

        public SponsoredSimulate(string deviceID) : base(deviceID)
        {
        }

        public override void DoAction()

        {
            int dest_y = (new Random()).Next(20, 53);
            //Scroll sponsored to X

            ADBSupport.ADBHelpers.ScrollImageToPosByPercent(deviceID, ADBSupport.BMPSource.Sponsored_SponsoredText, (double)dest_y);

            this.UpdateEngageButtonPos(true);

            var sponsored_y = this.viewcomment_icon_point.y - ((new Random()).NextDouble() * (9.3 - 2.3) + 2.3);


            var x = (new Random()).Next(6, 96);

            bool found_closex = false;
            bool found_back = false;

            //Click link
            try
            {
                KAutoHelper.ADBHelper.TapByPercent(deviceID, x, sponsored_y);

                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Sponsored_LoadWebsiteX, 2, 5, 3);
                    found_closex = true;
                }
                catch { }

                if(!found_closex)
                {
                    try
                    {
                        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Sponsored_LoadProfileBack, 1, 0, 3);
                        found_back = true;
                    }
                    catch { }

                    if (!found_back)
                        return;
                }
            }
            catch
            {
                return;
            }

            ADBSupport.ADBHelpers.Delay(5, 10);

            //Random scroll some times and delay
            int scroll_time = 0;
            while(true)
            {
                ADBSupport.ADBHelpers.RandomScrollScreen(deviceID, 1, 0,78.7,38.4,(new Random()).Next(1000,2000));
                ADBSupport.ADBHelpers.Delay(4, 10);
                scroll_time++;

                if(scroll_time>3)
                {
                    List<bool> choices = new List<bool>() { true, false };

                    var choice = choices[(new Random(Guid.NewGuid().GetHashCode())).Next(choices.Count)];

                    if (!choice)
                        break;
                }
            }

            //Click back home or click close

            List<string> backs = new List<string>() { "close btn", "press back" };

            var back_choice = backs[(new Random()).Next(backs.Count)];

            switch(back_choice)
            {
                case "close btn":

                    if(found_closex)
                        ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Sponsored_LoadWebsiteX);

                    if(found_back)
                        ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Sponsored_LoadProfileBack);

                    ADBSupport.ADBHelpers.Delay();
                    break;
                case "press back":
                    KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_BACK);
                    break;
            }

            ADBSupport.ADBHelpers.Delay();

        }
    }
}
