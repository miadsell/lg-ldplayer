﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Action
{
    class UpdateBio : IAction
    {
        public IGDevice IGDevice;

        public string biotext, deviceID;
        int max_bio;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iGDevice"></param>
        /// <param name="biotext"></param>
        /// <param name="max_bio">set 0 to no set limit</param>
        public UpdateBio(IGDevice iGDevice, string biotext, int max_bio = 150)
        {
            IGDevice = iGDevice;
            this.biotext = biotext;
            this.max_bio = max_bio;
            this.deviceID = IGDevice.deviceID;
        }

        public UpdateBio(string deviceID, string biotext, int max_bio = 150)
        {
            this.biotext = biotext;
            this.max_bio = max_bio;
            this.deviceID = deviceID;
        }

        public void DoAction()
        {
            List<string> screen_nav = new List<string>() { };

            ////Navigate to EditProfile

            //ADBSupport.IGScreen.GoToScreen(IGDevice, screen_nav);


            //COPY CONTENT

            //Push bio to sdcard

            //ADBSupport.ADBHelpers.CopyTextFromEsEditor(deviceID, bio_path);


            //UPDATE CONTENT TO BIO
            //Goto profile

            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "instagram://profile", "com.instagram.android");


            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_Goto_EditProfileScreen, 5, 5, 5);

            //Hit editprofile

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.Global_Goto_EditProfileScreen, ADBSupport.BMPSource.SwitchAccount, 1);

            //Click update bio


            var switchaccount_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.SwitchAccount);


            var bio_x_percentage = switchaccount_point.x + 4;
            var bio_y_percentage = switchaccount_point.y - 7.9;

            KAutoHelper.ADBHelper.TapByPercent(deviceID, bio_x_percentage, bio_y_percentage);

            //Wait Bio box

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.WaitBioInput);

        //Check if already has bio
        CheckBio:
            bool isemptybio = false;

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.EmptyBio, 1, 5, 0, 0.7);
                isemptybio = true;
            }
            catch
            {

            }

            if (!isemptybio)
            {
                //clear text

                while (true)
                {
                    //long press to show control
                    KAutoHelper.ADBHelper.LongPress(deviceID, 12, 67, 2000);
                    try
                    {
                        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.BioSelectAll, 1, 5, 0, 0.8);
                        break;
                    }
                    catch { }
                }


                //Click select all
                ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.BioSelectAll);

                //Send key delete

                ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell input keyevent 67"); Thread.Sleep(1000);

                goto CheckBio;
            }
            string final_biotext = "";
            var retry_bio = 0;
            while(true)
            {
                final_biotext = SharpSpin.Spinner.Spin(biotext);

                if (max_bio == 0)
                    break;

                if (final_biotext.Length <= max_bio)
                    break;
                retry_bio++;
                if (retry_bio > 5)
                    throw new System.ArgumentException("Can't generate proper bio");
            }

            //Send bio unicode base64

            ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID, IGDevice.ld_devicename, final_biotext);

            ////Longpress to show paste

            //while (true)
            //{
            //    //long press to show control
            //    KAutoHelper.ADBHelper.LongPress(deviceID, 12, 67, 1000);
            //    try
            //    {
            //        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.BioPaste, 1, 1, 0, 0.8);
            //        break;
            //    }
            //    catch { }
            //}

            ////Click paste

            //ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.BioPaste);


            //Check if Update Btn show


            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.ClickUpdateBio, 2, 5);



            System.Diagnostics.Stopwatch watch_updatebio = new System.Diagnostics.Stopwatch();
            watch_updatebio.Start();

            //Click update
            while (true)
            {
                if (watch_updatebio.Elapsed.TotalMinutes > 10)
                    throw new System.ArgumentException("Failed to update bio");
                try
                {
                    ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.ClickUpdateBio, ADBSupport.BMPSource.SwitchAccount, 1);
                    break;
                }
                catch
                {
                    //mean lot of character

                    //remove some character

                    for (int i = 0; i < 10; i++)
                    {
                        KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_DEL);
                        ADBSupport.ADBHelpers.Delay(0.3, 0.6);
                    }
                }
            }

            ADBSupport.ADBHelpers.Delay();
            //Click next

            KAutoHelper.ADBHelper.TapByPercent(deviceID, 95.8, 7.2); ADBSupport.ADBHelpers.Delay();

        }
    }
}
