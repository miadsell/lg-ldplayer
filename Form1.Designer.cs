﻿namespace InstagramLDPlayer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage_igaccounts = new System.Windows.Forms.TabPage();
            this.button_stop_autorun = new System.Windows.Forms.Button();
            this.label_autorun_counter = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label_run_xproxyaio = new System.Windows.Forms.Label();
            this.button_run_xproxy_aio = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.btn_run_xproxy_server = new System.Windows.Forms.Button();
            this.tb_log = new System.Windows.Forms.TextBox();
            this.panel_background_task = new System.Windows.Forms.Panel();
            this.btn_do_background_task = new System.Windows.Forms.Button();
            this.checkBox_scrape_slaves_comment = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.checkBox_skip_launch_error = new System.Windows.Forms.CheckBox();
            this.checkBox_keep_ldplay_open = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cb_skip_error = new System.Windows.Forms.CheckBox();
            this.cb_minimize_ldplayer = new System.Windows.Forms.CheckBox();
            this.btn_register_clone = new System.Windows.Forms.Button();
            this.main_btn_stop = new System.Windows.Forms.Button();
            this.btn_tasking = new System.Windows.Forms.Button();
            this.dataGridView_manage = new System.Windows.Forms.DataGridView();
            this.dgcol_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgcol_niche = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgcol_user = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgcol_fullname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgcol_note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgcol_state = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgcol_status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgcol_settings = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dgcol_launch = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dgcol_close = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cb_tasking = new System.Windows.Forms.ComboBox();
            this.btn_register_batch = new System.Windows.Forms.Button();
            this.cb_batch_option = new System.Windows.Forms.ComboBox();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.button_settings_removenow = new System.Windows.Forms.Button();
            this.label65 = new System.Windows.Forms.Label();
            this.textBox_settings_remove = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.rb_id = new System.Windows.Forms.RadioButton();
            this.rb_user = new System.Windows.Forms.RadioButton();
            this.label31 = new System.Windows.Forms.Label();
            this.btn_setting_json_clear_all = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.btn_settings_tab_update = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.tb_settings_json_setting = new System.Windows.Forms.TextBox();
            this.tb_settings_json_users = new System.Windows.Forms.TextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.btn_search_device = new System.Windows.Forms.Button();
            this.label_slaves_backup_status = new System.Windows.Forms.Label();
            this.dataGridView_slavesmanual = new System.Windows.Forms.DataGridView();
            this.btn_slaves_backupff_profile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cb_Has_Comment = new System.Windows.Forms.CheckBox();
            this.cb_Monitor_Message = new System.Windows.Forms.CheckBox();
            this.cb_Monitor_Comment = new System.Windows.Forms.CheckBox();
            this.cb_Has_Message = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_phonedevice = new System.Windows.Forms.TextBox();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.btn_refresh_slaves_comment = new System.Windows.Forms.Button();
            this.dataGridView_slaves_comment = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_resources_quantity = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btn_set_resource_browser = new System.Windows.Forms.Button();
            this.tb_resource_data_folder = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_set_resource = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBox_remove_old_resources = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cb_resources = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_accounts_resource = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.btn_tasks_import_addtasks = new System.Windows.Forms.Button();
            this.panel_import_from_csv = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.btn_tasks_import_browse_csv = new System.Windows.Forms.Button();
            this.tb_tasks_import_csv = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.panel_import_directly = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.tb_tasks_import_accounts = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btn_tasks_import_browse_folder = new System.Windows.Forms.Button();
            this.tb_tasks_import_folder = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.btn_tasks_import_fromcsv = new System.Windows.Forms.Button();
            this.btn_tasks_import_directly = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.checkBox_tasks_import_schedule_random_times = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.comboBox_tasks_import_task = new System.Windows.Forms.ComboBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel_posts_add = new System.Windows.Forms.Panel();
            this.btn_posts_add_images = new System.Windows.Forms.Button();
            this.btn_posts_add_addpost = new System.Windows.Forms.Button();
            this.comboBox_posts_add_state = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tb_posts_add_images = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tb_posts_add_settings = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tb_posts_add_hashtag = new System.Windows.Forms.TextBox();
            this.tb_posts_add_content = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.btn_posts_add = new System.Windows.Forms.Button();
            this.dataGridView_posts_manage = new System.Windows.Forms.DataGridView();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.label68 = new System.Windows.Forms.Label();
            this.checkBox_register_auto_generate_user = new System.Windows.Forms.CheckBox();
            this.comboBox_register_phone_services = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.cb_register_addto_account_reg = new System.Windows.Forms.CheckBox();
            this.label41 = new System.Windows.Forms.Label();
            this.btn_register_load_original_names = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.btn_register_generate_names = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.tb_register_quantity = new System.Windows.Forms.TextBox();
            this.tb_register_list_names = new System.Windows.Forms.TextBox();
            this.checkBox_register_skiperror = new System.Windows.Forms.CheckBox();
            this.btn_register_start = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.tb_register_deviceids = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.label63 = new System.Windows.Forms.Label();
            this.textBox_transferaccount_log = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.button_transferaccount_transfer_now = new System.Windows.Forms.Button();
            this.label53 = new System.Windows.Forms.Label();
            this.button_transferaccount_to_browse_data = new System.Windows.Forms.Button();
            this.textBox_transferaccount_to_folder_data = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.button_transferaccount_to_testconnection = new System.Windows.Forms.Button();
            this.textBox_transferaccount_to_database = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.textBox_transferaccount_to_server_ip = new System.Windows.Forms.TextBox();
            this.button_transferaccount_from_browse_data = new System.Windows.Forms.Button();
            this.textBox_transferaccount_from_folder_data = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.button_transferaccount_from_testconnection = new System.Windows.Forms.Button();
            this.textBox_transferaccount_from_database = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.textBox_transferaccount_from_server_ip = new System.Windows.Forms.TextBox();
            this.textBox_transferaccount_from_accountId = new System.Windows.Forms.TextBox();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.button_control_panel_clear = new System.Windows.Forms.Button();
            this.textBox_control_panel_pc_devices = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.goToSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.launchmanuallyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_Settings = new System.Windows.Forms.Button();
            this.label69 = new System.Windows.Forms.Label();
            this.label_project_name = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label_pc_devices = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage_igaccounts.SuspendLayout();
            this.panel_background_task.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_manage)).BeginInit();
            this.tabPage10.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_slavesmanual)).BeginInit();
            this.panel2.SuspendLayout();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_slaves_comment)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.panel_import_from_csv.SuspendLayout();
            this.panel_import_directly.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel_posts_add.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_posts_manage)).BeginInit();
            this.tabPage7.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage_igaccounts);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Location = new System.Drawing.Point(9, 30);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(0, 0);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1267, 534);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage_igaccounts
            // 
            this.tabPage_igaccounts.Controls.Add(this.button_stop_autorun);
            this.tabPage_igaccounts.Controls.Add(this.label_autorun_counter);
            this.tabPage_igaccounts.Controls.Add(this.label67);
            this.tabPage_igaccounts.Controls.Add(this.label_run_xproxyaio);
            this.tabPage_igaccounts.Controls.Add(this.button_run_xproxy_aio);
            this.tabPage_igaccounts.Controls.Add(this.label30);
            this.tabPage_igaccounts.Controls.Add(this.btn_run_xproxy_server);
            this.tabPage_igaccounts.Controls.Add(this.tb_log);
            this.tabPage_igaccounts.Controls.Add(this.panel_background_task);
            this.tabPage_igaccounts.Controls.Add(this.panel3);
            this.tabPage_igaccounts.Controls.Add(this.panel1);
            this.tabPage_igaccounts.Controls.Add(this.btn_register_clone);
            this.tabPage_igaccounts.Controls.Add(this.main_btn_stop);
            this.tabPage_igaccounts.Controls.Add(this.btn_tasking);
            this.tabPage_igaccounts.Controls.Add(this.dataGridView_manage);
            this.tabPage_igaccounts.Controls.Add(this.cb_tasking);
            this.tabPage_igaccounts.Controls.Add(this.btn_register_batch);
            this.tabPage_igaccounts.Controls.Add(this.cb_batch_option);
            this.tabPage_igaccounts.Location = new System.Drawing.Point(4, 22);
            this.tabPage_igaccounts.Name = "tabPage_igaccounts";
            this.tabPage_igaccounts.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_igaccounts.Size = new System.Drawing.Size(1259, 508);
            this.tabPage_igaccounts.TabIndex = 4;
            this.tabPage_igaccounts.Text = "IG Accounts";
            this.tabPage_igaccounts.UseVisualStyleBackColor = true;
            // 
            // button_stop_autorun
            // 
            this.button_stop_autorun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_stop_autorun.Location = new System.Drawing.Point(1162, 340);
            this.button_stop_autorun.Name = "button_stop_autorun";
            this.button_stop_autorun.Size = new System.Drawing.Size(91, 23);
            this.button_stop_autorun.TabIndex = 27;
            this.button_stop_autorun.Text = "Stop Auto Run";
            this.button_stop_autorun.UseVisualStyleBackColor = true;
            this.button_stop_autorun.Click += new System.EventHandler(this.button_stop_autorun_Click);
            // 
            // label_autorun_counter
            // 
            this.label_autorun_counter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_autorun_counter.AutoSize = true;
            this.label_autorun_counter.ForeColor = System.Drawing.Color.Red;
            this.label_autorun_counter.Location = new System.Drawing.Point(1133, 345);
            this.label_autorun_counter.Name = "label_autorun_counter";
            this.label_autorun_counter.Size = new System.Drawing.Size(13, 13);
            this.label_autorun_counter.TabIndex = 26;
            this.label_autorun_counter.Text = "0";
            // 
            // label67
            // 
            this.label67.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(1042, 345);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(85, 13);
            this.label67.TabIndex = 25;
            this.label67.Text = "Auto Run within:";
            // 
            // label_run_xproxyaio
            // 
            this.label_run_xproxyaio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_run_xproxyaio.AutoSize = true;
            this.label_run_xproxyaio.Location = new System.Drawing.Point(990, 345);
            this.label_run_xproxyaio.Name = "label_run_xproxyaio";
            this.label_run_xproxyaio.Size = new System.Drawing.Size(29, 13);
            this.label_run_xproxyaio.TabIndex = 24;
            this.label_run_xproxyaio.Text = "Stop";
            // 
            // button_run_xproxy_aio
            // 
            this.button_run_xproxy_aio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_run_xproxy_aio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_run_xproxy_aio.Location = new System.Drawing.Point(861, 322);
            this.button_run_xproxy_aio.Name = "button_run_xproxy_aio";
            this.button_run_xproxy_aio.Size = new System.Drawing.Size(106, 59);
            this.button_run_xproxy_aio.TabIndex = 23;
            this.button_run_xproxy_aio.Text = "Run XProxy AIO";
            this.button_run_xproxy_aio.UseVisualStyleBackColor = true;
            this.button_run_xproxy_aio.Click += new System.EventHandler(this.button_run_xproxy_aio_Click);
            // 
            // label30
            // 
            this.label30.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(973, 252);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(255, 13);
            this.label30.TabIndex = 22;
            this.label30.Text = "use when you want to manage multi device manually";
            // 
            // btn_run_xproxy_server
            // 
            this.btn_run_xproxy_server.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_run_xproxy_server.Location = new System.Drawing.Point(861, 247);
            this.btn_run_xproxy_server.Name = "btn_run_xproxy_server";
            this.btn_run_xproxy_server.Size = new System.Drawing.Size(106, 23);
            this.btn_run_xproxy_server.TabIndex = 21;
            this.btn_run_xproxy_server.Text = "Run XProxy Server";
            this.btn_run_xproxy_server.UseVisualStyleBackColor = true;
            this.btn_run_xproxy_server.Click += new System.EventHandler(this.btn_run_xproxy_server_Click);
            // 
            // tb_log
            // 
            this.tb_log.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_log.Location = new System.Drawing.Point(861, 3);
            this.tb_log.Multiline = true;
            this.tb_log.Name = "tb_log";
            this.tb_log.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_log.Size = new System.Drawing.Size(392, 238);
            this.tb_log.TabIndex = 20;
            // 
            // panel_background_task
            // 
            this.panel_background_task.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_background_task.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_background_task.Controls.Add(this.btn_do_background_task);
            this.panel_background_task.Controls.Add(this.checkBox_scrape_slaves_comment);
            this.panel_background_task.Controls.Add(this.label2);
            this.panel_background_task.Location = new System.Drawing.Point(130, 402);
            this.panel_background_task.Name = "panel_background_task";
            this.panel_background_task.Size = new System.Drawing.Size(296, 100);
            this.panel_background_task.TabIndex = 18;
            // 
            // btn_do_background_task
            // 
            this.btn_do_background_task.Location = new System.Drawing.Point(216, 3);
            this.btn_do_background_task.Name = "btn_do_background_task";
            this.btn_do_background_task.Size = new System.Drawing.Size(75, 92);
            this.btn_do_background_task.TabIndex = 2;
            this.btn_do_background_task.Text = "Do Background Task";
            this.btn_do_background_task.UseVisualStyleBackColor = true;
            this.btn_do_background_task.Click += new System.EventHandler(this.btn_do_background_task_Click);
            // 
            // checkBox_scrape_slaves_comment
            // 
            this.checkBox_scrape_slaves_comment.AutoSize = true;
            this.checkBox_scrape_slaves_comment.Checked = true;
            this.checkBox_scrape_slaves_comment.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_scrape_slaves_comment.Location = new System.Drawing.Point(6, 22);
            this.checkBox_scrape_slaves_comment.Name = "checkBox_scrape_slaves_comment";
            this.checkBox_scrape_slaves_comment.Size = new System.Drawing.Size(142, 17);
            this.checkBox_scrape_slaves_comment.TabIndex = 1;
            this.checkBox_scrape_slaves_comment.Text = "Scrape Slaves Comment";
            this.checkBox_scrape_slaves_comment.UseVisualStyleBackColor = true;
            this.checkBox_scrape_slaves_comment.CheckedChanged += new System.EventHandler(this.checkBox_scrape_slaves_comment_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Background Task";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.checkBox_skip_launch_error);
            this.panel3.Controls.Add(this.checkBox_keep_ldplay_open);
            this.panel3.Controls.Add(this.label29);
            this.panel3.Location = new System.Drawing.Point(432, 402);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(217, 100);
            this.panel3.TabIndex = 17;
            // 
            // checkBox_skip_launch_error
            // 
            this.checkBox_skip_launch_error.AutoSize = true;
            this.checkBox_skip_launch_error.Checked = true;
            this.checkBox_skip_launch_error.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_skip_launch_error.Location = new System.Drawing.Point(6, 45);
            this.checkBox_skip_launch_error.Name = "checkBox_skip_launch_error";
            this.checkBox_skip_launch_error.Size = new System.Drawing.Size(199, 17);
            this.checkBox_skip_launch_error.TabIndex = 2;
            this.checkBox_skip_launch_error.Text = "Skip Launch Error (use when go out)";
            this.checkBox_skip_launch_error.UseVisualStyleBackColor = true;
            this.checkBox_skip_launch_error.CheckedChanged += new System.EventHandler(this.checkBox_skip_launch_error_CheckedChanged);
            // 
            // checkBox_keep_ldplay_open
            // 
            this.checkBox_keep_ldplay_open.AutoSize = true;
            this.checkBox_keep_ldplay_open.Location = new System.Drawing.Point(6, 20);
            this.checkBox_keep_ldplay_open.Name = "checkBox_keep_ldplay_open";
            this.checkBox_keep_ldplay_open.Size = new System.Drawing.Size(154, 17);
            this.checkBox_keep_ldplay_open.TabIndex = 1;
            this.checkBox_keep_ldplay_open.Text = "Keep LD open, stop thread";
            this.checkBox_keep_ldplay_open.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(3, 4);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(56, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "Developer";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.cb_skip_error);
            this.panel1.Controls.Add(this.cb_minimize_ldplayer);
            this.panel1.Location = new System.Drawing.Point(655, 402);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 100);
            this.panel1.TabIndex = 16;
            // 
            // cb_skip_error
            // 
            this.cb_skip_error.AutoSize = true;
            this.cb_skip_error.Checked = true;
            this.cb_skip_error.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_skip_error.Location = new System.Drawing.Point(3, 8);
            this.cb_skip_error.Name = "cb_skip_error";
            this.cb_skip_error.Size = new System.Drawing.Size(178, 17);
            this.cb_skip_error.TabIndex = 1;
            this.cb_skip_error.Text = "Skip Error (log error to database)";
            this.cb_skip_error.UseVisualStyleBackColor = true;
            this.cb_skip_error.CheckedChanged += new System.EventHandler(this.cb_skip_error_CheckedChanged);
            // 
            // cb_minimize_ldplayer
            // 
            this.cb_minimize_ldplayer.AutoSize = true;
            this.cb_minimize_ldplayer.Location = new System.Drawing.Point(3, 31);
            this.cb_minimize_ldplayer.Name = "cb_minimize_ldplayer";
            this.cb_minimize_ldplayer.Size = new System.Drawing.Size(112, 17);
            this.cb_minimize_ldplayer.TabIndex = 0;
            this.cb_minimize_ldplayer.Text = "Minimize LDPlayer";
            this.cb_minimize_ldplayer.UseVisualStyleBackColor = true;
            // 
            // btn_register_clone
            // 
            this.btn_register_clone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_register_clone.Location = new System.Drawing.Point(6, 406);
            this.btn_register_clone.Name = "btn_register_clone";
            this.btn_register_clone.Size = new System.Drawing.Size(75, 23);
            this.btn_register_clone.TabIndex = 1;
            this.btn_register_clone.Text = "Clone";
            this.btn_register_clone.UseVisualStyleBackColor = true;
            this.btn_register_clone.Click += new System.EventHandler(this.btn_register_clone_Click);
            // 
            // main_btn_stop
            // 
            this.main_btn_stop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.main_btn_stop.Location = new System.Drawing.Point(877, 402);
            this.main_btn_stop.Name = "main_btn_stop";
            this.main_btn_stop.Size = new System.Drawing.Size(75, 100);
            this.main_btn_stop.TabIndex = 6;
            this.main_btn_stop.Text = "Stop ALL";
            this.main_btn_stop.UseVisualStyleBackColor = true;
            this.main_btn_stop.Click += new System.EventHandler(this.main_btn_stop_Click);
            // 
            // btn_tasking
            // 
            this.btn_tasking.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_tasking.Location = new System.Drawing.Point(1162, 447);
            this.btn_tasking.Name = "btn_tasking";
            this.btn_tasking.Size = new System.Drawing.Size(91, 23);
            this.btn_tasking.TabIndex = 15;
            this.btn_tasking.Text = "Tasking";
            this.btn_tasking.UseVisualStyleBackColor = true;
            this.btn_tasking.Click += new System.EventHandler(this.btn_tasking_Click);
            // 
            // dataGridView_manage
            // 
            this.dataGridView_manage.AllowUserToAddRows = false;
            this.dataGridView_manage.AllowUserToDeleteRows = false;
            this.dataGridView_manage.AllowUserToOrderColumns = true;
            this.dataGridView_manage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_manage.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_manage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_manage.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgcol_id,
            this.dgcol_niche,
            this.dgcol_user,
            this.dgcol_fullname,
            this.dgcol_note,
            this.dgcol_state,
            this.dgcol_status,
            this.dgcol_settings,
            this.dgcol_launch,
            this.dgcol_close});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_manage.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_manage.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_manage.Name = "dataGridView_manage";
            this.dataGridView_manage.ReadOnly = true;
            this.dataGridView_manage.Size = new System.Drawing.Size(852, 378);
            this.dataGridView_manage.TabIndex = 0;
            this.dataGridView_manage.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_manage_CellContentClick);
            // 
            // dgcol_id
            // 
            this.dgcol_id.DataPropertyName = "dgcol_id";
            this.dgcol_id.Frozen = true;
            this.dgcol_id.HeaderText = "ID";
            this.dgcol_id.Name = "dgcol_id";
            this.dgcol_id.ReadOnly = true;
            // 
            // dgcol_niche
            // 
            this.dgcol_niche.DataPropertyName = "dgcol_niche";
            this.dgcol_niche.Frozen = true;
            this.dgcol_niche.HeaderText = "Niche";
            this.dgcol_niche.Name = "dgcol_niche";
            this.dgcol_niche.ReadOnly = true;
            // 
            // dgcol_user
            // 
            this.dgcol_user.DataPropertyName = "dgcol_user";
            this.dgcol_user.Frozen = true;
            this.dgcol_user.HeaderText = "User";
            this.dgcol_user.Name = "dgcol_user";
            this.dgcol_user.ReadOnly = true;
            // 
            // dgcol_fullname
            // 
            this.dgcol_fullname.DataPropertyName = "dgcol_fullname";
            this.dgcol_fullname.Frozen = true;
            this.dgcol_fullname.HeaderText = "Fullname";
            this.dgcol_fullname.Name = "dgcol_fullname";
            this.dgcol_fullname.ReadOnly = true;
            // 
            // dgcol_note
            // 
            this.dgcol_note.DataPropertyName = "dgcol_note";
            this.dgcol_note.Frozen = true;
            this.dgcol_note.HeaderText = "Note";
            this.dgcol_note.Name = "dgcol_note";
            this.dgcol_note.ReadOnly = true;
            // 
            // dgcol_state
            // 
            this.dgcol_state.DataPropertyName = "dgcol_state";
            this.dgcol_state.Frozen = true;
            this.dgcol_state.HeaderText = "State";
            this.dgcol_state.Name = "dgcol_state";
            this.dgcol_state.ReadOnly = true;
            // 
            // dgcol_status
            // 
            this.dgcol_status.DataPropertyName = "dgcol_status";
            this.dgcol_status.Frozen = true;
            this.dgcol_status.HeaderText = "Status";
            this.dgcol_status.Name = "dgcol_status";
            this.dgcol_status.ReadOnly = true;
            // 
            // dgcol_settings
            // 
            this.dgcol_settings.DataPropertyName = "dgcol_settings";
            this.dgcol_settings.HeaderText = "Settings";
            this.dgcol_settings.Name = "dgcol_settings";
            this.dgcol_settings.ReadOnly = true;
            // 
            // dgcol_launch
            // 
            this.dgcol_launch.DataPropertyName = "dgcol_launch";
            this.dgcol_launch.HeaderText = "Launch";
            this.dgcol_launch.Name = "dgcol_launch";
            this.dgcol_launch.ReadOnly = true;
            // 
            // dgcol_close
            // 
            this.dgcol_close.DataPropertyName = "dgcol_close";
            this.dgcol_close.HeaderText = "Close";
            this.dgcol_close.Name = "dgcol_close";
            this.dgcol_close.ReadOnly = true;
            // 
            // cb_tasking
            // 
            this.cb_tasking.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cb_tasking.FormattingEnabled = true;
            this.cb_tasking.Items.AddRange(new object[] {
            "All In One",
            "Optimize Backup",
            "Assign Account",
            "Login New Account",
            "Clear Status All Devices"});
            this.cb_tasking.Location = new System.Drawing.Point(1006, 449);
            this.cb_tasking.Name = "cb_tasking";
            this.cb_tasking.Size = new System.Drawing.Size(121, 21);
            this.cb_tasking.TabIndex = 14;
            this.cb_tasking.Text = "All In One";
            // 
            // btn_register_batch
            // 
            this.btn_register_batch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_register_batch.Location = new System.Drawing.Point(1162, 406);
            this.btn_register_batch.Name = "btn_register_batch";
            this.btn_register_batch.Size = new System.Drawing.Size(91, 23);
            this.btn_register_batch.TabIndex = 5;
            this.btn_register_batch.Text = "Batch";
            this.btn_register_batch.UseVisualStyleBackColor = true;
            this.btn_register_batch.Click += new System.EventHandler(this.btn_register_batch_Click);
            // 
            // cb_batch_option
            // 
            this.cb_batch_option.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cb_batch_option.FormattingEnabled = true;
            this.cb_batch_option.Items.AddRange(new object[] {
            "UPDATE",
            "DELETE"});
            this.cb_batch_option.Location = new System.Drawing.Point(1006, 408);
            this.cb_batch_option.Name = "cb_batch_option";
            this.cb_batch_option.Size = new System.Drawing.Size(121, 21);
            this.cb_batch_option.TabIndex = 4;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.button_settings_removenow);
            this.tabPage10.Controls.Add(this.label65);
            this.tabPage10.Controls.Add(this.textBox_settings_remove);
            this.tabPage10.Controls.Add(this.label64);
            this.tabPage10.Controls.Add(this.rb_id);
            this.tabPage10.Controls.Add(this.rb_user);
            this.tabPage10.Controls.Add(this.label31);
            this.tabPage10.Controls.Add(this.btn_setting_json_clear_all);
            this.tabPage10.Controls.Add(this.label37);
            this.tabPage10.Controls.Add(this.label36);
            this.tabPage10.Controls.Add(this.label35);
            this.tabPage10.Controls.Add(this.label34);
            this.tabPage10.Controls.Add(this.btn_settings_tab_update);
            this.tabPage10.Controls.Add(this.label33);
            this.tabPage10.Controls.Add(this.label32);
            this.tabPage10.Controls.Add(this.tb_settings_json_setting);
            this.tabPage10.Controls.Add(this.tb_settings_json_users);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(1259, 508);
            this.tabPage10.TabIndex = 10;
            this.tabPage10.Text = "Settings (JSON)";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // button_settings_removenow
            // 
            this.button_settings_removenow.Location = new System.Drawing.Point(1221, 436);
            this.button_settings_removenow.Name = "button_settings_removenow";
            this.button_settings_removenow.Size = new System.Drawing.Size(122, 23);
            this.button_settings_removenow.TabIndex = 16;
            this.button_settings_removenow.Text = "REMOVE NOW";
            this.button_settings_removenow.UseVisualStyleBackColor = true;
            this.button_settings_removenow.Click += new System.EventHandler(this.button_settings_removenow_Click);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.ForeColor = System.Drawing.Color.Red;
            this.label65.Location = new System.Drawing.Point(870, 66);
            this.label65.MaximumSize = new System.Drawing.Size(500, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(473, 26);
            this.label65.TabIndex = 15;
            this.label65.Text = "This features is dangerous, make sure you know what are you doing? Normally, it\'s" +
    " used when you change structure of specific settings so you want to clear and im" +
    "plement new structure";
            // 
            // textBox_settings_remove
            // 
            this.textBox_settings_remove.Location = new System.Drawing.Point(872, 104);
            this.textBox_settings_remove.Multiline = true;
            this.textBox_settings_remove.Name = "textBox_settings_remove";
            this.textBox_settings_remove.Size = new System.Drawing.Size(471, 326);
            this.textBox_settings_remove.TabIndex = 14;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(870, 38);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(236, 13);
            this.label64.TabIndex = 13;
            this.label64.Text = "Remove these settings (path to remove property)";
            // 
            // rb_id
            // 
            this.rb_id.AutoSize = true;
            this.rb_id.Location = new System.Drawing.Point(229, 36);
            this.rb_id.Name = "rb_id";
            this.rb_id.Size = new System.Drawing.Size(33, 17);
            this.rb_id.TabIndex = 12;
            this.rb_id.Text = "id";
            this.rb_id.UseVisualStyleBackColor = true;
            // 
            // rb_user
            // 
            this.rb_user.AutoSize = true;
            this.rb_user.Checked = true;
            this.rb_user.Location = new System.Drawing.Point(178, 36);
            this.rb_user.Name = "rb_user";
            this.rb_user.Size = new System.Drawing.Size(45, 17);
            this.rb_user.TabIndex = 11;
            this.rb_user.TabStop = true;
            this.rb_user.Text = "user";
            this.rb_user.UseVisualStyleBackColor = true;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(111, 38);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(61, 13);
            this.label31.TabIndex = 10;
            this.label31.Text = "Input Type:";
            // 
            // btn_setting_json_clear_all
            // 
            this.btn_setting_json_clear_all.Location = new System.Drawing.Point(185, 436);
            this.btn_setting_json_clear_all.Name = "btn_setting_json_clear_all";
            this.btn_setting_json_clear_all.Size = new System.Drawing.Size(183, 23);
            this.btn_setting_json_clear_all.TabIndex = 9;
            this.btn_setting_json_clear_all.Text = "Clear All";
            this.btn_setting_json_clear_all.UseVisualStyleBackColor = true;
            this.btn_setting_json_clear_all.Click += new System.EventHandler(this.btn_setting_json_clear_all_Click);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(35, 547);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(327, 13);
            this.label37.TabIndex = 8;
            this.label37.Text = "{\"DoNormalUser_Publish\":{\"additional_comment\":{\"turn_on\":true}}}";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(35, 521);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(1015, 13);
            this.label36.TabIndex = 7;
            this.label36.Text = resources.GetString("label36.Text");
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(35, 498);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(252, 13);
            this.label35.TabIndex = 6;
            this.label35.Text = "Json Settings: list json point to property need update";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 474);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(333, 13);
            this.label34.TabIndex = 5;
            this.label34.Text = "Note: Usernames: list username need update json setting (line by line)";
            // 
            // btn_settings_tab_update
            // 
            this.btn_settings_tab_update.Location = new System.Drawing.Point(6, 436);
            this.btn_settings_tab_update.Name = "btn_settings_tab_update";
            this.btn_settings_tab_update.Size = new System.Drawing.Size(160, 23);
            this.btn_settings_tab_update.TabIndex = 4;
            this.btn_settings_tab_update.Text = "Update Settings";
            this.btn_settings_tab_update.UseVisualStyleBackColor = true;
            this.btn_settings_tab_update.Click += new System.EventHandler(this.btn_settings_tab_update_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(371, 38);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(70, 13);
            this.label33.TabIndex = 3;
            this.label33.Text = "Json Settings";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 38);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(60, 13);
            this.label32.TabIndex = 2;
            this.label32.Text = "Usernames";
            // 
            // tb_settings_json_setting
            // 
            this.tb_settings_json_setting.Location = new System.Drawing.Point(374, 66);
            this.tb_settings_json_setting.Multiline = true;
            this.tb_settings_json_setting.Name = "tb_settings_json_setting";
            this.tb_settings_json_setting.Size = new System.Drawing.Size(481, 364);
            this.tb_settings_json_setting.TabIndex = 1;
            // 
            // tb_settings_json_users
            // 
            this.tb_settings_json_users.Location = new System.Drawing.Point(6, 66);
            this.tb_settings_json_users.Multiline = true;
            this.tb_settings_json_users.Name = "tb_settings_json_users";
            this.tb_settings_json_users.Size = new System.Drawing.Size(362, 364);
            this.tb_settings_json_users.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tabControl4);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1259, 508);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "Slaves Manual";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabControl4
            // 
            this.tabControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl4.Controls.Add(this.tabPage8);
            this.tabControl4.Controls.Add(this.tabPage9);
            this.tabControl4.Location = new System.Drawing.Point(6, 6);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(1338, 551);
            this.tabControl4.TabIndex = 10;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.btn_search_device);
            this.tabPage8.Controls.Add(this.label_slaves_backup_status);
            this.tabPage8.Controls.Add(this.dataGridView_slavesmanual);
            this.tabPage8.Controls.Add(this.btn_slaves_backupff_profile);
            this.tabPage8.Controls.Add(this.label1);
            this.tabPage8.Controls.Add(this.panel2);
            this.tabPage8.Controls.Add(this.tb_phonedevice);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(1330, 525);
            this.tabPage8.TabIndex = 0;
            this.tabPage8.Text = "Messages";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // btn_search_device
            // 
            this.btn_search_device.Location = new System.Drawing.Point(170, 6);
            this.btn_search_device.Name = "btn_search_device";
            this.btn_search_device.Size = new System.Drawing.Size(75, 23);
            this.btn_search_device.TabIndex = 3;
            this.btn_search_device.Text = "Search";
            this.btn_search_device.UseVisualStyleBackColor = true;
            this.btn_search_device.Click += new System.EventHandler(this.btn_search_device_Click);
            // 
            // label_slaves_backup_status
            // 
            this.label_slaves_backup_status.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_slaves_backup_status.AutoSize = true;
            this.label_slaves_backup_status.Location = new System.Drawing.Point(1106, 11);
            this.label_slaves_backup_status.Name = "label_slaves_backup_status";
            this.label_slaves_backup_status.Size = new System.Drawing.Size(54, 13);
            this.label_slaves_backup_status.TabIndex = 9;
            this.label_slaves_backup_status.Text = "No Status";
            // 
            // dataGridView_slavesmanual
            // 
            this.dataGridView_slavesmanual.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_slavesmanual.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_slavesmanual.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_slavesmanual.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView_slavesmanual.Location = new System.Drawing.Point(6, 126);
            this.dataGridView_slavesmanual.Name = "dataGridView_slavesmanual";
            this.dataGridView_slavesmanual.Size = new System.Drawing.Size(1318, 393);
            this.dataGridView_slavesmanual.TabIndex = 0;
            this.dataGridView_slavesmanual.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_slavesmanual_CellContentClick);
            this.dataGridView_slavesmanual.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_slavesmanual_CellValueChanged);
            // 
            // btn_slaves_backupff_profile
            // 
            this.btn_slaves_backupff_profile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_slaves_backupff_profile.BackColor = System.Drawing.Color.GreenYellow;
            this.btn_slaves_backupff_profile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_slaves_backupff_profile.Location = new System.Drawing.Point(1186, 6);
            this.btn_slaves_backupff_profile.Name = "btn_slaves_backupff_profile";
            this.btn_slaves_backupff_profile.Size = new System.Drawing.Size(138, 23);
            this.btn_slaves_backupff_profile.TabIndex = 8;
            this.btn_slaves_backupff_profile.Text = "Backup";
            this.btn_slaves_backupff_profile.UseVisualStyleBackColor = false;
            this.btn_slaves_backupff_profile.Click += new System.EventHandler(this.btn_slaves_backupff_profile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Device";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.cb_Has_Comment);
            this.panel2.Controls.Add(this.cb_Monitor_Message);
            this.panel2.Controls.Add(this.cb_Monitor_Comment);
            this.panel2.Controls.Add(this.cb_Has_Message);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(5, 35);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1319, 85);
            this.panel2.TabIndex = 7;
            // 
            // cb_Has_Comment
            // 
            this.cb_Has_Comment.AutoSize = true;
            this.cb_Has_Comment.Location = new System.Drawing.Point(250, 9);
            this.cb_Has_Comment.Name = "cb_Has_Comment";
            this.cb_Has_Comment.Size = new System.Drawing.Size(92, 17);
            this.cb_Has_Comment.TabIndex = 4;
            this.cb_Has_Comment.Text = "Has Comment";
            this.cb_Has_Comment.UseVisualStyleBackColor = true;
            this.cb_Has_Comment.CheckedChanged += new System.EventHandler(this.cb_Has_Comment_CheckedChanged);
            // 
            // cb_Monitor_Message
            // 
            this.cb_Monitor_Message.AutoSize = true;
            this.cb_Monitor_Message.Location = new System.Drawing.Point(94, 53);
            this.cb_Monitor_Message.Name = "cb_Monitor_Message";
            this.cb_Monitor_Message.Size = new System.Drawing.Size(107, 17);
            this.cb_Monitor_Message.TabIndex = 3;
            this.cb_Monitor_Message.Text = "Monitor Message";
            this.cb_Monitor_Message.UseVisualStyleBackColor = true;
            this.cb_Monitor_Message.CheckedChanged += new System.EventHandler(this.cb_Monitor_Message_CheckedChanged);
            // 
            // cb_Monitor_Comment
            // 
            this.cb_Monitor_Comment.AutoSize = true;
            this.cb_Monitor_Comment.Location = new System.Drawing.Point(250, 53);
            this.cb_Monitor_Comment.Name = "cb_Monitor_Comment";
            this.cb_Monitor_Comment.Size = new System.Drawing.Size(108, 17);
            this.cb_Monitor_Comment.TabIndex = 2;
            this.cb_Monitor_Comment.Text = "Monitor Comment";
            this.cb_Monitor_Comment.UseVisualStyleBackColor = true;
            this.cb_Monitor_Comment.CheckedChanged += new System.EventHandler(this.cb_Monitor_Comment_CheckedChanged);
            // 
            // cb_Has_Message
            // 
            this.cb_Has_Message.AutoSize = true;
            this.cb_Has_Message.Location = new System.Drawing.Point(94, 9);
            this.cb_Has_Message.Name = "cb_Has_Message";
            this.cb_Has_Message.Size = new System.Drawing.Size(91, 17);
            this.cb_Has_Message.TabIndex = 1;
            this.cb_Has_Message.Text = "Has Message";
            this.cb_Has_Message.UseVisualStyleBackColor = true;
            this.cb_Has_Message.CheckedChanged += new System.EventHandler(this.cb_Has_Message_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 26);
            this.label3.TabIndex = 0;
            this.label3.Text = "Filter";
            // 
            // tb_phonedevice
            // 
            this.tb_phonedevice.Location = new System.Drawing.Point(52, 8);
            this.tb_phonedevice.Name = "tb_phonedevice";
            this.tb_phonedevice.Size = new System.Drawing.Size(100, 20);
            this.tb_phonedevice.TabIndex = 2;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.btn_refresh_slaves_comment);
            this.tabPage9.Controls.Add(this.dataGridView_slaves_comment);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(1330, 525);
            this.tabPage9.TabIndex = 1;
            this.tabPage9.Text = "Comments";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // btn_refresh_slaves_comment
            // 
            this.btn_refresh_slaves_comment.Location = new System.Drawing.Point(6, 10);
            this.btn_refresh_slaves_comment.Name = "btn_refresh_slaves_comment";
            this.btn_refresh_slaves_comment.Size = new System.Drawing.Size(75, 23);
            this.btn_refresh_slaves_comment.TabIndex = 1;
            this.btn_refresh_slaves_comment.Text = "Refresh";
            this.btn_refresh_slaves_comment.UseVisualStyleBackColor = true;
            this.btn_refresh_slaves_comment.Click += new System.EventHandler(this.btn_refresh_slaves_comment_Click);
            // 
            // dataGridView_slaves_comment
            // 
            this.dataGridView_slaves_comment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_slaves_comment.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView_slaves_comment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_slaves_comment.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView_slaves_comment.Location = new System.Drawing.Point(6, 39);
            this.dataGridView_slaves_comment.Name = "dataGridView_slaves_comment";
            this.dataGridView_slaves_comment.Size = new System.Drawing.Size(1318, 480);
            this.dataGridView_slaves_comment.TabIndex = 0;
            this.dataGridView_slaves_comment.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_slaves_comment_CellContentClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tabControl2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1259, 508);
            this.tabPage2.TabIndex = 6;
            this.tabPage2.Text = "Manage Resources";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Location = new System.Drawing.Point(6, 6);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(973, 551);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.tb_resources_quantity);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.btn_set_resource_browser);
            this.tabPage3.Controls.Add(this.tb_resource_data_folder);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.btn_set_resource);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.checkBox_remove_old_resources);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.cb_resources);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.tb_accounts_resource);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(965, 525);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Set Manually";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(200, 143);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(175, 13);
            this.label12.TabIndex = 15;
            this.label12.Text = "**choose random [x] files from folder";
            // 
            // tb_resources_quantity
            // 
            this.tb_resources_quantity.Location = new System.Drawing.Point(64, 140);
            this.tb_resources_quantity.Name = "tb_resources_quantity";
            this.tb_resources_quantity.Size = new System.Drawing.Size(130, 20);
            this.tb_resources_quantity.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 143);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "Quantity";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(423, 106);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(252, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "**folder contains files that you want to add to source";
            // 
            // btn_set_resource_browser
            // 
            this.btn_set_resource_browser.Location = new System.Drawing.Point(326, 101);
            this.btn_set_resource_browser.Name = "btn_set_resource_browser";
            this.btn_set_resource_browser.Size = new System.Drawing.Size(75, 23);
            this.btn_set_resource_browser.TabIndex = 11;
            this.btn_set_resource_browser.Text = "Browser";
            this.btn_set_resource_browser.UseVisualStyleBackColor = true;
            this.btn_set_resource_browser.Click += new System.EventHandler(this.btn_set_resource_browser_Click);
            // 
            // tb_resource_data_folder
            // 
            this.tb_resource_data_folder.Location = new System.Drawing.Point(64, 103);
            this.tb_resource_data_folder.Name = "tb_resource_data_folder";
            this.tb_resource_data_folder.Size = new System.Drawing.Size(240, 20);
            this.tb_resource_data_folder.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 106);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Folder";
            // 
            // btn_set_resource
            // 
            this.btn_set_resource.Location = new System.Drawing.Point(64, 205);
            this.btn_set_resource.Name = "btn_set_resource";
            this.btn_set_resource.Size = new System.Drawing.Size(75, 23);
            this.btn_set_resource.TabIndex = 8;
            this.btn_set_resource.Text = "Set";
            this.btn_set_resource.UseVisualStyleBackColor = true;
            this.btn_set_resource.Click += new System.EventHandler(this.btn_set_resource_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(200, 176);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(234, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "**remove old sources or use it together with new";
            // 
            // checkBox_remove_old_resources
            // 
            this.checkBox_remove_old_resources.AutoSize = true;
            this.checkBox_remove_old_resources.Location = new System.Drawing.Point(62, 175);
            this.checkBox_remove_old_resources.Name = "checkBox_remove_old_resources";
            this.checkBox_remove_old_resources.Size = new System.Drawing.Size(118, 17);
            this.checkBox_remove_old_resources.TabIndex = 6;
            this.checkBox_remove_old_resources.Text = "remove old sources";
            this.checkBox_remove_old_resources.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(247, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "**source that you want to change";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Source";
            // 
            // cb_resources
            // 
            this.cb_resources.FormattingEnabled = true;
            this.cb_resources.Items.AddRange(new object[] {
            "Default Reply",
            "Default Post Text",
            "Default Watermark Text"});
            this.cb_resources.Location = new System.Drawing.Point(64, 62);
            this.cb_resources.Name = "cb_resources";
            this.cb_resources.Size = new System.Drawing.Size(168, 21);
            this.cb_resources.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(323, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(169, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "**list accounts separate by comma";
            // 
            // tb_accounts_resource
            // 
            this.tb_accounts_resource.Location = new System.Drawing.Point(64, 25);
            this.tb_accounts_resource.Name = "tb_accounts_resource";
            this.tb_accounts_resource.Size = new System.Drawing.Size(240, 20);
            this.tb_accounts_resource.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Accounts";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.tabControl3);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1259, 508);
            this.tabPage5.TabIndex = 7;
            this.tabPage5.Text = "Tasks";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl3.Controls.Add(this.tabPage6);
            this.tabControl3.Location = new System.Drawing.Point(6, 6);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(973, 551);
            this.tabControl3.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.btn_tasks_import_addtasks);
            this.tabPage6.Controls.Add(this.panel_import_from_csv);
            this.tabPage6.Controls.Add(this.panel_import_directly);
            this.tabPage6.Controls.Add(this.btn_tasks_import_fromcsv);
            this.tabPage6.Controls.Add(this.btn_tasks_import_directly);
            this.tabPage6.Controls.Add(this.label14);
            this.tabPage6.Controls.Add(this.checkBox_tasks_import_schedule_random_times);
            this.tabPage6.Controls.Add(this.label13);
            this.tabPage6.Controls.Add(this.comboBox_tasks_import_task);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(965, 525);
            this.tabPage6.TabIndex = 0;
            this.tabPage6.Text = "Import Tasks";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // btn_tasks_import_addtasks
            // 
            this.btn_tasks_import_addtasks.Location = new System.Drawing.Point(43, 274);
            this.btn_tasks_import_addtasks.Name = "btn_tasks_import_addtasks";
            this.btn_tasks_import_addtasks.Size = new System.Drawing.Size(75, 23);
            this.btn_tasks_import_addtasks.TabIndex = 8;
            this.btn_tasks_import_addtasks.Text = "Add Tasks";
            this.btn_tasks_import_addtasks.UseVisualStyleBackColor = true;
            this.btn_tasks_import_addtasks.Click += new System.EventHandler(this.btn_tasks_import_addtasks_Click);
            // 
            // panel_import_from_csv
            // 
            this.panel_import_from_csv.Controls.Add(this.label20);
            this.panel_import_from_csv.Controls.Add(this.btn_tasks_import_browse_csv);
            this.panel_import_from_csv.Controls.Add(this.tb_tasks_import_csv);
            this.panel_import_from_csv.Controls.Add(this.label19);
            this.panel_import_from_csv.Location = new System.Drawing.Point(43, 303);
            this.panel_import_from_csv.Name = "panel_import_from_csv";
            this.panel_import_from_csv.Size = new System.Drawing.Size(740, 111);
            this.panel_import_from_csv.TabIndex = 7;
            this.panel_import_from_csv.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(75, 63);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(199, 13);
            this.label20.TabIndex = 12;
            this.label20.Text = "format: accountId|value per line in .txt file";
            // 
            // btn_tasks_import_browse_csv
            // 
            this.btn_tasks_import_browse_csv.Location = new System.Drawing.Point(351, 10);
            this.btn_tasks_import_browse_csv.Name = "btn_tasks_import_browse_csv";
            this.btn_tasks_import_browse_csv.Size = new System.Drawing.Size(95, 23);
            this.btn_tasks_import_browse_csv.TabIndex = 12;
            this.btn_tasks_import_browse_csv.Text = "Browse";
            this.btn_tasks_import_browse_csv.UseVisualStyleBackColor = true;
            this.btn_tasks_import_browse_csv.Click += new System.EventHandler(this.btn_tasks_import_browse_csv_Click);
            // 
            // tb_tasks_import_csv
            // 
            this.tb_tasks_import_csv.Location = new System.Drawing.Point(78, 12);
            this.tb_tasks_import_csv.Name = "tb_tasks_import_csv";
            this.tb_tasks_import_csv.Size = new System.Drawing.Size(250, 20);
            this.tb_tasks_import_csv.TabIndex = 12;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(17, 15);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(30, 13);
            this.label19.TabIndex = 12;
            this.label19.Text = "Data";
            // 
            // panel_import_directly
            // 
            this.panel_import_directly.Controls.Add(this.label18);
            this.panel_import_directly.Controls.Add(this.tb_tasks_import_accounts);
            this.panel_import_directly.Controls.Add(this.label17);
            this.panel_import_directly.Controls.Add(this.label16);
            this.panel_import_directly.Controls.Add(this.btn_tasks_import_browse_folder);
            this.panel_import_directly.Controls.Add(this.tb_tasks_import_folder);
            this.panel_import_directly.Controls.Add(this.label15);
            this.panel_import_directly.Location = new System.Drawing.Point(43, 168);
            this.panel_import_directly.Name = "panel_import_directly";
            this.panel_import_directly.Size = new System.Drawing.Size(740, 99);
            this.panel_import_directly.TabIndex = 6;
            this.panel_import_directly.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(348, 15);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(169, 13);
            this.label18.TabIndex = 11;
            this.label18.Text = "**list accounts separate by comma";
            // 
            // tb_tasks_import_accounts
            // 
            this.tb_tasks_import_accounts.Location = new System.Drawing.Point(78, 12);
            this.tb_tasks_import_accounts.Name = "tb_tasks_import_accounts";
            this.tb_tasks_import_accounts.Size = new System.Drawing.Size(250, 20);
            this.tb_tasks_import_accounts.TabIndex = 10;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 15);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 13);
            this.label17.TabIndex = 9;
            this.label17.Text = "Accounts";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(480, 44);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(133, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "** contains data per .txt file";
            // 
            // btn_tasks_import_browse_folder
            // 
            this.btn_tasks_import_browse_folder.Location = new System.Drawing.Point(351, 39);
            this.btn_tasks_import_browse_folder.Name = "btn_tasks_import_browse_folder";
            this.btn_tasks_import_browse_folder.Size = new System.Drawing.Size(95, 23);
            this.btn_tasks_import_browse_folder.TabIndex = 7;
            this.btn_tasks_import_browse_folder.Text = "Browse";
            this.btn_tasks_import_browse_folder.UseVisualStyleBackColor = true;
            this.btn_tasks_import_browse_folder.Click += new System.EventHandler(this.btn_tasks_import_browse_folder_Click);
            // 
            // tb_tasks_import_folder
            // 
            this.tb_tasks_import_folder.Location = new System.Drawing.Point(78, 41);
            this.tb_tasks_import_folder.Name = "tb_tasks_import_folder";
            this.tb_tasks_import_folder.Size = new System.Drawing.Size(250, 20);
            this.tb_tasks_import_folder.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(17, 44);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Folder";
            // 
            // btn_tasks_import_fromcsv
            // 
            this.btn_tasks_import_fromcsv.Location = new System.Drawing.Point(159, 108);
            this.btn_tasks_import_fromcsv.Name = "btn_tasks_import_fromcsv";
            this.btn_tasks_import_fromcsv.Size = new System.Drawing.Size(95, 23);
            this.btn_tasks_import_fromcsv.TabIndex = 5;
            this.btn_tasks_import_fromcsv.Text = "Import From Csv";
            this.btn_tasks_import_fromcsv.UseVisualStyleBackColor = true;
            this.btn_tasks_import_fromcsv.Click += new System.EventHandler(this.btn_tasks_import_fromcsv_Click);
            // 
            // btn_tasks_import_directly
            // 
            this.btn_tasks_import_directly.Location = new System.Drawing.Point(43, 108);
            this.btn_tasks_import_directly.Name = "btn_tasks_import_directly";
            this.btn_tasks_import_directly.Size = new System.Drawing.Size(95, 23);
            this.btn_tasks_import_directly.TabIndex = 4;
            this.btn_tasks_import_directly.Text = "Import Directly";
            this.btn_tasks_import_directly.UseVisualStyleBackColor = true;
            this.btn_tasks_import_directly.Click += new System.EventHandler(this.btn_tasks_import_directly_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(183, 62);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(316, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "** check for random do task in next 1 hours, if not, it\'ll be run asap";
            // 
            // checkBox_tasks_import_schedule_random_times
            // 
            this.checkBox_tasks_import_schedule_random_times.AutoSize = true;
            this.checkBox_tasks_import_schedule_random_times.Location = new System.Drawing.Point(43, 62);
            this.checkBox_tasks_import_schedule_random_times.Name = "checkBox_tasks_import_schedule_random_times";
            this.checkBox_tasks_import_schedule_random_times.Size = new System.Drawing.Size(134, 17);
            this.checkBox_tasks_import_schedule_random_times.TabIndex = 2;
            this.checkBox_tasks_import_schedule_random_times.Text = "schedule random times";
            this.checkBox_tasks_import_schedule_random_times.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Task";
            // 
            // comboBox_tasks_import_task
            // 
            this.comboBox_tasks_import_task.FormattingEnabled = true;
            this.comboBox_tasks_import_task.Items.AddRange(new object[] {
            "Update Bio"});
            this.comboBox_tasks_import_task.Location = new System.Drawing.Point(43, 18);
            this.comboBox_tasks_import_task.Name = "comboBox_tasks_import_task";
            this.comboBox_tasks_import_task.Size = new System.Drawing.Size(121, 21);
            this.comboBox_tasks_import_task.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.panel_posts_add);
            this.tabPage4.Controls.Add(this.btn_posts_add);
            this.tabPage4.Controls.Add(this.dataGridView_posts_manage);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1259, 508);
            this.tabPage4.TabIndex = 8;
            this.tabPage4.Text = "Posts";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panel_posts_add
            // 
            this.panel_posts_add.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_posts_add.Controls.Add(this.btn_posts_add_images);
            this.panel_posts_add.Controls.Add(this.btn_posts_add_addpost);
            this.panel_posts_add.Controls.Add(this.comboBox_posts_add_state);
            this.panel_posts_add.Controls.Add(this.label25);
            this.panel_posts_add.Controls.Add(this.tb_posts_add_images);
            this.panel_posts_add.Controls.Add(this.label24);
            this.panel_posts_add.Controls.Add(this.tb_posts_add_settings);
            this.panel_posts_add.Controls.Add(this.label23);
            this.panel_posts_add.Controls.Add(this.label22);
            this.panel_posts_add.Controls.Add(this.tb_posts_add_hashtag);
            this.panel_posts_add.Controls.Add(this.tb_posts_add_content);
            this.panel_posts_add.Controls.Add(this.label21);
            this.panel_posts_add.Location = new System.Drawing.Point(6, 364);
            this.panel_posts_add.Name = "panel_posts_add";
            this.panel_posts_add.Size = new System.Drawing.Size(973, 193);
            this.panel_posts_add.TabIndex = 2;
            // 
            // btn_posts_add_images
            // 
            this.btn_posts_add_images.Location = new System.Drawing.Point(766, 101);
            this.btn_posts_add_images.Name = "btn_posts_add_images";
            this.btn_posts_add_images.Size = new System.Drawing.Size(65, 89);
            this.btn_posts_add_images.TabIndex = 10;
            this.btn_posts_add_images.Text = "Add Images";
            this.btn_posts_add_images.UseVisualStyleBackColor = true;
            this.btn_posts_add_images.Click += new System.EventHandler(this.btn_posts_add_images_Click);
            // 
            // btn_posts_add_addpost
            // 
            this.btn_posts_add_addpost.Location = new System.Drawing.Point(840, 152);
            this.btn_posts_add_addpost.Name = "btn_posts_add_addpost";
            this.btn_posts_add_addpost.Size = new System.Drawing.Size(121, 38);
            this.btn_posts_add_addpost.TabIndex = 3;
            this.btn_posts_add_addpost.Text = "Add Post";
            this.btn_posts_add_addpost.UseVisualStyleBackColor = true;
            this.btn_posts_add_addpost.Click += new System.EventHandler(this.btn_posts_add_addpost_Click);
            // 
            // comboBox_posts_add_state
            // 
            this.comboBox_posts_add_state.FormattingEnabled = true;
            this.comboBox_posts_add_state.Items.AddRange(new object[] {
            "ACTIVE",
            "PAUSED"});
            this.comboBox_posts_add_state.Location = new System.Drawing.Point(840, 26);
            this.comboBox_posts_add_state.Name = "comboBox_posts_add_state";
            this.comboBox_posts_add_state.Size = new System.Drawing.Size(121, 21);
            this.comboBox_posts_add_state.TabIndex = 9;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(837, 10);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(42, 13);
            this.label25.TabIndex = 8;
            this.label25.Text = "STATE";
            // 
            // tb_posts_add_images
            // 
            this.tb_posts_add_images.Location = new System.Drawing.Point(560, 101);
            this.tb_posts_add_images.Multiline = true;
            this.tb_posts_add_images.Name = "tb_posts_add_images";
            this.tb_posts_add_images.Size = new System.Drawing.Size(200, 89);
            this.tb_posts_add_images.TabIndex = 7;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(560, 85);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 13);
            this.label24.TabIndex = 6;
            this.label24.Text = "Images";
            // 
            // tb_posts_add_settings
            // 
            this.tb_posts_add_settings.Location = new System.Drawing.Point(560, 26);
            this.tb_posts_add_settings.Multiline = true;
            this.tb_posts_add_settings.Name = "tb_posts_add_settings";
            this.tb_posts_add_settings.Size = new System.Drawing.Size(271, 56);
            this.tb_posts_add_settings.TabIndex = 5;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(559, 10);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(45, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "Settings";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(280, 10);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 13);
            this.label22.TabIndex = 3;
            this.label22.Text = "Hashtag";
            // 
            // tb_posts_add_hashtag
            // 
            this.tb_posts_add_hashtag.Location = new System.Drawing.Point(283, 26);
            this.tb_posts_add_hashtag.Multiline = true;
            this.tb_posts_add_hashtag.Name = "tb_posts_add_hashtag";
            this.tb_posts_add_hashtag.Size = new System.Drawing.Size(271, 164);
            this.tb_posts_add_hashtag.TabIndex = 2;
            // 
            // tb_posts_add_content
            // 
            this.tb_posts_add_content.Location = new System.Drawing.Point(6, 26);
            this.tb_posts_add_content.Multiline = true;
            this.tb_posts_add_content.Name = "tb_posts_add_content";
            this.tb_posts_add_content.Size = new System.Drawing.Size(271, 164);
            this.tb_posts_add_content.TabIndex = 1;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 10);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(44, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "Content";
            // 
            // btn_posts_add
            // 
            this.btn_posts_add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_posts_add.Location = new System.Drawing.Point(6, 335);
            this.btn_posts_add.Name = "btn_posts_add";
            this.btn_posts_add.Size = new System.Drawing.Size(75, 23);
            this.btn_posts_add.TabIndex = 1;
            this.btn_posts_add.Text = "Add Post";
            this.btn_posts_add.UseVisualStyleBackColor = true;
            // 
            // dataGridView_posts_manage
            // 
            this.dataGridView_posts_manage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_posts_manage.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView_posts_manage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_posts_manage.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView_posts_manage.Location = new System.Drawing.Point(6, 6);
            this.dataGridView_posts_manage.Name = "dataGridView_posts_manage";
            this.dataGridView_posts_manage.Size = new System.Drawing.Size(973, 323);
            this.dataGridView_posts_manage.TabIndex = 0;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.label68);
            this.tabPage7.Controls.Add(this.checkBox_register_auto_generate_user);
            this.tabPage7.Controls.Add(this.comboBox_register_phone_services);
            this.tabPage7.Controls.Add(this.label42);
            this.tabPage7.Controls.Add(this.cb_register_addto_account_reg);
            this.tabPage7.Controls.Add(this.label41);
            this.tabPage7.Controls.Add(this.btn_register_load_original_names);
            this.tabPage7.Controls.Add(this.label40);
            this.tabPage7.Controls.Add(this.btn_register_generate_names);
            this.tabPage7.Controls.Add(this.label39);
            this.tabPage7.Controls.Add(this.label38);
            this.tabPage7.Controls.Add(this.tb_register_quantity);
            this.tabPage7.Controls.Add(this.tb_register_list_names);
            this.tabPage7.Controls.Add(this.checkBox_register_skiperror);
            this.tabPage7.Controls.Add(this.btn_register_start);
            this.tabPage7.Controls.Add(this.label28);
            this.tabPage7.Controls.Add(this.label27);
            this.tabPage7.Controls.Add(this.tb_register_deviceids);
            this.tabPage7.Controls.Add(this.label26);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(1259, 508);
            this.tabPage7.TabIndex = 9;
            this.tabPage7.Text = "Register";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(379, 116);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(196, 13);
            this.label68.TabIndex = 19;
            this.label68.Text = "** auto generate user based on fullname";
            // 
            // checkBox_register_auto_generate_user
            // 
            this.checkBox_register_auto_generate_user.AutoSize = true;
            this.checkBox_register_auto_generate_user.Location = new System.Drawing.Point(368, 97);
            this.checkBox_register_auto_generate_user.Name = "checkBox_register_auto_generate_user";
            this.checkBox_register_auto_generate_user.Size = new System.Drawing.Size(120, 17);
            this.checkBox_register_auto_generate_user.TabIndex = 18;
            this.checkBox_register_auto_generate_user.Text = "Auto Generate User";
            this.checkBox_register_auto_generate_user.UseVisualStyleBackColor = true;
            this.checkBox_register_auto_generate_user.CheckedChanged += new System.EventHandler(this.checkBox_register_auto_generate_user_CheckedChanged);
            // 
            // comboBox_register_phone_services
            // 
            this.comboBox_register_phone_services.FormattingEnabled = true;
            this.comboBox_register_phone_services.Items.AddRange(new object[] {
            "rentcode.co",
            "nanosim.vn",
            "otpsim.com",
            "chothuesimcode.com"});
            this.comboBox_register_phone_services.Location = new System.Drawing.Point(458, 55);
            this.comboBox_register_phone_services.Name = "comboBox_register_phone_services";
            this.comboBox_register_phone_services.Size = new System.Drawing.Size(121, 21);
            this.comboBox_register_phone_services.TabIndex = 17;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(370, 58);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(82, 13);
            this.label42.TabIndex = 16;
            this.label42.Text = "Phone Services";
            // 
            // cb_register_addto_account_reg
            // 
            this.cb_register_addto_account_reg.AutoSize = true;
            this.cb_register_addto_account_reg.Location = new System.Drawing.Point(486, 278);
            this.cb_register_addto_account_reg.Name = "cb_register_addto_account_reg";
            this.cb_register_addto_account_reg.Size = new System.Drawing.Size(167, 17);
            this.cb_register_addto_account_reg.TabIndex = 15;
            this.cb_register_addto_account_reg.Text = "Add to database account_reg";
            this.cb_register_addto_account_reg.UseVisualStyleBackColor = true;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(474, 518);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(205, 13);
            this.label41.TabIndex = 14;
            this.label41.Text = "you can load original names from resource";
            // 
            // btn_register_load_original_names
            // 
            this.btn_register_load_original_names.Location = new System.Drawing.Point(368, 503);
            this.btn_register_load_original_names.Name = "btn_register_load_original_names";
            this.btn_register_load_original_names.Size = new System.Drawing.Size(100, 43);
            this.btn_register_load_original_names.TabIndex = 13;
            this.btn_register_load_original_names.Text = "Load Origin Names";
            this.btn_register_load_original_names.UseVisualStyleBackColor = true;
            this.btn_register_load_original_names.Click += new System.EventHandler(this.btn_register_load_original_names_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(370, 312);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(227, 13);
            this.label40.TabIndex = 12;
            this.label40.Text = "name will be copied to clipboard after generate";
            // 
            // btn_register_generate_names
            // 
            this.btn_register_generate_names.Location = new System.Drawing.Point(368, 274);
            this.btn_register_generate_names.Name = "btn_register_generate_names";
            this.btn_register_generate_names.Size = new System.Drawing.Size(100, 23);
            this.btn_register_generate_names.TabIndex = 11;
            this.btn_register_generate_names.Text = "Generate Names";
            this.btn_register_generate_names.UseVisualStyleBackColor = true;
            this.btn_register_generate_names.Click += new System.EventHandler(this.btn_register_generate_names_Click);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(365, 219);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(46, 13);
            this.label39.TabIndex = 10;
            this.label39.Text = "Quantity";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(13, 219);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(208, 13);
            this.label38.TabIndex = 9;
            this.label38.Text = "Fill list names (use to generate new names)";
            // 
            // tb_register_quantity
            // 
            this.tb_register_quantity.Location = new System.Drawing.Point(368, 235);
            this.tb_register_quantity.Name = "tb_register_quantity";
            this.tb_register_quantity.Size = new System.Drawing.Size(100, 20);
            this.tb_register_quantity.TabIndex = 7;
            // 
            // tb_register_list_names
            // 
            this.tb_register_list_names.Location = new System.Drawing.Point(16, 235);
            this.tb_register_list_names.Multiline = true;
            this.tb_register_list_names.Name = "tb_register_list_names";
            this.tb_register_list_names.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_register_list_names.Size = new System.Drawing.Size(329, 311);
            this.tb_register_list_names.TabIndex = 6;
            // 
            // checkBox_register_skiperror
            // 
            this.checkBox_register_skiperror.AutoSize = true;
            this.checkBox_register_skiperror.Location = new System.Drawing.Point(507, 97);
            this.checkBox_register_skiperror.Name = "checkBox_register_skiperror";
            this.checkBox_register_skiperror.Size = new System.Drawing.Size(72, 17);
            this.checkBox_register_skiperror.TabIndex = 5;
            this.checkBox_register_skiperror.Text = "Skip Error";
            this.checkBox_register_skiperror.UseVisualStyleBackColor = true;
            this.checkBox_register_skiperror.CheckedChanged += new System.EventHandler(this.checkBox_register_skiperror_CheckedChanged);
            // 
            // btn_register_start
            // 
            this.btn_register_start.Location = new System.Drawing.Point(91, 168);
            this.btn_register_start.Name = "btn_register_start";
            this.btn_register_start.Size = new System.Drawing.Size(254, 23);
            this.btn_register_start.TabIndex = 4;
            this.btn_register_start.Text = "Register";
            this.btn_register_start.UseVisualStyleBackColor = true;
            this.btn_register_start.Click += new System.EventHandler(this.btn_register_start_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.Color.Brown;
            this.label28.Location = new System.Drawing.Point(13, 129);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(445, 13);
            this.label28.TabIndex = 3;
            this.label28.Text = "Import note: You MUST Install Clipper and ADBKeyboard to make this function work " +
    "correctly";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(365, 21);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(116, 13);
            this.label27.TabIndex = 2;
            this.label27.Text = "**device Ids line by line";
            // 
            // tb_register_deviceids
            // 
            this.tb_register_deviceids.Location = new System.Drawing.Point(91, 18);
            this.tb_register_deviceids.Multiline = true;
            this.tb_register_deviceids.Name = "tb_register_deviceids";
            this.tb_register_deviceids.Size = new System.Drawing.Size(254, 96);
            this.tb_register_deviceids.TabIndex = 1;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(13, 21);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(58, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "Device Ids";
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.label63);
            this.tabPage11.Controls.Add(this.textBox_transferaccount_log);
            this.tabPage11.Controls.Add(this.label62);
            this.tabPage11.Controls.Add(this.label61);
            this.tabPage11.Controls.Add(this.label60);
            this.tabPage11.Controls.Add(this.label59);
            this.tabPage11.Controls.Add(this.label58);
            this.tabPage11.Controls.Add(this.label57);
            this.tabPage11.Controls.Add(this.label56);
            this.tabPage11.Controls.Add(this.label55);
            this.tabPage11.Controls.Add(this.label54);
            this.tabPage11.Controls.Add(this.button_transferaccount_transfer_now);
            this.tabPage11.Controls.Add(this.label53);
            this.tabPage11.Controls.Add(this.button_transferaccount_to_browse_data);
            this.tabPage11.Controls.Add(this.textBox_transferaccount_to_folder_data);
            this.tabPage11.Controls.Add(this.label48);
            this.tabPage11.Controls.Add(this.button_transferaccount_to_testconnection);
            this.tabPage11.Controls.Add(this.textBox_transferaccount_to_database);
            this.tabPage11.Controls.Add(this.label49);
            this.tabPage11.Controls.Add(this.label50);
            this.tabPage11.Controls.Add(this.label51);
            this.tabPage11.Controls.Add(this.label52);
            this.tabPage11.Controls.Add(this.textBox_transferaccount_to_server_ip);
            this.tabPage11.Controls.Add(this.button_transferaccount_from_browse_data);
            this.tabPage11.Controls.Add(this.textBox_transferaccount_from_folder_data);
            this.tabPage11.Controls.Add(this.label47);
            this.tabPage11.Controls.Add(this.button_transferaccount_from_testconnection);
            this.tabPage11.Controls.Add(this.textBox_transferaccount_from_database);
            this.tabPage11.Controls.Add(this.label46);
            this.tabPage11.Controls.Add(this.label45);
            this.tabPage11.Controls.Add(this.label44);
            this.tabPage11.Controls.Add(this.label43);
            this.tabPage11.Controls.Add(this.textBox_transferaccount_from_server_ip);
            this.tabPage11.Controls.Add(this.textBox_transferaccount_from_accountId);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(1259, 508);
            this.tabPage11.TabIndex = 11;
            this.tabPage11.Text = "TransferAccount";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(455, 145);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(25, 13);
            this.label63.TabIndex = 33;
            this.label63.Text = "Log";
            // 
            // textBox_transferaccount_log
            // 
            this.textBox_transferaccount_log.Location = new System.Drawing.Point(455, 161);
            this.textBox_transferaccount_log.Multiline = true;
            this.textBox_transferaccount_log.Name = "textBox_transferaccount_log";
            this.textBox_transferaccount_log.Size = new System.Drawing.Size(348, 233);
            this.textBox_transferaccount_log.TabIndex = 32;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(455, 474);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(52, 13);
            this.label62.TabIndex = 31;
            this.label62.Text = "- Do it job";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(455, 461);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(636, 13);
            this.label61.TabIndex = 30;
            this.label61.Text = "- First, the software will validate to make sure all infromation are provided cor" +
    "rectly. If one of those information not correct, it won\'t work";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(455, 445);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(68, 13);
            this.label60.TabIndex = 29;
            this.label60.Text = "How it works";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(455, 538);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(273, 13);
            this.label59.TabIndex = 28;
            this.label59.Text = "- Folder Data \"FROM\" and \"TO\" must be in the same pc";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(455, 524);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(30, 13);
            this.label58.TabIndex = 27;
            this.label58.Text = "Note";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(455, 509);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(225, 13);
            this.label57.TabIndex = 26;
            this.label57.Text = "- Fill information and list account need to move";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(455, 493);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(56, 13);
            this.label56.TabIndex = 25;
            this.label56.Text = "Instruction";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(455, 425);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(534, 13);
            this.label55.TabIndex = 24;
            this.label55.Text = "- Software will move data include ig_account,actionlog,ldplayer information, dnpl" +
    "ayer-data from \"FROM\" to \"TO\"";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(3, 145);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(64, 13);
            this.label54.TabIndex = 23;
            this.label54.Text = "Account Ids";
            // 
            // button_transferaccount_transfer_now
            // 
            this.button_transferaccount_transfer_now.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_transferaccount_transfer_now.Location = new System.Drawing.Point(1017, 62);
            this.button_transferaccount_transfer_now.Name = "button_transferaccount_transfer_now";
            this.button_transferaccount_transfer_now.Size = new System.Drawing.Size(139, 64);
            this.button_transferaccount_transfer_now.TabIndex = 22;
            this.button_transferaccount_transfer_now.Text = "Transfer NOW";
            this.button_transferaccount_transfer_now.UseVisualStyleBackColor = true;
            this.button_transferaccount_transfer_now.Click += new System.EventHandler(this.button_transferaccount_transfer_now_Click);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(455, 409);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(63, 13);
            this.label53.TabIndex = 21;
            this.label53.Text = "Introduction";
            // 
            // button_transferaccount_to_browse_data
            // 
            this.button_transferaccount_to_browse_data.Location = new System.Drawing.Point(702, 103);
            this.button_transferaccount_to_browse_data.Name = "button_transferaccount_to_browse_data";
            this.button_transferaccount_to_browse_data.Size = new System.Drawing.Size(101, 23);
            this.button_transferaccount_to_browse_data.TabIndex = 20;
            this.button_transferaccount_to_browse_data.Text = "Browse";
            this.button_transferaccount_to_browse_data.UseVisualStyleBackColor = true;
            this.button_transferaccount_to_browse_data.Click += new System.EventHandler(this.button_transferaccount_to_browse_data_Click);
            // 
            // textBox_transferaccount_to_folder_data
            // 
            this.textBox_transferaccount_to_folder_data.Location = new System.Drawing.Point(458, 105);
            this.textBox_transferaccount_to_folder_data.Name = "textBox_transferaccount_to_folder_data";
            this.textBox_transferaccount_to_folder_data.Size = new System.Drawing.Size(223, 20);
            this.textBox_transferaccount_to_folder_data.TabIndex = 19;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(455, 89);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(62, 13);
            this.label48.TabIndex = 18;
            this.label48.Text = "Folder Data";
            // 
            // button_transferaccount_to_testconnection
            // 
            this.button_transferaccount_to_testconnection.Location = new System.Drawing.Point(702, 60);
            this.button_transferaccount_to_testconnection.Name = "button_transferaccount_to_testconnection";
            this.button_transferaccount_to_testconnection.Size = new System.Drawing.Size(101, 23);
            this.button_transferaccount_to_testconnection.TabIndex = 17;
            this.button_transferaccount_to_testconnection.Text = "Test Connection";
            this.button_transferaccount_to_testconnection.UseVisualStyleBackColor = true;
            this.button_transferaccount_to_testconnection.Click += new System.EventHandler(this.button_transferaccount_to_testconnection_Click);
            // 
            // textBox_transferaccount_to_database
            // 
            this.textBox_transferaccount_to_database.Location = new System.Drawing.Point(581, 62);
            this.textBox_transferaccount_to_database.Name = "textBox_transferaccount_to_database";
            this.textBox_transferaccount_to_database.Size = new System.Drawing.Size(100, 20);
            this.textBox_transferaccount_to_database.TabIndex = 16;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(578, 46);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(53, 13);
            this.label49.TabIndex = 15;
            this.label49.Text = "Database";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(455, 46);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(50, 13);
            this.label50.TabIndex = 14;
            this.label50.Text = "Server Ip";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(455, 24);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(79, 13);
            this.label51.TabIndex = 13;
            this.label51.Text = "DB Connection";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(455, 8);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(22, 13);
            this.label52.TabIndex = 12;
            this.label52.Text = "TO";
            // 
            // textBox_transferaccount_to_server_ip
            // 
            this.textBox_transferaccount_to_server_ip.Location = new System.Drawing.Point(455, 62);
            this.textBox_transferaccount_to_server_ip.Name = "textBox_transferaccount_to_server_ip";
            this.textBox_transferaccount_to_server_ip.Size = new System.Drawing.Size(100, 20);
            this.textBox_transferaccount_to_server_ip.TabIndex = 11;
            // 
            // button_transferaccount_from_browse_data
            // 
            this.button_transferaccount_from_browse_data.Location = new System.Drawing.Point(250, 102);
            this.button_transferaccount_from_browse_data.Name = "button_transferaccount_from_browse_data";
            this.button_transferaccount_from_browse_data.Size = new System.Drawing.Size(101, 23);
            this.button_transferaccount_from_browse_data.TabIndex = 10;
            this.button_transferaccount_from_browse_data.Text = "Browse";
            this.button_transferaccount_from_browse_data.UseVisualStyleBackColor = true;
            this.button_transferaccount_from_browse_data.Click += new System.EventHandler(this.button_transferaccount_from_browse_data_Click);
            // 
            // textBox_transferaccount_from_folder_data
            // 
            this.textBox_transferaccount_from_folder_data.Location = new System.Drawing.Point(6, 104);
            this.textBox_transferaccount_from_folder_data.Name = "textBox_transferaccount_from_folder_data";
            this.textBox_transferaccount_from_folder_data.Size = new System.Drawing.Size(223, 20);
            this.textBox_transferaccount_from_folder_data.TabIndex = 9;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(3, 88);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(62, 13);
            this.label47.TabIndex = 8;
            this.label47.Text = "Folder Data";
            // 
            // button_transferaccount_from_testconnection
            // 
            this.button_transferaccount_from_testconnection.Location = new System.Drawing.Point(250, 59);
            this.button_transferaccount_from_testconnection.Name = "button_transferaccount_from_testconnection";
            this.button_transferaccount_from_testconnection.Size = new System.Drawing.Size(101, 23);
            this.button_transferaccount_from_testconnection.TabIndex = 7;
            this.button_transferaccount_from_testconnection.Text = "Test Connection";
            this.button_transferaccount_from_testconnection.UseVisualStyleBackColor = true;
            this.button_transferaccount_from_testconnection.Click += new System.EventHandler(this.button_transferaccount_from_testconnection_Click);
            // 
            // textBox_transferaccount_from_database
            // 
            this.textBox_transferaccount_from_database.Location = new System.Drawing.Point(129, 61);
            this.textBox_transferaccount_from_database.Name = "textBox_transferaccount_from_database";
            this.textBox_transferaccount_from_database.Size = new System.Drawing.Size(100, 20);
            this.textBox_transferaccount_from_database.TabIndex = 6;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(126, 45);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(53, 13);
            this.label46.TabIndex = 5;
            this.label46.Text = "Database";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(3, 45);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(50, 13);
            this.label45.TabIndex = 4;
            this.label45.Text = "Server Ip";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(3, 23);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(79, 13);
            this.label44.TabIndex = 3;
            this.label44.Text = "DB Connection";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(3, 7);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(38, 13);
            this.label43.TabIndex = 2;
            this.label43.Text = "FROM";
            // 
            // textBox_transferaccount_from_server_ip
            // 
            this.textBox_transferaccount_from_server_ip.Location = new System.Drawing.Point(3, 61);
            this.textBox_transferaccount_from_server_ip.Name = "textBox_transferaccount_from_server_ip";
            this.textBox_transferaccount_from_server_ip.Size = new System.Drawing.Size(100, 20);
            this.textBox_transferaccount_from_server_ip.TabIndex = 1;
            // 
            // textBox_transferaccount_from_accountId
            // 
            this.textBox_transferaccount_from_accountId.Location = new System.Drawing.Point(3, 161);
            this.textBox_transferaccount_from_accountId.Multiline = true;
            this.textBox_transferaccount_from_accountId.Name = "textBox_transferaccount_from_accountId";
            this.textBox_transferaccount_from_accountId.Size = new System.Drawing.Size(348, 399);
            this.textBox_transferaccount_from_accountId.TabIndex = 0;
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.button_control_panel_clear);
            this.tabPage12.Controls.Add(this.textBox_control_panel_pc_devices);
            this.tabPage12.Controls.Add(this.label66);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(1259, 508);
            this.tabPage12.TabIndex = 12;
            this.tabPage12.Text = "Control Panel";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // button_control_panel_clear
            // 
            this.button_control_panel_clear.Location = new System.Drawing.Point(327, 10);
            this.button_control_panel_clear.Name = "button_control_panel_clear";
            this.button_control_panel_clear.Size = new System.Drawing.Size(75, 23);
            this.button_control_panel_clear.TabIndex = 2;
            this.button_control_panel_clear.Text = "Clear NOW";
            this.button_control_panel_clear.UseVisualStyleBackColor = true;
            this.button_control_panel_clear.Click += new System.EventHandler(this.button_control_panel_clear_Click);
            // 
            // textBox_control_panel_pc_devices
            // 
            this.textBox_control_panel_pc_devices.Location = new System.Drawing.Point(207, 12);
            this.textBox_control_panel_pc_devices.Name = "textBox_control_panel_pc_devices";
            this.textBox_control_panel_pc_devices.Size = new System.Drawing.Size(100, 20);
            this.textBox_control_panel_pc_devices.TabIndex = 1;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(15, 15);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(186, 13);
            this.label66.TabIndex = 0;
            this.label66.Text = "Clear all status claimed by pc_devices";
            // 
            // goToSettingsToolStripMenuItem
            // 
            this.goToSettingsToolStripMenuItem.Name = "goToSettingsToolStripMenuItem";
            this.goToSettingsToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.goToSettingsToolStripMenuItem.Text = "Go to Settings";
            // 
            // launchmanuallyToolStripMenuItem
            // 
            this.launchmanuallyToolStripMenuItem.Name = "launchmanuallyToolStripMenuItem";
            this.launchmanuallyToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.launchmanuallyToolStripMenuItem.Text = "Launch (manually)";
            // 
            // btn_Settings
            // 
            this.btn_Settings.Location = new System.Drawing.Point(9, 4);
            this.btn_Settings.Name = "btn_Settings";
            this.btn_Settings.Size = new System.Drawing.Size(75, 23);
            this.btn_Settings.TabIndex = 1;
            this.btn_Settings.Text = "Settings";
            this.btn_Settings.UseVisualStyleBackColor = true;
            this.btn_Settings.Click += new System.EventHandler(this.btn_Settings_Click);
            // 
            // label69
            // 
            this.label69.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(871, 17);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(43, 13);
            this.label69.TabIndex = 2;
            this.label69.Text = "Project:";
            // 
            // label_project_name
            // 
            this.label_project_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_project_name.AutoSize = true;
            this.label_project_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_project_name.ForeColor = System.Drawing.Color.Green;
            this.label_project_name.Location = new System.Drawing.Point(916, 8);
            this.label_project_name.Name = "label_project_name";
            this.label_project_name.Size = new System.Drawing.Size(64, 26);
            this.label_project_name.TabIndex = 3;
            this.label_project_name.Text = "none";
            // 
            // label71
            // 
            this.label71.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(1030, 17);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(65, 13);
            this.label71.TabIndex = 4;
            this.label71.Text = "pc_devices:";
            // 
            // label_pc_devices
            // 
            this.label_pc_devices.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_pc_devices.AutoSize = true;
            this.label_pc_devices.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_pc_devices.ForeColor = System.Drawing.Color.Green;
            this.label_pc_devices.Location = new System.Drawing.Point(1095, 8);
            this.label_pc_devices.Name = "label_pc_devices";
            this.label_pc_devices.Size = new System.Drawing.Size(64, 26);
            this.label_pc_devices.TabIndex = 5;
            this.label_pc_devices.Text = "none";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1285, 573);
            this.Controls.Add(this.label_pc_devices);
            this.Controls.Add(this.label71);
            this.Controls.Add(this.label_project_name);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.btn_Settings);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Instagram LDPlayer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.tabControl1.ResumeLayout(false);
            this.tabPage_igaccounts.ResumeLayout(false);
            this.tabPage_igaccounts.PerformLayout();
            this.panel_background_task.ResumeLayout(false);
            this.panel_background_task.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_manage)).EndInit();
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabControl4.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_slavesmanual)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_slaves_comment)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.panel_import_from_csv.ResumeLayout(false);
            this.panel_import_from_csv.PerformLayout();
            this.panel_import_directly.ResumeLayout(false);
            this.panel_import_directly.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.panel_posts_add.ResumeLayout(false);
            this.panel_posts_add.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_posts_manage)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage11.ResumeLayout(false);
            this.tabPage11.PerformLayout();
            this.tabPage12.ResumeLayout(false);
            this.tabPage12.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button btn_register_clone;
        private System.Windows.Forms.Button btn_register_batch;
        private System.Windows.Forms.ComboBox cb_batch_option;
        private System.Windows.Forms.Button main_btn_stop;
        private System.Windows.Forms.ToolStripMenuItem launchmanuallyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToSettingsToolStripMenuItem;
        private System.Windows.Forms.Button btn_tasking;
        private System.Windows.Forms.ComboBox cb_tasking;
        private System.Windows.Forms.TabPage tabPage_igaccounts;
        private System.Windows.Forms.DataGridView dataGridView_manage;
        private System.Windows.Forms.Button btn_Settings;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox cb_minimize_ldplayer;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dataGridView_slavesmanual;
        private System.Windows.Forms.Button btn_search_device;
        private System.Windows.Forms.TextBox tb_phonedevice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox cb_Monitor_Message;
        private System.Windows.Forms.CheckBox cb_Monitor_Comment;
        private System.Windows.Forms.CheckBox cb_Has_Message;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cb_skip_error;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btn_set_resource;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBox_remove_old_resources;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cb_resources;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_accounts_resource;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btn_set_resource_browser;
        private System.Windows.Forms.TextBox tb_resource_data_folder;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tb_resources_quantity;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Panel panel_import_directly;
        private System.Windows.Forms.Button btn_tasks_import_fromcsv;
        private System.Windows.Forms.Button btn_tasks_import_directly;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox checkBox_tasks_import_schedule_random_times;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox comboBox_tasks_import_task;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tb_tasks_import_accounts;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btn_tasks_import_browse_folder;
        private System.Windows.Forms.TextBox tb_tasks_import_folder;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel_import_from_csv;
        private System.Windows.Forms.Button btn_tasks_import_browse_csv;
        private System.Windows.Forms.TextBox tb_tasks_import_csv;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btn_tasks_import_addtasks;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dataGridView_posts_manage;
        private System.Windows.Forms.Panel panel_posts_add;
        private System.Windows.Forms.Button btn_posts_add_images;
        private System.Windows.Forms.Button btn_posts_add_addpost;
        private System.Windows.Forms.ComboBox comboBox_posts_add_state;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tb_posts_add_images;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tb_posts_add_settings;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tb_posts_add_hashtag;
        private System.Windows.Forms.TextBox tb_posts_add_content;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btn_posts_add;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Button btn_register_start;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tb_register_deviceids;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox checkBox_keep_ldplay_open;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.CheckBox checkBox_register_skiperror;
        private System.Windows.Forms.Button btn_slaves_backupff_profile;
        private System.Windows.Forms.Label label_slaves_backup_status;
        private System.Windows.Forms.CheckBox checkBox_skip_launch_error;
        private System.Windows.Forms.Panel panel_background_task;
        private System.Windows.Forms.CheckBox checkBox_scrape_slaves_comment;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_do_background_task;
        private System.Windows.Forms.CheckBox cb_Has_Comment;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.DataGridView dataGridView_slaves_comment;
        private System.Windows.Forms.Button btn_refresh_slaves_comment;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button btn_settings_tab_update;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox tb_settings_json_setting;
        private System.Windows.Forms.TextBox tb_settings_json_users;
        private System.Windows.Forms.Button btn_setting_json_clear_all;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tb_register_quantity;
        private System.Windows.Forms.TextBox tb_register_list_names;
        private System.Windows.Forms.Button btn_register_generate_names;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Button btn_register_load_original_names;
        private System.Windows.Forms.CheckBox cb_register_addto_account_reg;
        private System.Windows.Forms.TextBox tb_log;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button btn_run_xproxy_server;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgcol_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgcol_niche;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgcol_user;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgcol_fullname;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgcol_note;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgcol_state;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgcol_status;
        private System.Windows.Forms.DataGridViewButtonColumn dgcol_settings;
        private System.Windows.Forms.DataGridViewButtonColumn dgcol_launch;
        private System.Windows.Forms.DataGridViewButtonColumn dgcol_close;
        private System.Windows.Forms.RadioButton rb_id;
        private System.Windows.Forms.RadioButton rb_user;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox comboBox_register_phone_services;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Button button_transferaccount_to_browse_data;
        private System.Windows.Forms.TextBox textBox_transferaccount_to_folder_data;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Button button_transferaccount_to_testconnection;
        private System.Windows.Forms.TextBox textBox_transferaccount_to_database;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox textBox_transferaccount_to_server_ip;
        private System.Windows.Forms.Button button_transferaccount_from_browse_data;
        private System.Windows.Forms.TextBox textBox_transferaccount_from_folder_data;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Button button_transferaccount_from_testconnection;
        private System.Windows.Forms.TextBox textBox_transferaccount_from_database;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox textBox_transferaccount_from_server_ip;
        private System.Windows.Forms.TextBox textBox_transferaccount_from_accountId;
        private System.Windows.Forms.Button button_transferaccount_transfer_now;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox textBox_transferaccount_log;
        private System.Windows.Forms.Button button_settings_removenow;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox textBox_settings_remove;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Button button_run_xproxy_aio;
        private System.Windows.Forms.Label label_run_xproxyaio;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.Button button_control_panel_clear;
        private System.Windows.Forms.TextBox textBox_control_panel_pc_devices;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Button button_stop_autorun;
        private System.Windows.Forms.Label label_autorun_counter;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.CheckBox checkBox_register_auto_generate_user;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label_project_name;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label_pc_devices;
    }
}

