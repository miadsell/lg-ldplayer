﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper
{
    static class GlobalHelper
    {
        static public void DownloadImage(string imageurl, string savepath)
        {
            ADBSupport.ADBHelpers.CreatePath(Path.GetDirectoryName(savepath));
            using (var m = new MemoryStream(new WebClient().DownloadData(imageurl)))
            {
                using (var img = Bitmap.FromStream(m))
                {

                    img.Save(savepath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
            }
        }

        static public bool Timespan_IsTimeInRange(DateTime checked_time, TimeSpan time_from, TimeSpan time_to)
        {
            var timeofday_checked_time = checked_time.TimeOfDay;

            var compare = TimeSpan.Compare(time_from, time_to);

            if (compare < 0) //time_from is earlier than time_to
            {
                if (timeofday_checked_time > time_from && timeofday_checked_time < time_to)
                    return true;
                return false;
            }
            else
            if (compare > 0) //time_from is later than time_to
            {
                if (timeofday_checked_time > time_from || timeofday_checked_time < time_to)
                    return true;
                return false;
            }

            return false;
        }

        /// <summary>
        /// detect datetime from time_from, time_to to check if generate stats for this date
        /// </summary>
        /// <param name="checked_time"></param>
        /// <param name="time_from"></param>
        /// <param name="time_to"></param>
        /// <returns></returns>
        static public bool DateTime_IsTimeInRange(DateTime checked_time, TimeSpan time_from, TimeSpan time_to)
        {
            var current_date = DateTime.Now.Date;

            if (time_from < time_to) //tuc la cùng trong 1 ngày theo calendar
            {
                //generate datetime from, to based on current_datetime, time_from, time_to
                DateTime dt_from = current_date + time_from;
                DateTime dt_to = current_date + time_to;

                //chec if checked_time in range

                if (checked_time > dt_from && checked_time < dt_to)
                    return true;
                return false;
            }

            if (time_from > time_to)
            {
                var current_timeofday = DateTime.Now.TimeOfDay;

                if (current_timeofday >= time_from)
                {
                    //truong hop current_timeofday >= time_from thi se lay dt_from today và dt_to tomorrow
                    DateTime dt_from = current_date + time_from;
                    DateTime dt_to = current_date.AddDays(1) + time_to;

                    if (checked_time >= dt_from && checked_time <= dt_to)
                        return true;
                    return false;
                }

                if (current_timeofday <= time_to)
                {
                    //truong hop current_timeofday <= time_from thi se lay dt_from yesterday và dt_to today
                    DateTime dt_from = current_date.AddDays(-1) + time_from;
                    DateTime dt_to = current_date + time_to;

                    if (checked_time >= dt_from && checked_time <= dt_to)
                        return true;
                    return false;
                }
            }

            throw new System.ArgumentException("Unknow Condition To Check");
        }
    }
}
