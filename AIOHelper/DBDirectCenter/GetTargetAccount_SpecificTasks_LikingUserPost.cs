﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.DBDirectCenter
{
    static class GetTargetAccount_SpecificTasks_LikingUserPost
    {
        static public dynamic RunByCenter(string accountId, string project, string type, bool is_take_data)
        {
            switch (project)
            {
                case "ww":
                    return WWProject(accountId, type, is_take_data);
                default:
                    throw new System.ArgumentException("Not found this project");
            }
        }


        #region --WWProject

        static private dynamic WWProject(string accountId, string type, bool is_take_data)
        {
            switch (type)
            {
                case "newliking":
                    return WWProject_NewLiking(accountId, is_take_data);
                case "ReEngage_Liking":
                    return WWProject_ReEngage_Liking(accountId, is_take_data);
                default:
                    throw new System.ArgumentException("Not found this type");
            }
        }

        /// <summary>
        /// use to like new target_account (not following before, mainly use to attract new following)
        /// </summary>
        static private dynamic WWProject_NewLiking(string accountId, bool is_take_data)
        {
            dynamic profile = null;

            var command_query = "";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();


                while (true)
                {
                    command_query =
                        @"select top 1 *
                            from scrapeusers
                            where
	                            [state]='READY' and
	                            ([status] is null or [status] not like 'Claimed%') and
	                            ([detect_gender]='male') and
	                            ([FollowingCount]<1000) and
	                            ([FollowerCount]<1000) and
	                            [following] is null and
	                            [likeuserpost] is null and
	                            [IsPrivate]='False' and
	                            [MediaCount]>0
                            order by first_added asc";
                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var command = new SqlCommand(command_query, conn))
                        {
                            using (var dataReader = command.ExecuteReader())
                            {
                                if (dataReader.HasRows)
                                {
                                    dataReader.Read();

                                    var pk = long.Parse(dataReader["pk"].ToString());
                                    var username = dataReader["UserName"].ToString();

                                    profile = new
                                    {
                                        pk,
                                        username
                                    };
                                }

                            }
                        }
                    }/*, "scrapeusers"*/);

                    if (profile != null && is_take_data)
                    {
                        PLAutoHelper.SQLHelper.Execute(() =>
                        {
                            using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                            {

                                command_query = @"update scrapeusers set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + "' where [UserName]=@UserName and ([status] is null or [status]='')";

                                using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                {
                                    command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = profile.username;
                                    var affected_row = command.ExecuteNonQuery();

                                    sqlTransaction.Commit();

                                    if (affected_row == 1)
                                    {
                                    }
                                    else
                                    {
                                        profile = null;
                                    }

                                }
                            }
                        }/*, "scrapeusers"*/);

                        if (profile != null)
                            break;
                    }
                    else
                        break;
                }
            }


            return profile;
        }

        /// <summary>
        /// reengage by liking user post with target_account was following or newliking before
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        //static private dynamic WWProject_ReEngage_Liking(string accountId, bool is_take_data, int last_x_days = 10)
        //{
        //    dynamic choiced_target_account = null;

        //    List<dynamic> target_accounts = new List<dynamic>();

        //    var command_query =
        //        @"select a1.target_account, al.do_at
        //        from actionlog a1 OUTER APPLY
	       //         (--muc dich lay nhung target_account lam likeuserpost_task_re tu xa toi gan, tu chua lam toi co lam
		      //          select top 1 *
		      //          from actionlog a2
		      //          where
			     //           a2.accountid=@accountId and
			     //           a2.target_account=a1.target_account and
			     //           a2.[action] like 'likeuserpost_task%'
		      //          order by id desc
	       //         ) as al
        //        where 
	       //         (a1.[action]='following_task' or a1.[action]='likeuserpost_task') and
	       //         a1.[accountid]=@accountId and
	       //         DATEDIFF(day,a1.do_at,getdate())<@last_x_days
        //        order by  al.do_at asc, a1.do_at asc";

        //    using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
        //    {
        //        conn.Open();

        //        using (var command = new SqlCommand(command_query, conn))
        //        {
        //            command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
        //            command.Parameters.Add("@last_x_days", System.Data.SqlDbType.Int).Value = last_x_days;

        //            using (var dataReader = command.ExecuteReader())
        //            {
        //                while (dataReader.Read())
        //                {
        //                    target_accounts.Add(new
        //                    {
        //                        target_account = dataReader["target_account"].ToString(),
        //                        do_at = dataReader["do_at"] == null ? "" : dataReader["do_at"].ToString()
        //                    });

        //                }
        //            }
        //        }
        //    }

        //    using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
        //    {
        //        conn.Open();
        //        //check tung account
        //        foreach (var t in target_accounts)
        //        {
        //            //get data from scrape users to check
        //            command_query =
        //                @"select *
        //            from scrapeusers
        //            where [UserName]=@target_account";

        //            using (var command = new SqlCommand(command_query, conn))
        //            {
        //                command.Parameters.Add("@target_account", System.Data.SqlDbType.VarChar).Value = t.target_account.ToString();
        //                using (var dataReader = command.ExecuteReader())
        //                {
        //                    dataReader.Read();

        //                    bool IsPrivate = bool.Parse(dataReader["IsPrivate"].ToString());
        //                    int FeedCount = dataReader["FeedCount"] == null ? 0 : int.Parse(dataReader["FeedCount"].ToString());
        //                    DateTime RecentPost = DateTime.Parse(dataReader["RecentPost"].ToString());

        //                    if (IsPrivate)
        //                        continue;

        //                    if (FeedCount == 0)
        //                        continue;

        //                    //toi buoc nay tuc la co bai viet
        //                    if (t.do_at == "")
        //                    {
        //                        //tuc la chua re-like lần nào --> choose it
        //                        choiced_target_account = new
        //                        {
        //                            pk = dataReader["pk"].ToString(),
        //                            username = t.target_account
        //                        };
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        //check if has new post to like
        //                        DateTime do_at = DateTime.Parse(t.do_at);

        //                        if (DateTime.Compare(do_at, RecentPost) < 0) //do_at earlier than RecentPost
        //                        {
        //                            choiced_target_account = new
        //                            {
        //                                pk = dataReader["pk"].ToString(),
        //                                username = t.target_account
        //                            };

        //                            break;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    return choiced_target_account;
        //}

        static private dynamic WWProject_ReEngage_Liking(string accountId, bool is_take_data, int last_x_days = 10)
        {
            dynamic choiced_target_account = null;

            List<dynamic> target_accounts_new_action = new List<dynamic>();
            List<dynamic> target_accounts_reengage_action = new List<dynamic>();

            List<dynamic> target_accounts_merge = new List<dynamic>();

            //get list target account that done by (following_task, likeuserpost_task_new) and datediff < last_x_days
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();
                var command_query =
                @"select *
                from actionlog
                where
	                ([action]='following_task' or [action]='likeuserpost_task_new') and
	                [accountid]=@accountId and
	                DATEDIFF(day,do_at,getdate())<@last_x_days
                order by do_at asc";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                    command.Parameters.Add("@last_x_days", System.Data.SqlDbType.Int).Value = last_x_days;

                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            target_accounts_new_action.Add(new
                            {
                                action = dataReader["action"].ToString(),
                                target_account = dataReader["target_account"].ToString(),
                                do_at = DateTime.Parse(dataReader["do_at"].ToString())
                            });
                        }
                    }
                }


                //get list target account that done by (likeuserpost_task_re)

                command_query =
                    @"select *
                from actionlog
                where
	                ([action]='likeuserpost_task_re') and
	                [accountid]=@accountId
                order by do_at desc";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;

                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            target_accounts_reengage_action.Add(new
                            {
                                action = dataReader["action"].ToString(),
                                target_account = dataReader["target_account"].ToString(),
                                do_at = DateTime.Parse(dataReader["do_at"].ToString())
                            });
                        }
                    }
                }

                //mapping 2 list, use do_at of reengage to override do_at in new action, if don't have reengage action, user do_at in new action and sort by asc

                foreach (var item in target_accounts_new_action)
                {
                    var action = item.action;

                    var target_account = item.target_account;

                    var do_at = item.do_at;

                    //check if has in reengage, if yes, use do_at

                    var target_account_reengage_items = target_accounts_reengage_action.Where(t => t.target_account == target_account).OrderByDescending(t => t.do_at).ToList(); //lay gia tri gần nhat neu co

                    if (target_account_reengage_items.Count > 0)
                    {
                        var target_account_reengage = target_account_reengage_items.First();
                        if (target_account_reengage != null)
                        {
                            action = target_account_reengage.action;
                            do_at = target_account_reengage.do_at;
                        }
                    }

                    target_accounts_merge.Add(new
                    {
                        action,
                        target_account,
                        do_at
                    });
                }
            }

            target_accounts_merge = target_accounts_merge.OrderBy(t => t.do_at).ToList();

            //check tung account in target_accounts_merge

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                foreach (var t in target_accounts_merge)
                {
                    //get data from scrape users to check
                    var command_query =
                        @"select [pk],IsPrivate,FeedCount,RecentPost
                    from scrapeusers
                    where [UserName]=@target_account";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@target_account", System.Data.SqlDbType.VarChar).Value = t.target_account.ToString();
                        using (var dataReader = command.ExecuteReader())
                        {
                            dataReader.Read();

                            bool IsPrivate = bool.Parse(dataReader["IsPrivate"].ToString());
                            int FeedCount = dataReader["FeedCount"] == null ? 0 : int.Parse(dataReader["FeedCount"].ToString());
                            DateTime RecentPost = DateTime.Parse(dataReader["RecentPost"].ToString());

                            if (IsPrivate)
                                continue;

                            if (FeedCount == 0)
                                continue;

                            //toi buoc nay tuc la co bai viet

                            if(t.action!= "likeuserpost_task_re")
                            {
                                //tuc la chua re-like lần nào --> choose it
                                choiced_target_account = new
                                {
                                    pk = dataReader["pk"].ToString(),
                                    username = t.target_account
                                };
                                break;
                            }
                            else
                            {
                                //check if has new post to like
                                DateTime do_at = t.do_at;

                                if (DateTime.Compare(do_at, RecentPost) < 0) //do_at earlier than RecentPost
                                {
                                    choiced_target_account = new
                                    {
                                        pk = dataReader["pk"].ToString(),
                                        username = t.target_account
                                    };

                                    //k can claim vi account chi duoc su dung boi accountId

                                    break;
                                }
                            }
                        }
                    }
                }
            }

            return choiced_target_account;

        }

        #endregion

        #region --Sunstore95--

        static private dynamic Sunstore95_Project(string accountId, string type, bool is_take_data)
        {
            switch (type)
            {
                case "reengage_liking":
                default:
                    throw new System.ArgumentException("Not found this type");
            }
        }

        static private dynamic Sunstore95_reengage_liking(string accountId, string type, bool is_take_data)
        {
            dynamic choiced_target_account = null;

            //get list target_account meet requirement to reengage
            string command_query =
                @"SELECT [target_account], do_at
                FROM actionlog a1
                WHERE a1.[accountid]=@accountId
                AND a1.[action]='following_task'
                AND a1.target_account IS NOT NULL
				AND DATEDIFF(day,do_at,getdate())<60 --2months
				AND (select count(*) --must be public
					from igtoolkit.dbo.scrapeusers
					where
						[UserName] COLLATE Latin1_General_CI_AS = (cast(a1.[target_account] as varchar))
						and [IsPrivate]='False'
						and [MediaCount]>0
						) =1
                order by do_at asc";

            List<dynamic> target_accounts = new List<dynamic>();

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using(var command=new SqlCommand(command_query,conn))
                {
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                    using(var dataReader=command.ExecuteReader())
                    {
                        while(dataReader.Read())
                        {
                            target_accounts.Add(new
                            {
                                UserName = dataReader["target_account"].ToString(),
                                do_at = DateTime.Parse(dataReader["do_at"].ToString())
                            });
                        }
                    }
                }

            }
            //get addition data RecentPost, likeuserpost and orderby likeuserpost, do_at

            List<dynamic> reengage_target_accounts = new List<dynamic>();

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                foreach (var item in target_accounts)
                {
                    command_query =
                        @"select *
                        from scrapeusers
                        where
	                        [UserName]=@UserName";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            dataReader.Read();

                            var RecentPost = dataReader["RecentPost"];

                            if (RecentPost == null)
                                continue;
                            else
                            {
                                if (RecentPost.ToString() == "")
                                    continue;
                            }

                            var likeuserpost = dataReader["likeuserpost"];

                            DateTime likeuserpost_dt = new DateTime();

                            if (likeuserpost != null)
                            {
                                if (likeuserpost.ToString() != "")
                                    likeuserpost_dt = DateTime.Parse(likeuserpost.ToString());
                            }

                            reengage_target_accounts.Add(new
                            {
                                UserName = item.UserName,
                                do_at = item.do_at,
                                RecentPost = DateTime.Parse(RecentPost.ToString()),
                                likeuserpost = likeuserpost_dt
                            });
                        }
                    }
                }
            }

            //order by
            reengage_target_accounts = reengage_target_accounts.OrderBy(a => a.likeuserpost).ThenBy(a => a.do_at).ToList();

            if(reengage_target_accounts.Count>0)
            {
                choiced_target_account = new
                {
                    UserName = reengage_target_accounts[0].UserName.ToString()
                };
            }

            return choiced_target_account;
        }


        #endregion
    }
}
