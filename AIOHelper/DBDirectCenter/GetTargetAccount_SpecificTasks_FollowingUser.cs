﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.DBDirectCenter
{
    static class GetTargetAccount_SpecificTasks_FollowingUser
    {
        static public dynamic RunByCenter(string accountId, string project, string type, bool is_take_data)
        {
            switch (project)
            {
                case "ww":
                    return WWProject(accountId, type, is_take_data);
                case "sunstore95":
                    return Sunstore95_Project(accountId, type, is_take_data);
                case "vbuck":
                    return vbuck_Project(accountId, type, is_take_data);
                case "weightloss":
                    return weightloss_Project(accountId, type, is_take_data);
                default:
                    throw new System.ArgumentException("Not found this project");
            }
        }


        #region --WWProject--

        static private dynamic WWProject(string accountId, string type, bool is_take_data)
        {
            switch(type)
            {
                case "default":
                    return FollowUser_WW_Default(accountId, is_take_data);
                default:
                    throw new System.ArgumentException("Not found this type");
            }
        }

        static private dynamic FollowUser_WW_Default(string accountId, bool is_take_data)
        {
            string command_query = "";

            dynamic profile = null;

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();


                while (true)
                {
                    command_query =
                        @"select top 100 *
                            from scrapeusers
                            where
	                            [state]='READY' and
	                            ([status] is null or [status] not like 'Claimed%') and
	                            ([detect_gender]='male') and
	                            ([FollowingCount]<1000) and
	                            ([FollowerCount]<1000) and
	                            [following] is null and
	                            [likeuserpost] is null AND 
								        'https://www.instagram.com/' +[from_account]+'/' IN
																	        (SELECT [ig_url]
																	         FROM target_accounts
																	         WHERE [state]='')
                                order by first_added asc";

                    List<dynamic> temp_profiles = new List<dynamic>();

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var command = new SqlCommand(command_query, conn))
                        {
                            using (var dataReader = command.ExecuteReader())
                            {

                                if (dataReader.HasRows)
                                {
                                    while (dataReader.Read())
                                    {

                                        var pk = long.Parse(dataReader["pk"].ToString());
                                        var username = dataReader["UserName"].ToString();

                                        temp_profiles.Add(new
                                        {
                                            pk,
                                            username
                                        });
                                    }
                                }
                            }
                        }
                    }/*, "scrapeusers"*/);

                    if (temp_profiles.Count > 0)
                    {
                        profile = temp_profiles.OrderBy(a => Guid.NewGuid()).First();
                    }

                    if (profile != null && is_take_data)
                    {
                        command_query =
                            @"update scrapeusers set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + "' where [UserName]=@UserName and ([status] is null or [status]='')";

                        PLAutoHelper.SQLHelper.Execute(() =>
                        {
                            using (var sqlTransaction = conn.BeginTransaction())
                            {
                                using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                {
                                    command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = profile.username;
                                    var affected_row = command.ExecuteNonQuery();

                                    sqlTransaction.Commit();
                                    if (affected_row == 1)
                                    {
                                    }
                                    else
                                    {
                                        profile = null;
                                    }
                                }
                            }
                        }/*, "scrapeusers"*/);
                        

                        if (profile != null)
                            break;
                    }
                    else
                        break;
                }

            }

            return profile;
        }

        #endregion

        #region --Sunstore95--
        static private dynamic Sunstore95_Project(string accountId, string type, bool is_take_data)
        {
            switch (type)
            {
                case "default":
                    return FollowUser_Sunstore95_Default(accountId, is_take_data);
                case "education_photo":
                    return FollowUser_Sunstore95_education_photo(accountId, is_take_data);
                default:
                    throw new System.ArgumentException("Not found this type");
            }
        }

        static private List<dynamic> FollowUser_Sunstore95_Default_target_accounts = new List<dynamic>();

        static private dynamic FollowUser_Sunstore95_Default(string accountId, bool is_take_data)
        {
            string command_query = "";

            dynamic profile = null;

            lock ("FollowUser_Sunstore95_Default")
            {

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();


                    while (true)
                    {

                        if (FollowUser_Sunstore95_Default_target_accounts.Count > 0)
                        {
                            profile = FollowUser_Sunstore95_Default_target_accounts.First();
                        }
                        else
                        {
                            command_query =
                                @"select top 1000 [pk],[UserName]
                        from scrapeusers
                        where
	                        [state]='READY' and
	                        ([status] is null or [status] not like 'Claimed%') and
	                        ([following] is null and likeuserpost is null) and
	                        [FollowingCount]<1000 and
	                        [Biography] not like '%shop%' and
	                        [Biography] not like '%direct%' and
	                        [Biography] not like '%cod%' and
	                        [Biography] not like '%zalo%' and
	                        [Biography] not like '%order%' and
	                        [Biography] not like '%fb%' and
	                        [Biography] not like '%facebook%'
                        order by first_added desc";

                            PLAutoHelper.SQLHelper.Execute(() =>
                            {
                                using (var command = new SqlCommand(command_query, conn))
                                {
                                    using (var dataReader = command.ExecuteReader())
                                    {

                                        if (dataReader.HasRows)
                                        {
                                            while (dataReader.Read())
                                            {

                                                var pk = long.Parse(dataReader["pk"].ToString());
                                                var username = dataReader["UserName"].ToString();

                                                FollowUser_Sunstore95_Default_target_accounts.Add(new
                                                {
                                                    pk,
                                                    username
                                                });
                                            }
                                        }
                                    }
                                }
                            }/*, "following"*/);

                            if (FollowUser_Sunstore95_Default_target_accounts.Count > 0)
                            {

                                FollowUser_Sunstore95_Default_target_accounts = FollowUser_Sunstore95_Default_target_accounts.OrderBy(a => Guid.NewGuid()).ToList();

                                profile = FollowUser_Sunstore95_Default_target_accounts.First();
                            }
                        }

                        if (profile != null && is_take_data)
                        {
                            command_query =
                                @"update scrapeusers set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + "' where [UserName]=@UserName and ([status] is null or [status]='') and ([following] is null or [following]='')";

                            PLAutoHelper.SQLHelper.Execute(() =>
                            {
                                using (var sqlTransaction = conn.BeginTransaction())
                                {
                                    using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    {
                                        command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = profile.username;
                                        var affected_row = command.ExecuteNonQuery();

                                        sqlTransaction.Commit();

                                        //remove from list event it's claimed by this pc_devices or another devices

                                        FollowUser_Sunstore95_Default_target_accounts.RemoveAll(x => x.username == profile.username);

                                        if (affected_row == 1)
                                        {
                                        }
                                        else
                                        {
                                            profile = null;
                                        }
                                    }
                                }
                            }/*, "scrapeusers"*/);


                            if (profile != null)
                                break;
                        }
                        else
                            break;
                    }

                }
            }

            return profile;
        }



        static private dynamic FollowUser_Sunstore95_education_photo(string accountId, bool is_take_data)
        {
            //Lấy account thỏa điều kiện
            //- Đã follow bởi niche ldshop
            //-Không được followback
            //-Đã follow hơn 10 days
            //- Sort từ xa tới gần

            string command_query = "";

            dynamic profile = null;

            string choiced_user = null;

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                //get list account already done following_task, [value]=ldshop

                List<string> target_accounts = new List<string>();

                command_query =
                    @"select [target_account]
                      from actionlog a1
                      where
	                        [action]='following_task' and
	                        [value]='ldshop' and
	                        DATEDIFF(day,do_at,getdate())>10 and  --da follow > 10 days
	                        ([target_account] is not null and [target_account]!='')and
	                        (select [status] --chua bi claim boi account khac
	                        from igtoolkit.dbo.scrapeusers
	                        where
		                        [UserName] COLLATE Latin1_General_CI_AS =a1.target_account)=''
                     order by id asc";

                using (var command = new SqlCommand(command_query, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {

                        while (dataReader.Read())
                        {
                            target_accounts.Add(dataReader["target_account"].ToString());
                        }
                    }
                }

                List<string> excluded_target_accounts = new List<string>();

                //get list account following_task, education_photo hoac target_account of followback

                command_query =
                    @"select [target_account]
                        from actionlog
                        where
	                        (([action]='following_task' and
	                        [value]='education_photo') or
							(
								[action]='followback'
							)) and
	                        ([target_account] is not null and [target_account]!='')";

                using (var command = new SqlCommand(command_query, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {

                        while (dataReader.Read())
                        {
                            excluded_target_accounts.Add(dataReader["target_account"].ToString());
                        }
                    }
                }

                //except list and take 100 oldest account
                target_accounts = target_accounts.Except(excluded_target_accounts).Take(100).ToList();

                //shuffel

                var final_accounts = target_accounts.OrderBy(a => Guid.NewGuid()).ToList();

                if (final_accounts.Count > 0 && is_take_data)
                {

                    foreach (var f in final_accounts)
                    {


                        command_query =
                            @"update igtoolkit.dbo.scrapeusers set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + "' where [UserName]=@UserName and ([status] is null or [status]='')";


                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = f;
                            var affected_row = command.ExecuteNonQuery();

                            if (affected_row == 1)
                            {
                                choiced_user = f;
                            }
                        }


                        if (choiced_user != null)
                            break;
                    }
                }
                else
                {
                    choiced_user = final_accounts[0];
                }

                if (choiced_user != null)
                {
                    command_query =
                        @"select *
                        from igtoolkit.dbo.scrapeusers
                        where
	                        [UserName]=@UserName";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = choiced_user;
                        using (var dataReader = command.ExecuteReader())
                        {
                            dataReader.Read();

                            profile = new
                            {
                                pk = dataReader["pk"].ToString(),
                                username = choiced_user
                            };
                        }
                    }
                }
            }

            return profile;
        }

        #endregion

        #region --vbuck--

        static private dynamic vbuck_Project(string accountId, string type, bool is_take_data)
        {
            switch (type)
            {
                case "default":
                    return FollowUser_vbuck_Default(accountId, is_take_data);
                default:
                    throw new System.ArgumentException("Not found this type");
            }
        }

        static private dynamic FollowUser_vbuck_Default(string accountId, bool is_take_data)
        {
            string command_query = "";

            dynamic profile = null;

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();


                while (true)
                {
                    command_query =
                        @"SELECT top 100 *
                    FROM scrapeusers
                    WHERE [FollowingCount]<1000
                      AND [FollowerCount]<1000
                      AND ([following] is null or [following]='')
                      AND ([likeuserpost] is null or [likeuserpost]='')
                      AND ([status] is null or [status]!='Claimed')
					  AND [UserName] not like '%fortnite%'
					  AND [Biography] not like '%fortnite%'
                      AND 'https://www.instagram.com/' +[from_account]+'/' IN
																	        (SELECT [ig_url]
																	         FROM target_accounts
																	         WHERE [state]='' or [state] is null)
                    order by first_added desc";

                    List<dynamic> temp_profiles = new List<dynamic>();

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var command = new SqlCommand(command_query, conn))
                        {
                            using (var dataReader = command.ExecuteReader())
                            {

                                if (dataReader.HasRows)
                                {
                                    while (dataReader.Read())
                                    {

                                        var pk = long.Parse(dataReader["pk"].ToString());
                                        var username = dataReader["UserName"].ToString();

                                        temp_profiles.Add(new
                                        {
                                            pk,
                                            username
                                        });
                                    }
                                }
                            }
                        }
                    }/*, "scrapeusers"*/);

                    if (temp_profiles.Count > 0)
                    {
                        profile = temp_profiles.OrderBy(a => Guid.NewGuid()).First();
                    }

                    if (profile != null && is_take_data)
                    {
                        command_query =
                            @"update scrapeusers set [status]='Claimed' where [UserName]=@UserName and ([status] is null or [status]='')";

                        PLAutoHelper.SQLHelper.Execute(() =>
                        {
                            using (var sqlTransaction = conn.BeginTransaction())
                            {
                                using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                {
                                    command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = profile.username;
                                    var affected_row = command.ExecuteNonQuery();

                                    sqlTransaction.Commit();
                                    if (affected_row == 1)
                                    {
                                    }
                                    else
                                    {
                                        profile = null;
                                    }
                                }
                            }
                        }/*, "scrapeusers"*/);


                        if (profile != null)
                            break;
                    }
                    else
                        break;
                }

            }

            return profile;
        }


        #endregion

        #region --weightloss--

        static private dynamic weightloss_Project(string accountId, string type, bool is_take_data)
        {
            switch (type)
            {
                case "default":
                    return FollowUser_weightloss_Default(accountId, is_take_data);
                default:
                    throw new System.ArgumentException("Not found this type");
            }
        }

        static private List<dynamic> FollowUser_weightloss_Default_target_accounts = new List<dynamic>();

        static private dynamic FollowUser_weightloss_Default(string accountId, bool is_take_data)
        {
            string command_query = "";

            dynamic profile = null;

            lock ("FollowUser_weightloss_Default")
            {

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();


                    while (true)
                    {

                        if (FollowUser_weightloss_Default_target_accounts.Count > 0)
                        {
                            profile = FollowUser_weightloss_Default_target_accounts.First();
                        }
                        else
                        {
                            //filter country
                            //command_query =
                            //    @"SELECT top 1000 *
                            //        FROM scrapeusers
                            //        WHERE [FollowingCount]<1000
                            //          AND [FollowerCount]<1000
                            //          AND ([following] is null or [following]='')
                            //          AND ([likeuserpost] is null or [likeuserpost]='')
                            //          AND ([status] is null or [status] not like 'Claimed%')
                            //    order by first_added desc";

                            command_query =
                                @"SELECT top 1000 *
                                    FROM scrapeusers
                                    WHERE [FollowingCount]<2000
                                      AND [FollowerCount]<2000
                                      AND ([following] is null or [following]='')
                                      AND ([likeuserpost] is null or [likeuserpost]='')
                                      AND ([status] is null or [status] not like 'Claimed%')
									  AND
									  (
										[detect_location] like '%US%' or 
										[detect_location] like '%DE%' or
										[detect_location] like '%GB%' or 
										[detect_location] like '%IT%' or 
										[detect_location] like '%FR%' or 
										[detect_location] like '%KR%' or 
										[detect_location] like '%CO%' or 
										[detect_location] like '%ES%' or 
										[detect_location] like '%CA%' or 
										[detect_location] like '%PL%' or 
										[detect_location] like '%AU%' or 
										[detect_location] like '%TW%' or 
										[detect_location] like '%NL%' or 
										[detect_location] like '%BE%' or 
										[detect_location] like '%CZ%' or 
										[detect_location] like '%SE%' or 
										[detect_location] like '%AT%' or 
										[detect_location] like '%IL%' or 
										[detect_location] like '%NI%' or 
										[detect_location] like '%DK%' or 
										[detect_location] like '%FI%' or 
										[detect_location] like '%NO%' or 
										[detect_location] like '%SK%' or 
										[detect_location] like '%IE%' or 
										[detect_location] like '%NZ%' or 
										[detect_location] like '%HR%' or 
										[detect_location] like '%GE%' or 
										[detect_location] like '%LT%' or 
										[detect_location] like '%SI%' or 
										[detect_location] like '%LU%'
									  ) and
									  (detect_gender='female' or [detect_gender]='unknown')
	                                AND [state]='READY'
                                order by first_added desc";

                            PLAutoHelper.SQLHelper.Execute(() =>
                            {
                                using (var command = new SqlCommand(command_query, conn))
                                {
                                    using (var dataReader = command.ExecuteReader())
                                    {

                                        if (dataReader.HasRows)
                                        {
                                            while (dataReader.Read())
                                            {

                                                var pk = long.Parse(dataReader["pk"].ToString());
                                                var username = dataReader["UserName"].ToString();

                                                FollowUser_weightloss_Default_target_accounts.Add(new
                                                {
                                                    pk,
                                                    username
                                                });
                                            }
                                        }
                                    }
                                }
                            }/*, "following"*/);

                            if (FollowUser_weightloss_Default_target_accounts.Count > 0)
                            {

                                FollowUser_weightloss_Default_target_accounts = FollowUser_weightloss_Default_target_accounts.OrderBy(a => Guid.NewGuid()).ToList();

                                profile = FollowUser_weightloss_Default_target_accounts.First();
                            }
                        }

                        if (profile != null && is_take_data)
                        {
                            command_query =
                                @"update scrapeusers set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + "' where [UserName]=@UserName and ([status] is null or [status]='') and ([following] is null or [following]='')";

                            PLAutoHelper.SQLHelper.Execute(() =>
                            {
                                using (var sqlTransaction = conn.BeginTransaction())
                                {
                                    using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    {
                                        command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = profile.username;
                                        var affected_row = command.ExecuteNonQuery();

                                        sqlTransaction.Commit();

                                        //remove from list event it's claimed by this pc_devices or another devices

                                        FollowUser_weightloss_Default_target_accounts.RemoveAll(x => x.username == profile.username);

                                        if (affected_row == 1)
                                        {
                                        }
                                        else
                                        {
                                            profile = null;
                                        }
                                    }
                                }
                            }/*, "scrapeusers"*/);


                            if (profile != null)
                                break;
                        }
                        else
                            break;
                    }

                }
            }

            return profile;
        }

        #endregion
    }
}
