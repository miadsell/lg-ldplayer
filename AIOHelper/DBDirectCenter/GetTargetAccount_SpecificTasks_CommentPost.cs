﻿using InstagramApiSharp.Classes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.DBDirectCenter
{
    class GetTargetAccount_SpecificTasks_CommentPost
    {
        static public dynamic RunByCenter(string accountId, string project, string data_type, bool is_take_data)
        {
            switch (project)
            {
                case "sunstore95":
                    return Sunstore95_Project(accountId, data_type, is_take_data);
                default:
                    throw new System.ArgumentException("Not found this project");
            }
        }

        #region --sunstore95--

        static private dynamic Sunstore95_Project(string accountId, string data_type, bool is_take_data)
        {
            switch (data_type)
            {
                case "comment_personal_profile":
                    return Sunstore95_comment_personal_profile(accountId, is_take_data);
                    break;
                default:
                    throw new System.ArgumentException("Not found this data_type CommentPost");
            }
        }


        // static private dynamic Sunstore95_comment_personal_profile(string accountId, bool is_take_data)
        // {
        //     //Lấy account thỏa điều kiện
        //     //- Đã follow bởi niche ldshop
        //     //-Không được followback
        //     //-Đã follow hơn 10 days
        //     //- Sort từ xa tới gần
        //     //- Chua do commentpost

        //     string command_query = "";

        //     dynamic data = new ExpandoObject();

        //     string choiced_user = null;

        //     using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
        //     {
        //         conn.Open();

        //         //get list account already done following_task

        //         List<string> target_accounts = new List<string>();

        //         command_query =
        //             @"select [UserName]
        //                 from igtoolkit.dbo.scrapeusers
        //                 where
        //                  [state]='READY' and
        //[following]='Done' and
        //[IsPrivate]='False' and
        //[MediaCount]>0 and
        //([status] is null or [status]='')
        //                 order by first_added asc";

        //         using (var command = new SqlCommand(command_query, conn))
        //         {
        //             using (var dataReader = command.ExecuteReader())
        //             {

        //                 while (dataReader.Read())
        //                 {
        //                     target_accounts.Add(dataReader["UserName"].ToString());
        //                 }
        //             }
        //         }

        //         List<string> followback_target_accounts = new List<string>();
        //         //get list account followback
        //         command_query =
        //             @"select [target_account]
        //             from actionlog
        //             where
        //              [action]='followback'";

        //         using (var command = new SqlCommand(command_query, conn))
        //         {
        //             using (var dataReader = command.ExecuteReader())
        //             {

        //                 while (dataReader.Read())
        //                 {
        //                     followback_target_accounts.Add(dataReader["target_account"].ToString());
        //                 }
        //             }
        //         }
        //         target_accounts = target_accounts.Except(followback_target_accounts).ToList();

        //         //get list account done commentpost, [value]='zalo_group
        //         List<string> excluded_target_accounts = new List<string>();

        //         command_query =
        //             @"select [target_account]
        //                 from actionlog
        //                 where
        //                  ([action]='commentpost_task' and
        //                  [value]='zalo_group') and
        //                  ([target_account] is not null and [target_account]!='')";

        //         using (var command = new SqlCommand(command_query, conn))
        //         {
        //             using (var dataReader = command.ExecuteReader())
        //             {

        //                 while (dataReader.Read())
        //                 {
        //                     excluded_target_accounts.Add(dataReader["target_account"].ToString());
        //                 }
        //             }
        //         }

        //         //except list and take 100 oldest account
        //         target_accounts = target_accounts.Except(excluded_target_accounts).Take(100).ToList();

        //         //shuffel

        //         var final_accounts = target_accounts.OrderBy(a => Guid.NewGuid()).ToList();


        //         if (final_accounts.Count > 0 && is_take_data)
        //         {
        //             foreach (var f in final_accounts)
        //             {


        //                 command_query =
        //                     @"update igtoolkit.dbo.scrapeusers set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + "' where [UserName]=@UserName and ([status] is null or [status]='')";


        //                 using (var command = new SqlCommand(command_query, conn))
        //                 {
        //                     command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = f;
        //                     var affected_row = command.ExecuteNonQuery();

        //                     if (affected_row == 1)
        //                     {
        //                         choiced_user = f;
        //                     }
        //                 }


        //                 if (choiced_user != null)
        //                 {
        //                     //use igscraper api to scrape latest post

        //                     var dyn = IGHelpers.Helpers.APIObJWithProxy();

        //                     InstagramApiSharp.Classes.Models.InstaMediaList medias = new InstagramApiSharp.Classes.Models.InstaMediaList();

        //                     int retry = 0;


        //                 ScrapeMediasFromUser:
        //                     IResult<InstagramApiSharp.Classes.Models.InstaMediaList> medias_result = IGAPIHelper.Scraper.ScrapeMediasFromUser(dyn.instaApi, choiced_user);


        //                     if (medias_result.Succeeded == false)
        //                     {
        //                         //check if user is set to private

        //                         if (medias_result.Info.Message == "Not authorized to view user")
        //                         {
        //                             //set private => update to scrapeusers igtoolkit
        //                             using (SqlConnection conn_igtoolkit = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
        //                             {
        //                                 conn_igtoolkit.Open();

        //                                 PLAutoHelper.SQLHelper.Execute(() =>
        //                                 {
        //                                     using (SqlTransaction sqlTransaction = conn_igtoolkit.BeginTransaction())
        //                                     {
        //                                         using (var command = new SqlCommand("update scrapeusers set [IsPrivate]='True', [status]='' where [UserName]=@UserName", conn_igtoolkit, sqlTransaction))
        //                                         {
        //                                             command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = f;
        //                                             command.ExecuteNonQuery();
        //                                         }

        //                                         sqlTransaction.Commit();
        //                                     }
        //                                 }/*, "scrapeusers"*/);
        //                             }


        //                             goto NextData;
        //                         }

        //                         //check if user is remove
        //                         var userinfo = IGAPIHelper.Scraper.ScrapeFullUserInfo_ByUsername(choiced_user, dyn.instaApi);

        //                         var userinfo_status = ADBSupport.ADBHelpers.GetDynamicProperty(userinfo, "status");

        //                         if (userinfo_status == "NotFoundUser")
        //                         {
        //                             //update state ERROR
        //                             using (SqlConnection conn_igtoolkit = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
        //                             {
        //                                 conn_igtoolkit.Open();

        //                                 PLAutoHelper.SQLHelper.Execute(() =>
        //                                 {
        //                                     using (SqlTransaction sqlTransaction = conn_igtoolkit.BeginTransaction())
        //                                     {
        //                                         using (var command = new SqlCommand("update scrapeusers set [state]='ERROR', [status]='' where [UserName]=@UserName", conn_igtoolkit, sqlTransaction))
        //                                         {
        //                                             command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = f;
        //                                             command.ExecuteNonQuery();
        //                                         }

        //                                         sqlTransaction.Commit();
        //                                     }
        //                                 }/*, "scrapeusers"*/);
        //                             }

        //                             goto NextData;
        //                         }
        //                         else
        //                         {
        //                             retry++;
        //                             if (retry < 3)
        //                             {
        //                                 Thread.Sleep(2000);
        //                                 goto ScrapeMediasFromUser;
        //                             }
        //                             else
        //                             {
        //                                 throw new System.ArgumentException("UnknownError|" + userinfo_status);
        //                             }
        //                         }
        //                     }
        //                     else
        //                     {
        //                         medias = medias_result.Value;

        //                         if (medias.Count == 0)
        //                         {
        //                             //throw new System.ArgumentException("UnknownWhyMediasIsEmpty");

        //                             //update state ERROR
        //                             using (SqlConnection conn_igtoolkit = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
        //                             {
        //                                 conn_igtoolkit.Open();

        //                                 PLAutoHelper.SQLHelper.Execute(() =>
        //                                 {
        //                                     using (SqlTransaction sqlTransaction = conn_igtoolkit.BeginTransaction())
        //                                     {
        //                                         using (var command = new SqlCommand("update scrapeusers set MediaCount=0 where [UserName]=@UserName", conn_igtoolkit, sqlTransaction))
        //                                         {
        //                                             command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = f;
        //                                             command.ExecuteNonQuery();
        //                                         }

        //                                         sqlTransaction.Commit();
        //                                     }
        //                                 }/*, "scrapeusers"*/);
        //                             }

        //                             goto NextData;
        //                         }

        //                         //get latest post link
        //                         var posturl = "https://www.instagram.com/p/" + medias[0].Code + "/";


        //                         data.posturl = posturl;
        //                         data.username = f;
        //                     }

        //                     break;
        //                 NextData:
        //                     choiced_user = null;

        //                     continue;
        //                 }
        //             }
        //         }
        //         else
        //         {
        //             choiced_user = final_accounts[0];

        //             data.username = choiced_user;
        //         }
        //     }

        //     return data;
        // }

        static private dynamic Sunstore95_comment_personal_profile(string accountId, bool is_take_data)
        {
            string command_query = "";

            dynamic data = new ExpandoObject();

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                command_query =
                    @"select top 10 *
                    from scrapeuser_comment_posts
                    where
	                    ([status] is null or [status]='')
                    order by scraped_at asc";

                List<dynamic> available_datas = new List<dynamic>();

                using (var command = new SqlCommand(command_query, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while(dataReader.Read())
                        {
                            available_datas.Add(new
                            {
                                posturl = dataReader["posturl"].ToString(),
                                username = dataReader["from_account"].ToString(),
                                id = dataReader["id"].ToString()
                            });
                        }
                    }
                }

                available_datas = available_datas.OrderBy(a => Guid.NewGuid()).ToList();

                if (available_datas.Count > 0)
                {

                    if (is_take_data)
                    {
                        foreach (var d in available_datas)
                        {
                            //try claimed
                            command_query =
                                @"update scrapeuser_comment_posts
                        set [status]=@status
                        where
	                        [id]=@id and
                            ([status] is null or [status]='')";

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = d.id;
                                command.Parameters.Add("@status", System.Data.SqlDbType.VarChar).Value = AIOHelper.DBLogic.HandleClaimed.TakeClaimedText();

                                var affect_row = command.ExecuteNonQuery();

                                if (affect_row == 1)
                                {
                                    data.id = d.id.ToString();
                                    data.posturl = d.posturl.ToString();
                                    data.username = d.username.ToString();

                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        data.id = available_datas[0].id.ToString();
                        data.posturl = available_datas[0].posturl.ToString();
                        data.username = available_datas[0].username.ToString();
                    }


                    return data;
                }
                else
                    return null;
            }

        }

        #endregion
    }
}
