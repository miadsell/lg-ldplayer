﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.DBDirectCenter
{
    static class GetTargetAccount_SpecificTasks_MassDM
    {
        static public string RunByCenter(string accountId, string project, string data_type, bool is_take_data)
        {
            switch (project)
            {
                case "sunstore95":
                    return Sunstore95_Project(accountId, data_type, is_take_data);
                default:
                    throw new System.ArgumentException("Not found this project");
            }
        }

        #region --sunstore95--

        static private string Sunstore95_Project(string accountId, string data_type, bool is_take_data)
        {

            switch (data_type)
            {
                case "welcome_ldshop":
                    return MassDM_sunstore95_welcome_ldshop(accountId, is_take_data);
                case "event_landau":
                    return MassDM_sunstore95_event_landau(accountId, is_take_data);
                case "event_christmas":
                    return MassDM_sunstore95_event_christmas(accountId, is_take_data);
                default:
                    throw new System.ArgumentException("Not found this type");
            }
        }

        /// <summary>
        /// welcome mess for niche:ldshop
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="is_take_data"></param>
        /// <returns></returns>
        static private string MassDM_sunstore95_welcome_ldshop(string accountId, bool is_take_data)
        {
            string choiced_username = null;

            string command_query = null;

            List<string> target_accounts = new List<string>();

            //get list username do_following by accountId and are followback
            //using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
            //{
            //    conn.Open();

            //    command_query =
            //    @"select *
            //    from scrapeusers
            //    where
            //        ([status] is null or [status] not like 'Claimed%') and
	           //     [followback]='yes' and
	           //     [UserName] in
	           //     (select [target_account] COLLATE Latin1_General_CI_AS
	           //     from instagramdb.dbo.actionlog
	           //     where
		          //      [action]='following_task'
		          //      and accountid=@accountId)
            //    order by first_added desc";

            //    using (var command = new SqlCommand(command_query, conn))
            //    {
            //        command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
            //        using (var dataReader = command.ExecuteReader())
            //        {
            //            while (dataReader.Read())
            //            {
            //                target_accounts.Add(dataReader["UserName"].ToString());
            //            }
            //        }
            //    }

            //}

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                command_query =
                @"select [target_account]
                from actionlog
                where
	                    [action]='followback' and
	                    [accountid]=@accountId
                order by do_at desc";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            target_accounts.Add(dataReader["target_account"].ToString());
                        }
                    }
                }

            }

            List<string> accounts_were_sentdm = new List<string>();

            //get list username that do massdm by accountId
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                command_query =
                    @"select *
                    from actionlog
                    where
	                    [action]='massdm_task' and
	                    [accountid]=@accountId";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            accounts_were_sentdm.Add(dataReader["target_account"].ToString());
                        }
                    }
                }
            }

            //except list
            target_accounts = target_accounts.Except(accounts_were_sentdm).ToList();

            if (target_accounts.Count > 0)
                choiced_username = target_accounts[0];

            if (choiced_username != null && is_take_data)
            {
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();
                    command_query =
                                    @"update scrapeusers set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"' where [UserName]=@UserName and ([status] is null or [status]='')";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = choiced_username;
                        var affected_row = command.ExecuteNonQuery();
                    }
                }
            }

            return choiced_username;
        }

        static private string MassDM_sunstore95_event_landau(string accountId, bool is_take_data)
        {
            string choiced_username = null;

            string command_query = null;

            List<string> target_accounts = new List<string>();

            //get list username that was sent dm welcome_ldshop in over 20 days 
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                command_query =
                @"select *
                from actionlog
                where
	                [action]='massdm_task' and
	                [value]=@massdm_type and
	                [accountid]=@accountId and
	                DATEDIFF(day,do_at,getdate())>20
                order by id desc";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                    command.Parameters.Add("@massdm_type", System.Data.SqlDbType.VarChar).Value = "welcome_ldshop";
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            target_accounts.Add(dataReader["target_account"].ToString());
                        }
                    }
                }

            }

            List<string> accounts_were_sentdm_event_landau = new List<string>();

            //get list username was sent dm event_landau
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                command_query =
                    @"select *
                    from actionlog
                    where
	                    [action]='massdm_task' and
	                    [value]=@massdm_type and
	                    [accountid]=@accountId
                    order by id desc";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                    command.Parameters.Add("@massdm_type", System.Data.SqlDbType.VarChar).Value = "event_landau";
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            accounts_were_sentdm_event_landau.Add(dataReader["target_account"].ToString());
                        }
                    }
                }
            }

            //except list
            target_accounts = target_accounts.Except(accounts_were_sentdm_event_landau).ToList();

            if (target_accounts.Count > 0)
                choiced_username = target_accounts[0];

            if (choiced_username != null && is_take_data)
            {
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();
                    command_query =
                                    @"update scrapeusers set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"' where [UserName]=@UserName and ([status] is null or [status]='')";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = choiced_username;
                        var affected_row = command.ExecuteNonQuery();
                    }
                }
            }

            return choiced_username;
        }


        static private string MassDM_sunstore95_event_christmas(string accountId, bool is_take_data)
        {
            string choiced_username = null;

            string command_query = null;

            List<string> target_accounts = new List<string>();

            //get list username that was sent dm welcome_ldshop in over 20 days 
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                command_query =
                @"select *
                from actionlog
                where
	                [action]='massdm_task' and
	                [value]=@massdm_type and
	                [accountid]=@accountId and
	                DATEDIFF(day,do_at,getdate())>20
                order by id desc";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                    command.Parameters.Add("@massdm_type", System.Data.SqlDbType.VarChar).Value = "welcome_ldshop";
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            target_accounts.Add(dataReader["target_account"].ToString());
                        }
                    }
                }

            }

            List<string> accounts_were_sentdm_event_christmas = new List<string>();

            //get list username was sent dm event_christmas
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                command_query =
                    @"select *
                    from actionlog
                    where
	                    [action]='massdm_task' and
	                    [value]=@massdm_type and
	                    [accountid]=@accountId
                    order by id desc";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                    command.Parameters.Add("@massdm_type", System.Data.SqlDbType.VarChar).Value = "event_christmas";
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            accounts_were_sentdm_event_christmas.Add(dataReader["target_account"].ToString());
                        }
                    }
                }
            }

            //except list
            target_accounts = target_accounts.Except(accounts_were_sentdm_event_christmas).ToList();

            if (target_accounts.Count > 0)
                choiced_username = target_accounts[0];

            if (choiced_username != null && is_take_data)
            {
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();
                    command_query =
                                    @"update scrapeusers set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"' where [UserName]=@UserName and ([status] is null or [status]='')";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = choiced_username;
                        var affected_row = command.ExecuteNonQuery();
                    }
                }
            }

            return choiced_username;
        }

        #endregion
    }
}
