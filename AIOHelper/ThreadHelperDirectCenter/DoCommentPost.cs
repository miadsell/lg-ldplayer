﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.ThreadHelperDirectCenter
{
    class DoCommentPost
    {
        static public string RunByCenter(IGDevice iGDevice, dynamic ig_account, dynamic comment)
        {
            var aio_settings = ig_account.aio_settings;

            string project = aio_settings.Project;
            string action_type = aio_settings.Task_CommentPost.action_type;

            switch (project)
            {
                case "sunstore95":
                    switch (action_type)
                    {
                        case "comment_personal_profile":
                            return sunstore95_comment_personal_profile(iGDevice, ig_account, comment);
                        default: throw new System.ArgumentException("Not found this type of DoFollowingUser");
                    }
                default: throw new System.ArgumentException("Not found this project");
            }
        }


        #region --sunstore95--

        static private string sunstore95_comment_personal_profile(IGDevice iGDevice, dynamic ig_account, dynamic comment)
        {
            string deviceName = iGDevice.ld_devicename;
            string deviceID = iGDevice.deviceID;
            var aio_settings = ig_account.aio_settings;
            string comment_text = SharpSpin.Spinner.Spin(aio_settings.Task_CommentPost.comment.ToString());

            try
            {
                comment_text = comment_text.Replace("@user", "@" + comment.username);
            }
            catch(Exception ex)
            {
                throw new System.ArgumentException(ex.Message + Newtonsoft.Json.JsonConvert.SerializeObject(comment));
            }

            //comment = new
            //{
            //    posturl = "https://www.instagram.com/p/CC52g2HsAEh/"
            //};

            int retry = 0;
        //open intent link
        OpenLink:
            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", comment.posturl, "com.instagram.android");

            ADBSupport.ADBHelpers.Delay();

            //make some simulate

            int scroll_times = 0;
            while (true)
            {
                if (!ADBSupport.ADBHelpers.IsImageExisted(deviceID, BMResource.SemiAutoComment.SemiAutoComment_CommentBtn, 1, 1, 1))
                {
                    //scroll down
                    ADBSupport.ADBHelpers.SwipeByPercent_RandomTouch(deviceID, "doc", "22-86", 58.3, 58.3 - 10, 100);
                    scroll_times++;

                    if (scroll_times > 5)
                    {
                        return "CANTCOMMENT";
                    }
                }
                else
                    break;
            }

            ////add comment
            //try
            //{
            //    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, BMResource.SemiAutoComment.SemiAutoComment_PostWasOpen, 2, 5, 2);
            //}
            //catch
            //{
            //    retry++;
            //    if (retry > 3)
            //        throw new System.ArgumentException("Can't open intent");
            //    goto OpenLink;
            //}

            if (!ADBSupport.ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.SemiAutoComment_CommentBtn, 2, 1, 2))
            {
                return "CANTCOMMENT";
            }

            //Click comment icon

            var commentbtn_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.SemiAutoComment_CommentBtn);

            ADBSupport.ADBHelpers.RandomTapImageByPercentage(deviceID, commentbtn_point.x, commentbtn_point.y, "SemiAutoComment_CommentBtn");

            //wait addcomment box

            //ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.SemiAutoComment_AddACommentBox, 5, 5, 3);

            if (!ADBSupport.ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.SemiAutoComment_AddACommentBox, 5, 5, 3))
            {
                //check if limited comment
                if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, BMResource.SemiAutoComment.SemiAutoComment_LimitedComment, 1, 1, 1))
                {
                    return "LimitedComment";
                }
                else
                {
                    throw new System.ArgumentException("Can't wait addcomment box");
                }
            }

            //Send text
            ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID, deviceName, comment_text);

            //Hit post

            var post_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.SemiAutoComment_PostComment);

            ADBSupport.ADBHelpers.RandomTapImageByPercentage(deviceID, post_point.x, post_point.y, "SemiAutoComment_PostComment");

            ADBSupport.ADBHelpers.Delay(5, 10);

            //wait posting disappear

            while (true)
            {
                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.SemiAutoComment_Posting, 1, 1);
                }
                catch
                {
                    break;
                }

                Thread.Sleep(1000);
            }

            //Check if action blocked
            var isaction_blocked = IGHelpers.Helpers.IsActionBlocked(deviceID, deviceName);

            if (isaction_blocked)
            {
                return "Action Blocked";
            }

            return "";
        }



        #endregion
    }
}
