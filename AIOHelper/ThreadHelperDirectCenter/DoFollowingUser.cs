﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.ThreadHelperDirectCenter
{
    static class DoFollowingUser
    {
        //function inside this class must be call only by TheadHelper.DoLikeUserPost
        static public string RunByCenter(IGDevice iGDevice, dynamic ig_account, string target_profile)
        {
            var aio_settings = ig_account.aio_settings;

            string project = aio_settings.Project;
            string action_type = aio_settings.Task_FollowingUser.action_type;

            switch (project)
            {
                case "ww":
                    switch (action_type)
                    {
                        case "default":
                            return WWProject_Default(iGDevice, ig_account, target_profile);
                        default: throw new System.ArgumentException("Not found this type of DoFollowingUser");
                    }
                case "sunstore95":
                    switch (action_type)
                    {
                        case "default":
                            return sunstore95_default(iGDevice, ig_account, target_profile);
                        default: throw new System.ArgumentException("Not found this type of DoFollowingUser");
                    }
                default: throw new System.ArgumentException("Not found this project");
            }
        }

        #region --WWProject--

        static private string WWProject_Default(IGDevice iGDevice, dynamic ig_account, string target_profile)
        {
            string deviceName = iGDevice.ld_devicename;
            string deviceID = iGDevice.deviceID;

            //Open intent user profile

            ADBSupport.LDHelpers.OpenIGProfile(deviceName, target_profile);

            //Check if user not found

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Following_UserNotFound, 1, 2, 0);
                return "user_not_found";
            }
            catch
            {

            }

            ////check if has post

            //if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, BMResource.FollowPeople.FollowPeople_HasPost, 1, 0, 0))
            //{

            //    //Scroll down random some times
            //    var scroll_down = new Action.RandomScroll("down", deviceID);

            //    var scroll_down_times = (new Random()).Next(1, 3);

            //    for (int s = 0; s < scroll_down_times; s++)
            //    {
            //        scroll_down.DoAction();

            //        ADBSupport.ADBHelpers.Delay(1, 3);
            //    }


            //    //Back to top

            //    var scroll_up = new Action.RandomScroll("up", deviceID);

            //    Stopwatch watch = new Stopwatch();
            //    watch.Start();

            //    while (true)
            //    {

            //        if (watch.Elapsed.TotalMinutes > 1)
            //        {
            //            //Check if action blocked
            //            var isaction_blocked = IGHelpers.Helpers.IsActionBlocked(deviceID, deviceName);

            //            if (isaction_blocked)
            //            {
            //                return "Action Blocked";
            //            }

            //            return "";
            //        }

            //        scroll_up.DoAction();

            //        try
            //        {
            //            //scroll until see Follow Btn
            //            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Follow1Btn, 1, 1);
            //            break;
            //        }
            //        catch { }

            //        ADBSupport.ADBHelpers.Delay(1, 2);

            //    }
            //}

            //no scroll, just do following action
            //Do follow

            var follow = new Action.FollowPeople(iGDevice);
            follow.DoAction();

            return "";
        }

        #endregion


        #region --Sunstore95--

        static private string sunstore95_default(IGDevice iGDevice, dynamic ig_account, string target_profile) 
        {
            string deviceName = iGDevice.ld_devicename;
            string deviceID = iGDevice.deviceID;

            //Open intent user profile

            ADBSupport.LDHelpers.OpenIGProfile(deviceName, target_profile);

            //Check if user not found

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Following_UserNotFound, 1, 2, 0);
                return "user_not_found";
            }
            catch
            {

            }

            //Do follow
            

            var follow = new Action.FollowPeople(iGDevice);
            follow.DoAction();

            var isaction_blocked = IGHelpers.Helpers.IsActionBlocked(deviceID, deviceName);

            if (isaction_blocked)
            {
                return "Action Blocked";
            }

            return "";
        }


        #endregion
    }
}
