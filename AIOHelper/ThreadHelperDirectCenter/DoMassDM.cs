﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.ThreadHelperDirectCenter
{
    static class DoMassDM
    {
        //function inside this class must be call only by TheadHelper.DoLikeUserPost
        static public string RunByCenter(IGDevice iGDevice, dynamic ig_account, dynamic target_profile)
        {
            var aio_settings = ig_account.aio_settings;

            string project = aio_settings.Project;
            string Task_MassDM_type = target_profile.Task_MassDM_type;
            string action_type = aio_settings.Task_MassDM[Task_MassDM_type].action_type;

            switch (project)
            {
                case "sunstore95":
                    switch (action_type)
                    {
                        case "default":
                            return sunstore95_Default(iGDevice, ig_account, target_profile);
                        case "event_landau":
                            return sunstore95_event_landau(iGDevice, ig_account, target_profile);
                        case "event_christmas":
                            return sunstore95_event_christmas(iGDevice, ig_account, target_profile);
                        default: throw new System.ArgumentException("Not found this type of DoMassDM");
                    }
                case "plant":
                    switch(action_type)
                    {
                        case "default":
                            return plant_Default(iGDevice, ig_account, target_profile);
                        default: throw new System.ArgumentException("Not found this type of DoMassDM");
                    }
                case "weightloss":
                    switch (action_type)
                    {
                        case "default":
                            return weightloss_Default(iGDevice, ig_account, target_profile);
                        default: throw new System.ArgumentException("Not found this type of DoMassDM");
                    }
                default: throw new System.ArgumentException("Not found this project");
            }
        }

        #region --Sunstore95--

        static private string sunstore95_Default(IGDevice iGDevice, dynamic ig_account, dynamic target_profile)
        {
            string deviceName = iGDevice.ld_devicename;
            string deviceID = iGDevice.deviceID;


            string Task_MassDM_type = target_profile.Task_MassDM_type;
            var aio_settings = ig_account.aio_settings;
            string spin_message = aio_settings.Task_MassDM[Task_MassDM_type].spin_message;

            string message = SharpSpin.Spinner.Spin(spin_message);

            //replace @user, @expired_date

            message = message.Replace("@user", target_profile.user);

            string expired_date = DateTime.Now.AddDays(7).ToString("dd/M/yyyy");

            message = message.Replace("@expired_date", expired_date);

            //Open intent user profile

            ADBSupport.LDHelpers.OpenIGProfile(deviceName, target_profile.user);

            //Check if user not found

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Following_UserNotFound, 1, 2, 0);
                return "user_not_found";
            }
            catch
            {

            }

            var dm_action = new Action.SendDM(iGDevice, message, null, true);
            dm_action.DoAction();

            return "";
        }

        static private string sunstore95_event_christmas(IGDevice iGDevice, dynamic ig_account, dynamic target_profile)
        {
            string deviceName = iGDevice.ld_devicename;
            string deviceID = iGDevice.deviceID;


            string Task_MassDM_type = target_profile.Task_MassDM_type;
            var aio_settings = ig_account.aio_settings;
            string spin_message = aio_settings.Task_MassDM[Task_MassDM_type].spin_message;

            string message = SharpSpin.Spinner.Spin(spin_message);

            //replace @user, @expired_date

            message = message.Replace("@user", target_profile.user);

            string expired_date = DateTime.Now.AddDays(7).ToString("dd/M/yyyy");

            message = message.Replace("@expired_date", expired_date);

            //Open intent user profile

            ADBSupport.LDHelpers.OpenIGProfile(deviceName, target_profile.user);

            //Check if user not found

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Following_UserNotFound, 1, 2, 0);
                return "user_not_found";
            }
            catch
            {

            }

            var dm_action = new Action.SendDM(iGDevice, message);
            dm_action.DoAction();

            return "";
        }

        static private string sunstore95_event_landau(IGDevice iGDevice, dynamic ig_account, dynamic target_profile)
        {
            //set value for @hello_message, @end_message

            var now = DateTime.Now;

            string message_type = null;

            if (now > DateTime.Parse("5:00:00") && now < DateTime.Parse("11:00:00"))
            {
                message_type = "morning";
            }
            else
            if (now >= DateTime.Parse("11:00:00") && now < DateTime.Parse("14:00:00"))
            {
                message_type = "afternoon";
            }
            else
            if (now >= DateTime.Parse("14:00:00") && now < DateTime.Parse("17:00:00"))
            {
                message_type = "evening";
            }
            else
            {
                message_type = "night";
            }

            string hello_message = null, end_message = null;

            switch (message_type)
            {
                case "morning":
                    hello_message = "{Chào buổi sáng ạ|Buổi sáng tốt lành nhé ạ}";
                    end_message = "{Chúc bạn có 1 ngày tràn đầy năng lượng nhé ạ!|Chúc bạn có 1 ngày làm việc hiệu quả nhé ạ}";
                    break;
                case "afternoon":
                    hello_message = "{Chào buổi trưa ạ}";
                    end_message = "";
                    break;
                case "evening":
                    hello_message = "{Chào buổi chiều ạ}";
                    end_message = "";
                    break;
                case "night":
                    hello_message = "{Chào buổi tối ạ}";
                    end_message = "{Chúc bạn ngủ ngon nhé ạ|Ngủ ngon và mơ đẹp nhé ạ}";
                    break;
                default:
                    throw new System.ArgumentException("Not recognize this message_type");
            }


            string deviceName = iGDevice.ld_devicename;
            string deviceID = iGDevice.deviceID;


            string Task_MassDM_type = target_profile.Task_MassDM_type;
            var aio_settings = ig_account.aio_settings;
            string spin_message = aio_settings.Task_MassDM[Task_MassDM_type].spin_message;
            string image_url = aio_settings.Task_MassDM[Task_MassDM_type].image_url;


            //MESSAGE

            string message = SharpSpin.Spinner.Spin(spin_message);

            //replace @user, @expired_date

            string expired_date = DateTime.Now.AddDays(7).ToString("dd/M/yyyy");

            message = message.Replace("@expired_date", expired_date);

            message = message.Replace("@hello_message", SharpSpin.Spinner.Spin(hello_message));
            message = message.Replace("@end_message", SharpSpin.Spinner.Spin(end_message));

            ////IMAGE

            //var temp_image_devicepath = "Temp\\" + iGDevice.ld_devicename + "\\temp_img\\";

            //ADBSupport.ADBHelpers.CreatePath(temp_image_devicepath);


            //string image_path = temp_image_devicepath + PLAutoHelper.NormalFuncHelper.RandomGUID(4) + Path.GetExtension(image_url);

            //AIOHelper.GlobalHelper.DownloadImage(image_url, image_path);

            //var modify_img = IGHelpers.ExifHelpers.OptimizeImage_FTPServer(iGDevice.ld_devicename, image_path);

            //Open intent user profile

            ADBSupport.LDHelpers.OpenIGProfile(deviceName, target_profile.user);

            //Check if user not found

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Following_UserNotFound, 1, 2, 0);
                return "user_not_found";
            }
            catch
            {

            }

            var dm_action = new Action.SendDM(iGDevice, message);
            dm_action.DoAction();

            return "";

        }
        #endregion

        #region --plant--

        static private string plant_Default(IGDevice iGDevice, dynamic ig_account, dynamic target_profile)
        {
            string deviceName = iGDevice.ld_devicename;
            string deviceID = iGDevice.deviceID;


            string Task_MassDM_type = target_profile.Task_MassDM_type;
            var aio_settings = ig_account.aio_settings;
            string spin_message = aio_settings.Task_MassDM[Task_MassDM_type].spin_message;

            string message = SharpSpin.Spinner.Spin(spin_message);

            //replace @user, @expired_date

            message = message.Replace("@user", target_profile.user);

            string expired_date = DateTime.Now.AddDays(7).ToString("MMM dd, yyyy", System.Globalization.CultureInfo.InvariantCulture);

            message = message.Replace("@expired_date", expired_date);

            //Open intent user profile

            ADBSupport.LDHelpers.OpenIGProfile(deviceName, target_profile.user);

            //Check if user not found

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Following_UserNotFound, 1, 2, 0);
                return "user_not_found";
            }
            catch
            {

            }

            var dm_action = new Action.SendDM(iGDevice, message, null, true);
            dm_action.DoAction();

            return "";
        }

        #endregion

        #region --weightloss--

        static private string weightloss_Default(IGDevice iGDevice, dynamic ig_account, dynamic target_profile)
        {
            string deviceName = iGDevice.ld_devicename;
            string deviceID = iGDevice.deviceID;


            string Task_MassDM_type = target_profile.Task_MassDM_type;
            var aio_settings = ig_account.aio_settings;
            string spin_message = aio_settings.Task_MassDM[Task_MassDM_type].spin_message;

            string message = SharpSpin.Spinner.Spin(spin_message);

            //replace @user, @expired_date

            message = message.Replace("@user", "@" + target_profile.user);


            //Open intent user profile

            ADBSupport.LDHelpers.OpenIGProfile(deviceName, target_profile.user);

            //Check if user not found

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Following_UserNotFound, 1, 2, 0);
                return "user_not_found";
            }
            catch
            {

            }

            var dm_action = new Action.SendDM(iGDevice, message, null, true);
            dm_action.DoAction();

            return "";
        }

        #endregion
    }
}
