﻿using InstagramApiSharp.Classes;
using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.ThreadHelperDirectCenter
{
    static class DoQueueTask_PublishPost
    {
        static public void RunByCenter(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            var aio_settings = ig_account.aio_settings;

            string project = aio_settings.Project;
            string type = aio_settings.PublishPost.type;

            switch (project)
            {
                case "sunstore95":
                    switch (type)
                    {
                        case "default":
                            Sunstore95_Default(iGDevice, ig_account, json_task);
                            break;
                        case "education_photo":
                            Sunstore95_education_photo(iGDevice, ig_account, json_task);
                            break;
                        default: throw new System.ArgumentException("Not found this type of PublishPost");
                    }
                    break;
                case "weightloss":
                    switch (type)
                    {
                        case "default":
                            weightloss_Default(iGDevice, ig_account, json_task);
                            break;
                        default: throw new System.ArgumentException("Not found this type of PublishPost");
                    }
                    break;
                case "personalized":
                    switch(type)
                    {
                        case "default":
                            personalized_default(iGDevice, ig_account, json_task);
                            break;
                        default: throw new System.ArgumentException("Not found this type of PublishPost");
                    }
                    break;
                case "manifest":
                    switch (type)
                    {
                        case "default":
                            manifest_default(iGDevice, ig_account, json_task);
                            break;
                        default: throw new System.ArgumentException("Not found this type of PublishPost");
                    }
                    break;
                default: throw new System.ArgumentException("Not found this project");
            }
        }

        #region --sunstore95--

        //{
        //  "PublishPost": {
        //    "append_caption":"",
        //    "type": "default"
        //  }
        //}

        static private void Sunstore95_Default(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            var aio_settings = ig_account.aio_settings;


            if (json_task.unique_word!=null)
            {
                string unique_word = json_task.unique_word;

                if (unique_word != "")
                {
                    //get list posts

                    //int retry = 0;
                    //ScrapeMedias:
                    var dyn = IGHelpers.Helpers.APIObJWithProxy();

                    InstagramApiSharp.Classes.Models.InstaMediaList medias = new InstagramApiSharp.Classes.Models.InstaMediaList();

                    int retry = 0;


                    ScrapeMediasFromUser:
                    IResult<InstagramApiSharp.Classes.Models.InstaMediaList> medias_result = IGAPIHelper.Scraper.ScrapeMediasFromUser(dyn.instaApi, ig_account.user);


                    if (medias_result.Succeeded == false)
                    {
                        //check if user is set to private

                        if (medias_result.Info.Message == "Not authorized to view user")
                        {
                            goto PublishPost;
                        }


                        retry++;
                        if (retry < 3)
                        {
                            Thread.Sleep(2000);
                            goto ScrapeMediasFromUser;
                        }
                        else
                        {
                            throw new System.ArgumentException("UnknownError|DoQueueTaskPublish");
                        }

                    }
                    else
                    {
                        medias = medias_result.Value;

                        if (medias.Count == 0)
                        {
                            goto PublishPost;
                        }
                    }

                    //check last 2 post if contains unique_word
                    for (int i = 0; i < 5 && i < medias.Count; i++)
                    {
                        if (medias[i].Caption != null)
                        {
                            if (medias[i].Caption.Text.Contains(unique_word))
                                return;
                        }
                    }
                }
            }

            PublishPost:

            var guid = Guid.NewGuid().GetHashCode().ToString();

            List<string> image_paths = new List<string>();

            List<string> images_urls = new List<string>();

            var images = json_task.image;

            foreach (var item in images)
            {
                images_urls.Add(item["image_url"].ToString());
            }

            if (images_urls[0].Contains("http"))
            {

                var temp_image_devicepath = "Temp\\" + iGDevice.ld_devicename + "\\temp_img\\";

                ADBSupport.ADBHelpers.CreatePath(temp_image_devicepath);

                foreach (var item in images_urls)
                {
                    //var image_path = temp_image_devicepath + PLAutoHelper.NormalFuncHelper.RandomGUID(4) + Path.GetExtension(item);

                    var image_path = "Temp\\" + Path.GetFileName(item);

                    if (!File.Exists(image_path))
                        AIOHelper.GlobalHelper.DownloadImage(item, image_path);

                    image_paths.Add(image_path);
                }
            }
            else
            {
                foreach (var item in images_urls)
                {
                    var image_path = PLAutoHelper.GDriveAPI.GDrive_DownloadFile(item, "Temp\\");

                    image_paths.Add(image_path);
                }
            }

            string caption = json_task.caption;

            string append_caption = SharpSpin.Spinner.Spin(aio_settings.PublishPost.append_caption.ToString());

            string before_caption = SharpSpin.Spinner.Spin(aio_settings.PublishPost.before_caption.ToString());

            caption = before_caption + "\r\n" + caption + "\r\n" + append_caption;

            string hashtag = json_task.hashtag;

            //Publish

            var task = new IGTasks.PublishPostTask(iGDevice, image_paths, caption + "\r\n" + hashtag);
            task.DoTask();
        }

        static public void Sunstore95_education_photo(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            var aio_settings = ig_account.aio_settings;

        PublishPost:

            var guid = Guid.NewGuid().GetHashCode().ToString();

            List<string> image_paths = new List<string>();

            List<string> images_urls = new List<string>();

            var images = json_task.image;

            foreach (var item in images)
            {
                images_urls.Add(item["image_url"].ToString());
            }

            if (images_urls[0].Contains("http"))
            {

                var temp_image_devicepath = "Temp\\" + iGDevice.ld_devicename + "\\temp_img\\";

                ADBSupport.ADBHelpers.CreatePath(temp_image_devicepath);

                foreach (var item in images_urls)
                {
                    //var image_path = temp_image_devicepath + PLAutoHelper.NormalFuncHelper.RandomGUID(4) + Path.GetExtension(item);

                    var image_path = "Temp\\" + Path.GetFileName(item);

                    if (!File.Exists(image_path))
                        AIOHelper.GlobalHelper.DownloadImage(item, image_path);

                    image_paths.Add(image_path);
                }
            }
            else
            {
                foreach (var item in images_urls)
                {
                    var image_path = PLAutoHelper.GDriveAPI.GDrive_DownloadFile(item, "Temp\\");

                    image_paths.Add(image_path);
                }
            }

            string caption = json_task.caption;

            //string append_caption = SharpSpin.Spinner.Spin(aio_settings.PublishPost.append_caption.ToString());


            //caption = caption + "\r\n" + append_caption;

            caption = caption.Replace("@main", "@" + ig_account.user);

            string hashtag = json_task.hashtag;

            //Publish

            var task = new IGTasks.PublishPostTask(iGDevice, image_paths, caption + "\r\n" + hashtag);
            task.DoTask();
        }

        #endregion

        #region --weightloss--

        static private void weightloss_Default(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            var guid = Guid.NewGuid().GetHashCode().ToString();

            List<string> image_paths = new List<string>();

            List<string> images_urls = new List<string>();

            var images = json_task.image;

            foreach (var item in images)
            {
                images_urls.Add(item["image_url"].ToString());
            }

            if (images_urls[0].Contains("http"))
            {

                var temp_image_devicepath = "Temp\\" + iGDevice.ld_devicename + "\\temp_img\\";

                ADBSupport.ADBHelpers.CreatePath(temp_image_devicepath);

                foreach (var item in images_urls)
                {
                    //var image_path = temp_image_devicepath + PLAutoHelper.NormalFuncHelper.RandomGUID(4) + Path.GetExtension(item);

                    var image_path = "Temp\\" + Path.GetFileName(item);

                    if (!File.Exists(image_path))
                        AIOHelper.GlobalHelper.DownloadImage(item, image_path);

                    image_paths.Add(image_path);
                }
            }
            else
            {
                foreach (var item in images_urls)
                {
                    var image_path = PLAutoHelper.GDriveAPI.GDrive_DownloadFile(item, "Temp\\");

                    image_paths.Add(image_path);
                }
            }

            string caption = json_task.caption;

            var aio_settings = ig_account.aio_settings;

            //List<bool> add_promote_content = new List<bool>() { true, false, true, true };

            //if (add_promote_content[(new Random(Guid.NewGuid().GetHashCode())).Next(add_promote_content.Count)])
            //{

            var append_caption_data = aio_settings.PublishPost.append_caption;

            string append_caption = null;

            if (append_caption_data != null)
                append_caption = SharpSpin.Spinner.Spin(append_caption_data.ToString());

            var before_caption_data = aio_settings.PublishPost.before_caption;

            string before_caption = null;

            if (before_caption_data != null)
                before_caption = SharpSpin.Spinner.Spin(before_caption_data.ToString());

            if (before_caption != null && before_caption != "")
            {
                caption = before_caption + "\r\n" + caption;
            }

            if (append_caption != null && append_caption != "")
            {
                caption = caption + "\r\n" + append_caption;
            }

            int accountId = int.Parse(ig_account.id);

            string choiced_promote_main = null;

            if (accountId <= 250)
            {

                List<string> promote_mains = new List<string>() { "weightlossplanliv" }; // "beskinnyadviceivy",

                choiced_promote_main = promote_mains[(new Random(Guid.NewGuid().GetHashCode())).Next(promote_mains.Count)];
            }
            else
            {

                List<string> promote_mains = new List<string>() { "weightlosstipsbysharon", "weightlosstipsbyalmie" }; // "beskinnyadviceivy",

                choiced_promote_main = promote_mains[(new Random(Guid.NewGuid().GetHashCode())).Next(promote_mains.Count)];
            }


            //remove tag from another person to prevent report spam

            caption = caption.Replace("@promote_main", "[[promote_main]]");

            caption = caption.Replace("@name", "[[name]]");

            caption = caption.Replace("@", "");

            caption = caption.Replace("[[promote_main]]", "@" + choiced_promote_main);
            //}

            //replace @name with @profile

            caption = caption.Replace("[[name]]", "@" + ig_account.user);


            string hashtag = json_task.hashtag;

            string final_hashtag = weightloss_GenerateHashtags(hashtag);

            //Publish

            var task = new IGTasks.PublishPostTask(iGDevice, image_paths, caption + "\r\n\r\n\r\n" + final_hashtag);
            task.DoTask();
        }

        static private string weightloss_GenerateHashtags(string hashtag)
        {
            List<string> hashtags = hashtag.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            //get all hashtags from db

            
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                string command_query =
                    @"select *
                    from hashtags
                    where
	                    [ok]='OK' and
                        media_count>400000";

                using (var command = new System.Data.SqlClient.SqlCommand(command_query, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            hashtags.Add("#" + dataReader["hashtag"].ToString());
                        }
                    }
                }

            }

            List<string> optimized = new List<string>();

            foreach (var h in hashtags)
            {
                optimized.Add(h.Replace(" ", ""));
            }

            var random_count = (new Random(Guid.NewGuid().GetHashCode())).Next(20, 27);

            hashtags = optimized.OrderBy(arg => Guid.NewGuid()).Take(random_count).ToList();

            var final_hashtag = "";

            foreach (var h in hashtags)
            {
                final_hashtag += " " + h;
            }

            return final_hashtag;
        }

        #endregion

        #region --personalized gift--

        static private void personalized_default(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            var guid = Guid.NewGuid().GetHashCode().ToString();

            List<string> image_paths = new List<string>();

            List<string> images_urls = new List<string>();

            var images = json_task.image;

            foreach (var item in images)
            {
                images_urls.Add(item["image_url"].ToString());
            }

            if (images_urls[0].Contains("http"))
            {

                var temp_image_devicepath = "Temp\\" + iGDevice.ld_devicename + "\\temp_img\\";

                ADBSupport.ADBHelpers.CreatePath(temp_image_devicepath);

                foreach (var item in images_urls)
                {
                    //var image_path = temp_image_devicepath + PLAutoHelper.NormalFuncHelper.RandomGUID(4) + Path.GetExtension(item);

                    var image_path = "Temp\\" + Path.GetFileName(item);

                    if (!File.Exists(image_path))
                        AIOHelper.GlobalHelper.DownloadImage(item, image_path);

                    image_paths.Add(image_path);
                }
            }
            else
            {
                foreach (var item in images_urls)
                {
                    var image_path = PLAutoHelper.GDriveAPI.GDrive_DownloadFile(item, "Temp\\");

                    image_paths.Add(image_path);
                }
            }

            string caption = json_task.caption;

            var aio_settings = ig_account.aio_settings;

            string append_caption = SharpSpin.Spinner.Spin(aio_settings.PublishPost.append_caption.ToString());

            string before_caption = SharpSpin.Spinner.Spin(aio_settings.PublishPost.before_caption.ToString());

            caption = before_caption + "\r\n" + caption + "\r\n" + append_caption;

            string hashtag = json_task.hashtag;

            string set_niche = "";

            if (caption.ToLower().Contains("wife"))
                set_niche = "wife";

            if (caption.ToLower().Contains("daughter"))
                set_niche = "daughter";

            if (caption.ToLower().Contains("mom"))
                set_niche = "mom";

            string additional_hashtag = personalized_GenerateHashtags(set_niche);

            //Publish

            var task = new IGTasks.PublishPostTask(iGDevice, image_paths, caption + "\r\n\r\n\r\n" + additional_hashtag);
            task.DoTask();


        }

        static private string personalized_GenerateHashtags(string niche)
        {
            string mom_hashtag_str = "#momgifts, #mamalife, #momlifebelike, #mothersdaygifts, #mamabear, #momlifeisthebestlife, #giftformom, #personalizedgifts, #giftsformom";

            List<string> mom_hashtags = mom_hashtag_str.Replace(" ", "").Split(new char[] { ',' }).ToList();

            string wife_hashtag_str = "#giftforwife, #birthdaygift, #customgift, #giftforher, #exclusivegifts, #jewelry, #giftideas, #handmadegifts, #handmadechristmas, #christmasgiftidea, #engravedgifts, #necklace";

            List<string> wife_hashtags = wife_hashtag_str.Replace(" ", "").Split(new char[] { ',' }).ToList();

            string daughter_hashtag_str = "#mommydaughtergoals, #daughterlove, #momdaughter, #familyiseverything, #mumanddaughter, #motherdaughter, #momanddaughter, #motheranddaughtertime, #momlife, #motherhood";

            List<string> daughter_hashtags = daughter_hashtag_str.Replace(" ", "").Split(new char[] { ',' }).ToList();

            List<string> final_hashtag = new List<string>();

            switch (niche)
            {
                case "wife":
                    final_hashtag = wife_hashtags.OrderBy(a => Guid.NewGuid()).ToList();
                    break;
                case "mom":
                    final_hashtag = mom_hashtags.OrderBy(a => Guid.NewGuid()).ToList();
                    break;
                case "daughter":
                    final_hashtag = daughter_hashtags.OrderBy(a => Guid.NewGuid()).ToList();
                    break;
                default:
                    break;
            }

            string final_hashtag_str = "";

            foreach(var item in final_hashtag)
            {
                final_hashtag_str += item + " ";
            }

            return final_hashtag_str;
        }

        #endregion

        #region --manifest--

        static private void manifest_default(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            var guid = Guid.NewGuid().GetHashCode().ToString();

            List<string> image_paths = new List<string>();

            List<string> images_urls = new List<string>();

            var images = json_task.image;

            foreach (var item in images)
            {
                images_urls.Add(item["image_url"].ToString());
            }

            if (images_urls[0].Contains("http"))
            {

                var temp_image_devicepath = "Temp\\" + iGDevice.ld_devicename + "\\temp_img\\";

                ADBSupport.ADBHelpers.CreatePath(temp_image_devicepath);

                foreach (var item in images_urls)
                {
                    //var image_path = temp_image_devicepath + PLAutoHelper.NormalFuncHelper.RandomGUID(4) + Path.GetExtension(item);

                    var image_path = "Temp\\" + Path.GetFileName(item);

                    if (!File.Exists(image_path))
                        AIOHelper.GlobalHelper.DownloadImage(item, image_path);

                    image_paths.Add(image_path);
                }
            }
            else
            {
                foreach (var item in images_urls)
                {
                    var image_path = PLAutoHelper.GDriveAPI.GDrive_DownloadFile(item, "Temp\\");

                    image_paths.Add(image_path);
                }
            }

            string caption = json_task.caption;

            caption = caption.Replace("@name", "@" + ig_account.user);

            var aio_settings = ig_account.aio_settings;

            string append_caption = SharpSpin.Spinner.Spin(aio_settings.PublishPost.append_caption.ToString());

            string before_caption = SharpSpin.Spinner.Spin(aio_settings.PublishPost.before_caption.ToString());

            caption = before_caption + "\r\n" + caption + "\r\n" + append_caption;

            string hashtag = json_task.hashtag;


            //Publish

            var task = new IGTasks.PublishPostTask(iGDevice, image_paths, caption + "\r\n\r\n\r\n" + manifest_GenerateHashtags(hashtag));
            task.DoTask();


        }

        static private string manifest_GenerateHashtags(string hashtag)
        {
            List<string> hashtags = hashtag.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            //get all hashtags from db


            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                string command_query =
                    @"select *
                    from hashtags
                    where
	                    [ok]='OK'";

                using (var command = new System.Data.SqlClient.SqlCommand(command_query, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            hashtags.Add("#" + dataReader["hashtag"].ToString());
                        }
                    }
                }

            }

            List<string> optimized = new List<string>();

            foreach (var h in hashtags)
            {
                optimized.Add(h.Replace(" ", ""));
            }

            var random_count = (new Random(Guid.NewGuid().GetHashCode())).Next(20, 27);

            hashtags = optimized.OrderBy(arg => Guid.NewGuid()).Take(random_count).ToList();

            var final_hashtag = "";

            foreach (var h in hashtags)
            {
                final_hashtag += " " + h;
            }

            return final_hashtag;
        }

        #endregion
    }
}
