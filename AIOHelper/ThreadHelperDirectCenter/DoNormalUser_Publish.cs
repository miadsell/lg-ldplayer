﻿using InstagramApiSharp.Classes;
using InstagramLDPlayer.ADBSupport;
using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.ThreadHelperDirectCenter
{
    static class DoNormalUser_Publish
    {
        static public void RunByCenter(IGDevice iGDevice, dynamic ig_account)
        {
            var aio_settings = ig_account.aio_settings;

            string project = aio_settings.Project;
            string DoNormalUser_Publish_type = aio_settings.DoNormalUser_Publish.type;

            switch (project)
            {
                case "ww":
                    switch (DoNormalUser_Publish_type)
                    {
                        case "default":
                            WWProject_Default(iGDevice, ig_account);
                            break;
                        default: throw new System.ArgumentException("Not found this type of DoNormalUser_Publish");
                    }
                    break;
                default: throw new System.ArgumentException("Not found this project");
            }
        }

        #region --WWProject--

        //{
        //  "DoNormalUser_Publish": {
        //    "comment": {
        //      "turn_on": true,
        //      "text": "{You can check my page for more private {photo|content} at link in bio.|Find more {photo|content} at link in bio.|Checkout my livestream at @link.|To connect with me through {Whatsapp|Snapchat}, check contact here link in bio}"
        //    },
        //    "watermark": {
        //      "turn_on": false,
        //      "image": ""
        //    },
        //    "caption": {
        //      "turn_on": true,
        //      "text": "{You can check my page for more private {photo|content} at link in bio.|Find more {photo|content} at link in bio.|Checkout my livestream at @link.|To connect with me through {Whatsapp|Snapchat}, check contact here link in bio}"
        //    }
        //  }
        //}


        static private void WWProject_Default(IGDevice iGDevice, dynamic ig_account)
        {
            //get settings

            var aio_settings = ig_account.aio_settings;

            string choiced_task = null;

            if (aio_settings != null)
            {
                List<string> added_tasks = new List<string>();

                if (aio_settings["DoNormalUser_Publish"]["comment"] != null && aio_settings["DoNormalUser_Publish"]["comment"]["turn_on"].ToString() == "True")
                    added_tasks.Add("comment");
                if (aio_settings["DoNormalUser_Publish"]["caption"] != null && aio_settings["DoNormalUser_Publish"]["caption"]["turn_on"].ToString() == "True")
                    added_tasks.Add("watermark");
                if (aio_settings["DoNormalUser_Publish"]["watermark"] != null && aio_settings["DoNormalUser_Publish"]["watermark"]["turn_on"].ToString() == "True")
                    added_tasks.Add("caption");

                choiced_task = added_tasks[(new Random()).Next(added_tasks.Count)];

                //do add task or not

                List<bool> do_added_task = new List<bool>() { true, false, true };

                var isdoit = do_added_task[(new Random(Guid.NewGuid().GetHashCode())).Next(do_added_task.Count)];

                if (!isdoit)
                    choiced_task = null;
            }

            //get source profile

            string source_profile = IGHelpers.DBHelpers.GetSourceUser(ig_account.id);


            //get post to repost

            //get list posts

            InstagramApiSharp.Classes.Models.InstaMedia instaMedia = null;

            //int retry = 0;
            //ScrapeMedias:
            var dyn = IGHelpers.Helpers.APIObJWithProxy();

            InstagramApiSharp.Classes.Models.InstaMediaList medias = new InstagramApiSharp.Classes.Models.InstaMediaList();

            int retry = 0;


        ScrapeMediasFromUser:
            IResult<InstagramApiSharp.Classes.Models.InstaMediaList> medias_result = IGAPIHelper.Scraper.ScrapeMediasFromUser(dyn.instaApi, source_profile);


            if (medias_result.Succeeded == false)
            {
                //check if user is set to private

                if(medias_result.Info.Message== "Not authorized to view user")
                {
                    IGHelpers.DBHelpers.UpdateSourceUser(source_profile, "error");
                    return;
                }

                //check if user is remove
                var userinfo = IGAPIHelper.Scraper.ScrapeFullUserInfo_ByUsername(source_profile, dyn.instaApi);

                var userinfo_status = ADBHelpers.GetDynamicProperty(userinfo, "status");

                if (userinfo_status == "NotFoundUser")
                {
                    IGHelpers.DBHelpers.UpdateSourceUser(source_profile, "error");
                    return;
                }
                else
                {
                    retry++;
                    if (retry < 3)
                    {
                        Thread.Sleep(2000);
                        goto ScrapeMediasFromUser;
                    }
                    else
                    {
                        throw new System.ArgumentException("UnknownError|" + userinfo_status);
                    }
                }
            }
            else
            {
                medias = medias_result.Value;

                if (medias.Count == 0)
                {
                    throw new System.ArgumentException("UnknownWhyMediasIsEmpty");
                }
            }
            //get medias id that not published

            foreach (var item in medias)
            {
                if (!IGHelpers.DBHelpers.IsPublishedThisPost(ig_account.id, item.Code))
                {
                    instaMedia = item;
                    break;
                }
            }

            if (instaMedia == null)
            {
                IGHelpers.DBHelpers.AddActionLog(iGDevice.ld_devicename, "published_post", "");
                goto SetNextPublish;
            }


            //tam thoi chi lay image đầu tien

            string imageurl = "";

            //check if has carousel
            if (instaMedia.Carousel != null)
            {
                imageurl = instaMedia.Carousel[0].Images[0].Uri;
            }
            else
            {
                imageurl = instaMedia.Images[0].Uri;
            }


            //image of post

            var imagepath = "Temp\\" + ig_account.phone_device + "\\temp_img\\" + "IMG_" + DateTime.Now.Ticks.ToString() + ".jpg";

            AIOHelper.GlobalHelper.DownloadImage(imageurl, imagepath);


            //Write to file and save to deviceName data

            //REMOVE TAGS FROM CAPTION

            var caption = "";

            if (instaMedia.Caption != null)
                caption = instaMedia.Caption.Text.Replace("@", "");

            //check if need do added task

            if (choiced_task == "caption")
            {
                if (aio_settings["DoNormalUser_Publish"]["caption"]["text"] != null)
                {
                    var added_text = IGHelpers.DBHelpers.ReplaceValueWithExtInfo(ig_account, aio_settings["DoNormalUser_Publish"]["caption"]["text"]);

                    if (added_text != "")
                    {
                        caption = caption + "\r\n" + added_text;
                    }
                }
            }

            var guid = Guid.NewGuid().GetHashCode().ToString();
            //var content_path = ADBSupport.LDHelpers.LDPlayerDATA_path + "\\" + ig_account.phone_device + "\\temp_img\\contentpost_" + guid.Substring(guid.Length - 6) + ".txt";

            //File.WriteAllText(content_path, caption);

            //Publish

            var task = new IGTasks.PublishPostTask(iGDevice, new List<string>() { imagepath }, caption);
            task.DoTask();

            //add log with value

            IGHelpers.DBHelpers.AddActionLog(iGDevice.ld_devicename, "published_post", instaMedia.Code);

        SetNextPublish:

            var next_publish_post = DateTime.Now.AddMinutes((new Random()).Next(720, 1440));

            //set next publish
            IGHelpers.DBHelpers.AddActionLog(iGDevice.ld_devicename, "next_publish_post", next_publish_post.ToString());

            if (instaMedia != null)
            { //neu co post moi thi moi do task add comment

                if (choiced_task == "comment")
                {
                    //check if post was published in last 10 mins
                    if (IGHelpers.DBHelpers.IsPublishedPostInLastXMinutes(ig_account.id, 10))
                    {

                        if (aio_settings["DoNormalUser_Publish"]["comment"]["text"] != null)
                        {
                            var comment = IGHelpers.DBHelpers.ReplaceValueWithExtInfo(ig_account, aio_settings["DoNormalUser_Publish"]["comment"]["text"].ToString());
                            if (comment != "")
                            {
                                var selfcomment = new IGTasks.SelfCommentTask(iGDevice, comment);
                                selfcomment.DoTask();
                            }
                        }
                    }
                }
            }
        }


        #endregion
    }
}
