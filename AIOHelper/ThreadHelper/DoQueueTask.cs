﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.ThreadHelper
{
    static class DoQueueTask
    {
        static public void Run(IGDevice iGDevice, dynamic ig_account)
        {
            var queue_tasks = AIOHelper.DBLogic.QueueTask.ReturnTask(ig_account.id);

            var aio_settings = ig_account.aio_settings;

            string project = aio_settings.Project;

            List<string> pre_actions = new List<string>();

            foreach (var t in queue_tasks)
            {
                var action = t.action;

                if (pre_actions.Contains(action)) //to prevent do multi same actions in shorts time, it can be flagged as unsual activity
                {
                    continue;
                }

                pre_actions.Add(action);

                var json_task = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(t.json_task);

                try
                {
                    switch (action)
                    {
                        case "UpdateLink":
                            UpdateLink(iGDevice, ig_account, json_task);
                            break;
                        case "UpdateBio":
                            UpdateBio(iGDevice, ig_account, json_task);
                            break;
                        case "UpdateImage":
                            UpdateImage(iGDevice, ig_account, json_task);
                            break;
                        case "PublishPost":
                            PublishPost(iGDevice, ig_account, json_task);
                            break;
                        case "UpdateName":
                            UpdateName(iGDevice, ig_account, json_task);
                            break;
                        case "PublishStory":
                            PublishStory(iGDevice, ig_account, json_task);
                            break;
                        case "SharePostToStory":
                            SharePostToStory(iGDevice, ig_account, json_task);
                            break;
                        case "SetPrivacy":
                            SetPrivacy(iGDevice, ig_account, json_task);
                            break;
                        case "UpdateProfile_FromSource":
                            UpdateProfile_FromSource(iGDevice, ig_account, json_task);
                            break;
                        default:
                            throw new System.ArgumentException("Not recognize this queue task");
                    }

                    //update queue task status
                    var command_query =
                    @"update queuetasks
                set
	                [status]='Done', [completed_time]=getdate()
                where [taskid]=@id";

                    using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                    {
                        conn.Open();

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = t.taskid.ToString();
                            command.ExecuteNonQuery();
                        }
                    }

                    //if task is PublishPost or PublishStory => move schedule type of schedule task to match completed time of current task

                    if (action == "PublishPost" || action == "PublishStory")
                    {
                        UpdateScheduledTime_BasedOn_CurrentCompletedTimeTask(ig_account.id, action, t.taskid);
                    }
                }
                catch (Exception ex)
                {

                    ADBSupport.LDHelpers.HandleException(iGDevice.ld_devicename, "DoQueueTask" + "|" + action + "|" + ex.Message, ex.StackTrace);
                    //increase error_times
                    using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                    {
                        conn.Open();

                        string command_query =
                            @"update queuetasks set [error_times]=[error_times]+1 where [taskid]=@taskid";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@taskid", System.Data.SqlDbType.Int).Value = t.taskid.ToString();
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
        }

        #region --Update Account--

        #region --UpdateLink--

        //{
        //  "link":""
        //}

        static private void UpdateLink(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            string link = json_task.link;

            var action = new Action.UpdateLink(iGDevice, link);
            action.DoAction();
        }

        #endregion

        #region --UpdateBio--

        //{
        //  "bio":""
        //}


        static private void UpdateBio(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            string bio = json_task.bio;

            var action = new Action.UpdateBio(iGDevice, bio);
            action.DoAction();
        }

        #endregion

        #region --UpdateImage--

        //{
        //  "image_url":""
        //}
        //image: online url

        static private void UpdateImage(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            //Bitmap bitmap_image = PLAutoHelper.ImageHelper.BitmapFromBase64Str(json_task.image_base64.ToString());

            string image_url = json_task.image_url.ToString();

            string download_path = null;

            if (image_url.Contains("http"))
            {
                download_path = "Temp\\" + iGDevice.ld_devicename + "\\temp_img\\" + PLAutoHelper.NormalFuncHelper.RandomGUID(3) + Path.GetExtension(image_url);

                AIOHelper.GlobalHelper.DownloadImage(image_url, download_path);
            }
            else
                download_path = PLAutoHelper.GDriveAPI.GDrive_DownloadFile(image_url, "Temp\\");

            var bitmap_image = (Bitmap)Image.FromFile(download_path);

            var guid = Guid.NewGuid().GetHashCode().ToString();

            var image_path = "Temp\\" + iGDevice.ld_devicename + "\\temp_img\\" + guid.Substring(guid.Length - 3) + ".jpg";

            ADBSupport.ADBHelpers.CreatePath(Directory.GetParent(image_path).FullName); //create path if not

            bitmap_image.Save(image_path, System.Drawing.Imaging.ImageFormat.Jpeg);

            var modify_img = IGHelpers.ExifHelpers.OptimizeImage_FTPServer(iGDevice.ld_devicename, image_path);

            Action.UpdateProfileImage updateProfileImage = new Action.UpdateProfileImage(iGDevice, modify_img);
            updateProfileImage.DoAction();
        }

        #endregion

        #region --Update FullName--

        //{
        //  "name":""
        //}

        static private void UpdateName(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            string new_name = json_task.name.ToString();

            Action.UpdateFullname updateFullname = new Action.UpdateFullname(iGDevice, new_name);
            updateFullname.DoAction();
        }

        #endregion

        #endregion

        #region --PublishPost--

        //{
        //  "caption":"",
        //  "image":
        //  [
        //    {
        //      "image_url":""
        //    },
        //    {
        //      "image_url":""
        //    },
        //    {
        //      "image_url":""
        //    },
        //    {
        //      "image_url":""
        //    }
        //  ],
        //  "hashtag":"",
        //  "unique_word":"" --if !="", check if last 2 posts contains this word, if yes, return and set Done for taks (ex: ma san pham,...)
        //}

        static public void PublishPost(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            var aio_settings = ig_account.aio_settings;

            string project = aio_settings.Project;
            string type = aio_settings.PublishPost.type;

            if (type == "")
            {
                PublishPost_Default(iGDevice, ig_account, json_task);
            }
            else
            {
                ThreadHelperDirectCenter.DoQueueTask_PublishPost.RunByCenter(iGDevice, ig_account, json_task);
            }
        }

        static private void PublishPost_Default(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            var guid = Guid.NewGuid().GetHashCode().ToString();

            List<string> image_paths = new List<string>();

            List<string> images_urls = new List<string>();

            var images = json_task.image;

            foreach (var item in images)
            {
                images_urls.Add(item["image_url"].ToString());
            }

            if (images_urls[0].Contains("http"))
            {

                var temp_image_devicepath = "Temp\\" + iGDevice.ld_devicename + "\\temp_img\\";

                ADBSupport.ADBHelpers.CreatePath(temp_image_devicepath);

                foreach (var item in images_urls)
                {
                    //var image_path = temp_image_devicepath + PLAutoHelper.NormalFuncHelper.RandomGUID(4) + Path.GetExtension(item);

                    var image_path = "Temp\\" + Path.GetFileName(item);

                    if(!File.Exists(image_path))
                        AIOHelper.GlobalHelper.DownloadImage(item, image_path);

                    image_paths.Add(image_path);
                }
            }
            else
            {
                foreach (var item in images_urls)
                {
                    var image_path = PLAutoHelper.GDriveAPI.GDrive_DownloadFile(item, "Temp\\");

                    image_paths.Add(image_path);
                }
            }

            string caption = json_task.caption;

            var aio_settings = ig_account.aio_settings;

            var append_caption_data = aio_settings.PublishPost.append_caption;

            string append_caption = null;

            if (append_caption_data != null)
                append_caption = SharpSpin.Spinner.Spin(append_caption_data.ToString());

            var before_caption_data = aio_settings.PublishPost.before_caption;

            string before_caption = null;

            if (before_caption_data != null)
                before_caption = SharpSpin.Spinner.Spin(before_caption_data.ToString());

            if (before_caption != null && before_caption != "")
            {
                caption = before_caption + "\r\n" + caption;
            }

            if (append_caption != null && append_caption != "")
            {
                caption = caption + "\r\n" + append_caption;
            }

            //replace @name with @profile

            caption = caption.Replace("@name", "@" + ig_account.user);

            string hashtag = json_task.hashtag;

            //Publish

            var task = new IGTasks.PublishPostTask(iGDevice, image_paths, caption + "\r\n\r\n\r\n" + hashtag);
            task.DoTask();
        }

        #endregion

        #region --PublishStory--

        //{
        //  "image_url": ""
        //}

        static public void PublishStory(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            var aio_settings = ig_account.aio_settings;

            string project = aio_settings.Project;
            string type = aio_settings.PublishStory.type;

            if (type == "")
            {
                PublishStory_Default(iGDevice, ig_account, json_task);
            }
            else
            {
                ThreadHelperDirectCenter.DoQueueTask_PublishStory.RunByCenter(iGDevice, ig_account, json_task);
            }
        }

        static private void PublishStory_Default(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            var guid = Guid.NewGuid().GetHashCode().ToString();

            string image_path = null;

            string image_url = json_task.image_url;

            //var images = json_task.image;

            if (image_url.Contains("http"))
            {

                var temp_image_devicepath = "Temp\\" + iGDevice.ld_devicename + "\\temp_img\\";

                ADBSupport.ADBHelpers.CreatePath(temp_image_devicepath);


                //image_path = temp_image_devicepath + PLAutoHelper.NormalFuncHelper.RandomGUID(4) + Path.GetExtension(image_url);

                image_path = "Temp\\" + Path.GetFileName(image_url);

                if (!File.Exists(image_path))
                    AIOHelper.GlobalHelper.DownloadImage(image_url, image_path);
            }
            else
            {
                image_path = PLAutoHelper.GDriveAPI.GDrive_DownloadFile(image_url, "Temp\\");
            }

            //PublishStory

            var task = new IGTasks.PublicStoriesTask(iGDevice, image_path);
            task.DoTask();
        }



        #endregion

        #region --SharePostToStory--


        //{
        //  "media_url": ""
        //}

        static public void SharePostToStory(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            var aio_settings = ig_account.aio_settings;

            string project = aio_settings.Project;

            SharePostToStory_Default(iGDevice, ig_account, json_task);
        }

        static private void SharePostToStory_Default(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            string media_url = json_task.media_url;


            var task = new IGTasks.SharePostToStoryTask(iGDevice, media_url);
            task.DoTask();
        }


        #endregion

        #region --UpdateProfile_FromSource--

        //{
        //  "source_profile":""
        //}

        static private void UpdateProfile_FromSource(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {
            //get source profile

            string source_profile = json_task.source_profile.ToString();

            //get profile image and bio

            dynamic profile = null;

            var dyn = IGHelpers.Helpers.APIObJWithProxy();

            profile = IGAPIHelper.Scraper.ScrapeProfileBio(dyn.instaApi, source_profile);


            var status = PLAutoHelper.NormalFuncHelper.GetDynamicProperty(profile, "status").ToString();
            if (status != "Success")
            {

                //Check if source user error
                var userinfo = IGAPIHelper.Scraper.ScrapeFullUserInfo_ByUsername(source_profile, dyn.instaApi);

                var userinfo_status = PLAutoHelper.NormalFuncHelper.GetDynamicProperty(userinfo, "status");

                if (userinfo_status == "NotFoundUser")
                {
                    throw new System.ArgumentException("SourceError");
                }
                else
                    throw new System.ArgumentException("UnknowError_ScrapeProfileBio" + status);
            }

            var imagepath = "Temp\\" + ig_account.phone_device + "\\temp_img\\" + "IMG_" + DateTime.Now.Ticks.ToString() + ".jpg";
            DownloadImage(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(profile, "profile_img"), imagepath);


            //create bio txt in device data path


            var task = new IGTasks.UpdateProfileTask(iGDevice, imagepath, PLAutoHelper.NormalFuncHelper.GetDynamicProperty(profile, "bio"));
            task.DoTask();

            //Add action log with target_account value
            IGHelpers.DBHelpers.AddActionLog(iGDevice.ld_devicename, "updateprofile", "", source_profile);
        }

        static private void DownloadImage(string imageurl, string savepath)
        {
            ADBSupport.ADBHelpers.CreatePath(Path.GetDirectoryName(savepath));
            using (var m = new MemoryStream(new WebClient().DownloadData(imageurl)))
            {
                using (var img = Bitmap.FromStream(m))
                {

                    img.Save(savepath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
            }
        }

        #endregion

        #region --SetPrivacy--

        //{
        //  "seton":true
        //}

        static public void SetPrivacy(IGDevice iGDevice, dynamic ig_account, dynamic json_task)
        {

            bool seton = json_task.seton;

            var action = new Action.SetPrivacy(iGDevice, seton);
            action.DoAction();
        }

        #endregion

        #region --reuse func--

        /// <summary>
        /// update schedule of task not done base on scheduled time, completed time of current task
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="action_task"></param>
        static private void UpdateScheduledTime_BasedOn_CurrentCompletedTimeTask(string accountId, string action_task, string current_taskid)
        {
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                //get all action_task task that not done
                string command_query =
                    @"select *
                    from queuetasks
                    where
	                    [action]=@action_task and
	                    ([status] is null or [status]!='Done') and
	                    [accountid]=@accountid
                    order by taskid desc";

                List<dynamic> available_tasks = new List<dynamic>();

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@action_task", System.Data.SqlDbType.VarChar).Value = action_task;
                    command.Parameters.Add("@accountid", System.Data.SqlDbType.Int).Value = accountId;
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            available_tasks.Add(new
                            {
                                taskid = dataReader["taskid"].ToString(),
                                schedule_time = DateTime.Parse(dataReader["schedule_time"].ToString())
                            });
                        }
                    }
                }


                //get scheduled_time, completed of current_taskid
                command_query =
                    @"select *
                    from queuetasks
                    where
	                    [taskid]=@taskid";

                DateTime schedule_time, completed_time;

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@taskid", System.Data.SqlDbType.VarChar).Value = current_taskid;
                    using (var dataReader = command.ExecuteReader())
                    {
                        dataReader.Read();
                        schedule_time = DateTime.Parse(dataReader["schedule_time"].ToString());
                        completed_time = DateTime.Parse(dataReader["completed_time"].ToString());
                    }
                }

                //calculate different time between scheduled_time and completed_time of current_taskid
                var diff = completed_time.Subtract(schedule_time);

                //re-schedule for undone task
                foreach (var t in available_tasks)
                {
                    var new_schedule_time = ((DateTime)t.schedule_time).Add(diff);

                    command_query = @"update queuetasks set [schedule_time]=@schedule_time where [taskid]=@taskid";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("schedule_time", System.Data.SqlDbType.DateTime).Value = new_schedule_time;
                        command.Parameters.Add("taskid", System.Data.SqlDbType.Int).Value = t.taskid;

                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        #endregion
    }
}
