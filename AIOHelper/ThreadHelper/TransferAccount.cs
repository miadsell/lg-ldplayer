﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.ThreadHelper
{
    static class TransferAccount
    {
        static string transferaccount_db_connection_template = @"Data Source=@server_ip;Initial Catalog=@db;User ID=sa;Password=123456789kdL;Pooling=true;Max Pool Size=150;Min Pool Size=50;Connect Timeout=30;Connection Lifetime=300;ConnectRetryCount=3";

        static public void TransferFromTo(List<string> ids, string server_ip_from, string db_name_from, string data_folder_from, string server_ip_to, string db_name_to, string data_folder_to)
        {
            string db_connection_from = transferaccount_db_connection_template.Replace("@server_ip", server_ip_from).Replace("@db", db_name_from);
            string db_connection_to = transferaccount_db_connection_template.Replace("@server_ip", server_ip_to).Replace("@db", db_name_to);


            //validate information

            List<string> cant_validates = new List<string>();


            Program.form.TransferAccount_WriteLog("Validating accounts.......");

            foreach (var id in ids)
            {
                var data_folder = PLAutoHelper.NormalFuncHelper.OptimizePathEndBackSlash(data_folder_from) + "LDPlayer-IG-" + id;

                if(!Directory.Exists(data_folder))
                {
                    cant_validates.Add(id);
                }
            }

            if(cant_validates.Count>0)
            {
                Program.form.TransferAccount_WriteLog("Failed to validate these account:");
                foreach (var id in cant_validates)
                {
                    Program.form.TransferAccount_WriteLog(id);
                }


                Program.form.TransferAccount_WriteLog("Stopped");

                return;
            }

            Program.form.TransferAccount_WriteLog("Validated");

            foreach (var accountId in ids)
            {


                string old_device_name = "LDPlayer-IG-" + accountId;
                var old_folder_name = PLAutoHelper.NormalFuncHelper.OptimizePathEndBackSlash(data_folder_from) + old_device_name;

                string command_query = null;

                //get accountIds obj
                dynamic account = null;
                dynamic ldplayer = null;
                dynamic account_info = null;
                List<dynamic> action_log = new List<dynamic>();

                using (SqlConnection conn = new SqlConnection(db_connection_from))
                {
                    conn.Open();


                    ////add link server
                    //if (server_ip_from != server_ip_to)
                    //{
                    //    //command_query =
                    //    //    @"exec sp_addlinkedserver    @server='" + server_ip_to + "'";

                    //    using (var command = new SqlCommand("sp_addlinkedserver", conn))
                    //    {
                    //        command.CommandType = System.Data.CommandType.StoredProcedure;
                    //        command.Parameters.Add("@server", System.Data.SqlDbType.VarChar).Value = server_ip_to;
                    //        command.ExecuteNonQuery();
                    //    }
                    //}


                    //put these in sqlTransaction




                    //transfer igaccount


                    command_query =
                        @"select *
                        from ig_account
                        where
	                        [id]=@accountId";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                        using (var dataReader = command.ExecuteReader())
                        {
                            dataReader.Read();
                            string user = dataReader["user"].ToString();
                            string niche = dataReader["niche"].ToString();
                            string fullname = dataReader["fullname"].ToString();
                            string note = dataReader["note"] == null ? "" : dataReader["note"].ToString();
                            string bio = dataReader["bio"] == null ? "" : dataReader["bio"].ToString();
                            string profile_image = dataReader["profile_image"] == null ? "" : dataReader["profile_image"].ToString();
                            string birthday = dataReader["birthday"] == null ? "" : dataReader["birthday"].ToString();
                            string password = dataReader["password"].ToString();
                            string phonenum = dataReader["phonenum"].ToString();
                            string register = dataReader["register"].ToString();
                            string state = dataReader["state"].ToString();
                            string mode = dataReader["mode"].ToString();
                            string type = dataReader["type"].ToString();
                            string already_hasprofile = dataReader["already_hasprofile"].ToString();

                            account = new
                            {
                                user,
                                niche,
                                fullname,
                                note,
                                bio,
                                profile_image,
                                birthday,
                                password,
                                phonenum,
                                register,
                                state,
                                mode,
                                type,
                                already_hasprofile
                            };
                        }
                    }


                    //get device information
                    command_query =
                        @"select *
                        from ldplayers
                        where
	                        ld_name = 'LDPlayer-IG-" + accountId + "'";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            dataReader.Read();

                            string ld_name = dataReader["ld_name"].ToString();
                            string manufacturer = dataReader["manufacturer"].ToString();
                            string model = dataReader["model"].ToString();
                            string imei = dataReader["imei"].ToString();
                            string imsi = dataReader["imsi"].ToString();
                            string simserial = dataReader["simserial"].ToString();
                            string androidid = dataReader["androidid"].ToString();
                            string hard_serial = dataReader["hard_serial"].ToString();
                            string mac = dataReader["mac"].ToString();
                            string pnumber = dataReader["pnumber"].ToString();
                            string google_account = dataReader["google_account"].ToString();
                            string location = dataReader["location"].ToString();

                            ldplayer = new
                            {
                                manufacturer,
                                model,
                                imei,
                                imsi,
                                simserial,
                                androidid,
                                hard_serial,
                                mac,
                                pnumber,
                                google_account,
                                location
                            };
                        }
                    }

                    //get account info

                    command_query =
                        @"select *
                      from account_info where [accountId]=@accountId";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                        using (var dataReader = command.ExecuteReader())
                        {
                            dataReader.Read();

                            string active_from = dataReader["active_from"].ToString();
                            string active_to = dataReader["active_to"].ToString();
                            string aio_settings = dataReader["aio_settings"].ToString();
                            string user_screenshoot_ldplayer = dataReader["user_screenshoot_ldplayer"].ToString();
                            string has_message = dataReader["has_message"].ToString();
                            string message_base64 = dataReader["message_base64"].ToString();
                            string total_timespend = dataReader["total_timespend"].ToString();
                            string current_date = dataReader["current_date"].ToString();
                            string rate_mobile_desktop = dataReader["rate_mobile_desktop"].ToString();
                            string target_timedate = dataReader["target_timedate"].ToString();
                            string current_timespend = dataReader["current_timespend"].ToString();
                            string current_timespend_desktop = dataReader["current_timespend_desktop"].ToString();
                            string lastengage = dataReader["lastengage"].ToString();
                            string nextengage = dataReader["nextengage"].ToString();
                            string engagetype = dataReader["engagetype"].ToString();
                            string followingtype = dataReader["followingtype"].ToString();
                            string monitor_comment = dataReader["monitor_comment"].ToString();
                            string monitor_message = dataReader["monitor_message"].ToString();
                            string note_manual = dataReader["note_manual"].ToString();

                            account_info = new
                            {
                                active_from,
                                active_to,
                                aio_settings,
                                user_screenshoot_ldplayer,
                                has_message,
                                message_base64,
                                total_timespend,
                                current_date,
                                rate_mobile_desktop,
                                target_timedate,
                                current_timespend,
                                current_timespend_desktop,
                                lastengage,
                                nextengage,
                                engagetype,
                                followingtype,
                                monitor_comment,
                                monitor_message,
                                note_manual
                            };
                        }
                    }

                    //get all actionlog

                    command_query =
                        @"select *
                    from actionlog
                    where
	                    [accountid]=@accountId";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                action_log.Add(new
                                {
                                    action = dataReader["action"].ToString(),
                                    value = dataReader["value"].ToString(),
                                    target_account = dataReader["target_account"].ToString(),
                                    do_at = dataReader["do_at"].ToString(),
                                });
                            }
                        }
                    }
                }

                string newaccountId = null;

                using (SqlConnection conn = new SqlConnection(db_connection_to))
                {
                    conn.Open();

                    using (var sqlTransaction = conn.BeginTransaction())
                    {

                        //insert into "to" database and get id
                        command_query = "insert into ig_account ([user],niche,fullname,note,bio,profile_image,birthday,password,phonenum,register,state,mode,type,already_hasprofile,pc_devices) values (@user,@niche,@fullname,@note,@bio,@profile_image,@birthday,@password,@phonenum,@register,@state,@mode,@type,@already_hasprofile,@pc_devices)";


                        using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                        {
                            command.Parameters.Add("@user", System.Data.SqlDbType.VarChar).Value = account.user.ToString();
                            command.Parameters.Add("@niche", System.Data.SqlDbType.VarChar).Value = account.niche;
                            command.Parameters.Add("@fullname", System.Data.SqlDbType.NVarChar).Value = account.fullname;
                            command.Parameters.Add("@note", System.Data.SqlDbType.NText).Value = account.note;
                            command.Parameters.Add("@bio", System.Data.SqlDbType.VarChar).Value = account.bio;
                            command.Parameters.Add("@profile_image", System.Data.SqlDbType.VarChar).Value = account.profile_image;
                            command.Parameters.Add("@birthday", System.Data.SqlDbType.DateTime).Value = DBNull.Value;
                            command.Parameters.Add("@password", System.Data.SqlDbType.VarChar).Value = account.password;
                            command.Parameters.Add("@phonenum", System.Data.SqlDbType.VarChar).Value = account.phonenum;
                            command.Parameters.Add("@register", System.Data.SqlDbType.VarChar).Value = account.register;
                            command.Parameters.Add("@state", System.Data.SqlDbType.VarChar).Value = account.state;
                            command.Parameters.Add("@mode", System.Data.SqlDbType.VarChar).Value = account.mode;
                            command.Parameters.Add("@type", System.Data.SqlDbType.VarChar).Value = account.type;
                            command.Parameters.Add("@already_hasprofile", System.Data.SqlDbType.VarChar).Value = account.already_hasprofile;
                            command.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = 1;

                            command.ExecuteNonQuery();
                        }

                        command_query = "select * from ig_account where [user]=@user";

                        using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                        {
                            command.Parameters.Add("@user", System.Data.SqlDbType.VarChar).Value = account.user;
                            using (var dataReader = command.ExecuteReader())
                            {
                                dataReader.Read();
                                newaccountId = dataReader["id"].ToString();
                            }
                        }

                        //transfer device information


                        string new_devicename = "LDPlayer-IG-" + newaccountId;

                        //insert device to "to" database

                        command_query = "insert into ldplayers (ld_name,manufacturer,model,imei,imsi,simserial,androidid,hard_serial,mac,pnumber,google_account,location) values (@ld_name,@manufacturer,@model,@imei,@imsi,@simserial,@androidid,@hard_serial,@mac,@pnumber,@google_account,@location)";


                        using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                        {
                            command.Parameters.Add("@ld_name", System.Data.SqlDbType.VarChar).Value = new_devicename;
                            command.Parameters.Add("@manufacturer", System.Data.SqlDbType.VarChar).Value = ldplayer.manufacturer;
                            command.Parameters.Add("@model", System.Data.SqlDbType.VarChar).Value = ldplayer.model;
                            command.Parameters.Add("@imei", System.Data.SqlDbType.VarChar).Value = ldplayer.imei;
                            command.Parameters.Add("@imsi", System.Data.SqlDbType.VarChar).Value = ldplayer.imsi;
                            command.Parameters.Add("@simserial", System.Data.SqlDbType.VarChar).Value = ldplayer.simserial;
                            command.Parameters.Add("@androidid", System.Data.SqlDbType.VarChar).Value = ldplayer.androidid;
                            command.Parameters.Add("@hard_serial", System.Data.SqlDbType.VarChar).Value = ldplayer.hard_serial;
                            command.Parameters.Add("@mac", System.Data.SqlDbType.VarChar).Value = ldplayer.mac;
                            command.Parameters.Add("@pnumber", System.Data.SqlDbType.VarChar).Value = ldplayer.pnumber;
                            command.Parameters.Add("@google_account", System.Data.SqlDbType.VarChar).Value = ldplayer.google_account;
                            command.Parameters.Add("@location", System.Data.SqlDbType.VarChar).Value = ldplayer.location;

                            command.ExecuteNonQuery();
                        }

                        //update phonedevice to newaccountId

                        command_query = "update ig_account set [phone_device]='" + new_devicename + "' where [id]=" + newaccountId;

                        using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                        {
                            command.ExecuteNonQuery();
                        }

                        //transfer accountinfo
                        command_query =
                            @"insert into account_info ([accountId], [active_from],[active_to],aio_settings,user_screenshoot_ldplayer,has_message,message_base64,total_timespend,[current_date],rate_mobile_desktop,target_timedate,current_timespend,current_timespend_desktop,lastengage,nextengage,engagetype,followingtype,monitor_comment,monitor_message,note_manual)
                        values (@accountId,@active_from,@active_to,@aio_settings,@user_screenshoot_ldplayer,@has_message,@message_base64,@total_timespend,@current_date,@rate_mobile_desktop,@target_timedate,@current_timespend,@current_timespend_desktop,@lastengage,@nextengage,@engagetype,@followingtype,@monitor_comment,@monitor_message,@note_manual)";

                        using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                        {
                            command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = newaccountId;
                            command.Parameters.Add("@active_from", System.Data.SqlDbType.Time).Value = account_info.active_from;
                            command.Parameters.Add("@active_to", System.Data.SqlDbType.Time).Value = account_info.active_to;
                            command.Parameters.Add("@aio_settings", System.Data.SqlDbType.NText).Value = account_info.aio_settings;
                            command.Parameters.Add("@user_screenshoot_ldplayer", System.Data.SqlDbType.VarChar).Value = account_info.user_screenshoot_ldplayer;
                            command.Parameters.Add("@has_message", System.Data.SqlDbType.VarChar).Value = account_info.has_message;
                            command.Parameters.Add("@message_base64", System.Data.SqlDbType.VarChar).Value = account_info.message_base64;
                            command.Parameters.Add("@total_timespend", System.Data.SqlDbType.Float).Value = account_info.total_timespend;
                            command.Parameters.Add("@current_date", System.Data.SqlDbType.DateTime).Value = account_info.current_date;
                            command.Parameters.Add("@rate_mobile_desktop", System.Data.SqlDbType.VarChar).Value = account_info.rate_mobile_desktop;
                            command.Parameters.Add("@target_timedate", System.Data.SqlDbType.Float).Value = account_info.target_timedate;
                            command.Parameters.Add("@current_timespend", System.Data.SqlDbType.Float).Value = account_info.current_timespend;
                            command.Parameters.Add("@current_timespend_desktop", System.Data.SqlDbType.Float).Value = account_info.current_timespend_desktop;
                            command.Parameters.Add("@lastengage", System.Data.SqlDbType.DateTime).Value = account_info.lastengage;
                            command.Parameters.Add("@nextengage", System.Data.SqlDbType.DateTime).Value = account_info.nextengage;
                            command.Parameters.Add("@engagetype", System.Data.SqlDbType.VarChar).Value = account_info.engagetype;
                            command.Parameters.Add("@followingtype", System.Data.SqlDbType.VarChar).Value = account_info.followingtype;
                            command.Parameters.Add("@monitor_comment", System.Data.SqlDbType.VarChar).Value = account_info.monitor_comment;
                            command.Parameters.Add("@monitor_message", System.Data.SqlDbType.VarChar).Value = account_info.monitor_message;
                            command.Parameters.Add("@note_manual", System.Data.SqlDbType.NText).Value = account_info.note_manual;

                            command.ExecuteNonQuery();
                        }

                        //transfer actionlog

                        foreach (var log in action_log)
                        {

                            command_query =
                                @"insert into actionlog (accountid,[action],[value],[target_account],[do_at]) values (@accountid,@action,@value,@target_account,@do_at)";

                            using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                            {
                                command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = newaccountId;
                                command.Parameters.Add("@action", System.Data.SqlDbType.VarChar).Value = log.action;
                                command.Parameters.Add("@value", System.Data.SqlDbType.VarChar).Value = log.value;
                                command.Parameters.Add("@target_account", System.Data.SqlDbType.VarChar).Value = log.target_account;
                                command.Parameters.Add("@do_at", System.Data.SqlDbType.DateTime).Value = log.do_at;

                                command.ExecuteNonQuery();
                            }
                        }

                        //-------
                        //copy folder data

                        var new_folder_name = PLAutoHelper.NormalFuncHelper.OptimizePathEndBackSlash(data_folder_to) + new_devicename;

                        //change name to match newid in target transferaccount database

                        PLAutoHelper.NormalFuncHelper.CopyFilesRecursively(new System.IO.DirectoryInfo(old_folder_name), new System.IO.DirectoryInfo(new_folder_name));

                        sqlTransaction.Commit();

                    }

                }

                //remove database from "from" database
                using (SqlConnection conn = new SqlConnection(db_connection_from))
                {
                    conn.Open();

                    using (var sqlTransaction = conn.BeginTransaction())
                    {
                        //remove all row related to accountId

                        command_query =
                            @"delete account_info where accountId=@accountId
                        delete AccountTasksRecurring where accountId=@accountId
                        delete actionlog where accountid=@accountId
                        delete firefoxtb where accountId=@accountId
                        delete ig_account_external where accountId=@accountId
                        delete ig_post_log where accountId=@accountId
                        delete queuetasks where accountid=@accountId
                        delete slaves_comment where accountId=@accountId
                        delete source_normaluser where accountId=@accountId
                        delete specifictasks where accountId=@accountId";

                        using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                        {
                            command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                            command.ExecuteNonQuery();
                        }

                        //remove ig_account
                        command_query = "delete ig_account where [id]=@accountId";

                        using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                        {
                            command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                            command.ExecuteNonQuery();
                        }


                        //remove ldplayers
                        command_query = "delete ldplayers where [ld_name]=@ld_name";

                        using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                        {
                            command.Parameters.Add("@ld_name", System.Data.SqlDbType.VarChar).Value = old_device_name;
                            command.ExecuteNonQuery();
                        }

                        sqlTransaction.Commit();

                    }
                }

                //remove data folder from "from" db
                System.IO.Directory.Delete(old_folder_name, true);


                Program.form.TransferAccount_WriteLog("Finished to move accountId: " + accountId);
            }


            Program.form.TransferAccount_WriteLog("Finished");
        }


        static private void ValidateInformation()
        {

        }
    }
}
