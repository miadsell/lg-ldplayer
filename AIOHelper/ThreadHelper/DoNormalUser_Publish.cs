﻿using InstagramApiSharp.Classes;
using InstagramLDPlayer.ADBSupport;
using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.ThreadHelper
{
    static class DoNormalUser_Publish
    {
        static public void Run(IGDevice iGDevice, dynamic ig_account)
        {
            var aio_settings = ig_account.aio_settings;

            string project = aio_settings.Project;
            string DoNormalUser_Publish_type = aio_settings.DoNormalUser_Publish.type;

            if (DoNormalUser_Publish_type == "")
                DoNormalUser_Publish_Default(iGDevice, ig_account);
            else
                ThreadHelperDirectCenter.DoNormalUser_Publish.RunByCenter(iGDevice, ig_account);
        }

        static private void DoNormalUser_Publish_Default(IGDevice iGDevice, dynamic ig_account)
        {
            //get source profile

            string source_profile = IGHelpers.DBHelpers.GetSourceUser(ig_account.id);

            //get list posts

            InstagramApiSharp.Classes.Models.InstaMedia instaMedia = null;

            //int retry = 0;
            //ScrapeMedias:
            var dyn = IGHelpers.Helpers.APIObJWithProxy();

            InstagramApiSharp.Classes.Models.InstaMediaList medias = new InstagramApiSharp.Classes.Models.InstaMediaList();

            int retry = 0;


        ScrapeMediasFromUser:
            IResult<InstagramApiSharp.Classes.Models.InstaMediaList> medias_result = IGAPIHelper.Scraper.ScrapeMediasFromUser(dyn.instaApi, source_profile);


            if (medias_result.Succeeded == false)
            {
                //check if user is set to private

                if (medias_result.Info.Message == "Not authorized to view user")
                {
                    IGHelpers.DBHelpers.UpdateSourceUser(source_profile, "error");
                    return;
                }

                //check if user is remove
                var userinfo = IGAPIHelper.Scraper.ScrapeFullUserInfo_ByUsername(source_profile, dyn.instaApi);

                var userinfo_status = ADBHelpers.GetDynamicProperty(userinfo, "status");

                if (userinfo_status == "NotFoundUser")
                {
                    IGHelpers.DBHelpers.UpdateSourceUser(source_profile, "error");
                    return;
                }
                else
                {
                    retry++;
                    if (retry < 3)
                    {
                        Thread.Sleep(2000);
                        goto ScrapeMediasFromUser;
                    }
                    else
                    {
                        throw new System.ArgumentException("UnknownError|" + userinfo_status);
                    }
                }
            }
            else
            {
                medias = medias_result.Value;

                if (medias.Count == 0)
                {
                    throw new System.ArgumentException("UnknownWhyMediasIsEmpty");
                }
            }
            //get medias id that not published

            foreach (var item in medias)
            {
                if (!IGHelpers.DBHelpers.IsPublishedThisPost(ig_account.id, item.Code))
                {
                    instaMedia = item;
                    break;
                }
            }

            if (instaMedia == null)
            {
                IGHelpers.DBHelpers.AddActionLog(iGDevice.ld_devicename, "published_post", "");
                goto SetNextPublish;
            }


            //tam thoi chi lay image đầu tien

            string imageurl = "";

            //check if has carousel
            if (instaMedia.Carousel != null)
            {
                imageurl = instaMedia.Carousel[0].Images[0].Uri;
            }
            else
            {
                imageurl = instaMedia.Images[0].Uri;
            }


            //image of post

            var imagepath = "Temp\\" + ig_account.phone_device + "\\temp_img\\" + "IMG_" + DateTime.Now.Ticks.ToString() + ".jpg";

            AIOHelper.GlobalHelper.DownloadImage(imageurl, imagepath);


            //Write to file and save to deviceName data

            //REMOVE TAGS FROM CAPTION

            var caption = "";

            if (instaMedia.Caption != null)
                caption = instaMedia.Caption.Text.Replace("@", "");

            var guid = Guid.NewGuid().GetHashCode().ToString();

            //Publish

            var task = new IGTasks.PublishPostTask(iGDevice, new List<string>() { imagepath }, caption);
            task.DoTask();

            //add log with value

            IGHelpers.DBHelpers.AddActionLog(iGDevice.ld_devicename, "published_post", instaMedia.Code);

        SetNextPublish:

            var next_publish_post = DateTime.Now.AddMinutes((new Random()).Next(720, 1440));

            //set next publish
            IGHelpers.DBHelpers.AddActionLog(iGDevice.ld_devicename, "next_publish_post", next_publish_post.ToString());
        }
    }
}
