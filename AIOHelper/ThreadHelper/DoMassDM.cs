﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.ThreadHelper
{
    static class DoMassDM
    {
        static public string Run(IGDevice iGDevice, dynamic ig_account, dynamic target_profile)
        {
            var aio_settings = ig_account.aio_settings;

            string project = aio_settings.Project;
            string Task_MassDM_type = target_profile.Task_MassDM_type;

            string action_type = aio_settings.Task_MassDM[Task_MassDM_type].action_type;

            switch(action_type)
            {
                case "":
                    return DoMassDM_Default(iGDevice, ig_account, target_profile);
                default:
                    return AIOHelper.ThreadHelperDirectCenter.DoMassDM.RunByCenter(iGDevice, ig_account, target_profile);
            }

        }

        static public string DoMassDM_Default(IGDevice iGDevice, dynamic ig_account, dynamic target_profile)
        {

            string deviceName = iGDevice.ld_devicename;
            string deviceID = iGDevice.deviceID;


            string Task_MassDM_type = target_profile.Task_MassDM_type;
            var aio_settings = ig_account.aio_settings;
            string spin_message = aio_settings.Task_MassDM[Task_MassDM_type].spin_message;

            string message = SharpSpin.Spinner.Spin(spin_message);

            message = message.Replace("@user", "@" + target_profile.user);
            //Open intent user profile

            ADBSupport.LDHelpers.OpenIGProfile(deviceName, target_profile.user);

            //Check if user not found

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Following_UserNotFound, 1, 2, 0);
                return "user_not_found";
            }
            catch
            {

            }

            var dm_action = new Action.SendDM(iGDevice, message, null, true);
            dm_action.DoAction();

            return "";
        }
    }
}
