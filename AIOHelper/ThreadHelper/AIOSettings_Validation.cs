﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.ThreadHelper
{
    static class AIOSettings_Validation
    {
        /// <summary>
        /// checking to make sure aio settings string in account_info table is correct
        /// </summary>
        static public dynamic Do()
        {
            List<dynamic> notvalidate_accounts = new List<dynamic>();

            using (SqlConnection sqlConnection = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                //get all active ig_account
                sqlConnection.Open();
                using (SqlCommand sqlCommand = new SqlCommand("", sqlConnection))
                {

                    var availableAccountIds = ActiveAccountToValidate(sqlCommand);

                    //check every accountId
                    foreach (var id in availableAccountIds)
                    {
                        dynamic account_dynamic = null;

                        //read all property need to validate
                        var command_query =
                            @"select [aio_settings],[type],specifictasks.followinguser,specifictasks.likepost
                            from ig_account
                            join account_info on ig_account.id=account_info.accountId
                            join specifictasks on ig_account.id=specifictasks.accountId
                            where [id]=" + id;

                        sqlCommand.CommandText = command_query;

                        using (var dataReader = sqlCommand.ExecuteReader())
                        {
                            dataReader.Read();

                            if (!dataReader.HasRows)
                                continue;

                            account_dynamic = new
                            {
                                aio_settings = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(dataReader["aio_settings"].ToString()),
                                type = dataReader["type"] == null ? "" : dataReader["type"].ToString(),
                                followinguser = dataReader["followinguser"] == null ? "" : dataReader["followinguser"].ToString(),
                                likepost = dataReader["likepost"] == null ? "" : dataReader["likepost"].ToString()
                            };
                        }

                        dynamic result;

                        result = Validate_AIO_Settings(account_dynamic);
                        if (result.status != "Success")
                        {
                            notvalidate_accounts.Add(new
                            {
                                id,
                                message = result.message
                            });
                            continue;
                        }


                        result = Validate_ProjectName(account_dynamic);
                        if (result.status != "Success")
                        {
                            notvalidate_accounts.Add(new
                            {
                                id,
                                message = result.message
                            });
                            continue;
                        }


                        result = Validate_DoNormalUser_Publish(account_dynamic);
                        if (result.status != "Success")
                        {
                            notvalidate_accounts.Add(new
                            {
                                id,
                                message = result.message
                            });
                            continue;
                        }

                        result = Validate_Task_FollowingUser_Setting(account_dynamic);
                        if (result.status != "Success")
                        {
                            notvalidate_accounts.Add(new
                            {
                                id,
                                message = result.message
                            });
                            continue;
                        }

                        result = Validate_Task_LikeUserPost_Setting(account_dynamic);
                        if (result.status != "Success")
                        {
                            notvalidate_accounts.Add(new
                            {
                                id,
                                message = result.message
                            });
                            continue;
                        }
                    }
                }
            }

            return notvalidate_accounts;
        }

        //{
        //  "DoNormalUser_Publish": {
        //    "comment": {
        //      "turn_on": true,
        //      "text": "{You can check my page for more private {photo|content} at link in bio.|Find more {photo|content} at link in bio.|Checkout my livestream at @link.|To connect with me through {Whatsapp|Snapchat}, check contact here link in bio}"
        //    },
        //    "watermark": {
        //      "turn_on": false,
        //      "image": ""
        //    },
        //    "caption": {
        //      "turn_on": true,
        //      "text": "{You can check my page for more private {photo|content} at link in bio.|Find more {photo|content} at link in bio.|Checkout my livestream at @link.|To connect with me through {Whatsapp|Snapchat}, check contact here link in bio}"
        //    },
        //    "type": "default"
        //  },
        //  "Project": "ww",
        //  "Task_FollowingUser": {
        //    "type": "default",
        //    "data_type": "default",
        //    "action_type": ""
        //  },
        //  "Task_LikeUserPost": {
        //    "type": ""
        //  }
        //}

        static private dynamic Validate_AIO_Settings(dynamic account_dynamic)
        {
            string status = "Success";
            string message = "";

            if (account_dynamic.aio_settings == null)
            {
                status = "Failed";
                message = "AIO_Settings";
                goto ReturnValue;
            }
        ReturnValue:

            return new
            {
                status,
                message
            };
        }

        static private dynamic Validate_ProjectName(dynamic account_dynamic)
        {
            string status = "Success";
            string message = "";

            dynamic aio_settings = account_dynamic.aio_settings;

            //validate settings
            if (aio_settings["Project"] == null)
            {
                status = "Failed";
                message = "Project";
                goto ReturnValue;
            }

        ReturnValue:

            return new
            {
                status,
                message
            };
        }

        static private dynamic Validate_DoNormalUser_Publish(dynamic account_dynamic)
        {
            string status = "Success";
            string message = "";
            //check if DoNormalUser_Publish is enable, if not return
            if (account_dynamic.type != "normaluser")
                goto ReturnValue;

            dynamic aio_settings = account_dynamic.aio_settings;


            //validate settings
            if (aio_settings["DoNormalUser_Publish"] == null)
            {
                status = "Failed";
                message = "DoNormalUser_Publish";
                goto ReturnValue;
            }

            if (aio_settings["DoNormalUser_Publish"]["type"] == null)
            {
                status = "Failed";
                message = "DoNormalUser_Publish|miss type";
                goto ReturnValue;
            }


        ReturnValue:

            return new
            {
                status,
                message
            };
        }

        static private dynamic Validate_Task_FollowingUser_Setting(dynamic account_dynamic)
        {
            string status = "Success";
            string message = "";
            //check if following task is enable, if not return
            if (account_dynamic.followinguser != "Do")
                goto ReturnValue;

            dynamic aio_settings = account_dynamic.aio_settings;


            //validate settings
            if (aio_settings["Task_FollowingUser"] == null)
            {
                status = "Failed";
                message = "Task_FollowingUser";
                goto ReturnValue;
            }

            if (aio_settings["Task_FollowingUser"]["data_type"] == null)
            {
                status = "Failed";
                message = "Task_FollowingUser|miss data_type";
                goto ReturnValue;
            }

            if (aio_settings["Task_FollowingUser"]["action_type"] == null)
            {
                status = "Failed";
                message = "Task_FollowingUser|miss action_type";
                goto ReturnValue;
            }



        ReturnValue:

            return new
            {
                status,
                message
            };


        }

        static private dynamic Validate_Task_LikeUserPost_Setting(dynamic account_dynamic)
        {
            string status = "Success";
            string message = "";

            //check if likeuserpost task is enable, if not return
            if (account_dynamic.likepost != "Do")
                goto ReturnValue;

            dynamic aio_settings = account_dynamic.aio_settings;


            //validate settings
            if (aio_settings["Task_LikeUserPost"] == null)
            {
                status = "Failed";
                message = "Task_LikeUserPost";
                goto ReturnValue;
            }

            if (aio_settings["Task_LikeUserPost"]["data_type"] == null)
            {
                status = "Failed";
                message = "Task_LikeUserPost|miss data_type";
                goto ReturnValue;
            }

            if (aio_settings["Task_LikeUserPost"]["action_type"] == null)
            {
                status = "Failed";
                message = "Task_LikeUserPost|miss action_type";
                goto ReturnValue;
            }

            if (aio_settings["Task_LikeUserPost"]["log_name"] == null)
            {
                status = "Failed";
                message = "Task_LikeUserPost|miss log_name";
                goto ReturnValue;
            }



        ReturnValue:

            return new
            {
                status,
                message
            };

        }

        #region --DBHelper--

        static private List<string> ActiveAccountToValidate(SqlCommand command)
        {
            List<string> availableAccountIds = new List<string>();

            var command_query =
                @"select [id]
                from ig_account
                where
	                [state]='ACTIVE'";

            command.CommandText = command_query;

            using (var dataReader = command.ExecuteReader())
            {
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        availableAccountIds.Add(dataReader["id"].ToString());
                    }
                }
            }

            return availableAccountIds;
        }

        #endregion


    }
}
