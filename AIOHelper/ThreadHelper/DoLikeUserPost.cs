﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.ThreadHelper
{
    static class DoLikeUserPost
    {
        static public void Run(IGDevice iGDevice, dynamic ig_account, string target_profile)
        {
            var aio_settings = ig_account.aio_settings;

            string project = aio_settings.Project;
            string type = aio_settings.Task_LikeUserPost.action_type;

            if (type == "")
            {
                DoLikeUserPost_Default(iGDevice, ig_account, target_profile);
            }
            else
                ThreadHelperDirectCenter.DoLikeUserPost.RunByCenter(iGDevice, ig_account, target_profile, project, type);
        }

        static private void DoLikeUserPost_Default(IGDevice iGDevice, dynamic ig_account, string target_profile)
        {

            string deviceName = iGDevice.ld_devicename;
            string deviceID = iGDevice.deviceID;

            //Open intent user profile

            ADBSupport.LDHelpers.OpenIGProfile(deviceName, target_profile);


            //Scroll down random some times
            var scroll_down = new Action.RandomScroll("down", deviceID);

            var scroll_down_times = (new Random()).Next(1, 3);

            for (int s = 0; s < scroll_down_times; s++)
            {
                scroll_down.DoAction();

                ADBSupport.ADBHelpers.Delay(1, 3);
            }


            //Back to top

            var scroll_up = new Action.RandomScroll("up", deviceID);


            Stopwatch watch = new Stopwatch();
            watch.Start();

            while (true)
            {

                if (watch.Elapsed.TotalMinutes > 1)
                    return;

                scroll_up.DoAction();

                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.LikeUserPost_PostTabIcon, 1, 1);
                    break;
                }
                catch { }

                ADBSupport.ADBHelpers.Delay(1, 2);

            }
            //Find post icon


            var dest_posticon = ADBSupport.ADBHelpers.RandomDoubleValue(16.6, 24.2);

            ADBSupport.ADBHelpers.ScrollImageToPosByPercent(deviceID, ADBSupport.BMPSource.LikeUserPost_PostTabIcon, dest_posticon);

            //get point of posticon tab

            var screen = ADBSupport.ADBHelpers.ScreenShoot(deviceID);

            var posticon_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, ADBSupport.BMPSource.LikeUserPost_PostTabIcon);

            var latest_post_y = ADBSupport.ADBHelpers.ConvertPointToPercentage(deviceID, "Y", posticon_point.Value.Y) + 7;


            //Find rectangle of latest post
            var latest_post_rec = new Class.RecPercentage(new { x = 5, y = latest_post_y }, 25, 10);


            for (int retry = 0; retry < 5; retry++)
            {
                //Click post

                ADBSupport.ADBHelpers.TapRecTanglePercentage(deviceID, latest_post_rec);

                try
                {
                    //Wait Posts title
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.LikeUserPost_WaitPosts, 4, 1, 2);
                    break;
                }
                catch
                {
                    if (retry == 4)
                        throw new System.ArgumentException("Not found post");
                }
            }

            //Hit like

            dynamic likebtn_point = null;

            var scroll = new Action.RandomScroll("down", deviceID);

            Stopwatch watch_scroll = new Stopwatch();
            watch_scroll.Start();

            while (true)
            {

                if (watch_scroll.Elapsed.TotalMinutes > 2)
                    break;

                likebtn_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.LikeButton);

                if (likebtn_point == null)
                {
                    //scroll tool likebtn
                    try
                    {
                        ADBSupport.ADBHelpers.ScrollDownToImage(deviceID, 60.1, ADBSupport.BMPSource.LikeButton);
                    }
                    catch
                    {
                        break;
                    }
                }
                else
                    break;
            }

            if (likebtn_point == null)
                return;


            //Swipe image post if yes (random)

            (new Action.SwipeHorizontal(iGDevice)).DoAction();

            //get likebtn point again
            likebtn_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.LikeButton);

            if (likebtn_point == null)
                return;

            ADBSupport.ADBHelpers.RandomTapImageByPercentage(deviceID, likebtn_point.x, likebtn_point.y, "LikeButton");
            ADBSupport.ADBHelpers.Delay(1, 3);

        }
    }
}
