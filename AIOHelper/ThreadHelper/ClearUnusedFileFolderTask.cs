﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.ThreadHelper
{
    static class ClearUnusedFileFolderTask
    {
        static public void Do()
        {
            string dr_location = "Temp";

            var dr = new DirectoryInfo(dr_location);

            //var files = dr.GetFiles("*",SearchOption.AllDirectories).ToList();

            //delete files older than 30 minutes
            var files = dr.GetFiles("*", SearchOption.AllDirectories).Where(x => x.CreationTime < DateTime.Now.Subtract(TimeSpan.FromMinutes(30))).ToList();

            foreach(var f in files)
            {
                try
                {
                    f.Delete();
                }
                catch { }
            }

            //Clear empty directory

            //RemoveEmptySubDirectories(dr_location);
        }


        static private void RemoveEmptySubDirectories(string startLocation)
        {
            foreach (var directory in Directory.GetDirectories(startLocation))
            {
                RemoveEmptySubDirectories(directory);
                if (Directory.GetFiles(directory).Length == 0 &&
                    Directory.GetDirectories(directory).Length == 0)
                {
                    Directory.Delete(directory, false);
                }
            }
        }

        #region --Ticker ManageThread--

        static private System.Windows.Forms.Timer timer_ClearUnusedFF;

        static public void InitTimer_ClearUnusedFF()
        {
            timer_ClearUnusedFF = new System.Windows.Forms.Timer();
            timer_ClearUnusedFF.Tick += new EventHandler(timer_ClearUnusedFF_Tick);
            timer_ClearUnusedFF.Interval = 30 * 60 * 1000; // in miliseconds --> 30 minutes
            timer_ClearUnusedFF.Start();

        }

        static private void timer_ClearUnusedFF_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(new ThreadStart(Do));
            worker.Name = "Ticking Manage Thread";
            worker.IsBackground = true;
            worker.Start();
        }

        #endregion
    }
}
