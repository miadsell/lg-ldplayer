﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.ThreadHelper
{
    /// <summary>
    /// use to monitor thread usage to optimize to run maximize as much as hardward can handle
    /// </summary>
    class MonitorThreadUsage
    {

        static public void ExecuteMonitor()
        {
            //get active thread_running
            string command_query =
                @"select count(*)
                from actionlog
                where
	                [action]='thread_running' and
	                [target_account]='pc_devices_" + Program.form.pc_devices + "'";

            int count_running_threads = 0;

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {
                    //command.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = Program.form.pc_devices;
                    count_running_threads = int.Parse(command.ExecuteScalar().ToString());
                }
            }

            //count thread wait_proxy_mass

            command_query =
                @"select count(*)
                from actionlog
                where
	                [action]='thread_running' and
                    [value]='wait_proxy_action_mass' and
	                [target_account]='pc_devices_" + Program.form.pc_devices + "'";

            int count_wait_proxy_threads_mass = 0;

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();


                using (var command = new SqlCommand(command_query, conn))
                {
                    //command.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = Program.form.pc_devices;
                    count_wait_proxy_threads_mass = int.Parse(command.ExecuteScalar().ToString());
                }
            }


            //count thread wait_proxy_light

            command_query =
                @"select count(*)
                from actionlog
                where
	                [action]='thread_running' and
                    [value]='wait_proxy_action_light' and
	                [target_account]='pc_devices_" + Program.form.pc_devices + "'";

            int count_wait_proxy_threads_light = 0;

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();


                using (var command = new SqlCommand(command_query, conn))
                {
                    //command.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = Program.form.pc_devices;
                    count_wait_proxy_threads_light = int.Parse(command.ExecuteScalar().ToString());
                }
            }

            //count all available accounts that can be run now if has more thread
            //handle later

            //insert into actionlog

            string value = "pc_devices|" + Program.form.pc_devices + "|count_running_threads|" + count_running_threads.ToString() + "|count_wait_proxy_threads_mass|" + count_wait_proxy_threads_mass.ToString() + "|count_wait_proxy_threads_light|" + count_wait_proxy_threads_light.ToString();

            IGHelpers.DBHelpers.AddActionLogByID("155", "monitor_thread_usage", value); //just random accountId to match condition about accountId property of actionlog table
        }

        #region --Count available accounts to run--

        static private int CountAvailableAccountsToRun()
        {
            throw new System.ArgumentException("Not implemented");
            return 0;
        }

        static private int Sunstore95_CountAvailableAccountsToRun()
        {
            //count queuetasks


            //count engage


            //count massaction


            //count normaluser task


            //remove duplicate


            //count and return


            return 0;
        }


        #endregion

        /// <summary>
        /// use to clear when start software
        /// </summary>
        static public void ClearThreadRunningLog(string pc_devices = null)
        {
            if (pc_devices == null)
            {
                pc_devices = Program.form.pc_devices;
            }

            var command_query =
                @"delete actionlog
                where
	                [action]='thread_running' and
	                [target_account]='pc_devices_" + pc_devices + "'";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();


                using (var command = new SqlCommand(command_query, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }


        static public void AddThread_RunningLog(string accountId)
        {
            IGHelpers.DBHelpers.AddActionLogByID(accountId, "thread_running", "", "pc_devices_" + Program.form.pc_devices);
        }

        static public void UpdateThreadRunning_Log(string accountId, string log_value)
        {
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                TakeLogid:
                //Take logid
                string command_query =
                @"select *
                from actionlog
                where
	                [accountid]=@accountId and
	                [action]='thread_running'";

                string logid = null;

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();
                            logid = dataReader["id"].ToString();
                        }
                    }
                }

                if (logid == null)
                {
                    //add log again
                    AddThread_RunningLog(accountId);

                    goto TakeLogid;
                }




                command_query =
                @"update actionlog set [value]=@log_value
                where
	                [id]=@logid";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@log_value", System.Data.SqlDbType.VarChar).Value = log_value;
                    command.Parameters.Add("@logid", System.Data.SqlDbType.VarChar).Value = logid;
                    command.ExecuteNonQuery();
                }

            }
        }

        static public void DeleteThread_Running(string accountId)
        {
            string command_query =
                @"delete actionlog
                where
	                [accountid]=@accountId and
	                [action]='thread_running'";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                            command.ExecuteNonQuery();
                        }
                    }, "DeleteThread_Running");
            }
        }
    }
}
