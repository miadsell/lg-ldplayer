﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.ThreadHelper
{
    static class DoFollowingUser
    {
        static public string Run(IGDevice iGDevice, dynamic ig_account, string target_profile)
        {
            var aio_settings = ig_account.aio_settings;

            string project = aio_settings.Project;
            string type = aio_settings.Task_FollowingUser.action_type;

            if (type == "")
            {
                return DoFollowingUser_Default(iGDevice, ig_account, target_profile);
            }

            return AIOHelper.ThreadHelperDirectCenter.DoFollowingUser.RunByCenter(iGDevice, ig_account, target_profile);

        }

        static private string DoFollowingUser_Default(IGDevice iGDevice, dynamic ig_account, string target_profile)
        {

            string deviceName = iGDevice.ld_devicename;
            string deviceID = iGDevice.deviceID;

            //Open intent user profile

            ADBSupport.LDHelpers.OpenIGProfile(deviceName, target_profile);

            //Check if user not found

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Following_UserNotFound, 1, 2, 0);
                return "user_not_found";
            }
            catch
            {

            }

            ////check if has post

            //if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, BMResource.FollowPeople.FollowPeople_HasPost, 1, 0, 0))
            //{

            //    //Scroll down random some times
            //    var scroll_down = new Action.RandomScroll("down", deviceID);

            //    var scroll_down_times = (new Random()).Next(1, 3);

            //    for (int s = 0; s < scroll_down_times; s++)
            //    {
            //        scroll_down.DoAction();

            //        ADBSupport.ADBHelpers.Delay(1, 3);
            //    }


            //    //Back to top

            //    var scroll_up = new Action.RandomScroll("up", deviceID);

            //    Stopwatch watch = new Stopwatch();
            //    watch.Start();

            //    while (true)
            //    {

            //        if (watch.Elapsed.TotalMinutes > 1)
            //        {
            //            //Check if action blocked
            //            var isaction_blocked = IGHelpers.Helpers.IsActionBlocked(deviceID, deviceName);

            //            if (isaction_blocked)
            //            {
            //                return "Action Blocked";
            //            }

            //            return "";
            //        }

            //        scroll_up.DoAction();

            //        try
            //        {
            //            //scroll until see Follow Btn
            //            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Follow1Btn, 1, 1);
            //            break;
            //        }
            //        catch { }

            //        ADBSupport.ADBHelpers.Delay(1, 2);

            //    }
            //}

            //Do follow


            var follow = new Action.FollowPeople(iGDevice);
            follow.DoAction();


            var isaction_blocked = IGHelpers.Helpers.IsActionBlocked(deviceID, deviceName);

            if (isaction_blocked)
            {
                return "Action Blocked";
            }

            return "";
        }
    }
}
