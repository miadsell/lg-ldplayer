﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.ThreadHelper
{
    static class MonitorDB
    {
        //class to help monitordb to prevent destroy server performance

        //use this variable to limit parallel process to get account or target account to do task

        static public Semaphore semaphore_monitor_processdb = new Semaphore(15, 15);

    }
}
