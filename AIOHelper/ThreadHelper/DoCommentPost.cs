﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.ThreadHelper
{
    static class DoCommentPost
    {
        static public string Run(IGDevice iGDevice, dynamic ig_account, dynamic comment)
        {
            var aio_settings = ig_account.aio_settings;

            string project = aio_settings.Project;
            string type = aio_settings.Task_CommentPost.action_type;

            if (type == "")
            {
                return DoCommentPost_Default(iGDevice, ig_account, comment);
            }

            return ThreadHelperDirectCenter.DoCommentPost.RunByCenter(iGDevice, ig_account, comment);

        }

        static private string DoCommentPost_Default(IGDevice iGDevice, dynamic ig_account, dynamic comment)
        {
            string deviceName = iGDevice.ld_devicename;
            string deviceID = iGDevice.deviceID;
            var aio_settings = ig_account.aio_settings;
            string comment_text = aio_settings.Task_CommentPost.comment;

            //comment = new
            //{
            //    posturl = "https://www.instagram.com/p/CC52g2HsAEh/"
            //};

            int retry = 0;
        //open intent link
        OpenLink:
            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", comment.posturl, "com.instagram.android");

            //make some simulate


            //add comment
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.SemiAutoComment_CommentBtn, 2, 5, 2);
            }
            catch
            {
                retry++;
                if (retry > 3)
                    throw new System.ArgumentException("Can't open intent");
                goto OpenLink;
            }

            //Click comment icon

            var commentbtn_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.SemiAutoComment_CommentBtn);

            ADBSupport.ADBHelpers.RandomTapImageByPercentage(deviceID, commentbtn_point.x, commentbtn_point.y, "SemiAutoComment_CommentBtn");

            //wait addcomment box

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.SemiAutoComment_AddACommentBox, 5, 5, 3);

            //Send text
            ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID, deviceName, comment_text);

            //Hit post

            var post_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.SemiAutoComment_PostComment);

            ADBSupport.ADBHelpers.RandomTapImageByPercentage(deviceID, post_point.x, post_point.y, "SemiAutoComment_PostComment");

            ADBSupport.ADBHelpers.Delay(10, 15);

            //wait posting disappear

            while (true)
            {
                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.SemiAutoComment_Posting, 1, 1);
                }
                catch
                {
                    break;
                }

                Thread.Sleep(1000);
            }

            return "";
        }
    }
}
