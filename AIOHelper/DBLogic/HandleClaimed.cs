﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.DBLogic
{
    /// <summary>
    /// handle claimed|clear claimed for all project
    /// </summary>
    static class HandleClaimed
    {
        static public string TakeClaimedText(string pc_devices = null)
        {
            if (pc_devices == null)
            {
                pc_devices = Program.form.pc_devices;
            }

            return "Claimed|By " + pc_devices;
        }
    }
}
