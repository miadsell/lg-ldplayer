﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.DBLogic
{
    static class GenerateStats
    {
        static public void Do()
        {

            var command_query = "";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();


                /*Insert ig_account that aren't added to account_info*/

                command_query =
                    @"insert into account_info ([accountId])
	                select [id] from ig_account
	                where
		                [id] not in (select [accountId] from account_info)";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.ExecuteNonQuery();
                }

                //get list accountId from accountInfo

                List<string> accountIds = new List<string>();

                command_query =
                    @"select accountId
                        from account_info";

                using (var command = new SqlCommand(command_query, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            accountIds.Add(dataReader["accountId"].ToString());
                        }
                    }
                }

                //set engagetype for account that aren't set

                foreach (var id in accountIds)
                {
                    //get current engagetype

                    string engage_type = null;

                    using (var command = new SqlCommand("select [engagetype] from account_info where accountId=@id", conn))
                    {
                        command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

                        using (var dataReader = command.ExecuteReader())
                        {
                            dataReader.Read();
                            engage_type = dataReader["engagetype"] == null ? "" : dataReader["engagetype"].ToString();
                        }
                    }

                    if (engage_type != "" && !engage_type.Contains("warmup"))
                    {
                        continue;
                    }

                    //get current engage_days

                    int engage_days = 0;

                    command_query =
                        @"(select count (distinct (CONVERT(date, do_at))) from actionlog where accountid=@id)";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                        engage_days = int.Parse(command.ExecuteScalar().ToString());
                    }

                    double total_engage_minutes = 0;

                    command_query =
                        @"(select account_info.total_timespend from account_info where accountId=@id)";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                        total_engage_minutes = double.Parse(command.ExecuteScalar().ToString());
                    }

                    string set_engagetype = "";

                    if (engage_days < 3 || total_engage_minutes <= 20)
                        set_engagetype = "warmuplite";
                    else
                    if (engage_days <= 5 || total_engage_minutes <= 50)
                        set_engagetype = "warmup";
                    else
                        set_engagetype = "default";

                    //update engagetype

                    command_query =
                            @"update account_info set [engagetype]='default' where [accountId]=@id";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                        command.ExecuteNonQuery();
                    }
                }

                /*Check if datediff current > 0, if yes, set currentdate again and reset CurrentActionDate and regenerate TargetActionDate*/

                foreach (var id in accountIds)
                {
                    string engagetype, current_date;

                    TimeSpan active_from, active_to;

                    //get engagetype, currentdate, activefrom
                    command_query =
                        @"select [engagetype],[current_date],[active_from],[active_to] from account_info where accountId=@id";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                        using (var dataReader = command.ExecuteReader())
                        {
                            dataReader.Read();

                            engagetype = dataReader["engagetype"].ToString();
                            current_date = dataReader["current_date"] == null ? "" : dataReader["current_date"].ToString();
                            active_from = TimeSpan.Parse(dataReader["active_from"].ToString());
                            active_to = TimeSpan.Parse(dataReader["active_to"].ToString());
                        }
                    }

                    //check xem DateTime.Now co nam trong time range k

                    if (!AIOHelper.GlobalHelper.Timespan_IsTimeInRange(DateTime.Now, active_from, active_to))
                    {
                        //neu khong thi continue
                        continue;
                    }

                    //check xem da generate stat chua
                    if (current_date != "")
                    {
                        if (AIOHelper.GlobalHelper.DateTime_IsTimeInRange(DateTime.Parse(current_date), active_from, active_to))
                        {
                            //generate stat roi
                            continue;
                        }

                        //chua generate stats
                    }
                    else
                    {
                        //== "" tuc la chua generate stat
                    }

                    //generate stats
                    string time_per_day = "";

                    command_query =
                        @"select [timeperday] from engagesettings where [type]=@engagetype";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@engagetype", System.Data.SqlDbType.VarChar).Value = engagetype;

                        time_per_day = command.ExecuteScalar().ToString();
                    }

                    int from = int.Parse(time_per_day.Split(new char[] { '-' })[0]);
                    int to = int.Parse(time_per_day.Split(new char[] { '-' })[1]);

                    int set_timespend = (new Random(Guid.NewGuid().GetHashCode())).Next(from, to);

                    //update to db

                    command_query =
                            @"update account_info set [current_date]=getdate(), [current_timespend]=0, [target_timedate] = @timespend where [accountId]=@id";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@timespend", System.Data.SqlDbType.Int).Value = set_timespend;
                        command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                        command.ExecuteNonQuery();
                    }
                }
            }

        }
    }
}

