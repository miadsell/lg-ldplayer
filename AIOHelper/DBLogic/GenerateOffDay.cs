﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.DBLogic
{
    static class GenerateOffDay
    {
        static public void Do()
        {
            //check if GenerateOffDay in last 15 minutes

            var command_query = "";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                command_query =
                    @"if not exists
		                    (select *
		                    from actionlog
		                    where
			                    [action]='GenerateOffDay' and
			                    DATEDIFF(minute,do_at,getdate())<15)
                    begin
	                    insert into actionlog (accountid,[action],[value]) 
	                    values (155,'GenerateOffDay',@pc_devices)
                    end";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@pc_devices", System.Data.SqlDbType.VarChar).Value = "pc_devices" + Program.form.pc_devices;

                    int affect_row = command.ExecuteNonQuery();
                    if (affect_row != 1)
                        return;
                }


                //GenerateOffDay
                int dayofweek;
                string firstday_week;

                command_query =
                    @"declare @dayofweek int = DATEPART(weekday, getdate())

                    select @dayofweek";

                using (var command = new SqlCommand(command_query, conn))
                {
                    dayofweek = int.Parse(command.ExecuteScalar().ToString());
                }

                if (dayofweek > 1)
                {
                    command_query =
                        @"declare @firstday_week datetime = DATEADD(day, -1 * (@dayofweek-1), getdate())
                        select @firstday_week";
                }
                else
                {
                    command_query =
                        @"declare @firstday_week datetime = @firstday_week = getdate()
                        select @firstday_week";
                }

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@dayofweek", System.Data.SqlDbType.Int).Value = dayofweek;
                    firstday_week = command.ExecuteScalar().ToString();
                }

                //get list accountId that were GenerateOffDay this week

                List<string> GenerateOffDay_accountIds = new List<string>();

                command_query =
                    @"select * from actionlog
                    where
	                    [action]='dayoffweek' and
	                    DATEDIFF(day,@firstday_week,do_at)>=0";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@firstday_week", System.Data.SqlDbType.DateTime).Value = firstday_week;
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            GenerateOffDay_accountIds.Add(dataReader["accountId"].ToString());
                        }
                    }
                }

                //list all accountIds
                List<string> accountIds = new List<string>();

                command_query =
                    @"select [id]
                    from ig_account";

                using (var command = new SqlCommand(command_query, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            accountIds.Add(dataReader["id"].ToString());
                        }
                    }
                }

                List<string> need_GenerateOffDay_accountIds = accountIds.Except(GenerateOffDay_accountIds).ToList();

                //generate off day
                foreach(var id in need_GenerateOffDay_accountIds)
                {
                    int offday = (new Random(Guid.NewGuid().GetHashCode())).Next(dayofweek, 8);

                    command_query =
                        @"insert into actionlog (accountid,[action],[value]) values (@accountId,'dayoffweek',@day_off)";

                    using(var command=new SqlCommand(command_query,conn))
                    {
                        command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = id;
                        command.Parameters.Add("@day_off", System.Data.SqlDbType.VarChar).Value = offday;
                        command.ExecuteNonQuery();
                    }
                }

            }
        }
    }
}
