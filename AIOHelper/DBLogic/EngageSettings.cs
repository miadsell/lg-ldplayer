﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.DBLogic
{
    /// <summary>
    /// help analysis mass task history
    /// </summary>
    static class EngageSettings
    {
        static public List<dynamic> HistoryOfMassTask(string accountId, string history_name)
        {
            switch(history_name)
            {
                case "engagetype":
                    return EngageType(accountId);
                case "following-history":
                    return MassHistory(accountId, "following-history");
                case "likeuserpost-history":
                    return MassHistory(accountId, "likeuserpost-history");
                case "massdm-history":
                    return MassHistory(accountId, "massdm-history");
                case "commentpost-history":
                    return MassHistory(accountId, "commentpost-history");
                default:
                    throw new System.ArgumentException("Not recognize this history");
            }
        }

        #region --History Func--

        static private List<dynamic> EngageType(string accountId)
        {
            List<dynamic> histories = new List<dynamic>();

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                string command_query =
                    @"select [engagetype]
		            from account_info
		            where [accountId]=@accountId";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.VarChar).Value = accountId;
                    using(var dataReader=command.ExecuteReader())
                    {
                        dataReader.Read();

                        histories.Add(dataReader["engagetype"].ToString());
                    }
                }
            }

            return histories;
        }

        static private List<dynamic> MassHistory(string accountId, string history_name)
        {
            List<dynamic> histories = new List<dynamic>();

            string action = history_name.Replace("-history", "");

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                string command_query =
                    @"select [action], count(*) times
		            from actionlog
		            where 
			            [action] like '" + action + @"-%' and
			            [accountid]=@accountId
		            group by [action]";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.VarChar).Value = accountId;
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            histories.Add(new
                            {
                                action = dataReader["action"].ToString(),
                                times = dataReader["times"].ToString()
                            });
                        }
                    }
                }
            }

            return histories;
        }


        #endregion
    }
}
