﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.DBLogic
{
    static class GetTargetAccount_SpecificTasks
    {//just use this class to store default code, for customize, please point/reference to DBDirectCenter

        static private string GetTargetAccount_SpecificTasks_lock = "GetTargetAccount_SpecificTasks";

        #region --LikingUserPost--

        //DBDirectCenter class:  GetTargetAccount_SpecificTasks_LikingUserPost

        static public dynamic LikingUserPost(dynamic ig_account, bool is_take_data = true)
        {
            var aio_settings = ig_account.aio_settings;

            string accountId = ig_account.id;
            string project = aio_settings.Project;
            string type = aio_settings.Task_LikeUserPost.data_type;

            lock (GetTargetAccount_SpecificTasks_lock)
            {
                if (type == "")
                {
                    return LikingUserPost_Default(accountId, is_take_data);
                }

                return AIOHelper.DBDirectCenter.GetTargetAccount_SpecificTasks_LikingUserPost.RunByCenter(accountId, project, type, is_take_data);
            }
        }


        /// <summary>
        /// like post of people what was following by me
        /// </summary>
        static private dynamic LikingUserPost_Default(string accountId, bool is_take_data)
        {
            List<string> target_accounts = new List<string>();

            dynamic profile = null;
            string choiced_account = null;

            string command_query;

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                //get list scrape users are followed by accountId

                command_query =
                @"SELECT [target_account]
                FROM actionlog a1
                WHERE a1.[accountid]=@accountId
                AND a1.[action]='following_task'
                  AND(datediff(MINUTE,
                                  (SELECT top 1 [do_at]
                                   FROM actionlog a2
                                   WHERE a2.[action] LIKE 'likeuserpost_%'
                                     AND a2.[accountid]= @accountId
                                     AND a2.target_account= a1.target_account
                                   ORDER BY id DESC),getdate())>180 --180 minutes

                       OR
                         (SELECT top 1 [do_at]
                          FROM actionlog a2
                          WHERE a2.[action] LIKE 'likeuserpost_%'
                            AND a2.[accountid]= @accountId
                            AND a2.target_account= a1.target_account
                          ORDER BY id DESC) IS NULL )
                  AND a1.target_account IS NOT NULL";

                PLAutoHelper.SQLHelper.Execute(() =>
                {
                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    target_accounts.Add(dataReader["target_account"].ToString());
                                }
                            }
                        }
                    }
                }/*, "scrapeusers"*/);
            }

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();
                //chon nhung account chua tuong tac voi bai moi nhat
                command_query =
                @"SELECT count([UserName])
                FROM scrapeusers
                WHERE ([likeuserpost] is null or [likeuserpost]='' or [likeuserpost]!=CONVERT(varchar,[RecentPost],
                       21))
                  AND [UserName]=@target_account
                  AND [IsPrivate]='False'
                  AND [FeedCount]>0
                  AND ([status] is null or [status] not like 'Claimed%')";

                foreach (var a in target_accounts)
                {
                    int count = 0;
                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@target_account", System.Data.SqlDbType.VarChar).Value = a;
                            count = int.Parse(command.ExecuteScalar().ToString());

                        }
                    }/*, "scrapeusers"*/);

                    if (count == 1)
                    {
                        choiced_account = a;
                        break;
                    }

                }
            }

            if (choiced_account == null)
            {
                //chon nhung account co lan tuong tac gan nhat sort by asc de like cac post old
                command_query =
                    @"SELECT top 1 [target_account]
                    FROM actionlog
                    WHERE [accountid]=@accountId
                      AND [action] LIKE 'LikeUserPost_%'
                      AND [target_account] IN
                        ( SELECT a2.[target_account]
                         FROM instagram_ww_db.dbo.actionlog a2
                         WHERE a2.[accountid]=@accountId
                           AND a2.[action]='following_task' )
                    ORDER BY id ASC";

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();
                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                            using (var dataReader = command.ExecuteReader())
                            {
                                if (dataReader.HasRows)
                                {
                                    dataReader.Read();
                                    choiced_account = dataReader["target_account"].ToString();
                                }
                            }
                        }
                    }/*, "scrapeusers"*/);
                }
            }

            if (choiced_account != null && is_take_data)
            {
                command_query =
                    @"update scrapeusers set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"' where [UserName] = @user
                    select * from scrapeusers where [UserName] = @user";

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();
                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@user", System.Data.SqlDbType.VarChar).Value = choiced_account;
                            using (var dataReader = command.ExecuteReader())
                            {
                                dataReader.Read();

                                var pk = long.Parse(dataReader["pk"].ToString());
                                var username = dataReader["UserName"].ToString();

                                profile = new
                                {
                                    pk,
                                    username
                                };

                            }
                        }
                    }/*, "scrapeusers"*/);
                }
            }

            return profile;
        }

        #endregion

        #region --FollowingUser--

        //DBDirectCenter class:  

        static public dynamic FollowingUser(dynamic ig_account, bool is_take_data = true)
        {
            Stopwatch watch_takedata = new Stopwatch();
            watch_takedata.Start();


            var aio_settings = ig_account.aio_settings;

            string accountId = ig_account.id;
            string project = aio_settings.Project;
            string type = aio_settings.Task_FollowingUser.data_type;

            dynamic data = null;

            //lock (GetTargetAccount_SpecificTasks_lock)
            {
                if (type == "")
                {
                    data = FollowingUser_Default(accountId, is_take_data);
                }
                else
                    data = AIOHelper.DBDirectCenter.GetTargetAccount_SpecificTasks_FollowingUser.RunByCenter(accountId, project, type, is_take_data);
            }

            if (is_take_data)
                IGHelpers.DBHelpers.AddActionLogByID(ig_account.id, "following_takedata_time", watch_takedata.Elapsed.TotalSeconds.ToString());

            return data;
        }


        static private List<dynamic> FollowUser_Default_target_accounts = new List<dynamic>();

        static private dynamic FollowingUser_Default(string accountId, bool is_take_data)
        {
            //string command_query = "";

            //dynamic profile = null;

            //using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
            //{
            //    conn.Open();


            //    while (true)
            //    {
            //        command_query =
            //            @"SELECT top 1 *
            //        FROM scrapeusers
            //        WHERE [FollowingCount]<1000
            //          AND [FollowerCount]<1000
            //          AND ([following] is null or [following]='')
            //          AND ([likeuserpost] is null or [likeuserpost]='')
            //          AND ([status] is null or [status] not like 'Claimed%')";

            //        PLAutoHelper.SQLHelper.Execute(() =>
            //        {

            //            using (var command = new SqlCommand(command_query, conn))
            //            {
            //                using (var dataReader = command.ExecuteReader())
            //                {
            //                    if (dataReader.HasRows)
            //                    {

            //                        dataReader.Read();

            //                        var pk = long.Parse(dataReader["pk"].ToString());
            //                        var username = dataReader["UserName"].ToString();

            //                        profile = new
            //                        {
            //                            pk,
            //                            username
            //                        };
            //                    }
            //                }
            //            }
            //        }/*, "scrapeusers"*/);

            //        if (profile != null && is_take_data)
            //        {
            //            PLAutoHelper.SQLHelper.Execute(() =>
            //            {
            //                using (var sqlTransaction = conn.BeginTransaction())
            //                {

            //                    command_query =
            //                        @"update scrapeusers set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"' where [UserName]=@UserName and ([status] is null or [status]='')";

            //                    using (var command = new SqlCommand(command_query, conn, sqlTransaction))
            //                    {
            //                        command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = profile.username;
            //                        var affected_row = command.ExecuteNonQuery();


            //                        sqlTransaction.Commit();
            //                        if (affected_row == 1)
            //                        {
            //                        }
            //                        else
            //                        {
            //                            profile = null;
            //                        }
            //                    }

            //                }
            //            }/*, "scrapeusers"*/);


            //            if (profile != null)
            //                break;
            //        }
            //        else
            //            break;

            //    }
            //}

            //return profile;

            string command_query = "";

            dynamic profile = null;

            lock ("FollowUser_Default")
            {

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();


                    while (true)
                    {

                        if (FollowUser_Default_target_accounts.Count > 0)
                        {
                            profile = FollowUser_Default_target_accounts.First();
                        }
                        else
                        {
                            command_query =
                                @"SELECT top 1000 *
                                    FROM scrapeusers
                                    WHERE [FollowingCount]<1000
                                      AND [FollowerCount]<1000
                                      AND ([following] is null or [following]='')
                                      AND ([likeuserpost] is null or [likeuserpost]='')
                                      AND ([status] is null or [status] not like 'Claimed%')
                                order by first_added desc";

                            PLAutoHelper.SQLHelper.Execute(() =>
                            {
                                using (var command = new SqlCommand(command_query, conn))
                                {
                                    using (var dataReader = command.ExecuteReader())
                                    {

                                        if (dataReader.HasRows)
                                        {
                                            while (dataReader.Read())
                                            {

                                                var pk = long.Parse(dataReader["pk"].ToString());
                                                var username = dataReader["UserName"].ToString();

                                                FollowUser_Default_target_accounts.Add(new
                                                {
                                                    pk,
                                                    username
                                                });
                                            }
                                        }
                                    }
                                }
                            }/*, "following"*/);

                            if (FollowUser_Default_target_accounts.Count > 0)
                            {

                                FollowUser_Default_target_accounts = FollowUser_Default_target_accounts.OrderBy(a => Guid.NewGuid()).ToList();

                                profile = FollowUser_Default_target_accounts.First();
                            }
                        }

                        if (profile != null && is_take_data)
                        {
                            command_query =
                                @"update scrapeusers set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + "' where [UserName]=@UserName and ([status] is null or [status]='') and ([following] is null or [following]='')";

                            PLAutoHelper.SQLHelper.Execute(() =>
                            {
                                using (var sqlTransaction = conn.BeginTransaction())
                                {
                                    using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    {
                                        command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = profile.username;
                                        var affected_row = command.ExecuteNonQuery();

                                        sqlTransaction.Commit();

                                        //remove from list event it's claimed by this pc_devices or another devices

                                        FollowUser_Default_target_accounts.RemoveAll(x => x.username == profile.username);

                                        if (affected_row == 1)
                                        {
                                        }
                                        else
                                        {
                                            profile = null;
                                        }
                                    }
                                }
                            }/*, "scrapeusers"*/);


                            if (profile != null)
                                break;
                        }
                        else
                            break;
                    }

                }
            }

            return profile;
        }


        #endregion

        #region --DoEngage--



        #endregion

        #region --CommentPost--


        /// <summary>
        /// take post to do comment
        /// </summary>
        /// <param name="ig_account"></param>
        /// <param name="is_take_data"></param>
        /// <returns></returns>
        static public dynamic CommentPost(dynamic ig_account, bool is_take_data = true)
        {
            var aio_settings = ig_account.aio_settings;

            string accountId = ig_account.id;
            string project = aio_settings.Project;
            string data_type = aio_settings.Task_CommentPost.data_type;

            dynamic data = null;

            if (data_type == "")
            {
                data = CommentPost_Default(ig_account, is_take_data);
            }
            else
                data = AIOHelper.DBDirectCenter.GetTargetAccount_SpecificTasks_CommentPost.RunByCenter(accountId, project, data_type, is_take_data);

            if(data!=null)
                data.actionlog = aio_settings.Task_CommentPost.actionlog.ToString();

            return data;
        }

        static private dynamic CommentPost_Default(dynamic ig_account, bool is_take_data)
        {
            string command_query = "";

            dynamic post = null;

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();


                while (true)
                {
                    command_query =
                        @"select top 1 *
                        from comment_posts
                        where
	                        DATEDIFF(day,creation_date,getdate())<3 and
	                        ([status] is null or [status]='')
                        order by creation_date desc";

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            using (var dataReader = command.ExecuteReader())
                            {
                                if (dataReader.HasRows)
                                {

                                    dataReader.Read();

                                    var posturl = dataReader["posturl"].ToString();

                                    post = new
                                    {
                                        posturl
                                    };
                                }
                            }
                        }
                    }/*, "scrapeusers"*/);

                    if (post != null && is_take_data)
                    {
                        PLAutoHelper.SQLHelper.Execute(() =>
                        {
                            using (var sqlTransaction = conn.BeginTransaction())
                            {

                                command_query =
                                    @"update comment_posts set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"' where [posturl]=@posturl";

                                using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                {
                                    command.Parameters.Add("@posturl", System.Data.SqlDbType.VarChar).Value = post.posturl;
                                    var affected_row = command.ExecuteNonQuery();


                                    sqlTransaction.Commit();
                                    if (affected_row == 1)
                                    {
                                    }
                                    else
                                    {
                                        post = null;
                                    }
                                }

                            }
                        }/*, "scrapeusers"*/);


                        if (post != null)
                            break;
                    }
                    else
                        break;

                }
            }

            return post;
        }


        #endregion

        #region --MassDM--

        static public dynamic MassDM(dynamic ig_account, bool is_take_data = true)
        {
            var aio_settings = ig_account.aio_settings;

            string accountId = ig_account.id;
            string project = aio_settings.Project;


            //get list Task_MassDM type
            List<string> Task_MassDM_type = IGHelpers.Settings.Settings_ListUnderChilds_Name(Newtonsoft.Json.JsonConvert.SerializeObject(aio_settings), "$..Task_MassDM");

            List<dynamic> types = new List<dynamic>();

            foreach(var item in Task_MassDM_type)
            {
                types.Add(new
                {
                    type = item,
                    action_type = aio_settings.Task_MassDM[item].action_type.ToString(),
                    data_type = aio_settings.Task_MassDM[item].data_type.ToString(),
                    priority = int.Parse(aio_settings.Task_MassDM[item].priority.ToString()),
                    running = bool.Parse(aio_settings.Task_MassDM[item].running.ToString()),
                    actionlog = aio_settings.Task_MassDM[item].actionlog.ToString()
                });
            }

            types = types.Where(t => t.running == true).OrderByDescending(t => t.priority).ToList();

            //sort by priority and running: true

            dynamic choiced_target_profile = null;
            string choiced_user = null;

            foreach (var t in types)
            {
                string data_type = t.data_type;

                switch(data_type)
                {
                    case "":
                        choiced_user = MassDM_Default(accountId, is_take_data);
                        break;
                    default:
                        choiced_user = DBDirectCenter.GetTargetAccount_SpecificTasks_MassDM.RunByCenter(accountId, project, data_type, is_take_data);
                        break;

                }

                if (choiced_user != null)
                {
                    choiced_target_profile = new
                    {
                        user = choiced_user,
                        Task_MassDM_type = t.type,
                        actionlog = t.actionlog
                    };
                    break;
                }
            }

            return choiced_target_profile;
        }

        static private string MassDM_Default(string accountId, bool is_take_data)
        {
            string choiced_username = null;

            string command_query = null;

            List<string> target_accounts = new List<string>();

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                command_query =
                @"select [target_account]
                from actionlog
                where
	                    [action]='followback' and
	                    [accountid]=@accountId
                order by do_at desc";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            target_accounts.Add(dataReader["target_account"].ToString());
                        }
                    }
                }

            }

            List<string> accounts_were_sentdm = new List<string>();

            //get list username that do massdm by accountId
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                command_query =
                    @"select *
                    from actionlog
                    where
	                    [action]='massdm_task' and
	                    [accountid]=@accountId";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            accounts_were_sentdm.Add(dataReader["target_account"].ToString());
                        }
                    }
                }
            }

            //except list
            target_accounts = target_accounts.Except(accounts_were_sentdm).ToList();

            if (target_accounts.Count > 0)
                choiced_username = target_accounts[0];

            if (choiced_username != null && is_take_data)
            {
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();
                    command_query =
                                    @"update scrapeusers set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"' where [UserName]=@UserName and ([status] is null or [status]='')";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = choiced_username;
                        var affected_row = command.ExecuteNonQuery();
                    }
                }
            }

            return choiced_username;
        }

        #endregion
    }
}
