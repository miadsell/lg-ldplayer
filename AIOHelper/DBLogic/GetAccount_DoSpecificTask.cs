﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.DBLogic
{
    static class GetAccount_DoSpecificTask
    {

        #region --LikingUserPost--

        static DateTime next_GetAccount_DoSpecificTask_LikingUserPost = new DateTime();

        static public dynamic GetAccount_DoSpecificTask_LikingUserPost()
        {
            try
            {
                ThreadHelper.MonitorDB.semaphore_monitor_processdb.WaitOne();

                if (next_GetAccount_DoSpecificTask_LikingUserPost > DateTime.Now)
                    return null;

                dynamic ig_account = null;
                string choicedID = null;

                int dayofweek = DayOfWeek();

                DateTime firstDayWeek = FirstDayWeek();


                //get available account to do action
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    //using (var sqlTransaction = conn.BeginTransaction())
                    //{
                    List<string> availableAccountIds = AvailableAccountToDoAction_AIO(new SqlCommand("", conn));

                    if (availableAccountIds.Count == 0)
                        return null;

                    //filter accountId turn of likepost

                    var command_query =
                        @"select [accountId]
                    from specifictasks with (tablock)
                    where [likepost] = 'Do'";

                    List<string> tempLikePost_accountIds = new List<string>();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    tempLikePost_accountIds.Add(dataReader["accountId"].ToString());
                                }
                            }
                        }
                    }

                    availableAccountIds = availableAccountIds.Where(s => tempLikePost_accountIds.Contains(s)).ToList();

                    //get list account offday
                    var offday_Ids = OffThisDay_AccountIds(firstDayWeek, new SqlCommand("", conn));

                    //get list account IsBlockorUnsualinXDays
                    var block_unsual_ids = BlockorUnsualinXDays_AccountIds(new SqlCommand("", conn));

                    //exclude

                    availableAccountIds = availableAccountIds.Except(offday_Ids).Except(block_unsual_ids).ToList();

                    if (availableAccountIds.Count == 0)
                        return null;

                    //check every accountId

                    foreach (var id in availableAccountIds)
                    {
                        ////check if is off day
                        //if (IsOffDayById(id, firstDayWeek, new SqlCommand("", conn)))
                        //    continue;

                        //if (IsBlockorUnsualinXDays(id, new SqlCommand("", conn), 3))
                        //    continue;

                        //get next session time

                        command_query =
                            @"SELECT top 1 [value]
                        FROM actionlog
                        WHERE[accountid] = @temp_accountId
                          AND DATEDIFF(DAY, [do_at], getdate())= 0
                          AND[action] = 'likeuserpost_next_session'
                        ORDER BY id DESC";

                        string next_session_time_str = null;

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;
                            using (var dataReader = command.ExecuteReader())
                            {
                                if (dataReader.HasRows)
                                {
                                    dataReader.Read();
                                    next_session_time_str = dataReader["value"].ToString();
                                }
                            }
                        }

                        if (next_session_time_str == null)
                        {
                            //--set next_sesstion_time -- thuc ra day la session dau tien trong ngay

                            //--random blocked_session_count

                            //--random 1-3 session per blocked for first block
                            command_query =

                                    @"insert into actionlog([accountid],[action],[value]) values(@temp_accountId,'block_likeuserpost_session_count', ABS(CHECKSUM(NEWID()) % 3) + 1)  

                                    insert into actionlog([accountid], [action], [value]) values(@temp_accountId,'likeuserpost_next_session', CONVERT(varchar, dateadd(millisecond, cast(14400000 * RAND() as int), getdate()),21))"; //randome start time tu thoi diem hien tai toi 4 tieng nua
                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;
                                command.ExecuteNonQuery();
                            }

                            continue;

                        }
                        else
                        {
                            var next_session_time = DateTime.Parse(next_session_time_str);

                            if (DateTime.Now.Subtract(next_session_time).TotalSeconds < 0)
                                continue;
                        }

                        //--neu thoa dieu kien next_session_time thi check if currentdate_likeuserpost < target_likeuserpost

                        int current_date_likeuserpost = 0;

                        command_query =
                            @"select count(*) from actionlog 
                        where
                            [action] like 'likeuserpost_task%' and
                              [accountid] = @temp_accountId and

                            DATEDIFF(day,[do_at], getdate()) = 0";

                        using (var command = new SqlCommand(command_query, conn))
                        {

                            command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;

                            current_date_likeuserpost = int.Parse(command.ExecuteScalar().ToString());
                        }

                        int target_likeuserpost = 0;

                        command_query =
                            @"select [value] from actionlog
                        where
                                [action] = 'target_likeuserpost' and
                                  [accountid] = @temp_accountId and
                                DATEDIFF(day,[do_at], getdate()) = 0";
                        using (var command = new SqlCommand(command_query, conn))
                        {

                            command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;

                            var value = command.ExecuteScalar();

                            if (value != null)
                            {
                                target_likeuserpost = int.Parse(value.ToString());
                                if (current_date_likeuserpost < target_likeuserpost)
                                {
                                    choicedID = id;
                                }
                            }
                            else
                            {
                                //tuc la chua chay lan nao hom nay => choose it
                                choicedID = id;
                            }
                        }


                        if (choicedID != null)
                        {

                            bool isclaimed = ClaimedAccountByID(new SqlCommand("", conn), choicedID);

                            if (isclaimed)
                                break;
                            else
                                choicedID = null;

                            //command_query =
                            //    "update ig_account set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + "' where [id] = @id and ([status]='' or [status] is null)";
                            //using (var command = new SqlCommand(command_query, conn))
                            //{
                            //    command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = choicedID;
                            //    var affected_row = command.ExecuteNonQuery();
                            //    if (affected_row == 1)
                            //    {
                            //        break;
                            //    }
                            //    else
                            //    {
                            //        choicedID = null;
                            //    }
                            //}
                        }
                    }
                    //}
                }

                if (choicedID != null)
                    ig_account = DynamicIGAccountByID(choicedID);

                if (ig_account == null)
                {
                    next_GetAccount_DoSpecificTask_LikingUserPost = DateTime.Now.AddMinutes(1);
                }

                return ig_account;
            }
            finally
            {
                Thread.Sleep(1000);
                ThreadHelper.MonitorDB.semaphore_monitor_processdb.Release();
            }
        }



        #endregion

        #region --FollowingUser--


        static DateTime next_GetAccount_DoSpecificTask_FollowingUser = new DateTime();

        /// <summary>
        /// get account to do FollowingUserTask
        /// </summary>
        /// <returns></returns>
        static public dynamic GetAccount_DoSpecificTask_FollowingUser()
        {
            try
            {
                ThreadHelper.MonitorDB.semaphore_monitor_processdb.WaitOne();

                dynamic ig_account = null;
                string choicedID = null;

                if (next_GetAccount_DoSpecificTask_FollowingUser > DateTime.Now)
                    return null;

                int dayofweek = DayOfWeek(); ;
                DateTime firstDayWeek = FirstDayWeek();

                //get available account to do action
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();
                    //using (var sqlTransaction = conn.BeginTransaction())
                    //{
                    //filter accountId turn of likepost

                    List<string> availableAccountIds = AvailableAccountToDoAction_AIO(new SqlCommand("", conn));

                    if (availableAccountIds.Count == 0)
                        return null;

                    var command_query =
                    @"select [accountId]
                    from specifictasks
                    where [followinguser] = 'Do'";

                    List<string> tempFollowingUser_accountIds = new List<string>();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    tempFollowingUser_accountIds.Add(dataReader["accountId"].ToString());
                                }
                            }
                        }
                    }

                    availableAccountIds = availableAccountIds.Where(s => tempFollowingUser_accountIds.Contains(s)).ToList();

                    //get account following_finished

                    command_query =
                        @"select [accountid]
                        from actionlog
                        where
	                        [action]='following_finished' and
							DATEDIFF(day,do_at,getdate())=0";

                    List<string> following_finished_ids = new List<string>();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                following_finished_ids.Add(dataReader["accountId"].ToString());
                            }

                        }
                    }

                    //except finished

                    availableAccountIds = availableAccountIds.Except(following_finished_ids).ToList();

                    //shuffel

                    availableAccountIds = availableAccountIds.OrderBy(a => Guid.NewGuid()).ToList();

                    //get list account offday
                    var offday_Ids = OffThisDay_AccountIds(firstDayWeek, new SqlCommand("", conn));

                    //get list account IsBlockorUnsualinXDays
                    var block_unsual_ids = BlockorUnsualinXDays_AccountIds(new SqlCommand("", conn));

                    //exclude

                    availableAccountIds = availableAccountIds.Except(offday_Ids).Except(block_unsual_ids).ToList();


                    if (availableAccountIds.Count == 0)
                        return null;

                    //check every accountId

                    foreach (var id in availableAccountIds)
                    {
                        ////check if is off day
                        //if (IsOffDayById(id, firstDayWeek, new SqlCommand("", conn)))
                        //    continue;

                        //if (IsBlockorUnsualinXDays(id, new SqlCommand("", conn), 3))
                        //    continue;

                        //get next session time
                        command_query =
                            @"SELECT top 1 [value]
                        FROM actionlog
                        WHERE[accountid] = @temp_accountId
                          AND DATEDIFF(DAY, [do_at], getdate())= 0
                          AND[action] = 'following_next_session'
                        ORDER BY id DESC";

                        string next_session_time_str = null;

                        try
                        {
                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;
                                using (var dataReader = command.ExecuteReader())
                                {
                                    if (dataReader.HasRows)
                                    {
                                        dataReader.Read();
                                        next_session_time_str = dataReader["value"].ToString();
                                    }
                                }
                            }
                        }
                        catch(Exception ex)
                        {
                            ADBSupport.LDHelpers.HandleException("nodevice", "GetAccount_DoSpecificTask_FollowingUser" + "|" + ex.Message, ex.StackTrace);
                            Thread.Sleep(2000);
                            continue;
                        }

                        if (next_session_time_str == null)
                        {
                            //--set next_sesstion_time -- thuc ra day la session dau tien trong ngay

                            //--random blocked_session_count

                            //--random 1-3 session per blocked for first block
                            command_query =

                                    @"insert into actionlog ([accountid],[action],[value]) values (@temp_accountId,'block_following_session_count',ABS(CHECKSUM(NEWID()) % 3) + 1)  --random 3-10 session per blocked

		                        insert into actionlog ([accountid],[action],[value]) values (@temp_accountId,'following_next_session',CONVERT(varchar, dateadd(millisecond, cast(3600000 * RAND() as int), getdate()),21))";// --randome start time tu thoi diem hien tai toi 1 tieng nua"

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;
                                command.ExecuteNonQuery();
                            }

                            continue;

                        }
                        else
                        {
                            var next_session_time = DateTime.Parse(next_session_time_str);

                            if (DateTime.Now.Subtract(next_session_time).TotalSeconds < 0)
                                continue;
                        }

                        //neu thoa dieu kien next_session_time thi check if currentdate_following < target_following
                        int current_date_followinguser = 0;

                        command_query =
                            @"select count(*) from actionlog 
                        where
                            [action] = 'following_task' and
                              [accountid] = @temp_accountId and

                            DATEDIFF(day,[do_at], getdate()) = 0";

                        using (var command = new SqlCommand(command_query, conn))
                        {

                            command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;

                            current_date_followinguser = int.Parse(command.ExecuteScalar().ToString());
                        }

                        int target_followinguser = 0;

                        command_query =
                            @"select [value] from actionlog
                        where
                                [action] = 'target_following' and
                                  [accountid] = @temp_accountId and
                                DATEDIFF(day,[do_at], getdate()) = 0";
                        using (var command = new SqlCommand(command_query, conn))
                        {

                            command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;

                            var value = command.ExecuteScalar();

                            if (value != null)
                            {
                                target_followinguser = int.Parse(value.ToString());
                                if (current_date_followinguser < target_followinguser)
                                {
                                    choicedID = id;
                                    //break;
                                }
                                else
                                {
                                    //add finish log
                                    command_query =

                                    @"insert into actionlog ([accountid],[action]) values (@temp_accountId,'following_finished')";// --randome start time tu thoi diem hien tai toi 1 tieng nua"

                                    using (var command_finished = new SqlCommand(command_query, conn))
                                    {
                                        command_finished.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;
                                        command_finished.ExecuteNonQuery();
                                    }
                                }
                            }
                            else
                            {
                                //tuc la chua chay lan nao hom nay => choose it
                                choicedID = id;
                                //break;
                            }
                        }

                        if (choicedID != null)
                        {
                            bool isclaimed = ClaimedAccountByID(new SqlCommand("", conn), choicedID);

                            if (isclaimed)
                                break;
                            else
                                choicedID = null;

                            //command_query =
                            //    "update ig_account set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + "' where [id] = @id and ([status]='' or [status] is null)";
                            //using (var command = new SqlCommand(command_query, conn))
                            //{
                            //    command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = choicedID;
                            //    var affected_row = command.ExecuteNonQuery();

                            //    if (affected_row == 1)
                            //    {
                            //        break;
                            //    }
                            //    else
                            //    {
                            //        choicedID = null;
                            //    }
                            //}
                        }
                    }

                    //sqlTransaction.Commit();
                    //}
                }

                if (choicedID != null)
                    ig_account = DynamicIGAccountByID(choicedID);

                if (ig_account == null)
                {
                    next_GetAccount_DoSpecificTask_FollowingUser = DateTime.Now.AddMinutes(1);
                }

                return ig_account;
            }
            finally
            {
                Thread.Sleep(1000);
                ThreadHelper.MonitorDB.semaphore_monitor_processdb.Release();
            }
        }

        static public List<dynamic> GetAccount_DoSpecificTask_FollowingUser_ReturnList()
        {
            List<dynamic> following_getaccount = new List<dynamic>();

            try
            {
                ThreadHelper.MonitorDB.semaphore_monitor_processdb.WaitOne();

                dynamic ig_account = null;
                string choicedID = null;

                if (next_GetAccount_DoSpecificTask_FollowingUser > DateTime.Now)
                    return null;

                int dayofweek = DayOfWeek(); ;
                DateTime firstDayWeek = FirstDayWeek();

                //get available account to do action
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    List<string> availableAccountIds = AvailableAccountToDoAction_AIO(new SqlCommand("", conn));

                    if (availableAccountIds.Count == 0)
                        return null;

                    var command_query =
                    @"select [accountId]
                    from specifictasks
                    where [followinguser] = 'Do'";

                    List<string> tempFollowingUser_accountIds = new List<string>();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    tempFollowingUser_accountIds.Add(dataReader["accountId"].ToString());
                                }
                            }
                        }
                    }

                    availableAccountIds = availableAccountIds.Where(s => tempFollowingUser_accountIds.Contains(s)).ToList();

                    //shuffel

                    availableAccountIds = availableAccountIds.OrderBy(a => Guid.NewGuid()).ToList();

                    //get list account offday
                    var offday_Ids = OffThisDay_AccountIds(firstDayWeek, new SqlCommand("", conn));

                    //get list account IsBlockorUnsualinXDays
                    var block_unsual_ids = BlockorUnsualinXDays_AccountIds(new SqlCommand("", conn));

                    //exclude

                    availableAccountIds = availableAccountIds.Except(offday_Ids).Except(block_unsual_ids).ToList();


                    if (availableAccountIds.Count == 0)
                        return null;


                    //check every accountId

                    foreach (var id in availableAccountIds)
                    {
                        choicedID = null;

                        ////check if is off day
                        //if (IsOffDayById(id, firstDayWeek, new SqlCommand("", conn)))
                        //    continue;

                        //if (IsBlockorUnsualinXDays(id, new SqlCommand("", conn), 3))
                        //    continue;

                        //get next session time
                        command_query =
                            @"SELECT top 1 [value]
                        FROM actionlog
                        WHERE[accountid] = @temp_accountId
                          AND DATEDIFF(DAY, [do_at], getdate())= 0
                          AND[action] = 'following_next_session'
                        ORDER BY id DESC";

                        string next_session_time_str = null;

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;
                            using (var dataReader = command.ExecuteReader())
                            {
                                if (dataReader.HasRows)
                                {
                                    dataReader.Read();
                                    next_session_time_str = dataReader["value"].ToString();
                                }
                            }
                        }

                        if (next_session_time_str == null)
                        {
                            //--set next_sesstion_time -- thuc ra day la session dau tien trong ngay

                            //--random blocked_session_count

                            //--random 1-3 session per blocked for first block
                            command_query =

                                    @"insert into actionlog ([accountid],[action],[value]) values (@temp_accountId,'block_following_session_count',ABS(CHECKSUM(NEWID()) % 3) + 1)  --random 3-10 session per blocked

		                        insert into actionlog ([accountid],[action],[value]) values (@temp_accountId,'following_next_session',CONVERT(varchar, dateadd(millisecond, cast(3600000 * RAND() as int), getdate()),21))";// --randome start time tu thoi diem hien tai toi 1 tieng nua"

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;
                                command.ExecuteNonQuery();
                            }

                            continue;

                        }
                        else
                        {
                            var next_session_time = DateTime.Parse(next_session_time_str);

                            if (DateTime.Now.Subtract(next_session_time).TotalSeconds < 0)
                                continue;
                        }

                        //neu thoa dieu kien next_session_time thi check if currentdate_following < target_following
                        int current_date_followinguser = 0;

                        command_query =
                            @"select count(*) from actionlog 
                        where
                            [action] = 'following_task' and
                              [accountid] = @temp_accountId and

                            DATEDIFF(day,[do_at], getdate()) = 0";

                        using (var command = new SqlCommand(command_query, conn))
                        {

                            command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;

                            current_date_followinguser = int.Parse(command.ExecuteScalar().ToString());
                        }

                        int target_followinguser = 0;

                        command_query =
                            @"select [value] from actionlog
                        where
                                [action] = 'target_following' and
                                  [accountid] = @temp_accountId and
                                DATEDIFF(day,[do_at], getdate()) = 0";
                        using (var command = new SqlCommand(command_query, conn))
                        {

                            command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;

                            var value = command.ExecuteScalar();

                            if (value != null)
                            {
                                target_followinguser = int.Parse(value.ToString());
                                if (current_date_followinguser < target_followinguser)
                                {
                                    choicedID = id;
                                    //break;
                                }
                            }
                            else
                            {
                                //tuc la chua chay lan nao hom nay => choose it
                                choicedID = id;
                                //break;
                            }
                        }

                        if (choicedID != null)
                        {
                            following_getaccount.Add(DynamicIGAccountByID(choicedID));
                        }
                    }

                    //sqlTransaction.Commit();
                    //}
                }

                return following_getaccount;
            }
            finally
            {
                Thread.Sleep(1000);
                ThreadHelper.MonitorDB.semaphore_monitor_processdb.Release();
            }
        }

        #endregion

        #region --DoNormalUserTask--

        static DateTime next_DoNormalUserTask = new DateTime(); //if GetAccount_DoNormalUserTask return null, set next time by Add 1 minutes

        static public dynamic GetAccount_DoNormalUserTask()
        {
            try
            {
                ThreadHelper.MonitorDB.semaphore_monitor_processdb.WaitOne();

                if (next_DoNormalUserTask > DateTime.Now)
                    return null;

                dynamic ig_account = null;
                string choicedID = null;

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    List<string> availableAccountIds = AvailableAccountToDoAction_AIO(new SqlCommand("", conn));

                    if (availableAccountIds.Count == 0)
                        return null;

                    //filter account type: normaluser, source_status!=error

                    var idtbs = TableFromListString(availableAccountIds);

                    var command_query =
                        @"select ig_account.id
                    from (values " + idtbs + @") idtb(accountId)
                    join ig_account on idtb.accountId=ig_account.id
                    join source_normaluser on idtb.accountId=source_normaluser.accountId
                    where
	                    ig_account.type='normaluser' and
	                    (source_normaluser.source_status is null or source_normaluser.source_status='')";

                    List<string> filterAccountIds = new List<string>();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                filterAccountIds.Add(dataReader["id"].ToString());
                            }
                        }
                    }


                    //shuffel

                    filterAccountIds = filterAccountIds.OrderBy(a => Guid.NewGuid()).ToList();

                    //get account that meet requirement to update profile and still not update profile

                    idtbs = TableFromListString(filterAccountIds);

                    command_query =
                        @"select idtb.accountId
                    from (values " + idtbs + @") idtb(accountId)
                    join account_info on idtb.accountId=account_info.accountId
                    where
	                        [total_timespend]>15 and
	                        not exists (
		                        select [id] from actionlog
		                        where	
			                        [action]='updateprofile' and
			                        actionlog.accountid=idtb.accountId)";

                    List<string> meet_requirement_accountIds = new List<string>();

                    if (meet_requirement_accountIds.Count == 0)
                        return null;

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    meet_requirement_accountIds.Add(dataReader["accountId"].ToString());
                                }
                            }
                        }
                    }

                    foreach (var id in meet_requirement_accountIds)
                    {
                        command_query =
                            @"update ig_account
                                    set
                                     [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"'
                                    where
                                     ([status] is null or [status]='') and
                                     ([id]=@id)";
                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.CommandText = command_query;
                            command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

                            var affected_row = command.ExecuteNonQuery();

                            if (affected_row == 1)
                            {
                                choicedID = id;
                                goto EndFunc;
                            }
                            else
                                continue;
                        }
                    }

                    meet_requirement_accountIds.Clear();

                    //foreach (var id in filterAccountIds)
                    //{
                    //    command_query =
                    //        @"select count([accountId])
                    //    from account_info
                    //    where
                    //     [accountId]=@id and
                    //     [total_timespend]>15 and
                    //     not exists (
                    //      select [id] from actionlog
                    //      where	
                    //       [action]='updateprofile' and
                    //       actionlog.accountid=@id)";

                    //    using (var command = new SqlCommand(command_query, conn))
                    //    {
                    //        command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

                    //        var count = int.Parse(command.ExecuteScalar().ToString());

                    //        if (count == 0)
                    //            continue;
                    //        else
                    //        {

                    //            command_query =
                    //                @"update ig_account
                    //            set
                    //             [status]='Claimed'
                    //            where
                    //             ([status] is null or [status]='') and
                    //             ([id]=@id)";

                    //            command.CommandText = command_query;
                    //            command.Parameters.Clear();
                    //            command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

                    //            var affected_row = command.ExecuteNonQuery();

                    //            if (affected_row == 1)
                    //            {
                    //                choicedID = id;
                    //                goto EndFunc;
                    //            }
                    //            else
                    //                continue;
                    //        }
                    //    }
                    //}

                    //get account that meet requirement to publish post

                    //get account haven't posted anything

                    //get account that total_timespend>30 (minium timespend to publish post

                    command_query =
                        @"select idtb.accountId
                        from (values " + idtbs + @") idtb(accountId)
                        join account_info on idtb.accountId=account_info.accountId
                        where
	                            total_timespend>30";

                    List<string> timespend_over = new List<string>();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    timespend_over.Add(dataReader["accountId"].ToString());
                                }
                            }
                        }
                    }

                    //get list account that published post

                    command_query =
                        @"select distinct (actionlog.[accountid]) from actionlog
	                            where
		                            [action]='published_post'";
                    List<string> has_published = new List<string>();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    has_published.Add(dataReader["accountid"].ToString());
                                }
                            }
                        }
                    }

                    meet_requirement_accountIds = timespend_over.Where(p => !has_published.Any(p2 => p2 == p)).ToList();

                    //command_query =
                    //    @"select idtb.accountId
                    //    from (values " + idtbs + @") idtb(accountId)
                    //    join account_info on idtb.accountId=account_info.accountId
                    //    where
                    //         total_timespend>30 and
                    //      --not publish anything
                    //         (select count([id]) from actionlog
                    //         where
                    //          actionlog.accountid=idtb.accountId and
                    //          [action]='published_post')=0";

                    //PLAutoHelper.SQLHelper.Execute(() =>
                    //{
                    //    using (var command = new SqlCommand(command_query, conn))
                    //    {
                    //        using (var dataReader = command.ExecuteReader())
                    //        {
                    //            if (dataReader.HasRows)
                    //            {
                    //                while (dataReader.Read())
                    //                {
                    //                    meet_requirement_accountIds.Add(dataReader["accountId"].ToString());
                    //                }
                    //            }
                    //        }
                    //    }
                    //}, "GetAccount_DoNormalUserTask");

                    foreach (var id in meet_requirement_accountIds)
                    {
                        command_query =
                            @"update ig_account
                                    set
                                     [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"'
                                    where
                                     ([status] is null or [status]='') and
                                     ([id]=@id)";
                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.CommandText = command_query;
                            command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

                            var affected_row = command.ExecuteNonQuery();

                            if (affected_row == 1)
                            {
                                choicedID = id;
                                goto EndFunc;
                            }
                            else
                                continue;
                        }
                    }

                    meet_requirement_accountIds.Clear();

                //foreach (var id in filterAccountIds)
                //{
                //    command_query =
                //        @"
                //    select count([accountId])
                //    from account_info
                //    where
                //     [accountId]=@id and
                //     total_timespend>30 and
                //      --not publish anything
                //     (select count([id]) from actionlog
                //     where
                //      actionlog.accountid=@id and
                //      [action]='published_post')=0";

                //    using (var command = new SqlCommand(command_query, conn))
                //    {
                //        command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

                //        var count = int.Parse(command.ExecuteScalar().ToString());

                //        if (count == 0)
                //            continue;
                //        else
                //        {

                //            command_query =
                //                @"update ig_account
                //            set
                //             [status]='Claimed'
                //            where
                //             ([status] is null or [status]='') and
                //             ([id]=@id)";

                //            command.CommandText = command_query;
                //            command.Parameters.Clear();
                //            command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

                //            var affected_row = command.ExecuteNonQuery();

                //            if (affected_row == 1)
                //            {
                //                choicedID = id;
                //                goto EndFunc;
                //            }
                //            else
                //                continue;
                //        }
                //    }
                //}

                //if null, get account meet requirement next post
                NextPostRequirement:
                    command_query =
                        @"select idtb.accountId
                        from (values " + idtbs + @") idtb(accountId)
                        join account_info on idtb.accountId=account_info.accountId
                        where
	                            total_timespend>30 and
	                        --meet requirement about next publish
	                        datediff(second, CONVERT(datetime, (select top 1 [value] from actionlog where actionlog.accountid=idtb.accountId and [action]='next_publish_post' order by id desc),21),getdate())>0
	                        ";

                    try
                    {
                        PLAutoHelper.SQLHelper.Execute(() =>
                        {
                            using (var command = new SqlCommand(command_query, conn))
                            {
                                using (var dataReader = command.ExecuteReader())
                                {
                                    if (dataReader.HasRows)
                                    {
                                        while (dataReader.Read())
                                        {
                                            meet_requirement_accountIds.Add(dataReader["accountId"].ToString());
                                        }
                                    }
                                }
                            }
                        }, "GetAccount_DoNormalUserTask");
                    }
                    catch
                    {
                        Thread.Sleep(10000);
                        goto NextPostRequirement;
                    }

                    if (meet_requirement_accountIds.Count > 0)
                    {
                        //sort by latest post asc --use latest next_publish_post actionlog (do_at) to sort

                        var meet_requirement_posted_accountIds = TableFromListString(meet_requirement_accountIds);

                        command_query =
                            @"select idtb.accountId
                            from (values " + meet_requirement_posted_accountIds + @") idtb(accountId)
                            OUTER APPLY 
	                            (select top 1 *
	                            from actionlog
	                            where [accountid]=idtb.accountId and
	                            [action]='next_publish_post'
	                            order by id desc) as pb
                            order by do_at asc";

                        meet_requirement_accountIds.Clear();
                        PLAutoHelper.SQLHelper.Execute(() =>
                        {
                            using (var command = new SqlCommand(command_query, conn))
                            {
                                using (var dataReader = command.ExecuteReader())
                                {
                                    if (dataReader.HasRows)
                                    {
                                        while (dataReader.Read())
                                        {
                                            meet_requirement_accountIds.Add(dataReader["accountId"].ToString());
                                        }
                                    }
                                }
                            }
                        }, "GetAccount_DoNormalUserTask");

                        foreach (var id in meet_requirement_accountIds)
                        {

                            bool isclaimed = ClaimedAccountByID(new SqlCommand("", conn), id);

                            if (isclaimed)
                            {
                                choicedID = id;
                                goto EndFunc;
                            }
                            else
                                continue;

                            //command_query =
                            //    @"update ig_account
                            //        set
                            //         [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"'
                            //        where
                            //         ([status] is null or [status]='') and
                            //         ([id]=@id)";
                            //using (var command = new SqlCommand(command_query, conn))
                            //{
                            //    command.CommandText = command_query;
                            //    command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

                            //    var affected_row = command.ExecuteNonQuery();

                            //    if (affected_row == 1)
                            //    {
                            //        choicedID = id;
                            //        goto EndFunc;
                            //    }
                            //    else
                            //        continue;
                            //}
                        }
                    }

                    //foreach (var id in filterAccountIds)
                    //{
                    //    command_query =
                    //        @"
                    //    select count([accountId])
                    //    from account_info
                    //    where
                    //     [accountId]=@id and
                    //     total_timespend>30 and
                    //     --meet requirement about next publish
                    //     datediff(second, CONVERT(datetime, (select top 1 [value] from actionlog where actionlog.accountid=@id and [action]='next_publish_post' order by id desc),21),getdate())>0
                    //     ";

                    //    using (var command = new SqlCommand(command_query, conn))
                    //    {
                    //        command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

                    //        var count = int.Parse(command.ExecuteScalar().ToString());

                    //        if (count == 0)
                    //            continue;
                    //        else
                    //        {

                    //            command_query =
                    //                @"update ig_account
                    //            set
                    //             [status]='Claimed'
                    //            where
                    //             ([status] is null or [status]='') and
                    //             ([id]=@id)";

                    //            command.CommandText = command_query;
                    //            command.Parameters.Clear();
                    //            command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

                    //            var affected_row = command.ExecuteNonQuery();

                    //            if (affected_row == 1)
                    //            {
                    //                choicedID = id;
                    //                goto EndFunc;
                    //            }
                    //            else
                    //                continue;
                    //        }
                    //    }
                    //}

                }

            EndFunc:
                if (choicedID != null)
                {
                    ig_account = DynamicIGAccountByID(choicedID);
                }


                if (ig_account == null)
                {
                    next_DoNormalUserTask = DateTime.Now.AddMinutes(1);
                }

                return ig_account;
            }
            finally
            {
                Thread.Sleep(1000);
                ThreadHelper.MonitorDB.semaphore_monitor_processdb.Release();
            }
        }

        #endregion

        #region --DoEngage--

        static DateTime next_GetAccount_DoEngage = new DateTime();
        static public dynamic GetAccount_DoEngage()
        {
            string command_query = "";

            try
            {
                ThreadHelper.MonitorDB.semaphore_monitor_processdb.WaitOne();

                if (next_GetAccount_DoEngage > DateTime.Now)
                {
                    return null;
                }

                dynamic ig_account = null;
                string choiced_id = null;

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    while (true)
                    {

                        List<string> availableAccountIds = AvailableAccountToDoAction_AIO(new SqlCommand("", conn));

                        if (availableAccountIds.Count == 0)
                            return null;


                        var idtbs = TableFromListString(availableAccountIds);

                        //get account don't engage in last 2 days first

                        List<string> not_engage_last_2days = new List<string>();

                        command_query =
                            @"select account_info.accountId 
			                from account_info
			                where
				                (current_timespend < target_timedate) and
				                /*condition ve lan cuoi cung thuc hien action*/
				                ((DATEDIFF(second, nextengage, GETDATE())>0) or [nextengage] is null) and
						        (DATEDIFF(day, lastengage, getdate())>2 or lastengage is null)
			                order by NEWID()";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            using (var dataReader = command.ExecuteReader())
                            {
                                while (dataReader.Read())
                                {
                                    not_engage_last_2days.Add(dataReader["accountId"].ToString());
                                }
                            }
                        }

                        not_engage_last_2days = availableAccountIds.Where(a => not_engage_last_2days.Contains(a)).ToList();

                        if (not_engage_last_2days.Count > 0)
                        {

                            foreach (var a in not_engage_last_2days)
                            {
                                choiced_id = a;

                                var isclaimed = ClaimedAccountByID(new SqlCommand("", conn), choiced_id);

                                if (isclaimed)
                                {
                                    goto EndFunc;
                                }
                                else
                                {
                                    choiced_id = null;
                                    continue;
                                }
                            }
                        }

                        //                  while (true)
                        //                  {

                        //                      command_query =
                        //                          @"select top 1 account_info.accountId 
                        //     from account_info
                        //     join (values " + idtbs + @") idtb(accountId) on account_info.accountId=idtb.accountId
                        //     join ig_account ig on account_info.accountId=ig.[id]
                        //     where
                        //      (current_timespend < target_timedate) and
                        //      /*condition ve lan cuoi cung thuc hien action*/
                        //      ((DATEDIFF(second, nextengage, GETDATE())>0) or [nextengage] is null) and
                        //(DATEDIFF(day, lastengage, getdate())>2 or lastengage is null)
                        //     order by NEWID()";

                        //                      using (var command = new SqlCommand(command_query, conn))
                        //                      {
                        //                          using (var dataReader = command.ExecuteReader())
                        //                          {
                        //                              if (dataReader.HasRows)
                        //                              {
                        //                                  dataReader.Read();
                        //                                  choiced_id = dataReader["accountId"].ToString();
                        //                              }
                        //                              else
                        //                                  break;
                        //                          }
                        //                      }

                        //                      if (choiced_id != null)
                        //                      {
                        //                          //try claim account
                        //                          command_query =
                        //                                  @"update ig_account
                        //                          set
                        //                           [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"'
                        //                          where
                        //                           ([status] is null or [status]='') and
                        //                           ([id]=@id)";
                        //                          using (var command = new SqlCommand(command_query, conn))
                        //                          {
                        //                              command.CommandText = command_query;
                        //                              command.Parameters.Clear();
                        //                              command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = choiced_id;

                        //                              var affected_row = command.ExecuteNonQuery();

                        //                              if (affected_row == 1)
                        //                              {
                        //                                  goto EndFunc;
                        //                              }
                        //                              else
                        //                              {
                        //                                  choiced_id = null;
                        //                                  continue;
                        //                              }
                        //                          }
                        //                      }
                        //                      else
                        //                      {
                        //                          //k lay dc data nen break
                        //                          break;
                        //                      }
                        //                  }

                        //get another account (engage in last 2 days)

                        List<string> engage_last_2days = new List<string>();

                        command_query =
                            @"select account_info.accountId 
			                from account_info
			                where
				                (current_timespend < target_timedate) and
				                /*condition ve lan cuoi cung thuc hien action*/
				                ((DATEDIFF(second, nextengage, GETDATE())>0) or [nextengage] is null) and
						        (DATEDIFF(day, lastengage, getdate())<=2)
			                order by NEWID()";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            using (var dataReader = command.ExecuteReader())
                            {
                                while (dataReader.Read())
                                {
                                    engage_last_2days.Add(dataReader["accountId"].ToString());
                                }
                            }
                        }

                        engage_last_2days = availableAccountIds.Where(a => engage_last_2days.Contains(a)).ToList();

                        if (engage_last_2days.Count > 0)
                        {

                            foreach (var a in engage_last_2days)
                            {
                                choiced_id = a;

                                var isclaimed = ClaimedAccountByID(new SqlCommand("", conn), choiced_id);

                                if (isclaimed)
                                {
                                    goto EndFunc;
                                }
                                else
                                {
                                    choiced_id = null;
                                    continue;
                                }
                            }
                        }


          //              while (true)
          //              {

          //                   command_query =
          //                      @"select top 1 account_info.accountId 
			       //     from account_info
			       //     join (values " + idtbs + @") idtb(accountId) on account_info.accountId=idtb.accountId
			       //     where
				      //      (current_timespend < target_timedate) and
				      //      /*condition ve lan cuoi cung thuc hien action*/
				      //      ((DATEDIFF(second, nextengage, GETDATE())>0) or [nextengage] is null) and
						    //DATEDIFF(day, lastengage, getdate())<=2
			       //     order by NEWID()";

          //                  using (var command = new SqlCommand(command_query, conn))
          //                  {
          //                      using (var dataReader = command.ExecuteReader())
          //                      {
          //                          if (dataReader.HasRows)
          //                          {
          //                              dataReader.Read();
          //                              choiced_id = dataReader["accountId"].ToString();
          //                          }
          //                          else
          //                              break;
          //                      }
          //                  }

          //                  if (choiced_id != null)
          //                  {
          //                      bool isclaimed = ClaimedAccountByID(new SqlCommand("", conn), choiced_id);

          //                      if (isclaimed)
          //                          goto EndFunc;
          //                      else
          //                      {
          //                          choiced_id = null;
          //                          continue;
          //                      }

          //                      ////try claim account
          //                      //command_query =
          //                      //        @"update ig_account
          //                      //set
	         //                      // [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"'
          //                      //where
	         //                      // ([status] is null or [status]='') and
	         //                      // ([id]=@id)";
          //                      //using (var command = new SqlCommand(command_query, conn))
          //                      //{
          //                      //    command.CommandText = command_query;
          //                      //    command.Parameters.Clear();
          //                      //    command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = choiced_id;

          //                      //    var affected_row = command.ExecuteNonQuery();

          //                      //    if (affected_row == 1)
          //                      //    {
          //                      //        goto EndFunc;
          //                      //    }
          //                      //    else
          //                      //    {
          //                      //        choiced_id = null;
          //                      //        continue;
          //                      //    }
          //                      //}
          //                  }
          //                  else
          //                  {
          //                      //k lay dc data nen break
          //                      break;
          //                  }
          //              }



                    EndFunc:

                        if (choiced_id != null)
                        {
                            //check if @selected_accountId last_engage is null or yesterday => if yes, day la lan check dau tien trong ngay, generate random nextengage nhu la lan chay dau tien trong ngay

                            string lastengage = null;

                            command_query = "select [lastengage] from account_info where [accountId]=@choiced_id";

                            using (var command = new SqlCommand(command_query, conn))
                            {

                                command.Parameters.Add("@choiced_id", System.Data.SqlDbType.Int).Value = choiced_id;

                                lastengage = command.ExecuteScalar() == null ? "" : command.ExecuteScalar().ToString();

                                if (lastengage == "" || DateTime.Now.Subtract(DateTime.Parse(lastengage)).TotalDays > 1)
                                {
                                    //--generate random nextengage nhu la lan chay dau tien trong ngay

                                    command.CommandText =
                                        @"update account_info
		                        set [lastengage]=getdate(),[nextengage]=dateadd(millisecond, cast(3600000 * RAND() as int), getdate()) --random 1 hour tu thoi diem nay
		                        where [accountId]=@choiced_id";

                                    command.ExecuteNonQuery();

                                    //unclaimed status

                                    command_query =
                                        @"update ig_account
                                    set
	                                    [status]=''
                                    where
	                                    [id]=@id";

                                    command.CommandText = command_query;
                                    command.Parameters.Clear();

                                    command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = choiced_id;

                                    command.ExecuteNonQuery();

                                    choiced_id = null;
                                }
                                else
                                    break;
                            }

                        }
                        else
                        {
                            break;
                        }
                    }
                }

                if (choiced_id != null)
                {

                    ig_account = DynamicIGAccountByID(choiced_id);
                }

                if (ig_account == null)
                {
                    next_GetAccount_DoEngage = DateTime.Now.AddMinutes(10);
                }

                return ig_account;
            }
            finally
            {
                Thread.Sleep(1000);
                ThreadHelper.MonitorDB.semaphore_monitor_processdb.Release();
            }
        }

        #endregion

        #region --DoQueueTask--

        static DateTime next_GetAccount_DoQueueTask = new DateTime();
        static public dynamic GetAccount_DoQueueTask(int max_error_times = 3)
        {

            try
            {
                ThreadHelper.MonitorDB.semaphore_monitor_processdb.WaitOne();

                if (next_GetAccount_DoQueueTask > DateTime.Now)
                    return null;

                dynamic ig_account = null;
                string choiced_id = null;

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    while (true)
                    {

                        List<string> availableAccountIds = AvailableAccountToDoAction_AIO(new SqlCommand("", conn));

                        if (availableAccountIds.Count == 0)
                            return null;

                        //get list accountid has queuetask that [status]='' or [status] is null

                        var command_query =
                            @"select [accountid]
                        from queuetasks
                        where
	                        ([status]='' or [status] is null) and
	                        ([schedule_time] is null or DATEDIFF(minute,[schedule_time],getdate())>0) and
                            [error_times]<=@max_error_times
                        order by high_priority desc, [schedule_time] asc";

                        List<string> accountIds_task = new List<string>();

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@max_error_times", System.Data.SqlDbType.Int).Value = max_error_times;
                            using (var dataReader = command.ExecuteReader())
                            {
                                while (dataReader.Read())
                                {
                                    accountIds_task.Add(dataReader["accountid"].ToString());
                                }
                            }
                        }

                        //filter with availableAccountIds
                        accountIds_task = accountIds_task.Where(a => availableAccountIds.Contains(a)).ToList();

                        //get list account IsBlockorUnsualinXDays
                        var block_unsual_ids = BlockorUnsualinXDays_AccountIds(new SqlCommand("", conn));

                        //exclude

                        availableAccountIds = availableAccountIds.Except(block_unsual_ids).ToList();

                        if (accountIds_task.Count > 0)
                        {
                            foreach (var item in accountIds_task)
                            {
                                choiced_id = item;

                                ////check if blocked or unsual activity recently

                                //if (IsBlockorUnsualinXDays(choiced_id, new SqlCommand("", conn), 2))
                                //    continue;

                                //check if doqueuetask in recently X minutes, if yes => continue
                                int recent_mins = 60;

                                command_query =
                                    @"select count(*)
                                    from actionlog
                                    where
	                                    [accountid]=@accountid and
	                                    [value]='DoQueueTask' and
	                                    DATEDIFF(minute,do_at,getdate())<@recent_mins";

                                using (var command = new SqlCommand(command_query, conn))
                                {
                                    command.Parameters.Add("@recent_mins", System.Data.SqlDbType.Int).Value = recent_mins;
                                    command.Parameters.Add("@accountid", System.Data.SqlDbType.Int).Value = choiced_id;
                                    int count_actionlog = int.Parse(command.ExecuteScalar().ToString());
                                    if (count_actionlog > 0)
                                        continue;
                                }

                                //claim
                                //try claim account
                                bool isclaimed = ClaimedAccountByID(new SqlCommand("", conn), choiced_id);

                                if (isclaimed)
                                    break;
                                else
                                {
                                    choiced_id = null;
                                    continue;
                                }

                                //command_query =
                                //    @"update ig_account
                                //set
	                               // [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"'
                                //where
	                               // ([status] is null or [status]='') and
	                               // ([id]=@id)";
                                //using (var command = new SqlCommand(command_query, conn))
                                //{
                                //    command.CommandText = command_query;
                                //    command.Parameters.Clear();
                                //    command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = choiced_id;

                                //    var affected_row = command.ExecuteNonQuery();

                                //    if (affected_row == 1)
                                //    {
                                //        break;
                                //    }
                                //    else
                                //    {
                                //        choiced_id = null;
                                //        continue;
                                //    }
                                //}
                            }

                            if (choiced_id != null)
                                break;
                        }
                        else
                            break;
                    }
                }

                if (choiced_id != null)
                {
                    ig_account = DynamicIGAccountByID(choiced_id);
                }

                if (ig_account == null)
                {
                    next_GetAccount_DoQueueTask = DateTime.Now.AddMinutes(1);
                }

                return ig_account;

            }
            finally
            {
                Thread.Sleep(1000);
                ThreadHelper.MonitorDB.semaphore_monitor_processdb.Release();
            }
        }

        #endregion

        #region --DoCommentTask--

        static DateTime next_GetAccount_DoSpecificTask_CommentPost = new DateTime();

        /// <summary>
        /// get account to do CommentPostTask - NOT TEST
        /// </summary>
        /// <returns></returns>
        static public dynamic GetAccount_DoSpecificTask_CommentPost()
        {
            try
            {
                ThreadHelper.MonitorDB.semaphore_monitor_processdb.WaitOne();

                dynamic ig_account = null;
                string choicedID = null;

                if (next_GetAccount_DoSpecificTask_CommentPost > DateTime.Now)
                    return null;

                int dayofweek = DayOfWeek(); ;
                DateTime firstDayWeek = FirstDayWeek();

                //get available account to do action
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();
                    //using (var sqlTransaction = conn.BeginTransaction())
                    //{
                    //filter accountId turn of likepost

                    List<string> availableAccountIds = AvailableAccountToDoAction_AIO(new SqlCommand("", conn));

                    if (availableAccountIds.Count == 0)
                        return null;

                    var command_query =
                    @"select [accountId]
                    from specifictasks
                    where [commentpost] = 'Do'";

                    List<string> tempCommentPost_accountIds = new List<string>();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    tempCommentPost_accountIds.Add(dataReader["accountId"].ToString());
                                }
                            }
                        }
                    }

                    availableAccountIds = availableAccountIds.Where(s => tempCommentPost_accountIds.Contains(s)).ToList();

                    //get account commentpost_finished

                    command_query =
                        @"select [accountid]
                        from actionlog
                        where
	                        [action]='commentpost_finished' and
							DATEDIFF(day,do_at,getdate())=0";

                    List<string> commentpost_finished_ids = new List<string>();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                commentpost_finished_ids.Add(dataReader["accountId"].ToString());
                            }

                        }
                    }

                    //except finished

                    availableAccountIds = availableAccountIds.Except(commentpost_finished_ids).ToList();

                    //shuffel

                    availableAccountIds = availableAccountIds.OrderBy(a => Guid.NewGuid()).ToList();

                    //get list account offday
                    var offday_Ids = OffThisDay_AccountIds(firstDayWeek, new SqlCommand("", conn));

                    //get list account IsBlockorUnsualinXDays
                    var block_unsual_ids = BlockorUnsualinXDays_AccountIds(new SqlCommand("", conn));

                    //exclude

                    availableAccountIds = availableAccountIds.Except(offday_Ids).Except(block_unsual_ids).ToList();


                    if (availableAccountIds.Count == 0)
                        return null;

                    //check every accountId

                    foreach (var id in availableAccountIds)
                    {
                        ////check if is off day
                        //if (IsOffDayById(id, firstDayWeek, new SqlCommand("", conn)))
                        //    continue;

                        //if (IsBlockorUnsualinXDays(id, new SqlCommand("", conn), 3))
                        //    continue;

                        //get next session time
                        command_query =
                            @"SELECT top 1 [value]
                        FROM actionlog
                        WHERE[accountid] = @temp_accountId
                          AND DATEDIFF(DAY, [do_at], getdate())= 0
                          AND [action] = 'commentpost_next_session'
                        ORDER BY id DESC";

                        string next_session_time_str = null;

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;
                            using (var dataReader = command.ExecuteReader())
                            {
                                if (dataReader.HasRows)
                                {
                                    dataReader.Read();
                                    next_session_time_str = dataReader["value"].ToString();
                                }
                            }
                        }

                        if (next_session_time_str == null)
                        {
                            //--set next_sesstion_time -- thuc ra day la session dau tien trong ngay

                            //--random blocked_session_count

                            //--random 1-3 session per blocked for first block
                            command_query =

                                    @"insert into actionlog ([accountid],[action],[value]) values (@temp_accountId,'block_commentpost_session_count',1)  --random 3-10 session per blocked

		                        insert into actionlog ([accountid],[action],[value]) values (@temp_accountId,'commentpost_next_session',CONVERT(varchar, dateadd(millisecond, cast(3600000 * RAND() as int), getdate()),21))";// --randome start time tu thoi diem hien tai toi 1 tieng nua"

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;
                                command.ExecuteNonQuery();
                            }

                            continue;

                        }
                        else
                        {
                            var next_session_time = DateTime.Parse(next_session_time_str);

                            if (DateTime.Now.Subtract(next_session_time).TotalSeconds < 0)
                                continue;
                        }

                        //neu thoa dieu kien next_session_time thi check if currentdate_following < target_following
                        int current_date_CommentPost = 0;

                        command_query =
                            @"select count(*) from actionlog 
                        where
                            [action] = 'commentpost_task' and
                              [accountid] = @temp_accountId and

                            DATEDIFF(day,[do_at], getdate()) = 0";

                        using (var command = new SqlCommand(command_query, conn))
                        {

                            command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;

                            current_date_CommentPost = int.Parse(command.ExecuteScalar().ToString());
                        }

                        int target_CommentPost = 0;

                        command_query =
                            @"select [value] from actionlog
                        where
                                [action] = 'target_commentpost' and
                                  [accountid] = @temp_accountId and
                                DATEDIFF(day,[do_at], getdate()) = 0";
                        using (var command = new SqlCommand(command_query, conn))
                        {

                            command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;

                            var value = command.ExecuteScalar();

                            if (value != null)
                            {
                                target_CommentPost = int.Parse(value.ToString());
                                if (current_date_CommentPost < target_CommentPost)
                                {
                                    choicedID = id;
                                    //break;
                                }
                                else
                                {
                                    //add finish log
                                    command_query =

                                    @"insert into actionlog ([accountid],[action]) values (@temp_accountId,'commentpost_finished')";// --randome start time tu thoi diem hien tai toi 1 tieng nua"

                                    using (var command_finished = new SqlCommand(command_query, conn))
                                    {
                                        command_finished.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;
                                        command_finished.ExecuteNonQuery();
                                    }
                                }
                            }
                            else
                            {
                                //tuc la chua chay lan nao hom nay => choose it
                                choicedID = id;
                                //break;
                            }
                        }

                        if (choicedID != null)
                        {
                            bool isclaimed = ClaimedAccountByID(new SqlCommand("", conn), choicedID);

                            if (isclaimed)
                                break;
                            else
                                choicedID = null;

                            //command_query =
                            //    "update ig_account set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"' where [id] = @id and ([status]='' or [status] is null)";
                            //using (var command = new SqlCommand(command_query, conn))
                            //{
                            //    command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = choicedID;
                            //    var affected_row = command.ExecuteNonQuery();

                            //    if (affected_row == 1)
                            //    {
                            //        break;
                            //    }
                            //    else
                            //    {
                            //        choicedID = null;
                            //    }
                            //}
                        }
                    }

                    //sqlTransaction.Commit();
                    //}
                }

                if (choicedID != null)
                    ig_account = DynamicIGAccountByID(choicedID);

                if (ig_account == null)
                {
                    next_GetAccount_DoSpecificTask_CommentPost = DateTime.Now.AddMinutes(1);
                }

                return ig_account;
            }
            finally
            {
                Thread.Sleep(1000);
                ThreadHelper.MonitorDB.semaphore_monitor_processdb.Release();
            }
        }


        #endregion

        #region --DMTask--


        static DateTime next_GetAccount_DoSpecificTask_massdm = new DateTime();

        /// <summary>
        /// get account to do CommentPostTask - NOT TEST
        /// </summary>
        /// <returns></returns>
        static public dynamic GetAccount_DoSpecificTask_MassDM()
        {


            try
            {
                ThreadHelper.MonitorDB.semaphore_monitor_processdb.WaitOne();

                dynamic ig_account = null;
                string choicedID = null;

                if (next_GetAccount_DoSpecificTask_massdm > DateTime.Now)
                    return null;

                int dayofweek = DayOfWeek(); ;
                DateTime firstDayWeek = FirstDayWeek();

                //get available account to do action
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();
                    //using (var sqlTransaction = conn.BeginTransaction())
                    //{
                    //filter accountId turn of likepost

                    List<string> availableAccountIds = AvailableAccountToDoAction_AIO(new SqlCommand("", conn));

                    if (availableAccountIds.Count == 0)
                        return null;

                    var command_query =
                    @"select [accountId]
                    from specifictasks
                    where [massdm] = 'Do'";

                    List<string> tempmassdm_accountIds = new List<string>();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    tempmassdm_accountIds.Add(dataReader["accountId"].ToString());
                                }
                            }
                        }
                    }

                    availableAccountIds = availableAccountIds.Where(s => tempmassdm_accountIds.Contains(s)).ToList();

                    //get list accountIds followback <= massdm => tuc la k con data de massdm  // dùng distinct để loại trừ truong hợp thuc hien trùng target_account dẫn đến sai sót nếu k distinct khi count

                    command_query =
                        @"select [id]
                        from ig_account
                        join specifictasks on ig_account.id=specifictasks.accountId
                        where
	                        (select count(distinct(a1.target_account))
	                        from actionlog a1
	                        where
		                        a1.[action]='followback' and
		                        a1.[accountid]=ig_account.id)<=
	                        (select count(distinct(a2.target_account))
	                        from actionlog a2
	                        where
		                        a2.[action]='massdm_task' and
		                        a2.[accountid]=ig_account.id) and
	                        [massdm]='Do'";

                    List<string> nodata_accountIds = new List<string>();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    nodata_accountIds.Add(dataReader["id"].ToString());
                                }
                            }
                        }
                    }

                    availableAccountIds = availableAccountIds.Except(nodata_accountIds).ToList();


                    //shuffel

                    availableAccountIds = availableAccountIds.OrderBy(a => Guid.NewGuid()).ToList();

                    //get list account offday
                    var offday_Ids = OffThisDay_AccountIds(firstDayWeek, new SqlCommand("", conn));

                    //get list account IsBlockorUnsualinXDays
                    var block_unsual_ids = BlockorUnsualinXDays_AccountIds(new SqlCommand("", conn));

                    //exclude

                    availableAccountIds = availableAccountIds.Except(offday_Ids).Except(block_unsual_ids).ToList();


                    if (availableAccountIds.Count == 0)
                        return null;

                    //check every accountId

                    //foreach (var id in availableAccountIds)
                    for(int i=0;i<availableAccountIds.Count;i++)
                    {
                        var id = availableAccountIds[i];
                        ////check if is off day
                        //if (IsOffDayById(id, firstDayWeek, new SqlCommand("", conn)))
                        //    continue;

                        //if (IsBlockorUnsualinXDays(id, new SqlCommand("", conn), 3))
                        //    continue;

                        //get next session time
                        command_query =
                            @"SELECT top 1 [value]
                        FROM actionlog
                        WHERE[accountid] = @temp_accountId
                          AND DATEDIFF(DAY, [do_at], getdate())= 0
                          AND [action] = 'massdm_next_session'
                        ORDER BY id DESC";

                        string next_session_time_str = null;
                        try
                        {
                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;
                                using (var dataReader = command.ExecuteReader())
                                {
                                    if (dataReader.HasRows)
                                    {
                                        dataReader.Read();
                                        next_session_time_str = dataReader["value"].ToString();
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ADBSupport.LDHelpers.HandleException("nodevice", "GetAccount_DoSpecificTask_FollowingUser" + "|" + ex.Message, ex.StackTrace);
                            Thread.Sleep(2000);
                            continue;
                        }

            if (next_session_time_str == null)
                        {
                            //--set next_sesstion_time -- thuc ra day la session dau tien trong ngay

                            //--random blocked_session_count

                            //--random 1-3 session per blocked for first block
                            command_query =

                                    @"insert into actionlog ([accountid],[action],[value]) values (@temp_accountId,'block_massdm_session_count',1)  --random 3-10 session per blocked

		                        insert into actionlog ([accountid],[action],[value]) values (@temp_accountId,'massdm_next_session',CONVERT(varchar, dateadd(millisecond, cast(3600000 * RAND() as int), getdate()),21))";// --randome start time tu thoi diem hien tai toi 1 tieng nua"

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;
                                command.ExecuteNonQuery();
                            }

                            continue;

                        }
                        else
                        {
                            var next_session_time = DateTime.Parse(next_session_time_str);

                            if (DateTime.Now.Subtract(next_session_time).TotalSeconds < 0)
                                continue;
                        }

                        //neu thoa dieu kien next_session_time thi check if currentdate_following < target_following
                        int current_date_massdm = 0;

                        command_query =
                            @"select count(*) from actionlog 
                        where
                            [action] = 'massdm_task' and
                              [accountid] = @temp_accountId and

                            DATEDIFF(day,[do_at], getdate()) = 0";

                        using (var command = new SqlCommand(command_query, conn))
                        {

                            command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;

                            current_date_massdm = int.Parse(command.ExecuteScalar().ToString());
                        }

                        int target_massdm = 0;

                        command_query =
                            @"select [value] from actionlog
                        where
                                [action] = 'target_massdm' and
                                  [accountid] = @temp_accountId and
                                DATEDIFF(day,[do_at], getdate()) = 0";
                        using (var command = new SqlCommand(command_query, conn))
                        {

                            command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = id;

                            var value = command.ExecuteScalar();

                            if (value != null)
                            {
                                target_massdm = int.Parse(value.ToString());
                                if (current_date_massdm < target_massdm)
                                {
                                    choicedID = id;
                                    //break;
                                }
                            }
                            else
                            {
                                //tuc la chua chay lan nao hom nay => choose it
                                choicedID = id;
                                //break;
                            }
                        }

                        if (choicedID != null)
                        {

                            //check if has target_account for this choicedID

                            if (AIOHelper.DBLogic.GetTargetAccount_SpecificTasks.MassDM(DynamicIGAccountByID(choicedID), false) == null)
                            {
                                choicedID = null;
                            }
                            else
                            {
                                bool isclaimed = ClaimedAccountByID(new SqlCommand("", conn), choicedID);

                                if (isclaimed)
                                    break;
                                else
                                    choicedID = null;

                                //command_query =
                                //    "update ig_account set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"' where [id] = @id and ([status]='' or [status] is null)";
                                //using (var command = new SqlCommand(command_query, conn))
                                //{
                                //    command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = choicedID;
                                //    var affected_row = command.ExecuteNonQuery();

                                //    if (affected_row == 1)
                                //    {
                                //        break;
                                //    }
                                //    else
                                //    {
                                //        choicedID = null;
                                //    }
                                //}
                            }
                        }
                    }

                    //sqlTransaction.Commit();
                    //}
                }

                if (choicedID != null)
                    ig_account = DynamicIGAccountByID(choicedID);

                if (ig_account == null)
                {
                    next_GetAccount_DoSpecificTask_massdm = DateTime.Now.AddMinutes(30);
                }

                return ig_account;
            }
            finally
            {
                Thread.Sleep(1000);
                ThreadHelper.MonitorDB.semaphore_monitor_processdb.Release();
            }

        }


        #endregion

        #region --LoginToNewACCOUNT--

     //   static public dynamic GetAccount_DoSpecificTask_LoginNewACCOUNT()
     //   {
     //       dynamic ig_account = null;
     //       string choicedId = null;

     //       lock ("NOTLOGIN")
     //       {
     //           using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
     //           {
     //               conn.Open();

     //               using (var command = new SqlCommand("select top 1 * from ig_account where [state]='NOTLOGIN' and ([status] is null or [status]='')", conn))
     //               {
     //                   using (var dataReader = command.ExecuteReader())
     //                   {
     //                       if (dataReader.HasRows)
     //                       {
     //                           dataReader.Read();
     //                           choicedId = dataReader["id"].ToString();
     //                       }
     //                   }
     //               }

     //               if (choicedId != null)
     //               {
     //                   //update
     //                   string command_query =
     //                       @"update ig_account set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"' where [id]=@accountId";

     //                   using (var command = new SqlCommand(command_query, conn))
     //                   {
     //                       command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = choicedId;
     //                       command.ExecuteNonQuery();
     //                   }
     //               }

     //           }
     //       }

     //       if(choicedId!=null)
     //       {

     //           using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
     //           {
     //               conn.Open();

     //               var command_query =
     //                   @"select *
     //               from ig_account
					//where [id]=@id";

     //               using (var command = new SqlCommand(command_query, conn))
     //               {
     //                   command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = choicedId;
     //                   using (var dataReader = command.ExecuteReader())
     //                   {
     //                       dataReader.Read();
     //                       string id = dataReader["id"].ToString();
     //                       string user = dataReader["user"].ToString();
     //                       string fullname = dataReader["fullname"].ToString();
     //                       string phone_device = dataReader["phone_device"].ToString();
     //                       string type = dataReader["type"] == null ? "" : dataReader["type"].ToString();
     //                       string mode = dataReader["mode"].ToString();
     //                       ig_account = new
     //                       {
     //                           id,
     //                           fullname,
     //                           phone_device,
     //                           type,
     //                           mode,
     //                           user
     //                       };
     //                   }
     //               }
     //           }
     //       }

     //       return ig_account;
     //   }

        static private List<dynamic> GetAccountToLogin_AIO_accounts = new List<dynamic>();

        static public dynamic GetAccount_DoSpecificTask_LoginNewACCOUNT()
        {
            dynamic data = null;
            string command_query = null;

            lock ("GetAccountToLogin_AIO")
            {

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    while (true)
                    {

                        if (GetAccountToLogin_AIO_accounts.Count > 0)
                        {
                            data = GetAccountToLogin_AIO_accounts.First();
                        }
                        else
                        {

                            //get list account need generate device
                            command_query =
                                @"select top 100 *
                        from ig_account
                        where 
	                        ([phone_device] is not null or [phone_device]!='') and
	                        [state]='NOTLOGIN' and
	                        ([status] is null or [status]='')";

                            //get list account need to generate device


                            using (var command = new SqlCommand(command_query, conn))
                            {
                                using (var dataReader = command.ExecuteReader())
                                {
                                    while (dataReader.Read())
                                    {
                                        string id = dataReader["id"].ToString();
                                        string user = dataReader["user"].ToString();
                                        string password = dataReader["password"].ToString();
                                        string phone_device = dataReader["phone_device"].ToString();
                                        string phonenum = dataReader["phonenum"].ToString();
                                        string fullname = dataReader["fullname"].ToString();
                                        string type = dataReader["type"] == null ? "" : dataReader["type"].ToString();
                                        string mode = dataReader["mode"].ToString();
                                        GetAccountToLogin_AIO_accounts.Add(new
                                        {
                                            id,
                                            user,
                                            password,
                                            phone_device,
                                            phonenum,
                                            fullname,
                                            type,
                                            mode
                                        });
                                    }
                                }
                            }

                            if (GetAccountToLogin_AIO_accounts.Count > 0)
                            {
                                data = GetAccountToLogin_AIO_accounts.First();
                            }
                        }

                        if (data != null)
                        {

                            bool isclaimed = ClaimedAccountByID(new SqlCommand("", conn), data.id);


                            GetAccountToLogin_AIO_accounts.RemoveAll(x => x.id == data.id);

                            if (isclaimed)
                                break;
                            else
                                data = null;

                            ////claimed it
                            //command_query =
                            //    @"update ig_account set [status]='Claimed' where [id]=@id";

                            //using (var command = new SqlCommand(command_query, conn))
                            //{
                            //    command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = data.id;
                            //    var affect_Row = command.ExecuteNonQuery();


                            //    GetAccountToLogin_AIO_accounts.RemoveAll(x => x.id == data.id);

                            //    if (affect_Row == 1)
                            //    {
                            //        break;
                            //    }
                            //    else
                            //    {
                            //        data = null;
                            //    }
                            //}
                        }
                        else
                            break;
                    }
                }
            }

            return data;
        }

        #endregion

        #region --GenerateDevice NEWACCOUNT--

        static private List<dynamic> GetAccountToGenerateDevice_AIO_accounts = new List<dynamic>();

        static public dynamic GetAccount_DoSpecificTask_GenerateDevice_AIO()
        {
            dynamic data = null;
            string command_query = null;

            lock ("GetAccountToGenerateDevice_AIO")
            {

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    while (true)
                    {

                        if (GetAccountToGenerateDevice_AIO_accounts.Count > 0)
                        {
                            data = GetAccountToGenerateDevice_AIO_accounts.First();
                        }
                        else
                        {

                            //get list account need generate device
                            command_query =
                                @"select top 100 *
                        from ig_account
                        where 
	                        ([phone_device] is null or [phone_device]='') and
	                        [state]='NOTLOGIN' and
	                        ([status] is null or [status]='')";

                            //get list account need to generate device


                            using (var command = new SqlCommand(command_query, conn))
                            {
                                using (var dataReader = command.ExecuteReader())
                                {
                                    while (dataReader.Read())
                                    {
                                        string id = dataReader["id"].ToString();
                                        string user = dataReader["user"].ToString();
                                        string password = dataReader["password"].ToString();
                                        string phone_device = dataReader["phone_device"].ToString();
                                        string phonenum = dataReader["phonenum"].ToString();
                                        GetAccountToGenerateDevice_AIO_accounts.Add(new
                                        {
                                            id,
                                            user,
                                            password,
                                            phone_device,
                                            phonenum
                                        });
                                    }
                                }
                            }

                            if (GetAccountToGenerateDevice_AIO_accounts.Count > 0)
                            {
                                data = GetAccountToGenerateDevice_AIO_accounts.First();
                            }
                        }

                        if (data != null)
                        {

                            bool isclaimed = ClaimedAccountByID(new SqlCommand("", conn), data.id);

                            GetAccountToGenerateDevice_AIO_accounts.RemoveAll(x => x.id == data.id);

                            if (isclaimed)
                                break;
                            else
                                data = null;



                            ////claimed it
                            //command_query =
                            //    @"update ig_account set [status]='Claimed' where [id]=@id";

                            //using (var command = new SqlCommand(command_query, conn))
                            //{
                            //    command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = data.id;
                            //    var affect_Row = command.ExecuteNonQuery();


                            //    GetAccountToGenerateDevice_AIO_accounts.RemoveAll(x => x.id == data.id);

                            //    if (affect_Row == 1)
                            //    {
                            //        break;
                            //    }
                            //    else
                            //    {
                            //        data = null;
                            //    }
                            //}
                        }
                        else
                            break;

                    }
                }
            }

            return data;
        }

        #endregion

        #region --reuse func--

        //         Select v.valueId, m.name
        //         From (values (1), (2), (3), (4), (5)) v(valueId)
        //         left Join otherTable m
        //         on m.id = v.valueId

        /// <summary>
        /// 
        /// </summary>
        static private string TableFromListString(List<string> items)
        {
            string tb = "";

            for (int i = 0; i < items.Count; i++)
            {
                if (i == 0)
                {
                    tb += "(" + items[i].ToString() + ")";
                }
                else
                {
                    tb += ",(" + items[i].ToString() + ")";
                }
            };

            return tb;
        }

        static public bool ClaimedAccountByID(SqlCommand command, string accountId)
        {
            Mutex m = new Mutex(false, accountId + "_claim");

            try
            {
                m.WaitOne();

                string command_query =
                        "update ig_account set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"' where [id] = @id and ([status]='' or [status] is null)";

                command.CommandText = command_query;

                command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = accountId;
                var affected_row = command.ExecuteNonQuery();

                if (affected_row == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                m.ReleaseMutex();
            }

        }

        static public dynamic DynamicIGAccountByID(string accountId)
        {
            dynamic ig_account = null;

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                var command_query =
                    @"select *
                    from ig_account
                    join account_info on ig_account.id=account_info.accountId
					join engagesettings on account_info.engagetype=engagesettings.type
					where [id]=@id";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = accountId;
                    using (var dataReader = command.ExecuteReader())
                    {
                        dataReader.Read();
                        string id = dataReader["id"].ToString();
                        string user = dataReader["user"].ToString();
                        string password = dataReader["password"].ToString();
                        string niche = dataReader["niche"].ToString();
                        string fullname = dataReader["fullname"].ToString();
                        string phone_device = dataReader["phone_device"].ToString();
                        string type = dataReader["type"] == null ? "" : dataReader["type"].ToString();
                        string mode = dataReader["mode"].ToString();
                        dynamic aio_settings = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(dataReader["aio_settings"].ToString());
                        string time_per_session = dataReader["time_per_session"].ToString();
                        ig_account = new
                        {
                            id,
                            fullname,
                            niche,
                            phone_device,
                            type,
                            mode,
                            user,
                            password,
                            aio_settings,
                            time_per_session
                        };
                    }
                }
            }

            return ig_account;
        }

        static private List<string> AvailableAccountToDoAction(SqlCommand command)
        {
            List<string> availableAccountIds = new List<string>();

            PLAutoHelper.SQLHelper.Execute(() =>
            {
                var command_query =
                    @"select [id]
                    from ig_account
                    where
	                    [pc_devices]=@pc_devices and
	                    ([state]='ACTIVE') and
	                    --check active hours
	                    (
		                    [id] in
		                    (
			                    select [accountId]
			                    from account_info
			                    where
			                    (CONVERT(VARCHAR(8), GETDATE(), 108) > CAST([active_from] as time) and CONVERT(VARCHAR(8), GETDATE(), 108) < CAST([active_to] as time)) and
			                    (CAST([active_from] as time) < CAST([active_to] as time))
		                    ) or
		                    [id] in
		                    (
			                    select [accountId]
			                    from account_info
			                    where
			                    (CONVERT(VARCHAR(8), GETDATE(), 108) > CAST([active_from] as time) or CONVERT(VARCHAR(8), GETDATE(), 108) < CAST([active_to] as time)) and
			                    (CAST([active_from] as time) > CAST([active_to] as time))
		                    )
	                    )and
	                    ([status]='' or [status] is null) and
	                    (register like '%20%')";

                command.CommandText = command_query;

                command.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = Program.form.pc_devices;

                using (var dataReader = command.ExecuteReader())
                {
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            availableAccountIds.Add(dataReader["id"].ToString());
                        }
                    }
                }
            }/*, "ig_account"*/);

            return availableAccountIds;
        }

        static private List<string> AvailableAccountToDoAction_AIO(SqlCommand command)
        {
            List<string> availableAccountIds = new List<string>();
            string command_query = null;

            PLAutoHelper.SQLHelper.Execute(() =>
            {
                command_query =
                    @"select [id]
                    from ig_account
                    where
	                    ([state]='ACTIVE') and
	                    --check active hours
	                    (
		                    [id] in
		                    (
			                    select [accountId]
			                    from account_info
			                    where
			                    (CONVERT(VARCHAR(8), GETDATE(), 108) > CAST([active_from] as time) and CONVERT(VARCHAR(8), GETDATE(), 108) < CAST([active_to] as time)) and
			                    (CAST([active_from] as time) < CAST([active_to] as time))
		                    ) or
		                    [id] in
		                    (
			                    select [accountId]
			                    from account_info
			                    where
			                    (CONVERT(VARCHAR(8), GETDATE(), 108) > CAST([active_from] as time) or CONVERT(VARCHAR(8), GETDATE(), 108) < CAST([active_to] as time)) and
			                    (CAST([active_from] as time) > CAST([active_to] as time))
		                    )
	                    )and
	                    ([status]='' or [status] is null) and
	                    (register like '%20%')";

                command.CommandText = command_query;

                using (var dataReader = command.ExecuteReader())
                {
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            availableAccountIds.Add(dataReader["id"].ToString());
                        }
                    }
                }
            }/*, "ig_account"*/);

            //get list accounts claimed in last 20 minutes
            List<string> last_20_mins_accountIds = new List<string>();

            command_query =
                @"select [accountid]
                from actionlog
                where
	                [action]='last_use_on_mobile' and
	                DATEDIFF(minute,do_at,getdate())<20
                order by id desc";

            command.CommandText = command_query;

            using (var dataReader = command.ExecuteReader())
            {
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        last_20_mins_accountIds.Add(dataReader["accountid"].ToString());
                    }
                }
            }

            availableAccountIds = availableAccountIds.Except(last_20_mins_accountIds).ToList();


            return availableAccountIds;
        }

        static private int DayOfWeek()
        {
            var command_query = "select DATEPART(weekday, getdate())";
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand(command_query, conn))
                {
                    var dayofweek = int.Parse(command.ExecuteScalar().ToString());

                    return dayofweek;
                }
            }
        }

        static private DateTime FirstDayWeek()
        {
            var command_query =
                @"DECLARE @dayofweek int = DATEPART(weekday, getdate()) DECLARE @firstday_week datetime;

                 IF (@dayofweek > 1) BEGIN
                SET @firstday_week = DATEADD(DAY, -1 * (@dayofweek - 1), getdate()) END ELSE
                SET @firstday_week = getdate()
                select @firstday_week";
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand(command_query, conn))
                {
                    var FirstDayWeek = DateTime.Parse(command.ExecuteScalar().ToString());

                    return FirstDayWeek;
                }
            }
        }

        static private bool IsOffDayById(string accountId, DateTime firstday_week, SqlCommand command)
        {
            var dayofweek = DayOfWeek();

            var command_query =
                @"SELECT top 1 [value]
                FROM actionlog
                WHERE [accountid] = @temp_accountId
                  AND [action] = 'dayoffweek'
                  AND DATEDIFF(DAY, @firstday_week, do_at)>= 0";

            //using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            //{
            //    conn.Open();

            //    using (var command = new SqlCommand(command_query, conn))
            //    {
            command.CommandText = command_query;
            command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = accountId;
            command.Parameters.Add("@firstday_week", System.Data.SqlDbType.DateTime).Value = firstday_week;

            using (var dataReader = command.ExecuteReader())
            {
                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    var offday = int.Parse(dataReader["value"].ToString());

                    if (offday == dayofweek)
                        return true;
                }
                else
                    return true;
            }
            //    }
            //}

            return false;
        }


        static List<string> offthisday_ids = new List<string>();

        static private List<string> OffThisDay_AccountIds(DateTime firstday_week, SqlCommand command)
        {

            if (offthisday_ids.Count > 0)
                return offthisday_ids;

            List<string> accountIds = new List<string>();

            var dayofweek = DayOfWeek();

            var command_query =
                @"select [accountid]
                from actionlog
                where
	                [action]='dayoffweek'
	                AND DATEDIFF(DAY, @firstday_week, do_at)>= 0
	                AND [value]=@value";

            command.CommandText = command_query;
            command.Parameters.Add("@firstday_week", System.Data.SqlDbType.DateTime).Value = firstday_week;
            command.Parameters.Add("@value", System.Data.SqlDbType.Int).Value = dayofweek;

            using (var dataReader = command.ExecuteReader())
            {
                if (dataReader.HasRows)
                {
                    while(dataReader.Read())
                    {
                        accountIds.Add(dataReader["accountid"].ToString());
                    }
                }
            }

            offthisday_ids.AddRange(accountIds);

            return accountIds;
        }


        /// <summary>
        /// is action blocked or unsual actvity in last x days
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="days"></param>
        static private bool IsBlockorUnsualinXDays(string accountId, SqlCommand command, int days = 3)
        {
            var command_query =
                @"SELECT count(*)
                FROM actionlog
                WHERE [accountid] = @temp_accountId
                  AND ([action] = 'Action Blocked' or [action] = 'Unsual Activity' or [action]='solved_unsualactivity')
                  AND DATEDIFF(DAY, [do_at], getdate())< @days";


            command.CommandText = command_query;
            command.Parameters.Add("@temp_accountId", System.Data.SqlDbType.Int).Value = accountId;
            command.Parameters.Add("@days", System.Data.SqlDbType.Int).Value = days;

            var count = int.Parse(command.ExecuteScalar().ToString());

            if (count > 0)
                return true;


            return false;
        }


        static private List<string> BlockorUnsualinXDays_AccountIds(SqlCommand command, int days = 3)
        {
            List<string> accountIds = new List<string>();

            var command_query =
                @"SELECT [accountid]
                FROM actionlog
                WHERE 
                  ([action] = 'Action Blocked' or [action] = 'Unsual Activity' or [action]='solved_unsualactivity')
                  AND DATEDIFF(DAY, [do_at], getdate())< @days
				group by accountid";


            command.CommandText = command_query;
            command.Parameters.Add("@days", System.Data.SqlDbType.Int).Value = days;

            using (var dataReader = command.ExecuteReader())
            {
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        accountIds.Add(dataReader["accountid"].ToString());
                    }
                }
            }

            return accountIds;
        }

        #endregion
    }
}
