﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.AIOHelper.DBLogic
{
    static class QueueTask
    {
        static public List<dynamic> ReturnTask(string accountId, int max_error_times = 3)
        {
            List<dynamic> queue_tasks = new List<dynamic>();

            var command_query =
                @"select * from queuetasks
                where 
	                ([status]='' or [status] is null) and
	                ([schedule_time] is null or DATEDIFF(minute,[schedule_time],getdate())>0) and --neu da qua schedule thi k duoc qua 12 hours
	                [accountid]=@accountId and
                    [error_times]<=@max_error_times
                order by high_priority desc, [schedule_time] asc";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                    command.Parameters.Add("@max_error_times", System.Data.SqlDbType.Int).Value = max_error_times;
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var taskid = dataReader["taskid"].ToString();
                            var action = dataReader["action"].ToString();
                            var value = dataReader["value"].ToString();

                            queue_tasks.Add(new
                            {
                                taskid,
                                action,
                                json_task = value
                            });
                        }
                    }
                }

                //set status to claimed
                foreach (var t in queue_tasks)
                {
                    var taskid = t.taskid.ToString();

                    command_query = "update queuetasks set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"' where [taskid]=@taskid";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@taskid", System.Data.SqlDbType.Int).Value = taskid;
                        command.ExecuteNonQuery();
                    }
                }

            }

            return queue_tasks;
        }

        static public void UpdateTask_Done(string taskId)
        {
            var command_query =
                @"update queuetasks
                set
	                [status]='Done', [completed_time]=getdate()
                where [taskid]=@id";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = taskId;
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
