﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    class SelfShareStoriesTask : IGTask
    {

        string deviceID, deviceName;

        public SelfShareStoriesTask(Class.IGDevice iGDevice)
        {
            this.IGDevice = iGDevice;
            this.deviceID = this.IGDevice.deviceID;
            this.deviceName = this.IGDevice.ld_devicename;
        }

        public SelfShareStoriesTask(string deviceID, string deviceName)
        {
            this.deviceID = deviceID;
            this.deviceName = deviceName;
        }

        public override void DoTask()
        {
            //Go ig home
            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "instagram://profile", "com.instagram.android");

            //Click my profile


            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.SelfComment_EditProfile, 5, 3, 2);

            //Click latest post

            //get point of posticon tab

            var screen = ADBSupport.ADBHelpers.ScreenShoot(deviceID);

            var posticon_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, ADBSupport.BMPSource.LikeUserPost_PostTabIcon);

            var latest_post_y = ADBSupport.ADBHelpers.ConvertPointToPercentage(deviceID, "Y", posticon_point.Value.Y) + 7;


            //Find rectangle of latest post
            var latest_post_rec = new Class.RecPercentage(new { x = 5, y = latest_post_y }, 25, 10);

            //Click post

            ADBSupport.ADBHelpers.TapRecTanglePercentage(deviceID, latest_post_rec);

            //Wait Posts title
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.LikeUserPost_WaitPosts, 4, 1, 2);


            //Click Share

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.SelfStories_ShareStoriesBtn, ADBSupport.BMPSource.SelfStories_AddPostToYourStory, 2, "");

            //Click add post to your story

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.SelfStories_AddPostToYourStory, ADBSupport.BMPSource.SelfStories_SendToBtn, 2, "");

            //Click your story

            //--demoA: 71.7,91.9 -demoA: 8.6,90.5 --demoC: 13.5, 93.6

            ADBSupport.ADBHelpers.ClickRelative(deviceID, ADBSupport.BMPSource.SelfStories_SendToBtn, new { x = 71.7, y = 91.9 }, new { x = 8.6, y = 90.5 }, new { x = 13.5, y = 93.6 });

            ADBSupport.ADBHelpers.Delay(10, 15);
        }
    }
}
