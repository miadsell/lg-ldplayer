﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    /// <summary>
    /// manage message, comment of ig in firefox browser manually
    /// </summary>
    static public class FFLiteSwitch
    {
        //Lauch switch firefox device 
        static public string LaunchSwitchFFDevice(string deviceName, string ldplayer_path, string lddata_path)
        {
            int switch_index = ADBSupport.LDHelpers.LDPlayer_switch_manual;
            //Check if switch is open, if is opening, return
            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + ldplayer_path + " \r\n ldconsole isrunning --index " + switch_index);


            if (!command.Contains("stop"))
            {
                return "Can't start this because device is running. Must close first!";
            }


            //Launch device (change device info, set proxy,....)
            Thread.SetData(ADBSupport.LDHelpers._ld_index, switch_index);

            //Read device info from deviceinfo.ig

            dynamic deviceinfo = IGHelpers.DBHelpers.GetDeviceInformation(deviceName);

            ADBSupport.LDHelpers.RestoreDevice(deviceName, deviceinfo);

            var devices = KAutoHelper.ADBHelper.GetDevices();

            if (devices.Count != 0)
            {
                return "Simple Launch! Just accept 1 device a time. Please disconnect all android devices to this computer";
            }

        //Check if proxifier is running

        CheckProxifier:
            var hwnd_proxifier = KAutoHelper.AutoControl.FindHandle(IntPtr.Zero, "", "Proxifier");

            if (hwnd_proxifier == IntPtr.Zero)
            {
                Process.Start(ADBSupport.LDHelpers.LDPlayer_proxifier_path);

                Thread.Sleep(5000);
                goto CheckProxifier;
            }

            throw new System.ArgumentException("Not handle proxy below line");
            RestClient client = new RestClient("http://" + ""/*ADBSupport.LDHelpers.proxy_server_ip*/ + ":10000/");

            var proxy_str_for_request = ADBSupport.LDHelpers.LDPlayer_manual_proxy.Replace(":5", ":4");

            var request = new RestRequest("reset?proxy=" + proxy_str_for_request, Method.GET);

            var query = client.Execute(request);

            Thread.Sleep(5000);

        LaunchLDPlayer:
            ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + ldplayer_path + " \r\n ldconsole modify --index "+switch_index+" --resolution 540,960,240"); Thread.Sleep(3000);
            ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + ldplayer_path + " \r\n ldconsole launch --index " + switch_index);

            while(true)
            {
                devices = KAutoHelper.ADBHelper.GetDevices();

                if (devices.Count == 1)
                {
                    if(devices[0].Contains("offline"))
                    {
                        //close ldplayer
                        ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + ldplayer_path + " \r\n ldconsole quit --index " + switch_index);

                        Thread.Sleep(5000);

                        goto LaunchLDPlayer;
                    }
                    break;
                }
                Thread.Sleep(5000);
            }

            var deviceID = devices.First();

            

            //Check to make sure ldplayer ip is not same with current pc ip

            var pc_ip = (new System.Text.RegularExpressions.Regex("(?<=ifconfig.me\\/ip\\r\\n)(.*?)(?=\\r\\n)")).Match(ADBSupport.ADBHelpers.ExecuteCMDTask("curl ifconfig.me/ip")).Value;

            var ldplayer_ip = (new System.Text.RegularExpressions.Regex("(?<=ifconfig.me\\/ip\\r\\n)(.*?)(?=\\r\\n)")).Match(ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell curl ifconfig.me/ip")).Value;

            if (pc_ip.Contains(".") & ldplayer_ip.Contains(".") && pc_ip != ldplayer_ip)
            {

            }
            else throw new System.ArgumentException("IP not enabled");

            //Clear firefox

            command = ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell pm clear org.mozilla.rocket");

            if(!command.Contains("Success"))
            {
                throw new System.ArgumentException("Can't clear data of app");
            }


            //clear download folder
            ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell \"su -c 'rm -r sdcard/Download/*'\"");

            //Check if has firefox data

            var ftp_ffdata_path = "/firefoxdata/" + deviceName + "/";

            if (IGHelpers.Helpers.FTP_IsDirectoryExisted(ftp_ffdata_path))
            {
                //var ffvmdks = Directory.GetFiles(ffdata_path, "ffdata*.vmdk", SearchOption.TopDirectoryOnly).OrderByDescending(d => new FileInfo(d).CreationTime).ToList<string>();

                var ftp_ffvmdks = IGHelpers.Helpers.FTP_GetFileListing(ftp_ffdata_path, "ffdata");

                ftp_ffvmdks = ftp_ffvmdks.OrderByDescending(x => x.Modified).ToList();

                if (ftp_ffvmdks.Count > 0)
                {
                    //push firefox data

                    var ftp_choiced_data = ftp_ffvmdks[0];

                    var temp_choiced_data = "Temp\\" + deviceName+"\\ffdata_temp.vmdk";

                    IGHelpers.Helpers.FTP_DownloadFile(ftp_choiced_data.FullName, temp_choiced_data);

                    command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + ldplayer_path + " \r\n ldconsole restoreapp --index " + switch_index + " --packagename \"org.mozilla.rocket\" --file \"" + temp_choiced_data + "\"");

                    File.Delete(temp_choiced_data);

                }

            }

            //Launch firefox

            ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell monkey -p org.mozilla.rocket -c android.intent.category.LAUNCHER 1");


            return deviceID;
        }

        static public void LoginFFInstagram(string deviceID, string user, string password, string deviceName, string ldplayer_path, string lddata_path)
        {
            int switch_index = ADBSupport.LDHelpers.LDPlayer_switch_manual;

            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.LaunchManual_PhoneNumber, "LaunchManual_PhoneNumber");

            ADBSupport.ADBHelpers.Delay();

            ADBSupport.ADBHelpers.InputTextWithDelay(deviceID, user);


            ADBSupport.ADBHelpers.Delay();

            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.LaunchManual_Password, "LaunchManual_Password");

            ADBSupport.ADBHelpers.Delay();

            ADBSupport.ADBHelpers.InputTextWithDelay(deviceID, password);


            ADBSupport.ADBHelpers.Delay();

            //Click login

            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.LaunchManual_LoginBtn, "LaunchManual_LoginBtn");

            ADBSupport.ADBHelpers.Delay(6, 10);


            //backup ig app
            var ftp_ffdata_path = "/firefoxdata/" + deviceName + "/";

            if(!IGHelpers.Helpers.FTP_IsDirectoryExisted(ftp_ffdata_path))
            {
                IGHelpers.Helpers.FTP_CreateDirectory(ftp_ffdata_path);
            }

            var backup_ffdata_name = DateTime.Now.Ticks.ToString() + ".vmdk";

            string ftp_path = ftp_ffdata_path + "ffdata" + backup_ffdata_name;

            var local_temp_path = "Temp\\" + deviceName + "\\" + "ffdata" + backup_ffdata_name;

            var command_backup = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + ldplayer_path + " \r\n ldconsole backupapp --index " + switch_index + " --packagename \"org.mozilla.rocket\" --file \"" + local_temp_path + "\"");

            //upload to server
            IGHelpers.Helpers.FTP_UploadFile(local_temp_path, ftp_path);

            Thread.Sleep(5000);

        }

        //static public void CloseFFSwitchDevice(string deviceName, string deviceID, string ldplayer_path, string lddata_path)
        //{
        //    int switch_index = ADBSupport.LDHelpers.LDPlayer_switch_manual;
        //    //backup ig app
        //    var ffdata_path = System.IO.Directory.GetParent(ADBSupport.LDHelpers.LDPlayerDATA_path) + "\\firefoxdata\\" + deviceName + "\\";

        //    if (!Directory.Exists(ffdata_path))
        //    {
        //        Directory.CreateDirectory(ffdata_path);


        //    }


        //    string path = ffdata_path + "ffdata" + DateTime.Now.Ticks.ToString() + ".vmdk";


        //    var command_backup = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + ldplayer_path + " \r\n ldconsole backupapp --index " + switch_index + " --packagename \"org.mozilla.rocket\" --file \"" + path + "\"");

        //    Thread.Sleep(5000);

        //    //clear ig app data

        //    var command = ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell pm clear org.mozilla.rocket");

        //    //clear download folder
        //    ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell \"su -c 'rm -r sdcard/Download/*'\"");

        //    //Close device
        //    ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + ldplayer_path + " \r\n ldconsole quit --index " + switch_index);
        //}
    }
}
