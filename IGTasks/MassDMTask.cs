﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    class MassDMTask:IGTask
    {
        string deviceName;
        dynamic ig_account, settings;

        public MassDMTask(Class.IGDevice IGDevice, dynamic ig_account)
        {
            this.IGDevice = IGDevice;
            this.ig_account = ig_account;

            this.deviceName = this.IGDevice.ld_devicename;
        }

        public override void DoTask()
        {
            MassDMUser();
        }


        private void MassDMUser()
        {
            //get settings of this account

            GetMassDMUserSettings();

            //Check if settings is null -> yes -> return

            if (settings == null)
                return;

            //get target massdm this date
            var target_massdm_date = SetTargetMassDM();

            //get current massdm count in this date

            var currentdate_massdm = CurrentMassDMThisDate();

            if (currentdate_massdm >= target_massdm_date)
                return;


            //set massdm for this session
            var session_massdm_count = SetMassDMCountThisSession();

            //insert log new_session (use to track finish a block_session
            IGHelpers.DBHelpers.AddActionLog(IGDevice.ld_devicename, "new_massdm_session");

            //run

            //use sleep mô phỏng run action

            for (int i = 0; i < session_massdm_count; i++)
            {
                //get target_profile to massdm

                var target_profile = AIOHelper.DBLogic.GetTargetAccount_SpecificTasks.MassDM(ig_account);

                if (target_profile == null)
                    break;

                //DO ACTION //DO ACTION //DO ACTION //DO ACTION
                //IGThreads.ThreadHelpers.DoMassDM(this.IGDevice, ig_account, comment);
                var status = AIOHelper.ThreadHelper.DoMassDM.Run(this.IGDevice, ig_account, target_profile);

                if (status == "Action Blocked")
                {
                    break;
                }

                //Add action log
                IGHelpers.DBHelpers.AddActionLog(IGDevice.ld_devicename, "massdm_task", target_profile.actionlog, target_profile.user);

                //Update status to igtoolkit scraping user

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                        {
                            using (var command = new SqlCommand("update scrapeusers set [status]='' where [UserName]=@UserName", conn, sqlTransaction))
                            {
                                command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = target_profile.user;
                                command.ExecuteNonQuery();
                            }

                            sqlTransaction.Commit();
                        }
                    });
                }

                int sleep_action = RandomFromStr(((string)settings.sleep_action).Split('|')[1]);

                Thread.Sleep(TimeSpan.FromSeconds(sleep_action));
            }

            //check if count session >= block_session_count

            if (IsFinishedCurrentBlockSession())
            {
                SetNextSessionTime(true);
            }
            else
            {
                //set next session time

                var next_session_time = SetNextSessionTime();
            }

            Thread.Sleep(5000);
        }


        #region --MassDM User Task Helpers--

        private void GetMassDMUserSettings()
        {
            //Get engagetype from account_info
            var massdmtype = "";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [action] from actionlog where [accountId]=" + ig_account.id + " and [action] like 'massdm-%' and DATEDIFF(day,[do_at],getdate())=0", conn))
                {
                    var massdmtype_object = sqlCommand.ExecuteScalar();
                    if (massdmtype_object != null)
                        massdmtype = sqlCommand.ExecuteScalar().ToString();
                }
            }

            if (massdmtype == "")
            {
                massdmtype = SetMassDMTypeAutomatically(massdmtype);
                if (massdmtype == "")
                    return;

                //add massdmtype to log

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var sqlCommand = new SqlCommand("insert into [actionlog] ([accountid],[action]) values (" + ig_account.id + ",'" + massdmtype + "')", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }

            //Get settings from engagetype

            settings = SettingsOfMassDMType(massdmtype);
        }

        class massdmsetting
        {
            public string setting_name;
            public int count;

            public massdmsetting(string setting_name, int count = 0)
            {
                this.setting_name = setting_name;
                this.count = count;
            }
        }

        private string SetMassDMTypeAutomatically(string current_massdmtype)
        {


            //get all current settings

            string engagetype = "";
            List<massdmsetting> massdm_history = new List<massdmsetting>();

            massdm_history.Add(new massdmsetting("massdm-warmlite"));
            massdm_history.Add(new massdmsetting("massdm-warmup"));
            massdm_history.Add(new massdmsetting("massdm-default"));

            //using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            //{
                //conn.Open();

                //using (var sqlCommand = new SqlCommand("ig_EngageSettings", conn))
                //{
                //    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                //    sqlCommand.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = ig_account.id;
                //    sqlCommand.Parameters.Add("@return_value", System.Data.SqlDbType.VarChar).Value = "engagetype";

                //    var engage_value = sqlCommand.ExecuteScalar();

                //    engagetype = engage_value == null ? "" : engage_value.ToString();
                //}

                engagetype = AIOHelper.DBLogic.EngageSettings.HistoryOfMassTask(ig_account.id, "engagetype")[0];

                //using (var sqlCommand = new SqlCommand("ig_EngageSettings", conn))
                //{
                //    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                //    sqlCommand.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = ig_account.id;
                //    sqlCommand.Parameters.Add("@return_value", System.Data.SqlDbType.VarChar).Value = "massdm-history";

                //    using (var dataReader = sqlCommand.ExecuteReader())
                //    {
                //        if (dataReader.HasRows)
                //        {
                //            while (dataReader.Read())
                //            {
                //                for (int i = 0; i < massdm_history.Count; i++)
                //                {
                //                    if (dataReader["action"].ToString() == massdm_history[i].setting_name)
                //                    {
                //                        massdm_history[i].count = int.Parse(dataReader["times"].ToString());
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}

                List<dynamic> histories = AIOHelper.DBLogic.EngageSettings.HistoryOfMassTask(ig_account.id, "massdm-history");

                foreach(var h in histories)
                {
                    for (int i = 0; i < massdm_history.Count; i++)
                    {
                        if (h.action == massdm_history[i].setting_name)
                        {
                            massdm_history[i].count = int.Parse(h.times.ToString());
                        }
                    }
                }
            //}

            if (engagetype != "default")
                return "";

            //massdm-warmlite

            if (massdm_history.FirstOrDefault(s => s.setting_name == "massdm-warmlite").count <= SettingsOfMassDMType("massdm-warmlite").days)
            {
                return "massdm-warmlite";
            }

            if (massdm_history.FirstOrDefault(s => s.setting_name == "massdm-warmup").count <= SettingsOfMassDMType("massdm-warmup").days)
            {
                return "massdm-warmup";
            }

            return "massdm-default";
        }


        private dynamic SettingsOfMassDMType(string massdmtype)
        {
            var settings_str = "";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [settings] from engagesettings where [type]='" + massdmtype + "'", conn))
                {
                    settings_str = sqlCommand.ExecuteScalar().ToString();
                }
            }

            List<string> settings = settings_str.Split(new string[] { "***" }, StringSplitOptions.None).ToList<string>();

            string random = settings.FirstOrDefault(s => s.Contains("random|"));
            string increase = settings.FirstOrDefault(s => s.Contains("increase|"));
            string action_per_session = settings.FirstOrDefault(s => s.Contains("action_per_session|"));
            string sleep_session = settings.FirstOrDefault(s => s.Contains("sleep_session|"));
            string sleep_action = settings.FirstOrDefault(s => s.Contains("sleep_action|"));
            string max_massdm = settings.FirstOrDefault(s => s.Contains("max_massdm|"));
            string block_session = settings.FirstOrDefault(s => s.Contains("block_session|"));
            string sleep_per_block = settings.FirstOrDefault(s => s.Contains("sleep_per_block|"));
            int days = settings.FirstOrDefault(s => s.Contains("days|")) == null ? -1 : int.Parse(settings.FirstOrDefault(s => s.Contains("days|")).Split('|')[1]);

            return new
            {
                random,
                increase,
                action_per_session,
                sleep_session,
                sleep_action,
                days,
                max_massdm,
                block_session,
                sleep_per_block
            };
        }


        private int SetTargetMassDM()
        {
            int target_massdm = 0;
            //get target massdm from db
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [value] from actionlog where [accountId]=" + ig_account.id + " and [action] like 'target_massdm' and DATEDIFF(day,[do_at],getdate())=0", conn))
                {
                    var target_massdm_obj = sqlCommand.ExecuteScalar();
                    if (target_massdm_obj != null)
                        target_massdm = int.Parse(target_massdm_obj.ToString());
                }
            }

            if (target_massdm == 0)
            {
                int pre_target_massdm = 0;

                // dynamic pre_target_massdm_dyn = null;
                //get target massdm of previous day
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var sqlCommand = new SqlCommand("select count(*) from actionlog where [action]='massdm_task' and DATEDIFF(day,convert(datetime, (select top 1 [do_at] from actionlog where [action]='massdm_task' and [accountid]=" + ig_account.id + " order by id desc),21),[do_at])=0 and [accountid]=" + ig_account.id, conn))
                    {

                        pre_target_massdm = int.Parse(sqlCommand.ExecuteScalar().ToString());

                        //using (var dataReader = sqlCommand.ExecuteReader())
                        //{
                        //    if (dataReader.HasRows)
                        //    {
                        //        dataReader.Read();
                        //        var value = int.Parse(dataReader["value"].ToString());
                        //        var date = DateTime.Parse(dataReader["do_at"].ToString());

                        //        pre_target_massdm_dyn = new
                        //        {
                        //            value,
                        //            date
                        //        };
                        //    }
                        //}
                    }
                }

                //if (pre_target_massdm_dyn != null)
                //{
                //    pre_target_massdm = pre_target_massdm_dyn.value;
                //}

                var increase = 0;

                try
                {
                    if (settings.increase != null)
                    {
                        string increase_str = settings.increase.ToString();
                        increase = RandomFromStr(increase_str.Split('|')[1]);
                    }
                }
                catch { }

                if (pre_target_massdm == 0 || increase == 0)
                {
                    //random value from settings.random

                    string random = settings.random.ToString();

                    target_massdm = RandomFromStr(random.Split('|')[1]);
                }
                else
                {
                    //pre_target_massdm + settings.random


                    target_massdm = pre_target_massdm + increase;
                }

                //get max_massdm i has, if target_massdm > max_massdm => set target_massdm in max_massdm range

                try
                {
                    if (settings.max_massdm != null)
                    {
                        string max_massdm_str = settings.max_massdm.ToString();

                        int max = int.Parse(max_massdm_str.Split('|')[1].Split('-')[1]);

                        if (target_massdm > max)
                            target_massdm = RandomFromStr(max_massdm_str.Split('|')[1]);
                    }
                }
                catch { }

                //check neu thoi diem thuc hien massdm_task gan nhat > 3 ngay thi target_massdm=target_massdm/2


                target_massdm = target_massdm / (IsLongTimeNoDoingTask());

                //set and add log to actionlog

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'target_massdm'," + target_massdm.ToString() + ")", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }




            return target_massdm;

        }

        private int CurrentMassDMThisDate()
        {
            int count = 0;
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select count(*) from actionlog where [accountId]=" + ig_account.id + " and [action] = 'massdm_task' and DATEDIFF(day,[do_at],getdate())=0", conn))
                {
                    count = int.Parse(sqlCommand.ExecuteScalar().ToString());
                }
            }

            return count;
        }

        private int SetMassDMCountThisSession()
        {
            string action_per_session = settings.action_per_session.ToString();

            var count = RandomFromStr(action_per_session.Split('|')[1]);

            return count;
        }

        private DateTime SetNextSessionTime(bool finish_block_massdm_session = false)
        {
            string sleep_session = settings.sleep_session.ToString();

            int sleep_seconds = RandomFromStr(sleep_session.Split('|')[1]);

            var next_session_time = DateTime.Now.AddSeconds(sleep_seconds);


            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                if (finish_block_massdm_session)
                {
                    //tuc la da het 1 block, set sleep dài hơn theo sleep_per_block settings

                    string sleep_per_block = settings.sleep_per_block.ToString();

                    sleep_seconds = RandomFromStr(sleep_per_block.Split('|')[1]);

                    //random block_massdm_session_count cho block ke tiep

                    var block_session = RandomFromStr(settings.block_session.ToString().Split('|')[1]);

                    //insert to log


                    using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'block_massdm_session_count','" + block_session.ToString() + "')", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }

                }


                using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'massdm_next_session','" + next_session_time.ToString() + "')", conn))
                {
                    sqlCommand.ExecuteNonQuery();
                }
            }

            return next_session_time;
        }

        private int IsLongTimeNoDoingTask()
        {
            string latest_dotask = null;
            //get latest do massdm_tasking day

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [do_at] from actionlog where [accountid]=" + ig_account.id + " and [action]='target_massdm' order by id desc", conn))
                {
                    using (var dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();
                            latest_dotask = dataReader["do_at"].ToString();
                        }
                    }
                }
            }

            if (latest_dotask == null) //chua thuc hien task bao gio
            {
                return 1;
            }

            //neu nhu khoang cach >10 thi chia 3

            if (DateTime.Now.Subtract(DateTime.Parse(latest_dotask)).TotalDays > 10)
                return 3;

            //neu khoang cach >3 thi chia 2
            if (DateTime.Now.Subtract(DateTime.Parse(latest_dotask)).TotalDays > 3)
                return 2;

            return 1;
        }

        #endregion

        #region --Block session--

        private bool IsFinishedCurrentBlockSession()
        {
            int current_new_massdm_session = 0;
            dynamic block_massdm_session = null;
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();


                //get latest block_session log

                using (var sqlCommand = new SqlCommand("select [id],[value] from actionlog where [accountid]=" + ig_account.id + " and [action]='block_massdm_session_count' and DATEDIFF(day,do_at,getdate())=0 order by id desc", conn))
                {
                    using (var dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();

                            var id = dataReader["id"].ToString();
                            var value = dataReader["value"].ToString();

                            block_massdm_session = new
                            {
                                id,
                                value
                            };
                        }
                    }
                }



                if (block_massdm_session == null)
                {
                    //tuc la chua set block_massdm_session_count
                    var block_session = RandomFromStr(settings.block_session.ToString().Split('|')[1]);

                    //insert to log

                    using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'block_massdm_session_count','" + block_session.ToString() + "')", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }

                    return false;

                }

                //count new_massdm_session with id > if of block_session log

                using (var sqlCommand = new SqlCommand("select count(*) from actionlog where [accountid]=" + ig_account.id + " and [action]='new_massdm_session' and [id]>" + block_massdm_session.id, conn))
                {
                    current_new_massdm_session = int.Parse(sqlCommand.ExecuteScalar().ToString());
                }

            }

            if (current_new_massdm_session >= int.Parse(block_massdm_session.value))
                return true;

            return false;
        }

        #endregion

        private int RandomFromStr(string random_str)
        {
            var from = int.Parse(random_str.Split('-')[0]);
            var to = int.Parse(random_str.Split('-')[1]);

            return (new Random(Guid.NewGuid().GetHashCode())).Next(from, to);
        }
    }
}
