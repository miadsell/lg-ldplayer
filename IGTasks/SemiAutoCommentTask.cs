﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    class SemiAutoCommentTask : IGTask
    {


        string deviceName, deviceID;
        dynamic ig_account, comment;

        public SemiAutoCommentTask(string deviceID, string deviceName, dynamic comment)
        {
            this.deviceID = deviceID;
            this.deviceName = deviceName;
            this.comment = comment;
        }

        public SemiAutoCommentTask(Class.IGDevice iGDevice, dynamic ig_account, dynamic comment)
        {
            this.IGDevice = iGDevice;
            this.deviceID = this.IGDevice.deviceID;
            this.deviceName = this.IGDevice.ld_devicename;
            this.ig_account = ig_account;
            this.comment = comment;
        }

        public override void DoTask()
        {
            SemiAutoComment();
        }


        private void SemiAutoComment()
        {

            //comment = new
            //{
            //    link = "https://www.instagram.com/p/CC52g2HsAEh/",
            //    text = "cả nhà đáng iu quá ạ"
            //};

            int retry = 0;
            //open intent link
            OpenLink:
            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", comment.link , "com.instagram.android");

            //make some simulate


            //add comment
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.SemiAutoComment_CommentBtn, 2, 5, 2);
            }
            catch
            {
                retry++;
                if (retry > 3)
                    throw new System.ArgumentException("Can't open intent");
                goto OpenLink;
            }

            //Click comment icon

            var commentbtn_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.SemiAutoComment_CommentBtn);

            ADBSupport.ADBHelpers.RandomTapImageByPercentage(deviceID, commentbtn_point.x, commentbtn_point.y, "SemiAutoComment_CommentBtn");

            //wait addcomment box

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.SemiAutoComment_AddACommentBox, 5, 5, 3);

            //Copy comment text

            var guid = Guid.NewGuid().GetHashCode().ToString();
            //var content_path = ADBSupport.LDHelpers.LDPlayerDATA_path + "\\" + deviceName + "\\temp_img\\comment_" + guid.Substring(guid.Length - 6) + ".txt";

            ////content_path = @"F:\DOWNLOAD\comment_951445.txt";

            //File.WriteAllText(content_path, comment.text);

            //ADBSupport.ADBHelpers.CopyTextFromEsEditor(deviceID, content_path);

            ////Reopen ig

            //ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell monkey -p com.instagram.android -c android.intent.category.LAUNCHER 1", 30);

            ////Paste
            //while (true)
            //{
            //    KAutoHelper.ADBHelper.LongPress(deviceID, 62, 464, 1000);

            //    try
            //    {
            //        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PasteBtn, 1, 0, 0);
            //        break;
            //    }
            //    catch
            //    { }
            //}

            ////Click paste

            //ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.PasteBtn); ADBSupport.ADBHelpers.Delay(5, 10);

            //Send text
            ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID, IGDevice.ld_devicename, comment.text);

            //Hit post

            var post_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.SemiAutoComment_PostComment);

            ADBSupport.ADBHelpers.RandomTapImageByPercentage(deviceID, post_point.x, post_point.y, "SemiAutoComment_PostComment");

            ADBSupport.ADBHelpers.Delay(10, 15);

            //wait posting disappear

            while(true)
            {
                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.SemiAutoComment_Posting, 1, 1);
                }
                catch
                {
                    break;
                }

                Thread.Sleep(1000);
            }

            //Get screenshot of comment just made - can be used to like to get on top

            var comment_screenshot = ADBSupport.ADBHelpers.CropScreenshotRelative(deviceID, ADBSupport.BMPSource.SemiAutoComment_Tag_HeartBtn, new { x = 90, y = 35.8 }, new { x = 17.6, y = 33.9 }, new { x = 84.1, y = 36.8 });

            //var comment_screenshot_path = ADBSupport.LDHelpers.LDPlayerDATA_path + "\\" + deviceName + "\\temp_img\\comment_screenshot_" + guid.Substring(guid.Length - 6) + ".png";

            ////Save to dnplayer data folder
            //comment_screenshot.Save(comment_screenshot_path, System.Drawing.Imaging.ImageFormat.Png);

            //var filename = Path.GetFileName(comment_screenshot_path);

            var screenshot_base64 = Action.ActionHelpers.SaveBitmapToBase64Str((Bitmap)comment_screenshot);

            //update status, comment_screenshot to db

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("update semi_autocomment_task set [posted]='yes',screenshot_base64='" + screenshot_base64 + "',[status]='',[commented_date]=getdate(),[accountId]=" + ig_account.id + " where [id]=" + comment.id, conn))
                {

                    command.ExecuteNonQuery();
                }
            }

            //Add to log

            IGHelpers.DBHelpers.AddActionLog(IGDevice.ld_devicename, "published_semi_comment");
        }
    }
}
