﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    class SimulateUserStoriesTask : IGTask
    {

        string deviceID, deviceName;
        dynamic settings, ig_account;

        public SimulateUserStoriesTask(Class.IGDevice IGDevice, dynamic ig_account)
        {
            this.IGDevice = IGDevice;
            this.ig_account = ig_account;

            this.deviceID = this.IGDevice.deviceID;
            this.deviceName = this.IGDevice.ld_devicename;
        }
        public override void DoTask()
        {
            SimulateUserStories();
        }


        private void SimulateUserStories()
        {
            //get settings of this account

            GetSimulateStoriesSettings();

            //Check if settings is null -> yes -> return

            if (settings == null)
                return;
            //neu đã tới bước này tức là account này đã thỏa điều kiện để thực hiện action

            //generate log for this session

            var guid = Guid.NewGuid().GetHashCode().ToString();
            var actionlog_name = "SimulateUserStories_" + guid.Substring(guid.Length - 4);

            //set time for this session

            int session_time = RandomFromStr(settings.random.ToString().Split('|')[1]);

            Stopwatch watch = new Stopwatch();
            watch.Start();

            while (true)
            {
                try
                {
                    if (watch.Elapsed.TotalMinutes > session_time)
                        break;

                    //get profile to follow

                    var profile = IGHelpers.DBHelpers.GetTargetAccount_SpecificTasks("SimulateUserStories", ig_account.id);

                    if (profile == null)
                        break;

                    //Do simulateuserstories

                    IGThreads.ThreadHelpers.DoSimulateStories(IGDevice, ig_account, profile.username);


                    //Add action log
                    IGHelpers.DBHelpers.AddActionLog(IGDevice.ld_devicename, actionlog_name, "", profile.username);

                    //Update status to igtoolkit scraping user


                    using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
                    {
                        conn.Open();

                        using (var command = new SqlCommand("update scrapeusers set [CurrentStories]=0 where [pk] = " + profile.pk.ToString(), conn))
                        {

                            command.ExecuteNonQuery();
                        }
                    }

                    IGHelpers.DBHelpers.UpdateStatusIgToolKitUser(profile.pk, "status", System.Data.SqlDbType.VarChar, "");
                }
                catch
                { }

                ADBSupport.ADBHelpers.Delay();
            }

            //set next session time

            var next_session_time = SetNextSessionTime();

            Thread.Sleep(5000);
        }

        private void GetSimulateStoriesSettings()
        {
            //Get simulateuserstories_type from account_info
            var simulateuserstories_type = "";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [action] from actionlog where [accountId]=" + ig_account.id + " and [action] like 'simulateuserstories-%' and DATEDIFF(day,[do_at],getdate())=0", conn))
                {
                    var simulateuserstories_type_object = sqlCommand.ExecuteScalar();
                    if (simulateuserstories_type_object != null)
                        simulateuserstories_type = sqlCommand.ExecuteScalar().ToString();
                }
            }

            if (simulateuserstories_type == "")
            {
                simulateuserstories_type = SetSimulateStoriesTypeAutomatically(simulateuserstories_type);
                if (simulateuserstories_type == "")
                    return;

                //add followingtype to log


                IGHelpers.DBHelpers.AddActionLog(deviceName, simulateuserstories_type);

                //add target sessions_count to log
                settings = SettingsOfSimulateUserStoriesType(simulateuserstories_type);

                IGHelpers.DBHelpers.AddActionLog(deviceName, "simulateuserstories_sessions_count", settings.sessions_count.ToString());

            }

            //Get settings from engagetype

            settings = SettingsOfSimulateUserStoriesType(simulateuserstories_type);
        }

        class simulateusersetting
        {
            public string setting_name;
            public int count;

            public simulateusersetting(string setting_name, int count = 0)
            {
                this.setting_name = setting_name;
                this.count = count;
            }
        }

        private string SetSimulateStoriesTypeAutomatically(string simulateuserstories_type)
        {
            //get all current settings

            string engagetype = "";
            List<simulateusersetting> simulateuserstories_history = new List<simulateusersetting>();

            simulateuserstories_history.Add(new simulateusersetting("simulateuserstories-warmlite"));
            simulateuserstories_history.Add(new simulateusersetting("simulateuserstories-warmup"));
            simulateuserstories_history.Add(new simulateusersetting("simulateuserstories-default"));

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("ig_EngageSettings", conn))
                {
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = ig_account.id;
                    sqlCommand.Parameters.Add("@return_value", System.Data.SqlDbType.VarChar).Value = "engagetype";

                    var engage_value = sqlCommand.ExecuteScalar();

                    engagetype = engage_value == null ? "" : engage_value.ToString();
                }

                using (var sqlCommand = new SqlCommand("ig_EngageSettings", conn))
                {
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = ig_account.id;
                    sqlCommand.Parameters.Add("@return_value", System.Data.SqlDbType.VarChar).Value = "simulateuserstories-history";

                    using (var dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                for (int i = 0; i < simulateuserstories_history.Count; i++)
                                {
                                    if (dataReader["action"].ToString() == simulateuserstories_history[i].setting_name)
                                    {
                                        simulateuserstories_history[i].count = int.Parse(dataReader["times"].ToString());
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (engagetype != "default")
                return "";

            //following-warmlite

            if (simulateuserstories_history.FirstOrDefault(s => s.setting_name == "simulateuserstories-warmlite").count <= SettingsOfSimulateUserStoriesType("simulateuserstories-warmlite").days)
            {
                return "simulateuserstories-warmlite";
            }

            if (simulateuserstories_history.FirstOrDefault(s => s.setting_name == "simulateuserstories-warmup").count <= SettingsOfSimulateUserStoriesType("simulateuserstories-warmup").days)
            {
                return "simulateuserstories-warmup";
            }

            return "simulateuserstories-default";
        }

        private dynamic SettingsOfSimulateUserStoriesType(string simulateuserstories_type)
        {
            var settings_str = "";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [settings] from engagesettings where [type]='" + simulateuserstories_type + "'", conn))
                {
                    settings_str = sqlCommand.ExecuteScalar().ToString();
                }
            }

            List<string> settings = settings_str.Split(new string[] { "***" }, StringSplitOptions.None).ToList<string>();

            string random = settings.FirstOrDefault(s => s.Contains("random|"));
            string sleep_session = settings.FirstOrDefault(s => s.Contains("sleep_session|"));
            int days = int.Parse(settings.FirstOrDefault(s => s.Contains("days|")).Split('|')[1]);
            int sessions_count = int.Parse(settings.FirstOrDefault(s => s.Contains("sessions_count|")).Split('|')[1]);

            return new
            {
                random,
                sleep_session,
                days,
                sessions_count
            };
        }

        private DateTime SetNextSessionTime()
        {
            string sleep_session = settings.sleep_session.ToString();

            int sleep_seconds = RandomFromStr(sleep_session.Split('|')[1]);

            var next_session_time = DateTime.Now.AddSeconds(sleep_seconds);

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'simulateuserstories_next_session','" + next_session_time.ToString() + "')", conn))
                {
                    sqlCommand.ExecuteNonQuery();
                }
            }

            return next_session_time;
        }

        private int RandomFromStr(string random_str)
        {
            var from = int.Parse(random_str.Split('-')[0]);
            var to = int.Parse(random_str.Split('-')[1]);

            return (new Random(Guid.NewGuid().GetHashCode())).Next(from, to);
        }
    }
}
