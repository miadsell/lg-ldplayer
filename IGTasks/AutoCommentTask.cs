﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    /// <summary>
    /// slave do commenting to market shop
    /// </summary>
    class AutoCommentTask : IGTask
    {

        string deviceName;
        dynamic ig_account, settings;

        public AutoCommentTask(dynamic ig_account)
        {
            this.ig_account = ig_account;
            this.deviceName = ig_account.phone_device;
        }

        public AutoCommentTask(Class.IGDevice IGDevice, dynamic ig_account)
        {
            this.IGDevice = IGDevice;
            this.ig_account = ig_account;

            this.deviceName = this.IGDevice.ld_devicename;
        }

        public override void DoTask()
        {
            AutoCommentUser();
        }

        private void AutoCommentUser()
        {
            //get settings of this account

            GetAutoCommentUserSettings();

            //Check if settings is null -> yes -> return

            if (settings == null)
                return;

            //get target autocomment this date
            var target_autocomment_date = SetTargetAutoComment();

            //get current autocomment count in this date

            var currentdate_autocomment = CurrentAutoCommentThisDate();

            if (currentdate_autocomment >= target_autocomment_date)
                return;


            //set autocomment for this session
            var session_autocomment_count = SetAutoCommentCountThisSession();

            //insert log new_session (use to track finish a block_session
            IGHelpers.DBHelpers.AddActionLog(IGDevice.ld_devicename, "new_autocomment_session");

            //run

            //use sleep mô phỏng run action

            for (int i = 0; i < session_autocomment_count; i++)
            {
                //get post to autocomment

                var comment = IGHelpers.DBHelpers.GetAutoCommentTask(ig_account.id);

                if (comment == null)
                    break;

                //DO ACTION //DO ACTION //DO ACTION //DO ACTION
                IGThreads.ThreadHelpers.DoAutoComment(this.IGDevice, ig_account, comment);

                //if (status == "Action Blocked")
                //{
                //    break;
                //}

                //Add action log
                IGHelpers.DBHelpers.AddActionLog(IGDevice.ld_devicename, "autocomment_task", comment.link);



                int sleep_action = RandomFromStr(((string)settings.sleep_action).Split('|')[1]);

                Thread.Sleep(TimeSpan.FromSeconds(sleep_action));
            }

            //check if count session >= block_session_count

            if (IsFinishedCurrentBlockSession())
            {
                SetNextSessionTime(true);
            }
            else
            {
                //set next session time

                var next_session_time = SetNextSessionTime();
            }

            Thread.Sleep(5000);
        }


        #region --AutoComment User Task Helpers--

        private void GetAutoCommentUserSettings()
        {
            //Get engagetype from account_info
            var autocommenttype = "";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [action] from actionlog where [accountId]=" + ig_account.id + " and [action] like 'autocomment-%' and DATEDIFF(day,[do_at],getdate())=0", conn))
                {
                    var autocommenttype_object = sqlCommand.ExecuteScalar();
                    if (autocommenttype_object != null)
                        autocommenttype = sqlCommand.ExecuteScalar().ToString();
                }
            }

            if (autocommenttype == "")
            {
                autocommenttype = SetAutoCommentTypeAutomatically(autocommenttype);
                if (autocommenttype == "")
                    return;

                //add autocommenttype to log

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var sqlCommand = new SqlCommand("insert into [actionlog] ([accountid],[action]) values (" + ig_account.id + ",'" + autocommenttype + "')", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }

            //Get settings from engagetype

            settings = SettingsOfAutoCommentType(autocommenttype);
        }

        class autocommentsetting
        {
            public string setting_name;
            public int count;

            public autocommentsetting(string setting_name, int count = 0)
            {
                this.setting_name = setting_name;
                this.count = count;
            }
        }

        private string SetAutoCommentTypeAutomatically(string current_autocommenttype)
        {


            //get all current settings

            string engagetype = "";
            List<autocommentsetting> autocomment_history = new List<autocommentsetting>();

            autocomment_history.Add(new autocommentsetting("autocomment-warmlite"));
            autocomment_history.Add(new autocommentsetting("autocomment-warmup"));
            autocomment_history.Add(new autocommentsetting("autocomment-default"));

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("ig_EngageSettings", conn))
                {
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = ig_account.id;
                    sqlCommand.Parameters.Add("@return_value", System.Data.SqlDbType.VarChar).Value = "engagetype";

                    var engage_value = sqlCommand.ExecuteScalar();

                    engagetype = engage_value == null ? "" : engage_value.ToString();
                }

                using (var sqlCommand = new SqlCommand("ig_EngageSettings", conn))
                {
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = ig_account.id;
                    sqlCommand.Parameters.Add("@return_value", System.Data.SqlDbType.VarChar).Value = "autocomment-history";

                    using (var dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                for (int i = 0; i < autocomment_history.Count; i++)
                                {
                                    if (dataReader["action"].ToString() == autocomment_history[i].setting_name)
                                    {
                                        autocomment_history[i].count = int.Parse(dataReader["times"].ToString());
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (engagetype != "default")
                return "";

            //autocomment-warmlite

            if (autocomment_history.FirstOrDefault(s => s.setting_name == "autocomment-warmlite").count <= SettingsOfAutoCommentType("autocomment-warmlite").days)
            {
                return "autocomment-warmlite";
            }

            if (autocomment_history.FirstOrDefault(s => s.setting_name == "autocomment-warmup").count <= SettingsOfAutoCommentType("autocomment-warmup").days)
            {
                return "autocomment-warmup";
            }

            return "autocomment-default";
        }


        private dynamic SettingsOfAutoCommentType(string autocommenttype)
        {
            var settings_str = "";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [settings] from engagesettings where [type]='" + autocommenttype + "'", conn))
                {
                    settings_str = sqlCommand.ExecuteScalar().ToString();
                }
            }

            List<string> settings = settings_str.Split(new string[] { "***" }, StringSplitOptions.None).ToList<string>();

            string random = settings.FirstOrDefault(s => s.Contains("random|"));
            string increase = settings.FirstOrDefault(s => s.Contains("increase|"));
            string action_per_session = settings.FirstOrDefault(s => s.Contains("action_per_session|"));
            string sleep_session = settings.FirstOrDefault(s => s.Contains("sleep_session|"));
            string sleep_action = settings.FirstOrDefault(s => s.Contains("sleep_action|"));
            string max_autocomment = settings.FirstOrDefault(s => s.Contains("max_autocomment|"));
            string block_session = settings.FirstOrDefault(s => s.Contains("block_session|"));
            string sleep_per_block = settings.FirstOrDefault(s => s.Contains("sleep_per_block|"));
            int days = settings.FirstOrDefault(s => s.Contains("days|")) == null ? -1 : int.Parse(settings.FirstOrDefault(s => s.Contains("days|")).Split('|')[1]);

            return new
            {
                random,
                increase,
                action_per_session,
                sleep_session,
                sleep_action,
                days,
                max_autocomment,
                block_session,
                sleep_per_block
            };
        }


        private int SetTargetAutoComment()
        {
            int target_autocomment = 0;
            //get target autocomment from db
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [value] from actionlog where [accountId]=" + ig_account.id + " and [action] like 'target_autocomment' and DATEDIFF(day,[do_at],getdate())=0", conn))
                {
                    var target_autocomment_obj = sqlCommand.ExecuteScalar();
                    if (target_autocomment_obj != null)
                        target_autocomment = int.Parse(target_autocomment_obj.ToString());
                }
            }

            if (target_autocomment == 0)
            {
                int pre_target_autocomment = 0;

                // dynamic pre_target_autocomment_dyn = null;
                //get target autocomment of previous day
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var sqlCommand = new SqlCommand("select count(*) from actionlog where [action]='autocomment_task' and DATEDIFF(day,convert(datetime, (select top 1 [do_at] from actionlog where [action]='autocomment_task' and [accountid]=" + ig_account.id + " order by id desc),21),[do_at])=0 and [accountid]=" + ig_account.id, conn))
                    {

                        pre_target_autocomment = int.Parse(sqlCommand.ExecuteScalar().ToString());

                        //using (var dataReader = sqlCommand.ExecuteReader())
                        //{
                        //    if (dataReader.HasRows)
                        //    {
                        //        dataReader.Read();
                        //        var value = int.Parse(dataReader["value"].ToString());
                        //        var date = DateTime.Parse(dataReader["do_at"].ToString());

                        //        pre_target_autocomment_dyn = new
                        //        {
                        //            value,
                        //            date
                        //        };
                        //    }
                        //}
                    }
                }

                //if (pre_target_autocomment_dyn != null)
                //{
                //    pre_target_autocomment = pre_target_autocomment_dyn.value;
                //}

                var increase = 0;

                try
                {
                    if (settings.increase != null)
                    {
                        string increase_str = settings.increase.ToString();
                        increase = RandomFromStr(increase_str.Split('|')[1]);
                    }
                }
                catch { }

                if (pre_target_autocomment == 0 || increase == 0)
                {
                    //random value from settings.random

                    string random = settings.random.ToString();

                    target_autocomment = RandomFromStr(random.Split('|')[1]);
                }
                else
                {
                    //pre_target_autocomment + settings.random


                    target_autocomment = pre_target_autocomment + increase;
                }

                //get max_autocomment i has, if target_autocomment > max_autocomment => set target_autocomment in max_autocomment range

                try
                {
                    if (settings.max_autocomment != null)
                    {
                        string max_autocomment_str = settings.max_autocomment.ToString();

                        int max = int.Parse(max_autocomment_str.Split('|')[1].Split('-')[1]);

                        if (target_autocomment > max)
                            target_autocomment = RandomFromStr(max_autocomment_str.Split('|')[1]);
                    }
                }
                catch { }

                //check neu thoi diem thuc hien autocomment_task gan nhat > 3 ngay thi target_autocomment=target_autocomment/2


                target_autocomment = target_autocomment / (IsLongTimeNoDoingTask());

                //set and add log to actionlog

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'target_autocomment'," + target_autocomment.ToString() + ")", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }




            return target_autocomment;

        }

        private int CurrentAutoCommentThisDate()
        {
            int count = 0;
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select count(*) from actionlog where [accountId]=" + ig_account.id + " and [action] = 'autocomment_task' and DATEDIFF(day,[do_at],getdate())=0", conn))
                {
                    count = int.Parse(sqlCommand.ExecuteScalar().ToString());
                }
            }

            return count;
        }

        private int SetAutoCommentCountThisSession()
        {
            string action_per_session = settings.action_per_session.ToString();

            var count = RandomFromStr(action_per_session.Split('|')[1]);

            return count;
        }

        private DateTime SetNextSessionTime(bool finish_block_autocomment_session = false)
        {
            string sleep_session = settings.sleep_session.ToString();

            int sleep_seconds = RandomFromStr(sleep_session.Split('|')[1]);

            var next_session_time = DateTime.Now.AddSeconds(sleep_seconds);


            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                if (finish_block_autocomment_session)
                {
                    //tuc la da het 1 block, set sleep dài hơn theo sleep_per_block settings

                    string sleep_per_block = settings.sleep_per_block.ToString();

                    sleep_seconds = RandomFromStr(sleep_per_block.Split('|')[1]);

                    //random block_autocomment_session_count cho block ke tiep

                    var block_session = RandomFromStr(settings.block_session.ToString().Split('|')[1]);

                    //insert to log


                    using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'block_autocomment_session_count','" + block_session.ToString() + "')", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }

                }


                using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'autocomment_next_session','" + next_session_time.ToString() + "')", conn))
                {
                    sqlCommand.ExecuteNonQuery();
                }
            }

            return next_session_time;
        }

        private int IsLongTimeNoDoingTask()
        {
            string latest_dotask = null;
            //get latest do autocomment_tasking day

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [do_at] from actionlog where [accountid]=" + ig_account.id + " and [action]='target_autocomment' order by id desc", conn))
                {
                    using (var dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();
                            latest_dotask = dataReader["do_at"].ToString();
                        }
                    }
                }
            }

            if (latest_dotask == null) //chua thuc hien task bao gio
            {
                return 1;
            }

            //neu nhu khoang cach >10 thi chia 3

            if (DateTime.Now.Subtract(DateTime.Parse(latest_dotask)).TotalDays > 10)
                return 3;

            //neu khoang cach >3 thi chia 2
            if (DateTime.Now.Subtract(DateTime.Parse(latest_dotask)).TotalDays > 3)
                return 2;

            return 1;
        }

        #endregion

        #region --Block session--

        private bool IsFinishedCurrentBlockSession()
        {
            int current_new_autocomment_session = 0;
            dynamic block_autocomment_session = null;
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();


                //get latest block_session log

                using (var sqlCommand = new SqlCommand("select [id],[value] from actionlog where [accountid]=" + ig_account.id + " and [action]='block_autocomment_session_count' and DATEDIFF(day,do_at,getdate())=0 order by id desc", conn))
                {
                    using (var dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();

                            var id = dataReader["id"].ToString();
                            var value = dataReader["value"].ToString();

                            block_autocomment_session = new
                            {
                                id,
                                value
                            };
                        }
                    }
                }



                if (block_autocomment_session == null)
                {
                    //tuc la chua set block_autocomment_session_count
                    var block_session = RandomFromStr(settings.block_session.ToString().Split('|')[1]);

                    //insert to log

                    using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'block_autocomment_session_count','" + block_session.ToString() + "')", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }

                    return false;

                }

                //count new_autocomment_session with id > if of block_session log

                using (var sqlCommand = new SqlCommand("select count(*) from actionlog where [accountid]=" + ig_account.id + " and [action]='new_autocomment_session' and [id]>" + block_autocomment_session.id, conn))
                {
                    current_new_autocomment_session = int.Parse(sqlCommand.ExecuteScalar().ToString());
                }

            }

            if (current_new_autocomment_session >= int.Parse(block_autocomment_session.value))
                return true;

            return false;
        }

        #endregion

        private int RandomFromStr(string random_str)
        {
            var from = int.Parse(random_str.Split('-')[0]);
            var to = int.Parse(random_str.Split('-')[1]);

            return (new Random(Guid.NewGuid().GetHashCode())).Next(from, to);
        }

    }
}

