﻿using InstagramLDPlayer.Class;
using KAutoHelper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.IGTasks
{
    class IGEngageTask : IGTask
    {
        string deviceID, deviceName;
        int time_out = 0;
        string current_action; //for debug
        Stopwatch watch_engage = new Stopwatch();


        #region --Account State--

        bool is_actionblocked = false;
        bool is_swipesuggested = false;

        #endregion


        #region --Static action and percentage define


        private void SettingUpEngage_Per()
        {
            List<string> settings = IGHelpers.EngageTaskHelpers.GetEngageSettings(deviceName);
            Like_Per = settings.FirstOrDefault(s => s.StartsWith("Like|"));
            ViewPost_Per = settings.FirstOrDefault(s => s.StartsWith("ViewPost|"));
            MoreCaption_Per = settings.FirstOrDefault(s => s.StartsWith("MoreCaption|"));
            SwipeHorizontal_Per = settings.FirstOrDefault(s => s.StartsWith("SwipeHorizontal|"));
            SaveCollection_Per = settings.FirstOrDefault(s => s.StartsWith("SaveCollection|"));
            AddComment_Per = settings.FirstOrDefault(s => s.StartsWith("AddComment|"));
            ScrollComment_Per = settings.FirstOrDefault(s => s.StartsWith("ScrollComment|"));
            LikeComment_Per = settings.FirstOrDefault(s => s.StartsWith("LikeComment|"));
            ScrollUp_Per = settings.FirstOrDefault(s => s.StartsWith("up|"));
            ScrollDown_Per = settings.FirstOrDefault(s => s.StartsWith("down|"));
            FeedMain_Per = settings.FirstOrDefault(s => s.StartsWith("main|"));
            FeedSearch_Per = settings.FirstOrDefault(s => s.StartsWith("search|"));
        }

        static string Like_Per;// = "Like|1-10";
        static string ViewPost_Per;// = "ViewPost|10-60";

        bool need_SwipeViewStories;
        //static string SwipeViewStories_Per = "SwipeViewStories|50-60";
        static string MoreCaption_Per;// = "MoreCaption|60-80";
        static string SwipeHorizontal_Per;// = "SwipeHorizontal|70-80";
        static string SaveCollection_Per;// = "SaveCollection|80-85";
        static string AddComment_Per;// = "AddComment|85-100";
        static string ScrollComment_Per;// = "ScrollComment|1-70";
        static string LikeComment_Per;// = "LikeComment|1-10";
        static string ScrollUp_Per;// = "up|1-70";
        static string ScrollDown_Per;// = "down|70-100";

        static string FeedMain_Per;// = "main|1-90";
        static string FeedSearch_Per;// = "search|90-100";

        #endregion

        public override void DoTask()
        {
            //Sleep to check IsUnsualActivity

            ADBSupport.ADBHelpers.Delay(20, 30);
            time_out += 30000;

        DoTask:
            try
            {
                this.Stage1();
            }
            catch (Exception ex)
            {
                ADBSupport.LDHelpers.HandleException(deviceName, current_action + "|" + ex.Message, ex.StackTrace);
                if (ex.Message == "UnsualActivity")
                    return;

                goto DoTask;
            }

        }


        static private List<string> feedtype = new List<string>() { "main", "search" };


        public IGEngageTask(string deviceID)
        {
            this.deviceID = deviceID ?? throw new ArgumentNullException(nameof(deviceID));
        }

        dynamic ig_account;

        public IGEngageTask(IGDevice iGDevice, dynamic ig_account, double timeout)
        {
            this.IGDevice = iGDevice;
            this.ig_account = ig_account;
            this.deviceID = this.IGDevice.deviceID;
            this.deviceName = this.IGDevice.ld_devicename;
            this.time_out = (int)(timeout * 60 * 1000);

            SettingUpEngage_Per();

            this.is_actionblocked = IGHelpers.DBHelpers.IsAccountState(deviceName, "Action Blocked");
            this.is_swipesuggested = IGHelpers.DBHelpers.IsAccountState(deviceName, "Swipe Suggested For You");

            this.watch_engage.Start();

            //Decide to view stories or not in constructor

            List<bool> stories_choices = new List<bool>() { false, true, false };
            need_SwipeViewStories = stories_choices[(new Random()).Next(stories_choices.Count)];
        }

        /// <summary>
        /// Mind map: https://app.mindmup.com/map/_free/2020/04/5ce4aaf08af211eabd8c1f2623e115a2
        /// </summary>

        #region --Stage 1--
        //Scroll feed
        protected void Stage1()
        {
            IsUnsualActivity();

            int feed_min_scrolldown = IGHelpers.Helpers.min_scrolldown();
            int current_down = 0;
            bool can_useup = false;


            var feeds = new List<string>() { FeedMain_Per, FeedSearch_Per };
            var feed_choiced = IGHelpers.Helpers.RandomActionByPercentage(feeds);
        Stage1:

            List<string> scrolls = new List<string>() { ScrollUp_Per, ScrollDown_Per };

            var scroll_choiced = IGHelpers.Helpers.RandomActionByPercentage(scrolls);

            //Check if is on top (your story), if yes, set scroll to down

            if (IGHelpers.Helpers.IsOnTopHome(deviceID) || !can_useup)
                scroll_choiced = "down";

            //check timeout
            if (this.watch_engage.ElapsedMilliseconds > time_out)
                return;

            switch(scroll_choiced)
            {
                case "up":
                    current_down--;
                    break;
                case "down":
                    current_down++;
                    if (current_down > feed_min_scrolldown)
                        can_useup = true;
                    break;
            }

            var scroll = new Action.RandomScroll(scroll_choiced, deviceID);


            switch (feed_choiced)
            {
                case "main": //A
                    //Click home icon
                    GoHomeTab();

                    

                    //Scroll
                    scroll.DoAction();
                    IGHelpers.DBHelpers.AddActionLog(deviceName, "Scroll Main");

                    //Move to stage2

                    Stage2();

                    break;
                case "search": //B
                    //Click search icon
                    GoSearchTab();


                    //Check if need do search people

                    var search_people_choices = new List<string>() { "do|1-20", "not|20-100" };
                    var search_choice = IGHelpers.Helpers.RandomActionByPercentage(search_people_choices);

                    if (search_choice == "do")
                    {

                        var search_term = IGHelpers.DBHelpers.GetRandomSearchTerms(ig_account.id); //also check if continue do search terms

                        if (search_term != null)
                        {

                            var action = new Action.SearchPeople(IGDevice, search_term.term, search_term.type);
                            action.DoAction();

                            IGHelpers.DBHelpers.AddActionLog(deviceName, "Search People");

                            //ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "instagram://explore", "com.instagram.android");
                            //ADBSupport.ADBHelpers.Delay(2, 5);

                            ////Click search
                            //ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Global_WaitSearchTab);
                        }
                    }
                //ScrollSearch:
                //    //Scroll search feed
                //    scroll.DoAction();
                //    IGHelpers.DBHelpers.AddActionLog(deviceName, "Scroll Search");
                //    //Click random post

                //    try
                //    {
                //        ADBSupport.ADBHelpers.TapRecTanglePercentage(deviceID, new Class.RecPercentage(new { x = 1.4, y = 11.8 }, 95, 79.5), ADBSupport.BMPSource.ExploreTab, 1, 0.2);
                //    }
                //    catch
                //    {
                //        ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "instagram://explore", "com.instagram.android");
                //        ADBSupport.ADBHelpers.Delay(2, 5);
                //        goto ScrollSearch;
                //    }


                    
                //    try
                //    {
                //        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Post_DropDownMenu);
                //    }
                //    catch
                //    {
                //        //wait next dropdown
                //        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.NextDropDown);
                //    }

                //    //Continue scroll like main feed

                //    //Scroll
                //    scroll.DoAction();

                //    //Move to stage2
                //    Stage2();

                    feed_choiced = "main";

                    break;
            }

            goto Stage1;
        }
        /// <summary>
        /// Support Stage1
        /// </summary>
        private void GoHomeTab()
        {
            //Check if current on home

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_WaitHome, 2, 0, 1);
                return;
            }
            catch { }

            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "https://instagram.com", "com.instagram.android");

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_WaitHome, 5, 5, 5);

            //Click home to scroll to top

            //ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Global_WaitHome);
            //ADBSupport.ADBHelpers.Delay(1, 3);


            IGHelpers.DBHelpers.AddActionLog(deviceName, "Go Home Tab");

            //decide to view stories or not

            var isviewstories = ADBSupport.ADBHelpers.RandomIntValue(0, 2);

            if (isviewstories == 0)
            {
                SwipeViewStories();
            }


        }

        /// <summary>
        /// Support Stage1
        /// </summary>
        private void GoSearchTab()
        {
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_WaitSearchTab, 2, 0, 1);
                return;
            }
            catch { }

            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "instagram://explore", "com.instagram.android");

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_WaitSearchTab, 5, 5, 5);

            //Click to scroll to top

            //ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Global_WaitSearchTab);
            //ADBSupport.ADBHelpers.Delay(1, 3);

            IGHelpers.DBHelpers.AddActionLog(deviceName, "Go Search Tab");
        }

        #endregion


        #region --Stage2--

        //Watch media or continue scroll (return)
        protected void Stage2()
        {
            CheckAfterScroll();
            List<string> actions = new List<string>() { "doaction", "backstage", "doaction" };

            var choiced_action = actions[(new Random()).Next(actions.Count)];


            if (this.watch_engage.ElapsedMilliseconds > time_out)
                return;

            switch (choiced_action)
            {
                case "doaction":

                    //Scroll to view full media
                    try
                    {
                        ADBSupport.ADBHelpers.ScrollImageToPosByPercent(deviceID, ADBSupport.BMPSource.Post_DropDownMenu, 16);
                    }
                    catch
                    {
                        //k co dropdown_menu tren man hinh
                        return;
                    }

                    //Watch in random time

                    ADBSupport.ADBHelpers.Delay(3, 8);

                    //Move to stage3
                    Stage3();

                    break;
                case "backstage":
                    return;
            }

        }

        /// <summary>
        /// Support stage2
        /// </summary>
        private void CheckAfterScroll()
        {
            IsActionBlocked();
            IsNewPosts();
            IsSuggestedForYou();
            IsUnsualActivity();
            IsSponsoredPost();
            IsNeedReadNotification();
        }

        int check_newpost = 0;

        private void IsNewPosts()
        {
            if (check_newpost > 3)
                return;
            //Check if has new post button
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_LoadNewPosts, 1, 3, 0);

                //Click

                ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Global_LoadNewPosts);
                ADBSupport.ADBHelpers.Delay(2, 4);

                IGHelpers.DBHelpers.AddActionLog(deviceName, "Click New Post");
            }
            catch
            {
                check_newpost++;
            }
        }

        bool use_suggested = false;
        private void IsSuggestedForYou()
        {
            if (is_swipesuggested)
                return;
            if (use_suggested)
                return;
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.SuggestedForYou, 1, 0, 0);

                var action = new Action.SwipeSuggestedFollow(deviceID);
                action.DoAction();
                use_suggested = true;

                IGHelpers.DBHelpers.AddActionLog(deviceName, "Swipe Suggested For You");
            }
            catch
            {
            }
        }

        private void IsActionBlocked()
        {
            if (IGHelpers.Helpers.IsActionBlocked(deviceID, deviceName))
            {
                is_actionblocked = true;
            }
            //try
            //{
            //    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.ActionBlocked, 1, 0, 0);

            //    //Hit tell us
            //    ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.TellUs);

            //    IGHelpers.DBHelpers.AddActionLog(deviceName, "Action Blocked");

            //    is_actionblocked = true;
            //}
            //catch
            //{
            //}
        }

        private void IsUnsualActivity()
        {
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.ErrorScreen_UnsualActivity, 1, 0, 0, 0.8);

                IGThreads.ThreadHelpers.Do_UnsualActivityAction(deviceName);


                throw new System.ArgumentException("UnsualActivity");
            }
            catch (Exception ex)
            {
                if (ex.Message == "UnsualActivity")
                    throw;
            }
        }

        private void IsSponsoredPost()
        {

            var latest_sponsoredsimulate = IGHelpers.DBHelpers.GetLatestActionDate(deviceName, "SponsoredSimulate");

            if(latest_sponsoredsimulate!=null)
            {
                if (DateTime.Now.Subtract(DateTime.Parse(latest_sponsoredsimulate)).TotalDays < 3) //neu lan gan nhat thuc hien sponsored it hon 3 ngay thi return
                    return;
            }

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Sponsored_SponsoredText, 1, 0, 0);

                var action = new Action.SponsoredSimulate(deviceID);
                action.DoAction();
                use_suggested = true;

                IGHelpers.DBHelpers.AddActionLog(deviceName, "SponsoredSimulate");
            }
            catch
            {
            }
        }

        private void IsNeedReadNotification()
        {
            var action = new Action.ReadNotifcation(IGDevice);
            action.DoAction();
        }

        #endregion

        #region --Stage3--

        //Do random action or back to stage2
        protected void Stage3()
        {
            List<string> actions = new List<string>() { "doaction", "backstage" };

            var choiced_action = actions[(new Random()).Next(actions.Count)];


            if (this.watch_engage.ElapsedMilliseconds > time_out)
                return;

            switch (choiced_action)
            {
                case "doaction":

                    //Choose random action http://prntscr.com/s903ii

                    DoRandomPostAction();


                    break;
                case "backstage":
                    return;
            }
        }

        


        protected void DoRandomPostAction()
        {
            List<string> actions = new List<string>() { Like_Per, ViewPost_Per, MoreCaption_Per, SwipeHorizontal_Per, SaveCollection_Per, AddComment_Per};

            var choiced_action = IGHelpers.Helpers.RandomActionByPercentage(actions);

            if (choiced_action == null)
                return;

            current_action = choiced_action;


            //Do action

            //var method = this.GetType().GetMethod(choiced_action, BindingFlags.NonPublic | BindingFlags.Instance);

            //method.Invoke(this,null);

            FuncRedirect(choiced_action);
        }

        private void DoAfterAction(List<string> after_action)
        {
            if (this.watch_engage.ElapsedMilliseconds > time_out)
                return;
            //Decide to do continue or not

            var iscontinue = (new Random()).Next(2) == 0 ? true : false;

            if (!iscontinue)
                return;

            //Check after_action lists
            if (after_action.Count == 0)
                return;

            var choiced_action = IGHelpers.Helpers.RandomActionByPercentage(after_action);

            if (choiced_action == null)
                return;


            FuncRedirect(choiced_action);
        }


        private void FuncRedirect(string func_name)
        {
            switch(func_name)
            {
                case "Like":
                    Like();
                    break;
                case "ViewPost":
                    ViewPost();
                    break;
                case "ScrollComment":
                    ScrollComment();
                    break;
                case "LikeComment":
                    LikeComment();
                    break;
                case "MoreCaption":
                    MoreCaption();
                    break;
                case "SwipeHorizontal":
                    SwipeHorizontal();
                    break;
                case "SaveCollection":
                    SaveCollection();
                    break;
                case "AddComment":
                    //AddComment(); pause comment
                    break;
                case "SponsoredSimulate":
                    SponsoredSimulate();
                    break;
                case "SwipeViewStories":
                    SwipeViewStories();
                    break;
                default:
                    throw new System.ArgumentException("Not recognize this func");
            }
        }


        private void Like()
        {
            if (is_actionblocked)
                return;
            (new Action.LikePost(IGDevice)).DoAction();

            IGHelpers.DBHelpers.AddActionLog(deviceName, "Like");

            List<string> after_action = new List<string>() { ViewPost_Per, MoreCaption_Per, SwipeHorizontal_Per, SaveCollection_Per, AddComment_Per };

            DoAfterAction(after_action);

        }

        private void ViewPost()
        {
            //try swipe if has multiple image
            var swipe_choice = new List<bool> { true, false };
            bool isswipe = swipe_choice[(new Random()).Next(swipe_choice.Count)];

            if(isswipe)
            {
                SwipeHorizontal(false);
            }


            (new Action.ViewPost(IGDevice)).DoAction();

            IGHelpers.DBHelpers.AddActionLog(deviceName, "ViewPost");

            ADBSupport.ADBHelpers.Delay();

            List<string> after_action = new List<string>() { ScrollComment_Per, LikeComment_Per };

            DoAfterAction(after_action);

            KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_BACK);

        }

        /// <summary>
        /// Child of VIEWPOST
        /// </summary>
        private void ScrollComment() //child of ViewPost
        {
            List<string> vectors = new List<string>() { "up|70-100", "down|1-70" };

            var choiced_vector = IGHelpers.Helpers.RandomActionByPercentage(vectors);

            (new Action.RandomScroll(choiced_vector, IGDevice.deviceID)).DoAction();

            IGHelpers.DBHelpers.AddActionLog(deviceName, "ScrollComment");

            ADBSupport.ADBHelpers.Delay(3, 9);

            List<string> after_action = new List<string>() { LikeComment_Per };

            DoAfterAction(after_action);
        }

        /// <summary>
        /// Child of VIEWPOST
        /// </summary>
        private void LikeComment() //child of ViewPost
        {
            if (is_actionblocked)
                return;
            (new Action.LikeComment(IGDevice)).DoAction();

            IGHelpers.DBHelpers.AddActionLog(deviceName, "LikeComment");

            List<string> after_action = new List<string>() { ScrollComment_Per };

            DoAfterAction(after_action);

        }

        private void MoreCaption()
        {
            (new Action.MoreCaption(IGDevice)).DoAction();

            IGHelpers.DBHelpers.AddActionLog(deviceName, "MoreCaption");

            ADBSupport.ADBHelpers.Delay();

            List<string> after_action = new List<string>() { Like_Per, ViewPost_Per, SwipeHorizontal_Per, SaveCollection_Per, AddComment_Per };

            DoAfterAction(after_action);

        }

        private void SwipeHorizontal(bool do_afteraction = true)
        {
            (new Action.SwipeHorizontal(IGDevice)).DoAction();

            IGHelpers.DBHelpers.AddActionLog(deviceName, "SwipeHorizontal");

            if (do_afteraction)
            {
                List<string> after_action = new List<string>() { Like_Per, ViewPost_Per, MoreCaption_Per, SaveCollection_Per, AddComment_Per };

                DoAfterAction(after_action);
            }
            

        }

        private void SaveCollection()
        {
            if (is_actionblocked)
                return;
            (new Action.SaveCollection(IGDevice)).DoAction();

            IGHelpers.DBHelpers.AddActionLog(deviceName, "SaveCollection");

            ADBSupport.ADBHelpers.Delay(0,1);

            List<string> after_action = new List<string>() { Like_Per, ViewPost_Per, MoreCaption_Per, SwipeHorizontal_Per, AddComment_Per };

            DoAfterAction(after_action);

        }

        private void AddComment()
        {

            if (is_actionblocked)
                return;
            (new Action.AddComment(IGDevice)).DoAction();

            IGHelpers.DBHelpers.AddActionLog(deviceName, "AddComment");

            ADBSupport.ADBHelpers.Delay(1, 3);

            List<string> after_action = new List<string>() { ScrollComment_Per, LikeComment_Per };

            DoAfterAction(after_action);

        }

        private void SponsoredSimulate()
        {
            if (is_actionblocked)
                return;
            (new Action.SponsoredSimulate(IGDevice)).DoAction();

            IGHelpers.DBHelpers.AddActionLog(deviceName, "SponsoredSimulate");

            ADBSupport.ADBHelpers.Delay(1, 3);

            List<string> after_action = new List<string>() { Like_Per, ViewPost_Per, MoreCaption_Per, SwipeHorizontal_Per, SaveCollection_Per, AddComment_Per };

            DoAfterAction(after_action);
        }

        private void SwipeViewStories()
        {
            if(!need_SwipeViewStories)
            {
                return;
            }

            need_SwipeViewStories = false;

            if (is_actionblocked)
                return;
            (new Action.SwipeViewStories(IGDevice)).DoAction();

            IGHelpers.DBHelpers.AddActionLog(deviceName, "SwipeViewStories");

            ADBSupport.ADBHelpers.Delay(1, 3);
        }



        



        #endregion

    }
}
