﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    class SelfCommentTask : IGTask
    {
        string deviceID, deviceName;
        string comment_text;

        public SelfCommentTask(string deviceID, string deviceName)
        {
            this.deviceID = deviceID;
            this.deviceName = deviceName;
        }

        public SelfCommentTask(Class.IGDevice iGDevice, string comment_text)
        {
            this.IGDevice = iGDevice;
            this.deviceID = this.IGDevice.deviceID;
            this.deviceName = this.IGDevice.ld_devicename;
            this.comment_text = comment_text;
        }

        public override void DoTask()
        {
            SelfComment();
        }

        private void SelfComment()
        {
            //Click my profile


            ADBSupport.ADBHelpers.TapRecTanglePercentage(deviceID, new Class.RecPercentage(new { x = 88.9, y = 96.1 }, 2.5, 1), ADBSupport.BMPSource.SelfComment_EditProfile);

            //Click latest post

            //get point of posticon tab

            var screen = ADBSupport.ADBHelpers.ScreenShoot(deviceID);

            var posticon_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, ADBSupport.BMPSource.LikeUserPost_PostTabIcon);

            var latest_post_y = ADBSupport.ADBHelpers.ConvertPointToPercentage(deviceID, "Y", posticon_point.Value.Y) + 7;


            //Find rectangle of latest post
            var latest_post_rec = new Class.RecPercentage(new { x = 5, y = latest_post_y }, 25, 10);

            //Click post

            ADBSupport.ADBHelpers.TapRecTanglePercentage(deviceID, latest_post_rec);

            //Wait Posts title
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.LikeUserPost_WaitPosts, 4, 1, 2);

            //Click comment icon

            var commentbtn_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.SelfComment_CommentBtn);

            ADBSupport.ADBHelpers.RandomTapImageByPercentage(deviceID, commentbtn_point.x, commentbtn_point.y, "SelfComment_CommentBtn");

            //wait addcomment box

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.SelfComment_AddACommentBox, 5, 5, 3);

            //Paste comment to box

            //Copy and paste a caption

            //Write to file and save to deviceName data

            var guid = Guid.NewGuid().GetHashCode().ToString();
            //var content_path = ADBSupport.LDHelpers.LDPlayerDATA_path + "\\" + deviceName + "\\temp_img\\reply_" + guid.Substring(guid.Length - 6) + ".txt";

            //File.WriteAllText(content_path, reply);

            //ADBSupport.ADBHelpers.CopyTextFromEsEditor(deviceID, content_path);

            ////Reopen ig

            //ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell monkey -p com.instagram.android -c android.intent.category.LAUNCHER 1", 30);

            ////Paste
            //while (true)
            //{
            //    KAutoHelper.ADBHelper.LongPress(deviceID, 62, 464, 1000);

            //    try
            //    {
            //        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PasteBtn, 1, 0, 0);
            //        break;
            //    }
            //    catch
            //    { }
            //}

            ////Click paste

            //ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.PasteBtn); ADBSupport.ADBHelpers.Delay(5, 10);

            //Send unicode base64

            ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID, IGDevice.ld_devicename, comment_text);

            //Hit post

            var post_point = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.SelfComment_PostComment);

            ADBSupport.ADBHelpers.RandomTapImageByPercentage(deviceID, post_point.x, post_point.y, "SelfComment_PostComment");

            ADBSupport.ADBHelpers.Delay(5, 10);

        }

    }
}
