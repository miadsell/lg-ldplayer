﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    class CommentPostTask : IGTask
    {

        string deviceName;
        dynamic ig_account, settings;

        public CommentPostTask(Class.IGDevice IGDevice, dynamic ig_account)
        {
            this.IGDevice = IGDevice;
            this.ig_account = ig_account;

            this.deviceName = this.IGDevice.ld_devicename;
        }

        public override void DoTask()
        {
            CommentPostUser();
        }


        private void CommentPostUser()
        {
            //get settings of this account

            GetCommentPostUserSettings();

            //Check if settings is null -> yes -> return

            if (settings == null)
                return;

            //get target commentpost this date
            var target_commentpost_date = SetTargetCommentPost();

            //get current commentpost count in this date

            var currentdate_commentpost = CurrentCommentPostThisDate();

            if (currentdate_commentpost >= target_commentpost_date)
                return;


            //set commentpost for this session
            var session_commentpost_count = SetCommentPostCountThisSession();

            //insert log new_session (use to track finish a block_session
            IGHelpers.DBHelpers.AddActionLog(IGDevice.ld_devicename, "new_commentpost_session");

            //run

            //use sleep mô phỏng run action

            for (int i = 0; i < session_commentpost_count; i++)
            {
                //get post to commentpost

                var comment = AIOHelper.DBLogic.GetTargetAccount_SpecificTasks.CommentPost(ig_account);

                if (comment == null)
                    break;

                //DO ACTION //DO ACTION //DO ACTION //DO ACTION
                //IGThreads.ThreadHelpers.DoCommentPost(this.IGDevice, ig_account, comment);
                var status = AIOHelper.ThreadHelper.DoCommentPost.Run(this.IGDevice, ig_account, comment);

                if (status == "Action Blocked")
                {
                    break;
                }

                if (status == "")
                {
                    //Add action log
                    IGHelpers.DBHelpers.AddActionLog(IGDevice.ld_devicename, "commentpost_task", comment.actionlog.ToString(), comment.username.ToString());
                    status = "Done";
                }
                

                //set status=Done in scrapeuser_comment_posts  (truong hop limited comment thi van set done de account khac k lam lai data nay

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    string command_query =
                        @"update scrapeuser_comment_posts set [status]=@status where [id]=@id";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = comment.id;
                        command.Parameters.Add("@status", System.Data.SqlDbType.VarChar).Value = status;
                        command.ExecuteNonQuery();
                    }


                }

                int sleep_action = RandomFromStr(((string)settings.sleep_action).Split('|')[1]);

                Thread.Sleep(TimeSpan.FromSeconds(sleep_action));
            }

            //check if count session >= block_session_count

            if (IsFinishedCurrentBlockSession())
            {
                SetNextSessionTime(true);
            }
            else
            {
                //set next session time

                var next_session_time = SetNextSessionTime();
            }

            Thread.Sleep(5000);
        }


        #region --CommentPost User Task Helpers--

        private void GetCommentPostUserSettings()
        {
            //Get engagetype from account_info
            var commentposttype = "";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [action] from actionlog where [accountId]=" + ig_account.id + " and [action] like 'commentpost-%' and DATEDIFF(day,[do_at],getdate())=0", conn))
                {
                    var commentposttype_object = sqlCommand.ExecuteScalar();
                    if (commentposttype_object != null)
                        commentposttype = sqlCommand.ExecuteScalar().ToString();
                }
            }

            if (commentposttype == "")
            {
                commentposttype = SetCommentPostTypeAutomatically(commentposttype);
                if (commentposttype == "")
                    return;

                //add commentposttype to log

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var sqlCommand = new SqlCommand("insert into [actionlog] ([accountid],[action]) values (" + ig_account.id + ",'" + commentposttype + "')", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }

            //Get settings from engagetype

            settings = SettingsOfCommentPostType(commentposttype);
        }

        class commentpostsetting
        {
            public string setting_name;
            public int count;

            public commentpostsetting(string setting_name, int count = 0)
            {
                this.setting_name = setting_name;
                this.count = count;
            }
        }

        private string SetCommentPostTypeAutomatically(string current_commentposttype)
        {


            //get all current settings

            string engagetype = "";
            List<commentpostsetting> commentpost_history = new List<commentpostsetting>();

            commentpost_history.Add(new commentpostsetting("commentpost-warmlite"));
            commentpost_history.Add(new commentpostsetting("commentpost-warmup"));
            commentpost_history.Add(new commentpostsetting("commentpost-default"));

            //using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            //{
                //conn.Open();

                //using (var sqlCommand = new SqlCommand("ig_EngageSettings", conn))
                //{
                //    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                //    sqlCommand.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = ig_account.id;
                //    sqlCommand.Parameters.Add("@return_value", System.Data.SqlDbType.VarChar).Value = "engagetype";

                //    var engage_value = sqlCommand.ExecuteScalar();

                //    engagetype = engage_value == null ? "" : engage_value.ToString();
                //}


                engagetype = AIOHelper.DBLogic.EngageSettings.HistoryOfMassTask(ig_account.id, "engagetype")[0];

            //using (var sqlCommand = new SqlCommand("ig_EngageSettings", conn))
            //{
            //    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            //    sqlCommand.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = ig_account.id;
            //    sqlCommand.Parameters.Add("@return_value", System.Data.SqlDbType.VarChar).Value = "commentpost-history";

            //    using (var dataReader = sqlCommand.ExecuteReader())
            //    {
            //        if (dataReader.HasRows)
            //        {
            //            while (dataReader.Read())
            //            {
            //                for (int i = 0; i < commentpost_history.Count; i++)
            //                {
            //                    if (dataReader["action"].ToString() == commentpost_history[i].setting_name)
            //                    {
            //                        commentpost_history[i].count = int.Parse(dataReader["times"].ToString());
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            List<dynamic> histories = AIOHelper.DBLogic.EngageSettings.HistoryOfMassTask(ig_account.id, "commentpost-history");

            foreach (var h in histories)
            {
                for (int i = 0; i < commentpost_history.Count; i++)
                {
                    if (h.action == commentpost_history[i].setting_name)
                    {
                        commentpost_history[i].count = int.Parse(h.times.ToString());
                    }
                }
            }
            //}

            if (engagetype != "default")
                return "";

            //commentpost-warmlite

            if (commentpost_history.FirstOrDefault(s => s.setting_name == "commentpost-warmlite").count <= SettingsOfCommentPostType("commentpost-warmlite").days)
            {
                return "commentpost-warmlite";
            }

            if (commentpost_history.FirstOrDefault(s => s.setting_name == "commentpost-warmup").count <= SettingsOfCommentPostType("commentpost-warmup").days)
            {
                return "commentpost-warmup";
            }

            return "commentpost-default";
        }


        private dynamic SettingsOfCommentPostType(string commentposttype)
        {
            var settings_str = "";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [settings] from engagesettings where [type]='" + commentposttype + "'", conn))
                {
                    settings_str = sqlCommand.ExecuteScalar().ToString();
                }
            }

            List<string> settings = settings_str.Split(new string[] { "***" }, StringSplitOptions.None).ToList<string>();

            string random = settings.FirstOrDefault(s => s.Contains("random|"));
            string increase = settings.FirstOrDefault(s => s.Contains("increase|"));
            string action_per_session = settings.FirstOrDefault(s => s.Contains("action_per_session|"));
            string sleep_session = settings.FirstOrDefault(s => s.Contains("sleep_session|"));
            string sleep_action = settings.FirstOrDefault(s => s.Contains("sleep_action|"));
            string max_commentpost = settings.FirstOrDefault(s => s.Contains("max_commentpost|"));
            string block_session = settings.FirstOrDefault(s => s.Contains("block_session|"));
            string sleep_per_block = settings.FirstOrDefault(s => s.Contains("sleep_per_block|"));
            int days = settings.FirstOrDefault(s => s.Contains("days|")) == null ? -1 : int.Parse(settings.FirstOrDefault(s => s.Contains("days|")).Split('|')[1]);

            return new
            {
                random,
                increase,
                action_per_session,
                sleep_session,
                sleep_action,
                days,
                max_commentpost,
                block_session,
                sleep_per_block
            };
        }


        private int SetTargetCommentPost()
        {
            int target_commentpost = 0;
            //get target commentpost from db
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [value] from actionlog where [accountId]=" + ig_account.id + " and [action] like 'target_commentpost' and DATEDIFF(day,[do_at],getdate())=0", conn))
                {
                    var target_commentpost_obj = sqlCommand.ExecuteScalar();
                    if (target_commentpost_obj != null)
                        target_commentpost = int.Parse(target_commentpost_obj.ToString());
                }
            }

            if (target_commentpost == 0)
            {
                int pre_target_commentpost = 0;

                // dynamic pre_target_commentpost_dyn = null;
                //get target commentpost of previous day
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var sqlCommand = new SqlCommand("select count(*) from actionlog where [action]='commentpost_task' and DATEDIFF(day,convert(datetime, (select top 1 [do_at] from actionlog where [action]='commentpost_task' and [accountid]=" + ig_account.id + " order by id desc),21),[do_at])=0 and [accountid]=" + ig_account.id, conn))
                    {

                        pre_target_commentpost = int.Parse(sqlCommand.ExecuteScalar().ToString());

                        //using (var dataReader = sqlCommand.ExecuteReader())
                        //{
                        //    if (dataReader.HasRows)
                        //    {
                        //        dataReader.Read();
                        //        var value = int.Parse(dataReader["value"].ToString());
                        //        var date = DateTime.Parse(dataReader["do_at"].ToString());

                        //        pre_target_commentpost_dyn = new
                        //        {
                        //            value,
                        //            date
                        //        };
                        //    }
                        //}
                    }
                }

                //if (pre_target_commentpost_dyn != null)
                //{
                //    pre_target_commentpost = pre_target_commentpost_dyn.value;
                //}

                var increase = 0;

                try
                {
                    if (settings.increase != null)
                    {
                        string increase_str = settings.increase.ToString();
                        increase = RandomFromStr(increase_str.Split('|')[1]);
                    }
                }
                catch { }

                if (pre_target_commentpost == 0 || increase == 0)
                {
                    //random value from settings.random

                    string random = settings.random.ToString();

                    target_commentpost = RandomFromStr(random.Split('|')[1]);
                }
                else
                {
                    //pre_target_commentpost + settings.random


                    target_commentpost = pre_target_commentpost + increase;
                }

                //get max_commentpost i has, if target_commentpost > max_commentpost => set target_commentpost in max_commentpost range

                try
                {
                    if (settings.max_commentpost != null)
                    {
                        string max_commentpost_str = settings.max_commentpost.ToString();

                        int max = int.Parse(max_commentpost_str.Split('|')[1].Split('-')[1]);

                        if (target_commentpost > max)
                            target_commentpost = RandomFromStr(max_commentpost_str.Split('|')[1]);
                    }
                }
                catch { }

                //check neu thoi diem thuc hien commentpost_task gan nhat > 3 ngay thi target_commentpost=target_commentpost/2

                //neu target >3 thi check xem co longtimenodoing task khong, neu <3 thi k can check
                if(target_commentpost>3)
                    target_commentpost = target_commentpost / (IsLongTimeNoDoingTask());

                //set and add log to actionlog

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'target_commentpost'," + target_commentpost.ToString() + ")", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }




            return target_commentpost;

        }

        private int CurrentCommentPostThisDate()
        {
            int count = 0;
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select count(*) from actionlog where [accountId]=" + ig_account.id + " and [action] = 'commentpost_task' and DATEDIFF(day,[do_at],getdate())=0", conn))
                {
                    count = int.Parse(sqlCommand.ExecuteScalar().ToString());
                }
            }

            return count;
        }

        private int SetCommentPostCountThisSession()
        {
            string action_per_session = settings.action_per_session.ToString();

            var count = RandomFromStr(action_per_session.Split('|')[1]);

            return count;
        }

        private DateTime SetNextSessionTime(bool finish_block_commentpost_session = false)
        {
            string sleep_session = settings.sleep_session.ToString();

            int sleep_seconds = RandomFromStr(sleep_session.Split('|')[1]);

            var next_session_time = DateTime.Now.AddSeconds(sleep_seconds);


            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                if (finish_block_commentpost_session)
                {
                    //tuc la da het 1 block, set sleep dài hơn theo sleep_per_block settings

                    string sleep_per_block = settings.sleep_per_block.ToString();

                    sleep_seconds = RandomFromStr(sleep_per_block.Split('|')[1]);

                    //random block_commentpost_session_count cho block ke tiep

                    var block_session = RandomFromStr(settings.block_session.ToString().Split('|')[1]);

                    //insert to log


                    using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'block_commentpost_session_count','" + block_session.ToString() + "')", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }

                }


                using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'commentpost_next_session','" + next_session_time.ToString() + "')", conn))
                {
                    sqlCommand.ExecuteNonQuery();
                }
            }

            return next_session_time;
        }

        private int IsLongTimeNoDoingTask()
        {
            string latest_dotask = null;
            //get latest do commentpost_tasking day

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [do_at] from actionlog where [accountid]=" + ig_account.id + " and [action]='target_commentpost' order by id desc", conn))
                {
                    using (var dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();
                            latest_dotask = dataReader["do_at"].ToString();
                        }
                    }
                }
            }

            if (latest_dotask == null) //chua thuc hien task bao gio
            {
                return 1;
            }

            //neu nhu khoang cach >10 thi chia 3

            if (DateTime.Now.Subtract(DateTime.Parse(latest_dotask)).TotalDays > 10)
                return 3;

            //neu khoang cach >3 thi chia 2
            if (DateTime.Now.Subtract(DateTime.Parse(latest_dotask)).TotalDays > 3)
                return 2;

            return 1;
        }

        #endregion

        #region --Block session--

        private bool IsFinishedCurrentBlockSession()
        {
            int current_new_commentpost_session = 0;
            dynamic block_commentpost_session = null;
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();


                //get latest block_session log

                using (var sqlCommand = new SqlCommand("select [id],[value] from actionlog where [accountid]=" + ig_account.id + " and [action]='block_commentpost_session_count' and DATEDIFF(day,do_at,getdate())=0 order by id desc", conn))
                {
                    using (var dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();

                            var id = dataReader["id"].ToString();
                            var value = dataReader["value"].ToString();

                            block_commentpost_session = new
                            {
                                id,
                                value
                            };
                        }
                    }
                }



                if (block_commentpost_session == null)
                {
                    //tuc la chua set block_commentpost_session_count
                    var block_session = RandomFromStr(settings.block_session.ToString().Split('|')[1]);

                    //insert to log

                    using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'block_commentpost_session_count','" + block_session.ToString() + "')", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }

                    return false;

                }

                //count new_commentpost_session with id > if of block_session log

                using (var sqlCommand = new SqlCommand("select count(*) from actionlog where [accountid]=" + ig_account.id + " and [action]='new_commentpost_session' and [id]>" + block_commentpost_session.id, conn))
                {
                    current_new_commentpost_session = int.Parse(sqlCommand.ExecuteScalar().ToString());
                }

            }

            if (current_new_commentpost_session >= int.Parse(block_commentpost_session.value))
                return true;

            return false;
        }

        #endregion

        private int RandomFromStr(string random_str)
        {
            var from = int.Parse(random_str.Split('-')[0]);
            var to = int.Parse(random_str.Split('-')[1]);

            return (new Random(Guid.NewGuid().GetHashCode())).Next(from, to);
        }

    }
}
