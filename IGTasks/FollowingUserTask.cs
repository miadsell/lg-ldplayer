﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    class FollowingUserTask : IGTask
    {
        string deviceName;
        dynamic ig_account, settings;

        public FollowingUserTask(dynamic ig_account)
        {
            this.ig_account = ig_account;
            this.deviceName = ig_account.phone_device;
        }

        public FollowingUserTask(Class.IGDevice IGDevice, dynamic ig_account)
        {
            this.IGDevice = IGDevice;
            this.ig_account = ig_account;

            this.deviceName = this.IGDevice.ld_devicename;
        }

        public override void DoTask()
        {
            FollowingUser();
        }

        private void FollowingUser()
        {
            //get settings of this account

            GetFollowingUserSettings();

            //Check if settings is null -> yes -> return

            if (settings == null)
                return;

            //get target following this date
            var target_following_date = SetTargetFollowing();

            //get current following count in this date

            var currentdate_following = CurrentFollowingThisDate();

            if (currentdate_following >= target_following_date)
                return;


            //set following for this session
            var session_following_count = SetFollowingCountThisSession();

            //insert log new_session (use to track finish a block_session
            IGHelpers.DBHelpers.AddActionLog(IGDevice.ld_devicename, "new_following_session");

            //run

            //use sleep mô phỏng run action

            for (int i = 0; i < session_following_count; i++)
            {
                //get profile to follow

                //var profile = IGHelpers.DBHelpers.GetTargetAccount_SpecificTasks("FollowingUser");
                var profile = AIOHelper.DBLogic.GetTargetAccount_SpecificTasks.FollowingUser(ig_account);

                if (profile == null)
                    break;

                //DO ACTION //DO ACTION //DO ACTION //DO ACTION
                //var status = IGThreads.ThreadHelpers.DoFollowingUser(this.IGDevice, ig_account, profile.username);
                var status = AIOHelper.ThreadHelper.DoFollowingUser.Run(this.IGDevice, ig_account, profile.username);

                if (status == "Action Blocked")
                {
                    break;
                }

                //Add action log
                IGHelpers.DBHelpers.AddActionLog(IGDevice.ld_devicename, "following_task", ig_account.niche, profile.username);

                //Update status to igtoolkit scraping user

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                        {
                            using (var command = new SqlCommand("update scrapeusers set [following]='Done', [status]='' where [UserName]=@UserName", conn, sqlTransaction))
                            {
                                command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = profile.username.ToString();
                                command.ExecuteNonQuery();
                            }

                            sqlTransaction.Commit();
                        }
                    }/*, "scrapeusers"*/);
                }

                //IGHelpers.DBHelpers.UpdateStatusIgToolKitUser(profile.pk, "following", System.Data.SqlDbType.VarChar, "Done");

                //IGHelpers.DBHelpers.UpdateStatusIgToolKitUser(profile.pk, "status", System.Data.SqlDbType.VarChar, status);

                int sleep_action = RandomFromStr(((string)settings.sleep_action).Split('|')[1]);

                Thread.Sleep(TimeSpan.FromSeconds(sleep_action));
            }

            //check if count session >= block_session_count

            if (IsFinishedCurrentBlockSession())
            {
                SetNextSessionTime(true);
            }
            else
            {
                //set next session time

                var next_session_time = SetNextSessionTime();
            }

            Thread.Sleep(5000);
        }


        #region --Following User Task Helpers--

        private void GetFollowingUserSettings()
        {
            //Get engagetype from account_info
            var followingtype = "";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [action] from actionlog where [accountId]=" + ig_account.id + " and [action] like 'following-%' and DATEDIFF(day,[do_at],getdate())=0", conn))
                {
                    var followingtype_object = sqlCommand.ExecuteScalar();
                    if (followingtype_object != null)
                        followingtype = sqlCommand.ExecuteScalar().ToString();
                }
            }

            if (followingtype == "")
            {
                followingtype = SetFollowingTypeAutomatically(followingtype);
                if (followingtype == "")
                    return;

                //add followingtype to log

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var sqlCommand = new SqlCommand("insert into [actionlog] ([accountid],[action]) values (" + ig_account.id + ",'" + followingtype + "')", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }

            //Get settings from engagetype

            settings = SettingsOfFollowingType(followingtype);
        }

        class followsetting
        {
            public string setting_name;
            public int count;

            public followsetting(string setting_name, int count = 0)
            {
                this.setting_name = setting_name;
                this.count = count;
            }
        }

        private string SetFollowingTypeAutomatically(string current_followingtype)
        {


            //get all current settings

            string engagetype = "";
            List<followsetting> following_history = new List<followsetting>();

            following_history.Add(new followsetting("following-warmlite"));
            following_history.Add(new followsetting("following-warmup"));
            following_history.Add(new followsetting("following-default"));

            //using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            //{
            //    conn.Open();

            //    using (var sqlCommand = new SqlCommand("ig_EngageSettings", conn))
            //    {
            //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            //        sqlCommand.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = ig_account.id;
            //        sqlCommand.Parameters.Add("@return_value", System.Data.SqlDbType.VarChar).Value = "engagetype";

            //        var engage_value = sqlCommand.ExecuteScalar();

            //        engagetype = engage_value == null ? "" : engage_value.ToString();
            //    }

            //    using (var sqlCommand = new SqlCommand("ig_EngageSettings", conn))
            //    {
            //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            //        sqlCommand.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = ig_account.id;
            //        sqlCommand.Parameters.Add("@return_value", System.Data.SqlDbType.VarChar).Value = "following-history";

            //        using (var dataReader = sqlCommand.ExecuteReader())
            //        {
            //            if (dataReader.HasRows)
            //            {
            //                while (dataReader.Read())
            //                {
            //                    for (int i = 0; i < following_history.Count; i++)
            //                    {
            //                        if (dataReader["action"].ToString() == following_history[i].setting_name)
            //                        {
            //                            following_history[i].count = int.Parse(dataReader["times"].ToString());
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}

            engagetype = AIOHelper.DBLogic.EngageSettings.HistoryOfMassTask(ig_account.id, "engagetype")[0];

            List<dynamic> histories = AIOHelper.DBLogic.EngageSettings.HistoryOfMassTask(ig_account.id, "following-history");

            foreach (var h in histories)
            {
                for (int i = 0; i < following_history.Count; i++)
                {
                    if (h.action == following_history[i].setting_name)
                    {
                        following_history[i].count = int.Parse(h.times.ToString());
                    }
                }
            }

            if (engagetype != "default")
                return "";

            //following-warmlite

            if (following_history.FirstOrDefault(s => s.setting_name == "following-warmlite").count <= SettingsOfFollowingType("following-warmlite").days)
            {
                return "following-warmlite";
            }

            if (following_history.FirstOrDefault(s => s.setting_name == "following-warmup").count <= SettingsOfFollowingType("following-warmup").days)
            {
                return "following-warmup";
            }

            return "following-default";
        }


        private dynamic SettingsOfFollowingType(string followingtype)
        {
            var settings_str = "";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [settings] from engagesettings where [type]='" + followingtype + "'", conn))
                {
                    settings_str = sqlCommand.ExecuteScalar().ToString();
                }
            }

            List<string> settings = settings_str.Split(new string[] { "***" }, StringSplitOptions.None).ToList<string>();

            string random = settings.FirstOrDefault(s => s.Contains("random|"));
            string increase = settings.FirstOrDefault(s => s.Contains("increase|"));
            string action_per_session = settings.FirstOrDefault(s => s.Contains("action_per_session|"));
            string sleep_session = settings.FirstOrDefault(s => s.Contains("sleep_session|"));
            string sleep_action = settings.FirstOrDefault(s => s.Contains("sleep_action|"));
            string max_following = settings.FirstOrDefault(s => s.Contains("max_following|"));
            string block_session = settings.FirstOrDefault(s => s.Contains("block_session|"));
            string sleep_per_block = settings.FirstOrDefault(s => s.Contains("sleep_per_block|"));
            int days = settings.FirstOrDefault(s => s.Contains("days|")) == null ? -1 : int.Parse(settings.FirstOrDefault(s => s.Contains("days|")).Split('|')[1]);

            return new
            {
                random,
                increase,
                action_per_session,
                sleep_session,
                sleep_action,
                days,
                max_following,
                block_session,
                sleep_per_block
            };
        }


        private int SetTargetFollowing()
        {
            int target_following = 0;
            //get target following from db
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [value] from actionlog where [accountId]=" + ig_account.id + " and [action] like 'target_following' and DATEDIFF(day,[do_at],getdate())=0", conn))
                {
                    var target_following_obj = sqlCommand.ExecuteScalar();
                    if (target_following_obj != null)
                        target_following = int.Parse(target_following_obj.ToString());
                }
            }

            if (target_following == 0)
            {
                int pre_target_following = 0;

               // dynamic pre_target_following_dyn = null;
                //get target following of previous day
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var sqlCommand = new SqlCommand("select count(*) from actionlog where [action]='following_task' and DATEDIFF(day,convert(datetime, (select top 1 [do_at] from actionlog where [action]='following_task' and [accountid]="+ig_account.id+" order by id desc),21),[do_at])=0 and [accountid]=" + ig_account.id, conn))
                    {

                        pre_target_following = int.Parse(sqlCommand.ExecuteScalar().ToString());

                        //using (var dataReader = sqlCommand.ExecuteReader())
                        //{
                        //    if (dataReader.HasRows)
                        //    {
                        //        dataReader.Read();
                        //        var value = int.Parse(dataReader["value"].ToString());
                        //        var date = DateTime.Parse(dataReader["do_at"].ToString());

                        //        pre_target_following_dyn = new
                        //        {
                        //            value,
                        //            date
                        //        };
                        //    }
                        //}
                    }
                }

                //if (pre_target_following_dyn != null)
                //{
                //    pre_target_following = pre_target_following_dyn.value;
                //}

                var increase = 0;

                try
                {
                    if (settings.increase != null)
                    {
                        string increase_str = settings.increase.ToString();
                        increase = RandomFromStr(increase_str.Split('|')[1]);
                    }
                }
                catch { }

                if (pre_target_following == 0 || increase == 0)
                {
                    //random value from settings.random

                    string random = settings.random.ToString();

                    target_following = RandomFromStr(random.Split('|')[1]);
                }
                else
                {
                    //pre_target_following + settings.random


                    target_following = pre_target_following + increase;
                }

                //get max_following i has, if target_following > max_following => set target_following in max_following range

                try
                {
                    if (settings.max_following != null)
                    {
                        string max_following_str = settings.max_following.ToString();

                        int max = int.Parse(max_following_str.Split('|')[1].Split('-')[1]);

                        if (target_following > max)
                            target_following = RandomFromStr(max_following_str.Split('|')[1]);
                    }
                }
                catch { }

                //check neu thoi diem thuc hien following_task gan nhat > 3 ngay thi target_following=target_following/2


                target_following = target_following / (IsLongTimeNoDoingTask());

                //set and add log to actionlog

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'target_following'," + target_following.ToString() + ")", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }




            return target_following;

        }

        private int CurrentFollowingThisDate()
        {
            int count = 0;
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select count(*) from actionlog where [accountId]="+ig_account.id+" and [action] = 'following_task' and DATEDIFF(day,[do_at],getdate())=0", conn))
                {
                    count = int.Parse(sqlCommand.ExecuteScalar().ToString());
                }
            }

            return count;
        }

        private int SetFollowingCountThisSession()
        {
            string action_per_session = settings.action_per_session.ToString();

            var count = RandomFromStr(action_per_session.Split('|')[1]);

            return count;
        }

        private DateTime SetNextSessionTime(bool finish_block_following_session = false)
        {
            string sleep_session = settings.sleep_session.ToString();

            int sleep_seconds = RandomFromStr(sleep_session.Split('|')[1]);

            var next_session_time = DateTime.Now.AddSeconds(sleep_seconds);


            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                if (finish_block_following_session)
                {
                    //tuc la da het 1 block, set sleep dài hơn theo sleep_per_block settings

                    string sleep_per_block = settings.sleep_per_block.ToString();

                    sleep_seconds = RandomFromStr(sleep_per_block.Split('|')[1]);

                    //random block_following_session_count cho block ke tiep

                    var block_session = RandomFromStr(settings.block_session.ToString().Split('|')[1]);

                    //insert to log


                    using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'block_following_session_count','" + block_session.ToString() + "')", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }

                }


                using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'following_next_session','" + next_session_time.ToString() + "')", conn))
                {
                    sqlCommand.ExecuteNonQuery();
                }
            }

            return next_session_time;
        }

        private int IsLongTimeNoDoingTask()
        {
            string latest_dotask = null;
            //get latest do following_tasking day

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [do_at] from actionlog where [accountid]="+ig_account.id+" and [action]='target_following' order by id desc", conn))
                {
                    using (var dataReader=sqlCommand.ExecuteReader())
                    {
                        if(dataReader.HasRows)
                        {
                            dataReader.Read();
                            latest_dotask = dataReader["do_at"].ToString();
                        }
                    }
                }
            }

            if(latest_dotask==null) //chua thuc hien task bao gio
            {
                return 1;
            }

            //neu nhu khoang cach >10 thi chia 3

            if (DateTime.Now.Subtract(DateTime.Parse(latest_dotask)).TotalDays > 10)
                return 3;

            //neu khoang cach >3 thi chia 2
            if (DateTime.Now.Subtract(DateTime.Parse(latest_dotask)).TotalDays > 3)
                return 2;

            return 1;
        }

        #endregion

        #region --Block session--

        private bool IsFinishedCurrentBlockSession()
        {
            int current_new_following_session = 0;
            dynamic block_following_session = null;
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();


                //get latest block_session log

                using (var sqlCommand = new SqlCommand("select [id],[value] from actionlog where [accountid]=" + ig_account.id + " and [action]='block_following_session_count' and DATEDIFF(day,do_at,getdate())=0 order by id desc", conn))
                {
                    using(var dataReader=sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();

                            var id = dataReader["id"].ToString();
                            var value = dataReader["value"].ToString();

                            block_following_session = new
                            {
                                id,
                                value
                            };
                        }
                    }
                }



                if (block_following_session == null)
                {
                    //tuc la chua set block_following_session_count
                    var block_session = RandomFromStr(settings.block_session.ToString().Split('|')[1]);

                    //insert to log

                    using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'block_following_session_count','" + block_session.ToString() + "')", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }

                    return false;

                }

                //count new_following_session with id > if of block_session log

                using (var sqlCommand = new SqlCommand("select count(*) from actionlog where [accountid]=" + ig_account.id + " and [action]='new_following_session' and [id]>" + block_following_session.id, conn))
                {
                    current_new_following_session = int.Parse(sqlCommand.ExecuteScalar().ToString());
                }

            }

            if (current_new_following_session >= int.Parse(block_following_session.value))
                return true;

            return false;
        }

        #endregion

        private int RandomFromStr(string random_str)
        {
            var from = int.Parse(random_str.Split('-')[0]);
            var to = int.Parse(random_str.Split('-')[1]);

            return (new Random(Guid.NewGuid().GetHashCode())).Next(from, to);
        }

    }
}
