﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    static class RecurringTasksHelper
    {
        static public void ScanRecurringTask()
        {
            List<string> accountIds = new List<string>(); //list of id accounts has recurring task

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("ig_GetAccountNotGenerateRecurringTask", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            accountIds.Add(dataReader["accountId"].ToString());
                        }
                    }
                }
            }

            //Check every account and add task to queue

            foreach (var id in accountIds)
            {
                //get list tasks and settings
                List<dynamic> dyn_recurring = new List<dynamic>();

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var command = new SqlCommand("select [recurringtask],[settings] from AccountTasksRecurring where [accountId]=" + id, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                dyn_recurring.Add(new
                                {
                                    recurringtask = dataReader["recurringtask"].ToString(),
                                    settings = dataReader["settings"].ToString()
                                });
                            }
                        }
                    }
                }

                foreach(var task in dyn_recurring)
                {
                    switch(task.recurringtask)
                    {
                        case "publish":
                            AddPublish(id, task.settings);
                            break;
                    }
                }

                //Add log to mark this account is scanned recurring tasks

                IGHelpers.DBHelpers.AddActionLogByID(id, "generate_task_recurring");

            }
        }

        static private void AddPublish(string accountId, string settings_str)
        {
            List<string> settings = settings_str.Split(new string[] { "***" }, StringSplitOptions.None).ToList<string>();

            //meaning can post [posts] post per [per_x_days]

            var posts = int.Parse(settings.FirstOrDefault(s => s.Contains("posts|")).Split('|')[1]);

            var per_x_days = int.Parse(settings.FirstOrDefault(s => s.Contains("per_x_days|")).Split('|')[1]);

            //get published post count in latest per_x_days

            var published_post_count = 0;

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("select count(*) from ig_post_log where [accountId]=" + accountId + " and (DATEDIFF(day,[published_at],getdate())>0 and DATEDIFF(day,[published_at],getdate())<" + per_x_days.ToString() + ")", conn))
                {
                    published_post_count = int.Parse(command.ExecuteScalar().ToString());
                }
            }

            var can_post_count = posts - published_post_count; //so luong post co the publish

            if (can_post_count <= 0)
                return;

            //get active hours
            var active_hours = GetActiveHours(accountId);


            //get schedule times

            List<TimeSpan> schedule_times = RandomTimes(active_hours, can_post_count);


            //Add to queue
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                foreach (var time in schedule_times)
                {
                    var publish_time = DateTime.Now.Date.Add(time);
                    using (var command = new SqlCommand("insert into queuetasks (accountid,[action],[value],[schedule_time]) values (" + accountId + ",'publish','" + settings_str + "','" + publish_time + "')", conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }

        }

        static private dynamic GetActiveHours(string accountId)
        {
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("select * from account_info where [accountId]=" + accountId, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        dataReader.Read();
                        var from = dataReader["active_from"].ToString();
                        var to = dataReader["active_to"].ToString();

                        return new
                        {
                            from,
                            to
                        };
                    }
                }
            }
        }

        static private List<TimeSpan> RandomTimes(dynamic active_hours, int times)
        {
            int min_seconds_between_post = 60*60;

            List<TimeSpan> schedule_times = new List<TimeSpan>();
            //generate publish time in active hours range
            var start = TimeSpan.Parse(active_hours.from.ToString());

            var current_time = DateTime.Now.TimeOfDay;

            if (current_time >= start)
                start = current_time;

            var end = TimeSpan.Parse(active_hours.to.ToString());

            var seconds_diff = (int) (end.TotalSeconds - start.TotalSeconds);

            for (int t = 0; t < times; t++)
            {
                int retry = 0;
            GenerateTime:
                if (retry > 3)
                    break;
                var time = start + TimeSpan.FromSeconds((new Random(Guid.NewGuid().GetHashCode())).Next(seconds_diff));

                for(int s=0;s< schedule_times.Count;s++)
                {
                    if ((time.TotalSeconds - schedule_times[s].TotalSeconds) > (min_seconds_between_post * (-1)) && (time.TotalSeconds - schedule_times[s].TotalSeconds) < min_seconds_between_post)
                    {
                        retry++;
                        goto GenerateTime;
                    }
                }

                schedule_times.Add(time);
            }


            return schedule_times;
        }


        static private int RandomFromStr(string random_str)
        {
            var from = int.Parse(random_str.Split('-')[0]);
            var to = int.Parse(random_str.Split('-')[1]);

            return (new Random(Guid.NewGuid().GetHashCode())).Next(from, to);
        }
    }
}
