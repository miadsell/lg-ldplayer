﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    class UpdateProfileTask : IGTask
    {

        string image_path, biotext;

        public UpdateProfileTask(IGDevice iGDevice, string image_path, string biotext)
        {
            this.IGDevice = iGDevice;
            this.image_path = image_path;
            this.biotext = biotext;
        }

        public override void DoTask()
        {
            UpdateProfile();
        }



        private void UpdateProfile()
        {
            //get random image profile

            var modify_img = IGHelpers.ExifHelpers.OptimizeImage_FTPServer(IGDevice.ld_devicename, image_path);

            //Send to xammp
            //var destpath= ADBSupport.ADBHelpers.CreatePath(IGHelpers.Helpers.xampp_server_location + IGDevice.ld_devicename);

            //File.Copy(modify_img, destpath + "\\" + Path.GetFileName(modify_img));

            //var imageserver_link = IGHelpers.Helpers.xammp_url + IGDevice.ld_devicename + "/" + Path.GetFileName(modify_img);

            Action.UpdateProfileImage updateProfileImage = new Action.UpdateProfileImage(this.IGDevice, modify_img);
            updateProfileImage.DoAction();


            //Bio txt path


            Action.UpdateBio updateBio = new Action.UpdateBio(this.IGDevice, biotext, 0);
            updateBio.DoAction();


            //add action log

            IGHelpers.DBHelpers.AddActionLog(IGDevice.ld_devicename, "updateprofile");
        }
    }
}
