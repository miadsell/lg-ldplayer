﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    class PublicStoriesTask : IGTask
    {
        string image_path;

        public PublicStoriesTask(IGDevice IGDevice, string image_path)
        {
            this.IGDevice = IGDevice;
            this.image_path = image_path;
        }


        public override void DoTask()
        {
            PublishStories();
        }


        private void PublishStories()
        {
            //modify image

            var modify_img = IGHelpers.ExifHelpers.OptimizeImage_FTPServer(IGDevice.ld_devicename, image_path);

            //Send to xammp
            //var destpath = ADBSupport.ADBHelpers.CreatePath(IGHelpers.Helpers.xampp_server_location + IGDevice.ld_devicename);

            //File.Copy(modify_img, destpath + "\\" + Path.GetFileName(modify_img));

            //var imageserver_link = IGHelpers.Helpers.xammp_url + IGDevice.ld_devicename + "/" + Path.GetFileName(modify_img);


            var action = new Action.PublishStories(this.IGDevice, modify_img);
            action.DoAction();


            IGHelpers.DBHelpers.AddActionLog(IGDevice.ld_devicename, "published_stories");
        }
    }
}
