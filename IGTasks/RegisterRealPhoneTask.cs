﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    class RegisterRealPhoneTask : IGTask
    {
        string deviceID;
        dynamic account;
        string phone_service;

        public override void DoTask()
        {
            DoRegister();
        }

        #region --bitmap--

        static public Bitmap RegisterRealPhone_CreateNewAccount = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_CreateNewAccount;

        static public Bitmap RegisterRealPhone_PhoneScreen = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_PhoneScreen;
        static public Bitmap RegisterRealPhone_PhoneRegionUS = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_PhoneRegionUS;
        static public Bitmap RegisterRealPhone_SelectYourCountryPopup = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_SelectYourCountryPopup;
        static public Bitmap RegisterRealPhone_CountryTextbox = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_CountryTextbox;
        static public Bitmap RegisterRealPhone_VietNam84 = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_VietNam84;
        static public Bitmap RegisterRealPhone_PhoneBox = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_PhoneBox;
        static public Bitmap RegisterRealPhone_NextBtn = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_NextBtn;
        static public Bitmap RegisterRealPhone_ConfirmationCodeBox = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_ConfirmationCodeBox;

        static public Bitmap RegisterRealPhone_NameAndPasswordScreen = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_NameAndPasswordScreen;
        static public Bitmap RegisterRealPhone_FullNameBox = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_FullNameBox;
        static public Bitmap RegisterRealPhone_PasswordBox   = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_PasswordBox;
        static public Bitmap RegisterRealPhone_ContinueWithoutSync = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_ContinueWithoutSync;

        static public Bitmap RegisterRealPhone_AddYourBirthdayScreen = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_AddYourBirthdayScreen;
        static public Bitmap RegisterRealPhone_NumberKeyboardShow = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_NumberKeyboardShow;
        static public Bitmap RegisterRealPhone_TiepBtn = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_TiepBtn;

        static public Bitmap RegisterRealPhone_WelcomeToInstagramScreen = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_WelcomeToInstagramScreen;
        static public Bitmap RegisterRealPhone_FaceBookFriendsScreen = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_FaceBookFriendsScreen;
        static public Bitmap RegisterRealPhone_SkipBtn = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_SkipBtn;
        static public Bitmap RegisterRealPhone_SkipBlack = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_SkipBlack;
        static public Bitmap RegisterRealPhone_AddProfilePhotoScreen = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_AddProfilePhotoScreen;
        static public Bitmap RegisterRealPhone_PeopleSuggestionScreen = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_PeopleSuggestionScreen;
        static public Bitmap RegisterRealPhone_FollowBtn = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_FollowBtn;
        static public Bitmap RegisterRealPhone_Follow_NextBtn = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_Follow_NextBtn;


        static public Bitmap RegisterRealPhone_HomeIGIcon = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_HomeIGIcon;


        static public Bitmap RegisterRealPhone_AirplaneMode = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_AirplaneMode;
        static public Bitmap RegisterRealPhone_APMode_On = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_APMode_On;
        static public Bitmap RegisterRealPhone_APMode_Off = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_APMode_Off;


        //--case 2
        static public Bitmap RegisterRealPhone_WelcomeToIG_Home = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_WelcomeToIG_Home;
        static public Bitmap RegisterRealPhone_FindPeopleToFollowPopup = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_FindPeopleToFollowPopup;
        static public Bitmap RegisterRealPhone_ClosePopup = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_ClosePopup;


        static public Bitmap RegisterRealPhone_TagHomeIcon = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_TagHomeIcon;
        static public Bitmap RegisterRealPhone_EditProfileBtn = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_EditProfileBtn;
        static public Bitmap RegisterRealPhone_CopyBtn = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_CopyBtn;
        static public Bitmap RegisterRealPhone_UsernameTag = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_UsernameTag;
        static public Bitmap RegisterRealPhone_ChangeProfilePhotoText = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_ChangeProfilePhotoText;


        static public Bitmap RegisterRealPhone_NotificationShow = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_NotificationShow;
        static public Bitmap RegisterRealPhone_NotificationShow_SearchIcon = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_NotificationShow_SearchIcon;
        static public Bitmap RegisterRealPhone_CloseIncognitor = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_CloseIncognitor;
        static public Bitmap RegisterRealPhone_ClonedAppTab = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_ClonedAppTab;
        static public Bitmap RegisterRealPhone_IGLD = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_IGLD;
        static public Bitmap RegisterRealPhone_NewIdentity = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_NewIdentity;
        static public Bitmap RegisterRealPhone_NewIdentityPopup = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_NewIdentityPopup;
        static public Bitmap RegisterRealPhone_ClearCacheCheckbox = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_ClearCacheCheckbox;
        static public Bitmap RegisterRealPhone_ClearCacheChecked = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_ClearCacheChecked;
        static public Bitmap RegisterRealPhone_DeleteAppDataCheckbox = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_DeleteAppDataCheckbox;
        static public Bitmap RegisterRealPhone_DeleteAppDataChecked = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_DeleteAppDataChecked;
        static public Bitmap RegisterRealPhone_NewIdentityOk = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_NewIdentityOk;
        static public Bitmap RegisterRealPhone_NewIdentityGenerated = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_NewIdentityGenerated;

        static public Bitmap RegisterRealPhone_IGHome_Icon_IGLD = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_IGHome_Icon_IGLD;

        static public Bitmap RegisterRealPhone_ADBKeyboardOnNotification = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_ADBKeyboardOnNotification;

        static public Bitmap RegisterRealPhone_SearchBtn = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_SearchBtn;
        static public Bitmap RegisterRealPhone_FindPeopleBtn = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_FindPeopleBtn;
        static public Bitmap RegisterRealPhone_BackBtn = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_BackBtn;
        static public Bitmap RegisterRealPhone_ConfirmItsYouToLogin = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_ConfirmItsYouToLogin;
        static public Bitmap RegisterRealPhone_RestrictActivity = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_RestrictActivity;
        static public Bitmap RegisterRealPhone_RequestANewOneCode = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_RequestANewOneCode;

        //create username

        static public Bitmap RegisterRealPhone_CreateUserName_Title = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_CreateUserName_Title;
        static public Bitmap RegisterRealPhone_UserNameTextbox = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_UserNameTextbox;
        static public Bitmap RegisterRealPhone_SuggestedUsername = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_SuggestedUsername;
        static public Bitmap RegisterRealPhone_UserNameOK = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_UsernameOK;
        static public Bitmap RegisterRealPhone_SuggestedNameItem = BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_SuggestedNameItem;

        #endregion

        public RegisterRealPhoneTask(string deviceID, dynamic account, string phone_service)
        {
            this.deviceID = deviceID;
            this.account = account;
            this.phone_service = phone_service;
        }

        public dynamic DoRegister()
        {
            SettingEnvironment();


            //Wait CreateNewAccountBtn
            for (int i = 0; i < 5; i++)
            {

                KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_HOME);

                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_IGHome_Icon_IGLD, 5, 1, 1);


                //Open app

                ADBSupport.ADBHelpers.ClickByImage(deviceID, RegisterRealPhone_IGHome_Icon_IGLD);


                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_CreateNewAccount, 5, 1, 2);
                    break;
                }
                catch
                {
                    ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell am force-stop com.instagram.androie");
                    Thread.Sleep(2000);
                }

                if (i == 4)
                    throw new System.ArgumentException("Can't open IG App");
            }

            ADBSupport.ADBHelpers.ClickandWaitRandom(deviceID, RegisterRealPhone_CreateNewAccount, RegisterRealPhone_PhoneScreen, 1);


            //Phone screen

            //Click US+1

            ADBSupport.ADBHelpers.ClickandWaitRandom(deviceID, RegisterRealPhone_PhoneRegionUS, RegisterRealPhone_SelectYourCountryPopup);

            //Click CountryTextbox

            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_CountryTextbox); ADBSupport.ADBHelpers.Delay(0.5, 1);


            //Type 84

            ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID, null, "84", true);

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_VietNam84, 5, 1, 2);

            ADBSupport.ADBHelpers.ClickandWaitRandom(deviceID, RegisterRealPhone_VietNam84,RegisterRealPhone_PhoneBox);


            //Send phone number

            //var phone = IGHelpers.PhoneHelpers.RequestPhone_RentCode();

            dynamic phone = null;

            switch (phone_service)
            {
                case "rentcode.co":
                    phone = PLAutoHelper.PhoneHelper.RequestPhone_RentCode(PLAutoHelper.PhoneHelper.SERVICE_INSTAGRAM);
                    break;
                case "nanosim.vn":
                    phone = PLAutoHelper.PhoneHelper.NANOSIM_RequestPhone("instagram");
                    break;
                case "otpsim.com":
                    phone = PLAutoHelper.PhoneHelper.OTPSim_RequestPhone("instagram");
                    break;
                case "chothuesimcode.com":
                    phone = PLAutoHelper.PhoneHelper.chothuesimcode_RequestPhone("instagram");
                    break;
                default:
                    throw new System.ArgumentException("Not recognize this phone service");
            }


            ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID, null, PLAutoHelper.NormalFuncHelper.GetDynamicProperty(phone, "phonenum"), true);

            //Click next

            ADBSupport.ADBHelpers.ClickandWaitRandom(deviceID, RegisterRealPhone_NextBtn,RegisterRealPhone_ConfirmationCodeBox);


            //Send code

            string code = null;

            int retry = 0;

            WaitingCode:

            switch (phone_service)
            {
                case "rentcode.co":
                    code = PLAutoHelper.PhoneHelper.GetSMS_RentCode(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(phone, "orderid"), 1, PLAutoHelper.PhoneHelper.SERVICE_INSTAGRAM);
                    break;
                case "nanosim.vn":
                    code = PLAutoHelper.PhoneHelper.NANOSIM_ReceiveSms(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(phone, "orderid"), 1, PLAutoHelper.PhoneHelper.SERVICE_INSTAGRAM);
                    break;
                case "otpsim.com":
                    code = PLAutoHelper.PhoneHelper.OTPSim_ReceiveSMS(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(phone, "orderid"), 1, PLAutoHelper.PhoneHelper.SERVICE_INSTAGRAM);
                    break;
                case "chothuesimcode.com":
                    code = PLAutoHelper.PhoneHelper.chothuesimcode_ReceiveSMS(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(phone, "orderid"), 1, PLAutoHelper.PhoneHelper.SERVICE_INSTAGRAM);
                    break;
                default:
                    throw new System.ArgumentException("Not recognize this phone service");
            }

            if (code == null)
            {
                ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_RequestANewOneCode);

                retry++;

                if (retry > 1)
                    return null;

                goto WaitingCode;
            }

            //if (code == null)
            //    return null;

            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_ConfirmationCodeBox);

            ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID,null, code,true);

            ADBSupport.ADBHelpers.ClickandWaitRandom(deviceID, RegisterRealPhone_NextBtn, RegisterRealPhone_NameAndPasswordScreen, 1);


            //Send name and password

            var fullname = account.fullname;

            var password = "123456789kdL";

            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_FullNameBox);

            ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID,null, fullname,true); ADBSupport.ADBHelpers.Delay(1, 2);

            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_PasswordBox);

            ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID,null, password,true); ADBSupport.ADBHelpers.Delay(1, 2);

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_ContinueWithoutSync, 5, 1, 1);

            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_ContinueWithoutSync);

            ADBSupport.ADBHelpers.Delay(1, 2);

            //Birthday

            //AddYourBirthday Tag and demo in pixel, must convert to percentage

            //Year
            //demoTag: 133,169 --demoA: 423,1097 --demoC: 487,1124
            //=>Percentage: demoTag: 18.5, 11.4 --demoA: 58.8,74.1   --demoC: 67.6,75.9

            while (true)
            {
                try
                {
                    ADBSupport.ADBHelpers.LongPressRelative(deviceID, RegisterRealPhone_AddYourBirthdayScreen, new { x = 18.5, y = 11.4 }, new { x = 58.8, y = 74.1 }, new { x = 67.6, y = 75.9 }, 1000);

                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_ADBKeyboardOnNotification, 3, 1, 1);
                    break;
                }
                catch
                {
                    //check if go to facebook friends step
                    var screenshoot_check = ADBSupport.ADBHelpers.ScreenShoot(deviceID);

                    if (KAutoHelper.ImageScanOpenCV.FindOutPoint(screenshoot_check, RegisterRealPhone_FaceBookFriendsScreen) != null)
                    {
                        goto DoAfterRegCase;
                    }

                    if (KAutoHelper.ImageScanOpenCV.FindOutPoint(screenshoot_check, RegisterRealPhone_WelcomeToInstagramScreen) != null)
                    {
                        goto SubmitRegister;
                    }
                }
                Thread.Sleep(2000);
            }

            var year = ADBSupport.ADBHelpers.RandomIntValue(1992, 2002);

            ADBSupport.ADBHelpers.InputTextWithDelay(deviceID, year.ToString());

            KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_BACK);

            //Month
            //demoTag: 133,169 --demoA: 181,1101 --demoC: 199,1122
            //=>Percentage: --demoA: 23.5,74.7 --demoC: 29.7,75.9

            while (true)
            {
                try
                {
                    ADBSupport.ADBHelpers.LongPressRelative(deviceID, RegisterRealPhone_AddYourBirthdayScreen, new { x = 18.5, y = 11.4 }, new { x = 25.1, y = 74.4 }, new { x = 27.6, y = 75.8 }, 1000);
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_ADBKeyboardOnNotification);
                    break;
                }
                catch
                {

                }
                Thread.Sleep(2000);
            }

            for (int d = 0; d < 4; d++)
            {
                KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_DEL);
                Thread.Sleep(500);
            }

            List<string> month_letter = new List<string>() { "J", "F", "M", "A", "J", "A", "O", "N", "D" };

            var month = month_letter[(new Random(Guid.NewGuid().GetHashCode())).Next(month_letter.Count)];

            ADBSupport.ADBHelpers.InputTextWithDelay(deviceID, month);

            KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_BACK);

            //Day
            //demoTag: 133,169 --demoA: 307,1100 --demoC: 340,1121
            //=>Percentage: --demoA: 42.6,74.3 --demoC: 47.2,75.7

            while (true)
            {
                try
                {
                    ADBSupport.ADBHelpers.LongPressRelative(deviceID, RegisterRealPhone_AddYourBirthdayScreen, new { x = 18.5, y = 11.4 }, new { x = 42.6, y = 74.3 }, new { x = 47.2, y = 75.7 }, 1000);

                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_ADBKeyboardOnNotification, 3, 1, 1);
                    break;
                }
                catch
                {

                }
                Thread.Sleep(2000);
            }


            var day = ADBSupport.ADBHelpers.RandomIntValue(1, 25);

            ADBSupport.ADBHelpers.InputTextWithDelay(deviceID, day.ToString());

            KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_BACK);

            //Hit next

            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_NextBtn);

            ADBSupport.ADBHelpers.Delay(2, 4);


            //Next again
            SubmitRegister:
            //check if account.user!=""

            if (account.user != "" || Program.form.register_auto_generate_user)
            {
                string fill_user = account.user;

                if (fill_user == "")
                {
                    //auto generate user
                    fill_user = fullname.ToLower();
                    Regex rgx = new Regex("[^a-zA-Z0-9]");
                    fill_user = rgx.Replace(fill_user, "");
                }

                //change username
                ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_ChangeUserName);

                //wait tick
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_GreenTick, 5, 1, 1);

                //demoTag: 83.8,29.9 --demoA: 13.1,30.1   --demoC: 22.5,30.8

                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_GreenTick_Tag, 5, 2, 1);

                for (int clear = 0; clear < 3; clear++)
                {
                    while (true)
                    {
                        try
                        {
                            ADBSupport.ADBHelpers.LongPressRelative(deviceID, BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_GreenTick_Tag, new { x = 83.8, y = 29.9 }, new { x = 13.1, y = 30.1 }, new { x = 22.5, y = 30.8 }, 1000);
                            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_CopyBtn);
                            break;
                        }
                        catch
                        {

                        }
                        Thread.Sleep(2000);
                    }

                    //hit delete
                    KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_DEL);

                    //verify empty box
                    if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_EmptyBox, 5, 1, 1))
                        break;
                }

                //fill new username
                ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID, null, fill_user, true);

                ValidateUsername:
                if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_GreenTick_Tag, 5, 2, 1))
                {

                    //hit next
                    ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_NextBtn);

                    //check if ask CreateNewAccount
                    if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_CreateNewAccount_Option, 5, 2, 1))
                    {
                        ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_CreateNewAccount_Option);
                    }
                }
                else//username is exists
                {
                    //hit autousername
                    ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, BMResource.RegisterRealPhone_720_1480.RegisterRealPhone_720_1480_AutoUsername);
                    goto ValidateUsername;
                }

            }
            else
            {
                ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_NextBtn);
            }

            //Check if create username

            if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, RegisterRealPhone_CreateUserName_Title, 2, 5, 5))
            {
                //click username box
                ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_UserNameTextbox);

                string username_generate = ((string)PLAutoHelper.DetectPersonality.DetectNameFromIG_Fullname(fullname)).ToLower().Replace(" ", "");

                string random_number = (new Random(Guid.NewGuid().GetHashCode())).Next(100, 999).ToString();

                ADBSupport.ADBHelpers.SendUnicodeTextByADBKeyboard(deviceID, null, username_generate + random_number, true);

                while (!ADBSupport.ADBHelpers.IsImageExisted(deviceID, RegisterRealPhone_UserNameOK, 1, 5))
                {
                    //click suggested username
                    if (ADBSupport.ADBHelpers.IsImageExisted(deviceID, RegisterRealPhone_SuggestedUsername, 1, 1))
                    {
                        var screenshoot_username_screen = ADBSupport.ADBHelpers.ScreenShoot(deviceID);
                        var suggested_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screenshoot_username_screen, RegisterRealPhone_SuggestedNameItem);

                        //click
                        KAutoHelper.ADBHelper.Tap(deviceID, suggested_point.Value.X + (new Random(Guid.NewGuid().GetHashCode())).Next(40, 45), suggested_point.Value.Y + (new Random(Guid.NewGuid().GetHashCode())).Next(38, 42));

                        Thread.Sleep(1000);
                    }
                    else
                    {
                        throw new System.ArgumentException("No suggested username");
                    }
                }

                ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_NextBtn);
            }

            //Check what case
            ADBSupport.ADBHelpers.Delay(10, 15);
            DoAfterRegCase:
            var screenshoot = ADBSupport.ADBHelpers.ScreenShoot(deviceID);

            if(KAutoHelper.ImageScanOpenCV.FindOutPoint(screenshoot,RegisterRealPhone_FaceBookFriendsScreen)!=null)
            {
                AfterRegisteringCase1();
                goto EndCase;
            }

            if(KAutoHelper.ImageScanOpenCV.FindOutPoint(screenshoot,RegisterRealPhone_WelcomeToIG_Home)!=null)
            {
                AfterRegisteringCase2();
                goto EndCase;
            }

            if (KAutoHelper.ImageScanOpenCV.FindOutPoint(screenshoot, RegisterRealPhone_ConfirmItsYouToLogin) != null)
            {
                return new { statuscode = "AskCaptcha" };
            }

            if (KAutoHelper.ImageScanOpenCV.FindOutPoint(screenshoot, RegisterRealPhone_RestrictActivity) != null)
            {
                return new { statuscode = "AskCaptcha" };
            }

        EndCase:

            //Wait home

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_HomeIGIcon);

            //GET USERNAME

            //Click profile

            //demoTag: 34,1104 --demoA: 625,1117 --demoC: 652,1150
            //=>Percentage: 4.7,74.6     --demoA: 86.8,75.5       --demoC: 90.6,77.7

            ADBSupport.ADBHelpers.ClickRelative(deviceID, RegisterRealPhone_TagHomeIcon, new { x = 4.7, y = 74.6 }, new { x = 86.8, y = 75.5 }, new { x = 90.6, y = 77.7 });

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_EditProfileBtn, 5, 1, 1);

            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_EditProfileBtn);

            //demoTag: 26.1,26  --demoA: 5.4,45.3     --demoC: 15.3, 46.1

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_ChangeProfilePhotoText, 5, 2, 1);

            ADBSupport.ADBHelpers.ClickRelative(deviceID, RegisterRealPhone_ChangeProfilePhotoText, new { x = 26.1, y = 26 }, new { x = 5.4, y = 45.3 }, new { x = 15.3, y = 46.1 });
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_UsernameTag);

            //demoTag: 3.4,4.8 --demoA: 3.8, 14.1   --demoC: 21.3,14.9

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_UsernameTag, 5, 2, 1);

            while (true)
            {
                try
                {
                    ADBSupport.ADBHelpers.LongPressRelative(deviceID, RegisterRealPhone_UsernameTag, new { x = 3.4, y = 4.8 }, new { x = 3.8, y = 14.1 }, new { x = 21.3, y = 14.9 }, 1000);
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_CopyBtn);
                    break;
                }
                catch
                {

                }
                Thread.Sleep(2000);
            }

            //Click copy
            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_CopyBtn);
            
            var username = ADBSupport.ADBHelpers.ContentFromClipBoard(deviceID);

            return new
            {
                id = account.id,
                username,
                phone = PLAutoHelper.NormalFuncHelper.GetDynamicProperty(phone, "phonenum").ToString(),
                statuscode = "Success"
            };
        }

        private void AfterRegisteringCase1()
        {
            //Wait find facebook friends

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_FaceBookFriendsScreen, 5, 1, 1);

            //Hit skip

            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_SkipBtn);

            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_SkipBlack);

            //Wait add profile photo
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_AddProfilePhotoScreen, 5, 1, 1);

            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_SkipBtn);

            //Wait people suggestion

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_PeopleSuggestionScreen, 5, 5, 2); ;
            }
            catch
            {
                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_HomeIGIcon, 2, 1, 1);
                    AfterRegisteringCase2();
                    return;
                }
                catch 
                {
                    throw new System.ArgumentException("Not handle this case yet");
                }
            }

            //Follow suggestion

            FollowSuggestion();

            //Scroll top to show nextfollow_btn

            ADBSupport.ADBHelpers.ScrollUpToImage(deviceID, 65, RegisterRealPhone_Follow_NextBtn, 15);

            //Click next

            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_Follow_NextBtn);

        }

        /// <summary>
        /// Show home
        /// </summary>
        private void AfterRegisteringCase2()
        {
            //Click find people to follow in home
            ADBSupport.ADBHelpers.ClickandWaitRandom(deviceID, RegisterRealPhone_SearchBtn, RegisterRealPhone_FindPeopleBtn, 1);


            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_FindPeopleBtn);

            //Click close
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_FindPeopleToFollowPopup, 5, 3, 1);
            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_ClosePopup);

            FollowSuggestion();

            //Click back
            ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, RegisterRealPhone_BackBtn);

            KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_BACK);
        }


        private void FollowSuggestion()
        {
            //Random follow 3 people

            var current_following = 0;

            while (true)
            {
                List<bool> follow_or_not = new List<bool>() { true, false };

                var dofollow = follow_or_not[(new Random()).Next(follow_or_not.Count)];

                if (!dofollow)
                    goto ScrollSuggestion;

                var screen = ADBSupport.ADBHelpers.ScreenShoot();

                var follow_btns = KAutoHelper.ImageScanOpenCV.FindOutPoints(screen, RegisterRealPhone_FollowBtn);

                var choiced = (new Random(Guid.NewGuid().GetHashCode())).Next(follow_btns.Count);

                var point_x = follow_btns[choiced].X + (new Random(Guid.NewGuid().GetHashCode())).Next(RegisterRealPhone_FollowBtn.Width);
                var point_y = follow_btns[choiced].Y + (new Random(Guid.NewGuid().GetHashCode())).Next(RegisterRealPhone_FollowBtn.Height);

                //tap image
                KAutoHelper.ADBHelper.Tap(deviceID, point_x, point_y);

                current_following++;

                if (current_following >= 3)
                    break;

                ScrollSuggestion:
                //random scroll
                (new Action.RandomScroll("down", deviceID, ADBSupport.ADBHelpers.RandomIntValue(100, 500))).DoAction();

            }
        }

        #region --Setting Environment To CreateNewAccount--

        private void SettingEnvironment()
        {
            //CloseIncognitorApp
            //Expand notification


            ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell service call statusbar 1");

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_NotificationShow_SearchIcon);

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_CloseIncognitor, 3, 1, 1);
                ADBSupport.ADBHelpers.ClickByImage(deviceID, RegisterRealPhone_CloseIncognitor);
            }
            catch
            {

                KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_BACK);
            }
            NewIdentityApp:
            ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell am force-stop com.applisto.appcloner");

            //New identity
            ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell monkey -p com.applisto.appcloner -c android.intent.category.LAUNCHER 1");

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_ClonedAppTab, 5, 1, 1);

            int retry = 0;

            OpenClones:
            ADBSupport.ADBHelpers.ClickByImage(deviceID, RegisterRealPhone_ClonedAppTab);

            //wait instagram ld show
            if(!ADBSupport.ADBHelpers.IsImageExisted(deviceID, RegisterRealPhone_IGLD,5,1,1))
            {
                if(ADBSupport.ADBHelpers.IsImageExisted(deviceID,RegisterRealPhone_ClonedAppTab))
                {
                    retry++;
                    if (retry > 5)
                        throw new System.ArgumentException("Can't open clones tab");
                    goto OpenClones;
                }
            }
            //ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_IGLD,5,1,1);

            //demoTag: 3,23.8 --demoA: 91.8, 27.3 --demoC: 92.1, 28.7
            ADBSupport.ADBHelpers.ClickRelative(deviceID, RegisterRealPhone_IGLD, new { x = 3, y = 23.8 }, new { x = 91.8, y = 27.3 }, new { x = 92.1, y = 28.7 });

            //Wait NewIdentity

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_NewIdentity, 5, 1, 1);

            ADBSupport.ADBHelpers.ClickandWait(deviceID, RegisterRealPhone_NewIdentity, RegisterRealPhone_NewIdentityPopup, 1);

            ADBSupport.ADBHelpers.ClickandWait(deviceID, RegisterRealPhone_ClearCacheCheckbox, RegisterRealPhone_ClearCacheChecked, 1);

            ADBSupport.ADBHelpers.ClickandWait(deviceID, RegisterRealPhone_DeleteAppDataCheckbox, RegisterRealPhone_DeleteAppDataChecked, 1);

            try
            {
                ADBSupport.ADBHelpers.ClickandWait(deviceID, RegisterRealPhone_NewIdentityOk, RegisterRealPhone_NewIdentityGenerated, 1);
            }
            catch
            {
                goto NewIdentityApp;
            }

            //Turn on airplane mode
            TurnOn_AP();

            Thread.Sleep(60000);

            //Turn off airplane modee
            TurnOff_AP();



        }

        private void CloseIncognitorApp()
        {

        }


        private void NewIdentityApp()
        {

        }

        #endregion

        #region --Airplane Mode--

        private void TurnOn_AP()
        {
            //Check if AP Mode is on

            ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell am start -a android.settings.AIRPLANE_MODE_SETTINGS");
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_AirplaneMode, 5, 1, 1);

            while (true)
            {
                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_APMode_On, 3, 1, 1);
                    break;
                }
                catch
                {
                    //click button to turn on
                    ADBSupport.ADBHelpers.ClickByImage(deviceID, RegisterRealPhone_APMode_Off);
                }
            }
        }

        private void TurnOff_AP()
        {

            ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell am start -a android.settings.AIRPLANE_MODE_SETTINGS");
            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_AirplaneMode, 5, 1, 1);

            while (true)
            {
                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, RegisterRealPhone_APMode_Off, 3, 1, 1);
                    break;
                }
                catch
                {
                    //click button to turn on
                    ADBSupport.ADBHelpers.ClickByImage(deviceID, RegisterRealPhone_APMode_On);
                }
            }
        }

        #endregion


        #region --Username Suggestion--

        public void UsernameSuggestion()
        {
            throw new NotImplementedException();
            RestClient restClient = new RestClient("https://www.instagram.com/");


            restClient.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;

            restClient.UserAgent = " Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0";


            restClient.Proxy = new WebProxy("127.0.0.1", 8888);

            CookieContainer _cookieJar = new CookieContainer();
            restClient.CookieContainer = _cookieJar;

            RestRequest request = new RestRequest("", Method.GET);

            request.AddHeader("Host", "www.instagram.com");
            request.AddHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            request.AddHeader("Accept-Language", "en-US,en;q=0.5");
            request.AddHeader("Accept-Encoding", "gzip, deflate, br");
            request.AddHeader("Connection", "keep-alive");
            //request.AddHeader("Cookie", "ig_did=EAD357AD-D5EB-487E-B874-8EE6761D98E4");
            request.AddHeader("Upgrade-Insecure-Requests", "1");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
        }


        #endregion
    }
}