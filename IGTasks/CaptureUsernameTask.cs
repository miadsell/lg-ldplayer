﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    class CaptureUsernameTask : IGTask
    {
        string deviceID;
        dynamic ig_account;

        public CaptureUsernameTask(string deviceID, dynamic ig_account)
        {
            this.deviceID = deviceID;
            this.ig_account = ig_account;
        }

        public override void DoTask()
        {
            CaptureUsername();
        }

        public Bitmap ReturnTask()
        {
            var bmp = CaptureUsername();
            return bmp;
        }

        private Bitmap CaptureUsername()
        {
            //open profile
            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "instagram://profile", "com.instagram.android");

            Stopwatch watch_editprofile = new Stopwatch();
            watch_editprofile.Start();
            while (true)
            {
                if (watch_editprofile.Elapsed.TotalSeconds > 30)
                    throw new System.ArgumentException("Can't wait screen");
                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_Goto_EditProfileScreen, 5, 5, 5);
                    break;
                }
                catch
                {
                    KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_BACK);
                }
            }

            //click switch profile and wait TagChooseProfile
            //demoTag: 47.6, 5.8 --demoA: 31.7,6.6 --demoC: 43.8,7.5
            ADBSupport.ADBHelpers.ClickRelative(deviceID, ADBSupport.BMPSource.CaptureUsername_TagSwitchAccount, new { x = 47.6, y = 5.8 }, new { x = 31.7, y = 6.6 }, new { x = 43.8, y = 7.5 });

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.CaptureUsername_TagChooseProfile, 2, 2, 1);

            //demoTag: 89.6,81.6 --demoA: 23.1, 81.9 --demoC: 70.7, 84.9

            var username_bmp = (Bitmap)ADBSupport.ADBHelpers.CropScreenshotRelative(deviceID, ADBSupport.BMPSource.CaptureUsername_TagChooseProfile, new { x = 89.6, y = 81.6 }, new { x = 23.7, y = 82.6 }, new { x = 86, y = 84.8 });

            var height = username_bmp.Height;

            var temp_username_bmp = new Bitmap((Image)username_bmp);

            //var points = KAutoHelper.ImageScanOpenCV.FindColor(temp_username_bmp, Color.FromArgb(73, 73, 73));

            //var width = points[points.Count - 1].X;

            var width = (int) Math.Round(((string)ig_account.user).Length * 5.65, 0);

            var trim_username_bmp = KAutoHelper.CaptureHelper.CropImage(username_bmp, new Rectangle(0, 0, width, height));

            return trim_username_bmp;
        }
    }
}
