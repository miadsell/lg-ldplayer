﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    class IsCorrectAccountTask : IGTask
    {
        string deviceID;
        string username;

        public IsCorrectAccountTask(string deviceID, string username)
        {
            this.deviceID = deviceID;
            this.username = username;
        }

        public override void DoTask()
        {
            throw new NotImplementedException();
        }

        public bool ReturnTask()
        {
            return IsCorrectAccount();
        }

        private bool IsCorrectAccount()
        {


            Stopwatch watch_editprofile = new Stopwatch();
            watch_editprofile.Start();
            while (true)
            {

                //open profile
                ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "instagram://profile", "com.instagram.android");

                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_Goto_EditProfileScreen, 5, 5, 1);
                    break;
                }
                catch
                {
                    KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_BACK);
                }


                if (watch_editprofile.Elapsed.TotalSeconds > 60)
                    throw new System.ArgumentException("Can't wait screen");
            }

            //Hit editprofile

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.Global_Goto_EditProfileScreen, ADBSupport.BMPSource.SwitchAccount, 1);

            //CLICK username
            ADBSupport.ADBHelpers.ClickRelative(deviceID, ADBSupport.BMPSource.IsAccountCorrect_TagUsername, new { x = 5.2, y = 48.1 }, new { x = 5.5, y = 52.3 }, new { x = 13.5, y = 53.5 });
            bool isonusername_screen = false;
            //try check if navigate to username
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.IsAccountCorrect_TagUsernameScreen, 2, 3, 1);
                isonusername_screen = true;
            }
            catch
            {

            }

            string username_on_device = null;

            try
            {
                if (isonusername_screen)
                {
                    while (true)
                    {
                        //demoTag: 3.5,5.8 --demoA: 7.3,15.9 --demoC: 14.2,16.8
                        ADBSupport.ADBHelpers.LongPressRelative(deviceID, ADBSupport.BMPSource.IsAccountCorrect_TagUsernameScreen, new { x = 3.5, y = 5.8 }, new { x = 7.3, y = 15.9 }, new { x = 14.2, y = 16.8 }, 1000);
                        try
                        {
                            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.IsAccountCorrect_CopyBtn, 2, 2, 2);
                            break;
                        }
                        catch { }
                        Thread.Sleep(500);
                    }
                }
                else
                {
                    while (true)
                    {
                        //demoTag: 5.2,48.1 --demoA: 5.5,52.3 --demoC: 13.5,53.5
                        ADBSupport.ADBHelpers.LongPressRelative(deviceID, ADBSupport.BMPSource.IsAccountCorrect_TagUsername, new { x = 5.2, y = 48.1 }, new { x = 5.5, y = 52.3 }, new { x = 13.5, y = 53.5 }, 1000);
                        try
                        {
                            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.IsAccountCorrect_CopyBtn, 2, 2, 2);
                            break;
                        }
                        catch { }
                        Thread.Sleep(500);
                    }
                }

                //hit select all
                ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.IsAccountCorrect_SelectAllBtn);

                //hit copy

                lock (ADBSupport.LDHelpers.lock_copy)
                {
                    ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.IsAccountCorrect_CopyBtn);

                    //get username on device
                    username_on_device = ADBSupport.ADBHelpers.ContentFromClipBoard(deviceID);
                    if (username_on_device == null || username_on_device == "")
                    {
                        throw new System.ArgumentException("Can't get username from clipboard");
                    }
                }
            }
            catch
            {

            }
            finally
            {
            }


            if (username_on_device == username)
                return true;
            return false;
        }
    }
}
