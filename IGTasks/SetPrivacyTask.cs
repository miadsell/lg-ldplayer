﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    class SetPrivacyTask : IGTask
    {
        bool seton;

        public SetPrivacyTask(IGDevice IGDevice, bool seton)
        {
            this.IGDevice = IGDevice;
            this.seton = seton;
        }

        public override void DoTask()
        {
            SetPrivacy();
        }

        private void SetPrivacy()
        {
            string deviceID = IGDevice.deviceID;

            //open instagram
            ADBSupport.ADBHelpers.GoToIntentView(IGDevice.deviceID, "android.intent.action.VIEW", "https://instagram.com", "com.instagram.android");


            //Click profile

            KAutoHelper.ADBHelper.TapByPercent(deviceID, 89.3, 96.9);

            //Wait setting btn

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.SettingBtn, 5, 3, 3);

            //Click setting

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.SettingBtn, ADBSupport.BMPSource.SettingsText, 1);

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.SettingsText, ADBSupport.BMPSource.PrivacyMenu, 1);

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.PrivacyMenu, ADBSupport.BMPSource.AccountPrivacy, 1);

            //wait setting

            //Click Account Privacy

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.AccountPrivacy, ADBSupport.BMPSource.WaitPrivateAccount, 1);

            //seton

            if (seton)
            {
                if (isPrivateOn(deviceID))
                    return;

                //Click setting button
                KAutoHelper.ADBHelper.TapByPercent(deviceID, 89.3, 16.3);

                //Wait popup ok

                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.OkPopup, 3, 3, 2);

                //Click ok

                ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.OkPopup);


            }
            else
            {
                if (isPrivateOff(deviceID))
                {
                    return;
                }

                //Click setting button
                KAutoHelper.ADBHelper.TapByPercent(deviceID, 89.3, 16.3);

                //Wait popup ok

                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.OkPopup, 3, 3, 2);

                //Click ok

                ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.OkPopup);
            }


            ADBSupport.ADBHelpers.Delay(5, 10);
        }

        private bool isPrivateOn(string deviceID)
        {

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PrivateOn, 1, 5, 0);
                //if already on -> return

                return true;
            }
            catch
            {

            }

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PrivateOn_1, 1, 5, 0);
                //if already on -> return

                return true;
            }
            catch
            {

            }

            return false;
        }

        private bool isPrivateOff(string deviceID)
        {
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PrivateOff, 1, 5, 0);
                //if already on -> return

                return true;
            }
            catch
            {

            }

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.PrivateOff_1, 1, 5, 0);
                //if already on -> return

                return true;
            }
            catch
            {

            }

            return false;
        }
    }
}
