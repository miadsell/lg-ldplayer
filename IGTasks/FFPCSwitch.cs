﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    static class FFPCSwitch
    {

        //static public string originalprofile = @"C:\Users\luan\AppData\Roaming\Mozilla\Firefox\Profiles\IG-Original";
        static public string ffprofile_locate = "FFProfiles";
        //static public string uivision_path = "file:///" + AppDomain.CurrentDomain.BaseDirectory.Replace("\\", "/") + "ui.visionma.html?";
        //static public string uivision_folder = @"C:\Users\ad\Desktop\uivision\";
        //static public string download_path = @"C:\Users\ad\Downloads\";
        static public string ftp_backup_path = "/firefoxpc_bakup/";
        static public string ftp_original_firefox = "/IG-Original-Portable.zip";
        static public string originalprofile_local = "IG-Original-Portable";

        static public void SettingFFPCSwitch_Constants(dynamic devices)
        {
            //originalprofile = devices.firefox_originalprofile;
            //ffprofile_locate = devices.firefox_ffprofile_locate;
            //ftp_backup_path = devices.firefox_backup_path;
        }

        static public string Launch(dynamic account, string proxy, bool already_hasprofile)
        {
            bool openinstance = false;

            IntPtr hwnd = new IntPtr();

            string profilename = "igprofile_" + account.id;

            if (!already_hasprofile)
            {
                //check if original profile portable exists
                CheckIfOriginalProfilePortableExists();
                if (!Directory.Exists(Path.GetFullPath(ffprofile_locate) + "\\" + profilename))
                    PLAutoHelper.RPAFirefox.CreateFFPortable(Path.GetFullPath(ffprofile_locate) + "\\" + profilename, Path.GetFullPath(originalprofile_local), false);

                ////Copy all data from original (copy all settings,.... to new profile)

                //PLAutoHelper.RPAFirefox.CopySettingsFromOriginal(profilename, originalprofile, ffprofile_locate);


                //Generate browser unique information for first time

                var pid = PLAutoHelper.RPAFirefox.LaunchFFPortable(Path.GetFullPath(ffprofile_locate) + "\\" + profilename);

                hwnd = Process.GetProcessById(pid).MainWindowHandle;

                openinstance = true;

                //Settings up browser

                PLAutoHelper.RPAFirefox.SettingBrowser(hwnd, null, null);

            }
            else
            {
                //check if profile is in local
                if(!Directory.Exists(Path.GetFullPath(ffprofile_locate) + "\\" + profilename))
                {
                    //check if original profile portable exists
                    CheckIfOriginalProfilePortableExists();

                    //create profile

                    PLAutoHelper.RPAFirefox.CreateFFPortable(Path.GetFullPath(ffprofile_locate) + "\\" + profilename, Path.GetFullPath(originalprofile_local));

                    var ftp_backups = IGHelpers.Helpers.FTP_GetFileListing(ftp_backup_path, profilename + "*.zip").ToList().OrderByDescending(f => f.Modified).ToList();

                    if (ftp_backups.Count==0)
                    {
                        throw new System.ArgumentException("NotFoundBackup");
                    }

                    var temp_zip_path = "Temp\\" + profilename + ".zip";

                    //copy profile from backup path
                    //File.Copy(backups[0].FullName, temp_zip_path, true);
                    IGHelpers.Helpers.FTP_DownloadFile(ftp_backups[0].FullName, temp_zip_path);

                    PLAutoHelper.RPAFirefox.RestoreFFPortable(Path.GetFullPath(ffprofile_locate) + "\\" + profilename, temp_zip_path);
                }
            }

            if (!openinstance)
            {
                var pid = PLAutoHelper.RPAFirefox.LaunchFFPortable(Path.GetFullPath(ffprofile_locate) + "\\" + profilename);

                hwnd = Process.GetProcessById(pid).MainWindowHandle;

                openinstance = true;
            }

            PLAutoHelper.RPAFirefox.MaximizeBrowser(hwnd);

            //Setproxy

            PLAutoHelper.RPAFirefox.SetProxy(proxy, hwnd);

            return "Success";

        }

        static public void CheckIfOriginalProfilePortableExists()
        {
            if (!Directory.Exists(originalprofile_local) || !Directory.Exists(Path.GetFullPath(originalprofile_local)+ @"\Data\profile"))
            {
                //download file from ftp
                IGHelpers.Helpers.FTP_DownloadFile(ftp_original_firefox, originalprofile_local + ".zip");

                Directory.CreateDirectory(Path.GetFullPath(originalprofile_local));

                using (ZipFile zip = ZipFile.Read(originalprofile_local + ".zip"))
                {
                    zip.ExtractAll(Path.GetFullPath(originalprofile_local), ExtractExistingFileAction.OverwriteSilently);
                }
            }
        }

        static private string GetPortableProfile(string accountId)
        {
            string profilename = "igprofile_" + accountId;

            return Path.GetFullPath(ffprofile_locate) + "\\" + profilename;
        }

        static public string LoginIG(dynamic account)
        {
            //Open instagram
            //get handle
            int pid = PLAutoHelper.RPAFirefox.GetProcessIdByPath(GetPortableProfile(account.id));
            IntPtr intPtr = Process.GetProcessById(pid).MainWindowHandle;

            //Check if instagram is open
            try
            {
                PLAutoHelper.RPA.WaitImage(Resources.FFPCSwitch.Images.FFLoginIGBtn_Disable, intPtr, 5);
            }
            catch
            {
                return "NotFoundIG";
            }

            //Login
            PLAutoHelper.RPA.SendTextToTextboxByImageOnBackground(Resources.FFPCSwitch.Images.FFLoginIG_UsernameBox, account.user, intPtr); PLAutoHelper.RPA.XDelay(1, 3);
            PLAutoHelper.RPA.SendTextToTextboxByImageOnBackground(Resources.FFPCSwitch.Images.FFLoginIG_PasswordBox, account.password, intPtr); PLAutoHelper.RPA.XDelay(1, 3);
            PLAutoHelper.RPA.ClickByImageRandom(Resources.FFPCSwitch.Images.FFLoginIG_LoginBtn, intPtr);

            //Thread thread = new Thread(() => BackupFFProfile(account, 10000));
            //thread.Start();

            return "Success";
        }

        static public void BackupFFProfile(string accountId, int delay=0)
        {

            Thread.Sleep(delay);

            string profilename = "igprofile_" + accountId;

            var from_path = Path.GetFullPath(ffprofile_locate) + "\\" + profilename;

            var guid = Guid.NewGuid().GetHashCode().ToString();

            var local_backup_path = "Temp\\" + profilename + "_" + guid.Substring(guid.Length - 3) + ".zip";

            var ftp_old_backup = IGHelpers.Helpers.FTP_GetFileListing(ftp_backup_path, profilename + "*.zip").OrderBy(f => f.Modified).ToList();

            //remove old_backup
            if (ftp_old_backup.Count > 5)
            {
                var remove_count = ftp_old_backup.Count - 5;

                for (int b = 0; b < remove_count; b++)
                {
                    IGHelpers.Helpers.FTP_DeleteFile(ftp_old_backup[b].FullName);
                }
            }

            PLAutoHelper.RPAFirefox.BackupFFPortable(from_path, Path.GetFullPath(local_backup_path));

            if(!File.Exists(Path.GetFullPath(local_backup_path)))
            {
                throw new System.ArgumentException("Not found backup");
            }

            //upload to ftp
            IGHelpers.Helpers.FTP_UploadFile(local_backup_path, ftp_backup_path + profilename + "_" + guid.Substring(guid.Length - 3) + ".zip");


            File.Delete(local_backup_path);
        }

        static public void BackupFFThread(List<string> igprofiles)
        {
            List<Thread> threads = new List<Thread>();
            threads.Clear();


            for (int t = 0; t < igprofiles.Count; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => BackupFFProfile(igprofiles[pos]));
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }
            foreach (var t in threads)
            {
                t.Start();

                Thread.Sleep(5000);
            }

            foreach (var t in threads)
            {
                t.Join();
            }
        }
    }
}
