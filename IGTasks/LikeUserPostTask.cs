﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    class LikeUserPostTask : IGTask
    {
        string deviceID, deviceName;
        string log_name;

        public LikeUserPostTask(string deviceID, string deviceName)
        {
            this.deviceID = deviceID;
            this.deviceName = deviceName;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="IGDevice"></param>
        /// <param name="ig_account"></param>
        /// <param name="log_name">likeuserpost_task, likeuserpost_task_re</param>
        public LikeUserPostTask(Class.IGDevice IGDevice, dynamic ig_account)
        {
            this.IGDevice = IGDevice;
            this.ig_account = ig_account;

            this.deviceID = this.IGDevice.deviceID;
            this.deviceName = this.IGDevice.ld_devicename;

            var aio_settings = ig_account.aio_settings;

            this.log_name = aio_settings.Task_LikeUserPost.log_name;
        }

        public override void DoTask()
        {
            LikeUserPost();
        }

        dynamic settings, ig_account;

        private void LikeUserPost()
        {
            //get settings of this account

            GetLikeUserPostSettings();

            //Check if settings is null -> yes -> return

            if (settings == null)
                return;
            //neu đã tới bước này tức là account này đã thỏa điều kiện để thực hiện action

            ////generate log for this session

            //var guid = Guid.NewGuid().GetHashCode().ToString();
            //var actionlog_name = "LikeUserPost_" + guid.Substring(guid.Length - 4);

            ////set time for this session

            //int session_time = RandomFromStr(settings.random.ToString().Split('|')[1]);

            //string old_username = "";


            //get target likeuserpost this date
            var target_likeuserpost_date = SetTargetLikeUserPost();

            //get current following count in this date

            var currentdate_like = CurrentLikeThisDate();

            if (currentdate_like >= target_likeuserpost_date)
                return;

            //set like for this session
            var session_like_count = SetLikeCountThisSession();

            //insert log new_session (use to track finish a block_session
            IGHelpers.DBHelpers.AddActionLog(IGDevice.ld_devicename, "new_likeuserpost_session");

            //use sleep mô phỏng run action

            for (int i = 0; i < session_like_count; i++)
            {
                //get profile to follow

                //var profile = IGHelpers.DBHelpers.GetTargetAccount_SpecificTasks("LikeUserPost", ig_account.id);
                var profile = AIOHelper.DBLogic.GetTargetAccount_SpecificTasks.LikingUserPost(ig_account);

                if (profile == null)
                    break;

                //DO ACTION //DO ACTION //DO ACTION //DO ACTION
                //Do likeuserpost

                //IGThreads.ThreadHelpers.DoLikeUserPost(IGDevice, ig_account, profile.username);

                AIOHelper.ThreadHelper.DoLikeUserPost.Run(IGDevice, ig_account, profile.username);

                //Add action log
                IGHelpers.DBHelpers.AddActionLog(IGDevice.ld_devicename, log_name, "", profile.username);

                //Update status, [likeuserpost] to igtoolkit scraping user


                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                        {
                            using (var command = new SqlCommand("update scrapeusers set [likeuserpost]=CONVERT(varchar,[RecentPost],21), [status]='' where [UserName]=@UserName", conn, sqlTransaction))
                            {
                                command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = profile.username.ToString();
                                command.ExecuteNonQuery();
                            }

                            sqlTransaction.Commit();
                        }
                    }/*, "scrapeusers"*/);
                }


                int sleep_action = RandomFromStr(((string)settings.sleep_action).Split('|')[1]);

                Thread.Sleep(TimeSpan.FromSeconds(sleep_action));
            }


            //check if count session >= block_session_count

            if (IsFinishedCurrentBlockSession())
            {
                SetNextSessionTime(true);
            }
            else
            {
                //set next session time

                var next_session_time = SetNextSessionTime();
            }

            Thread.Sleep(5000);

        }

        private bool IsFinishedCurrentBlockSession()
        {
            int current_new_likeuserpost_session = 0;
            dynamic block_likeuserpost_session = null;
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();


                //get latest block_session log

                using (var sqlCommand = new SqlCommand("select [id],[value] from actionlog where [accountid]=" + ig_account.id + " and [action]='block_likeuserpost_session_count' and DATEDIFF(day,do_at,getdate())=0 order by id desc", conn))
                {
                    using (var dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();

                            var id = dataReader["id"].ToString();
                            var value = dataReader["value"].ToString();

                            block_likeuserpost_session = new
                            {
                                id,
                                value
                            };
                        }
                    }
                }



                if (block_likeuserpost_session == null)
                {
                    //tuc la chua set block_following_session_count
                    var block_session = RandomFromStr(settings.block_session.ToString().Split('|')[1]);

                    //insert to log

                    using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'block_likeuserpost_session_count','" + block_session.ToString() + "')", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }

                    return false;

                }

                //count new_following_session with id > if of block_session log

                using (var sqlCommand = new SqlCommand("select count(*) from actionlog where [accountid]=" + ig_account.id + " and [action]='new_likeuserpost_session' and [id]>" + block_likeuserpost_session.id, conn))
                {
                    current_new_likeuserpost_session = int.Parse(sqlCommand.ExecuteScalar().ToString());
                }

            }

            if (current_new_likeuserpost_session >= int.Parse(block_likeuserpost_session.value))
                return true;

            return false;
        }

        private int SetLikeCountThisSession()
        {
            string action_per_session = settings.action_per_session.ToString();

            var count = RandomFromStr(action_per_session.Split('|')[1]);

            return count;
        }

        private int CurrentLikeThisDate()
        {
            int count = 0;
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select count(*) from actionlog where [accountId]=" + ig_account.id + " and [action] like 'likeuserpost_task%' and DATEDIFF(day,[do_at],getdate())=0", conn))
                {
                    count = int.Parse(sqlCommand.ExecuteScalar().ToString());
                }
            }

            return count;
        }

        private void GetLikeUserPostSettings()
        {
            //Get likeuserpost_type from account_info
            var likeuserpost_type = "";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [action] from actionlog where [accountId]=" + ig_account.id + " and [action] like 'likeuserpost-%' and DATEDIFF(day,[do_at],getdate())=0", conn))
                {
                    var likeuserpost_type_object = sqlCommand.ExecuteScalar();
                    if (likeuserpost_type_object != null)
                        likeuserpost_type = sqlCommand.ExecuteScalar().ToString();
                }
            }

            if (likeuserpost_type == "")
            {
                likeuserpost_type = SetLikeUserPostTypeAutomatically(likeuserpost_type);
                if (likeuserpost_type == "")
                    return;

                //add followingtype to log


                IGHelpers.DBHelpers.AddActionLog(deviceName, likeuserpost_type);

                ////add target sessions_count to log
                //settings = SettingsOfLikeUserPostType(likeuserpost_type);

                //IGHelpers.DBHelpers.AddActionLog(deviceName, "likeuserpost_sessions_count", settings.sessions_count.ToString());

            }

            //Get settings from engagetype

            settings = SettingsOfLikeUserPostType(likeuserpost_type);
        }

        class likeuserpostsetting
        {
            public string setting_name;
            public int count;

            public likeuserpostsetting(string setting_name, int count = 0)
            {
                this.setting_name = setting_name;
                this.count = count;
            }
        }

        private string SetLikeUserPostTypeAutomatically(string likeuserpost_type)
        {
            //get all current settings

            string engagetype = "";
            List<likeuserpostsetting> likeuserpost_history = new List<likeuserpostsetting>();

            likeuserpost_history.Add(new likeuserpostsetting("likeuserpost-warmlite"));
            likeuserpost_history.Add(new likeuserpostsetting("likeuserpost-warmup"));
            likeuserpost_history.Add(new likeuserpostsetting("likeuserpost-default"));

            //using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            //{
            //    conn.Open();

            //    using (var sqlCommand = new SqlCommand("ig_EngageSettings", conn))
            //    {
            //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            //        sqlCommand.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = ig_account.id;
            //        sqlCommand.Parameters.Add("@return_value", System.Data.SqlDbType.VarChar).Value = "engagetype";

            //        var engage_value = sqlCommand.ExecuteScalar();

            //        engagetype = engage_value == null ? "" : engage_value.ToString();
            //    }

            //    using (var sqlCommand = new SqlCommand("ig_EngageSettings", conn))
            //    {
            //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            //        sqlCommand.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = ig_account.id;
            //        sqlCommand.Parameters.Add("@return_value", System.Data.SqlDbType.VarChar).Value = "likeuserpost-history";

            //        using (var dataReader = sqlCommand.ExecuteReader())
            //        {
            //            if (dataReader.HasRows)
            //            {
            //                while (dataReader.Read())
            //                {
            //                    for (int i = 0; i < likeuserpost_history.Count; i++)
            //                    {
            //                        if (dataReader["action"].ToString() == likeuserpost_history[i].setting_name)
            //                        {
            //                            likeuserpost_history[i].count = int.Parse(dataReader["times"].ToString());
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}


            engagetype = AIOHelper.DBLogic.EngageSettings.HistoryOfMassTask(ig_account.id, "engagetype")[0];

            List<dynamic> histories = AIOHelper.DBLogic.EngageSettings.HistoryOfMassTask(ig_account.id, "likeuserpost-history");

            foreach (var h in histories)
            {
                for (int i = 0; i < likeuserpost_history.Count; i++)
                {
                    if (h.action == likeuserpost_history[i].setting_name)
                    {
                        likeuserpost_history[i].count = int.Parse(h.times.ToString());
                    }
                }
            }

            if (engagetype != "default")
                return "";

            //following-warmlite

            if (likeuserpost_history.FirstOrDefault(s => s.setting_name == "likeuserpost-warmlite").count <= SettingsOfLikeUserPostType("likeuserpost-warmlite").days)
            {
                return "likeuserpost-warmlite";
            }

            if (likeuserpost_history.FirstOrDefault(s => s.setting_name == "likeuserpost-warmup").count <= SettingsOfLikeUserPostType("likeuserpost-warmup").days)
            {
                return "likeuserpost-warmup";
            }

            return "likeuserpost-default";
        }

        private dynamic SettingsOfLikeUserPostType(string likeuserpost_type)
        {
            var settings_str = "";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [settings] from engagesettings where [type]='" + likeuserpost_type + "'", conn))
                {
                    settings_str = sqlCommand.ExecuteScalar().ToString();
                }
            }

            List<string> settings = settings_str.Split(new string[] { "***" }, StringSplitOptions.None).ToList<string>();

            //string random = settings.FirstOrDefault(s => s.Contains("random|"));
            //string sleep_session = settings.FirstOrDefault(s => s.Contains("sleep_session|"));
            //int days = settings.FirstOrDefault(s => s.Contains("days|"))==null?-1:int.Parse(settings.FirstOrDefault(s => s.Contains("days|")).Split('|')[1]);
            //int sessions_count = int.Parse(settings.FirstOrDefault(s => s.Contains("sessions_count|")).Split('|')[1]);

            string random = settings.FirstOrDefault(s => s.Contains("random|"));
            string increase = settings.FirstOrDefault(s => s.Contains("increase|"));
            string action_per_session = settings.FirstOrDefault(s => s.Contains("action_per_session|"));
            string sleep_session = settings.FirstOrDefault(s => s.Contains("sleep_session|"));
            string sleep_action = settings.FirstOrDefault(s => s.Contains("sleep_action|"));
            string max_like = settings.FirstOrDefault(s => s.Contains("max_like|"));
            string block_session = settings.FirstOrDefault(s => s.Contains("block_session|"));
            string sleep_per_block = settings.FirstOrDefault(s => s.Contains("sleep_per_block|"));
            int days = settings.FirstOrDefault(s => s.Contains("days|")) == null ? -1 : int.Parse(settings.FirstOrDefault(s => s.Contains("days|")).Split('|')[1]);

            return new
            {
                random,
                increase,
                action_per_session,
                sleep_session,
                sleep_action,
                days,
                max_like,
                block_session,
                sleep_per_block
            };
        }

        private int SetTargetLikeUserPost()
        {
            int target_likeuserpost= 0;
            //get target following from db
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [value] from actionlog where [accountId]=" + ig_account.id + " and [action] like 'target_likeuserpost' and DATEDIFF(day,[do_at],getdate())=0", conn))
                {
                    var target_likeuserpost_obj = sqlCommand.ExecuteScalar();
                    if (target_likeuserpost_obj != null)
                        target_likeuserpost = int.Parse(target_likeuserpost_obj.ToString());
                }
            }

            if (target_likeuserpost == 0)
            {
                int pre_target_likeuserpost = 0;

                // dynamic pre_target_following_dyn = null;
                //get target following of previous day
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var sqlCommand = new SqlCommand("select count(*) from actionlog where [action] like 'likeuserpost_task%' and DATEDIFF(day,convert(datetime, (select top 1 [do_at] from actionlog where [action] like 'likeuserpost_task%' and [accountid]=" + ig_account.id + " order by id desc),21),[do_at])=0 and [accountid]=" + ig_account.id, conn))
                    {

                        pre_target_likeuserpost = int.Parse(sqlCommand.ExecuteScalar().ToString());

                        //using (var dataReader = sqlCommand.ExecuteReader())
                        //{
                        //    if (dataReader.HasRows)
                        //    {
                        //        dataReader.Read();
                        //        var value = int.Parse(dataReader["value"].ToString());
                        //        var date = DateTime.Parse(dataReader["do_at"].ToString());

                        //        pre_target_following_dyn = new
                        //        {
                        //            value,
                        //            date
                        //        };
                        //    }
                        //}
                    }
                }

                //if (pre_target_following_dyn != null)
                //{
                //    pre_target_following = pre_target_following_dyn.value;
                //}

                var increase = 0;

                try
                {
                    if (settings.increase != null)
                    {
                        string increase_str = settings.increase.ToString();
                        increase = RandomFromStr(increase_str.Split('|')[1]);
                    }
                }
                catch { }

                if (pre_target_likeuserpost == 0 && increase == 0) //CHANGE || TO && -> because old version use differ log name
                {
                    //random value from settings.random

                    string random = settings.random.ToString();

                    target_likeuserpost = RandomFromStr(random.Split('|')[1]);
                }
                else
                {
                    //pre_target_following + settings.random


                    target_likeuserpost = pre_target_likeuserpost + increase;
                }

                //get max_following i has, if target_following > max_following => set target_following in max_following range

                try
                {
                    if (settings.max_like != null)
                    {
                        string max_like_str = settings.max_like.ToString();

                        int max = int.Parse(max_like_str.Split('|')[1].Split('-')[1]);

                        if (target_likeuserpost > max)
                            target_likeuserpost = RandomFromStr(max_like_str.Split('|')[1]);
                    }
                }
                catch { }

                //check neu thoi diem thuc hien following_task gan nhat > 3 ngay thi target_following=target_following/2


                target_likeuserpost = target_likeuserpost / (IsLongTimeNoDoingTask());

                //set and add log to actionlog

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'target_likeuserpost'," + target_likeuserpost.ToString() + ")", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }




            return target_likeuserpost;

        }


        private int IsLongTimeNoDoingTask()
        {
            string latest_dotask = null;
            //get latest do following_tasking day

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [do_at] from actionlog where [accountid]=" + ig_account.id + " and [action]='target_likeuserpost' order by id desc", conn))
                {
                    using (var dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();
                            latest_dotask = dataReader["do_at"].ToString();
                        }
                    }
                }
            }

            if (latest_dotask == null) //chua thuc hien task bao gio
            {
                return 1;
            }

            //neu nhu khoang cach >10 thi chia 3

            if (DateTime.Now.Subtract(DateTime.Parse(latest_dotask)).TotalDays > 10)
                return 3;

            //neu khoang cach >3 thi chia 2
            if (DateTime.Now.Subtract(DateTime.Parse(latest_dotask)).TotalDays > 3)
                return 2;

            return 1;
        }

        private DateTime SetNextSessionTime(bool finish_block_likeuserpost_session = false)
        {
            string sleep_session = settings.sleep_session.ToString();

            int sleep_seconds = RandomFromStr(sleep_session.Split('|')[1]);

            var next_session_time = DateTime.Now.AddSeconds(sleep_seconds);


            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                if (finish_block_likeuserpost_session)
                {
                    //tuc la da het 1 block, set sleep dài hơn theo sleep_per_block settings

                    string sleep_per_block = settings.sleep_per_block.ToString();

                    sleep_seconds = RandomFromStr(sleep_per_block.Split('|')[1]);

                    //random block_following_session_count cho block ke tiep

                    var block_session = RandomFromStr(settings.block_session.ToString().Split('|')[1]);

                    //insert to log


                    using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'block_likeuserpost_session_count','" + block_session.ToString() + "')", conn))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }

                }


                using (var sqlCommand = new SqlCommand("insert into actionlog ([accountId], [action],[value]) values (" + ig_account.id + ",'likeuserpost_next_session','" + next_session_time.ToString() + "')", conn))
                {
                    sqlCommand.ExecuteNonQuery();
                }
            }

            return next_session_time;
        }

        private int RandomFromStr(string random_str)
        {
            var from = int.Parse(random_str.Split('-')[0]);
            var to = int.Parse(random_str.Split('-')[1]);

            return (new Random(Guid.NewGuid().GetHashCode())).Next(from, to);
        }
    }
}
