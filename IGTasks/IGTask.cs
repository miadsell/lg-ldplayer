﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    abstract class IGTask
    {
        public Class.IGDevice IGDevice;
        public abstract void DoTask();
    }
}
