﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    class SharePostToStoryTask:IGTask
    {
        string media_url;

        public SharePostToStoryTask(IGDevice IGDevice, string media_url)
        {
            this.IGDevice = IGDevice;
            this.media_url = media_url;
        }

        public override void DoTask()
        {
            var action = new Action.SharePostToStory(IGDevice, media_url);
            action.DoAction();

            IGHelpers.DBHelpers.AddActionLog(IGDevice.ld_devicename, "sharepost_story");
        }
    }
}
