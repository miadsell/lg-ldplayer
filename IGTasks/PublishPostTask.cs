﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGTasks
{
    class PublishPostTask : IGTask
    {
        string content;
        List<string> image_paths;

        public PublishPostTask(IGDevice IGDevice, List<string> image_paths, string content)
        {
            this.IGDevice = IGDevice;
            this.image_paths = image_paths;
            this.content = content;
        }

        public override void DoTask()
        {
            PublishPost();
        }

        private void PublishPost()
        {
            //modify image

            List<string> modify_imgs = new List<string>();

            foreach (var image_path in image_paths)
            {
                var modify_img = IGHelpers.ExifHelpers.OptimizeImage_FTPServer(IGDevice.ld_devicename, image_path);
                modify_imgs.Add(modify_img);
            }

            //Send to xammp
            //var destpath = ADBSupport.ADBHelpers.CreatePath(IGHelpers.Helpers.xampp_server_location + IGDevice.ld_devicename);

            //File.Copy(modify_img, destpath + "\\" + Path.GetFileName(modify_img));

            //var imageserver_link = IGHelpers.Helpers.xammp_url + IGDevice.ld_devicename + "/" + Path.GetFileName(modify_img);


            var action = new Action.PublishPost(this.IGDevice, content, modify_imgs);
            action.DoAction();


            IGHelpers.DBHelpers.AddActionLog(IGDevice.ld_devicename, "published_post");
        }
    }
}
