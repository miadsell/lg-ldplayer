﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGThreads
{
    class CheckBackupThread : IGThread
    {

        Semaphore semaphore_check = new Semaphore(20, 20);

        List<string> need_backup_ids = new List<string>();

        List<string> need_remove_files = new List<string>();

        List<string> need_remove_folders = new List<string>();

        List<Thread> threads = new List<Thread>();
        public override void DoThread()
        {

            //CheckBackup("506");

            threads.Clear();

            List<string> accountIds = new List<string>();

            //get all account
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select * from ig_account where [state]!='DISABLE'", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                            accountIds.Add(dataReader["id"].ToString());
                    }
                }

            }


            for (int t = 0; t < accountIds.Count; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => CheckBackup(accountIds[pos]));
                thread.Name = "checkbackup_" + pos.ToString();
                threads.Add(thread);
            }

            foreach (var t in threads)
            {
                t.Start();


            }

            foreach (var t in threads)
            {
                t.Join();
            }

            List<string> inactive_accountIds = new List<string>();

            //get all account DISABLE or AskSecurityCode
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                var sql_command =
                    @"select *
                    from ig_account
                    where
	                    [state]='DISABLE' or [state]='AskSecurityCode'";

                using (var command = new SqlCommand(sql_command, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                            inactive_accountIds.Add(dataReader["id"].ToString());
                    }
                }

            }

            foreach(var item in inactive_accountIds)
            {
                var deviceName = "LDPlayer-IG-" + item;

                var local_backup_folder = PLAutoHelper.NormalFuncHelper.OptimizePathEndBackSlash(ADBSupport.LDHelpers.LDPlayer_dnplayerdata) + deviceName;

                if(Directory.Exists(local_backup_folder))
                {
                    need_remove_folders.Add(local_backup_folder);
                }

            }

            //if want to delete file, uncomment below code

            foreach (var item in need_remove_folders)
            {
                if (item == null)
                    continue;
                Directory.Delete(item, true);
            }

            foreach (var item in need_remove_files)
            {
                if (item == null)
                    continue;

                if (File.Exists(item))
                    File.Delete(item);
            }


        }

        public void Temp_ClearUnsedFolderFile(string accountId)
        {

            var deviceName = "LDPlayer-IG-" + accountId;

            var local_backup_folder = PLAutoHelper.NormalFuncHelper.OptimizePathEndBackSlash(ADBSupport.LDHelpers.LDPlayer_dnplayerdata) + deviceName + @"\";


            if (Directory.Exists(local_backup_folder + "temp_img"))
            {
                need_remove_folders.Add(local_backup_folder + "temp_img");
            }

            if (Directory.Exists(local_backup_folder + "igprofile_img"))
            {
                need_remove_folders.Add(local_backup_folder + "igprofile_img");
            }

            if (Directory.Exists(local_backup_folder + "error_screen"))
            {
                need_remove_folders.Add(local_backup_folder + "error_screen");
            }

            if(Directory.Exists(local_backup_folder + "system"))
            {
                //list all file under system
                var files = Directory.GetFiles(local_backup_folder + "\\system").ToList().OrderByDescending(f => (new DirectoryInfo(f).LastWriteTime)).ToList();

                //remove default_exif.txt

                files.RemoveAll(s => s.Contains("default_exif.txt"));

                need_remove_files.AddRange(files);
            }
        }


        public void CheckBackup(string accountId)
        {
            semaphore_check.WaitOne();

            //check if account is DISABLE or 


            var deviceName = "LDPlayer-IG-" + accountId;

            var local_backup_folder = PLAutoHelper.NormalFuncHelper.OptimizePathEndBackSlash(ADBSupport.LDHelpers.LDPlayer_dnplayerdata) + deviceName + @"\system\backup\";

            if (!Directory.Exists(local_backup_folder))
            {
                need_backup_ids.Add(accountId);
                goto FinishCheck;
            }



            var backup_folders = Directory.GetDirectories(local_backup_folder).ToList().OrderByDescending(f => (new DirectoryInfo(f).LastWriteTime)).ToList();

            //remove folder not meet requirement or !=daily folder
            foreach (var f in backup_folders)
            {
                if (f.Contains("\\daily"))
                    continue;

                //var files = Directory.GetFiles(f).Where(s => Path.GetFileName(s).Contains(".tar") || Path.GetFileName(s).Contains(".properties")).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();

                //if (files.Count < 2)
                //{
                //    need_remove_folders.Add(f);
                //}

                need_remove_folders.Add(f);
            }

            //remove unuse file
            foreach (var f in backup_folders)
            {
                if (f.Contains("\\daily"))
                    continue;

                var files = Directory.GetFiles(f).Where(s => (Path.GetFileName(s).Contains(".tar") || Path.GetFileName(s).Contains(".properties")) && !Path.GetFileName(s).Contains("_bkpl.tar.gz")).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();

                //get properties files and order by new to old
                var property_files = Directory.GetFiles(f).Where(s => Path.GetFileName(s).Contains(".properties")).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();

                //check every properties file to see if has a .tar file in same name, if not => add to remove_files queue

                List<string> keep_files = new List<string>();

                foreach (var p in property_files)
                {
                    if (File.Exists(f + "\\" + Path.GetFileNameWithoutExtension(p) + ".tar.gz"))
                    {
                        keep_files.Add(p);
                        keep_files.Add(f + "\\" + Path.GetFileNameWithoutExtension(p) + ".tar.gz");
                        break;
                    }

                    if (File.Exists(f + "\\" + Path.GetFileNameWithoutExtension(p) + ".tar.lzop"))
                    {
                        keep_files.Add(p);
                        keep_files.Add(f + "\\" + Path.GetFileNameWithoutExtension(p) + ".tar.lzop");
                        break;
                    }
                }

                //
                foreach (var item in keep_files)
                {
                    files.Remove(item);
                }

                need_remove_files.AddRange(files);
            }

            //remove old backups
            var backup_daily_folder = local_backup_folder + "daily";


            //uu tien keep _bkpl.tar.gz file
            if (Directory.Exists(backup_daily_folder))
            {
                int max_block_backup_keep = 5;

                //check bkpl first
                //var daily_bkpl_files = Directory.GetFiles(backup_daily_folder).Where(s => Path.GetFileName(s).Contains("_bkpl.tar.gz")).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();

                var daily_bkpl_files = Directory.GetFiles(backup_daily_folder).Where(s => Path.GetFileName(s).Contains("_bkpl.tar.gz") && (new FileInfo(s)).Length > 1572864).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();

                if (daily_bkpl_files.Count > max_block_backup_keep)
                {
                    //remove bkpl file since max_block_backup_keep
                    for (int c = max_block_backup_keep; c < daily_bkpl_files.Count; c++)
                    {
                        need_remove_files.Add(daily_bkpl_files[c]);
                    }

                    max_block_backup_keep = 0;
                }
                else
                {
                    max_block_backup_keep = max_block_backup_keep - daily_bkpl_files.Count;
                }

                //get file <1.5mb

                var daily_bkpl_files_1_5 = Directory.GetFiles(backup_daily_folder).Where(s => Path.GetFileName(s).Contains("_bkpl.tar.gz") && (new FileInfo(s)).Length < 1572864).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();

                need_remove_files.AddRange(daily_bkpl_files_1_5);

                //get available property_files

                var daily_property_files = Directory.GetFiles(backup_daily_folder).Where(s => Path.GetFileName(s).Contains(".properties")).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();


                if (daily_property_files.Count > max_block_backup_keep)
                {

                    //get file that need to keep
                    List<string> keep_files_property = new List<string>();

                    //duyet tu gan nhat den xa nhat
                    foreach(var d in daily_property_files)
                    {
                        if (File.Exists(backup_daily_folder + "\\" + Path.GetFileNameWithoutExtension(d) + ".tar.lzop"))
                        {
                            keep_files_property.Add(d);
                        }

                        if (keep_files_property.Count > max_block_backup_keep)
                            break;
                    }

                    //check if daily property file in keep list, if not=>remove
                    for (int d = 0; d < daily_property_files.Count; d++)
                    {
                        //neu nam trong keep list thi giu lai
                        if (keep_files_property.Contains(daily_property_files[d]))
                            continue;

                        if (File.Exists(backup_daily_folder + "\\" + Path.GetFileNameWithoutExtension(daily_property_files[d]) + ".tar.lzop"))
                        {
                            need_remove_files.Add(backup_daily_folder + "\\" + Path.GetFileNameWithoutExtension(daily_property_files[d]) + ".tar.lzop");
                        }

                        if (File.Exists(backup_daily_folder + "\\" + Path.GetFileNameWithoutExtension(daily_property_files[d]) + ".tar.gz"))
                        {
                            need_remove_files.Add(backup_daily_folder + "\\" + Path.GetFileNameWithoutExtension(daily_property_files[d]) + ".tar.gz");
                        }

                        need_remove_files.Add(daily_property_files[d]);
                    }
                }

                //get available .tar.lzop file
                var daily_tar_lzop_files = Directory.GetFiles(backup_daily_folder).Where(s => Path.GetFileName(s).Contains(".tar.lzop")).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();

                //check if not exists .properties file tuong ung thi add to remove list

                foreach(var f in daily_tar_lzop_files)
                {
                    string property_file_path = backup_daily_folder + "\\" + Path.GetFileName(f).Replace(".tar.lzop", "") + ".properties";

                    if(!File.Exists(property_file_path))
                    {
                        need_remove_files.Add(f);
                    }
                }

            }

            //get account need backup again

            if (backup_folders.Count==0)
            {
                need_backup_ids.Add(accountId);
                goto FinishCheck;
            }

            bool need_backup = true;

            foreach (var f in backup_folders)
            {
                var files = Directory.GetFiles(f).Where(s => Path.GetFileName(s).Contains(".tar") || Path.GetFileName(s).Contains(".properties")).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();

                if (files.Count >= 2)
                {
                    need_backup = false;
                    break;
                }
            }

            if (need_backup)
                need_backup_ids.Add(accountId);

        FinishCheck:
            semaphore_check.Release();
        }


        private void ListAccountINACTIVE()
        {

        }
    }
}
