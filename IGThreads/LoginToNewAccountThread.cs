﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.IGThreads
{
    class LoginToNewAccountThread : IGThread
    {
        List<Thread> threads = new List<Thread>();
        public override void DoThread()
        {

            threads.Clear();

            var focus_engage = ADBSupport.LDHelpers.LDPlayer_FocusEngage_Threads_Count;
            var focus_mass = ADBSupport.LDHelpers.LDPlayer_FocusMassAction_Threads_Count;

            for (int t = 0; t < ADBSupport.LDHelpers.LDPlayerThread_Count; t++)
            {
                var pos = t;

                Thread thread = new Thread(() => LoginNewAccount());
                thread.Name = "igaio" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }

            //Start a thread to clearlog, use thread to pass thread name pattern

            //IGHelpers.ProxyHelpers.ProxyServerOthers("igaio", true);


            //Load chrome to tab

            //IGHelpers.Helpers.LaunchDriver();

            foreach (var t in threads)
            {
                t.Start();
            }


            ManageThread();

            //Run ticker

            //InitTimer_UpdateProxy();
            //InitTimer_UpdateProxyStatus();
            InitTimer_ManageThread();
        }

        private void LoginNewAccount()
        {
            while (true)
            {
                //take account to login

                var ig_account = AIOHelper.DBLogic.GetAccount_DoSpecificTask.GetAccount_DoSpecificTask_LoginNewACCOUNT();

                if (ig_account == null)
                    break;

                //login
                Thread.SetData(Thread.GetNamedDataSlot("thread_type"), "action_light");

                //add do_thread_name to log

                IGHelpers.DBHelpers.AddActionLogByID(ig_account.id, "do_thread_name", "loginnewaccount");

                if (Program.form.isstop)
                    break;

                IGDevice iGDevice = null;

                iGDevice = new IGDevice(ig_account, new LDDevice(ig_account.phone_device), true, Program.form.skip_launch_error);

                if (!iGDevice.islogin)
                {
                    goto IGDeviceDispose;
                }
                else
                {
                    //update state to active

                    IGHelpers.DBHelpers.UpdateStatusofIG(ig_account.id, "");
                    IGHelpers.DBHelpers.UpdateStateofIG(ig_account.phone_device, "ACTIVE");
                }

            //dispose
            IGDeviceDispose:
                iGDevice.Dispose();
                //Unset thread

                Thread.SetData(Thread.GetNamedDataSlot("thread_type"), null);

            }
        }

        private void ManageThread()
        {
            int count_running_thread = 0;
            for (int i = 0; i < threads.Count; i++)
            {
                if (threads[i].ThreadState.ToString() == "Running" || threads[i].ThreadState.ToString() == "WaitSleepJoin")
                {
                    count_running_thread++;
                }
            }

            if (count_running_thread == 0)
            {
                timer_UpdateProxy.Stop();
                timer_UpdateProxyStatus.Stop();
                //timer_RefreshForm.Stop();
                timer_ManageThread.Stop();

                //Unfreeze form
                Program.form.Invoke((MethodInvoker)delegate
                {
                    Program.form.UnFreezeControl();
                });
            }
        }

        #region --Ticker ManageThread--

        private System.Windows.Forms.Timer timer_ManageThread;

        public void InitTimer_ManageThread()
        {
            timer_ManageThread = new System.Windows.Forms.Timer();
            timer_ManageThread.Tick += new EventHandler(timer_ManageThread_Tick);
            timer_ManageThread.Interval = 5000; // in miliseconds
            timer_ManageThread.Start();

        }

        private void timer_ManageThread_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(new ThreadStart(ManageThread));
            worker.Name = "Ticking Manage Thread";
            worker.IsBackground = true;
            worker.Start();
        }

        #endregion
    }
}
