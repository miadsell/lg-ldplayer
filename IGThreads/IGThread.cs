﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGThreads
{
    abstract class IGThread
    {
        public abstract void DoThread();


        #region --Ticker Update CurrentUsed Proxy--
        //Update proxy status

        protected System.Windows.Forms.Timer timer_UpdateProxy;

        protected void InitTimer_UpdateProxy()
        {
            timer_UpdateProxy = new System.Windows.Forms.Timer();
            timer_UpdateProxy.Tick += new EventHandler(timer_UpdateProxy_Tick);
            timer_UpdateProxy.Interval = 5000; // in miliseconds
            timer_UpdateProxy.Start();

        }

        private void timer_UpdateProxy_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(() => IGHelpers.ProxyHelpers.UpdateCurrentUsed());
            worker.Name = "Update Proxy CurrentUsed Thread";
            worker.IsBackground = true;
            worker.Start();
        }


        #endregion


        #region --Ticker Update Proxy Status--
        //Update proxy status

        protected System.Windows.Forms.Timer timer_UpdateProxyStatus;

        protected void InitTimer_UpdateProxyStatus()
        {
            timer_UpdateProxyStatus = new System.Windows.Forms.Timer();
            timer_UpdateProxyStatus.Tick += new EventHandler(timer_UpdateProxyStatus_Tick);
            timer_UpdateProxyStatus.Interval = 10000; // in miliseconds
            timer_UpdateProxyStatus.Start();

        }

        private void timer_UpdateProxyStatus_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(() => IGHelpers.ProxyHelpers.ProxyServerOthers(""));
            worker.Name = "Update Proxy Status Thread";
            worker.IsBackground = true;
            worker.Start();
        }

        #endregion

        #region --Ticker refresh form--

        protected System.Windows.Forms.Timer timer_RefreshForm;

        protected void InitTimer_RefreshForm()
        {
            timer_RefreshForm = new System.Windows.Forms.Timer();
            timer_RefreshForm.Tick += new EventHandler(timer_RefreshForm_Tick);
            timer_RefreshForm.Interval = 5000; // in miliseconds
            timer_RefreshForm.Start();

        }

        private void timer_RefreshForm_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(new ThreadStart(Program.form.RefreshForm));
            worker.Name = "Refresh Form Thread";
            worker.IsBackground = true;
            worker.Start();
        }


        #endregion
    }
}
