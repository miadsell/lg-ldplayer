﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGThreads
{
    class GrabSlavesCommentThread:IGThread
    {

        List<Thread> threads = new List<Thread>();
        public override void DoThread()
        {
            threads.Clear();

            for (int t = 0; t < 3; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => GrabSlavesComment());
                thread.Name = "grabslave" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }

            foreach (var t in threads)
            {
                t.Start();
            }
        }

        private void GrabSlavesComment()
        {

            while (true)
            {

                if (Program.form.isstop)
                    break;


                var proxy = IGHelpers.ProxyHelpers.GetProxyForQuickScrapeTask();

            GetAPIObj:
                var dyn = IGAPIHelper.Scraper.GetAPIObj(proxy, "noclaim");

                var account = dyn.account;

                InstagramApiSharp.API.IInstaApi _instaApi = dyn.instaApi;

                if (_instaApi == null)
                {
                    Thread.Sleep(20000);
                    goto GetAPIObj;
                }

                var slave = IGHelpers.DBHelpers.GetSlaveAccountToScrape();

                if (slave == null)
                {
                    Thread.Sleep(TimeSpan.FromMinutes(15));
                    continue;
                }

                //get slave account
                string accountId = slave.accountId;
                var slave_user = slave.user;

                //get posts
                int retry = 0;
            GetMedias:

                var medias = IGAPIHelper.Scraper.ScrapeMediasFromUser(_instaApi, slave_user);

                if (medias == null)
                {
                    retry++;
                    if (retry > 5)
                        continue;
                    Thread.Sleep(2000);
                    goto GetMedias;
                }

                //get comment on posts

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    foreach (var item in medias)
                    {
                        var comments = IGAPIHelper.Scraper.ScrapeCommenters("https://www.instagram.com/p/" + item.Code + "/", _instaApi);

                        if (comments != null)
                        {
                            foreach (var comment in comments.Comments)
                            {
                                var user = comment.User.UserName;
                                if (user == slave_user)
                                    continue;
                                var post_url = "https://www.instagram.com/p/" + item.Code + "/";
                                var comment_pk = comment.Pk;
                                var comment_text = comment.Text;
                                var comment_date = comment.CreatedAt;
                                var scraped_at = DateTime.Now;


                                try
                                {
                                    using (var command = new SqlCommand("insert into slaves_comment (accountId,slave_user,post_url,comment_pk,comment_text,comment_at) values (" + accountId + ",'" + slave_user + "','" + post_url + "','" + comment_pk + "',@comment_text,'" + comment_date.ToString() + "')", conn))
                                    {
                                        command.Parameters.Add("@comment_text", System.Data.SqlDbType.NVarChar).Value = comment_text;
                                        command.ExecuteNonQuery();
                                    }
                                }
                                catch { }

                            }
                        }
                    }
                }
                Thread.Sleep(TimeSpan.FromMinutes(5));
            }
        }
    }
}
