﻿using InstagramApiSharp.Classes;
using InstagramLDPlayer.ADBSupport;
using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGThreads
{
    static class ThreadHelpers
    {
        //static public void DoLikeUserPost(IGDevice iGDevice, dynamic ig_account, string target_profile)
        //{



        //}

        //static public string DoFollowingUser(IGDevice iGDevice, dynamic ig_account, string target_profile)
        //{

        //    string deviceName = iGDevice.ld_devicename;
        //    string deviceID = iGDevice.deviceID;

        //    //Open intent user profile

        //    ADBSupport.LDHelpers.OpenIGProfile(deviceName, target_profile);

        //    //Check if user not found

        //    try
        //    {
        //        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Following_UserNotFound, 1, 5);
        //        return "user_not_found";
        //    }
        //    catch
        //    {

        //    }

        //    //Scroll down random some times
        //    var scroll_down = new Action.RandomScroll("down", deviceID);

        //    var scroll_down_times = (new Random()).Next(1, 3);

        //    for (int s = 0; s < scroll_down_times; s++)
        //    {
        //        scroll_down.DoAction();

        //        ADBSupport.ADBHelpers.Delay(1, 3);
        //    }


        //    //Back to top

        //    var scroll_up = new Action.RandomScroll("up", deviceID);

        //    Stopwatch watch = new Stopwatch();
        //    watch.Start();

        //    while (true)
        //    {

        //        if (watch.Elapsed.TotalMinutes > 1)
        //        {
        //            //Check if action blocked
        //            var isaction_blocked=IGHelpers.Helpers.IsActionBlocked(deviceID, deviceName);

        //            if(isaction_blocked)
        //            {
        //                return "Action Blocked";
        //            }

        //            return "";
        //        }

        //        scroll_up.DoAction();

        //        try
        //        {
        //            //scroll until see Follow Btn
        //            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Follow1Btn, 1, 1);
        //            break;
        //        }
        //        catch { }

        //        ADBSupport.ADBHelpers.Delay(1, 2);

        //    }

        //    //Do follow

        //    var follow = new Action.FollowPeople(iGDevice);
        //    follow.DoAction();

        //    return "";
        //}

        static public void DoEngage(IGDevice iGDevice, dynamic ig_account)
        {
            string time_per_session = ig_account.time_per_session;

            double from = double.Parse(time_per_session.Split('-')[0]);
            double to = double.Parse(time_per_session.Split('-')[1]);

            double time_this_session = (new Random()).NextDouble() * (to - from) + from;

            Stopwatch watch_engage = new Stopwatch();
            watch_engage.Start();


            //Open ig

            ADBSupport.ADBHelpers.GoToIntentView(iGDevice.deviceID, "android.intent.action.VIEW", "https://instagram.com", "com.instagram.android");


            //Start adb clipper

            var task = new IGTasks.IGEngageTask(iGDevice, ig_account, time_this_session);
            task.DoTask();


            //put in finally (try catch)

            //update time spent to account_info


            IGHelpers.DBHelpers.UpdateEngageInfo(ig_account.id, time_this_session);
        }

        static public void DoSimulateStories(IGDevice iGDevice, dynamic ig_account, string target_profile)
        {

            string deviceName = iGDevice.ld_devicename;
            string deviceID = iGDevice.deviceID;

            //Open intent user profile

            ADBSupport.LDHelpers.OpenIGProfile(deviceName, target_profile);


            ////Scroll down random some times
            //var scroll_down = new Action.RandomScroll("down", deviceID);

            //var scroll_down_times = (new Random()).Next(1, 3);

            //for (int s = 0; s < scroll_down_times; s++)
            //{
            //    scroll_down.DoAction();

            //    ADBSupport.ADBHelpers.Delay(1, 3);
            //}


            ////Back to top

            //var scroll_up = new Action.RandomScroll("up", deviceID);

            //Stopwatch watch = new Stopwatch();
            //watch.Start();

            //while (true)
            //{

            //    if (watch.Elapsed.TotalMinutes > 1)
            //        return;

            //    scroll_up.DoAction();

            //    try
            //    {
            //        //scroll until see Follow Btn
            //        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Follow1Btn, 1, 1);
            //        break;
            //    }
            //    catch { }

            //    ADBSupport.ADBHelpers.Delay(1, 2);

            //}

            //Check if see followingbtn
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Follow1Btn, 1, 4);

                //if yes, click follow

                var follow = new Action.FollowPeople(iGDevice);
                follow.DoAction();
            }
            catch
            {

            }

            //Check if has new stories

            //use Ramtinak to check if has new stories
            var dyn = IGHelpers.Helpers.APIObJWithProxy();

            var current_stories = IGAPIHelper.Scraper.IsHasNewStoriesByUsername(dyn.instaApi, target_profile);


            if (current_stories == 0)
                return;


            //var started_round = ADBSupport.ADBHelpers.FindPointByPercentage(deviceID, ADBSupport.BMPSource.SimulateStoriesTask_Stories_Round);


            //if (started_round == null)
            //    return;

            //If yes, click stories

            ADBSupport.ADBHelpers.ClickRelative(deviceID, ADBSupport.BMPSource.SimulateStoriesTask_Tag_BackArrow, new { x = 3.5, y = 6.2 }, new { x = 9.7, y = 16.7 }, new { x = 23.8, y = 21.9 });

            //var rec_stories = new Class.RecPercentage(started_round, 22.8, 2.3);

            //ADBSupport.ADBHelpers.TapRecTanglePercentage(deviceID, rec_stories);

            //Engage stories

            int target_engage = (new Random()).Next(current_stories, current_stories + 5);
            int swipe_right = 0;

            var choice_vector = "";

            List<string> vectors = new List<string>() { "left|60-80", "right|1-60", "hold|80-100" };

            bool force_end = false;

            for (int t = 0; t < target_engage; t++)
            {
                var temp_vectors = vectors;
                //Swipe
                if (swipe_right == 0) //neu vi tri hien tai la anh dau tien thi choice_vector must right or hold)
                {
                    //choice_vector = "right";
                    temp_vectors = temp_vectors.Where(a => !a.Contains("left")).ToList<string>();
                }

                if (swipe_right == (current_stories - 1))
                {
                    //tuc la dang o anh cuoi cung => must left or hold


                    temp_vectors = temp_vectors.Where(a => !a.Contains("right")).ToList<string>();

                }

                choice_vector = IGHelpers.Helpers.RandomActionByPercentage(temp_vectors, 0);
                if (choice_vector == null)
                    force_end = true;
                else
                {
                    switch (choice_vector)
                    {
                        case "left":


                            swipe_right--;

                            KAutoHelper.ADBHelper.TapByPercent(deviceID, 3.5, 55.8);

                            break;
                        case "right":


                            swipe_right++;

                            KAutoHelper.ADBHelper.TapByPercent(deviceID, 91.4, 55.8);

                            break;
                        case "hold":
                            //Hold

                            int hold_dur = (new Random()).Next(2000, 10000);

                            KAutoHelper.ADBHelper.LongPress(deviceID, 47, 56, hold_dur);

                            break;
                        default:
                            force_end = true;
                            break;
                    }
                }

                //check if stories end => break

                if (force_end)
                    break;

                ADBSupport.ADBHelpers.Delay(1, 3);

                //var follow = new Action.FollowPeople(iGDevice);
                //follow.DoAction();
            }
        }


        #region --Publish Post Thread--

        static public string DoPublishPost(IGDevice iGDevice, dynamic ig_account, string settings = null)
        {
            string deviceID = iGDevice.deviceID;


            string status = "";

            //Get content from db


            string include_in_settings = null;

            switch (settings)
            {
                case string a when a.Contains("watermark|true"):
                    include_in_settings = "watermark_zone";
                    break;
            }

            var post = IGHelpers.DBHelpers.GetPostToPublish(ig_account.id, include_in_settings);

            //test

            //var post = new
            //{
            //    id = "",
            //    content = "",
            //    image = "MAXIV01.jpg",
            //    hashtag = "",
            //    settings = "watermark_zone|569-98-188***watermark_zone|24-580-185"
            //};

            if (post == null)
            {
                status = "OutOfPost"; //meant no post
                goto UpdateStatus;
            }


            var guid = Guid.NewGuid().GetHashCode().ToString();

            List<string> image_paths = new List<string>();

            List<string> images_base64 = ((string)post.image).Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            var temp_image_devicepath = "Temp\\" + iGDevice.ld_devicename + "\\temp_img\\";

            ADBSupport.ADBHelpers.CreatePath(temp_image_devicepath);

            foreach (var item in images_base64)
            {
                //write to temp
                var bmp = Action.ActionHelpers.BitmapFromBase64Str(item);
                var temp_guid = Guid.NewGuid().GetHashCode().ToString();
                var image_path = temp_image_devicepath + temp_guid.Substring(temp_guid.Length - 4) + ".jpg";
                bmp.Save(image_path, System.Drawing.Imaging.ImageFormat.Jpeg);

                image_paths.Add(image_path);
            }

            //string postimage_path = IGHelpers.Helpers.postimage_path + "\\" + post.image;

            //Modify image if needed

            //if (settings.Contains("watermark|true"))
            //{
            //    //get all zones
            //    var post_settings = ReadPostSettings(post.settings);

            //    //choose random zone to add watermark

            //    string choiced_zone = post_settings.watermark_zone[(new Random()).Next(post_settings.watermark_zone.Count)].Split('|')[1];  //x-y-width

            //    var x = int.Parse(choiced_zone.Split('-')[0]);
            //    var y = int.Parse(choiced_zone.Split('-')[1]);
            //    var target_width = int.Parse(choiced_zone.Split('-')[2]);

            //    //get watermark .png
            //    var watermark_path = GetWatermark(iGDevice.ld_devicename);

            //    //add watermark

            //    postimage_path = ADBSupport.LDHelpers.LDPlayerDATA_path + "\\" + ig_account.phone_device + "\\temp_img\\postimage_" + guid.Substring(guid.Length - 6) + ".png";

            //    ADBSupport.ImageHelpers.AddWaterImage(IGHelpers.Helpers.postimage_path + "\\" + post.image, watermark_path, postimage_path, new Point(x, y), target_width);

            //}

            //Generate content from db

            //post.content + default_content + hashtag


            //default content
            var default_content = GenerateDefaultContent(iGDevice.ld_devicename);


            //hashtag

            string hashtag_str = post.hashtag.ToString();

            var hashtag_list = hashtag_str.Replace(" ", "").Split(',').ToList<string>();

            var hashtag_count = 15;

            if (hashtag_list.Count < 15)
                hashtag_count = hashtag_list.Count;

            Random rnd = new Random();
            var choiced_hashtags = hashtag_list.OrderBy(user => rnd.Next()).Take(hashtag_count).ToList();

            var choiced_hashtags_str = "";

            foreach (var item in choiced_hashtags)
            {
                choiced_hashtags_str += item + ", ";
            }

            choiced_hashtags_str = choiced_hashtags_str.Remove(choiced_hashtags_str.Length - 2);


            var final_content = post.content + "\r\n" + default_content + "\r\n" + choiced_hashtags_str;

            //check if use watermark, if yes -> add default_watermark content

            //if (settings.Contains("watermark|true"))
            //{
            //    var watermark_txt = GetWatermarkContent(iGDevice.ld_devicename);

            //    final_content = post.content + "\r\n" + watermark_txt + "\r\n" + default_content + "\r\n" + choiced_hashtags_str;
            //}


            //Publish

            var task = new IGTasks.PublishPostTask(iGDevice, image_paths, final_content);
            task.DoTask();


            //Add post log
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("insert into ig_post_log (postId,accountId) values (" + post.id + "," + ig_account.id + ")", conn))
                {
                    command.ExecuteNonQuery();
                }
            }
            //Update status
            status = "Done";
        UpdateStatus:
            return status;
        }

        static public string GenerateSelfComment(string deviceName)
        {
            var ftp_folder_path = ADBSupport.LDHelpers.ftp_dnplayerdata + deviceName + "/system/";

        GetReplies:
            var ftp_default_reply_paths = IGHelpers.Helpers.FTP_GetFileListing(ftp_folder_path, "default_reply");

            //Check if default content generate

            if (ftp_default_reply_paths.Count == 0)
            {
                //If not generated

                //DirectoryInfo dinfo_default = new DirectoryInfo(IGHelpers.Helpers.default_reply_selfcomment);
                //FileInfo[] default_files = dinfo_default.GetFiles("*.txt");

                var ftp_default_files = IGHelpers.Helpers.FTP_GetFileListing(IGHelpers.Helpers.ftp_default_reply_selfcomment, ".txt");

                var ftp_default_txt = IGHelpers.Helpers.FTP_TextFileContent(ftp_default_files[(new Random(Guid.NewGuid().GetHashCode())).Next(ftp_default_files.Count)].FullName);

                //write default txt to file.


                var guid = Guid.NewGuid().GetHashCode().ToString();

                //File.WriteAllText(folder_path + "default_reply_" + guid.Substring(guid.Length - 3) + ".txt", default_txt);

                IGHelpers.Helpers.FTP_WriteTextToFile(ftp_default_txt, ftp_folder_path + guid.Substring(guid.Length - 3) + ".txt");

                goto GetReplies;
            }

            return IGHelpers.Helpers.FTP_TextFileContent(ftp_default_reply_paths[(new Random()).Next(ftp_default_reply_paths.Count)].FullName);
        }

        static private string GenerateDefaultContent(string deviceName)
        {
        DefaultTextFiles:
            var ftp_default_text_folder = ADBSupport.LDHelpers.ftp_dnplayerdata + deviceName + "/system";


            //DirectoryInfo dinfo_default_text_fold = new DirectoryInfo(default_text_folder);
            //FileInfo[] default_txt_files = dinfo_default_text_fold.GetFiles("defaulttext_post*.txt");

            var ftp_default_txt_files = IGHelpers.Helpers.FTP_GetFileListing(ftp_default_text_folder, "defaulttext_post");


            //Check if default content generate
            if (ftp_default_txt_files.Count() == 0)
            {
                //If not generated

                var ftp_default_text_path = ADBSupport.LDHelpers.ftp_dnplayerdata + deviceName + "/system/defaulttext_post.txt";

                //DirectoryInfo dinfo_default = new DirectoryInfo(IGHelpers.Helpers.default_post_text_path);
                //FileInfo[] default_files = dinfo_default.GetFiles("*.txt");

                var ftp_default_files = IGHelpers.Helpers.FTP_GetFileListing(IGHelpers.Helpers.ftp_default_post_text_path, ".txt");

                var ftp_default_txt = IGHelpers.Helpers.FTP_TextFileContent(ftp_default_files[(new Random(Guid.NewGuid().GetHashCode())).Next(ftp_default_files.Count)].FullName);

                //write default txt to file
                IGHelpers.Helpers.FTP_WriteTextToFile(ftp_default_txt, ftp_default_text_path);

                //File.WriteAllText(default_text_path, default_txt);
                goto DefaultTextFiles;
            }

            var choiced_default_txt = (new Random()).Next(ftp_default_txt_files.Count());

            return SharpSpin.Spinner.Spin(IGHelpers.Helpers.FTP_TextFileContent(ftp_default_txt_files[choiced_default_txt].FullName));

        }

        static private string GetWatermark(string deviceName)
        {
            //var default_watermark_folder = ADBSupport.LDHelpers.LDPlayerDATA_path + "\\" + deviceName + "\\system";

            var ftp_default_watermark_folder = ADBSupport.LDHelpers.ftp_dnplayerdata + deviceName + "/system";

            //DirectoryInfo dinfo_watermark_fold = new DirectoryInfo(default_watermark_folder);
            //FileInfo[] default_watermark = dinfo_watermark_fold.GetFiles("watermark_default.png");

            //Check if default content generate
            if (!IGHelpers.Helpers.FTP_IsFileExisted(ftp_default_watermark_folder+ "/watermark_default.png"))
            {
                //If not generated


                //DirectoryInfo dinfo_default = new DirectoryInfo(IGHelpers.Helpers.watermark_path);
                //FileInfo[] default_files = dinfo_default.GetFiles("*_watermark_*.png");

                var ftp_default_files = IGHelpers.Helpers.FTP_GetFileListing(IGHelpers.Helpers.ftp_watermark_path, "_watermark_");

                var choiced = ftp_default_files[(new Random()).Next(ftp_default_files.Count)];

                //copy to deviceName watermark folder

                //File.Copy(choiced.FullName, default_watermark_folder + "\\watermark_default.png");

                IGHelpers.Helpers.FTP_CopyFile(choiced.FullName, ftp_default_watermark_folder);

                //dinfo_watermark_fold.GetFiles("watermark_*.png");
            }

            var local_watermark = "Temp\\" + deviceName + "\\watermark_default.png";

            IGHelpers.Helpers.FTP_DownloadFile(ftp_default_watermark_folder + "/watermark_default.png", local_watermark);

            return local_watermark;

        }

        static private string GetWatermarkContent(string deviceName)
        {
            //var default_watermark_folder = ADBSupport.LDHelpers.LDPlayerDATA_path + "\\" + deviceName + "\\system";

            var ftp_default_watermark_folder = ADBSupport.LDHelpers.ftp_dnplayerdata + deviceName + "/system";


            //DirectoryInfo dinfo_default_watermark_fold = new DirectoryInfo(default_watermark_folder);
            //FileInfo[] default_txt_files = dinfo_default_watermark_fold.GetFiles("defaulttext_watermark*.txt");


            //Check if default content generate
            if (!IGHelpers.Helpers.FTP_IsFileExisted(ftp_default_watermark_folder+ "/defaulttext_watermark.txt"))
            {
                //If not generated

                var default_text_path = ADBSupport.LDHelpers.ftp_dnplayerdata + deviceName + "/system/defaulttext_watermark.txt";

                //DirectoryInfo dinfo_default = new DirectoryInfo(IGHelpers.Helpers.ftp_watermark_content_path);
                //FileInfo[] default_files = dinfo_default.GetFiles("*.txt");

                var default_files = IGHelpers.Helpers.FTP_GetFileListing(IGHelpers.Helpers.ftp_watermark_content_path, ".txt");

                var choiced = default_files[(new Random(Guid.NewGuid().GetHashCode())).Next(default_files.Count)];

                IGHelpers.Helpers.FTP_CopyFile(choiced.FullName, ftp_default_watermark_folder + "/defaulttext_watermark.txt");
                
            }

            var text = IGHelpers.Helpers.FTP_TextFileContent(ftp_default_watermark_folder + "/defaulttext_watermark.txt");

            return SharpSpin.Spinner.Spin(text);
        }

        static private dynamic ReadPostSettings(string settings_str)
        {
            List<string> settings = settings_str.Split(new string[] { "***" }, StringSplitOptions.None).ToList<string>();

            List<string> watermark_zone = settings.Where(s => s.Contains("watermark_zone|")).ToList<string>();

            return new
            {
                watermark_zone
            };
        }

        #endregion

        #region --Update profile, bio--

        static public void DoUpdateProfile(IGDevice iGDevice, dynamic ig_account)
        {
            string deviceID = iGDevice.deviceID;

            //get imagepath and bio

            String searchFolder = IGHelpers.Helpers.folder_profile;
            var filters = new String[] { "jpg", "jpeg", "png", "gif", "tiff", "bmp", "svg" };
            var files = GetFilesFrom(searchFolder, filters, false).ToList<string>();

            var imagepath = files[(new Random()).Next(files.Count)];

            //get bio

            //get ads text

            DirectoryInfo dinfo_ads = new DirectoryInfo(IGHelpers.Helpers.folder_bio + "\\ads");
            FileInfo[] ads_files = dinfo_ads.GetFiles("*.txt");

            //get default text

            DirectoryInfo dinfo_default = new DirectoryInfo(IGHelpers.Helpers.folder_bio + "\\default");
            FileInfo[] default_files = dinfo_default.GetFiles("*.txt");

        //Choose random ads and default text
        RandomText:
            var ads_txt = File.ReadAllText(ads_files[(new Random(Guid.NewGuid().GetHashCode())).Next(ads_files.Length)].FullName);
            var default_txt = File.ReadAllText(default_files[(new Random(Guid.NewGuid().GetHashCode())).Next(default_files.Length)].FullName);

            //var biotext = ads_txt + "\r\n" + default_txt;

            var biotext = default_txt;

            if (biotext.Length > 145)
                goto RandomText;

            //Write text to data of device


            var guid = Guid.NewGuid().GetHashCode().ToString();


            //var biopath = ADBSupport.LDHelpers.LDPlayerDATA_path + "\\" + ig_account.phone_device + "\\temp_img\\biotext_" + guid.Substring(guid.Length - 6) + ".txt";

            //ADBSupport.ADBHelpers.CreatePath(Path.GetDirectoryName(biopath));

            //File.WriteAllText(biopath, biotext);

            var task = new IGTasks.UpdateProfileTask(iGDevice, imagepath, biotext);
            task.DoTask();

        }


        public static String[] GetFilesFrom(String searchFolder, String[] filters, bool isRecursive)
        {
            List<String> filesFound = new List<String>();
            var searchOption = isRecursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            foreach (var filter in filters)
            {
                filesFound.AddRange(Directory.GetFiles(searchFolder, String.Format("*.{0}", filter), searchOption));
            }
            return filesFound.ToArray();
        }

        #endregion

        #region --Do semiauto_comment--

        static public void DoSemiAuto_Comment(IGDevice iGDevice, dynamic ig_account, string task_value)
        {
            string deviceID = iGDevice.deviceID;


            //get semi auto comment


            dynamic comment = IGHelpers.DBHelpers.GetSemiAutoCommentTask(task_value);  //{ link, text }

            var task = new IGTasks.SemiAutoCommentTask(iGDevice, ig_account, comment);
            task.DoTask();
        }


        #endregion

        #region --SetPrivacyThread--

        static public void DoSetPrivacy(IGDevice iGDevice, bool seton)
        {
            var task = new IGTasks.SetPrivacyTask(iGDevice, seton);
            task.DoTask();


            //Add action log
            IGHelpers.DBHelpers.AddActionLog(iGDevice.ld_devicename, "SetPrivacy");
        }

        #endregion

        #region --Backup Full - igdata and device information -

        static public void DoTitanBackup(IGDevice iGDevice, dynamic ig_account)
        {

            string deviceID = iGDevice.deviceID;

            //Close and reenable IG

            

            //Check if Titanium is installed, if not install

            //go home phone

            KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_HOME); Thread.Sleep(5000);

            //Check if Titanium icon is visible

            //bool installtitanium = false;
            //bool first_install = false;

            //while (true)
            //{
            //    try
            //    {
            //        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.TitaniumIcon, 1, 5, 0);
            //        break;
            //    }
            //    catch
            //    {
            //        if (!installtitanium)
            //        {
            //            var titaniumbackup_path = "Temp\\" + ig_account.phone_device + "\\system\\titaniumbackup.apk";
            //            File.Copy("titaniumbackup.apk", titaniumbackup_path, true);
            //            ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + ADBSupport.LDHelpers.LDPlayer_path + " \r\n ldconsole installapp " + ADBSupport.LDHelpers.NameIndex(iGDevice.ld_devicename) + "--filename \"" + titaniumbackup_path + "\"");
            //            installtitanium = true;
            //            first_install = true;
            //        }
            //    }
            //    Thread.Sleep(5000);
            //}


            //if (first_install)
            //{
            //    ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell monkey -p com.keramidas.TitaniumBackup -c android.intent.category.LAUNCHER 1");

            //    try
            //    {
            //        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Remember_choice_forever, 1, 20, 0);

            //        ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.Remember_choice_forever, ADBSupport.BMPSource.AllowRoot, 1);

            //        ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.AllowRoot);
            //    }
            //    catch { }

            //    //Wait allow media
            //    try
            //    {
            //        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.First_Allow, 5, 5, 5);

            //        ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.First_Allow, ADBSupport.BMPSource.First_Allow, 1);
            //        ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.First_Allow, ADBSupport.BMPSource.First_Allow, 1);
            //        ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.First_Allow, ADBSupport.BMPSource.First_Allow, 1);
            //    }
            //    catch
            //    { }

            //    try
            //    {
            //        //Wait TitaniumBackupAddon ask

            //        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.TitaniumBackupAddOn, 2, 5, 3);

            //        //Click cancel
            //        ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.CancelAddOn, ADBSupport.BMPSource.Backup_Restore, 1);
            //    }
            //    catch
            //    { }

            //    try
            //    {
            //        //Wait ask install Addon

            //        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.First_TitaniumAddOn, 2, 0, 5);
            //        KAutoHelper.ADBHelper.TapByPercent(deviceID, 65.5, 95.0);
            //    }
            //    catch { }

            //    //Wait first start
            //    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.FirstStart_Ok, 2, 5, 5);
            //    ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.FirstStart_Ok);

            //    //Wait changelog
            //    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.First_ChangeLog, 2, 5, 5);
            //    KAutoHelper.ADBHelper.TapByPercent(deviceID, 82.1, 96.1);

            //    //Warning
            //    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.First_Warning, 2, 5, 5);

            //    KAutoHelper.ADBHelper.TapByPercent(deviceID, 39.3, 65.3); ADBSupport.ADBHelpers.Delay();
            //    KAutoHelper.ADBHelper.TapByPercent(deviceID, 50.3, 72.1); ADBSupport.ADBHelpers.Delay();

            //    //Hit backup and restore

            //    ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.Backup_Restore, ADBSupport.BMPSource.First_EditFilter, 1);

            //    //Click edit filter

            //    ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.First_EditFilter, ADBSupport.BMPSource.First_CreateLabel, 1);

            //    //Click create label


            //    ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.First_CreateLabel, ADBSupport.BMPSource.First_WaitLabelPopup, 1);

            //    //Clear default label

            //    for (int d = 0; d < 12; d++)
            //    {
            //        ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell input keyevent KEYCODE_FORWARD_DEL");
            //    }

            //    while (true)
            //    {
            //        try
            //        {
            //            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.First_WaitClearBox, 1, 1);
            //            break;
            //        }
            //        catch
            //        {
            //            KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_DEL);
            //        }
            //        Thread.Sleep(500);
            //    }

            //    ADBSupport.ADBHelpers.InputTextWithDelay(deviceID, "IG");

            //    //Click add remove element

            //    ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.AddRemoveElements);

            //    //Scroll to IG

            //    ADBSupport.ADBHelpers.ScrollDownToImage(deviceID, 79.9, ADBSupport.BMPSource.First_Instagram138);

            //    //Click IG

            //    ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.First_Instagram138);

            //    //Click save

            //    KAutoHelper.ADBHelper.TapByPercent(deviceID, 92.0, 8.3);

            //    //Click save again

            //    KAutoHelper.ADBHelper.TapByPercent(deviceID, 26.9, 67.8);

            //    //Click checkbox

            //    KAutoHelper.ADBHelper.TapByPercent(deviceID, 6.6, 86.8);


            //    //Click save

            //    KAutoHelper.ADBHelper.TapByPercent(deviceID, 91.7, 7.3);

            //    Thread.Sleep(5000);

            //}

            ADBSupport.TitaniumBackupHelpers.BackUpIG(iGDevice.deviceID, LDHelpers.LDPlayer_dnplayerdata + ig_account.phone_device + "\\system\\backup\\" + DateTime.Now.ToShortDateString().Replace("/", "-"), ig_account.user);

            //Backup system information
            //ADBSupport.LDHelpers.BackUpDevice(ig_account.phone_device);

            IGHelpers.DBHelpers.AddActionLog(iGDevice.ld_devicename, "titanbackup");
        }

        #endregion

        #region --Restore device--

        static public void DoRestore(string deviceName)
        {
            //Read device info from deviceinfo.ig

            dynamic deviceinfo = IGHelpers.DBHelpers.GetDeviceInformation(deviceName);

            //IGDevice iGDevice = new IGDevice(device.fullname, null, device.phone_device, new LDDevice(device.phone_device));

            //string deviceID = iGDevice.deviceID;


            //Create device with settings in destination location

            ADBSupport.LDHelpers.CloneDevice(deviceName);

            //Change device information --Restore Offline

            ADBSupport.LDHelpers.RestoreDevice(deviceName, deviceinfo);

            //Open and Get deviceID

            //string deviceID = ADBSupport.LDHelpers.LaunchLDPlayer(deviceName, false);

            //Restore hardserial

            //ADBSupport.LDHelpers.RestoreLDOnline(deviceName, "ro.serialno", deviceinfo.hard_serial);

            //Set locale region

            //ADBSupport.ADBHelpers.SetLocaleRegion(deviceID);


            //Restore device masker information

            //var generate_device_info = new Action.GenerateRandomDeviceInfo(deviceID, deviceName, "", ADBSupport.LDHelpers.LDPlayerDATA_path + "\\" + deviceName + "\\system\\" + deviceName + ".txt");
            //generate_device_info.DoAction();

            //deviceID = ADBSupport.LDHelpers.LaunchLDPlayer(deviceName, false);

            //Check if IG is installed, if not -> install

            //try
            //{
            //    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_IconApp, 2, 4, 1);
            //}
            //catch
            //{
            //    //Install ig
            //    IGHelpers.IGADBHelpers.InstallIG(deviceID, deviceName);
            //    KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_HOME);
            //}

            //Import ig
            //ADBSupport.TitaniumBackupHelpers.RestoreIG(deviceID, ADBSupport.LDHelpers.GetLatestTitaniumBackupFolder(deviceName));

            //ADBSupport.LDHelpers.RestoreApp(deviceName, "com.instagram.android", deviceID);

            //go home

            //KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_HOME);
        }

        #endregion

        #region --Account Error Handle--

        static public void Do_UnsualActivityAction(string deviceName)
        {
            IGHelpers.DBHelpers.AddActionLog(deviceName, "Unsual Activity");

            IGHelpers.DBHelpers.UpdateStateofIG(deviceName, "Unsual");
        }

        static public void Do_DISABLE_Action(string deviceName)
        {
            IGHelpers.DBHelpers.AddActionLog(deviceName, "DISABLE");

            IGHelpers.DBHelpers.UpdateStateofIG(deviceName, "DISABLE");
        }

        static public void Do_ErrorRequest_Login(string deviceName)
        {
            IGHelpers.DBHelpers.AddActionLog(deviceName, "Error Request");

            IGHelpers.DBHelpers.UpdateStateofIG(deviceName, "Error Request");
        }

        #endregion

        #region --DoNormalUser--

        static public void DoNormalUser_UpdateProfile(IGDevice iGDevice, dynamic ig_account)
        {
            //get source profile

            string source_profile = IGHelpers.DBHelpers.GetSourceUser(ig_account.id);

            //get profile image and bio

            dynamic profile = null;

            var dyn = IGHelpers.Helpers.APIObJWithProxy();

            profile = IGAPIHelper.Scraper.ScrapeProfileBio(dyn.instaApi, source_profile);


            var status = ADBHelpers.GetDynamicProperty(profile, "status").ToString();
            if (status != "Success")
            {

                //Check if source user error
                var userinfo = IGAPIHelper.Scraper.ScrapeFullUserInfo_ByUsername(source_profile, dyn.instaApi);

                var userinfo_status = ADBHelpers.GetDynamicProperty(userinfo, "status");

                if (userinfo_status == "NotFoundUser")
                {
                    IGHelpers.DBHelpers.UpdateSourceUser(source_profile, "error");
                    return;
                }
                else
                    throw new System.ArgumentException("UnknowError_ScrapeProfileBio" + status);
            }

            //download image to device data path

            //XEM LAI, NEU LAY LINK NAY THI CHUA CHINH SUA EXIF


            var imagepath = "Temp\\" + ig_account.phone_device + "\\temp_img\\" + "IMG_" + DateTime.Now.Ticks.ToString() + ".jpg";
            DownloadImage(ADBHelpers.GetDynamicProperty(profile, "profile_img"), imagepath);


            //create bio txt in device data path


            //var guid = Guid.NewGuid().GetHashCode().ToString();

            //var biopath = ADBSupport.LDHelpers.LDPlayerDATA_path + "\\" + ig_account.phone_device + "\\temp_img\\biotext_" + guid.Substring(guid.Length - 6) + ".txt";

            //File.WriteAllText(biopath, userinfo.bio);

            var task = new IGTasks.UpdateProfileTask(iGDevice, imagepath, ADBHelpers.GetDynamicProperty(profile, "bio"));
            task.DoTask();

            //Add action log with target_account value
            IGHelpers.DBHelpers.AddActionLog(iGDevice.ld_devicename, "updateprofile", "", source_profile);
        }

        static public void DoNormalUser_Publish(IGDevice iGDevice, dynamic ig_account, dynamic setting, string added_task)
        {
            //get source profile

            string source_profile = IGHelpers.DBHelpers.GetSourceUser(ig_account.id);


            //get post to repost

            //get list posts

            InstagramApiSharp.Classes.Models.InstaMedia instaMedia = null;

            //int retry = 0;
            //ScrapeMedias:
            var dyn = IGHelpers.Helpers.APIObJWithProxy();

            InstagramApiSharp.Classes.Models.InstaMediaList medias = new InstagramApiSharp.Classes.Models.InstaMediaList();

            int retry = 0;


        ScrapeMediasFromUser:
            IResult<InstagramApiSharp.Classes.Models.InstaMediaList> medias_result = IGAPIHelper.Scraper.ScrapeMediasFromUser(dyn.instaApi, source_profile);


            if (medias_result.Succeeded == false)
            {
                //check if user is remove
                var userinfo = IGAPIHelper.Scraper.ScrapeFullUserInfo_ByUsername(source_profile, dyn._instaApi);

                var userinfo_status = ADBHelpers.GetDynamicProperty(userinfo, "status");

                if (userinfo_status == "NotFoundUser")
                {
                    IGHelpers.DBHelpers.UpdateSourceUser(source_profile, "error");
                    return;
                }
                else
                {
                    retry++;
                    if (retry < 3)
                    {
                        Thread.Sleep(2000);
                        goto ScrapeMediasFromUser;
                    }
                    else
                    {
                        throw new System.ArgumentException("UnknownError|" + userinfo_status);
                    }
                }
            }
            else
            {
                medias = medias_result.Value;

                if (medias.Count == 0)
                {
                    throw new System.ArgumentException("UnknownWhyMediasIsEmpty");
                }
            }
            //get medias id that not published

            foreach (var item in medias)
            {
                if (!IGHelpers.DBHelpers.IsPublishedThisPost(ig_account.id, item.Code))
                {
                    instaMedia = item;
                    break;
                }
            }

            if (instaMedia == null)
            {
                IGHelpers.DBHelpers.AddActionLog(iGDevice.ld_devicename, "published_post", "");
                goto SetNextPublish;
            }


            //tam thoi chi lay image đầu tien

            string imageurl = "";

            //check if has carousel
            if (instaMedia.Carousel != null)
            {
                imageurl = instaMedia.Carousel[0].Images[0].Uri;
            }
            else
            {
                imageurl = instaMedia.Images[0].Uri;
            }


            //image of post

            var imagepath = "Temp\\" + ig_account.phone_device + "\\temp_img\\" + "IMG_" + DateTime.Now.Ticks.ToString() + ".jpg";

            DownloadImage(imageurl, imagepath);


            //Write to file and save to deviceName data

            //REMOVE TAGS FROM CAPTION

            var caption = "";

            if (instaMedia.Caption != null)
                caption = instaMedia.Caption.Text.Replace("@", "");

            //check if need do added task

            if (added_task == "caption")
            {
                if (setting["DoNormalUser_Publish"]["caption"]["text"] != null)
                {
                    var added_text = IGHelpers.DBHelpers.ReplaceValueWithExtInfo(ig_account, setting["DoNormalUser_Publish"]["caption"]["text"]);

                    if (added_text != "")
                    {
                        caption = caption + "\r\n" + added_text;
                    }
                }
            }

            var guid = Guid.NewGuid().GetHashCode().ToString();
            //var content_path = ADBSupport.LDHelpers.LDPlayerDATA_path + "\\" + ig_account.phone_device + "\\temp_img\\contentpost_" + guid.Substring(guid.Length - 6) + ".txt";

            //File.WriteAllText(content_path, caption);

            //Publish

            var task = new IGTasks.PublishPostTask(iGDevice, new List<string>() { imagepath }, caption);
            task.DoTask();

            //add log with value

            IGHelpers.DBHelpers.AddActionLog(iGDevice.ld_devicename, "published_post", instaMedia.Code);

            SetNextPublish:

            var next_publish_post = DateTime.Now.AddMinutes((new Random()).Next(720, 1440));

            //set next publish
            IGHelpers.DBHelpers.AddActionLog(iGDevice.ld_devicename, "next_publish_post", next_publish_post.ToString());

        }

        #region --Publish Stories--

        static public void DoNormalUser_PublishStories(IGDevice iGDevice, dynamic ig_account)
        {
            //get source profile

            string source_profile = IGHelpers.DBHelpers.GetSourceUser(ig_account.id);


            InstagramApiSharp.Classes.Models.InstaStoryItem instaStoryItem = null;

            var dyn = IGHelpers.Helpers.APIObJWithProxy();

            if (dyn == null)
                throw new System.ArgumentException("APIObj is null");

            var stories_result = GrabStories(dyn.instaApi, source_profile);

            List<InstagramApiSharp.Classes.Models.InstaStoryItem> stories = new List<InstagramApiSharp.Classes.Models.InstaStoryItem>();

            if (stories_result.status != "Success")
            {
                //check if user is remove
                var userinfo = IGAPIHelper.Scraper.ScrapeFullUserInfo_ByUsername(source_profile, dyn._instaApi);

                var userinfo_status = ADBHelpers.GetDynamicProperty(userinfo, "status");

                if (userinfo_status == "NotFoundUser")
                {
                    IGHelpers.DBHelpers.UpdateSourceUser(source_profile, "error");
                    return;
                }
                throw new System.ArgumentException(stories_result.status);
            }
            else
            {
                stories = (List<InstagramApiSharp.Classes.Models.InstaStoryItem>)stories_result.stories;
                if (stories == null)
                    return;
            }

            foreach (var item in stories)
            {
                if (!IGHelpers.DBHelpers.IsPublishedThisStory(ig_account.id, item.Code))
                {
                    instaStoryItem = item;
                    break;
                }
            }

            if (instaStoryItem == null)
                return;

            //tam thoi chi lay image đầu tien

            string imageurl = instaStoryItem.ImageList[0].Uri;


            //image of post

            var imagepath = "Temp\\" + ig_account.phone_device + "\\temp_img\\" + "IMG_" + DateTime.Now.Ticks.ToString() + ".jpg";

            DownloadImage(imageurl, imagepath);

            //Publish

            var task = new IGTasks.PublicStoriesTask(iGDevice, imagepath);
            task.DoTask();

            //add log with value

            IGHelpers.DBHelpers.AddActionLog(iGDevice.ld_devicename, "published_stories", instaStoryItem.Code);

        }

        static public dynamic GrabStories(InstagramApiSharp.API.IInstaApi _instaApi, string username)
        {
            Stopwatch watch_timeout = new Stopwatch();
            watch_timeout.Start();


        GrabPK:
            if (watch_timeout.Elapsed.TotalSeconds > 60)
            {
                return new { status = "Timeout" };
            }

            var task_user_detail = Task.Run(() =>
            {
                return _instaApi.UserProcessor.GetUserAsync(username);
            });

            var result = task_user_detail.GetAwaiter().GetResult();

            if (result.Succeeded == false)
            {
                var status = result.Info.Message;

                if (status.Contains("A task was canceled"))
                {
                    Thread.Sleep(TimeSpan.FromSeconds(2));
                    goto GrabPK;
                }
            }

            if (result.Info.Message.Contains("Can't find this user"))
            {
                var status = "NotFoundUser";
                return new
                {
                    status
                };
            }

            long userId = result.Value.Pk;

        UserFullInfo:
            var task_user_fullinfo = Task.Run(() =>
            {
                return _instaApi.UserProcessor.GetFullUserInfoAsync(userId);
            });

            var user_fullinfo_result = task_user_fullinfo.GetAwaiter().GetResult();

            if (user_fullinfo_result.Succeeded)
            {
                return new
                {
                    status = "Success",
                    stories = user_fullinfo_result.Value.ReelFeed.Items
                };
            }
            else
            {
                return new
                {
                    status = user_fullinfo_result.Info.Message
                };
            }
        }

        #endregion

        static public void DownloadImage(string imageurl, string savepath)
        {
            ADBSupport.ADBHelpers.CreatePath(Path.GetDirectoryName(savepath));
            using (var m = new MemoryStream(new WebClient().DownloadData(imageurl)))
            {
                using (var img = Bitmap.FromStream(m))
                {

                    img.Save(savepath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
            }
        }

        #endregion

        #region --Do seeding like--


        static public void DoSeeding_Like(IGDevice iGDevice, string semiauto_comment_postId)
        {
            string deviceID = iGDevice.deviceID;

            //get semi auto comment

            var status = IGHelpers.SeedingHelpers.DoSeedingLike(deviceID, semiauto_comment_postId);

            if(status== "not_found_comment")
            {
                //update status to semiauto_comment_task
                IGHelpers.SeedingHelpers.UpdateSeedingStatus(semiauto_comment_postId, "NotFound");
            }
        }

        #endregion

        #region --DoAutoComment--

        static public void DoAutoComment(IGDevice iGDevice, dynamic ig_account, dynamic comment)
        {
            string deviceID = iGDevice.deviceID;


            //set text comment
            var text = IGHelpers.DBHelpers.GetAutoCommentText();

            var finalcomment = new
            {
                id = comment.id,
                link = comment.link,
                text
            };

            var task = new IGTasks.SemiAutoCommentTask(iGDevice, ig_account, finalcomment);
            task.DoTask();
        }


        #endregion

        #region --CheckUsername is correctly account--


        static public bool IsCorrectAccount(string deviceID, string username)
        {
            var task = new IGTasks.IsCorrectAccountTask(deviceID, username);
            var iscorrect = task.ReturnTask();
            return iscorrect;
        }


        #endregion
    }
}
