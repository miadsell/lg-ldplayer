﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.IGThreads
{
    class EngageThread : IGThread
    {

        List<Thread> threads = new List<Thread>();
        public override void DoThread()
        {
            IGHelpers.DBHelpers.IsNeedGenerateTask();

            //Scan and add required task

            IGHelpers.DBHelpers.ScanRequiredTask();

            threads.Clear();

            for (int t = 0; t < ADBSupport.LDHelpers.LDPlayerThread_Count; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => Engage());
                thread.Name = "igengage" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }

            //Start a thread to clearlog, use thread to pass thread name pattern

            IGHelpers.ProxyHelpers.ProxyServerOthers("igengage", true);


            //Load chrome to tab

            //IGHelpers.Helpers.LaunchDriver();

            foreach (var t in threads)
            {
                t.Start();
            }


            ManageThread();

            //Run ticker

            InitTimer_UpdateProxy();
            InitTimer_UpdateProxyStatus();
            InitTimer_RefreshForm();
            InitTimer_ManageThread();
        }

        private void Engage()
        {
            while (true)
            {

                if (Program.form.isstop)
                    break;

                //Get igaccount to engage

                dynamic ig_account = IGHelpers.DBHelpers.GetAccountToEngage();

                if (ig_account == null)
                {
                    Thread.Sleep(TimeSpan.FromMinutes(5));
                    continue;
                }


                IGDevice iGDevice = new IGDevice(ig_account, new LDDevice(ig_account.phone_device));


                IGThreads.ThreadHelpers.DoEngage(iGDevice, ig_account);


                //Dispose

                iGDevice.Dispose();

                //Unclaimed status

                IGHelpers.DBHelpers.UpdateStatusofIG(ig_account.id, "");

            }
        }

        private void ManageThread()
        {
            int count_running_thread = 0;
            for (int i = 0; i < threads.Count; i++)
            {
                if (threads[i].ThreadState.ToString() == "Running" || threads[i].ThreadState.ToString() == "WaitSleepJoin")
                {
                    count_running_thread++;
                }
            }

            if (count_running_thread == 0)
            {
                timer_UpdateProxy.Stop();
                timer_UpdateProxyStatus.Stop();
                timer_RefreshForm.Stop();
                timer_ManageThread.Stop();

                //Unfreeze form
                Program.form.Invoke((MethodInvoker)delegate
                {
                    Program.form.UnFreezeControl();
                });
            }
        }

        #region --Ticker ManageThread--

        private System.Windows.Forms.Timer timer_ManageThread;

        public void InitTimer_ManageThread()
        {
            timer_ManageThread = new System.Windows.Forms.Timer();
            timer_ManageThread.Tick += new EventHandler(timer_ManageThread_Tick);
            timer_ManageThread.Interval = 5000; // in miliseconds
            timer_ManageThread.Start();

        }

        private void timer_ManageThread_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(new ThreadStart(ManageThread));
            worker.Name = "Ticking Manage Thread";
            worker.IsBackground = true;
            worker.Start();
        }

        #endregion



        
    }
}
