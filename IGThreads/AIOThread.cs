﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.IGThreads
{
    class AIOThread : IGThread
    {
        string error_actionlog = null; //use to track error times of specific account on this run
        List<Thread> threads = new List<Thread>();
        public override void DoThread()
        {
            //generate unique error_actionlog

            error_actionlog = "error_" + Guid.NewGuid().GetHashCode().ToString();

            //IGHelpers.DBHelpers.IsNeedGenerateTask();
            AIOHelper.DBLogic.GenerateStats.Do();
            //Scan specific table and add task to queuetasks if needed
            //IGHelpers.DBHelpers.ScanSpecificTask();
            IGHelpers.DBHelpers.ScanSpecificTask();

            //Scan queue

            //IGTasks.RecurringTasksHelper.ScanRecurringTask();

            threads.Clear();

            var focus_engage = ADBSupport.LDHelpers.LDPlayer_FocusEngage_Threads_Count;
            var focus_mass = ADBSupport.LDHelpers.LDPlayer_FocusMassAction_Threads_Count;

            for (int t = 0; t < ADBSupport.LDHelpers.LDPlayerThread_Count; t++)
            {
                var pos = t;
                var focus = "";

                if(focus_mass>0)
                {
                    focus = "focus_mass";
                    focus_mass--;
                    goto AddNewThread;
                }

                if (focus_engage > 0)
                {
                    focus = "focus_engage";
                    focus_engage--;
                    goto AddNewThread;
                }
                AddNewThread:
                Thread thread = new Thread(() => AIOFunc(focus));
                thread.Name = "igaio" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                thread.IsBackground = true;
                threads.Add(thread);
            }

            //Start a thread to clearlog, use thread to pass thread name pattern

            //IGHelpers.ProxyHelpers.ProxyServerOthers("igaio", true);


            //Load chrome to tab

            //IGHelpers.Helpers.LaunchDriver();

            foreach (var t in threads)
            {
                t.Start();
            }


            ManageThread_AIOProxy();

            //Run ticker

            //itTimer_UpdateProxy();
            //itTimer_UpdateProxyStatus();
            InitTimer_ManageThread();
            InitTimer_GenerateStats();
            InitTimer_MonitorThreadUsage();
            AIOHelper.ThreadHelper.ClearUnusedFileFolderTask.InitTimer_ClearUnusedFF();
        }

        /// <summary>
        /// * AIO function
        //- Check all specific tasks to see if need do task - Done
        //	+ If yes, add to queue task
        //- Check queue task to see if it has task to do
        //	+ If yes
        //        .Get account has queue task
        //		. Get all queue tasks of that account that meet requirement about schedule time
        //        .Return list
        //		. Execute tasks



        //	+ If not
        //		- Run likeposts if it suitable 
        //			else
        //	 	- Run engage task
        /// </summary>

        private void AIOFunc(string focus = "")
        {
            while (true)
            {

                Stopwatch watch_takeaccount = new Stopwatch();
                watch_takeaccount.Start();

                string thread_type = null;

                bool second_check_flag = false; //use when focus_engage, if engage return null, try to get account from another

                if (Program.form.isstop)
                    break;

                string do_thread_name = "";

                switch (focus)
                {
                    case "focus_engage":
                        goto DoNormalUserPost;
                    case "focus_mass":
                        goto DoMassAction;
                    default: break;
                }

            StartGetAccount:


                dynamic ig_account = null;



                //get queuetask account
                DoQueueTask:
                //ig_account = IGHelpers.DBHelpers.GetAccountToQueueTask();

                ig_account = AIOHelper.DBLogic.GetAccount_DoSpecificTask.GetAccount_DoQueueTask();

                if (ig_account != null)
                {
                    //do queue task
                    do_thread_name = "DoQueueTask";
                    thread_type = "action_light";
                    goto DoThread;

                }

                //get random DoFollowingUser vs DoLikeUserPost first
                DoMassAction:
                

                List<string> random_orders = new List<string>() { "DoFollowingUser", "DoLikeUserPost", "DoMassDM", "DoCommentPost" };

                random_orders = random_orders.OrderBy(x => Guid.NewGuid().GetHashCode()).ToList();

                foreach (var item in random_orders)
                {
                    switch (item)
                    {
                        case "DoMassDM":
                            //do massdm

                            ig_account = AIOHelper.DBLogic.GetAccount_DoSpecificTask.GetAccount_DoSpecificTask_MassDM();

                            if (ig_account != null)
                            {
                                //do like post
                                do_thread_name = "DoMassDM";
                                thread_type = "action_mass";
                                goto DoThread;
                            }
                            break;
                        case "DoCommentPost":
                            //commentpost
                            //ig_account = AIOHelper.DBLogic.GetAccount_DoSpecificTask.GetAccount_DoSpecificTask_CommentPost();

                            //if (ig_account != null)
                            //{
                            //    //do like post
                            //    do_thread_name = "DoCommentPost";
                            //    thread_type = "action_mass";
                            //    goto DoThread;
                            //}
                            break;
                        case "DoFollowingUser":
                            //DoFollowingUser:
                            //ig_account = IGHelpers.DBHelpers.GetAccountDoSpecificTasks("FollowingUser");
                            ig_account = AIOHelper.DBLogic.GetAccount_DoSpecificTask.GetAccount_DoSpecificTask_FollowingUser();

                            if (ig_account != null)
                            {
                                //do like post
                                do_thread_name = "DoFollowingUser";
                                thread_type = "action_mass";
                                goto DoThread;
                            }
                            break;
                        case "DoLikeUserPost":
                            //if null, get like post
                            //DoLikeUserPost:
                            //ig_account = IGHelpers.DBHelpers.GetAccountDoSpecificTasks("LikeUserPost");
                            //ig_account = AIOHelper.DBLogic.GetAccount_DoSpecificTask.GetAccount_DoSpecificTask_LikingUserPost();

                            //if (ig_account != null)
                            //{
                            //    //do like post
                            //    do_thread_name = "DoLikeUserPost";
                            //    thread_type = "action_mass";
                            //    goto DoThread;
                            //}
                            break;
                    }

                }


                //if null, get normal user to do some task (change profile, bio)
                DoNormalUserPost:
                ig_account = AIOHelper.DBLogic.GetAccount_DoSpecificTask.GetAccount_DoNormalUserTask();

                if (ig_account != null)
                {
                    //do like post
                    do_thread_name = "DoNormalUserPost";
                    thread_type = "action_light";
                    goto DoThread;
                }

                //if null, do engage
                DoEngage:
                ig_account = AIOHelper.DBLogic.GetAccount_DoSpecificTask.GetAccount_DoEngage();

                if (ig_account != null)
                {
                    //do engage
                    do_thread_name = "DoEngage";
                    thread_type = "action_light";
                    goto DoThread;
                }

                //dung cho cac thread focus, neu nhu k lay duoc data thi chay 1 vong nua, neu van k co thi sleep
                if (focus != "" && !second_check_flag)
                {
                    second_check_flag = true;
                    goto StartGetAccount;

                }

            //if null, do backup
            //DoTitanBackup:
            //    ig_account = IGHelpers.DBHelpers.GetAccountDoSpecificTasks("Backup");

            //    if (ig_account != null)
            //    {
            //        //do engage
            //        do_thread_name = "DoTitanBackup";
            //        goto DoThread;
            //    }

                Thread.Sleep(TimeSpan.FromMinutes(5));
                continue;


            DoThread:

                double time_takeaccount = watch_takeaccount.Elapsed.TotalSeconds;

                IGHelpers.DBHelpers.AddActionLogByID(ig_account.id, "doaction_takeaccount_time",time_takeaccount.ToString() + "|" + do_thread_name);

                //add actionlog to handle thread_running

                AIOHelper.ThreadHelper.MonitorThreadUsage.AddThread_RunningLog(ig_account.id);

                //set thread_type

                Thread.SetData(Thread.GetNamedDataSlot("thread_type"), thread_type);

                Thread.SetData(Thread.GetNamedDataSlot("deviceName"), ig_account.phone_device);

                //add do_thread_name to log

                IGHelpers.DBHelpers.AddActionLogByID(ig_account.id, "do_thread_name", do_thread_name);

                if (Program.form.isstop)
                    break;

                IGDevice iGDevice = null;

                iGDevice = new IGDevice(ig_account, new LDDevice(ig_account.phone_device),true,Program.form.skip_launch_error);

                if (Program.form.isstop)
                {
                    if (iGDevice.switch_device != null)
                        goto IGDeviceDispose;
                    break;
                }

                if (!iGDevice.islogin)
                {
                    goto IGDeviceDispose;
                }


                Stopwatch watch_time_on_instagram = new Stopwatch();
                watch_time_on_instagram.Start();

                if (!Program.form.skip_error)
                {
                    DoAIOAction(do_thread_name, iGDevice, ig_account);


                    //Unclaimed status

                    IGHelpers.DBHelpers.UpdateStatusofIG(ig_account.id, "");

                    if (do_thread_name != "DoEngage")
                    {
                        //add time to current_timespend db
                        IGHelpers.DBHelpers.AddTimeToCurrent_TimeSpend(ig_account.id, watch_time_on_instagram.Elapsed.TotalMinutes);
                    }

                }
                else
                {
                    try
                    {
                        DoAIOAction(do_thread_name, iGDevice, ig_account);


                        //Unclaimed status

                        IGHelpers.DBHelpers.UpdateStatusofIG(ig_account.id, "");

                        if (do_thread_name != "DoEngage")
                        {
                            //add time to current_timespend db
                            IGHelpers.DBHelpers.AddTimeToCurrent_TimeSpend(ig_account.id, watch_time_on_instagram.Elapsed.TotalMinutes);
                        }

                    }
                    catch(Exception ex)
                    {
                        //add error actionlog
                        IGHelpers.DBHelpers.AddActionLogByID(ig_account.id, error_actionlog);

                        if(ex.Message.Contains("UnsualActivity"))
                        {
                            IGThreads.ThreadHelpers.Do_UnsualActivityAction(ig_account.phone_device);
                        }

                        //check current error times
                        var error_times = IGHelpers.DBHelpers.ErrorTimesOfThisRun(ig_account.id, error_actionlog);

                        if(error_times<3)
                        {
                            //clear claim status so it can be continue to run in this run

                            //Unclaimed status

                            IGHelpers.DBHelpers.UpdateStatusofIG(ig_account.id, "");
                        }

                        //log trace and screen of ldplayer to db
                        //var screenshoot = ADBSupport.ADBHelpers.ScreenShoot(iGDevice.deviceID);

                        //string error_path = "";
                        //if (screenshoot != null)
                        //{
                        //    error_path = "ErrorScreenShoot\\error_" + Guid.NewGuid().GetHashCode().ToString().Replace("-", "") + ".jpg";

                        //    ADBSupport.ADBHelpers.CreatePath("ErrorScreenShoot");

                        //    screenshoot.Save(error_path, ImageFormat.Jpeg);
                        //}

                        ADBSupport.LDHelpers.HandleException(iGDevice.ld_devicename, do_thread_name + "|" + ex.Message, ex.StackTrace);

                        //check if keep_ld_open

                        if (Program.form.keep_ldplayer_open)
                        {
                            break; //stop thread and keep ldplayer open
                        }
                    }
                }

                //After do task (unclaimed account,....)

                watch_time_on_instagram.Stop();

            //Dispose
            IGDeviceDispose:
                iGDevice.Dispose();

                //Unset thread

                AIOHelper.ThreadHelper.MonitorThreadUsage.DeleteThread_Running(ig_account.id);

                Thread.SetData(Thread.GetNamedDataSlot("thread_type"), null);


                Thread.SetData(Thread.GetNamedDataSlot("deviceName"), null);

            }
        }

        private void DoAIOAction(string do_thread_name, IGDevice iGDevice, dynamic ig_account)
        {
            //switch case do thread

            switch (do_thread_name)
            {
                case "DoQueueTask":
                    DoQueue(iGDevice, ig_account);
                    break;
                case "DoMassDM":
                    DoMassDM(iGDevice, ig_account);
                    break;
                case "DoCommentPost":
                    DoCommentPost(iGDevice, ig_account);
                    break;
                case "DoFollowingUser":
                    DoFollowingUser(iGDevice, ig_account);
                    break;
                case "DoLikeUserPost":
                    DoLikeUserPost(iGDevice, ig_account);
                    break;
                case "SimulateStories":
                    DoSimulateStories(iGDevice, ig_account);
                    break;
                case "DoNormalUserPost":
                    DoNormalUserTask(iGDevice, ig_account);
                    break;
                case "DoEngage":
                    DoEngage(iGDevice, ig_account);
                    break;
                case "DoTitanBackup":
                    DoTitanBackup(iGDevice, ig_account);
                    break;
                default:
                    throw new System.ArgumentException("Wrong do_thread_name property");
            }
        }


        private void DoEngage(IGDevice iGDevice, dynamic ig_account)
        {
            IGThreads.ThreadHelpers.DoEngage(iGDevice, ig_account);

        }

        private void DoMassDM(IGDevice iGDevice, dynamic ig_account)
        {
            var task = new IGTasks.MassDMTask(iGDevice, ig_account);
            task.DoTask();
        }

        private void DoCommentPost(IGDevice iGDevice, dynamic ig_account)
        {
            //var comment = IGHelpers.DBHelpers.GetAutoCommentTask(ig_account.id);
            //IGThreads.ThreadHelpers.DoAutoComment(iGDevice, ig_account, comment);

            var task = new IGTasks.CommentPostTask(iGDevice, ig_account);
            task.DoTask();
        }

        private void DoTitanBackup(IGDevice iGDevice, dynamic ig_account)
        {
            IGThreads.ThreadHelpers.DoTitanBackup(iGDevice, ig_account);

        }

        private void DoFollowingUser(IGDevice iGDevice, dynamic ig_account)
        {
            var task = new IGTasks.FollowingUserTask(iGDevice, ig_account);
            task.DoTask();
        }

        private void DoLikeUserPost(IGDevice iGDevice, dynamic ig_account)
        {
            var task = new IGTasks.LikeUserPostTask(iGDevice, ig_account);
            task.DoTask();
        }

        private void DoSimulateStories(IGDevice iGDevice, dynamic ig_account)
        {
                var task = new IGTasks.SimulateUserStoriesTask(iGDevice, ig_account);
                task.DoTask();
        }

        private void DoNormalUserTask(IGDevice iGDevice, dynamic ig_account)
        {
            List<string> done_tasks = new List<string>();
            //get tasks
            GetNormalUserTask:
            string task = IGHelpers.DBHelpers.GetNormalUserTask(ig_account.id);


            if (task == "")
                return;

            if(done_tasks.Contains(task))
            {
                return;
            }

            switch (task)
            {
                case "publish":
                    DoNormalUser_Publish(iGDevice, ig_account);

                    break;
                case "updateprofile":
                    DoNormalUser_UpdateProfile(iGDevice, ig_account);

                    break;

                case "publish_stories":
                    DoNormalUser_PublishStories(iGDevice, ig_account); //sau khi lam stories thi return de tranh truong hop k co stories để publish sẽ lặp forever
                    return;

                default:
                    throw new System.ArgumentException("Not found this task type");
            }

            done_tasks.Add(task);

            goto GetNormalUserTask;
        }


        #region --DoQueue--

        private void DoQueue(IGDevice iGDevice, dynamic ig_account)
        {

            AIOHelper.ThreadHelper.DoQueueTask.Run(iGDevice, ig_account);

        }

        private string DoQueue_PublishPost(IGDevice iGDevice, dynamic ig_account, dynamic qtask)
        {
            string status = IGThreads.ThreadHelpers.DoPublishPost(iGDevice, ig_account, qtask.value);

            if (status == "OutOfPost")
                return "Done";

            if (status != "Done")
                return "";

            //get value of task
            string settings = qtask.value;

            //check if comment of self post
            try
            {
                if (settings.Contains("comment|true"))
                {
                    var selfcomment = new IGTasks.SelfCommentTask(iGDevice, ThreadHelpers.GenerateSelfComment(iGDevice.ld_devicename));
                    selfcomment.DoTask();
                }
            }
            catch (Exception ex)
            {
                //log trace and screen of ldplayer to db
                //var screenshoot = ADBSupport.ADBHelpers.ScreenShoot(iGDevice.deviceID);


                //var error_path = "Temp\\error_" + Guid.NewGuid().GetHashCode().ToString().Replace("-", "") + ".jpg";

                //screenshoot.Save(error_path, ImageFormat.Jpeg);

                ADBSupport.LDHelpers.HandleException(iGDevice.ld_devicename, "SelfComment Post" + "|" + ex.Message, ex.StackTrace);
                //check if keep_ld_open
            }

            try
            {
                if (settings.Contains("stories|true"))
                {
                    var selfshare_stories = new IGTasks.SelfShareStoriesTask(iGDevice);
                    selfshare_stories.DoTask();
                }
            }
            catch (Exception ex)
            {
                //log trace and screen of ldplayer to db
                //var screenshoot = ADBSupport.ADBHelpers.ScreenShoot(iGDevice.deviceID);


                //var error_path = "Temp\\error_" + Guid.NewGuid().GetHashCode().ToString().Replace("-", "") + ".jpg";

                //screenshoot.Save(error_path, ImageFormat.Jpeg);

                ADBSupport.LDHelpers.HandleException(iGDevice.ld_devicename, "Self Share Stories" + "|" + ex.Message, ex.StackTrace);
                //check if keep_ld_open
            }

            return "Done";

        }

        private void DoQueue_UpdateProfile_Bio(IGDevice iGDevice, dynamic qtask)
        {
            var biotext = ADBSupport.ADBHelpers.Base64Decode(qtask.value);

            Action.UpdateBio updateBio = new Action.UpdateBio(iGDevice, biotext);
            updateBio.DoAction();
        }

        private void DoQueue_UpdateProfile_Image(IGDevice iGDevice, dynamic qtask)
        {
            Bitmap bitmap_image = Action.ActionHelpers.BitmapFromBase64Str(qtask.value);

            var guid = Guid.NewGuid().GetHashCode().ToString();

            var image_path = "Temp\\" + iGDevice.ld_devicename + "\\temp_img\\" + guid.Substring(guid.Length - 3) + ".jpg";

            ADBSupport.ADBHelpers.CreatePath(Directory.GetParent(image_path).FullName); //create path if not

            bitmap_image.Save(image_path, System.Drawing.Imaging.ImageFormat.Jpeg);

            var modify_img = IGHelpers.ExifHelpers.OptimizeImage_FTPServer(iGDevice.ld_devicename, image_path);

            Action.UpdateProfileImage updateProfileImage = new Action.UpdateProfileImage(iGDevice, modify_img);
            updateProfileImage.DoAction();
        }

        private void DoQueue_UpdateProfile_Fullname()
        {

        }

        #endregion

        #region --DoNormalUser--

        private static void DoNormalUser_Publish(IGDevice iGDevice, dynamic ig_account)
        {
            AIOHelper.ThreadHelper.DoNormalUser_Publish.Run(iGDevice, ig_account);
            ////get settings

            //var setting = IGHelpers.Settings.Settings_GetSettingByName(ig_account.id, "DoNormalUser_Publish");

            //string choiced_task = null;

            //if (setting != null)
            //{
            //    List<string> added_tasks = new List<string>();

            //    if (setting["DoNormalUser_Publish"]["comment"] != null && setting["DoNormalUser_Publish"]["comment"]["turn_on"].ToString() == "True")
            //        added_tasks.Add("comment");
            //    if (setting["DoNormalUser_Publish"]["watermark"] != null && setting["DoNormalUser_Publish"]["watermark"]["turn_on"].ToString() == "True")
            //        added_tasks.Add("watermark");
            //    if (setting["DoNormalUser_Publish"]["watermark"] != null && setting["DoNormalUser_Publish"]["caption"]["turn_on"].ToString() == "True")
            //        added_tasks.Add("caption");

            //    choiced_task = added_tasks[(new Random()).Next(added_tasks.Count)];
            //}

            //IGThreads.ThreadHelpers.DoNormalUser_Publish(iGDevice, ig_account, setting, choiced_task);

            //if (choiced_task == "comment")
            //{
            //    //check if post was published in last 10 mins
            //    if (IGHelpers.DBHelpers.IsPublishedPostInLastXMinutes(ig_account.id, 10))
            //    {

            //        if (setting["DoNormalUser_Publish"]["comment"]["text"] != null)
            //        {
            //            var comment = IGHelpers.DBHelpers.ReplaceValueWithExtInfo(ig_account, setting["DoNormalUser_Publish"]["comment"]["text"].ToString());
            //            if (comment != "")
            //            {
            //                var selfcomment = new IGTasks.SelfCommentTask(iGDevice, comment);
            //                selfcomment.DoTask();
            //            }
            //        }
            //    }
            //}
        }


        private static void DoNormalUser_UpdateProfile(IGDevice iGDevice, dynamic ig_account)
        {
            IGThreads.ThreadHelpers.DoNormalUser_UpdateProfile(iGDevice, ig_account);
        }

        private static void DoNormalUser_PublishStories(IGDevice iGDevice, dynamic ig_account)
        {
            IGThreads.ThreadHelpers.DoNormalUser_PublishStories(iGDevice, ig_account);
        }

        #endregion

        //private void ManageThread()
        //{
        //    int count_running_thread = 0;
        //    for (int i = 0; i < threads.Count; i++)
        //    {
        //        if (threads[i].ThreadState.ToString() == "Running" || threads[i].ThreadState.ToString() == "WaitSleepJoin")
        //        {
        //            count_running_thread++;
        //        }
        //    }

        //    if (count_running_thread == 0)
        //    {
        //        timer_UpdateProxy.Stop();
        //        timer_UpdateProxyStatus.Stop();
        //        timer_ManageThread.Stop();

        //        //Unfreeze form
        //        Program.form.Invoke((MethodInvoker)delegate
        //        {
        //            Program.form.UnFreezeControl();
        //        });
        //    }
        //}

        private void ManageThread_AIOProxy()
        {
            int count_running_thread = 0;
            for (int i = 0; i < threads.Count; i++)
            {
                if (threads[i].ThreadState.ToString() == "Running" || threads[i].ThreadState.ToString() == "WaitSleepJoin" || threads[i].ThreadState.ToString() == "Background")
                {
                    count_running_thread++;
                }
            }

            if (count_running_thread == 0)
            {
                timer_ManageThread.Stop();

                //Unfreeze form
                Program.form.Invoke((MethodInvoker)delegate
                {
                    Program.form.UnFreezeControl();
                });
            }
        }

        #region --Ticker ManageThread--

        private System.Windows.Forms.Timer timer_ManageThread;

        public void InitTimer_ManageThread()
        {
            timer_ManageThread = new System.Windows.Forms.Timer();
            timer_ManageThread.Tick += new EventHandler(timer_ManageThread_Tick);
            timer_ManageThread.Interval = 5000; // in miliseconds
            timer_ManageThread.Start();

        }

        private void timer_ManageThread_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(new ThreadStart(ManageThread_AIOProxy));
            worker.Name = "Ticking Manage Thread";
            worker.IsBackground = true;
            worker.Start();
        }

        #endregion


        #region --Ticking Scan Task--

        private System.Windows.Forms.Timer timer_ScanTask;

        public void InitTimer_ScanTask()
        {
            timer_ScanTask = new System.Windows.Forms.Timer();
            timer_ScanTask.Tick += new EventHandler(timer_ScanTask_Tick);
            timer_ScanTask.Interval = 10 * 60 * 1000; // in miliseconds -> 10 minutes
            timer_ScanTask.Start();

        }

        private void timer_ScanTask_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(new ThreadStart(ScanTask));
            worker.Name = "Ticking Scan Task";
            worker.IsBackground = true;
            worker.Start();
        }

        private void ScanTask()
        {
            //Scan specific table and add task to queuetasks if needed
            IGHelpers.DBHelpers.ScanSpecificTask();

            //Scan queue

            IGTasks.RecurringTasksHelper.ScanRecurringTask();

            //Scan seeding like

            //IGHelpers.SeedingHelpers.ScanSeedingLike();
        }

        #endregion

        #region --Ticking Generate Engage Stats--

        private System.Windows.Forms.Timer timer_GenerateEngageStats;

        public void InitTimer_GenerateStats()
        {
            timer_GenerateEngageStats = new System.Windows.Forms.Timer();
            timer_GenerateEngageStats.Tick += new EventHandler(timer_GenerateEngageStats_Tick);
            timer_GenerateEngageStats.Interval = 120 * 60 * 1000; // in miliseconds -> 2 hours
            timer_GenerateEngageStats.Start();

        }

        private void timer_GenerateEngageStats_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(new ThreadStart(GenerateStats));
            worker.Name = "Ticking Generate Stats Task";
            worker.IsBackground = true;
            worker.Start();
        }

        private void GenerateStats()
        {
            //IGHelpers.DBHelpers.IsNeedGenerateTask();
            AIOHelper.DBLogic.GenerateStats.Do();
        }

        #endregion

        #region --Ticking Monitor Thread--

        private System.Windows.Forms.Timer timer_MonitorThreadUsage;

        public void InitTimer_MonitorThreadUsage()
        {
            timer_MonitorThreadUsage = new System.Windows.Forms.Timer();
            timer_MonitorThreadUsage.Tick += new EventHandler(timer_MonitorThreadUsage_Tick);
            timer_MonitorThreadUsage.Interval = 5 * 60 * 1000; // in miliseconds -> 5 minutes
            timer_MonitorThreadUsage.Start();

        }

        private void timer_MonitorThreadUsage_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(new ThreadStart(AIOHelper.ThreadHelper.MonitorThreadUsage.ExecuteMonitor));
            worker.Name = "Ticking Monitor Thread Usage Task";
            worker.IsBackground = true;
            worker.Start();
        }

        #endregion
    }
}
