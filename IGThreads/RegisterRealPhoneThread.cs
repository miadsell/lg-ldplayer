﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.IGThreads
{
    class RegisterRealPhoneThread:IGThread
    {

        List<string> deviceIds;
        string phone_service;

        public RegisterRealPhoneThread(List<string> deviceIds, string phone_service)
        {
            this.deviceIds = deviceIds;
            this.phone_service = phone_service;
        }

        List<Thread> threads = new List<Thread>();
        public override void DoThread()
        {
            //Clear status when start

            IGHelpers.DBHelpers.RealPhoneReg_ClearStatus();

            threads.Clear();


            for (int t = 0; t < deviceIds.Count; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => Register(deviceIds[pos]));
                thread.Name = "register" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }
            foreach (var t in threads)
            {
                t.Start();
            }


            ManageThread();

            InitTimer_ManageThread();
        }

        int continue_askcaptcha = 0;
        int error = 0;
        private void Register(string deviceID)
        {

            while (true)
            {
                //get information to reg
                var account = IGHelpers.DBHelpers.RealPhoneReg_GetAccountToReg();

                if (error > 20)
                    break;

                if (account == null)
                    break;

                dynamic success_account = null;

                if (Program.form.register_skiperror)
                {
                    try
                    {
                        var reg = new IGTasks.RegisterRealPhoneTask(deviceID, account, phone_service);
                        success_account = reg.DoRegister();
                    }
                    catch(Exception ex)
                    {
                        error++;
                        ADBSupport.LDHelpers.HandleException("LDPlayer-IG-155", "register_realphone|" + ex.Message, ex.StackTrace);
                    }
                }
                else
                {
                    var reg = new IGTasks.RegisterRealPhoneTask(deviceID, account, phone_service);
                    success_account = reg.DoRegister();
                }

                if (success_account == null)
                    continue;


                if (success_account.statuscode == "AskCaptcha")
                {
                     ADBSupport.LDHelpers.HandleException("LDPlayer-IG-155", "register_realphone|AskCaptcha", null);
                    continue_askcaptcha++;
                    if (continue_askcaptcha > 3) //ask captcha 3 lan lien tiep
                    {
                        throw new System.ArgumentException("Ask Captcha Continously");
                    }
                    continue;
                }


                //update information to db

                IGHelpers.DBHelpers.RealPhoneReg_Success_UpdateInformation(success_account);

                continue_askcaptcha = 0;
            }


        }

        private void ManageThread()
        {
            int count_running_thread = 0;
            for (int i = 0; i < threads.Count; i++)
            {
                if (threads[i].ThreadState.ToString() == "Running" || threads[i].ThreadState.ToString() == "WaitSleepJoin")
                {
                    count_running_thread++;
                }
            }

            if (count_running_thread == 0)
            {
                timer_ManageThread.Stop();

                //Unfreeze form
                Program.form.Invoke((MethodInvoker)delegate
                {
                    Program.form.UnFreezeControl();
                });
            }
        }

        #region --Ticker ManageThread--

        private System.Windows.Forms.Timer timer_ManageThread;


        public void InitTimer_ManageThread()
        {
            timer_ManageThread = new System.Windows.Forms.Timer();
            timer_ManageThread.Tick += new EventHandler(timer_ManageThread_Tick);
            timer_ManageThread.Interval = 5000; // in miliseconds
            timer_ManageThread.Start();

        }

        private void timer_ManageThread_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(new ThreadStart(ManageThread));
            worker.Name = "Ticking Manage Thread";
            worker.IsBackground = true;
            worker.Start();
        }

        #endregion
    }
}
