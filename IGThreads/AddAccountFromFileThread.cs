﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.IGThreads
{
    class AddAccountFromFileThread : IGThread
    {
        List<Thread> threads = new List<Thread>();
        public override void DoThread()
        {
            threads.Clear();

            for (int t = 0; t < ADBSupport.LDHelpers.LDPlayerThread_Count; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => AddAccount());
                thread.Name = "igaddaccount" + pos.ToString();
                threads.Add(thread);
            }

            //IGHelpers.ProxyHelpers.ProxyServerOthers("igaddaccount", true);

            foreach (var t in threads)
            {
                t.Start();
            }


            ManageThread();

            //InitTimer_UpdateProxy();
            //InitTimer_UpdateProxyStatus();
            InitTimer_RefreshForm();
            InitTimer_ManageThread();
        }

        //first import all account information to db, set default status NOTLOGIN
        //Clone device for all account NOTLOGIN and phone_device is null
        //Start login, if login succesfull, set state to ACTIVE
        public string add_mode = null;


        public AddAccountFromFileThread(string add_mode)
        {
            if(add_mode==null)
            {
                throw new System.ArgumentException("Please choose mode");
            }
            this.add_mode = add_mode;
        }

        private void AddAccount()
        {
            switch(add_mode)
            {
                case "1_1":
                    //AddAccount1_1();
                    break;
                case "switch":
                    AddSwitchAccount();
                    break;
                default:
                    throw new System.ArgumentException("Not Found This Type");
            }
        }

        //private void AddAccount1_1()
        //{
        //    while (true)
        //    {

        //        if (Program.form.isstop)
        //            break;

        //        //get account, device need to login
        //        dynamic account = IGHelpers.DBHelpers.GetAccountToLogin("1-1");

        //        if (account == null)
        //            break;

        //        string deviceName = account.phone_device;

        //        string accountId = account.id;

        //        //launch device

        //        string deviceID = ADBSupport.LDHelpers.LaunchLDPlayer(deviceName);

                

        //        try
        //        {
        //            //login to account
        //            var login = new Action.Login(deviceID, account.user, account.password);
        //            login.DoAction();

        //            //Update state of account to db

        //            IGHelpers.DBHelpers.UpdateStateofIG(deviceName, "ACTIVE");

        //            IGHelpers.DBHelpers.UpdateStatusofIG(accountId, "");
        //        }
        //        catch(Exception ex)
        //        {

        //        }
        //        finally
        //        {
        //            //Close device
        //            ADBSupport.LDHelpers.CloseDevice(deviceName);

        //            //IGHelpers.ViewDeviceHelpers.RemovePanel(deviceName);

        //            IGHelpers.ProxyHelpers.ClearLogProxy();
        //        }

        //        ADBSupport.ADBHelpers.Delay(60, 120); //delay to prevent blacklist ip range
        //    }
        //}


        private void AddSwitchAccount()
        {

            while (true)
            {

                //get switch device

                var switch_device = ADBSupport.LDHelpers.ReturnSwitchDeviceAvailable();



                Thread.SetData(ADBSupport.LDHelpers._ld_index, switch_device.index);

                if (Program.form.isstop)
                    break;

                //get account, device need to login
                dynamic account = AIOHelper.DBLogic.GetAccount_DoSpecificTask.GetAccount_DoSpecificTask_GenerateDevice_AIO();

                if (account == null)
                    break;


                string deviceName = "LDPlayer-IG-" + account.id;

                string accountId = account.id;

                //change switch device information

                ADBSupport.LDHelpers.GenerateDeviceInfo(deviceName);

                ADBSupport.LDHelpers.SetPhoneNumber(deviceName, account.phonenum);

                //launch device

                string deviceID = ADBSupport.LDHelpers.LaunchLDPlayer(deviceName, false);


                //clear ig app data
                ADBSupport.ADBHelpers.ClearAPPData(deviceID, "com.instagram.android");

                //clear download folder
                var command = ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell \"su -c 'rm -r sdcard/Download/*'\"");

                //login

                //try
                //{
                    //login to account
                    ///WARNING: IF RUN LOGIN, PLEASE SET USERPROXY TRUE WHEN LAUNCH DEVICE ABOVE
                    //var login = new Action.Login(deviceID, account.user, account.password);
                    //login.DoAction();

                    //after login

                    //Add and save device information to database and save deviceinfo.ig to device path

                    //Add device to DB

                    IGHelpers.DBHelpers.AddDevice(deviceName, "");

                    using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                    {
                        conn.Open();
                        //Update device to ig_account

                        using (SqlCommand sql_command = new SqlCommand("update ig_account set phone_device='" + deviceName + "', [status]='' where id=" + account.id, conn))
                        {
                            sql_command.ExecuteNonQuery();
                        }

                        //Backup device

                        var deviceinfo = ADBSupport.LDHelpers.BackUpDevice(deviceName);

                        var command_update = "update ldplayers set manufacturer='" + deviceinfo.manufacturer + "',model='" + deviceinfo.model + "',imei='" + deviceinfo.imei + "',imsi='" + deviceinfo.imsi + "',simserial='" + deviceinfo.simserial + "',androidid='" + deviceinfo.androidid + "',hard_serial='" + deviceinfo.hard_serial + "', mac='" + deviceinfo.mac + "',pnumber='" + account.phonenum + "', location='" + ADBSupport.LDHelpers.LDPlayer_path + "' where ld_name='" + deviceName + "'";

                        using (SqlCommand sql_command = new SqlCommand(command_update, conn))
                        {
                            sql_command.ExecuteNonQuery();
                        }
                    }



                    ////Titanium backup
                    ////Close app

                    //ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell pm disable com.instagram.android");

                    ////Re-enable

                    //ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell pm enable com.instagram.android");

                    //ADBSupport.TitaniumBackupHelpers.BackUpIG(deviceID, ADBSupport.LDHelpers.ftp_dnplayerdata + deviceName + "/system/backup/" + DateTime.Now.ToShortDateString().Replace("/", "-"), account.user);

                    ////clear ig app data
                    //ADBSupport.ADBHelpers.ClearAPPData(deviceID, "com.instagram.android");

                    ////clear download folder
                    //ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell \"su -c 'rm -r sdcard/Download/*'\"");


                    //Update state of account to db

                    //IGHelpers.DBHelpers.UpdateStateofIG(deviceName, "PAUSED");

                    IGHelpers.DBHelpers.UpdateStatusofIG(accountId, "");

                    IGHelpers.DBHelpers.UpdateModeofIG(accountId, "switch");


            //}
            //    catch (Exception ex)
            //{

            //}
            //finally
            //{
                //Close device
                ADBSupport.LDHelpers.CloseDevice(deviceName);

                    //IGHelpers.ViewDeviceHelpers.RemovePanel(deviceName);

                    IGHelpers.ProxyHelpers.ClearLogProxy();


                    IGHelpers.DBHelpers.SetSwitchDeviceStatus(switch_device.index, "");
                }

                //ADBSupport.ADBHelpers.Delay(60, 120);

            //}

        }



        #region --Ticker ManageThread--

        private System.Windows.Forms.Timer timer_ManageThread;

        public void InitTimer_ManageThread()
        {
            timer_ManageThread = new System.Windows.Forms.Timer();
            timer_ManageThread.Tick += new EventHandler(timer_ManageThread_Tick);
            timer_ManageThread.Interval = 5000; // in miliseconds
            timer_ManageThread.Start();

        }

        private void timer_ManageThread_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(new ThreadStart(ManageThread));
            worker.Name = "Ticking Manage Thread";
            worker.IsBackground = true;
            worker.Start();
        }

        private void ManageThread()
        {
            int count_running_thread = 0;
            for (int i = 0; i < threads.Count; i++)
            {
                if (threads[i].ThreadState.ToString() == "Running" || threads[i].ThreadState.ToString() == "WaitSleepJoin")
                {
                    count_running_thread++;
                }
            }

            if (count_running_thread == 0)
            {
                timer_RefreshForm.Stop();
                timer_ManageThread.Stop();

                //Unfreeze form
                Program.form.Invoke((MethodInvoker)delegate
                {
                    Program.form.UnFreezeControl();
                });
            }
        }

        #endregion

    }
}
