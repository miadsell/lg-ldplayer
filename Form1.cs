﻿using InstagramApiSharp.Classes;
using InstagramLDPlayer.ADBSupport;
using InstagramLDPlayer.IGHelpers;
using InstagramLDPlayer.IGTasks;
using InstagramLDPlayer.IGThreads;
using Ionic.Zip;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();


            InitTimer_Monitor_AllDevices();

            InitTimer_Monitor_ADBNotACK();


        }

        private void Form1_Shown(object sender, EventArgs e)
        {

            Thread thread = new Thread(() => SettingUpFromStart());
            thread.Start();
        }

        #region --Settingup Tool--

        private void SettingUpFromStart()
        {

            WriteLog("Start setting up! Please wait....");


            WriteLog("Read app settings.....");
            ReadAppSettings();

            WriteLog("Generate Off Day for accounts....");
            GenerateOffDay();

            //Check required library
            WriteLog("Export source....");
            ExportExSource();

            //Clear temp and deviceID folder

            WriteLog("Clear Temp Folder...");
            ClearTempFolder();


            //Setting constants

            WriteLog("Setting Up LDConstants...");
            IGHelpers.DBHelpers.SettingUpLDConstants();

            WriteLog("Get current pc information....");
            pc_devices = IGHelpers.DBHelpers.CheckDevice();

            label_pc_devices.Invoke((MethodInvoker)delegate
            {
                label_pc_devices.Text = pc_devices;
            });


            //Setting up proxy

            IGHelpers.ProxyHelpers.DeleteLogProxy();
            IGHelpers.ProxyHelpers.UpdateCurrentUsed();

            //Close all ldplayer devices
            //Thread thread_quitall = new Thread(() => ADBSupport.LDHelpers.QuitAllDevices());
            //thread_quitall.Start();

            //Clear status

            WriteLog("Clear status of ig account...");
            IGHelpers.DBHelpers.UpdateStatusofIG();

            //Clear status of scrape users
            WriteLog("Clear status of scrapeusers...");
            IGHelpers.DBHelpers.ClearStatusOfUserScrape();

            //Clear status of commentposts
            WriteLog("Clear status of commentposts...");
            IGHelpers.DBHelpers.ClearStatusOfCommentPost();

            //Clear status of commentposts scrapeusers
            WriteLog("Clear status of commentposts of scrapeusers...");
            IGHelpers.DBHelpers.ClearStatusOfCommentPost_ForScrapeUsers();

            //Clear queue task
            WriteLog("Clear status of queuetasks....");
            IGHelpers.DBHelpers.ClearQueueTask();

            //Clear status of semiautocomment
            WriteLog("Clear status of semiautocomment...");
            IGHelpers.DBHelpers.ClearStatusOfSemiAutoTask();

            //Clear thread_running actionlog
            WriteLog("Clear thread_running log....");
            AIOHelper.ThreadHelper.MonitorThreadUsage.ClearThreadRunningLog();



            WriteLog("Load Bitmap...");
            ADBSupport.IGScreen.LoadScreen(); //load bitmap

            //After load setting block

            WriteLog("Scan switchdevice...");
            ADBSupport.LDHelpers.ScanSwitchDevice();

            //Check if generate token for gdrive
            WriteLog("Scan gdrive token.....");
            PLAutoHelper.GDriveAPI.GDrive_ListFiles();


            //PAUSE PERMISSION HANDLE
            //List<string> alltabs = new List<string>() { "igaccounts", "slaves" };

            //if (ADBSupport.LDHelpers.LDPlayer_permission == "")
            //{
            //    ReloadManageOLV();
            //}

            ////Just shown screen match permission
            //List<string> tabs = ADBSupport.LDHelpers.LDPlayer_permission.Split('|').ToList<string>();

            //var remove_tabs = alltabs.Except(tabs).ToList();

            //foreach (var item in remove_tabs)
            //{
            //    switch (item)
            //    {
            //        case "igaccounts":
            //            tabControl1.Invoke((MethodInvoker)delegate
            //            {
            //                tabControl1.TabPages.RemoveByKey("tabPage_igaccounts");
            //            });
            //            break;
            //    }
            //}

            //Validate aio_settings

            WriteLog("Validate aio settings....");
            var notvalidate_accounts = AIOHelper.ThreadHelper.AIOSettings_Validation.Do();

            if (notvalidate_accounts.Count > 0)
                WriteLog("accountId\tmessage");

            foreach (var a in notvalidate_accounts)
            {
                WriteLog(a.id + "\t" + a.message);
            }


            RefreshForm();

            WriteLog("FINISHED SETTING UP");

            InitTimer_AutoRun_Counter();
        }


        private void ClearStatusClaimedBy_Pc_Devices_ID(string choiced_pc_devices)
        {
            //Setting up proxy

            IGHelpers.ProxyHelpers.DeleteLogProxy(null, choiced_pc_devices);
            IGHelpers.ProxyHelpers.UpdateCurrentUsed();

            //Clear status

            WriteLog("Clear status of ig account...");
            IGHelpers.DBHelpers.UpdateStatusofIG(null, "", choiced_pc_devices);

            //Clear status of scrape users
            WriteLog("Clear status of scrapeusers...");
            IGHelpers.DBHelpers.ClearStatusOfUserScrape(false, choiced_pc_devices);

            //Clear status of commentposts
            WriteLog("Clear status of commentposts...");
            IGHelpers.DBHelpers.ClearStatusOfCommentPost(false, choiced_pc_devices);

            //Clear status of commentposts scrapeusers
            WriteLog("Clear status of commentposts of scrapeusers...");
            IGHelpers.DBHelpers.ClearStatusOfCommentPost_ForScrapeUsers(choiced_pc_devices);

            //Clear queue task
            WriteLog("Clear status of queuetasks....");
            IGHelpers.DBHelpers.ClearQueueTask(false, choiced_pc_devices);

            //Clear status of semiautocomment
            WriteLog("Clear status of semiautocomment...");
            IGHelpers.DBHelpers.ClearStatusOfSemiAutoTask(false, choiced_pc_devices);

            //Clear thread_running actionlog
            WriteLog("Clear thread_running log....");
            AIOHelper.ThreadHelper.MonitorThreadUsage.ClearThreadRunningLog(choiced_pc_devices);

        }

        private void WriteLog(string log)
        {
            tb_log.Invoke((MethodInvoker)delegate
            {
                tb_log.AppendText(log + "\r\n");
            });
        }

        /// <summary>
        /// read for setting database connection, ftp_server,....
        /// </summary>
        public void ReadAppSettings()
        {
            var app_settings_json = File.ReadAllText("app.ld");

            var dynamic = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(app_settings_json);

            string server_ip = dynamic["server_ip"];
            string instagramdb_name = dynamic["instagramdb_name"];
            string igtoolkitdb_name = dynamic["igtoolkitdb_name"];
            string ftp_server_host = dynamic["ftp_server_host"];
            string project_name = dynamic["project"];

            label_project_name.Invoke((MethodInvoker)delegate
            {
                label_project_name.Text = project_name;
            });

            //settingup db connection
            IGHelpers.DBHelpers.db_connection = IGHelpers.DBHelpers.db_connection.Replace("@server_ip", server_ip).Replace("@instagramdb", instagramdb_name);
            IGHelpers.DBHelpers.db_connection_igtoolkit = IGHelpers.DBHelpers.db_connection_igtoolkit.Replace("@server_ip", server_ip).Replace("@igtoolkit", igtoolkitdb_name);

            ADBSupport.LDHelpers.ftp_server_host = ftp_server_host;

            foreach (var item in dynamic["proxy_server_ips"])
            {
                ADBSupport.LDHelpers.proxy_server_ips.Add(item["ip"].ToString());
            }


            IGHelpers.ExifHelpers.lat_from = double.Parse((string)dynamic["lat_from"]);
            IGHelpers.ExifHelpers.lat_to = double.Parse((string)dynamic["lat_to"]);
            IGHelpers.ExifHelpers.long_from = double.Parse((string)dynamic["long_from"]);
            IGHelpers.ExifHelpers.long_to = double.Parse((string)dynamic["long_to"]);
        }

        static public void GenerateOffDay()
        {
            //IGHelpers.DBHelpers.IsNeedGenerateOFFDay();
            AIOHelper.DBLogic.GenerateOffDay.Do();
        }

        #region --Setting Up Static Variable--

        public string pc_devices;

        #endregion

        #region --Include require DLL--

        /// <summary>
        /// export dll and imagesource
        /// </summary>
        private void ExportExSource()
        {
            //remove old file

            //File.Delete("imagesource.zip");
            //File.Delete("dllsource.zip");

            //File.WriteAllBytes("imagesource.zip", Resources.ExSource.ImageSource);
            //File.WriteAllBytes("dllsource.zip", Resources.ExSource.DLLSource);

            //Unzip ImageSource

            //using (ZipFile zip = ZipFile.Read("imagesource.zip"))
            //{

            //    ADBHelpers.CreatePath("ImageSource");
            //    zip.ExtractAll(Path.GetFullPath("ImageSource"), ExtractExistingFileAction.OverwriteSilently);
            //}

            ////Unzip DLLSource and copy to executable folder

            //using (ZipFile zip = ZipFile.Read("dllsource.zip"))
            //{
            //    zip.ExtractAll("", ExtractExistingFileAction.OverwriteSilently);
            //}

            string[] files = System.IO.Directory.GetFiles("DLLSource");

            // Copy the files and overwrite destination files if they already exist.
            foreach (string s in files)
            {
                // Use static Path methods to extract only the file name from the path.
                var fileName = System.IO.Path.GetFileName(s);
                var destFile = System.IO.Path.Combine("", fileName);
                System.IO.File.Copy(s, destFile, true);
            }
        }

        #endregion

        #region --Clear Temp and DeviceID folder--

        private void ClearTempFolder()
        {
            var temp_folder = AppDomain.CurrentDomain.BaseDirectory + "Temp";
            var deviceid_folder = AppDomain.CurrentDomain.BaseDirectory + "DeviceID";

            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("rmdir " + temp_folder + " /s /q");
            command = ADBSupport.ADBHelpers.ExecuteCMDTask("rmdir " + deviceid_folder + " /s /q");

            Directory.CreateDirectory(temp_folder);
            Directory.CreateDirectory(deviceid_folder);
        }


        #endregion

        #endregion

        #region --Prepare, Load, Filter form--

        private void Form1_Load(object sender, EventArgs e)
        {

            this.WindowState = FormWindowState.Maximized;
            this.MinimumSize = this.Size;
            this.MaximumSize = this.Size;


        }



        #endregion

        #region --Auto Run--

        private System.Windows.Forms.Timer timer_autorun;

        int counter_auto_run = 10;

        protected void InitTimer_AutoRun_Counter()
        {
            timer_autorun = new System.Windows.Forms.Timer();
            timer_autorun.Tick += new EventHandler(timer_autorun_Tick);
            timer_autorun.Interval = 1000;

            this.Invoke(new MethodInvoker(delegate ()
            {
                timer_autorun.Start();
            }));

        }

        private void timer_autorun_Tick(object sender, EventArgs e)
        {
            counter_auto_run--;
            if (counter_auto_run == 0)
            {
                timer_autorun.Stop();
                AutoRun();
            }
            label_autorun_counter.Text = counter_auto_run.ToString();

            //Thread worker = new Thread(new ThreadStart(Program.form.UpdateRunningStatus));
            //worker.Name = "Monitor Devices Thread";
            //worker.IsBackground = true;
            //worker.Start();
        }

        private void AutoRun()
        {
            //run proxy server
            button_run_xproxy_aio.PerformClick();

            //run tasking
            btn_tasking.PerformClick();
        }


        private void button_stop_autorun_Click(object sender, EventArgs e)
        {
            timer_autorun.Stop();
        }

        #endregion

        #region --Monitor all pc devices--


        protected System.Windows.Forms.Timer timer_MonitorDevices;

        protected void InitTimer_Monitor_AllDevices()
        {
            timer_MonitorDevices = new System.Windows.Forms.Timer();
            timer_MonitorDevices.Tick += new EventHandler(timer_MonitorDevices_Tick);
            timer_MonitorDevices.Interval = 5 * 60 * 1000; // in miliseconds -> 5 minutes
            timer_MonitorDevices.Start();

        }

        private void timer_MonitorDevices_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(new ThreadStart(Program.form.UpdateRunningStatus));
            worker.Name = "Monitor Devices Thread";
            worker.IsBackground = true;
            worker.Start();
        }

        //if an account was claim manual and the status of device not update longer 10 minutes, release claimed manual device

        private void UpdateRunningStatus() //update last check running every 5 minutes
        {
            IGHelpers.DBHelpers.UpdateLastRunning();

            //Clear status of account Claimed Manual By device_id in device that aren't running

            //TAM PAUSED THIS FUNC
            //IGHelpers.DBHelpers.ClearStatusOfAccountsClaimedByStopDevice();
        }

        #endregion

        #region --Monitor failed to start daemon--

        protected System.Windows.Forms.Timer timer_MonitorADBNotACK;

        protected void InitTimer_Monitor_ADBNotACK()
        {
            timer_MonitorADBNotACK = new System.Windows.Forms.Timer();
            timer_MonitorADBNotACK.Tick += new EventHandler(timer_ADBNotACK_Tick);
            timer_MonitorADBNotACK.Interval = 5 * 60 * 1000; // in miliseconds -> 60 minutes
            timer_MonitorADBNotACK.Start();

        }

        private void timer_ADBNotACK_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(new ThreadStart(MonitorADBDidNOTACK));
            worker.Name = "Monitor ADB Didn't ACK Thread";
            worker.IsBackground = true;
            worker.Start();
        }

        private void MonitorADBDidNOTACK()
        {
            var command = ADBHelpers.ExecuteCMDTask("adb devices");

            if (command.Contains("daemon not running"))
            {
                Thread.Sleep(10000);
                command = ADBHelpers.ExecuteCMDTask("adb devices");

                if (command.Contains("daemon not running"))
                    ADBHelpers.KillAllADBProcesses();
            }
        }


        #endregion

        #region --Filter--

        List<dynamic> filter_condition = new List<dynamic>();

        private string GenerateFilterStr()
        {
            string filter_str = "";
            for (int i = 0; i < filter_condition.Count; i++)
            {
                filter_str += filter_condition[i].name + "='" + filter_condition[i].value + "'";
                if (i < filter_condition.Count - 1)
                    filter_str += " AND ";
            }

            return filter_str;
        }

        #endregion

        #region --Register Tab--

        private void btn_register_Click(object sender, EventArgs e)
        {

            isstop = false;
            FreezeControl();
            //var reg = new IGThreads.RegisterThread();
            //reg.DoThread();

            //IGHelpers.PhoneHelpers.RequestPhone_RentCode();

            //string deviceID = null;
            //var listDevice = KAutoHelper.ADBHelper.GetDevices();
            //if (listDevice != null && listDevice.Count > 0)
            //{
            //    deviceID = listDevice.First();
            //}

            //IGHelpers.IGADBHelpers.GetUsername(null);

            //var point = KAutoHelper.ADBHelper.FindImage(deviceID, @"DeviceID\1270015571\637233219004600231.png");

            ////ADBSupport.LDHelpers.BackUpDevice("LDPlayer-3");

            ////ADBSupport.LDHelpers.BackUpDevice("LDPlayer_dream.clothes");

            //ADBSupport.LDHelpers.RestoreApp("LDPlayer - ORIGIN-5", "com.instagram.android", @"C:\Users\luan\source\repos\InstagramLDPlayer\bin\Debug\LDData\LDPlayer_dream.clothes\ig.vmdk");

            //var action = new Action.Register(deviceID);
            //action.DoAction();
        }

        #region --Test multi thread update sql server--


        private void TestUpdate()
        {
            List<Thread> threads = new List<Thread>();
            for (int t = 0; t < 500; t++)
            {
                Thread thread = new Thread(() => IGHelpers.DBHelpers.UpdateStatusIgToolKitUser(44744031559, "status", SqlDbType.VarChar, ""));
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }

            Stopwatch watch = new Stopwatch();
            watch.Start();

            foreach (var t in threads)
            {
                t.Start();
            }

            foreach (var t in threads)
            {
                t.Join();
            }

            var finished = watch.Elapsed.TotalSeconds;

            Console.WriteLine(finished.ToString());
        }

        #endregion

        private void Remove_After_Completed_AddFollowBackToActionLog()
        {
            //get list accountId has turn on following
            List<string> accountIds = new List<string>();

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                string command_query =
                    @"select *
                    from specifictasks
                    where
	                    [followinguser]='Do'";

                using(var command=new SqlCommand(command_query,conn))
                {
                    using(var dataReader=command.ExecuteReader())
                    {
                        while(dataReader.Read())
                        {
                            accountIds.Add(dataReader["accountId"].ToString());
                        }
                    }
                }
            }
            List<string> followback_users = new List<string>();
            //get list followback from igtoolkit
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                string command_query =
                    @"select *
                    from scrapeusers
                    where
	                    [followback]='yes'";

                using (var command = new SqlCommand(command_query, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            followback_users.Add(dataReader["UserName"].ToString());
                        }
                    }
                }
            }

            //check each accountIds

            foreach (var id in accountIds)
            {
                List<string> following_target_accounts = new List<string>();
                //check all target_account following by this accountId, if followback, add to actionlog
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    string command_query =
                        @"select *
                        from actionlog
                        where
	                        [action]='following_task' and
	                        [accountid]=@id";

                    using (var command=new SqlCommand(command_query,conn))
                    {
                        command.Parameters.Add("@id", SqlDbType.Int).Value = id;
                        using(var dataReader=command.ExecuteReader())
                        {
                            while(dataReader.Read())
                            {
                                following_target_accounts.Add(dataReader["target_account"].ToString());
                            }
                        }
                    }
                }

                foreach (var item in following_target_accounts)
                {
                    if (followback_users.Contains(item))
                    {
                        IGHelpers.DBHelpers.AddActionLogByID(id, "followback", "", item);
                    }
                }
            }
        }

        private void btn_register_clone_Click(object sender, EventArgs e)
        {
            //PLAutoHelper.GDriveAPI.GDrive_UploadSingleFile(@"F:\DOWNLOAD\Screenshot_156.png", "1xjTgI1UQnROuNMBgXXzYnyzjbH2woLvZ");

            //IGHelpers.ProxyHelpers.ProxyServerOthers_AIO("igaio", true);

            //IGHelpers.ExifHelpers.ModifyExif_ExifToolCMD_FTPServer(@"E:\Google Drive\MerchData\represent\temp_Control VPN Chrome\temp_Control VPN Chrome\bin\Debug\default_exif.txt", @"F:\DOWNLOAD\error_1878768008 (1).jpg", @"F:\DOWNLOAD\modified_exif_309.jpg");


            //var modify_img = IGHelpers.ExifHelpers.OptimizeImage_FTPServer("LDPlayer-IG-155", @"F:\DOWNLOAD\error_1878768008 (1).jpg");

            //var action = new Action.FF_Login("emulator-5558", "teeslufu", "123456789kdL");
            //action.DoAction();dd

            Thread.SetData(Thread.GetNamedDataSlot("thread_type"), "action_light");
            var igdevice = new Class.IGDevice("LDPlayer-IG-517", false);

            Thread.Sleep(2000);

            //ADBSupport.LDHelpers.OpenIGMediaByID(igdevice.ld_devicename, "https://www.instagram.com/p/CMsjwf_ByNK/");

            //var action = new Action.SharePostToStory(igdevice, "https://www.instagram.com/p/CMsjwf_ByNK/");
            //action.DoAction();

            //var task = new IGTasks.MassDMTask(igdevice, igdevice.ig_account);
            //task.DoTask();


            //bool isexisted = ADBHelpers.IsImageExisted("127.0.0.1:5597", BMResource.SemiAutoComment.SemiAutoComment_PostWasOpen);

            //Stopwatch watch = new Stopwatch();
            //watch.Start();

            //var engage_data = AIOHelper.DBLogic.GetAccount_DoSpecificTask.GetAccount_DoEngage();

            //////var temp = AIOHelper.DBLogic.GetAccount_DoSpecificTask.GetAccount_DoSpecificTask_FollowingUser_ReturnList();

            //var time = watch.Elapsed.TotalSeconds.ToString();

            //Thread.SetData(Thread.GetNamedDataSlot("thread_type"), "action_light");
            //var igdevice = new Class.IGDevice("LDPlayer-IG-155");

            //igdevice.Dispose();

            //igdevice.deviceID = "127.0.0.1:5563";

            //var massdm_task = new IGTasks.MassDMTask(igdevice, igdevice.ig_account);
            //massdm_task.DoTask();

            //Thread.Sleep(2000);

            //var action = new Action.PublishStories(igdevice, @"C:\Users\pc\source\repos\lg-ldplayer\bin\Debug\Temp\error_2017123930.jpg");
            //action.DoAction();

            //Action.UpdateFullname updateFullname = new Action.UpdateFullname(igdevice, "Chuyen Dam Vay");
            //updateFullname.DoAction();

            //Thread.CurrentThread.Name = "igaio10";
            //Thread.SetData(Thread.GetNamedDataSlot("thread_type"), "action_light");
            //IGHelpers.ProxyHelpers.GetProxyOthers_v1();

            //PLAutoHelper.ProxyHelper.Xproxy_DB_UpdateProxyByImeis(new PLAutoHelper.ProxyHelper.ProxyDB(IGHelpers.DBHelpers.db_connection_igtoolkit, "proxytb"), "192.168.1.8");

            //PLAutoHelper.OnlineImageServerHelper.imgur_Authorize("minadearn");

            //var check_timespan = AIOHelper.GlobalHelper.Timespan_IsTimeInRange(DateTime.Now, TimeSpan.Parse("15:00:00"), TimeSpan.Parse("2:00:00"));

            //var check = AIOHelper.GlobalHelper.DateTime_IsTimeInRange(DateTime.Parse("1-17-2021 00:00:00"), TimeSpan.Parse("15:00:00"), TimeSpan.Parse("2:00:00"));

            //AIOHelper.DBLogic.GetAccount_DoSpecificTask.GetAccount_DoSpecificTask_FollowingUser();
            //IGHelpers.DBHelpers.UpdateStatusIgToolKitUser(44744031559, "status", SqlDbType.VarChar, "");
            //AIOHelper.DBLogic.GetAccount_DoSpecificTask.GetAccount_DoSpecificTask_FollowingUser();

            //var json = PLAutoHelper.DetectPersonality.DetectCountry_Namsor("Mỹ Duyên", "5896ca883255934a28e2a021bd8e650b");

            //IGAPIHelper.Scraper.APIObj dyn = IGAPIHelper.Scraper.GetAPIObj("192.168.1.8:4003", IGHelpers.DBHelpers.db_connection_igtoolkit);

            //InstagramApiSharp.API.IInstaApi _instaApi = dyn.instaApi;


            //IResult<InstagramApiSharp.Classes.Models.InstaMediaList> medias_result = IGAPIHelper.Scraper.ScrapeMediasFromUser(dyn.instaApi, "alinvlee");

            //var profile = IGAPIHelper.Scraper.ScrapeProfileBio(dyn.instaApi, "alinvlee");

            //var stories = IGThreads.ThreadHelpers.GrabStories(_instaApi, "hanagianganh");

            //var temp = IGAPIHelper.Scraper.ScrapeComment(_instaApi, "https://www.instagram.com/p/CIvYqKglzei/");

            //var thread = new IGThreads.MonitorSourceUsersThread();
            //thread.DoThread();

            //var backupname = Path.GetFileNameWithoutExtension(@"D:\dnplayer-ww-data\LDPlayer-IG-53\system\backup\daily\com.instagram.android-20201211-050504.tar.lzop").Replace(".tar","");
            //ADBSupport.LDHelpers.RemoveTitaniumBackupByName("LDPlayer-IG-53", "com.instagram.android-20201211-050504");

            //string deviceID = null;
            //var listDevice = KAutoHelper.ADBHelper.GetDevices();
            //if (listDevice != null && listDevice.Count > 0)
            //{
            //    deviceID = listDevice.First();
            //}

            //var screen = ADBHelpers.ScreenShoot(deviceID, true);

            //var find_point_postremoved = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, ADBSupport.BMPSource.ErrorScreen_PostRemoved);

            //find_point_postremoved = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, ADBSupport.BMPSource.ErrorScreen_PostRemoved_OK,0.8);

            //if (find_point_postremoved != null)
            //{
            //    ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.ErrorScreen_PostRemoved_OK);
            //    ADBSupport.ADBHelpers.Delay(6, 10);
            //}

            //IGHelpers.ProxyHelpers.ProxyServerOthers("igaio", true);



            //Thread.SetData(Thread.GetNamedDataSlot("thread_type"), "action_light");
            //var igdevice = new Class.IGDevice("LDPlayer-IG-36");


            //IGThreads.ThreadHelpers.DoNormalUser_PublishStories(igdevice, igdevice.ig_account);


            //igdevice.Dispose();


            //IGThreads.ThreadHelpers.DoNormalUser_UpdateProfile(igdevice, igdevice.ig_account);

            //var dyn = IGHelpers.Helpers.APIObJWithProxy();

            //var medias = IGAPIHelper.Scraper.GrabFromAccount(dyn.instaApi, "lucy.artmodel");

            //var userid = IGAPIHelper.Scraper.UserPKByURL("https://www.instagram.com/laurenreese268/", dyn.instaApi);

            //var userinfo = IGAPIHelper.Scraper.ScrapeFullUserInfo("", dyn.instaApi);


            //(new Action.AddComment(igdevice)).DoAction();



            //var temp =
            //Task.Run(((InstagramApiSharp.API.IInstaApi)dyn.instaApi).GetCurrentUserAsync).GetAwaiter().GetResult();

            //var comment_content = IGAPIHelper.Scraper.ScrapeComment(dyn.instaApi, "https://www.instagram.com/p/CIDl5hppVKo/");


            //comment_content = comment_content.Replace("@", "");

            //var dyn = IGHelpers.Helpers.APIObJWithProxy();

            //var userinfo = IGAPIHelper.Scraper.ScrapeProfileBio(dyn.instaApi, "alinvlee");

            //var dyn = IGHelpers.Helpers.APIObJWithProxy();

            //var comment_content = IGAPIHelper.Scraper.ScrapeComment(dyn.instaApi, "https://www.instagram.com/p/CGDMPGQlkk0/?igshid=1vkzvq3uwck7p");

            //TitaniumBackupHelpers.RestoreIG("", @"C:\Users\ad\source\repos\lg-ldplayer\bin\Debug\Temp\LDPlayer-IG-1\backup\", true);

            //var dyn = IGHelpers.Helpers.APIObJWithProxy();

            //var comment_content = IGAPIHelper.Scraper.ScrapeComment(dyn.instaApi, "https://www.instagram.com/p/CF68wbQsiDT/");


            //IGThreads.ThreadHelpers.DoNormalUser_Publish(igdevice, igdevice.ig_account);




            //var iswork = IGHelpers.ProxyHelpers.IsWorkFB("192.168.100.6:4006");

            //var igdevice = new Class.IGDevice("LDPlayer-IG-46");

            //string deviceID = null;
            //var listDevice = KAutoHelper.ADBHelper.GetDevices();
            //if (listDevice != null && listDevice.Count > 0)
            //{
            //    deviceID = listDevice.First();
            //}

            //ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.ErrorScreen_UnsualActivity);

            //ADBSupport.TitaniumBackupHelpers.BackUpIG(deviceID, ADBSupport.LDHelpers.ftp_dnplayerdata + "LDPlayer-IG-23" + "/system/backup/" + DateTime.Now.ToShortDateString().Replace("/", "-"), "sarajackson1355");

            //ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell service call statusbar 1");

            //Stopwatch watch_launch = new Stopwatch();
            //watch_launch.Start();


            //var launch_time = watch_launch.Elapsed.TotalSeconds;

            //var thread = new IGThreads.CheckBackupThread();
            //thread.DoThread();

            //List<string> accountIds = new List<string>();

            //List<string> need_backup_again = new List<string>();

            //using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            //{
            //    conn.Open();

            //    using (var command = new SqlCommand("select * from ig_account where [state]='ACTIVE'", conn))
            //    {
            //        using (var dataReader = command.ExecuteReader())
            //        {
            //            while (dataReader.Read())
            //            {
            //                accountIds.Add(dataReader["id"].ToString());

            //                var deviceName = "LDPlayer-IG-" + dataReader["id"].ToString();

            //                string ftp_backup_folder = ADBSupport.LDHelpers.ftp_dnplayerdata + deviceName + "/system/backup";

            //                var ftp_directories = IGHelpers.Helpers.GetDirectoryListing(ftp_backup_folder).OrderByDescending(d => d.Modified).ToList();

            //                var files = IGHelpers.Helpers.FTP_GetFileListing(ftp_directories[0].FullName).OrderByDescending(f=>f.Modified).ToList();

            //                if (files.Count != 3)
            //                {
            //                    need_backup_again.Add(dataReader["id"].ToString());
            //                    //for (int i = 3; i < files.Count; i++)
            //                    //{
            //                    //    IGHelpers.Helpers.FTP_DeleteFile(files[i].FullName);
            //                    //}
            //                }
            //            }
            //        }
            //    }

            //    //foreach (var item in need_backup_again)
            //    //{
            //    //    using (var command = new SqlCommand("update specifictasks set [backup]='Do' where [accountId]=" + item, conn))
            //    //    {
            //    //        command.ExecuteNonQuery();
            //    //    }
            //    //}
            //}



        }


        #endregion

        #region --Check Settings--

        public bool IsMinimizeLDPlayer()
        {
            return cb_minimize_ldplayer.Checked;
        }


        #endregion


        #region --Handle Batch btn--

        private void btn_register_batch_Click(object sender, EventArgs e)
        {
            if (cb_batch_option.Text == "")
                return;
            FreezeControl();
            switch (cb_batch_option.Text)
            {
                case "DELETE":
                    //DeletedSelectedRow();
                    break;
                case "UPDATE":
                    //UpdateSelectedRow();
                    break;
            }

            cb_batch_option.Text = "";
        }



        #endregion


        #region --Load, Update View--

        public void FreezeControl()
        {
            this.btn_register_clone.Enabled = false;
            this.btn_register_batch.Enabled = false;
        }

        public void UnFreezeControl()
        {
            this.btn_register_clone.Enabled = true;
            this.btn_register_batch.Enabled = true;
        }

        public void RefreshForm()
        {
            Mutex m = new Mutex(false, "ig_refreshform");
            m.WaitOne();

            ReloadManageOLV();

            m.ReleaseMutex();
        }

        #endregion




        #region --LoadDeviceToPanel--

        #region --User32--

        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndParent);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, int uFlags);

        #endregion


        #endregion



        #region --Handle Click Event--


        public bool isstop = false;

        private void main_btn_stop_Click(object sender, EventArgs e)
        {
            isstop = true;
        }

        #endregion



        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (current_phone_device != "")
            {
                //IGTasks.FFLiteSwitch.CloseFFSwitchDevice(current_phone_device, switch_device_fflite, LDHelpers.LDPlayer_path, LDHelpers.LDPlayerDATA_path);

                IGHelpers.ManageOLVHelpers.MakeDeviceFree(current_phone_device);

                switch_device_fflite = "";

                current_user = "";

                current_phone_device = "";
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            var processes = Process.GetProcessesByName("InstagramLDPlayer");

            foreach (var item in processes)
            {
                item.Kill();
            }
        }

        private void btn_tasking_Click(object sender, EventArgs e)
        {

            skip_error = cb_skip_error.Checked;

            skip_launch_error = checkBox_skip_launch_error.Checked;

            var choiced_task = cb_tasking.Text;

            if (choiced_task == "")
                return;

            isstop = false;
            FreezeControl();

            IGThread tasking = null;

            switch (choiced_task)
            {
                case "All In One":
                    tasking = new IGThreads.AIOThread();
                    tasking.DoThread();
                    break;
                case "Optimize Backup":
                    tasking = new IGThreads.CheckBackupThread();
                    tasking.DoThread();
                    break;
                case "Assign Account":
                    tasking = new IGThreads.AddAccountFromFileThread("switch");
                    tasking.DoThread();
                    break;
                case "Login New Account":
                    tasking = new IGThreads.LoginToNewAccountThread();
                    tasking.DoThread();
                    break;
                case "Clear Status All Devices":
                    ClearStatusAllDevices();
                    break;
                default:
                    throw new NotImplementedException("Not implement tasking function");
            }
        }

        private void ClearStatusAllDevices()
        {
            //Clear status

            WriteLog("Clear status of ig account...");
            IGHelpers.DBHelpers.UpdateStatusofIG();

            //Clear status of scrape users
            WriteLog("Clear status of scrapeusers...");
            IGHelpers.DBHelpers.ClearStatusOfUserScrape(true);

            //Clear status of commentposts
            WriteLog("Clear status of commentposts...");
            IGHelpers.DBHelpers.ClearStatusOfCommentPost(true);

            //Clear queue task
            WriteLog("Clear status of queuetasks....");
            IGHelpers.DBHelpers.ClearQueueTask(true);

            //Clear status of semiautocomment
            WriteLog("Clear status of semiautocomment...");
            IGHelpers.DBHelpers.ClearStatusOfSemiAutoTask(true);

            MessageBox.Show("Done!!");
        }


        #region --Manage DatagridView--

        public void ReloadManageOLV()
        {
            int current_index = dataGridView_manage.FirstDisplayedScrollingRowIndex;

            if (current_index == -1)
                current_index = 0;

            List<Class.IGAccountItem> ig_Accounts = IGHelpers.ManageOLVHelpers.LoadItems();

            //dataGridView_manage.DataSource = semicomments;

            //if (dataGridView_manage.CurrentRow != null)
            //    current_index = dataGridView_manage.CurrentRow.Index;

            dataGridView_manage.Invoke((MethodInvoker)delegate
            {
                dataGridView_manage.DataSource = ig_Accounts;
                if (ig_Accounts.Count > 0)
                    dataGridView_manage.FirstDisplayedScrollingRowIndex = current_index;
            });
        }


        private void dataGridView_manage_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                //TODO - Button Clicked - Execute Code Here
                if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Settings")
                {
                    var accountsetting_form = new IGForms.Account_Setting(dataGridView_manage, e.RowIndex);
                    accountsetting_form.ShowDialog();
                    return;
                }

                if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Launch")
                {
                    var id = dataGridView_manage.Rows[e.RowIndex].Cells["dgcol_id"].Value.ToString();

                    DBHelpers.UpdateStatusofIG(id, "launching device");

                    Thread thread = new Thread(() => LaunchManual(id, e.RowIndex));
                    thread.Name = "igaio" + id;
                    thread.IsBackground = true;
                    thread.Start();
                }

                if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Close")
                {
                    var id = dataGridView_manage.Rows[e.RowIndex].Cells["dgcol_id"].Value.ToString();

                    DBHelpers.UpdateStatusofIG(id, "closing device");

                    RefreshForm();

                    Thread thread = new Thread(() => CloseManual(id, e.RowIndex));
                    thread.Name = "igaio" + id;
                    thread.IsBackground = true;
                    thread.Start();
                }
            }
        }


        #endregion

        #region --Launch Manual--

        List<Class.IGDevice> iGDevices = new List<Class.IGDevice>() { };

        private void LaunchManual(string accountId, int rowindex)
        {
            Thread.SetData(Thread.GetNamedDataSlot("thread_type"), "action_light");

            var ig_account = IGHelpers.DBHelpers.DynamicIGAccountByID(accountId);
            var igdevice = new Class.IGDevice(ig_account, new Class.LDDevice(ig_account.phone_device), true, Program.form.skip_launch_error);

            iGDevices.Add(igdevice);

            //change launch status column value to switch device name

            DBHelpers.UpdateStatusofIG(accountId, igdevice.switch_device.name);

            RefreshForm();
        }

        private void CloseManual(string accountId, int rowindex)
        {
            var deviceName = "LDPlayer-IG-" + accountId.ToString();
            //search igdevice by name
            var igdevice = iGDevices.Where(i => i.ld_devicename == deviceName).First();

            Thread.SetData(ADBSupport.LDHelpers._ld_index, igdevice.switch_device.index);

            igdevice.Dispose();

            iGDevices.Remove(igdevice);

            //change launch status column value to closed


            DBHelpers.UpdateStatusofIG(accountId, "");

            RefreshForm();
        }


        private void btn_run_xproxy_server_Click(object sender, EventArgs e)
        {
            IGHelpers.ProxyHelpers.ProxyServerOthers("igaio", true);
            InitTimer_UpdateProxy();
            InitTimer_UpdateProxyStatus();
        }

        #region --Ticker Update CurrentUsed Proxy--
        //Update proxy status

        protected System.Windows.Forms.Timer timer_UpdateProxy;

        protected void InitTimer_UpdateProxy()
        {
            timer_UpdateProxy = new System.Windows.Forms.Timer();
            timer_UpdateProxy.Tick += new EventHandler(timer_UpdateProxy_Tick);
            timer_UpdateProxy.Interval = 5000; // in miliseconds
            timer_UpdateProxy.Start();

        }

        private void timer_UpdateProxy_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(() => IGHelpers.ProxyHelpers.UpdateCurrentUsed());
            worker.Name = "Update Proxy CurrentUsed Thread";
            worker.IsBackground = true;
            worker.Start();
        }


        #endregion


        #region --Ticker Update Proxy Status--
        //Update proxy status

        protected System.Windows.Forms.Timer timer_UpdateProxyStatus;

        protected void InitTimer_UpdateProxyStatus()
        {
            timer_UpdateProxyStatus = new System.Windows.Forms.Timer();
            timer_UpdateProxyStatus.Tick += new EventHandler(timer_UpdateProxyStatus_Tick);
            timer_UpdateProxyStatus.Interval = 10000; // in miliseconds
            timer_UpdateProxyStatus.Start();

        }

        private void timer_UpdateProxyStatus_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(() => IGHelpers.ProxyHelpers.ProxyServerOthers_AIO(""));
            worker.Name = "Update Proxy Status Thread";
            worker.IsBackground = true;
            worker.Start();
        }

        #endregion

        #endregion

        #region --Slaves Manual Datagridview--

        public void ReloadManageOLV_SlavesManual(string phone_device = null)
        {
            slaves_is_shown = false;


            List<dynamic> profiles = IGHelpers.ManageOLVHelpers.LoadItemsDynamic(phone_device);

            //remove column 

            dataGridView_slavesmanual.Columns.Clear();



            //dataGridView_manage.DataSource = semicomments;

            //apply filter to semicomments

            List<dynamic> filter_profiles = new List<dynamic>();

            if (cb_Has_Message.Checked || cb_Monitor_Comment.Checked || cb_Monitor_Message.Checked)
            {
                if (cb_Has_Message.Checked)
                    filter_profiles.AddRange(profiles.Where(s => s.has_message == "yes").ToList());
                if (cb_Monitor_Message.Checked)
                    filter_profiles.AddRange(profiles.Where(s => s.monitor_message == "true").ToList());
                if (cb_Monitor_Comment.Checked)
                    filter_profiles.AddRange(profiles.Where(s => s.monitor_comment == "true").ToList());

                filter_profiles = filter_profiles.Distinct().ToList();
            }
            else
                filter_profiles = profiles;



            if (filter_profiles.Count == 0)
            {
                dataGridView_slavesmanual.DataSource = null;
                return;
            }

            dataGridView_slavesmanual.Invoke((MethodInvoker)delegate
            {
                dataGridView_slavesmanual.DataSource = filter_profiles;
            });



            int index = 5;

            DataGridViewButtonColumn launch_firefox_ButtonColumn = new DataGridViewButtonColumn();
            launch_firefox_ButtonColumn.Name = "Launch Firefox";
            launch_firefox_ButtonColumn.Text = "Launch Firefox";
            launch_firefox_ButtonColumn.UseColumnTextForButtonValue = true;

            int columnIndex = index + 3;
            if (dataGridView_slavesmanual.Columns["Launch Firefox"] == null)
            {
                dataGridView_slavesmanual.Columns.Insert(columnIndex, launch_firefox_ButtonColumn);
            }

            //DataGridViewButtonColumn launchButtonColumn = new DataGridViewButtonColumn();
            //launchButtonColumn.Name = "Launch";
            //launchButtonColumn.Text = "Launch";
            //launchButtonColumn.UseColumnTextForButtonValue = true;

            //columnIndex = index + 3;
            //if (dataGridView_slavesmanual.Columns["Launch"] == null)
            //{
            //    dataGridView_slavesmanual.Columns.Insert(columnIndex, launchButtonColumn);
            //}



            //DataGridViewButtonColumn fillButtonColumn = new DataGridViewButtonColumn();
            //fillButtonColumn.Name = "Fill";
            //fillButtonColumn.Text = "Fill";
            //fillButtonColumn.UseColumnTextForButtonValue = true;

            //columnIndex = index + 4;
            //if (dataGridView_slavesmanual.Columns["Fill"] == null)
            //{
            //    dataGridView_slavesmanual.Columns.Insert(columnIndex, fillButtonColumn);
            //}

            DataGridViewButtonColumn clear_messageButtonColumn = new DataGridViewButtonColumn();
            clear_messageButtonColumn.Name = "Clear Message";
            clear_messageButtonColumn.Text = "Clear Message";
            clear_messageButtonColumn.UseColumnTextForButtonValue = true;

            columnIndex = index + 6;
            if (dataGridView_slavesmanual.Columns["Clear Message"] == null)
            {
                dataGridView_slavesmanual.Columns.Insert(columnIndex, clear_messageButtonColumn);
            }

            //DataGridViewButtonColumn closeButtonColumn = new DataGridViewButtonColumn();
            //closeButtonColumn.Name = "Close";
            //closeButtonColumn.Text = "Close";
            //closeButtonColumn.UseColumnTextForButtonValue = true;

            //columnIndex = index + 7;
            //if (dataGridView_slavesmanual.Columns["Close"] == null)
            //{
            //    dataGridView_slavesmanual.Columns.Insert(columnIndex, closeButtonColumn);
            //}

            DataGridViewButtonColumn viewmessageButtonColumn = new DataGridViewButtonColumn();
            viewmessageButtonColumn.Name = "View Message";
            viewmessageButtonColumn.Text = "View Message";
            viewmessageButtonColumn.UseColumnTextForButtonValue = true;

            columnIndex = index + 5;
            if (dataGridView_slavesmanual.Columns["View Message"] == null)
            {
                dataGridView_slavesmanual.Columns.Insert(columnIndex, viewmessageButtonColumn);
            }

            //test checkbox

            DataGridViewCheckBoxColumn monitorMessageCheckboxColumn = new DataGridViewCheckBoxColumn();
            monitorMessageCheckboxColumn.Name = "Monitor Message";


            columnIndex = index + 1;
            if (dataGridView_slavesmanual.Columns["Monitor Message"] == null)
            {
                dataGridView_slavesmanual.Columns.Insert(columnIndex, monitorMessageCheckboxColumn);
            }


            DataGridViewCheckBoxColumn monitorCommentCheckboxColumn = new DataGridViewCheckBoxColumn();
            monitorCommentCheckboxColumn.Name = "Monitor Comment";


            columnIndex = index + 2;
            if (dataGridView_slavesmanual.Columns["Monitor Comment"] == null)
            {
                dataGridView_slavesmanual.Columns.Insert(columnIndex, monitorCommentCheckboxColumn);
            }


            DataGridViewTextBoxColumn HasMessageTextColumn = new DataGridViewTextBoxColumn();
            HasMessageTextColumn.Name = "Has Message";


            columnIndex = index;
            if (dataGridView_slavesmanual.Columns["Has Message"] == null)
            {
                dataGridView_slavesmanual.Columns.Insert(columnIndex, HasMessageTextColumn);
            }

            //Hide monitor_

            dataGridView_slavesmanual.Columns["monitor_comment"].Visible = false;
            dataGridView_slavesmanual.Columns["monitor_message"].Visible = false;
            dataGridView_slavesmanual.Columns["has_message"].Visible = false;
            dataGridView_slavesmanual.Columns["message_base64"].Visible = false;
            dataGridView_slavesmanual.Columns["Status"].Visible = false;
            dataGridView_slavesmanual.Columns["already_hasprofile"].Visible = false;

            //Get monitor value and set checkbox

            foreach (DataGridViewRow row in dataGridView_slavesmanual.Rows)
            {
                if (row.Cells["monitor_comment"].Value.ToString() == "true")
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells["Monitor Comment"];
                    chk.Value = true;
                }
            }

            foreach (DataGridViewRow row in dataGridView_slavesmanual.Rows)
            {
                if (row.Cells["monitor_message"].Value.ToString() == "true")
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells["Monitor Message"];
                    chk.Value = true;
                }
            }

            foreach (DataGridViewRow row in dataGridView_slavesmanual.Rows)
            {
                if (row.Cells["has_message"].Value.ToString() == "yes")
                {
                    DataGridViewTextBoxCell has_message_cell = (DataGridViewTextBoxCell)row.Cells["Has Message"];
                    has_message_cell.Value = "yes";
                    has_message_cell.Style.BackColor = Color.FromArgb(186, 220, 88);
                }
            }

            foreach (DataGridViewRow row in dataGridView_slavesmanual.Rows)
            {
                if (row.Cells["message_base64"].Value.ToString() == "")
                {
                    DataGridViewButtonCell viewmessage_cell = (DataGridViewButtonCell)row.Cells["View Message"];
                    DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
                    dataGridViewCellStyle.Padding = new Padding(0, 0, 1000, 0);
                    viewmessage_cell.Style = dataGridViewCellStyle;
                }
            }

            //Format cell has message

            //foreach (DataGridViewRow row in dataGridView_slavesmanual.Rows)
            //{
            //    if(row.Cells["HasMessage"].Value.ToString()=="yes")
            //        row.Cells["HasMessage"].Style.BackColor = Color.FromArgb(186, 220, 88);
            //}




            slaves_is_shown = true;
        }

        string switch_device_fflite = "";
        string current_user = "";
        string current_phone_device = "";

        bool slaves_is_shown = false;

        private void dataGridView_slavesmanual_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                //TODO - Button Clicked - Execute Code Here
                //if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Launch")
                //{
                //    if (current_user != "" || current_phone_device != "")
                //    {
                //        MessageBox.Show("You must close current device first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //        return;
                //    }

                //    //Check if this igaccount is login in another pc

                //    if (!IGHelpers.ManageOLVHelpers.IsDeviceFree(senderGrid.Rows[e.RowIndex].Cells["PhoneDevice"].Value.ToString()))
                //    {
                //        MessageBox.Show("This account is uing by another device", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //        return;
                //    }

                //    current_phone_device = senderGrid.Rows[e.RowIndex].Cells["PhoneDevice"].Value.ToString();

                //    switch_device_fflite = IGTasks.FFLiteSwitch.LaunchSwitchFFDevice(current_phone_device, LDHelpers.LDPlayer_path, LDHelpers.LDPlayerDATA_path);
                //    current_user = senderGrid.Rows[e.RowIndex].Cells["User"].Value.ToString();
                //}

                if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Launch Firefox")
                {
                    var id = senderGrid.Rows[e.RowIndex].Cells["Id"].Value.ToString();
                    var user = senderGrid.Rows[e.RowIndex].Cells["User"].Value.ToString();
                    var password = "123456789kdL";

                    var account = new { id, user, password };

                    bool already_hasprofile = senderGrid.Rows[e.RowIndex].Cells["already_hasprofile"].Value.ToString() == "yes" ? true : false;

                    throw new System.ArgumentException("Not handle proxy below line");
                    var proxy = "";// ADBSupport.LDHelpers.proxy_server_ip + ":500" + (new Random(Guid.NewGuid().GetHashCode())).Next(1, 9);

                    IGTasks.FFPCSwitch.Launch(account, proxy, already_hasprofile);

                    if (!already_hasprofile)
                    {
                        //update hasprofile
                        IGHelpers.DBHelpers.SetAlready_HasProfile(id);
                    }
                }

                //if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Fill")
                //{
                //    if (current_user != "" && senderGrid.Rows[e.RowIndex].Cells["User"].Value.ToString() != current_user)
                //    {
                //        MessageBox.Show("Wrong Click!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //        return;
                //    }
                //    IGTasks.FFLiteSwitch.LoginFFInstagram(switch_device_fflite, senderGrid.Rows[e.RowIndex].Cells["User"].Value.ToString(), "123456789kdL", senderGrid.Rows[e.RowIndex].Cells["PhoneDevice"].Value.ToString(), LDHelpers.LDPlayer_path, LDHelpers.LDPlayerDATA_path);
                //}

                if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Fill")
                {
                    var id = senderGrid.Rows[e.RowIndex].Cells["Id"].Value.ToString();
                    var user = senderGrid.Rows[e.RowIndex].Cells["User"].Value.ToString();
                    var password = "123456789kdL";

                    var account = new { id, user, password };

                    var status = FFPCSwitch.LoginIG(account);

                    if (status == "NotFoundIG")
                    {
                        MessageBox.Show("Please open Instagram.com on browser and Click Fill Again", "Error", MessageBoxButtons.OK);
                    }
                    else
                    if (status == "Success")
                    {
                        MessageBox.Show("Login Success!!");
                    }
                }

                if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Clear Message")
                {
                    IGHelpers.ManageOLVHelpers.UpdateClearMessage(senderGrid.Rows[e.RowIndex].Cells["Id"].Value.ToString());

                    senderGrid.Rows[e.RowIndex].Cells["Has Message"].Value = "";
                    senderGrid.Rows[e.RowIndex].Cells["Has Message"].Style.BackColor = Color.White;

                    //disable viewmessage

                    DataGridViewButtonCell viewmessage_cell = (DataGridViewButtonCell)senderGrid.Rows[e.RowIndex].Cells["View Message"];
                    DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
                    dataGridViewCellStyle.Padding = new Padding(0, 0, 1000, 0);
                    viewmessage_cell.Style = dataGridViewCellStyle;
                }

                if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "View Message")
                {
                    var message_base64 = senderGrid.Rows[e.RowIndex].Cells["message_base64"].Value.ToString();
                    if (message_base64 != "")
                    {
                        var messagescreen = new IGForms.MessageScreen(message_base64);
                        messagescreen.Show();
                    }
                }

                //if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Close")
                //{
                //    if (current_user != "" && senderGrid.Rows[e.RowIndex].Cells["User"].Value.ToString() != current_user)
                //    {
                //        MessageBox.Show("Wrong Click!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //        return;
                //    }
                //    IGTasks.FFLiteSwitch.CloseFFSwitchDevice(senderGrid.Rows[e.RowIndex].Cells["PhoneDevice"].Value.ToString(), switch_device_fflite, LDHelpers.LDPlayer_path, LDHelpers.LDPlayerDATA_path);

                //    IGHelpers.ManageOLVHelpers.MakeDeviceFree(senderGrid.Rows[e.RowIndex].Cells["PhoneDevice"].Value.ToString());

                //    switch_device_fflite = "";

                //    current_user = "";

                //    current_phone_device = "";
                //}
            }

        }


        private void btn_search_device_Click(object sender, EventArgs e)
        {
            ReloadManageOLV_SlavesManual(tb_phonedevice.Text);
        }

        #endregion

        private void btn_Settings_Click(object sender, EventArgs e)
        {
            IGForms.Tool_Settings tool_Settings = new IGForms.Tool_Settings();
            tool_Settings.ShowDialog();
        }

        private void dataGridView_slavesmanual_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

            var senderGrid = (DataGridView)sender;

            if (!slaves_is_shown)
                return;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn &&
                e.RowIndex >= 0)
            {
                if (senderGrid.Columns[e.ColumnIndex].Name == "Monitor Comment")
                {
                    var value = senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToLower();

                    //update to db
                    IGHelpers.ManageOLVHelpers.UpdateMonitorComment(senderGrid.Rows[e.RowIndex].Cells["Id"].Value.ToString(), value);
                }

                if (senderGrid.Columns[e.ColumnIndex].Name == "Monitor Message")
                {
                    var value = senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToLower();

                    //update to db
                    IGHelpers.ManageOLVHelpers.UpdateMonitorMessage(senderGrid.Rows[e.RowIndex].Cells["Id"].Value.ToString(), value);
                }
            }
        }

        private void cb_Has_Message_CheckedChanged(object sender, EventArgs e)
        {
            ReloadManageOLV_SlavesManual();
        }

        private void cb_Has_Comment_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void cb_Monitor_Comment_CheckedChanged(object sender, EventArgs e)
        {
            ReloadManageOLV_SlavesManual();

        }

        private void cb_Monitor_Message_CheckedChanged(object sender, EventArgs e)
        {
            ReloadManageOLV_SlavesManual();

        }


        private void btn_slaves_backupff_profile_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Phần mềm sẽ tắt hết Firefox đang mở. Bạn có muốn tiếp tục", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

            if (result == DialogResult.Cancel)
                return;

            Thread thread = new Thread(() =>
            {
                label_slaves_backup_status.Invoke((MethodInvoker)delegate
                {
                    label_slaves_backup_status.Text = "Doing Backup";
                });
            });
            thread.IsBackground = true;
            thread.Start();
            var ff_proccesses = Process.GetProcessesByName("Firefox");

            foreach (var p in ff_proccesses)
            {
                p.Kill();
            }

            //Backup

            //get all account already has profile

            var profiles = IGHelpers.DBHelpers.FFPC_ProfilesToBackup();

            //filter profile is available on this pc

            List<string> need_backup_profiles = new List<string>();

            foreach (var p in profiles)
            {
                var profile_path = FFPCSwitch.ffprofile_locate + "\\igprofile_" + p;

                if (Directory.Exists(profile_path))
                {
                    //check if current profile in pc is newest than latest backup
                    var d = new DirectoryInfo(profile_path);
                    if (IGHelpers.DBHelpers.IsNewestBackupTime(p, d.LastWriteTime))
                        need_backup_profiles.Add(p);
                }
            }

            //run backup thread for all
            FFPCSwitch.BackupFFThread(need_backup_profiles);

            //add log to db

            foreach (var item in need_backup_profiles)
            {
                IGHelpers.DBHelpers.AddActionLogByID(item, "backup_ffpc");
            }


            MessageBox.Show("Backup: Completed!", "Annoucement", MessageBoxButtons.OK, MessageBoxIcon.Information);

            label_slaves_backup_status.Text = "Finished";
            this.RefreshForm();
        }

        public bool skip_error = false;
        public bool keep_ldplayer_open = false;
        private void cb_skip_error_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_skip_error.Checked)
            {
                var dialog_result = MessageBox.Show("Make sure you know what're you doing! This will skip error -> Can't monitor new bug and effect results of tool. Do you still want to enable it?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (dialog_result == DialogResult.Yes)
                {
                    skip_error = true;
                }
                else
                {
                    cb_skip_error.Checked = false;
                }
            }
        }


        private void checkBox_keep_ldplay_open_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_keep_ldplay_open.Checked)
            {
                keep_ldplayer_open = true;
            }
        }

        #region --Set Resources--

        private void btn_set_resource_Click(object sender, EventArgs e)
        {
            if (cb_resources.Text == "" || tb_resources_quantity.Text == "")
            {
                MessageBox.Show("You must choose resource, quantity that you want to change");
                return;
            }

            var resource = cb_resources.Text;

            string file_pattern = "";

            switch (resource)
            {
                case "Default Reply":
                    file_pattern = "default_reply";
                    break;
                case "Default Post Text":
                    file_pattern = "defaulttext_post";
                    break;
                case "Default Watermark Text":
                    file_pattern = "defaulttext_watermark";
                    break;
            }

            List<string> accountIds = tb_accounts_resource.Text.Split(',').ToList();

            var resource_files = Directory.GetFiles(tb_resource_data_folder.Text, "*.txt");

            foreach (var id in accountIds)
            {
                var ftp_system_path = LDHelpers.ftp_dnplayerdata + "LDPlayer-IG-" + id + "/system";


                if (checkBox_remove_old_resources.Checked)
                {
                    //remove old resource
                    var ftp_files = IGHelpers.Helpers.FTP_GetFileListing(ftp_system_path, file_pattern + "*.txt").ToList();

                    foreach (var f in ftp_files)
                    {
                        IGHelpers.Helpers.FTP_DeleteFile(f.FullName);
                    }
                }

                var choiced_resources = resource_files.OrderBy(x => Guid.NewGuid()).Take(int.Parse(tb_resources_quantity.Text)).ToList();

                //add new resource to device folder

                foreach (var item in choiced_resources)
                {
                    var copied_filepath = "";

                    while (true)
                    {
                        var guid = Guid.NewGuid().GetHashCode().ToString();

                        if (!IGHelpers.Helpers.FTP_IsFileExisted(ftp_system_path + "/" + file_pattern + "_" + guid.Substring(guid.Length - 3) + ".txt"))
                        {
                            copied_filepath = ftp_system_path + "/" + file_pattern + "_" + guid.Substring(guid.Length - 3) + ".txt";
                            break;
                        }
                    }

                    //copy

                    IGHelpers.Helpers.FTP_UploadFile(item, copied_filepath, false);
                }
            }

            MessageBox.Show("Set Resource Completed");
        }


        private void btn_set_resource_browser_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();

            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                tb_resource_data_folder.Text = folderBrowserDialog.SelectedPath;
            }
        }

        #endregion

        #region --Tasks--

        #region --Import--

        private void btn_tasks_import_directly_Click(object sender, EventArgs e)
        {
            btn_tasks_import_directly.Enabled = false;
            btn_tasks_import_fromcsv.Enabled = true;

            //show/hide panel

            panel_import_directly.Visible = true;
            panel_import_from_csv.Visible = false;

            panel_import_directly.Location = new Point(43, 168);

            tasks_import_type = "import_directly";
        }


        private void btn_tasks_import_fromcsv_Click(object sender, EventArgs e)
        {
            btn_tasks_import_fromcsv.Enabled = false;
            btn_tasks_import_directly.Enabled = true;


            //show/hide panel

            panel_import_directly.Visible = false;
            panel_import_from_csv.Visible = true;

            panel_import_from_csv.Location = new Point(43, 168);

            tasks_import_type = "import_fromcsv";
        }

        string tasks_import_type = "";


        private void btn_tasks_import_addtasks_Click(object sender, EventArgs e)
        {
            if (comboBox_tasks_import_task.Text == "")
            {
                MessageBox.Show("You muse choose task!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (tasks_import_type == "")
            {
                MessageBox.Show("Not found data to import", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string taskname = "";

            switch (comboBox_tasks_import_task.Text)
            {
                case "Update Profile Image":
                    throw new NotImplementedException();
                    break;
                case "Update Bio":
                    taskname = "updateprofile_bio";
                    break;
                case "Edit Fullname":
                    throw new NotImplementedException();
                    break;
                default:
                    throw new System.ArgumentException("Not recognize this task");
            }


            List<dynamic> tasks = new List<dynamic>();

            //accountId, taskname, value, schedule

            switch (tasks_import_type)
            {
                case "import_directly":
                    tasks = Tasks_Import_Directly(taskname);
                    break;
                case "import_fromcsv":
                    tasks = Tasks_Import_FromCsv(taskname);
                    break;
            }

            IGHelpers.DBHelpers.AddTasks(tasks, checkBox_tasks_import_schedule_random_times.Checked);

            MessageBox.Show("Completed!");
        }

        private List<dynamic> Tasks_Import_Directly(string taskname)
        {
            List<dynamic> dynamics = new List<dynamic>();

            List<string> accountIds = tb_tasks_import_accounts.Text.Split(',').ToList();

            var files = Directory.GetFiles(tb_tasks_import_folder.Text, "*.txt").ToList();

            foreach (var accountId in accountIds)
            {
                var value = File.ReadAllText(files[(new Random(Guid.NewGuid().GetHashCode())).Next(files.Count)]);

                dynamics.Add(new
                {
                    accountId,
                    taskname,
                    value

                });
            }
            return dynamics;
        }



        private List<dynamic> Tasks_Import_FromCsv(string taskname)
        {
            List<dynamic> dynamics = new List<dynamic>();

            var lines = File.ReadAllLines(tb_tasks_import_csv.Text);

            foreach (var item in lines)
            {
                var accountId = item.Split('|')[0];
                var value = item.Split('|')[1];

                dynamics.Add(new
                {
                    accountId,
                    taskname,
                    value

                });
            }

            return dynamics;
        }


        private void btn_tasks_import_browse_folder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();

            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                tb_tasks_import_folder.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void btn_tasks_import_browse_csv_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();

            fileDialog.Filter = "Text files (*.txt)|*.txt";

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                tb_tasks_import_csv.Text = fileDialog.FileName;
            }
        }

        #endregion

        #endregion

        #region --Posts Tab--

        private void btn_posts_add_images_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.Filter =
        "Images (*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|" +
        "All files (*.*)|*.*";

            openFileDialog.Multiselect = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                foreach (var file in openFileDialog.FileNames)
                {
                    tb_posts_add_images.AppendText(file + "\r\n");
                }
            }
        }


        private void btn_posts_add_addpost_Click(object sender, EventArgs e)
        {
            var content = tb_posts_add_content.Text;
            var hashtag = tb_posts_add_hashtag.Text;
            var settings = tb_posts_add_settings.Text;
            var state = comboBox_posts_add_state.Text;

            List<string> images = tb_posts_add_images.Lines.ToList().Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList();

            var image_base64 = "";
            //images
            foreach (var item in images)
            {
                var base64 = Action.ActionHelpers.SaveBitmapToBase64Str((Bitmap)Image.FromFile(item));

                image_base64 = image_base64 + base64 + "|||";
            }

            var post = new
            {
                content,
                hashtag,
                settings,
                state,
                image_base64
            };

            IGHelpers.DBHelpers.AddPost(post);

            //clear form

            tb_posts_add_content.Clear();
            tb_posts_add_hashtag.Clear();
            tb_posts_add_images.Clear();
            comboBox_posts_add_state.Text = "";
            tb_posts_add_settings.Clear();

        }


        #endregion

        #region --Register Tab--

        public bool register_skiperror = false;
        public bool register_auto_generate_user = false;

        private void btn_register_start_Click(object sender, EventArgs e)
        {
            if (comboBox_register_phone_services.Text == "")
            {
                MessageBox.Show("Please choose phone service");
                return;
            }

            List<string> deviceIds = new List<string>();

            deviceIds.AddRange(tb_register_deviceids.Lines.ToList());

            var reg = new IGThreads.RegisterRealPhoneThread(deviceIds, comboBox_register_phone_services.Text);
            reg.DoThread();
        }


        private void checkBox_register_skiperror_CheckedChanged(object sender, EventArgs e)
        {
            register_skiperror = checkBox_register_skiperror.Checked;
        }

        private void checkBox_register_auto_generate_user_CheckedChanged(object sender, EventArgs e)
        {
            register_auto_generate_user = checkBox_register_auto_generate_user.Checked;
        }

        private void btn_register_generate_names_Click(object sender, EventArgs e)
        {
            //read all name from textarea
            List<string> origin_names = new List<string>();

            var reader = new StringReader(tb_register_list_names.Text);
            string line;
            while (null != (line = reader.ReadLine()))
            {
                origin_names.Add(line);
            }

            List<string> firstname = new List<string>();
            List<string> lastname = new List<string>();


            foreach (var name in origin_names)
            {
                var temp_firstname = name.Split(new char[] { ' ' })[0];

                if (temp_firstname == name)
                    continue;

                var temp_lastname = name.Substring(temp_firstname.Length + 1);

                if (temp_firstname != "")
                    firstname.Add(temp_firstname);
                if (temp_lastname != "")
                    lastname.Add(temp_lastname);
            }

            List<string> new_names = new List<string>();

            var quantity = int.Parse(tb_register_quantity.Text);

            for (int i = 0; i < quantity; i++)
            {
                var temp_first = firstname[(new Random(Guid.NewGuid().GetHashCode())).Next(firstname.Count)];
                var temp_last = lastname[(new Random(Guid.NewGuid().GetHashCode())).Next(lastname.Count)];

                var new_name = temp_first + " " + temp_last;
                new_names.Add(new_name);
            }

            var new_names_str = "";
            foreach (var item in new_names)
            {
                new_names_str += item + "\r\n";
            }

            if (cb_register_addto_account_reg.Checked)
            {
                //add to db
                IGHelpers.DBHelpers.RealPhoneReg_AddFullname(new_names);
            }

            Clipboard.SetText(new_names_str);

            MessageBox.Show("New Names Copied To Clipboard");
        }


        private void btn_register_load_original_names_Click(object sender, EventArgs e)
        {

            tb_register_list_names.Text = Resources.ExSource.original_names;
        }

        #endregion

        public bool skip_launch_error = false;

        private void checkBox_skip_launch_error_CheckedChanged(object sender, EventArgs e)
        {
            skip_launch_error = checkBox_skip_launch_error.Checked;
        }

        #region --Scrape Slaves Comment--

        public bool is_scrape_slaves_comment = true;
        private void checkBox_scrape_slaves_comment_CheckedChanged(object sender, EventArgs e)
        {
            is_scrape_slaves_comment = checkBox_scrape_slaves_comment.Checked;
        }

        private void btn_refresh_slaves_comment_Click(object sender, EventArgs e)
        {
            //Load slaves comment
            List<dynamic> slaves_commment = IGHelpers.ManageOLVHelpers.LoaddAllSlavesComment();

            slaves_commment = slaves_commment.OrderByDescending(x => DateTime.Parse(x.Comment_At)).ToList();

            dataGridView_slaves_comment.Invoke((MethodInvoker)delegate
            {
                dataGridView_slaves_comment.DataSource = slaves_commment;
            });

            //set width

            dataGridView_slaves_comment.Columns["PostUrl"].Width = 240;
            dataGridView_slaves_comment.Columns["Comment_Text"].Width = 480;
            dataGridView_slaves_comment.Columns["Comment_At"].Width = 150;

            //Sort by comment_at

            int index = 7;

            DataGridViewButtonColumn launch_firefox_ButtonColumn = new DataGridViewButtonColumn();
            launch_firefox_ButtonColumn.Name = "Launch Firefox";
            launch_firefox_ButtonColumn.Text = "Launch Firefox";
            launch_firefox_ButtonColumn.UseColumnTextForButtonValue = true;
            launch_firefox_ButtonColumn.Width = 100;

            int columnIndex = index;
            if (dataGridView_slaves_comment.Columns["Launch Firefox"] == null)
            {
                dataGridView_slaves_comment.Columns.Insert(columnIndex, launch_firefox_ButtonColumn);
            }


            DataGridViewButtonColumn copylink_ButtonColumn = new DataGridViewButtonColumn();
            copylink_ButtonColumn.Name = "Copy Link";
            copylink_ButtonColumn.Text = "Copy Link";
            copylink_ButtonColumn.UseColumnTextForButtonValue = true;
            copylink_ButtonColumn.Width = 100;

            columnIndex = index + 1;
            if (dataGridView_slaves_comment.Columns["Copy Link"] == null)
            {
                dataGridView_slaves_comment.Columns.Insert(columnIndex, copylink_ButtonColumn);
            }
        }


        private void dataGridView_slaves_comment_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                //TODO - Button Clicked - Execute Code Here
                if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Copy Link")
                {
                    Clipboard.SetText(senderGrid.Rows[e.RowIndex].Cells["PostUrl"].Value.ToString());
                }

                if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Launch Firefox")
                {
                    var id = senderGrid.Rows[e.RowIndex].Cells["Id"].Value.ToString();
                    var user = senderGrid.Rows[e.RowIndex].Cells["User"].Value.ToString();
                    var password = "123456789kdL";

                    var account = new { id, user, password };

                    bool already_hasprofile = senderGrid.Rows[e.RowIndex].Cells["already_hasprofile"].Value.ToString() == "yes" ? true : false;

                    throw new System.ArgumentException("Not handle proxy below line");
                    var proxy = "";// ADBSupport.LDHelpers.proxy_server_ip + ":500" + (new Random(Guid.NewGuid().GetHashCode())).Next(1, 9);

                    IGTasks.FFPCSwitch.Launch(account, proxy, already_hasprofile);

                    if (!already_hasprofile)
                    {
                        //update hasprofile
                        IGHelpers.DBHelpers.SetAlready_HasProfile(id);
                    }

                    MessageBox.Show("Đã launch firefox thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1, (MessageBoxOptions)0x40000);  // MB_TOPMOST
                }

            }
        }

        #endregion

        private void btn_do_background_task_Click(object sender, EventArgs e)
        {
            panel_background_task.Enabled = false;

            if (is_scrape_slaves_comment)
            {
                var thread = new IGThreads.GrabSlavesCommentThread();
                thread.DoThread();
            }
        }

        #region --Settings JSON--

        private void btn_setting_json_clear_all_Click(object sender, EventArgs e)
        {
            tb_settings_json_users.Text = "";
            tb_settings_json_setting.Text = "";
        }

        private void btn_settings_tab_update_Click(object sender, EventArgs e)
        {
            List<string> users = tb_settings_json_users.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();
            List<string> update_jsons = tb_settings_json_setting.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();

            string datatype = "user";

            if (rb_id.Checked)
                datatype = "id";

            IGHelpers.Settings.Settings_UpdateByUsername(users, update_jsons, datatype);

            MessageBox.Show("Complete!");
        }

        private void button_settings_removenow_Click(object sender, EventArgs e)
        {
            List<string> users = tb_settings_json_users.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();
            string remove_j_path = textBox_settings_remove.Text;

            string datatype = "user";

            if (rb_id.Checked)
                datatype = "id";

            IGHelpers.Settings.Settings_RemoveSettingsByUsername(users, remove_j_path, datatype);

            MessageBox.Show("Complete!");
        }

        #endregion

        #region --TransferAccount--

        string transferaccount_db_connection_template = @"Data Source=@server_ip;Initial Catalog=@db;User ID=sa;Password=123456789kdL;Pooling=true;Max Pool Size=150;Min Pool Size=50;Connect Timeout=30;Connection Lifetime=300;ConnectRetryCount=3";

        private void button_transferaccount_from_testconnection_Click(object sender, EventArgs e)
        {
            string server_ip = textBox_transferaccount_from_server_ip.Text;
            string db_name = textBox_transferaccount_from_database.Text;

            string db_connection = transferaccount_db_connection_template.Replace("@server_ip", server_ip).Replace("@db", db_name);

            try
            {
                using (SqlConnection conn = new SqlConnection(db_connection))
                {
                    conn.Open();

                }

                MessageBox.Show("Connection is OK","Annoucement",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch
            {

                MessageBox.Show("Connection is OK", "Annoucement", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void button_transferaccount_from_browse_data_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    textBox_transferaccount_from_folder_data.Text = fbd.SelectedPath;
                }
            }
        }

        private void button_transferaccount_to_testconnection_Click(object sender, EventArgs e)
        {
            string server_ip = textBox_transferaccount_to_server_ip.Text;
            string db_name = textBox_transferaccount_to_database.Text;

            string db_connection = transferaccount_db_connection_template.Replace("@server_ip", server_ip).Replace("@db", db_name);

            try
            {
                using (SqlConnection conn = new SqlConnection(db_connection))
                {
                    conn.Open();

                }

                MessageBox.Show("Connection is OK", "Annoucement", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {

                MessageBox.Show("Connection is OK", "Annoucement", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void button_transferaccount_to_browse_data_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    textBox_transferaccount_to_folder_data.Text = fbd.SelectedPath;
                }
            }
        }


        private void button_transferaccount_transfer_now_Click(object sender, EventArgs e)
        {
            textBox_transferaccount_from_server_ip.Text = "192.168.1.7";
            textBox_transferaccount_from_database.Text = "instagram_ww_db";

            textBox_transferaccount_to_server_ip.Text = "192.168.1.6";
            textBox_transferaccount_to_database.Text = "aio_instagram_db";

            var server_ip_from = textBox_transferaccount_from_server_ip.Text;
            var db_name_from= textBox_transferaccount_from_database.Text;
            var data_folder_from = @"D:\dnplayer-ww-data";

            var server_ip_to = textBox_transferaccount_to_server_ip.Text;
            var db_name_to = textBox_transferaccount_to_database.Text;
            var data_folder_to = @"D:\dnplayer-aio-data";

            List<string> transfer_accountIds = textBox_transferaccount_from_accountId.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();

            Thread thread = new Thread(() => AIOHelper.ThreadHelper.TransferAccount.TransferFromTo(transfer_accountIds, server_ip_from, db_name_from, data_folder_from, server_ip_to, db_name_to, data_folder_to));
            thread.Start();
            
        }

        public void TransferAccount_WriteLog(string message)
        {
            textBox_transferaccount_log.Invoke((MethodInvoker)delegate
            {
                textBox_transferaccount_log.AppendText(message + " at " + DateTime.Now + "\r\n");
            });
        }


        #endregion

        private void button_run_xproxy_aio_Click(object sender, EventArgs e)
        {
            button_run_xproxy_aio.Enabled = false;
            label_run_xproxyaio.Text = "Running";
            label_run_xproxyaio.ForeColor = Color.FromArgb(46, 204, 113);

            IGHelpers.ProxyHelpers.ProxyServerOthers_AIO("igaio", true);
            InitTimer_UpdateProxy();
            InitTimer_UpdateProxyStatus();
        }

        #region --Control Panel--

        private void button_control_panel_clear_Click(object sender, EventArgs e)
        {
            if(textBox_control_panel_pc_devices.Text=="")
            {
                MessageBox.Show("Please fill pc_devices id");
                return;
            }

            ClearStatusClaimedBy_Pc_Devices_ID(textBox_control_panel_pc_devices.Text);

            MessageBox.Show("Completed!");
        }

        #endregion

    }
}
