﻿using InstagramLDPlayer.ADBSupport;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Class
{
    internal class IGDevice : IDisposable
    {
        //public string user, phonenum;
        public string deviceID, ld_devicename;
        public bool islogin = false, skip_error;
        public dynamic ig_account;
        bool useproxy;
        public dynamic switch_device;

        public LDDevice ldDevice;

        public IGDevice(dynamic ig_account, LDDevice ldDevice, bool useproxy = true, bool skip_error = false)
        {
            this.ig_account = ig_account;
            //this.user = user;
            //this.deviceID = deviceID;
            this.ld_devicename = ig_account.phone_device;
            this.ldDevice = ldDevice ?? throw new ArgumentNullException(nameof(ldDevice));
            this.skip_error = skip_error;

            this.useproxy = useproxy;

            Program.form.RefreshForm();

        AIOLaunch:
            if (Program.form.isstop)
                return;
            if (this.skip_error)
            {
                try
                {
                    AIOLaunch();
                }
                catch (Exception ex)
                {
                    //var error_path = "";
                    //if (deviceID != null)
                    //{
                    //    //log trace and screen of ldplayer to db
                    //    var screenshoot = ADBSupport.ADBHelpers.ScreenShoot(deviceID);

                    //    if (screenshoot != null)
                    //    {

                    //        error_path = "Temp\\error_" + Guid.NewGuid().GetHashCode().ToString().Replace("-", "") + ".jpg";

                    //        screenshoot.Save(error_path, ImageFormat.Jpeg);
                    //    }

                    //}
                    ADBSupport.LDHelpers.HandleException(ld_devicename, "NewIGDevice" + "|" + ex.Message, ex.StackTrace);
                }
            }
            else
            {
                AIOLaunch();
            }

            if (deviceID != null)
            {
                if (!ADBHelpers.VerifyDeviceIDOnline(deviceID))
                    deviceID = null;
            }

            if (deviceID == null)
            {
                //may be loi luc setproxy

                ADBSupport.LDHelpers.CloseDevice(ld_devicename);

                //IGHelpers.ViewDeviceHelpers.RemovePanel(ld_devicename);

                IGHelpers.ProxyHelpers.ClearLogProxy();

                //Set Thread _ld_index to null


                var index = Thread.GetData(ADBSupport.LDHelpers._ld_index).ToString();

                Thread.SetData(ADBSupport.LDHelpers._ld_index, null);

                //Release device
                IGHelpers.DBHelpers.SetSwitchDeviceStatus(index, "");

                switch_device = null;

                goto AIOLaunch;
            }


            //ADBSupport.ADBHelpers.StartClipperService(deviceID, ld_devicename);


            IGHelpers.DBHelpers.AddActionLog(ld_devicename, "last_use_on_mobile");
        }

        public IGDevice(string deviceName, bool useproxy = true, bool skip_error = false, bool launch = true)
        {
            //check if device is claimed, if not -> claim, return information
            this.ig_account = IGHelpers.DBHelpers.QuickLaunch_IGAccount(deviceName);

            if (this.ig_account == null)
                return;


            this.ld_devicename = ig_account.phone_device;
            this.ldDevice = new LDDevice(ig_account.phone_device);
            this.skip_error = skip_error;

            this.useproxy = useproxy;

            Thread.CurrentThread.Name = "quicklaunch_test";

            if (launch)
            {
                var status = Mode_Switch(); //vi đang test nen luôn launch device switch để có thể test trên bất kỳ máy nào

                if (status == "Success")
                {
                    islogin = true;
                }
            }
            else
            {
                //da launch manual hoac đã có mở sẵn device và login account nên k login lại
                islogin = true;
            }

            IGHelpers.DBHelpers.AddActionLog(ld_devicename, "last_use_on_mobile");
        }

        private void AIOLaunch()
        {
            string status = "";
            if (ig_account.mode == "switch")
            {
                status = Mode_Switch();
            }
            else
            {
                //status = Mode_1_1();
            }

            if (status == "Disabled")
            {
                //Update state
                islogin = false;
                return;
            }
            if (status == "UnsualActivity")
            {
                islogin = false;
                return;
            }
            if (status == "AskConfirmPhone")
            {
                islogin = false;
                return;
            }
            if (status == "ErrorRequest")
            {
                islogin = false;
                return;
            }

            if (status == "UnknownErrorLogin")
            {
                islogin = false;
                return;
            }



            //Scroll top to load new feeds
            ADBSupport.ADBHelpers.RandomScrollScreen(deviceID, 1, 5, 22.1, 78.7, ADBSupport.ADBHelpers.RandomIntValue(500, 800));

            ADBSupport.ADBHelpers.Delay(5, 10);


            //if (ig_account.type == "slave")
            //{
            //    //Launch ig and check message
            //    if (Program.form.skip_error)
            //    {
            //        try
            //        {
            //            Action.ActionHelpers.CheckMessage(this, ig_account.id);
            //        }
            //        catch (Exception ex)
            //        {
            //            //var error_path = "";
            //            //if (deviceID != null)
            //            //{
            //            //    //log trace and screen of ldplayer to db
            //            //    var screenshoot = ADBSupport.ADBHelpers.ScreenShoot(deviceID);


            //            //    error_path = "Temp\\error_" + Guid.NewGuid().GetHashCode().ToString().Replace("-", "") + ".jpg";

            //            //    screenshoot.Save(error_path, ImageFormat.Jpeg);

            //            //}
            //            ADBSupport.LDHelpers.HandleException(ld_devicename, "CheckMessage" + "|" + ex.Message, ex.StackTrace);
            //        }
            //    }
            //    else
            //        Action.ActionHelpers.CheckMessage(this, ig_account.id);
            //}


            islogin = true;
        }

        //private string Mode_1_1()
        //{
        //    //Launch device
        //    this.deviceID = ADBSupport.LDHelpers.LaunchLDPlayer(ld_devicename, useproxy);

        //    //Check if login, if not, try to restore database using ldconsole

        //    return CheckIfLogin_Titanium();
        //}

        //private string CheckIfLogin_Titanium()
        //{
        //    bool run_restore = false;

        //    //reenable in case it's disabled
        //    ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell pm enable com.instagram.android");

        //    //open by intent

        //    var command_openig = ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "https://instagram.com", "com.instagram.android");

        //    if (command_openig.Contains("Error: Activity not started, unable to resolve Intent") && command_openig.Contains("flg=0x10000"))
        //    {
        //        //tuc la bi loi luc install ig nen k the open intent duoc => anh huong den cac tinh nang khac => remove and reinstall instagram
        //        //throw new NotImplementedException();

        //        //check if instagram is install

        //        var list_package = ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell pm list packages");

        //        if (list_package.Contains("instagram"))
        //        {
        //            var backup_path = LDHelpers.LDPlayer_dnplayerdata + ld_devicename + "\\system\\backup\\" + DateTime.Now.ToShortDateString().Replace("/", "-");
        //            //backup ig
        //            ADBSupport.TitaniumBackupHelpers.BackUpIG(deviceID, backup_path, ig_account.user.ToString());
        //            //Upload to ftp server
        //            //IGHelpers.Helpers.FTP_UploadFile(backup_path, ADBSupport.LDHelpers.ftp_dnplayerdata + ld_devicename + "/system/backup/" + DateTime.Now.ToShortDateString().Replace("/", "-"));

        //            //remove ig
        //            ADBSupport.LDHelpers.UninstallApp(ld_devicename, "com.instagram.android");
        //        }

        //        //reinstall ig
        //        IGHelpers.IGADBHelpers.InstallIG(deviceID, ld_devicename);
        //        KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_HOME);

        //    }
        //    CheckIfLogin_Titanium:
        //    command_openig = ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "https://instagram.com", "com.instagram.android");

        //    try
        //    {
        //        //Wait home

        //        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_WaitHome, 2, 10, 5);
        //    }
        //    catch
        //    {
                

        //        if (!run_restore)
        //        {
        //            ADBSupport.TitaniumBackupHelpers.RestoreIG(deviceID, ADBSupport.LDHelpers.GetLatestTitaniumBackupFolder(ld_devicename));

        //            run_restore = true;

        //            command_openig = ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "https://instagram.com", "com.instagram.android");

        //            try
        //            {
        //                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Login_ThisWasMe, 5, 10, 2);
        //                //hit ok

        //                ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.Login_ThisWasMe);
        //            }
        //            catch
        //            {

        //            }


        //            try
        //            {
        //                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_WaitHome, 2, 5, 2);
        //                goto FinishLogin;
        //            }
        //            catch
        //            {
        //                throw new System.ArgumentException("Can't login by restore using Titanium");
        //            }
        //        }
        //    }

        //    try
        //    {
        //        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Login_ThisWasMe, 5, 2, 2);
        //        //hit ok

        //        ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.Login_ThisWasMe);


        //        try
        //        {
        //            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_WaitHome, 2, 5, 2);
        //        }
        //        catch
        //        {
        //            throw new System.ArgumentException("Can't login by restore using Titanium");
        //        }
        //    }
        //    catch
        //    {

        //    }

        //    FinishLogin:

        //    return "Success";
        //}

        private string Mode_Switch()
        {
            isswitch = true;
            //get switch device

            switch_device = ADBSupport.LDHelpers.ReturnSwitchDeviceAvailable();

            Thread.SetData(ADBSupport.LDHelpers._ld_index, switch_device.index);

            //Add switch device to log

            IGHelpers.DBHelpers.AddActionLog(ld_devicename, "switch_device", switch_device.name.ToString());

            //Read device info from deviceinfo.ig

            dynamic deviceinfo = IGHelpers.DBHelpers.GetDeviceInformation(ld_devicename);

            ADBSupport.LDHelpers.RestoreDevice(ld_devicename, deviceinfo);

            //Launch device
            this.deviceID = ADBSupport.LDHelpers.LaunchLDPlayer(ld_devicename, useproxy);


            //clear ig app data
            ADBSupport.ADBHelpers.ClearAPPData(this.deviceID, "com.instagram.android");

            //clear download folder
            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + this.deviceID + " shell \"su -c 'rm -r sdcard/Download/*'\"");

            //check if solve unsualactivity recently

            if(!IGHelpers.DBHelpers.IsSolveUnsualActivityRecently(ig_account.id))
            {
                var status = NewLogin(true);

                if(status=="Success")
                {
                    IGHelpers.DBHelpers.UpdateValueSolvedActivityRecently(ig_account.id);
                }

                return status;
            }

            //var backup_data_folder = ADBSupport.LDHelpers.GetLatestTitaniumBackupFolder(ld_devicename);


            //if (ADBSupport.LDHelpers.GetLatestTitaniumBackupFolder(ld_devicename) != null || ADBSupport.LDHelpers.GetLatestBackupByPL(ld_devicename) != null)
            if (ADBSupport.LDHelpers.GetLatestTitaniumBackupFolder_FTPServer(ld_devicename) != null || ADBSupport.LDHelpers.GetLatestBackupByPL_FromFTPServer(ld_devicename) != null) //--FTPServer
            {
                //Restore ig
                //ADBSupport.TitaniumBackupHelpers.RestoreIG(deviceID, backup_data_folder);

                var status = LoginUsingBackup();

                if (status == "NotLogin")
                    return NewLogin();
                if (status == "UnsualActivity")
                {
                    IGThreads.ThreadHelpers.Do_UnsualActivityAction(ig_account.phone_device);
                    return status;
                }
                if (status == "AskConfirmPhone")
                {
                    IGThreads.ThreadHelpers.Do_UnsualActivityAction(ig_account.phone_device);
                    return status;
                }
                if (status == "Disabled")
                {
                    IGThreads.ThreadHelpers.Do_DISABLE_Action(ig_account.phone_device);
                    return status;
                }
                return status;
            }
            else
            {
                //Login
                return NewLogin();
            }

        }

        private string LoginUsingBackup()
        {
            int retry = 0;

            string backupname = null;
        LoginUsingBackup:

            string backup_type = null, backup_data_path = null;

            //backup_data_path = ADBSupport.LDHelpers.GetLatestBackupByPL(ld_devicename);  //_bkpl_tar.gz backup file

            backup_data_path = ADBSupport.LDHelpers.GetLatestBackupByPL_FromFTPServer(ld_devicename);  //_bkpl_tar.gz backup file  --FTPSERVER

            if (backup_data_path != null)
            {
                backup_type = "BackupByPL";
            }
            else
            {
                //backup_data_path = ADBSupport.LDHelpers.GetLatestTitaniumBackupFolder(ld_devicename); //folder contains backup

                backup_data_path = ADBSupport.LDHelpers.GetLatestTitaniumBackupFolder_FTPServer(ld_devicename); //folder contains backup  --FTPSERVER

                if (backup_data_path!=null)
                    backup_type = "TitaniumBackup";
            }

            if (backup_data_path != null)
            {
                switch (backup_type)
                {

                    case "TitaniumBackup":
                        backupname = Path.GetFileNameWithoutExtension(Directory.GetFiles(backup_data_path).ToList()[0]).Replace(".tar", "");

                        //Restore ig
                        ADBSupport.TitaniumBackupHelpers.RestoreIG(deviceID, backup_data_path);
                        break;
                    case "BackupByPL":
                        //ADBSupport.ADBHelpers.RestoreByPL(deviceID, "com.instagram.android", backup_data_path, true);

                        ADBSupport.ADBHelpers.RestoreByPL_FTPServer(deviceID, "com.instagram.android", backup_data_path, true); //--FTPSERVER
                        break;
                    default:
                        throw new System.ArgumentException("Not found this type of backup");
                }
            }
            IsLogin:
            bool use_another_backup = false;

            bool empty_screen_flag = false;

            var command_openig = ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "https://instagram.com", "com.instagram.android");


            Thread.Sleep(20000);

            Stopwatch watch_wait_home = new Stopwatch();
            watch_wait_home.Start();

            while (true)
            {
                if (watch_wait_home.Elapsed.TotalMinutes > 2.5)
                    break;

                WaitIGHome:


                var guid = "";
                while (guid.Length <= 6)
                    guid = Guid.NewGuid().GetHashCode().ToString();

                //Wait home
                var screen = ADBHelpers.ScreenShoot(deviceID, true, "screenShoot_" + guid.Substring(guid.Length - 6) + ".png");

                //screen.Save(path +"\\" + screenshot_temp + ".png", System.Drawing.Imaging.ImageFormat.Png);

                Point? find_point = null;

                DoFindPoint:
                Bitmap temp_bmp = null;

                //Check if been logged out
                lock (BMResource.LogIn.Login_BeenLogOut)
                {
                    temp_bmp = new Bitmap((Image)BMResource.LogIn.Login_BeenLogOut);
                }

                find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.9);

                if (find_point != null)
                {
                    use_another_backup = true;
                    break;
                }

                //check if login success

                lock (ADBSupport.BMPSource.Global_WaitHome)
                {
                    temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.Global_WaitHome);
                }

                find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.9);

                if (find_point != null)
                    return "Success";

                //check if IGStopped

                lock (ADBSupport.BMPSource.ModeSwitch_IGStopped)
                {
                    temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.ModeSwitch_IGStopped);
                }

                find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.9);

                if (find_point != null)
                {
                    ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Global_ClickHasStopped_Ok);

                    use_another_backup = true;

                    break;
                }

                //Check if supicious login
                lock (ADBSupport.BMPSource.Login_ThisWasMe)
                {
                    temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.Login_ThisWasMe);
                }

                find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.9);

                if (find_point != null)
                {
                    ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.Login_ThisWasMe);

                    goto WaitIGHome;
                }
                //Check if unsual activity
                lock (ADBSupport.BMPSource.ErrorScreen_UnsualActivity)
                {
                    temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.ErrorScreen_UnsualActivity);
                }

                find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.9);

                if (find_point != null)
                {
                    return "UnsualActivity";
                }

                //Check if unsual activity
                lock (BMResource.ErrorScreen.UnsualActivity_1)
                {
                    temp_bmp = new Bitmap((Image)BMResource.ErrorScreen.UnsualActivity_1);
                }

                find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.9);

                if (find_point != null)
                {
                    return "UnsualActivity";
                }

                //check if ask confirm phone

                lock (BMResource.ErrorScreen.ErrorScreen_AskConfirmPhone)
                {
                    temp_bmp = new Bitmap((Image)BMResource.ErrorScreen.ErrorScreen_AskConfirmPhone);
                }

                find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.9);

                if (find_point != null)
                {
                    return "AskConfirmPhone";
                }

                //Check if disable
                lock (ADBSupport.BMPSource.Login_AccountIsDisabled)
                {
                    temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.Login_AccountIsDisabled);
                }

                find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.9);

                if (find_point != null)
                {
                    return "Disabled";
                }

                //Check if login screen
                lock (ADBSupport.BMPSource.Login_LoginBtn)
                {
                    temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.Login_LoginBtn);
                }

                find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.9);

                if (find_point != null)
                {
                    use_another_backup = true;
                    break;
                }

                //Check if empty screen
                lock (BMResource.ErrorScreen.ErrorScreen_EmptyLogin)
                {
                    temp_bmp = new Bitmap((Image)BMResource.ErrorScreen.ErrorScreen_EmptyLogin);
                }

                find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.9);

                if (find_point != null)
                {
                    //recheck again to make sure it empty

                    if (!ADBHelpers.IsImageExisted(deviceID, temp_bmp, 5, 5, 2))
                    {
                        goto WaitIGHome;
                    }

                    if (!empty_screen_flag)
                    {
                        empty_screen_flag = true;
                        var fflogin = new Action.FF_Login(deviceID, ig_account.user, ig_account.password);
                        fflogin.DoAction();
                        watch_wait_home.Restart();


                        goto WaitIGHome;
                    }
                    else
                    {
                        IGThreads.ThreadHelpers.Do_ErrorRequest_Login(ig_account.phone_device);
                        throw new System.ArgumentException("EmptyScreen - Not Found Reason Why Not Login");
                    }
                }

                //check if temporarylocked
                lock (ADBSupport.BMPSource.ErrorScreen_TemporarilyLocked)
                {
                    temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.ErrorScreen_TemporarilyLocked);
                }

                find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.9);

                if (find_point != null)
                {
                    return "Disabled";
                }

                //check if verify your identity
                lock (ADBSupport.BMPSource.ErrorScreen_VerifyYourIdentity)
                {
                    temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.ErrorScreen_VerifyYourIdentity);
                }

                find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.9);

                if (find_point != null)
                {
                    return "Disabled";
                }

                //Check if post removed nudity and click OK
                lock (ADBSupport.BMPSource.ErrorScreen_PostRemoved)
                {
                    temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.ErrorScreen_PostRemoved);
                }
                find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.9);
                if (find_point != null)
                {
                    //add log post removed
                    IGHelpers.DBHelpers.AddActionLog(ld_devicename, "post_removed");


                    if(ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.ErrorScreen_PostRemoved_OK,1,1))
                    {
                        //click ok
                        ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.ErrorScreen_PostRemoved_OK, 0.7);
                    }
                    else
                    {
                        //check if next
                        if(ADBHelpers.IsImageExisted(deviceID,ADBSupport.BMPSource.ErrorScreen_NextBtn,1,1,1,0.7))
                        {
                            ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.ErrorScreen_NextBtn,0.7);


                            //if account may deleted
                            if(ADBHelpers.IsImageExisted(deviceID,ADBSupport.BMPSource.ErrorScreen_YourAccountMayDeleted,1,1))
                            {
                                //set source stop => no post any more on this account
                                IGHelpers.DBHelpers.UpdateSourceUserByAccountID(ig_account.id, "STOP");

                                ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.ErrorScreen_PostRemoved_OK, 0.7);
                            }
                        }
                    }

                    
                    ADBSupport.ADBHelpers.Delay(6, 10);
                    goto WaitIGHome;
                }

                //Check if story removed nudity and click OK
                lock (ADBSupport.BMPSource.ErrorScreen_StoryRemoved)
                {
                    temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.ErrorScreen_StoryRemoved);
                }
                find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.8);
                if (find_point != null)
                {
                    //add log post removed
                    IGHelpers.DBHelpers.AddActionLog(ld_devicename, "post_removed");

                    if (ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.ErrorScreen_PostRemoved_OK, 1, 1))
                    {
                        //click ok
                        ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.ErrorScreen_PostRemoved_OK, 0.8);
                    }
                    else
                    {
                        //check if next
                        if (ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.ErrorScreen_NextBtn, 1, 1,1,0.7))
                        {
                            ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.ErrorScreen_NextBtn, 0.7);


                            //if account may deleted
                            if (ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.ErrorScreen_YourAccountMayDeleted, 1, 1) || ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.ErrorScreen_YourAccountMayDeleted_1, 1, 1))
                            {
                                //set source stop => no post any more on this account
                                IGHelpers.DBHelpers.UpdateSourceUserByAccountID(ig_account.id, "STOP");

                                if(ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.ErrorScreen_PostRemoved_OK,1,1,1,0.7))
                                    ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.ErrorScreen_PostRemoved_OK, 0.7);
                                else if (ADBHelpers.IsImageExisted(deviceID, ADBSupport.BMPSource.ErrorScreen_Ok_1, 1, 1, 1, 0.9))
                                    ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.ErrorScreen_Ok_1, 0.7);
                            }
                        }
                    }
                    ADBSupport.ADBHelpers.Delay(6, 10);
                    goto WaitIGHome;
                }
            }


            if(!use_another_backup)
            {
                throw new System.ArgumentException("Not Found Reason Why Not Login");
            }

            switch(backup_type)
            {
                case "TitaniumBackup":
                    //remove failed backup
                    //LDHelpers.RemoveTitaniumBackupByName(ld_devicename, backupname);
                    LDHelpers.RemoveTitaniumBackupByName_FTPServer(ld_devicename, backupname); //--FTPServer
                    break;
                case "BackupByPL":
                    //File.Delete(backup_data_path);
                    PLAutoHelper.FTPHelper.FTP_DeleteFile(LDHelpers.pLFTP, backup_data_path); //--FTPServer
                    break;
                default:
                    throw new System.ArgumentException("Not found this type of backup");
            }

            retry++;

            if (retry < 3)
            {
                //clear ig app data
                ADBSupport.ADBHelpers.ClearAPPData(this.deviceID, "com.instagram.android");
                ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell am force-stop com.keramidas.TitaniumBackup");
                goto LoginUsingBackup;
            }

            return "NotLogin";
        }

        private string CheckIfLogin_Switch()
        {
            //Check if login
            bool login_again = false;
        IsLogin:
            var command_openig = ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "https://instagram.com", "com.instagram.android");
            try
            {
                //Wait home

                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_WaitHome, 3, 10, 5);

            }
            catch
            {
                //check if instagram has stopped

                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.ModeSwitch_IGStopped, 5, 5, 2);
                    //hit ok

                    ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Global_ClickHasStopped_Ok);


                }
                catch
                {

                }

                //Check if supicious login
                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Login_ThisWasMe, 1, 1, 1);
                    //hit ok

                    ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.Login_ThisWasMe);

                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_WaitHome, 2, 10, 5);

                    //close instagram app

                    ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell am force-stop com.instagram.android");

                    goto IsLogin;
                }
                catch
                {

                }

                //throw new System.ArgumentException("Not Found Reason Why Not Login");

                //clear ig app data
                ADBSupport.ADBHelpers.ClearAPPData(deviceID, "com.instagram.android");

                command_openig = ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "https://instagram.com", "com.instagram.android");


                if (login_again)
                    throw new System.ArgumentException("Can't login after try login again");
                //login to account
                try
                {
                    var login = new Action.Login(deviceID, ig_account.user, ig_account.password);
                    login.DoAction();

                    //Titanium backup
                    IGThreads.ThreadHelpers.DoTitanBackup(this, ig_account);
                }
                catch (Exception ex)
                {
                    if (ex.Message == "Account Is Disabled")
                    {
                        return "Disabled";
                    }

                    if (ex.Message == "UnsualActivity")
                    {
                        IGThreads.ThreadHelpers.Do_UnsualActivityAction(ig_account.phone_device);
                        return "UnsualActivity";
                    }

                    if (ex.Message == "ErrorRequest")
                    {
                        IGThreads.ThreadHelpers.Do_ErrorRequest_Login(ig_account.phone_device);
                        return "ErrorRequest";
                    }

                    throw;

                    //return "UnknownErrorLogin";
                }
                login_again = true;
                goto IsLogin;
            }

            return "Success";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="remove_all_daily_backup">set true after solve unsual activity</param>
        /// <returns></returns>
        private string NewLogin(bool remove_all_daily_backup = false)
        {
            try
            {
                var login = new Action.Login(deviceID, ig_account.user, ig_account.password);
                login.DoAction();

                //check if correct account
                if (!IGThreads.ThreadHelpers.IsCorrectAccount(deviceID, ig_account.user))
                {
                    throw new System.ArgumentException("Can't Backup Because Account Isn't CORRECT");
                }

                var backup_path = "/" + ld_devicename + "/system/backup/daily"; //--FTPServer
                ADBHelpers.BackupByPL_FTPServer(deviceID, "com.instagram.android", backup_path);

                ////Titanium backup
                //IGThreads.ThreadHelpers.DoTitanBackup(this, ig_account);

                if (remove_all_daily_backup)
                {
                    ////Clear all daily backup
                    //var daily_backup_path = LDHelpers.LDPlayer_dnplayerdata + ld_devicename + "\\system\\backup\\daily";

                    //if (Directory.Exists(daily_backup_path))
                    //    Directory.Delete(daily_backup_path, true);

                    if (PLAutoHelper.FTPHelper.FTP_IsDirectoryExisted(ADBSupport.LDHelpers.pLFTP, backup_path))
                        PLAutoHelper.FTPHelper.FTP_DeleteDirectory(ADBSupport.LDHelpers.pLFTP, backup_path);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == "Account Is Disabled")
                {
                    IGThreads.ThreadHelpers.Do_DISABLE_Action(ig_account.phone_device);
                    return "Disabled";
                }

                if (ex.Message == "UnsualActivity")
                {
                    IGThreads.ThreadHelpers.Do_UnsualActivityAction(ig_account.phone_device);
                    return "UnsualActivity";
                }

                if (ex.Message == "ErrorRequest")
                {
                    IGThreads.ThreadHelpers.Do_ErrorRequest_Login(ig_account.phone_device);
                    return "ErrorRequest";
                }

                throw;

                //return "UnknownErrorLogin";
            }


            return "Success";
        }


        #region --Use for switch device--

        public bool isswitch = false;
        public int ld_index=-1; //create public List to mapping ld_index with deviceName, when execute ld command, check if ld_index!=-1 thì dùng ld_index để execute command



        #endregion

        public void Dispose()
        {
            switch(isswitch)
            {
                case false:
                    ADBSupport.LDHelpers.CloseDevice(ld_devicename);

                    //IGHelpers.ViewDeviceHelpers.RemovePanel(ld_devicename);

                    IGHelpers.ProxyHelpers.ClearLogProxy();
                    break;
                case true:

                    //backup ig app to daily folder

                    if (islogin)
                    {
                        if (this.skip_error)
                        {
                            try
                            {

                                //var backup_path = LDHelpers.LDPlayer_dnplayerdata + ld_devicename + "\\system\\backup\\daily";

                                var backup_path = "/" + ld_devicename + "/system/backup/daily"; //--FTPServer

                                //TitaniumBackupHelpers.BackUpIG(deviceID, backup_path, ig_account.user, false); //false vi da check dau vao roi
                                //ADBHelpers.BackupByPL(deviceID, "com.instagram.android", backup_path);
                                ADBHelpers.BackupByPL_FTPServer(deviceID, "com.instagram.android", backup_path);
                            }
                            catch (Exception ex)
                            {
                                //var error_path = "";
                                //if (deviceID != null)
                                //{
                                //    //log trace and screen of ldplayer to db
                                //    var screenshoot = ADBSupport.ADBHelpers.ScreenShoot(deviceID);

                                //    if (screenshoot != null)
                                //    {

                                //        error_path = "Temp\\error_" + Guid.NewGuid().GetHashCode().ToString().Replace("-", "") + ".jpg";

                                //        screenshoot.Save(error_path, ImageFormat.Jpeg);
                                //    }

                                //}
                                ADBSupport.LDHelpers.HandleException(ld_devicename, "NewIGDevice" + "|" + ex.Message, ex.StackTrace);
                            }
                        }
                        else
                        {

                            //var backup_path = LDHelpers.LDPlayer_dnplayerdata + ld_devicename + "\\system\\backup\\daily";

                            var backup_path = "/" + ld_devicename + "/system/backup/daily"; //--FTPServer

                            //TitaniumBackupHelpers.BackUpIG(deviceID, backup_path, ig_account.user, false); //false vi da check dau vao roi
                            //ADBHelpers.BackupByPL(deviceID, "com.instagram.android", backup_path);
                            ADBHelpers.BackupByPL_FTPServer(deviceID, "com.instagram.android", backup_path);
                        }
                    }
                    //clear ig app data
                    ADBSupport.ADBHelpers.ClearAPPData(deviceID, "com.instagram.android");

                    //clear download folder
                    ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell \"su -c 'rm -r sdcard/Download/*'\"");


                    ADBSupport.LDHelpers.CloseDevice(ld_devicename);

                    //IGHelpers.ViewDeviceHelpers.RemovePanel(ld_devicename);

                    IGHelpers.ProxyHelpers.ClearLogProxy();

                    //Set Thread _ld_index to null

                    //var ld_index_val = Thread.GetData(ADBSupport.LDHelpers._ld_index);

                    //if (ld_index_val == null)
                    //    break;

                    var index = Thread.GetData(ADBSupport.LDHelpers._ld_index).ToString();

                    Thread.SetData(ADBSupport.LDHelpers._ld_index, null);

                    //Release device
                    IGHelpers.DBHelpers.SetSwitchDeviceStatus(index, "");

                    break;
            }

            IGHelpers.DBHelpers.AddActionLog(ld_devicename, "last_use_on_mobile");

            Program.form.RefreshForm();
        }
    }
}
