﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Class
{
    class LaunchDevice
    {
        public string deviceName;
        public string status;

        public LaunchDevice(string deviceName, string status)
        {
            this.deviceName = deviceName;
            this.status = status;
        }
    }
}
