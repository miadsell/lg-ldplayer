﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Class
{
    class RecPercentage
    {
        public dynamic point;
        public double height, width;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point">in percentage</param>
        /// <param name="width">in percentage</param>
        /// <param name="height">in percentage</param>
        public RecPercentage(dynamic point, double width, double height)
        {
            this.point = point ?? throw new ArgumentNullException(nameof(point));
            this.height = height;
            this.width = width;
        }

        public Rectangle ConvertToRectangle(int device_width, int device_height)
        {
            var point_x = (int) ((((double)device_width) / 100) * (double.Parse(point.X.ToString())));
            var point_y = (int) ((((double)device_height) / 100) * (double.Parse(point.Y.ToString())));

            var height_rec = (int) ( (double) device_height / 100 * height);
            var width_rec = (int) ( (double) device_width / 100 * width);

            return new Rectangle(point_x, point_y, width_rec, height_rec);
        }

    }
}
