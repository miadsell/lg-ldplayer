﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Class
{
    class LDDevice
    {
        public string ld_name, manufacturer, model, pnumber, imei, imsi, simserial, androidid, mac;

        public LDDevice(string ld_name)
        {
            this.ld_name = ld_name ?? throw new ArgumentNullException(nameof(ld_name));
        }

        public LDDevice(string ld_name, string manufacturer, string model, string pnumber, string imei, string imsi, string simserial, string androidid, string mac)
        {
            this.ld_name = ld_name ?? throw new ArgumentNullException(nameof(ld_name));
            this.manufacturer = manufacturer ?? throw new ArgumentNullException(nameof(manufacturer));
            this.model = model ?? throw new ArgumentNullException(nameof(model));
            this.pnumber = pnumber ?? throw new ArgumentNullException(nameof(pnumber));
            this.imei = imei ?? throw new ArgumentNullException(nameof(imei));
            this.imsi = imsi ?? throw new ArgumentNullException(nameof(imsi));
            this.simserial = simserial ?? throw new ArgumentNullException(nameof(simserial));
            this.androidid = androidid ?? throw new ArgumentNullException(nameof(androidid));
            this.mac = mac ?? throw new ArgumentNullException(nameof(mac));
        }


    }
}
