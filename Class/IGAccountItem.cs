﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Class
{
    class IGAccountItem
    {
        //public string  user, fullname, note, state, status, niche, settings, launch;
        public string dgcol_id { get; set; }
        public string dgcol_user { get; set; }
        public string dgcol_fullname { get; set; }
        public string dgcol_note { get; set; }
        public string dgcol_state { get; set; }
        public string dgcol_status { get; set; }
        public string dgcol_niche { get; set; }
        public string dgcol_settings { get; set; }
        public string dgcol_launch { get; set; }
        public string dgcol_close { get; set; }


        public IGAccountItem(string id, string niche, string user, string fullname, string note, string state, string status)
        {
            this.dgcol_id = id;
            this.dgcol_niche = niche;
            this.dgcol_user = user;
            this.dgcol_fullname = fullname;
            this.dgcol_note = note;
            this.dgcol_state = state;
            this.dgcol_status = status;
            this.dgcol_settings = "Settings";
            this.dgcol_launch = "Launch";
            this.dgcol_close = "Close";
        }

    }
}
