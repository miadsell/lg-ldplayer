﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.Class
{
    //define click range in image
    class ClickRange
    {
        public int width, height;

        public ClickRange(int width, int height)
        {
            this.width = width;
            this.height = height;
        }
    }
}
