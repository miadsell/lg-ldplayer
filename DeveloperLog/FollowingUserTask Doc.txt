﻿** Quản lý following + rule

 *Get Account

 - SpecificTask table: FollowingUser: Do
 - Next session time: reached
 - Current date following < Targeting following

 ==> mở file followinguser_getaccount.sql viết tiếp


 ***Get Scrape User to Follow

 --***Must Condition
-- Bio: !contains shop,order,zalo,shopee, 'cod ', feedback, phone, online, ship, store
-- Following: <1000 ->
-- Follower: <1000 ->
-- IsPrivate: false -> true


-- Bio: has bio -> none bio
-- Recent Post: <30 days, <90 days, others
-- Stories: has stories - none
-- AVGLike: <10 -> <20 -> <30
-- AVG Comments: >0 -> others

----Update score

Use Score to get account by order

 *Stage

 - following-warmlite
 	+ engage state: normal
 	+ condition
 		. no following-warmlite before
 	+ settings:
 		. random 3-7 following
 	+ days length: 3 days (k)
 - following-warmup
 	+ engage state: normal
 	+ condition
 		. following-warmlite >= 3 days (k days)
 		. not do following-warmup
 	+ settings
 		. random 5-10
 		. increase 5 per days
 	+ days length: 5 days (z)
 - following-default
 	+ engage state: normal
 	+ condition
 		. following-warmup >= 5 days (z)
 	+ settings
 		. increase daily X following until reach Y
 	+ days length: no length

 - action blocked
 		. Sleep 2 days (just sleep for following task) - Added to condition when get account
 		. Start from small following per day = latest targeting following / 2
 		. increase daily X : X = increase của followingtype tương ứng với account
 			+ Trường hợp là stage following-warmlite: thì start bằng settings của warmlite
 - unsual activity

*Session, sleep between action

*Use actionlog to set target follower current date

List tags (save to action col):

target_following: set target following for current date
following_task : use for log following action from following task (phân biệt với following dùng engage)
following_next_session: next session will be do when this time reach
block_following_session_count: set blocked quantity for this run
new_following_session: use when start new session and check if end of blocked_session
following_finished: finish today, mean following_task count >= target_following

------Exception

- Sleep task for 3 days if get Action Blocked or Unsual Activity (use db to check when get account)

------Coding

- Get Account to do Following
- Get following settings and set settings if needed

- following-warmlite:
	random|3-7***action_per_session|1-3***sleep_session|3000-5000***sleep_action|30-120***days|3***block_session|1-3***sleep_per_block|3600-7200
- following-warmup:
    random|5-10***action_per_session|2-6***sleep_session|1500-3000***sleep_action|30-120***days|5***block_session|2-5***sleep_per_block|1800-2700
- following-default
    increase|10-12***action_per_session|3-10***sleep_session|720-1500***sleep_action|30-120***max_following|90-100***block_session|4-10***sleep_per_block|1800-2700

Explain:
    - random: sổ follower khởi đầu
    - action_per_session: random số follow thực hiện trong 1 session
    - sleep_session: thời gian giữa các session
    - sleep_action: thời gian sleep giữa các action
    - days: thời gian thực hiện stage này
    - max_following: số action duoc thuc hien tối đa trong stage này
    - block_session: số session thực hiện trong 1 block (sau mỗi block sẽ sleep 1 khoảng thời gian dài hơn)
    - sleep_per_block: sleep giữa các block
------Testing
accountId: 100
- Change engagetype warmuplite -> default