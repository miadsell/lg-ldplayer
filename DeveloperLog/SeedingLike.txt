﻿** Khi có comment cần push like
- Set target like: random from range (tìm cách set range?????)
- Random time to do LIKE in time range
- Tiếp tục monitor post để xem comment có số like nhiều nhất là bao nhiêu, nếu top like thay đổi -> generate thêm like để đẩy top

**ACTION

-Scroll comment để tìm comment có user trùng với user_screenshoot_ldplayer trong table account_info

** Quản lý data

- Scan semi_autocomment_task per x time
	+ Check if new comment was posted and not generate target like
		. Set target like
			- Nếu như chưa có top like thì lấy target like là trung bình 3 post trước đó
			- Nếu có top like thì target like = random from targetlike -3 to targetlike +3
	+ With old comment
		. Check if seeding status if finish
			- If finish, check and set target like again if needed
- Do task
	+ First time, if next_seeding_like is null, set next
	+ Do like if current time meet next_seeding_like

** Table schema structure

target_like: target like to seeding
current_like: current seeding like
next_seeding_like: next time to do like
seeding_like_status:
	+ Finished: finish seeding
		. If finish seeding, can check target like again
	+ ASSIGNING : đang chờ assign account làm task
	+ Seeding: current doing
	+ Null: just posted

** [action] name

action: seeding_like
value: semiauto_comment id