﻿------New Settings

- autocomment-warmlite:
	random|1-3***action_per_session|1-1***sleep_session|3000-5000***sleep_action|30-120***days|3***block_session|1-1***sleep_per_block|3600-7200
- autocomment-warmup:
    random|2-5***action_per_session|1-1***sleep_session|1500-3000***sleep_action|30-120***days|5***block_session|1-1***sleep_per_block|1800-2700
- autocomment-default
    random|2-5***action_per_session|1-1***sleep_session|1500-3000***sleep_action|30-120***max_autocomment|5-6***block_session|1-1***sleep_per_block|1800-2700

***Actionlog

commentpost_finished: finish today, mean commentpost_task count >= commentpost_following