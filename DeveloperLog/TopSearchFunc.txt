﻿*** Nghiên cứu lên top search

**Generate random action to push main store to top

- Lên list cấu trúc trước, sau đó mỗi lần đẩy bài sẽ generate kịch bản dựa trên cấu trúc

SEARCH|keyword : search keyword
SCROLL|time : random scroll in time
TOP : click top tab
ACCOUNTS : click accounts tab
TAGS : click tags tab
FIND@MAIN|bitmap : scroll to find main account
CLICK@MAIN|bitmap : click main account
FIND@POST|bitmap : scroll to find target post (gồm co full post hoặc thumbnail post)
FIND@TAG