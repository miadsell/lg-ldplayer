﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.ADBSupport
{
    static class BMPSource
    {

        #region --Cross--

        static public Bitmap WaitDeviceOpen = (Bitmap)Bitmap.FromFile(@"ImageSource\WaitDeviceOpen.png");
        static public Bitmap ClickHasStopped_Ok = (Bitmap)Bitmap.FromFile(@"ImageSource\ClickHasStopped_Ok.png");



        #endregion

        #region --IGScreen--

        static public Bitmap Goto_ProfileScreen = (Bitmap)Bitmap.FromFile(@"ImageSource\Global\GoTo_ProfileScreen.png");
        static public Bitmap Goto_EditProfileScreen = (Bitmap)Bitmap.FromFile(@"ImageSource\Global\Goto_EditProfileScreen.png");
        static public Bitmap Goto_PostScreenBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\Global\Goto_PostScreenBtn.png");

        static public Bitmap WaitProfileScreen = (Bitmap)Bitmap.FromFile(@"ImageSource\Global\WaitProfileScreen.png");
        static public Bitmap WaitHome = (Bitmap)Bitmap.FromFile(@"ImageSource\Global\WaitHome.png");
        static public Bitmap WaitSearchTab = (Bitmap)Bitmap.FromFile(@"ImageSource\Global\WaitSearchTab.png");



        static public Bitmap isOnTopHome = (Bitmap)Bitmap.FromFile(@"ImageSource\Global\isOnTopHome.png");
        //static public Bitmap ErrorScreen_VerifyHuman = (Bitmap)Bitmap.FromFile(@"ImageSource\ErrorScreen\VerifyHuman.png");
        static public Bitmap ErrorScreen_UnsualActivity = (Bitmap)Bitmap.FromFile(@"ImageSource\ErrorScreen\UnsualActivity.png");
        static public Bitmap ErrorScreen_PostRemoved = (Bitmap)Bitmap.FromFile(@"ImageSource\ErrorScreen\PostRemoved.png");
        static public Bitmap ErrorScreen_StoryRemoved = (Bitmap)Bitmap.FromFile(@"ImageSource\ErrorScreen\StoryRemoved.png");
        static public Bitmap ErrorScreen_PostRemoved_OK = (Bitmap)Bitmap.FromFile(@"ImageSource\ErrorScreen\PostRemoved_OK.png");


        #endregion

        #region --ProxyDroid--

        static string proxymode = "";

        static public Bitmap ProxyOff = (Bitmap)Bitmap.FromFile(@"ImageSource\ProxyDroid\" + proxymode + "ProxyOff.png");
        static public Bitmap ProxyOn = (Bitmap)Bitmap.FromFile(@"ImageSource\ProxyDroid\" + proxymode + "ProxyOn.png");
        static public Bitmap ProxyDroidPort = (Bitmap)Bitmap.FromFile(@"ImageSource\ProxyDroid\" + proxymode + "ProxyDroidPort.png");
        static public Bitmap PortPopupEdit = (Bitmap)Bitmap.FromFile(@"ImageSource\ProxyDroid\" + proxymode + "PortPopupEdit.png");
        static public Bitmap ClearedPort = (Bitmap)Bitmap.FromFile(@"ImageSource\ProxyDroid\" + proxymode + "ClearedPort.png");
        static public Bitmap EditPortOK = (Bitmap)Bitmap.FromFile(@"ImageSource\ProxyDroid\" + proxymode + "EditPortOK.png");
        static public Bitmap Socks5Option = (Bitmap)Bitmap.FromFile(@"ImageSource\ProxyDroid\" + proxymode + "Socks5Option.png");
        static public Bitmap ProxyType = (Bitmap)Bitmap.FromFile(@"ImageSource\ProxyDroid\" + proxymode + "ProxyType.png");
        static public Bitmap VerifySetSocks5 = (Bitmap)Bitmap.FromFile(@"ImageSource\ProxyDroid\" + proxymode + "VerifySetSocks5.png");
        static public Bitmap ProxyTypeTitle = (Bitmap)Bitmap.FromFile(@"ImageSource\ProxyDroid\" + proxymode + "ProxyTypeTitle.png");
        static public Bitmap ProxySwitchTitle = (Bitmap)Bitmap.FromFile(@"ImageSource\ProxyDroid\" + proxymode + "ProxySwitchTitle.png");

        #endregion

        #region --Register Instagram

        static public Bitmap IconApp = (Bitmap)Bitmap.FromFile(@"ImageSource\IG_Icon.png");
        //static public Bitmap AppLoading = (Bitmap)Bitmap.FromFile(@"ImageSource\IG_WaitOpenApp.png");
        static public Bitmap CreateNewAccount = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\IG_Button_CreateNewAccount.png");
        static public Bitmap Wait_FillPhoneScreen = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\Wait_FillPhoneScreen.png");
        static public Bitmap Next = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\IG_Button_Next.png");
        static public Bitmap Option84 = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\IG_Option_84VN.png");
        static public Bitmap WaitConfirmCodeBox = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\IG_Text_WaitConfirmCodeBox.png");
        static public Bitmap WaitNamePass = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\IG_Text_WaitNamePass.png");
        static public Bitmap WaitBirthday = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\IG_Text_WaitBirthday.png");
        static public Bitmap FollowButton = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\FollowButton.png");
        static public Bitmap WaitSuggestion = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\WaitSuggestion.png");
        static public Bitmap SuggestionArrow = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\SuggestionArrow.png");
        static public Bitmap Profile = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\Profile.png");
        static public Bitmap WaitPrivacy = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\IG_WaitPrivacy.png");
        static public Bitmap WaitFindFBFriends = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\IG_WaitFindFBFriends.png");
        static public Bitmap WaitAddPhoto = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\IG_WaitAddPhoto.png");
        static public Bitmap WaitGoBackFillPhone = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\WaitGoBackFillPhone.png");


        //---Verify Human---

        static public Bitmap VerifyHuman = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\ErrorScreen\VerifyHuman.png");
        static public Bitmap AddPhoneNumber = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\ErrorScreen\AddPhoneNumber.png");
        static public Bitmap SendConfirmation = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\ErrorScreen\SendConfirmation.png");
        static public Bitmap EnterCode = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\ErrorScreen\EnterCode.png");
        static public Bitmap SubmitCode = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\ErrorScreen\SubmitCode.png");
        static public Bitmap WelcomeToIG = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\ErrorScreen\WelcomeToIG.png");
        static public Bitmap NextBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\ErrorScreen\NextBtn.png");
        static public Bitmap BackToInstagram = (Bitmap)Bitmap.FromFile(@"ImageSource\Register\ErrorScreen\BackToInstagram.png");

        #endregion

        #region --UserName

        //GetUsername()

        static public Bitmap User_ProfileButton = (Bitmap)Bitmap.FromFile(@"ImageSource\GetUserName\ProfileButton.png");
        static public Bitmap User_WaitEditProfile = (Bitmap)Bitmap.FromFile(@"ImageSource\GetUserName\WaitEditProfile.png");
        static public Bitmap User_WaitUserNameLabel = (Bitmap)Bitmap.FromFile(@"ImageSource\GetUserName\WaitUserNameLabel.png");

        #endregion

        #region --UpdateBio--


        static public Bitmap SwitchAccount = (Bitmap)Bitmap.FromFile(@"ImageSource\UpdateBio\ClickSwitchAccount.png");
        static public Bitmap WaitBioInput = (Bitmap)Bitmap.FromFile(@"ImageSource\UpdateBio\WaitBioInput.png");
        static public Bitmap ClickUpdateBio = (Bitmap)Bitmap.FromFile(@"ImageSource\UpdateBio\ClickUpdateBio.png");
        static public Bitmap EmptyBio = (Bitmap)Bitmap.FromFile(@"ImageSource\UpdateBio\EmptyBio.png");
        static public Bitmap BioSelectAll = (Bitmap)Bitmap.FromFile(@"ImageSource\UpdateBio\BioSelectAll.png");
        static public Bitmap BioPaste = (Bitmap)Bitmap.FromFile(@"ImageSource\UpdateBio\BioPaste.png");
        static public Bitmap WaitHtmlViewer = (Bitmap)Bitmap.FromFile(@"ImageSource\UpdateBio\WaitHtmlViewer.png");


        #endregion

        #region --UpdateProfileImage--

        static public Bitmap ClickChangeProfilePhoto = (Bitmap)Bitmap.FromFile(@"ImageSource\UpdateProfileImage\ClickChangeProfilePhoto.png");
        static public Bitmap ClickNewProfilePhoto = (Bitmap)Bitmap.FromFile(@"ImageSource\UpdateProfileImage\ClickNewProfilePhoto.png");
        static public Bitmap WaitGallery = (Bitmap)Bitmap.FromFile(@"ImageSource\UpdateProfileImage\WaitGallery.png");
        static public Bitmap SaveImageProfile = (Bitmap)Bitmap.FromFile(@"ImageSource\UpdateProfileImage\SaveImage.png");
        static public Bitmap StartingDownload = (Bitmap)Bitmap.FromFile(@"ImageSource\UpdateProfileImage\StartingDownload.png");

        #endregion

        #region--Device masker--

        static public Bitmap CurrentNetworkCountry = (Bitmap)Bitmap.FromFile(@"ImageSource\DeviceMasker\Wait_CurrentNetworkCountry.png");
        static public Bitmap CurrentNetworkType = (Bitmap)Bitmap.FromFile(@"ImageSource\DeviceMasker\Wait_CurrentNetworkType.png");
        static public Bitmap CurrentGoogleEmail = (Bitmap)Bitmap.FromFile(@"ImageSource\DeviceMasker\Wait_CurrentGoogleEmail.png");
        static public Bitmap RandomBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\DeviceMasker\RandomBtn.png");
        static public Bitmap ApplyAll = (Bitmap)Bitmap.FromFile(@"ImageSource\DeviceMasker\ApplyAll.png");
        static public Bitmap Reboot = (Bitmap)Bitmap.FromFile(@"ImageSource\DeviceMasker\Reboot.png");
        static public Bitmap ClickSettings = (Bitmap)Bitmap.FromFile(@"ImageSource\DeviceMasker\ClickSettings.png");
        static public Bitmap ImportSettings = (Bitmap)Bitmap.FromFile(@"ImageSource\DeviceMasker\ImportSettings.png");
        static public Bitmap WaitSettingsPage = (Bitmap)Bitmap.FromFile(@"ImageSource\DeviceMasker\WaitSettingsPage.png");
        static public Bitmap DeviceIDMaskerLite = (Bitmap)Bitmap.FromFile(@"ImageSource\DeviceMasker\DeviceIDMaskerLite.png");
        static public Bitmap ClickSettingButton = (Bitmap)Bitmap.FromFile(@"ImageSource\DeviceMasker\ClickSettingButton.png");

        #endregion

        #region --PostEngage--

        static public Bitmap Post_DropDownMenu = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Post_DropDownMenu.png");
        static public Bitmap LikeButton = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\LikeButton.png");
        static public Bitmap Liked_Button = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Liked_Button.png");
        static public Bitmap CommentButton = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\CommentButton.png");
        static public Bitmap SaveCollectionButton = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\SaveCollectionButton.png");
        static public Bitmap ViewCommentLink = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\ViewCommentLink.png");
        static public Bitmap MoreCaption = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\MoreCaption.png");
        static public Bitmap AddACommentBox = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\AddACommentBox.png");
        static public Bitmap AddCommentButton = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\AddCommentButton.png");
        static public Bitmap Post_TimeAgo = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\TimeAgo.png");

        static public Bitmap WaitViewPostPage = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\WaitViewPostPage.png");

        static public Bitmap ExploreTab = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\ExploreTab.png");
        static public Bitmap NextDropDown = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\NextDropDown.png");

        static public Bitmap LoadNewPosts = (Bitmap)Bitmap.FromFile(@"ImageSource\Global\NewPosts.png");

        //Swipe post

        static public Bitmap SwipeStartDot = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\SwipeHorizontal\SwipeStartDot.png");
        static public Bitmap SwipeLastDot = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\SwipeHorizontal\SwipeLastDot.png");

        //Comment

        static public Bitmap LikeCommentBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\LikeCommentBtn.png");
        static public Bitmap ClickPost = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\ClickPost.png");
        static public Bitmap PasteBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\PasteBtn.png");
        static public Bitmap ViewReplies = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\ViewReplies.png");
        static public Bitmap ViewPreviousReplies = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\ViewPreviousReplies.png");
        static public Bitmap SelectAllEsEditor = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\SelectAllEsEditor.png");
        static public Bitmap TapSettingEsEditor = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\TapSettingEsEditor.png");
        static public Bitmap EditEsEditor = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\EditEsEditor.png");
        static public Bitmap CopyEsEditor = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\CopyEsEditor.png");
        static public Bitmap WaitEsEditor = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\WaitEsEditor.png");
        static public Bitmap CopyLink = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\CopyLink.png");
        static public Bitmap WaitLoading_CopyLink = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\WaitLoading_CopyLink.png");
        static public Bitmap ConfirmYesEsEditor = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\ConfirmYesEsEditor.png");
        static public Bitmap BackEsEditor = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\BackEsEditor.png");
        static public Bitmap HtmlViewerOpenTxt = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\HtmlViewerOpenTxt.png");
        static public Bitmap HtmlViewerSelect = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\HtmlViewerSelect.png");
        static public Bitmap HtmlViewerCopy = (Bitmap)Bitmap.FromFile(@"ImageSource\Post\Comment\HtmlViewerCopy.png");


        //Video Dark Feed

        static public Bitmap ExploreVideoFeed = (Bitmap)Bitmap.FromFile(@"ImageSource\Explore\ExploreVideoFeed.png");
        static public Bitmap LikeBtn_Dark = (Bitmap)Bitmap.FromFile(@"ImageSource\Explore\LikeBtn_Dark.png");

        #endregion

        #region --Swipe Suggested For You--

        static public Bitmap SuggestedForYou = (Bitmap)Bitmap.FromFile(@"ImageSource\SuggestedForYou\SuggestedForYou.png");
        static public Bitmap Suggested_Follow = (Bitmap)Bitmap.FromFile(@"ImageSource\SuggestedForYou\FollowBtn.png");


        #endregion

        #region --Swipe Stories--

        static public Bitmap StoryElement = (Bitmap)Bitmap.FromFile(@"ImageSource\SimulateStories\StoryElement.png");
        static public Bitmap StoryElementViewed = (Bitmap)Bitmap.FromFile(@"ImageSource\SimulateStories\StoryElementViewed.png");
        static public Bitmap YourStory = (Bitmap)Bitmap.FromFile(@"ImageSource\SimulateStories\YourStory.png");
        static public Bitmap DirectMess = (Bitmap)Bitmap.FromFile(@"ImageSource\SimulateStories\DirectMess.png");

        #endregion

        #region --Search tab--

        static public Bitmap SearchBox = (Bitmap)Bitmap.FromFile(@"ImageSource\SearchTab\SearchBox.png");
        static public Bitmap Tab_Accounts = (Bitmap)Bitmap.FromFile(@"ImageSource\SearchTab\Tab_Accounts.png");
        static public Bitmap Tab_Tags = (Bitmap)Bitmap.FromFile(@"ImageSource\SearchTab\Tab_Tags.png");
        static public Bitmap Tab_Places = (Bitmap)Bitmap.FromFile(@"ImageSource\SearchTab\Tab_Places.png");
        static public Bitmap Tab_Top = (Bitmap)Bitmap.FromFile(@"ImageSource\SearchTab\Tab_Top.png");
        static public Bitmap BlueTick = (Bitmap)Bitmap.FromFile(@"ImageSource\SearchTab\BlueTick.png");

        #endregion

        #region --Follow Action--

        static public Bitmap Follow1Btn = (Bitmap)Bitmap.FromFile(@"ImageSource\FollowPeople\Follow1Btn.png");
        static public Bitmap Follow2Btn = (Bitmap)Bitmap.FromFile(@"ImageSource\FollowPeople\Follow2Btn.png");
        static public Bitmap Following_UserNotFound = (Bitmap)Bitmap.FromFile(@"ImageSource\FollowPeople\UserNotFound.png");


        #endregion

        #region --Publish--

        static public Bitmap SaveImage = (Bitmap)Bitmap.FromFile(@"ImageSource\Publish\SaveImage.png");
        static public Bitmap AddPost = (Bitmap)Bitmap.FromFile(@"ImageSource\Publish\AddPost.png");
        static public Bitmap LightBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\Publish\LightBtn.png");
        static public Bitmap WaitNewPostEdit = (Bitmap)Bitmap.FromFile(@"ImageSource\Publish\WaitNewPostEdit.png");
        static public Bitmap WriteACaption = (Bitmap)Bitmap.FromFile(@"ImageSource\Publish\WriteACaption.png");
        static public Bitmap ShareBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\Publish\ShareBtn.png");
        static public Bitmap WaitFinishingUp = (Bitmap)Bitmap.FromFile(@"ImageSource\Publish\WaitFinishingUp.png");
        static public Bitmap Publish_NextBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\Publish\Publish_NextBtn.png");
        static public Bitmap ReloadImage = (Bitmap)Bitmap.FromFile(@"ImageSource\Publish\ReloadImage.png");
        static public Bitmap MultiClicked = (Bitmap)Bitmap.FromFile(@"ImageSource\Publish\MultiClicked.png");
        static public Bitmap PublishPost_BackBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\Publish\BackBtn.png");



        #endregion

        #region --Titanium Backup--

        static public Bitmap TitaniumBackupAddOn = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\TitaniumBackupAddOn.png");
        static public Bitmap CancelAddOn = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\CancelAddOn.png");
        static public Bitmap Backup_Restore = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\Backup-Restore.png");
        static public Bitmap InstagramBackup = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\Instagram.png");
        static public Bitmap WaitPopUp = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\WaitPopUp.png");
        static public Bitmap ExecRestore = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\ExecRestore.png");
        static public Bitmap App_Data = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\App+Data.png");
        static public Bitmap DataOnly = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\DataOnly.png");
        static public Bitmap AskInstallUpdate = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\AskInstallUpdate.png");
        static public Bitmap FinishRestore = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\FinishRestore.png");
        static public Bitmap BackupTitanium = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\Backup.png");
        static public Bitmap WaitBackingUp = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\WaitBackingUp.png");
        static public Bitmap TitaniumIcon = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\TitaniumIcon.png");
        static public Bitmap Titanium_IgnoreKeepNewId = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\IgnoreKeepNewId.png");
        static public Bitmap Titanium_Ok_IgnoreKeepNewId = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\Ok_IgnoreKeepNewId.png");

        static public Bitmap Remember_choice_forever = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\Remember choice forever.png");
        static public Bitmap AllowRoot = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\AllowRoot.png");
        static public Bitmap First_TitaniumAddOn = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\First_TitaniumAddOn.png");
        static public Bitmap FirstStart_Ok = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\FirstStart_Ok.png");
        static public Bitmap First_ChangeLog = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\First_ChangeLog.png");
        static public Bitmap First_Warning = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\First_Warning.png");
        static public Bitmap First_Allow = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\First_Allow.png");

        static public Bitmap First_EditFilter = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\First_EditFilter.png");
        static public Bitmap First_CreateLabel = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\First_CreateLabel.png");
        static public Bitmap First_WaitLabelPopup = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\First_WaitLabelPopup.png");
        static public Bitmap First_WaitClearBox = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\First_WaitClearBox.png");
        static public Bitmap First_Instagram138 = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\First_Instagram138.png");
        static public Bitmap AddRemoveElements = (Bitmap)Bitmap.FromFile(@"ImageSource\TitaniumBackup\AddRemoveElements.png");


        #endregion


        #region --Privacy--

        static public Bitmap AccountPrivacy = (Bitmap)Bitmap.FromFile(@"ImageSource\AccountPrivacy\AccountPrivacy.png");
        static public Bitmap WaitPrivateAccount = (Bitmap)Bitmap.FromFile(@"ImageSource\AccountPrivacy\WaitPrivateAccount.png");
        static public Bitmap PrivateOff = (Bitmap)Bitmap.FromFile(@"ImageSource\AccountPrivacy\PrivateOff.png");
        static public Bitmap PrivateOn = (Bitmap)Bitmap.FromFile(@"ImageSource\AccountPrivacy\PrivateOn.png");

        static public Bitmap PrivateOff_1 = (Bitmap)Bitmap.FromFile(@"ImageSource\AccountPrivacy\PrivateOff_1.png");
        static public Bitmap PrivateOn_1 = (Bitmap)Bitmap.FromFile(@"ImageSource\AccountPrivacy\PrivateOn_1.png");

        static public Bitmap OkPopup = (Bitmap)Bitmap.FromFile(@"ImageSource\AccountPrivacy\OkPopup.png");
        static public Bitmap SettingBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\AccountPrivacy\SettingBtn.png");
        static public Bitmap SettingsText = (Bitmap)Bitmap.FromFile(@"ImageSource\AccountPrivacy\SettingsText.png");
        static public Bitmap PrivacyMenu = (Bitmap)Bitmap.FromFile(@"ImageSource\AccountPrivacy\PrivacyMenu.png");
        #endregion


        #region --Es Editor--
        static public Bitmap Es_Menu = (Bitmap)Bitmap.FromFile(@"ImageSource\EsEditor\MenuBtn.png");
        static public Bitmap Es_Edit = (Bitmap)Bitmap.FromFile(@"ImageSource\EsEditor\Edit.png");
        static public Bitmap Es_SelectAll = (Bitmap)Bitmap.FromFile(@"ImageSource\EsEditor\SelectAll.png");
        static public Bitmap Es_Copy = (Bitmap)Bitmap.FromFile(@"ImageSource\EsEditor\Copy.png");
        static public Bitmap Es_Encoding = (Bitmap)Bitmap.FromFile(@"ImageSource\EsEditor\Encoding.png");
        static public Bitmap Es_UTF8 = (Bitmap)Bitmap.FromFile(@"ImageSource\EsEditor\UTF8.png");


        #endregion

        #region --ACTION BLOCK--

        static public Bitmap ActionBlocked = (Bitmap)Bitmap.FromFile(@"ImageSource\ActionBlocked\ActionBlocked.png");
        static public Bitmap TellUs = (Bitmap)Bitmap.FromFile(@"ImageSource\ActionBlocked\TellUs.png");
        static public Bitmap RestrictActivity = (Bitmap)Bitmap.FromFile(@"ImageSource\ActionBlocked\RestrictActivity.png");

        #endregion


        #region --Sponsored--

        static public Bitmap Sponsored_SponsoredText = (Bitmap)Bitmap.FromFile(@"ImageSource\Sponsored\SponsoredText.png");
        static public Bitmap Sponsored_LoadWebsiteX = (Bitmap)Bitmap.FromFile(@"ImageSource\Sponsored\LoadWebsiteX.png");
        static public Bitmap Sponsored_LoadProfileBack = (Bitmap)Bitmap.FromFile(@"ImageSource\Sponsored\LoadProfileBack.png");

        #endregion

        #region --SelfComment--

        static public Bitmap SelfComment_EditProfile = (Bitmap)Bitmap.FromFile(@"ImageSource\SelfComment\EditProfileBtn.png");
        static public Bitmap SelfComment_CommentBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\SelfComment\CommentBtn.png");
        static public Bitmap SelfComment_AddACommentBox = (Bitmap)Bitmap.FromFile(@"ImageSource\SelfComment\AddACommentBox.png");
        static public Bitmap SelfComment_PostComment = (Bitmap)Bitmap.FromFile(@"ImageSource\SelfComment\PostComment.png");

        #endregion


        #region --LikeUserPostTask--


        static public Bitmap LikeUserPost_followbtn = (Bitmap)Bitmap.FromFile(@"ImageSource\LikeUserPostTask\followbtn.png");
        static public Bitmap LikeUserPost_PostTabIcon = (Bitmap)Bitmap.FromFile(@"ImageSource\LikeUserPostTask\PostTabIcon.png");
        static public Bitmap LikeUserPost_WaitPosts = (Bitmap)Bitmap.FromFile(@"ImageSource\LikeUserPostTask\WaitPosts.png");



        #endregion

        #region --Login To Instagram--

        static public Bitmap Login_LoginBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\LogIn\LoginBtn.png");
        static public Bitmap Login_PhoneEmailUser = (Bitmap)Bitmap.FromFile(@"ImageSource\LogIn\PhoneEmailUser.png");
        static public Bitmap Login_Password = (Bitmap)Bitmap.FromFile(@"ImageSource\LogIn\Password.png");
        static public Bitmap Login_LoginBtnBlue = (Bitmap)Bitmap.FromFile(@"ImageSource\LogIn\LoginBtnBlue.png");
        static public Bitmap Login_Disabled = (Bitmap)Bitmap.FromFile(@"ImageSource\LogIn\Disabled.png");
        static public Bitmap Login_ThisWasMe = (Bitmap)Bitmap.FromFile(@"ImageSource\LogIn\ThisWasMe.png");
        static public Bitmap Login_ErrorRequest = (Bitmap)Bitmap.FromFile(@"ImageSource\LogIn\ErrorRequest.png");
        static public Bitmap Login_AccountIsDisabled = (Bitmap)Bitmap.FromFile(@"ImageSource\LogIn\AccountIsDisabled.png");

        #endregion

        #region --Install IG--

        static public Bitmap InstallIG_InstagramAPK = (Bitmap)Bitmap.FromFile(@"ImageSource\Install-IG\InstagramAPK.png");
        static public Bitmap InstallIG_InstallBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\Install-IG\InstallBtn.png");
        static public Bitmap InstallIG_Next = (Bitmap)Bitmap.FromFile(@"ImageSource\Install-IG\Next.png");
        static public Bitmap InstallIG_ConfirmInstall = (Bitmap)Bitmap.FromFile(@"ImageSource\Install-IG\ConfirmInstall.png");
        static public Bitmap InstallIG_Installing = (Bitmap)Bitmap.FromFile(@"ImageSource\Install-IG\Installing.png");
        static public Bitmap InstallIG_AppInstalled = (Bitmap)Bitmap.FromFile(@"ImageSource\Install-IG\AppInstalled.png");
        static public Bitmap InstallIG_OpenIG = (Bitmap)Bitmap.FromFile(@"ImageSource\Install-IG\OpenIG.png");
        static public Bitmap InstallIG_EsFileAskUpdate = (Bitmap)Bitmap.FromFile(@"ImageSource\Install-IG\EsFileAskUpdate.png");
        static public Bitmap InstallIG_EsFileAskUpdate_Cancel = (Bitmap)Bitmap.FromFile(@"ImageSource\Install-IG\EsFileAskUpdate_Cancel.png");

        #endregion

        #region --Rating--
        static public Bitmap Rating_NoThanksOption = (Bitmap)Bitmap.FromFile(@"ImageSource\Rating\NoThanksOption.png");

        #endregion


        #region --Read Notification--

        static public Bitmap ReadNotice_NoticationAlert = (Bitmap)Bitmap.FromFile(@"ImageSource\ReadNotification\NoticationAlert.png");
        static public Bitmap ReadNotice_Activity = (Bitmap)Bitmap.FromFile(@"ImageSource\ReadNotification\Activity.png");
        static public Bitmap ReadNotice_HeartBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\ReadNotification\HeartBtn.png");

        #endregion

        #region --SimulateStoriesTask--

        static public Bitmap SimulateStoriesTask_Stories_Round = (Bitmap)Bitmap.FromFile(@"ImageSource\SimulateStoriesTask\Stories_Round.png");

        static public Bitmap SimulateStoriesTask_Stories_End = (Bitmap)Bitmap.FromFile(@"ImageSource\SimulateStoriesTask\Stories_End.png");

        static public Bitmap SimulateStoriesTask_Tag_BackArrow = (Bitmap)Bitmap.FromFile(@"ImageSource\SimulateStoriesTask\Tag_BackArrow.png");

        #endregion


        #region --SemiAutoComment--

        static public Bitmap SemiAutoComment_AddACommentBox = (Bitmap)Bitmap.FromFile(@"ImageSource\SemiAutoComment\AddACommentBox.png");
        static public Bitmap SemiAutoComment_CommentBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\SemiAutoComment\CommentBtn.png");
        static public Bitmap SemiAutoComment_PostComment = (Bitmap)Bitmap.FromFile(@"ImageSource\SemiAutoComment\PostComment.png");
        static public Bitmap SemiAutoComment_Tag_HeartBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\SemiAutoComment\Tag_HeartBtn.png");
        static public Bitmap SemiAutoComment_Posting = (Bitmap)Bitmap.FromFile(@"ImageSource\SemiAutoComment\Posting.png");

        #endregion

        #region --Publish Stories--

        static public Bitmap PublishStories_AddACommentBox = (Bitmap)Bitmap.FromFile(@"ImageSource\PublishStories\TagCameraBtn.png");
        static public Bitmap PublishStories_AddToYourStory = (Bitmap)Bitmap.FromFile(@"ImageSource\PublishStories\AddToYourStory.png");
        static public Bitmap PublishStories_ShootBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\PublishStories\ShootBtn.png");
        static public Bitmap PublishStories_Gallery = (Bitmap)Bitmap.FromFile(@"ImageSource\PublishStories\Gallery.png");
        static public Bitmap PublishStories_TagSendToBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\PublishStories\TagSendToBtn.png");
        static public Bitmap PublishStories_AskStoriesArchive = (Bitmap)Bitmap.FromFile(@"ImageSource\PublishStories\AskStoriesArchive.png");
        static public Bitmap PublishStories_OK_StoriesArchive = (Bitmap)Bitmap.FromFile(@"ImageSource\PublishStories\OK_StoriesArchive.png");

        #endregion

        #region --AppInfo--

        static public Bitmap AppInfo_DataCleared = (Bitmap)Bitmap.FromFile(@"ImageSource\AppInfo\DataCleared.png");
        static public Bitmap AppInfo_DataAvail = (Bitmap)Bitmap.FromFile(@"ImageSource\AppInfo\DataAvail.png");

        #endregion

        #region --Mode Switch--

        static public Bitmap ModeSwitch_IGStopped = (Bitmap)Bitmap.FromFile(@"ImageSource\ModeSwitch\IGStopped.png");

        #endregion

        #region --Login Instagram Manually Firefox--

        static public Bitmap LaunchManual_PhoneNumber = (Bitmap)Bitmap.FromFile(@"ImageSource\LaunchManual\PhoneNumber.png");
        static public Bitmap LaunchManual_Password = (Bitmap)Bitmap.FromFile(@"ImageSource\LaunchManual\Password.png");
        static public Bitmap LaunchManual_LoginBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\LaunchManual\LoginBtn.png");

        #endregion

        #region --Message--


        static public Bitmap Message_NoMessage = (Bitmap)Bitmap.FromFile(@"ImageSource\Message\NoMessage.png");
        static public Bitmap Message_HasRequestMessage = (Bitmap)Bitmap.FromFile(@"ImageSource\Message\HasRequestMessage.png");
        static public Bitmap Message_TagInstagram = (Bitmap)Bitmap.FromFile(@"ImageSource\Message\TagInstagram.png");
        static public Bitmap Message_DirectMessage = (Bitmap)Bitmap.FromFile(@"ImageSource\Message\DirectMessage.png");
        static public Bitmap Message_TagCameraIcon = (Bitmap)Bitmap.FromFile(@"ImageSource\Message\TagCameraIcon.png");
        static public Bitmap Message_TagMessage = (Bitmap)Bitmap.FromFile(@"ImageSource\Message\TagMessage.png");
        static public Bitmap Message_TagRequests = (Bitmap)Bitmap.FromFile(@"ImageSource\Message\TagRequests.png");


        #endregion

        #region --Seeding--

        static public Bitmap Seeding_LikeBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\Seeding\LikeBtn.png");
        static public Bitmap Seeding_PostOption = (Bitmap)Bitmap.FromFile(@"ImageSource\Seeding\PostOption.png");
        static public Bitmap Seeding_ViewComment = (Bitmap)Bitmap.FromFile(@"ImageSource\Seeding\ViewComment.png");
        static public Bitmap Seeding_NoMoreComment = (Bitmap)Bitmap.FromFile(@"ImageSource\Seeding\NoMoreComment.png");


        #endregion


        #region --Update Fullname--

        static public Bitmap UpdateFullname_TagTitleName = (Bitmap)Bitmap.FromFile(@"ImageSource\UpdateFullname\TagTitleName.png");
        static public Bitmap UpdateFullname_SelectAllBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\UpdateFullname\SelectAllBtn.png");
        static public Bitmap UpdateFullname_EmptyFullname = (Bitmap)Bitmap.FromFile(@"ImageSource\UpdateFullname\EmptyFullname.png");
        static public Bitmap UpdateFullname_UpdateProfileTick = (Bitmap)Bitmap.FromFile(@"ImageSource\UpdateFullname\UpdateProfileTick.png");

        #endregion

        #region --Self Share Stories--


        static public Bitmap SelfStories_ShareStoriesBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\SelfStories\ShareStoriesBtn.png");
        static public Bitmap SelfStories_AddPostToYourStory = (Bitmap)Bitmap.FromFile(@"ImageSource\SelfStories\AddPostToYourStory.png");
        static public Bitmap SelfStories_SendToBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\SelfStories\SendToBtn.png");

        #endregion

        #region --CaptureUsername--

        static public Bitmap CaptureUsername_TagChooseProfile = (Bitmap)Bitmap.FromFile(@"ImageSource\CaptureUsername\TagChooseProfile.png");
        static public Bitmap CaptureUsername_EmptySpace = (Bitmap)Bitmap.FromFile(@"ImageSource\CaptureUsername\EmptySpace.png");
        static public Bitmap CaptureUsername_TagSwitchAccount = (Bitmap)Bitmap.FromFile(@"ImageSource\CaptureUsername\TagSwitchAccount.png");

        #endregion

        #region --IsAccountCorrect--

        static public Bitmap IsAccountCorrect_TagUsername = (Bitmap)Bitmap.FromFile(@"ImageSource\IsAccountCorrect\TagUsername.png");
        static public Bitmap IsAccountCorrect_CopyBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\IsAccountCorrect\CopyBtn.png");
        static public Bitmap IsAccountCorrect_TagUsernameScreen = (Bitmap)Bitmap.FromFile(@"ImageSource\IsAccountCorrect\TagUsernameScreen.png");
        static public Bitmap IsAccountCorrect_SelectAllBtn = (Bitmap)Bitmap.FromFile(@"ImageSource\IsAccountCorrect\SelectAllBtn.png");

        #endregion

    }
}
