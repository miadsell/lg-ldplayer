﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGHelpers
{
    static class SeedingHelpers
    {
        #region --Seeding Like--

        #region --Do Seeding Like--

        static public string DoSeedingLike(string deviceID, string semiauto_comment_postId)
        {
            dynamic dyn_comment = SemiAutoCommentInfo(semiauto_comment_postId);

            if (dyn_comment.seeding_like_status != "SEEDING")
                return "";

            var postUrl = "http://instagram.com/p/" + dyn_comment.postId.ToString();
            var comment_screenshot_base64 = dyn_comment.screenshot_base64;

            var comment_screenshot = Action.ActionHelpers.BitmapFromBase64Str(comment_screenshot_base64);

            ////test

            //var postUrl = "https://www.instagram.com/p/CD-au9-HjLG/";
            //var comment_screenshot = (Bitmap) Image.FromFile(@"F:\DOWNLOAD\comment_semi.png");

            //open post
            ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", postUrl, "com.instagram.android");

            //wait post load

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Seeding_PostOption, 3, 5, 5);

            //scroll to view full post

            ADBSupport.ADBHelpers.ScrollDownToImage(deviceID, 68, ADBSupport.BMPSource.Seeding_ViewComment);

            //Click view comment

            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Seeding_ViewComment, "Seeding_ViewComment");

            //scroll to find comment - sleep per scroll like reading
            Stopwatch watch_find_comment = new Stopwatch();
            watch_find_comment.Start();

            Bitmap pre_screen=null;

            while (true)
            {
                ADBSupport.ADBHelpers.SwipeByPercent_RandomTouch(deviceID, "doc", "22-86", 68, 68 - 15, 100);

                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, comment_screenshot, 0, 0, 0);
                    break;
                }
                catch
                {

                    //check if nomorecomment
                    try
                    {
                        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Seeding_NoMoreComment, 0, 0, 0);
                        return "not_found_comment";
                    }
                    catch { }

                    var current_screen = ADBSupport.ADBHelpers.ScreenShoot(deviceID);

                    if (pre_screen != null)
                    {
                        //check if pre_screen=current_screen -> if yes -> no more comment

                        var find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(pre_screen, current_screen);
                        if (find_point != null)
                        {

                            return "not_found_comment";
                        }
                    }

                    pre_screen = current_screen;

                }

                //check if nomorecomment

                if (watch_find_comment.Elapsed.TotalMinutes > 5)
                    return "";
            }

            //Sleep sometimes like read

            ADBSupport.ADBHelpers.Delay(3, 6);

            //Hit like

            var screen = ADBSupport.ADBHelpers.ScreenShoot(deviceID);

            //point of comment in screen

            var point = (Point) KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, comment_screenshot);

            //Crop region of comment to get like btn location
            var comment_region = KAutoHelper.CaptureHelper.CropImage(screen, new Rectangle((Point)point, new Size(270 - point.X, 40)));

            //Find like btn in comment_region

            var like_point = (Point) KAutoHelper.ImageScanOpenCV.FindOutPoint(comment_region, ADBSupport.BMPSource.Seeding_LikeBtn);

            var like_point_in_screen = new Point(like_point.X + point.X, like_point.Y + point.Y);

            //Click random on like btn

            ADBSupport.ADBHelpers.RandomTapImage(deviceID, like_point_in_screen.X, like_point_in_screen.Y, "Seeding_LikeBtn");

            return "";
        }

        #endregion

        static public void ScanSeedingLike()
        {
            List<dynamic> semicomments = new List<dynamic>();
            //get all semicomment task that was commented

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select * from semi_autocomment_task where [posted]='yes' order by commented_date desc", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var postId = dataReader["postId"].ToString();
                            var seeding_like_status = dataReader["seeding_like_status"].ToString();
                            var target_like = dataReader["target_like"].ToString();
                            var current_like = dataReader["current_like"].ToString();

                            semicomments.Add(new
                            {
                                postId,
                                seeding_like_status,
                                target_like,
                                current_like
                            });
                        }
                    }
                }
            }

            //filter semicomment task that was commented but seeding_like_status is null

            var filter_semicomments = semicomments.Where(s => ((string)s.seeding_like_status) == "").ToList();

            foreach (var comment in filter_semicomments)
            {
                int target_likecount = SetTargetLike("https://www.instagram.com/p/" + comment.postId);

                //update to db and change status to SEEDING
                UpdateSeedingLike(comment.postId, target_likecount.ToString(), "SEEDING");
            }


            //filter semicomment task that was commented and seeding_like_status is FINISHED

            filter_semicomments = semicomments.Where(s => ((string)s.seeding_like_status) == "FINISHED").ToList();

            foreach (var comment in filter_semicomments)
            {
                var new_target_likecount = SetTargetLike("https://www.instagram.com/p/" + comment.postId, int.Parse(comment.target_like));

                if (new_target_likecount == -1)
                    continue;

                //update to db and change status to SEEDING
                UpdateSeedingLike(comment.postId, new_target_likecount.ToString(), "SEEDING");
            }

            //filter semicomment task that are SEEDING and update current_like

            filter_semicomments = semicomments.Where(s => ((string)s.seeding_like_status) == "SEEDING").ToList();
            foreach (var comment in filter_semicomments)
            {
                int current_like_count = CurrentLike(comment.postId);
                //update to db and change status to SEEDING
                UpdateSeedingLike_Current(comment.postId, current_like_count.ToString());
            }

            //filter semicomment task that are SEEDING and check if current_like >= target_like, if yes => set status to FINISHED

            filter_semicomments = semicomments.Where(s => ((string)s.seeding_like_status) == "SEEDING" && int.Parse(s.current_like) >= int.Parse(s.target_like)).ToList();
            foreach (var comment in filter_semicomments)
            {
                //update to db and change status to SEEDING
                UpdateSeedingLike(comment.postId, comment.target_like, "FINISHED");
            }

        }

        static private int SetTargetLike(string postLink, int current_target_like = 0)
        {

            var dyn = IGHelpers.Helpers.APIObJWithProxy();

            var top_like = IGAPIHelper.Scraper.TopCommentLikeCount(dyn.instaApi, postLink);

            if (top_like > current_target_like)
            {

                return top_like + ADBSupport.ADBHelpers.RandomIntValue(2, 5);
            }

            return -1;
        }

        static private void UpdateSeedingLike(string postId, string target_like, string seeding_like_status)
        {
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("update semi_autocomment_task set [target_like]="+target_like+", seeding_like_status='"+seeding_like_status+"' where [postId]='"+postId+"'", conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        static public void UpdateSeedingStatus(string postId,string seeding_like_status)
        {
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("update semi_autocomment_task set seeding_like_status='" + seeding_like_status + "' where [postId]='" + postId + "'", conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        static private void UpdateSeedingLike_Current(string postId, string current_like)
        {
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("update semi_autocomment_task set [current_like]='" + current_like + "' where [postId]='" + postId + "'", conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// get current like by count from actionlog
        /// </summary>
        static private int CurrentLike(string postId)
        {
            var current_like = 0;
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select count(*) from actionlog where [action]='seeding_like' and [value]='" + postId + "' and [do_at] is not null", conn))
                {
                    current_like = int.Parse(command.ExecuteScalar().ToString());
                }
            }

            return current_like;
        }

        static private dynamic SemiAutoCommentInfo(string postId)
        {
            dynamic dyn_comment;

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select * from semi_autocomment_task where [postId]='" + postId + "'", conn))
                {
                    using(var dataReader=command.ExecuteReader())
                    {
                        dataReader.Read();

                        var screenshot_base64 = dataReader["screenshot_base64"].ToString();
                        var seeding_like_status= dataReader["seeding_like_status"].ToString();

                        dyn_comment = new
                        {
                            postId,
                            screenshot_base64,
                            seeding_like_status
                        };
                    }
                }
            }

            return dyn_comment;
        }

        #endregion
    }
}
