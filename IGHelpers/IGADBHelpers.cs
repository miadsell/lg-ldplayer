﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGHelpers
{
    static class IGADBHelpers
    {
        static public void GetUsername(Class.IGDevice iGDevice)
        {
            try
            {
                var deviceID = iGDevice.deviceID;
                var deviceName = iGDevice.ld_devicename;

                //Check if instagram was open

                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_Goto_PostScreenBtn, 2, 2, 0);

                KAutoHelper.ADBHelper.TapByPercent(deviceID, 90.3, 97.3);

                //Wait edit profile screen
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.User_WaitEditProfile, 2, 3, 2);

                //Click editprofile

                ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.User_WaitEditProfile);


                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.User_WaitUserNameLabel, 2, 3, 2);

                //Extract username

                ADBSupport.ADBHelpers.ValueFromScreenByPercentage(deviceID, deviceName, ADBSupport.CropRectangle.rec_Username_EditProfile, true, "username");

                IGHelpers.DBHelpers.AddActionLog(deviceName, "getusername");
            }
            catch { }

        }

        static public void InstallIG(string deviceID, string deviceName)
        {
            OpenDownloadFolder:
            //Open Download folder
            ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s \"" + deviceID + "\" shell am start -n com.estrongs.android.pop/.view.FileExplorerActivity -d \"file:///sdcard/Download\"", 20);

            //check if AskUpdate EsFileExplorer

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.InstallIG_EsFileAskUpdate,2,5);

                ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.InstallIG_EsFileAskUpdate_Cancel);
            }
            catch
            {

            }


            //Wait instagram.apk show
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.InstallIG_InstagramAPK);
            }
            catch
            {
                //not found ig
                var ig_path = "Temp\\" + deviceName + "\\system\\instagram.apk";
                File.Copy("instagram.apk", ig_path, true);
                //push to folder Download
                ADBSupport.ADBHelpers.PushFileToPhone(deviceID, ig_path, "/sdcard/Download");
                goto OpenDownloadFolder;

            }




            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.InstallIG_InstagramAPK, ADBSupport.BMPSource.InstallIG_InstallBtn, 1);

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.InstallIG_InstallBtn, ADBSupport.BMPSource.InstallIG_Next, 1);

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.InstallIG_Next, ADBSupport.BMPSource.InstallIG_ConfirmInstall, 1);

            ADBSupport.ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.InstallIG_ConfirmInstall, ADBSupport.BMPSource.InstallIG_Installing, 1);

            //Wait until app installed

            while(true)
            {
                Thread.Sleep(10000);
                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.InstallIG_AppInstalled, 1, 2);
                    break;
                }
                catch { }
            }

            //Click icon ig

            KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_HOME);

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_IconApp, 2, 5, 5);

            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Global_IconApp);

            //Wait CreateNewAccount btn

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.CreateNewAccount, 3, 5, 3);

            //Close app

            ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell pm disable com.instagram.android");

            //Re-enable

            ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell pm enable com.instagram.android");

            //turn of notification

            ADBSupport.ADBHelpers.BlockedNotificatioin(deviceID);
        }

        static public void Backup()
        {

        }

        static private void BackUpDevice()
        {

        }

        static private void BackupInstagram(Class.IGDevice iGDevice)
        {

        }
    }
}
