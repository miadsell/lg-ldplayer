﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGHelpers
{
    static class EngageTaskHelpers
    {
        static public List<string> GetEngageSettings(string deviceName)
        {
            //Get engagetype from account_info
            var engagetype = "";

            using (SqlConnection conn = new SqlConnection(DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select account_info.engagetype from account_info join ig_account on account_info.accountid=ig_account.id where phone_device='"+deviceName+"'", conn))
                {
                    var engagetype_object = sqlCommand.ExecuteScalar();
                    if(engagetype_object!=null)
                        engagetype = sqlCommand.ExecuteScalar().ToString();
                }
            }

            if (engagetype == "" || engagetype == "warmup")
                engagetype = SetEngageTypeAutomatic(deviceName);

            //Get settings from engagetype

            var settings_str = "";

            using (SqlConnection conn = new SqlConnection(DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select [settings] from engagesettings where [type]='" + engagetype + "'", conn))
                {
                    settings_str = sqlCommand.ExecuteScalar().ToString();
                }
            }

            List<string> settings = settings_str.Split(new string[] { "***" }, StringSplitOptions.None).ToList<string>();


            return settings;
        }

        static private string SetEngageTypeAutomatic(string deviceName)
        {
            string engagetype = "";
            int engage_days;
            double total_engage_minutes;
            //get count engage days
            using (SqlConnection conn = new SqlConnection(DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("select count (distinct (CONVERT(date, do_at))) from actionlog join ig_account on actionlog.accountid=ig_account.id where phone_device='" + deviceName + "'", conn))
                {
                    engage_days = int.Parse(sqlCommand.ExecuteScalar().ToString());
                }

                using (var sqlCommand = new SqlCommand("select account_info.total_timespend from account_info join ig_account on account_info.accountid=ig_account.id where phone_device='" + deviceName + "'", conn))
                {
                    total_engage_minutes = double.Parse(sqlCommand.ExecuteScalar().ToString());
                }
            }

            //warmup

            if (engage_days <= 5 || total_engage_minutes < 50)
                engagetype = "warmup";
            else
                engagetype = "default";

            //Update engagetype

            using (SqlConnection conn = new SqlConnection(DBHelpers.db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("update account_info set [engagetype]='warmup' where [accountId]=(select [id] from ig_account where [phone_device]='" + deviceName + "')", conn))
                {
                    sqlCommand.ExecuteNonQuery();
                }
            }

            return engagetype;

        }
    }
}
