﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGHelpers
{
    static class OCRSpaceHelpers
    {
        public class Rootobject
        {
            public Parsedresult[] ParsedResults { get; set; }
            public int OCRExitCode { get; set; }
            public bool IsErroredOnProcessing { get; set; }
            public string ErrorMessage { get; set; }
            public string ErrorDetails { get; set; }
        }

        public class Parsedresult
        {
            public object FileParseExitCode { get; set; }
            public string ParsedText { get; set; }
            public string ErrorMessage { get; set; }
            public string ErrorDetails { get; set; }
        }

        static public async Task<List<string>> OCRActionAsync(string ImagePath)
        {
            List<string> texts = new List<string>();

            try
            {
                HttpClient httpClient = new HttpClient();
                httpClient.Timeout = new TimeSpan(1, 1, 1);


                MultipartFormDataContent form = new MultipartFormDataContent();
                form.Add(new StringContent("8bca9ae24388957"), "apikey"); //Added api key in form data
                form.Add(new StringContent("eng"), "language");


                
                    byte[] imageData = File.ReadAllBytes(ImagePath);
                    form.Add(new ByteArrayContent(imageData, 0, imageData.Length), "image", "image.jpg");
                

                HttpResponseMessage response = await httpClient.PostAsync("https://api.ocr.space/Parse/Image", form);

                string strContent = await response.Content.ReadAsStringAsync();



                Rootobject ocrResult = Newtonsoft.Json.JsonConvert.DeserializeObject<Rootobject>(strContent);


                if (ocrResult.OCRExitCode == 1)
                {
                    for (int i = 0; i < ocrResult.ParsedResults.Count(); i++)
                    {
                        texts.Add(ocrResult.ParsedResults[i].ParsedText);
                    }
                }
                else
                {
                    throw new System.ArgumentException("Error when extract text");
                }



            }
            catch (Exception exception)
            {
                throw new System.ArgumentException("OOps");
            }

            return texts;
        }
    }
}
