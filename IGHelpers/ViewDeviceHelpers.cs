﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.IGHelpers
{
    static class ViewDeviceHelpers
    {
        #region --User32--

        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndParent);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, int uFlags);

        #endregion

        static int width = 202;
        static int height = 360;

        static public void LoadDeviceToPanel(string deviceName)
        {
            Process p = null;

            Stopwatch watch_addpanel = new Stopwatch();
            watch_addpanel.Start();

            while (true)
            {
                if(watch_addpanel.Elapsed.TotalMinutes>5)
                {
                    throw new System.ArgumentException("Not Found Device Window");
                }

                var processes = Process.GetProcessesByName("dnplayer");

                foreach (var item in processes)
                {
                    if (item.MainWindowTitle == deviceName)
                    {
                        p = item;
                        break;
                    }
                }
                if (p != null)
                    break;
                Thread.Sleep(10);
            }

            //Program.form.AddToViewPanel(p, deviceName);

            //Panel panel_ld = new Panel();
            //Panel panel_title = new Panel();
            //panel_title.Width = width;
            //panel_title.Height = 37;
            //panel_title.BackColor = Color.Black;

            //panel_ld.Width = width;
            //panel_ld.Height = height + panel_title.Height;

            

            //Panel panel_ld_child = new Panel();
            //panel_ld_child.Width = panel_ld.Width;
            //panel_ld_child.Height = panel_ld.Height;

            //SetParent(p.MainWindowHandle, panel_ld_child.Handle);      // Set the process parent window to the window we want
            //SetWindowPos(p.MainWindowHandle, IntPtr.Zero, 0, 0, width, height, 0x0040);//| 0x0001);


            //panel_ld.Controls.Add(panel_title);

            //panel_ld.Controls.Add(panel_ld_child);
            ////panel_title.Controls.Add(panel_ld);
            //panel_title.BringToFront();
            //panel_title.Visible = true;
            //panel_title.Show();

            //Label tb_title = new Label();
            //tb_title.Text = deviceName;
            //tb_title.Width = panel_title.Width;
            //tb_title.Height = panel_title.Height;
            //tb_title.TextAlign = ContentAlignment.MiddleCenter;
            //tb_title.ForeColor = Color.White;
            //panel_title.Controls.Add(tb_title);

            ////Program.form.flowLayoutPanel_runningDevice.Invoke((MethodInvoker)delegate
            ////{
            ////    Program.form.flowLayoutPanel_runningDevice.Controls.Add(panel_ld.Invoke(    );
            ////    panel_ld.Show();
            ////    panel_ld.Name = deviceName;
            ////});

            //Thread th = new Thread(() => Program.form.AddToViewPanel(panel_ld, deviceName));
            //th.Start();
        }

        

        //static public void UpdateDeviceStatus(string deviceName, string status, Color color)
        //{
        //    var panel_child = GetAll(Program.form.flowLayoutPanel_runningDevice, typeof(Panel));

        //    foreach (var item in panel_child)
        //    {
        //        if (item.Name == deviceName)
        //        {

        //            var label_childs = GetAll(item, typeof(Label));

        //            foreach(var label_item in label_childs)
        //            {
        //                if(label_item.Name.Contains(deviceName))
        //                {
        //                    label_item.Invoke((MethodInvoker)delegate
        //                    {
        //                        label_item.Text = status;
        //                        label_item.ForeColor = color;
        //                    });
        //                }
        //            }
        //        }
        //    }
        //}

        static private IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }


        #region --Minimize Device--

        private const int SW_SHOWNORMAL = 1;
        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_SHOWMAXIMIZED = 3;

        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

        static public void MinimizeWindow(IntPtr hWnd, int nCmdShow)
        {
            ShowWindowAsync(hWnd, nCmdShow);
        }

        #endregion
    }
}
