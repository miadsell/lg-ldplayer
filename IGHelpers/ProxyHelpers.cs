﻿using Microsoft.SqlServer.Server;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGHelpers
{
    static class ProxyHelpers
    {
        static public void UpdateCurrentUsed(string updatetype = "engage")
        {
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                PLAutoHelper.SQLHelper.Execute(() =>
                {

                    SqlCommand cm = new SqlCommand("igdb_UpdateCurrentUsed", conn);
                    cm.CommandType = System.Data.CommandType.StoredProcedure;

                    cm.Parameters.Add("@updatetype", System.Data.SqlDbType.VarChar).Value = updatetype;

                    cm.ExecuteNonQuery();
                });
            }
        }

        #region --Proxy--

        static public string GetProxyOthers(string exclude_proxy = "")
        {
        GetProxy:
            string proxy_str = null;
            //Get available proxy from db

            ClearLogProxy();

            //Mutex m_proxy = new Mutex(false, "ig_proxy");
            //m_proxy.WaitOne();

            //while (true)
            //{

            //    using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            //    {
            //        conn.Open();

            //        using (var command = new SqlCommand("igdb_GetProxy", conn))
            //        {
            //            command.CommandType = System.Data.CommandType.StoredProcedure;

            //            command.Parameters.Add("@threadid", System.Data.SqlDbType.VarChar).Value = Thread.CurrentThread.Name;
            //            command.Parameters.Add("@exclude_proxy", System.Data.SqlDbType.VarChar).Value = exclude_proxy;
            //            command.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = Program.form.pc_devices;

            //            using (var dataReader = command.ExecuteReader())
            //            {
            //                if (dataReader.HasRows)
            //                {
            //                    dataReader.Read();
            //                    proxy_str = dataReader[0].ToString();
            //                }
            //            }
            //        }
            //    }

            //    if (proxy_str != null)
            //        break;
            //    Thread.Sleep(10000);
            //}

            while (true)
            {

                string temp_proxy_str = null, temp_current_used = null;

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();
                    using (var sqlTransaction = conn.BeginTransaction())
                    {
                        var command_query =
                        @"select top 1 [proxy_str] from proxytb with (updlock)
	                    where 
		                    [used_time]<max_thread
		                    and [status]='Ready'
		                    and [proxy_str]!= @exclude_proxy
		                    and (latest_claimed is null or DATEDIFF(SECOND,latest_claimed,GETDATE())>30)
		                    and [pc_device]=@pc_devices
	                    order by used_time desc, [latest_claimed] asc";

                        using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                        {
                            command.CommandType = System.Data.CommandType.StoredProcedure;

                            command.Parameters.Add("@threadid", System.Data.SqlDbType.VarChar).Value = Thread.CurrentThread.Name;
                            command.Parameters.Add("@exclude_proxy", System.Data.SqlDbType.VarChar).Value = exclude_proxy;
                            command.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = Program.form.pc_devices;

                            using (var dataReader = command.ExecuteReader())
                            {
                                if (dataReader.HasRows)
                                {
                                    dataReader.Read();
                                    temp_proxy_str = dataReader[0].ToString();
                                    temp_current_used = dataReader["current_used"].ToString();
                                }
                            }
                        }

                        if (temp_proxy_str != null)
                        {
                            //claimed it
                            command_query =
                                @"update proxytb set [used_time]=[used_time]+1, [current_used]=current_used+1, latest_claimed=GETDATE() where [proxy_str]=@proxy_str and [current_used]=@current_used";

                            using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                            {
                                command.CommandType = System.Data.CommandType.StoredProcedure;

                                command.Parameters.Add("@threadid", System.Data.SqlDbType.VarChar).Value = Thread.CurrentThread.Name;
                                command.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = Program.form.pc_devices;
                                command.Parameters.Add("@proxy_str", System.Data.SqlDbType.VarChar).Value = temp_proxy_str;
                                command.Parameters.Add("@current_used", System.Data.SqlDbType.Int).Value = temp_current_used;

                                var affect = command.ExecuteNonQuery();

                                if (affect == 1)
                                {
                                    proxy_str = temp_proxy_str;
                                    command.CommandText =
                                        @"insert into log_threaduseproxy ([threadid],[proxy_str],pc_device) values (@threadid,@proxy_str,@pc_devices)";
                                    command.ExecuteNonQuery();
                                }
                            }
                        }

                        sqlTransaction.Commit();
                    }
                }

                if (proxy_str != null)
                    break;
                Thread.Sleep(10000);
            }

            return proxy_str;
        }

        static public string GetProxyOthers_v1(string deviceName)
        {
            string proxy_str = null;

            var thread_type = Thread.GetData(Thread.GetNamedDataSlot("thread_type"));

            if (thread_type == null)
                throw new System.ArgumentException("GetProxyOthers: thread_type can't be null");

            while (true)
            {

                if(Program.form.isstop)
                {
                    throw new System.ArgumentException("Force stopping software");
                }

                string temp_proxy_str = null, temp_current_used = null;

                switch(thread_type.ToString())
                {
                    case "action_mass":
                        using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                        {
                            conn.Open();
                            using (var sqlTransaction = conn.BeginTransaction())
                            {
                                var command_query =
                                @"select top 1 * from proxytb with (updlock)
	                    where 
		                    [used_time]<max_thread
                            and [used_mass] < max_thread_mass
		                    and [status]='Ready'
		                    and (latest_claimed is null or DATEDIFF(SECOND,latest_claimed,GETDATE())>30)
		                    and [pc_device]=@pc_devices
	                    order by used_mass asc, [latest_claimed] asc";

                                using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                {

                                    command.Parameters.Add("@threadid", System.Data.SqlDbType.VarChar).Value = Thread.CurrentThread.Name;
                                    command.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = Program.form.pc_devices;

                                    using (var dataReader = command.ExecuteReader())
                                    {
                                        if (dataReader.HasRows)
                                        {
                                            dataReader.Read();
                                            temp_proxy_str = dataReader["proxy_str"].ToString();
                                            temp_current_used = dataReader["current_used"].ToString();
                                        }
                                    }
                                }

                                if (temp_proxy_str != null)
                                {
                                    //claimed it
                                    command_query =
                                        @"update proxytb set [used_time]=[used_time]+1, [used_mass]=[used_mass]+1, [current_used]=current_used+1, latest_claimed=GETDATE() where [proxy_str]=@proxy_str and [current_used]=@current_used";

                                    using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    {

                                        command.Parameters.Add("@threadid", System.Data.SqlDbType.VarChar).Value = Thread.CurrentThread.Name;
                                        command.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = Program.form.pc_devices;
                                        command.Parameters.Add("@proxy_str", System.Data.SqlDbType.VarChar).Value = temp_proxy_str;
                                        command.Parameters.Add("@current_used", System.Data.SqlDbType.Int).Value = temp_current_used;

                                        var affect = command.ExecuteNonQuery();

                                        if (affect == 1)
                                        {
                                            proxy_str = temp_proxy_str;
                                            command.CommandText =
                                                @"insert into log_threaduseproxy ([threadid],[proxy_str],pc_device) values (@threadid,@proxy_str,@pc_devices)";
                                            command.ExecuteNonQuery();
                                        }
                                    }
                                }

                                sqlTransaction.Commit();
                            }
                        }
                        break;
                    case "action_light":
                        using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                        {
                            conn.Open();
                            using (var sqlTransaction = conn.BeginTransaction())
                            {
                                var command_query =
                                @"select top 1 * from proxytb with (updlock)
	                    where 
		                    [used_time]<max_thread
                            and [used_light] < max_thread_light
		                    and [status]='Ready'
		                    and (latest_claimed is null or DATEDIFF(SECOND,latest_claimed,GETDATE())>30)
		                    and [pc_device]=@pc_devices
	                    order by used_time desc, [latest_claimed] asc";

                                using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                {

                                    command.Parameters.Add("@threadid", System.Data.SqlDbType.VarChar).Value = Thread.CurrentThread.Name;
                                    command.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = Program.form.pc_devices;

                                    using (var dataReader = command.ExecuteReader())
                                    {
                                        if (dataReader.HasRows)
                                        {
                                            dataReader.Read();
                                            temp_proxy_str = dataReader["proxy_str"].ToString();
                                            temp_current_used = dataReader["current_used"].ToString();
                                        }
                                    }
                                }

                                if (temp_proxy_str != null)
                                {
                                    //claimed it
                                    command_query =
                                        @"update proxytb set [used_time]=[used_time]+1, [used_light]=[used_light]+1, [current_used]=current_used+1, latest_claimed=GETDATE() where [proxy_str]=@proxy_str and [current_used]=@current_used";

                                    using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    {

                                        command.Parameters.Add("@threadid", System.Data.SqlDbType.VarChar).Value = Thread.CurrentThread.Name;
                                        command.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = Program.form.pc_devices;
                                        command.Parameters.Add("@proxy_str", System.Data.SqlDbType.VarChar).Value = temp_proxy_str;
                                        command.Parameters.Add("@current_used", System.Data.SqlDbType.Int).Value = temp_current_used;

                                        var affect = command.ExecuteNonQuery();

                                        if (affect == 1)
                                        {
                                            proxy_str = temp_proxy_str;
                                            command.CommandText =
                                                @"insert into log_threaduseproxy ([threadid],[proxy_str],pc_device) values (@threadid,@proxy_str,@pc_devices)";
                                            command.ExecuteNonQuery();
                                        }
                                    }
                                }

                                sqlTransaction.Commit();
                            }
                        }
                        break;
                    default:
                        throw new System.ArgumentException("Not recognize this thread_type");
                }



                if (proxy_str != null)
                {
                    AIOHelper.ThreadHelper.MonitorThreadUsage.UpdateThreadRunning_Log(deviceName.Replace("LDPlayer-IG-", ""), "");
                    break;
                }
                //update log wait_proxy

                AIOHelper.ThreadHelper.MonitorThreadUsage.UpdateThreadRunning_Log(deviceName.Replace("LDPlayer-IG-", ""), "wait_proxy_" + thread_type);

                Thread.Sleep(10000);
            }

            return proxy_str;
        }


        /// <summary>
        /// no devices, use for all pc_devices
        /// </summary>
        /// <param name="deviceName"></param>
        /// <returns></returns>
        static public string GetProxyOthers_v1_AIO(string deviceName)
        {
            GetProxyOthers_v1_AIO:
            string proxy_str = null;

            var thread_type = Thread.GetData(Thread.GetNamedDataSlot("thread_type"));

            if (thread_type == null)
                throw new System.ArgumentException("GetProxyOthers: thread_type can't be null");

            while (true)
            {

                if (Program.form.isstop)
                {
                    throw new System.ArgumentException("Force stopping software");
                }

                string temp_proxy_str = null, temp_current_used = null;

                switch (thread_type.ToString())
                {
                    case "action_mass":
                        try
                        {
                            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                            {
                                conn.Open();
                                using (var sqlTransaction = conn.BeginTransaction())
                                {
                                    var command_query =
                                    @"select top 1 * from proxytb with (updlock)
	                    where 
		                    [used_time]<max_thread
                            and [used_mass] < max_thread_mass
		                    and [status]='Ready'
		                    and (latest_claimed is null or DATEDIFF(SECOND,latest_claimed,GETDATE())>30)
	                    order by used_mass asc, [latest_claimed] asc";

                                    using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    {

                                        command.Parameters.Add("@threadid", System.Data.SqlDbType.VarChar).Value = Thread.CurrentThread.Name;

                                        using (var dataReader = command.ExecuteReader())
                                        {
                                            if (dataReader.HasRows)
                                            {
                                                dataReader.Read();
                                                temp_proxy_str = dataReader["proxy_str"].ToString();
                                                temp_current_used = dataReader["current_used"].ToString();
                                            }
                                        }
                                    }

                                    if (temp_proxy_str != null)
                                    {
                                        //claimed it
                                        command_query =
                                            @"update proxytb set [used_time]=[used_time]+1, [used_mass]=[used_mass]+1, [current_used]=current_used+1, latest_claimed=GETDATE() where [proxy_str]=@proxy_str and [current_used]=@current_used";

                                        using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                        {

                                            command.Parameters.Add("@threadid", System.Data.SqlDbType.VarChar).Value = Thread.CurrentThread.Name;
                                            command.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = Program.form.pc_devices;
                                            command.Parameters.Add("@proxy_str", System.Data.SqlDbType.VarChar).Value = temp_proxy_str;
                                            command.Parameters.Add("@current_used", System.Data.SqlDbType.Int).Value = temp_current_used;

                                            var affect = command.ExecuteNonQuery();

                                            if (affect == 1)
                                            {
                                                proxy_str = temp_proxy_str;
                                                command.CommandText =
                                                    @"insert into log_threaduseproxy ([threadid],[proxy_str],pc_device) values (@threadid,@proxy_str,@pc_devices)";
                                                command.ExecuteNonQuery();
                                            }
                                        }
                                    }

                                    sqlTransaction.Commit();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            if (proxy_str == null)
                                goto GetProxyOthers_v1_AIO;
                        }
                        break;
                    case "action_light":
                        try

                        {
                            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                            {
                                conn.Open();
                                using (var sqlTransaction = conn.BeginTransaction())
                                {
                                    var command_query =
                                    @"select top 1 * from proxytb with (updlock)
	                    where 
		                    [used_time]<max_thread
                            and [used_light] < max_thread_light
		                    and [status]='Ready'
		                    and (latest_claimed is null or DATEDIFF(SECOND,latest_claimed,GETDATE())>30)
	                    order by used_time desc, [latest_claimed] asc";

                                    using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    {

                                        command.Parameters.Add("@threadid", System.Data.SqlDbType.VarChar).Value = Thread.CurrentThread.Name;

                                        using (var dataReader = command.ExecuteReader())
                                        {
                                            if (dataReader.HasRows)
                                            {
                                                dataReader.Read();
                                                temp_proxy_str = dataReader["proxy_str"].ToString();
                                                temp_current_used = dataReader["current_used"].ToString();
                                            }
                                        }
                                    }

                                    if (temp_proxy_str != null)
                                    {
                                        //claimed it
                                        command_query =
                                            @"update proxytb set [used_time]=[used_time]+1, [used_light]=[used_light]+1, [current_used]=current_used+1, latest_claimed=GETDATE() where [proxy_str]=@proxy_str and [current_used]=@current_used";

                                        using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                        {

                                            command.Parameters.Add("@threadid", System.Data.SqlDbType.VarChar).Value = Thread.CurrentThread.Name;
                                            command.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = Program.form.pc_devices;
                                            command.Parameters.Add("@proxy_str", System.Data.SqlDbType.VarChar).Value = temp_proxy_str;
                                            command.Parameters.Add("@current_used", System.Data.SqlDbType.Int).Value = temp_current_used;

                                            var affect = command.ExecuteNonQuery();

                                            if (affect == 1)
                                            {
                                                proxy_str = temp_proxy_str;
                                                command.CommandText =
                                                    @"insert into log_threaduseproxy ([threadid],[proxy_str],pc_device) values (@threadid,@proxy_str,@pc_devices)";
                                                command.ExecuteNonQuery();
                                            }
                                        }
                                    }

                                    sqlTransaction.Commit();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message.Contains("This SqlTransaction has completed") && proxy_str == null)
                                goto GetProxyOthers_v1_AIO;
                        }
                        break;
                    default:
                        throw new System.ArgumentException("Not recognize this thread_type");
                }



                if (proxy_str != null)
                {
                    AIOHelper.ThreadHelper.MonitorThreadUsage.UpdateThreadRunning_Log(deviceName.Replace("LDPlayer-IG-", ""), "");
                    break;
                }
                //update log wait_proxy

                AIOHelper.ThreadHelper.MonitorThreadUsage.UpdateThreadRunning_Log(deviceName.Replace("LDPlayer-IG-", ""), "wait_proxy_" + thread_type);

                Thread.Sleep(10000);
            }

            return proxy_str;
        }

        static public bool IsWorkFB(string proxy_str)
        {
            RestClient client = new RestClient(new Uri("https://www.facebook.com/"));

            client.Timeout = 5000;
            client.ReadWriteTimeout = 10000;

            //client.Timeout = 30000;

            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;

            client.Proxy = new WebProxy(proxy_str.Split(':')[0], int.Parse(proxy_str.Split(':')[1]));

            CookieContainer _cookieJar = new CookieContainer();
            client.CookieContainer = _cookieJar;

            var request = new RestRequest("", Method.GET);

            request.AddHeader("Host", "www.facebook.com");
            request.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0");
            request.AddHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            request.AddHeader("Accept-Language", "en-US,en;q=0.5");
            request.AddHeader("Accept-Encoding", "gzip, deflate, br");
            request.AddHeader("Connection", "keep-alive");
            request.AddHeader("Upgrade-Insecure-Requests", "1");
            var task = Task.Run(() =>
            {
                return client.Execute(request);
            });
            if (!task.Wait(TimeSpan.FromSeconds(10)))
                return false;

            var query_load = task.Result;

            if (query_load.StatusCode.ToString() != "OK")
                return false;
            return true;

        }

        //static public void UpdateProxyStatus()
        //{
        //    RestClient client = new RestClient("http://" + ADBSupport.LDHelpers.proxy_server_ip + ":10000/");

        //    var request_proxy_data = new RestRequest("proxy_list", Method.GET);

        //    var query_proxy_data = client.Execute(request_proxy_data);

        //    var json_proxies = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(query_proxy_data.Content);


        //    using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
        //    {
        //        conn.Open();
        //        foreach (var proxy in json_proxies)
        //        {
        //            if (proxy["imei"].Value == null)
        //            {
        //                break;
        //            }
        //            var proxy_str = ADBSupport.LDHelpers.proxy_server_ip + ":" + proxy["proxy_port"].Value.ToString();
        //            if (proxy["public_ip"].Value != null)
        //            {
        //                if (!proxy["public_ip"].Value.ToString().Contains("103"))
        //                {
        //                    //Update false
        //                    using (var command = new SqlCommand("update proxytb set [status]='False' where [proxy_str]='" + proxy_str + "'", conn))
        //                    {
        //                        command.ExecuteNonQuery();
        //                    }
        //                }
        //                else //work =>> clear status
        //                {
        //                    using (var command = new SqlCommand("update proxytb set [status]='' where [proxy_str]='" + proxy_str + "'", conn))
        //                    {
        //                        command.ExecuteNonQuery();
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        //static public bool IsProxyReady(string proxy_str)
        //{

        //    var port = proxy_str.Split(':')[1];

        //    RestClient client = new RestClient("http://" + ADBSupport.LDHelpers.proxy_server_ip + ":10000/");

        //    var request_proxy_data = new RestRequest("proxy_list", Method.GET);

        //    var query_proxy_data = client.Execute(request_proxy_data);

        //    var json_proxies = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(query_proxy_data.Content);


        //    foreach (var proxy in json_proxies)
        //    {
        //        if (proxy["proxy_port"].Value.ToString() == port)
        //        {
        //            if (proxy["public_ip"].Value != null)
        //            {
        //                if (!proxy["public_ip"].Value.ToString().Contains("103"))
        //                    return false;
        //                break;
        //            }
        //            break;

        //        }
        //    }

        //    return true;
        //}

        //use for all task except register task

        /// <summary>
        /// check and set proper status for all proxies need reset
        /// </summary>
        static public void ProxyServerOthers(string threadname_pattern, bool firsttime = false)
        {


            //Check if proxifier is running, if yes, kill it


            var process = Process.GetProcessesByName("Proxifier");

            if (process.Count() > 0)
                process[0].Kill();

            if (firsttime)
            {
                //update proxy by device imeis
                PLAutoHelper.ProxyHelper.Xproxy_DB_UpdateProxyByImeis(new PLAutoHelper.ProxyHelper.ProxyDB(IGHelpers.DBHelpers.db_connection, "proxytb"), ADBSupport.LDHelpers.proxy_server_ips);

                //IGHelpers.ProxyHelpers.DeleteLogProxy(threadname_pattern);
                //IGHelpers.ProxyHelpers.UpdateCurrentUsed();

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    var sql_command = "update proxytb set [reset]='Reseting', [status]='NotReady' where pc_device=" + Program.form.pc_devices;

                    using (var command = new SqlCommand(sql_command, conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
            else
            {
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    var sql_command = "update proxytb set [reset]='Reseting', [status]='NotReady' where used_time > 0 and current_used = 0 and [reset]!='UpdatingIP' and pc_device=" + Program.form.pc_devices;

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var command = new SqlCommand(sql_command, conn))
                        {
                            command.ExecuteNonQuery();
                        }
                    });
                }
            }

            // reset each proxy that needed

            List<string> resetproxies = new List<string>();
            List<Thread> threads = new List<Thread>();

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select * from proxytb where [reset]='Reseting' and pc_device=" + Program.form.pc_devices, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            resetproxies.Add(dataReader["proxy_str"].ToString());
                        }
                    }
                }


                foreach (var item in resetproxies)
                {
                    if (item == "")
                        continue;

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var command = new SqlCommand("update proxytb set [reset]='UpdatingIP' where proxy_str='" + item + "'", conn))
                        {
                            command.ExecuteNonQuery();

                        }
                    });


                    //Update proxy multi thread

                    Thread thread = new Thread(() => ResetEachProxy(item));
                    threads.Add(thread);
                }
            }

            foreach (var t in threads)
            {
                t.Start();
            }
        }

        /// <summary>
        /// use proxy for all pc_Devices, khong phan biet // just run 1 thread cho toan bo tool tren tat ca may
        /// </summary>
        /// <param name="threadname_pattern"></param>
        /// <param name="firsttime"></param>
        static public void ProxyServerOthers_AIO(string threadname_pattern, bool firsttime = false)
        {

            bool do_check = false;


            //Check if proxifier is running, if yes, kill it


            var process = Process.GetProcessesByName("Proxifier");

            if (process.Count() > 0)
                process[0].Kill();

            //check latest check
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                //get latest check

                var sql_command =
                    @"select top 1 *
                    from actionlog
                    where
	                    [action]='ProxyServerOthers_AIO'
                    order by id desc";


                //below code to check if need check proxy or not, it also help to prevent multicheck by multi pc_devices

                using(var command=new SqlCommand(sql_command,conn))
                {
                    using(var dataReader=command.ExecuteReader())
                    {
                        if(dataReader.HasRows)
                        {
                            dataReader.Read();


                            var seconds = DateTime.Now.Subtract(DateTime.Parse(dataReader["do_at"].ToString())).TotalSeconds;

                            var pc_devices = dataReader["value"].ToString();
                            if (pc_devices == "")
                            {
                                do_check = true;
                                goto CheckOrNot;
                            }

                            if (pc_devices == Program.form.pc_devices)
                            {
                                if (seconds > 10)
                                {
                                    do_check = true;
                                }
                            }
                            else
                            {
                                if (seconds > 60)
                                    do_check = true;
                            }
                        }
                        else
                        {
                            do_check = true;
                        }
                    }
                }

                //sql_command =
                //    @"select count(*)
                //    from actionlog
                //    where
	               //     [action]='ProxyServerOthers_AIO' and
	               //     DATEDIFF(second, do_at,getdate())<=10";

                //using (var command = new SqlCommand(sql_command, conn))
                //{
                //    var count = int.Parse(command.ExecuteScalar().ToString());

                //    if (count > 0)
                //        return;
                //}
            }

        CheckOrNot:

            if (!do_check)
                return;

            IGHelpers.DBHelpers.AddActionLogByID("155", "ProxyServerOthers_AIO", Program.form.pc_devices);



            if (firsttime)
            {
                //update proxy by device imeis
                PLAutoHelper.ProxyHelper.Xproxy_DB_UpdateProxyByImeis(new PLAutoHelper.ProxyHelper.ProxyDB(IGHelpers.DBHelpers.db_connection, "proxytb"), ADBSupport.LDHelpers.proxy_server_ips);

                //IGHelpers.ProxyHelpers.DeleteLogProxy(threadname_pattern); //delete proxy by devices //should to another func

                //IGHelpers.ProxyHelpers.UpdateCurrentUsed();

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    //clear actionlog reset_proxy

                    var sql_command = "delete actionlog where [action]='reset_proxy'";

                    using (var command = new SqlCommand(sql_command, conn))
                    {
                        command.ExecuteNonQuery();
                    }

                    sql_command = "update proxytb set [reset]='Reseting', [status]='NotReady'";

                    using (var command = new SqlCommand(sql_command, conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
            else
            {
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    var sql_command = "update proxytb set [reset]='Reseting', [status]='NotReady' where used_time > 0 and current_used = 0 and [reset]!='UpdatingIP'";

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var command = new SqlCommand(sql_command, conn))
                        {
                            command.ExecuteNonQuery();
                        }
                    });
                }
            }

            // reset each proxy that needed

            List<string> resetproxies = new List<string>();
            List<Thread> threads = new List<Thread>();

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select * from proxytb where [reset]='Reseting'", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            resetproxies.Add(dataReader["proxy_str"].ToString());
                        }
                    }
                }

                List<string> active_proxies = Current_ProxiesInTimeRange();

                foreach (var item in resetproxies)
                {
                    if (item == "" || !active_proxies.Contains(item))
                        continue;

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var command = new SqlCommand("update proxytb set [reset]='UpdatingIP' where proxy_str='" + item + "'", conn))
                        {
                            command.ExecuteNonQuery();

                        }
                    });


                    //Update proxy multi thread

                    Thread thread = new Thread(() => ResetEachProxy(item));
                    threads.Add(thread);
                }

                //get proxy_str reset='Updateting', no reset action in last 10 minutes and not in above list

                List<string> rest_noactive_last10mins = new List<string>();

                string command_query =
                    @"select [proxy_str]
                    from proxytb
                    where
	                    [reset]='UpdatingIP' and
	                    [proxy_str] not in
	                    (select [target_account]
	                    from actionlog
	                    where
		                    [action]='reset_proxy' and
		                    DATEDIFF(minute,do_at,getdate())<10)";

                try
                {
                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                rest_noactive_last10mins.Add(dataReader["proxy_str"].ToString());
                            }
                        }
                    }

                    rest_noactive_last10mins = rest_noactive_last10mins.Except(resetproxies).ToList();

                    if (rest_noactive_last10mins.Count > 0)
                    {
                        foreach (var item in rest_noactive_last10mins)
                        {
                            if (item == "" || !active_proxies.Contains(item))
                                continue;


                            //Update proxy multi thread

                            Thread thread = new Thread(() => ResetEachProxy(item));
                            threads.Add(thread);
                        }
                    }
                }
                catch { }
            }

            foreach (var t in threads)
            {
                t.Start();
            }
        }


        static public void ClearLogProxy()
        {
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("delete log_threaduseproxy where [threadid]='" + Thread.CurrentThread.Name + "' and pc_device=" + Program.form.pc_devices, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }


        static public void DeleteLogProxy(string threadname_pattern = null, string pc_devices = null)
        {
            if (pc_devices == null)
            {
                pc_devices = Program.form.pc_devices;
            }

            var sql = "delete log_threaduseproxy where pc_device = " + pc_devices;

            //if(threadname_pattern!=null)
            //{
            //    sql = "delete log_threaduseproxy where [threadid] like '" + threadname_pattern + "%' and [proxy_str] in (select[proxy_str] from proxytb where pc_device = " + Program.form.pc_devices + ")";
            //}

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand(sql, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion


        #region --Helpers for register--

        static public void ProxyServer(bool firstrun = false)
        {
            if (firstrun) //neu la lan dau chay tool
            {
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var command = new SqlCommand("update proxytb set reg_status='NotReady'", conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
            //reset each proxy that needed

            List<string> resetproxies = new List<string>();
            List<Thread> threads = new List<Thread>();

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select * from proxytb where reg_status='NotReady' or reg_status='false' or reg_status='' or reg_status is NULL", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            resetproxies.Add(dataReader["proxy_str"].ToString());
                        }
                    }
                }


                foreach (var item in resetproxies)
                {
                    using (var command = new SqlCommand("update proxytb set reg_status='Updating' where proxy_str='" + item + "'", conn))
                    {
                        command.ExecuteNonQuery();

                        //Update proxy multi thread

                        Thread thread = new Thread(() => ResetEachProxy(item, "register"));
                        threads.Add(thread);
                    }
                }
            }

            foreach (var t in threads)
            {
                t.Start();
            }


        }


        static public string GetProxyReg()
        {
            string proxy_str = null;

            while (true)
            {
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var command = new SqlCommand("igdb_GetProxyReg", conn))
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.Add("@threadid", System.Data.SqlDbType.VarChar).Value = Thread.CurrentThread.Name;

                        var proxy = command.ExecuteScalar();

                        if (proxy != null)
                            proxy_str = proxy.ToString();
                    }
                }

                if (proxy_str != null && proxy_str != "")
                    break;

                Thread.Sleep(10000);
            }

            return proxy_str;
        }

        static List<string> Current_ProxiesInTimeRange()
        {
            List<string> proxies = new List<string>();

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                var sqlcommand =
                    @"select proxy_str
                    from proxytb
                    where
	                    ((CONVERT(VARCHAR(8), GETDATE(), 108) > CAST([active_from] as time) and CONVERT(VARCHAR(8), GETDATE(), 108) < CAST([active_to] as time)) and
			                                        (CAST([active_from] as time) < CAST([active_to] as time)))

								                    OR

	                    ((CONVERT(VARCHAR(8), GETDATE(), 108) > CAST([active_from] as time) or CONVERT(VARCHAR(8), GETDATE(), 108) < CAST([active_to] as time)) and
			                                        (CAST([active_from] as time) > CAST([active_to] as time)))

								                    OR
	                    ((active_from is null and active_to is null) or
		                    CAST([active_from] as time) = CAST([active_to] as time)
	                    )";

                using (var command = new SqlCommand(sqlcommand, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            proxies.Add(dataReader["proxy_str"].ToString());
                        }
                    }
                }

            }


            return proxies;
        }

        static public bool ResetEachProxy(string proxy_str, string type = "engage")
        {
            //check if in time range, if not ==> return
            if (!Current_ProxiesInTimeRange().Contains(proxy_str))
                return true;

            RestClient client = new RestClient("http://" + PLAutoHelper.ProxyHelper.Xproxy_ProxyServer_FromProxyStr(proxy_str) + ":10000/");

            var proxy_str_for_request = proxy_str.Replace(":5", ":4");

            var request = new RestRequest("reset?proxy=" + proxy_str_for_request, Method.GET);

        ResetProxy:
            var query = client.Execute(request);

            //add log reset_proxy , use to check if need to reset by another pc_devices in case some pc_devices claimed it and crash result in t never finish reset
            IGHelpers.DBHelpers.AddActionLogByID("155", "reset_proxy", "", proxy_str);

            //Check condition if proxy is ready

            Thread.Sleep(5000);

            Stopwatch watch_Reset = new Stopwatch();
            watch_Reset.Start();

            while (true)
            {
                if (watch_Reset.Elapsed.TotalMinutes > 2)
                {
                    Thread.Sleep(30000);
                    //reset again

                    goto ResetProxy;
                }

                var request_proxy_data = new RestRequest("proxy_list", Method.GET);

                var query_proxy_data = client.Execute(request_proxy_data);

                if (query_proxy_data.StatusCode.ToString() != "OK")
                    continue;

                var json_proxies = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(query_proxy_data.Content);

                var port = proxy_str_for_request.Split(':')[1];

                bool is_changed = false;

                foreach (var proxy in json_proxies)
                {
                    if (proxy["proxy_port"].Value.ToString() == port)
                    {
                        if (proxy["public_ip"].Value != null)
                        {
                            if (proxy["public_ip"].Value.ToString().Contains("1") && proxy["public_ip"].Value.ToString().Contains("."))
                            {
                                is_changed = true;
                            }
                        }

                    }
                }

                if (is_changed)
                    break;

                Thread.Sleep(2000);
            }

            //check if work with fb and ig

            if (!IsWorkFB(proxy_str_for_request))
                goto ResetProxy;

            //get proxy ip address

            var ipconfig_request = ADBSupport.ADBHelpers.ExecuteCMDTask("curl -x " + proxy_str.Replace(":5", ":4") + " ifconfig.me/ip");

            if (ipconfig_request == null)
            {
                goto ResetProxy;
            }

            var ipaddress = (new System.Text.RegularExpressions.Regex("(?<=ifconfig.me\\/ip\\r\\n)(.*?)(?=\\r\\n)")).Match(ipconfig_request).Value;

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                //insert ipaddress to db


                using (var command = new SqlCommand("insert into ipaddress_log (ipaddress) values ('" + ipaddress + "')", conn))
                {
                    command.ExecuteNonQuery();
                }


                //update ready status to db
                if (type == "register")
                {
                    using (var command = new SqlCommand("update proxytb set reg_status='Ready' where proxy_str='" + proxy_str + "'", conn))
                    {
                        command.ExecuteNonQuery();
                    }

                }
                else
                {
                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var command = new SqlCommand("update proxytb set [status]='Ready', current_used=0, used_time=0, [used_mass]=0,[used_light]=0, [reset]='', latest_claimed=NULL where [proxy_str]='" + proxy_str + "'", conn))
                        {
                            command.ExecuteNonQuery();
                        }
                    });
                }
            }
            return true;
        }



        #endregion

        #region --Quick Scrape Proxy Helpers

        public class QuickProxy
        {
            public DateTime last_reset;
            public string system, proxy_port;

            public QuickProxy(string system, string proxy_port, DateTime last_reset)
            {
                this.last_reset = last_reset;
                this.system = system;
                this.proxy_port = proxy_port;
            }
        }

        static public List<QuickProxy> GetListProxies()
        {

            List<QuickProxy> proxies = new List<QuickProxy>();

            foreach (var ip in ADBSupport.LDHelpers.proxy_server_ips)
            {

                RestClient client = new RestClient("http://" + ip + ":10000/");

                var request_proxy_data = new RestRequest("proxy_list", Method.GET);

                var query_proxy_data = client.Execute(request_proxy_data);

                if (query_proxy_data.StatusCode.ToString() != "OK")
                {
                    continue;
                }

                var json_proxies = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(query_proxy_data.Content);


                foreach (var proxy in json_proxies)
                {

                    if (proxy["public_ip"].Value != null)
                    {
                        if (proxy["public_ip"].Value.ToString().Contains("1") && proxy["public_ip"].Value.ToString().Contains("."))
                        {
                            try
                            {
                                if (proxy["last_reset"].Value == null)
                                    continue;
                            }
                            catch
                            {
                                continue;
                            }
                            var last_reset = DateTime.ParseExact(proxy["last_reset"].Value.ToString(), "HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                            var system = proxy["system"].Value.ToString();
                            var proxy_port = proxy["proxy_port"].Value.ToString();

                            proxies.Add(new QuickProxy(
                                system,
                                proxy_port,
                                last_reset
                            ));
                        }
                    }
                }
            }

            //filter last_reset must less than 15 minutes
            proxies = proxies.Where(x => DateTime.Now.Subtract(x.last_reset).TotalMinutes < 15 && DateTime.Now.Subtract(x.last_reset).TotalMinutes > 0).ToList();

            //order last_reset desc
            proxies = proxies.OrderByDescending(x => x.last_reset).ToList();

            return proxies;
        }

        static List<dynamic> log_claim_quickproxy = new List<dynamic>();

        static public string GetProxyForQuickScrapeTask(bool ischeck = true) //check when use for grab slaves comment
        {
            string proxy = null;
            while (true)
            {
                var proxies = GetListProxies();

                if (!ischeck)
                {
                    var choiced = (new Random()).Next(proxies.Count);
                    proxy = proxies[choiced].system + ":" + proxies[choiced].proxy_port;
                    break;
                }

                foreach (var item in proxies)
                {
                    //check if it's claimed in last 10 minutes, if yes, continue

                    var logs = log_claim_quickproxy.Where(x => x.port.ToString() == item.proxy_port).ToList();

                    if (logs.Count > 0)
                    {
                        var log = logs[0];
                        //check if it's claimed in last 10 minutes
                        if (DateTime.Now.Subtract(DateTime.Parse(log.last_claim.ToString())).TotalMinutes < 10)
                            continue;

                        //remove old log
                        log_claim_quickproxy.RemoveAll(x => x.port == item.proxy_port);

                        //add again
                        log_claim_quickproxy.Add(new
                        {
                            port = item.proxy_port,
                            last_claim = DateTime.Now
                        });


                        proxy = item.system + ":" + item.proxy_port;

                        break;
                    }
                    else
                    {
                        proxy = item.system + ":" + item.proxy_port;

                        //add log
                        log_claim_quickproxy.Add(new
                        {
                            port = item.proxy_port,
                            last_claim = DateTime.Now
                        });

                        break;
                    }
                }


                if (proxy != null)
                    break;

                Thread.Sleep(TimeSpan.FromMinutes(1));
            }

            return proxy;
        }

        #endregion
    }
}
