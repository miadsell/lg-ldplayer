﻿using FluentFTP;
using InstagramLDPlayer.ADBSupport;
using InstagramLDPlayer.Class;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGHelpers
{
    static class Helpers
    {

        #region --App Setting--

        static public int thread = 5;
        static public string IGAPK_path = "";
        //static public string proxy_ip = "192.168.100.6";

        #endregion

        #region --Cross--

        static public double scroll_x_min = 20;
        static public double scroll_x_max = 80;

        static public double RandomScroll_X()
        {
            return (new Random()).NextDouble() * (scroll_x_max - scroll_x_min) + scroll_x_min;
        }

        #endregion

        #region --Engage Constants

        static public int min_scrolldown()
        {
            return (new Random()).Next(8, 16);
        }


        /// <summary>
        /// not do action in x hours
        /// </summary>
        static public int actionblocked_sleep = 24;

        static public int swipesuggest_sleep = 80;

        #endregion

        #region --Launch Thread Constants--

        /// <summary>
        /// deviceName, status
        /// </summary>
        static public List<LaunchDevice> launch_devices = new List<LaunchDevice>();

        #endregion

        static public string xampp_server_location = @"C:\xampp\htdocs\igldplayer\";
        static public string xammp_url = "http://maxampp.ddns.net:8080/igldplayer/";

        #region

        #region --Update profile constants--

        static public string folder_profile = @"C:\Users\ad\Google Drive\LDPlayerData\resource\profile";
        static public string folder_bio = @"C:\Users\ad\Google Drive\LDPlayerData\resource\bio";


        #endregion

        #region --Publish Post--

        static public string default_post_text_path = @"C:\Users\ad\Google Drive\LDPlayerData\resource\default post text"; //Default post text under every post
        static public string default_reply_selfcomment = @"C:\Users\ad\Google Drive\LDPlayerData\resource\default reply"; //default comment reply under post
        static public string postimage_path = @"C:\Users\ad\Google Drive\LDPlayerData\resource\postimage";
        static public string watermark_path = @"C:\Users\ad\Google Drive\LDPlayerData\resource\watermark";
        static public string watermark_content_path = @"C:\Users\ad\Google Drive\LDPlayerData\resource\watermark_txt";
        static public string autocomment_path = @"C:\Users\ad\Google Drive\LDPlayerData\resource\autocomment";

        static public string ftp_default_post_text_path = "/resource/default post text"; //Default post text under every post
        static public string ftp_default_reply_selfcomment = "/resource/default reply"; //default comment reply under post
        static public string ftp_postimage_path = "/resource/postimage";
        static public string ftp_watermark_path = "/resource/watermark";
        static public string ftp_watermark_content_path = "/resource/watermark_txt";
        static public string ftp_autocomment_path = "/resource/autocomment";

        #endregion

        static public void UpdateResourceConstants()
        {

            //folder_profile = System.IO.Directory.GetParent(LDHelpers.LDPlayerDATA_path) + @"\resource\profile";
            //folder_bio = System.IO.Directory.GetParent(LDHelpers.LDPlayerDATA_path) + @"\resource\bio";
            //default_post_text_path = System.IO.Directory.GetParent(LDHelpers.LDPlayerDATA_path) + @"\resource\default post text"; //Default post text under every post
            //default_reply_selfcomment = System.IO.Directory.GetParent(LDHelpers.LDPlayerDATA_path) + @"\resource\default reply"; //default comment reply under post
            //postimage_path = System.IO.Directory.GetParent(LDHelpers.LDPlayerDATA_path) + @"\resource\postimage";
            //watermark_path = System.IO.Directory.GetParent(LDHelpers.LDPlayerDATA_path) + @"\resource\watermark";
            //watermark_content_path = System.IO.Directory.GetParent(LDHelpers.LDPlayerDATA_path) + @"\resource\watermark_txt";
            //autocomment_path = System.IO.Directory.GetParent(LDHelpers.LDPlayerDATA_path) + @"\resource\autocomment";

            folder_profile = "/resource/profile";
            folder_bio = "/resource/bio";
            default_post_text_path = "/resource/default post text"; //Default post text under every post
            default_reply_selfcomment = "/resource/default reply"; //default comment reply under post
            postimage_path = "/resource/postimage";
            watermark_path = "/resource/watermark";
            watermark_content_path = "/resource/watermark_txt";
            autocomment_path = "/resource/autocomment";
        }

        #endregion


        #region -Register constants

        static public int min_year_scroll = 18;
        static public int max_year_scroll = 29;

        static public int RandomYear_Birthday()
        {
            return (new Random()).Next(min_year_scroll, max_year_scroll);
        }


        static public int min_follow_register = 5;
        static public int max_follow_register = 10;

        static public int RandomFollowing()
        {
            return (new Random()).Next(min_follow_register, max_follow_register);
        }

        #endregion


        #region --Reset Proxy--





        #endregion

        /// <summary>
        /// Choose random action from list by percentage. List item pattern (action|percentage range)
        /// </summary>
        /// <returns></returns>
        static public string RandomActionByPercentage(List<string> actions, int retry = 10)
        {
            int retry_random = 0;
        RandomActionByPercentage:
            var random_num = (new Random(Guid.NewGuid().GetHashCode())).Next(100);

            for (int i = 0; i < actions.Count; i++)
            {
                int from = int.Parse(actions[i].Split('|')[1].Split('-')[0]);
                int to = int.Parse(actions[i].Split('|')[1].Split('-')[1]);

                if (random_num >= from && random_num < to)
                    return actions[i].Split('|')[0];
            }

            retry_random++;

            if (retry_random > retry)
                return null;

            goto RandomActionByPercentage;
        }

        static public bool IsOnTopHome(string deviceID)
        {
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_isOnTopHome, 1, 1, 0);
                return true;
            }
            catch
            {
                return false;
            }

        }

        static public bool IsOnVideoExploreTab(string deviceID)
        {
            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.LikeBtn_Dark, 1, 3, 1);
                return true;
            }
            catch
            {
                return false;
            }
        }

        #region --Manage mac address of device

        static public string MacAddressOfDevice()
        {
            const int MIN_MAC_ADDR_LENGTH = 12;
            string macAddress = string.Empty;
            long maxSpeed = -1;

            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                string tempMac = nic.GetPhysicalAddress().ToString();
                if (nic.Speed > maxSpeed &&
                    !string.IsNullOrEmpty(tempMac) &&
                    tempMac.Length >= MIN_MAC_ADDR_LENGTH &&
                    !nic.Description.Contains("VirtualBox")
                    )
                {
                    maxSpeed = nic.Speed;
                    macAddress = tempMac;
                }
            }

            return macAddress;
        }

        #endregion


        #region --Grab Comment--
        static ChromeDriver driver = null;

        static public void LaunchDriver()
        {
            ChromeOptions options = new ChromeOptions();

            options.AddArgument(@"--user-data-dir=Profile1");
            options.AddArgument("--mute-audio");
            options.AddArgument("--start-minimized");

            ChromeDriverService chromeDriverService = ChromeDriverService.CreateDefaultService();
            chromeDriverService.HideCommandPromptWindow = true;

            driver = new ChromeDriver(chromeDriverService, options);

            driver.Navigate().GoToUrl("https://www.instagram.com/");

            Process process = null;
            var processes = Process.GetProcessesByName("chrome");

            while (true)
            {
                foreach (var item in processes)
                {
                    if (item.MainWindowTitle.Contains("Instagram"))
                    {
                        process = item;
                        break;
                    }

                }
                if (process != null)
                    break;
            }

            //Add to panel

            //Program.form.AddChromeToPanel(process);
        }

        static public string GrabComments(string post)
        {
            Mutex m_grabcomment = new Mutex(false, "grabcomment");
            m_grabcomment.WaitOne();
            driver.Navigate().GoToUrl(post);


            WaitElement(driver, "//span[@aria-label='Load more comments']"); Thread.Sleep(2000);
            //Hit load more

            for (int i = 0; i < 3; i++)
            {
                try
                {
                    WaitElement(driver, "//span[@aria-label='Load more comments']");
                    var loadmore = driver.FindElementByXPath("//span[@aria-label='Load more comments']");
                    Actions actions = new Actions(driver);
                    actions.MoveToElement(loadmore);
                    actions.Perform();
                    Thread.Sleep(1000);
                    loadmore.Click();
                    Thread.Sleep(1000);
                }
                catch
                {
                    break;
                }
            }

            //Find comment elements

            var comment_element = driver.FindElementsByXPath("//div[@class='C4VMK']/span");

            List<string> comments = new List<string>();

            foreach (var item in comment_element)
            {
                comments.Add(item.Text);
            }

            var choiced = (new Random()).Next(0, comments.Count);

            m_grabcomment.ReleaseMutex();

            return comments[choiced];
        }

        static private void WaitElement(ChromeDriver driver, string xpath)
        {
            var wait_loginagain = new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementIsVisible(By.XPath(xpath)));
        }

        #endregion


        static public bool IsActionBlocked(string deviceID, string deviceName)
        {

            //Check if action blocked

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.ActionBlocked, 1, 0, 0);

                //Hit tell us
                ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.TellUs);

                IGHelpers.DBHelpers.AddActionLog(deviceName, "Action Blocked");

                return true;
            }
            catch
            {
            }

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.RestrictActivity, 1, 0, 0);

                //Hit tell us
                ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.TellUs);

                IGHelpers.DBHelpers.AddActionLog(deviceName, "Action Blocked");

                return true;
            }
            catch
            {
            }

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, BMResource.ActionBlocked.ActionBlocked_Restrictv1, 1, 0, 0);

                if (ADBHelpers.IsImageExisted(deviceID, BMResource.ActionBlocked.ActionBlocked_TellUsv1, 1, 1, 1))
                {
                    //Hit tell us
                    ADBSupport.ADBHelpers.ClickByImage(deviceID, BMResource.ActionBlocked.ActionBlocked_TellUsv1);
                }

                if(ADBHelpers.IsImageExisted(deviceID,BMResource.ActionBlocked.ActionBlocked_OKBtn,1,1,1))
                {
                    ADBHelpers.ClickByImageRandom(deviceID, BMResource.ActionBlocked.ActionBlocked_OKBtn);
                }

                IGHelpers.DBHelpers.AddActionLog(deviceName, "Action Blocked");

                return true;
            }
            catch
            {
            }

            return false;
        }

        #region --IGAPI--

        class APIObj
        {
            public dynamic dyn;
            public DateTime added, last_claimed;

            public APIObj(dynamic dyn, DateTime added, DateTime last_claimed)
            {
                this.dyn = dyn;
                this.added = added;
                this.last_claimed = last_claimed;
            }
        }

        static public dynamic APIObJWithProxy()
        {
            dynamic dyn = null;
            //get available object in APIObjs
            lock (APIObjs)
            {
                TakeAPIObj:
                if (APIObjs.Count > 0)
                {
                    for (int a = 0; a < APIObjs.Count; a++)
                    {
                        //Check iff over 10 minutes, if yes remove from list
                        if (DateTime.Now.Subtract(APIObjs[a].added).TotalMinutes > 10)
                        {
                            APIObjs.RemoveAt(a);
                            a--;
                            continue;
                        }

                        //Check if last claimed is over 30 seconds
                        if (DateTime.Now.Subtract(APIObjs[a].last_claimed).TotalSeconds > 60)
                        {
                            dyn = APIObjs[a].dyn;
                            APIObjs[a].last_claimed = DateTime.Now;
                            break;
                        }
                    }
                }

                if(dyn==null && APIObjs.Count>30)
                {
                    Thread.Sleep(10000);
                    goto TakeAPIObj;
                }

                if (dyn == null)
                {

                    while (dyn == null)
                    {

                        var proxy = IGHelpers.ProxyHelpers.GetProxyForQuickScrapeTask(false);

                        dyn = IGAPIHelper.Scraper.GetAPIObj(proxy, IGHelpers.DBHelpers.db_connection_igtoolkit, "noclaim");

                        if (dyn != null)
                            break;

                        Thread.Sleep(10000);

                    }

                    //add to list
                    APIObjs.Add(new APIObj(dyn, DateTime.Now, DateTime.Now));

                }
            }

            return dyn;
        }

        static private List<APIObj> APIObjs = new List<APIObj>();


        #endregion

        #region --FTP Server--

        static string ftp_connect_string = "ftp_connect";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ftp_file_from">file path</param>
        /// <param name="ftp_destination">folder destination path</param>
        static public void FTP_CopyFile(string ftp_file_from, string ftp_destination)
        {
            if (ftp_file_from.Contains("\\") || ftp_destination.Contains("\\"))
                throw new System.ArgumentException("Remote path can't contains \\");

            bool finish = false;
            while (!finish)
            {
                lock (ftp_connect_string)
                {

                    using (FtpClient client = new FtpClient(LDHelpers.ftp_server_host, LDHelpers.ftp_credential))
                    {

                        // begin connecting to the server
                        client.Connect();

                        //Download file from server to temp
                        var fileName = Path.GetFileName(ftp_file_from);

                        var status = client.DownloadFile("Temp\\" + fileName, ftp_file_from);

                        if (status.ToString() != "Success")
                        {
                            continue;
                        }

                        //Upload to ftp_destination
                        status = client.UploadFile("Temp\\" + fileName, ftp_destination);

                        if (status.ToString() != "Success")
                        {
                            continue;
                        }

                        //Remove temp file
                        File.Delete("Temp\\" + fileName);

                        finish = true;
                    }
                }
            }
        }
    

        static public List<FtpListItem> FTP_GetFileListing(string directory, string search_pattern = null)
        {
            if (directory.Contains("\\"))
                throw new System.ArgumentException("Remote path can't contains \\");


            List<FtpListItem> final_ftp_listings = new List<FtpListItem>();

            lock (ftp_connect_string)
            {

                using (FtpClient client = new FtpClient(LDHelpers.ftp_server_host, LDHelpers.ftp_credential))
                {

                    // begin connecting to the server
                    client.Connect();

                    var ftp_listings = client.GetListing(directory).ToList().Where(x => x.Type == FtpFileSystemObjectType.File).ToList();


                    if (search_pattern != null)
                    {
                        List<string> search_terms = search_pattern.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).ToList();

                        foreach (var item in ftp_listings)
                        {
                            bool match = true;
                            foreach (var s in search_terms)
                            {
                                if (!item.FullName.Contains(s))
                                {
                                    match = false;
                                    break;
                                }
                            }

                            if (match)
                                final_ftp_listings.Add(item);
                        }
                    }
                    else
                        final_ftp_listings.AddRange(ftp_listings);

                }
            }

            return final_ftp_listings;
        }

        static public bool FTP_IsFileExisted(string file_path)
        {
            bool is_existed;
            if (file_path.Contains("\\"))
                throw new System.ArgumentException("Remote path can't contains \\");

            lock (ftp_connect_string)
            {

                using (FtpClient client = new FtpClient(LDHelpers.ftp_server_host, LDHelpers.ftp_credential))
                {

                    // begin connecting to the server
                    client.Connect();

                    is_existed = client.FileExists(file_path);

                }
            }

            return is_existed;
        }

        static public List<FtpListItem> GetDirectoryListing(string directory, string search_pattern = null)
        {
            if (directory.Contains("\\"))
                throw new System.ArgumentException("Remote path can't contains \\");
            var ftp_listings = new List<FtpListItem>();

            lock (ftp_connect_string)
            {

                using (FtpClient client = new FtpClient(LDHelpers.ftp_server_host, LDHelpers.ftp_credential))
                {

                    // begin connecting to the server
                    client.Connect();

                    ftp_listings = client.GetListing(directory).ToList().Where(x => x.Type == FtpFileSystemObjectType.Directory).ToList();

                }
            }

            return ftp_listings;

        }

        static public void FTP_DownloadDirectory(string local_directory, string remote_directory)
        {
            if (remote_directory.Contains("\\"))
                throw new System.ArgumentException("Remote path can't contains \\");

            bool finished = false;

            while (!finished)
            {

                lock (ftp_connect_string)
                {

                    using (FtpClient client = new FtpClient(LDHelpers.ftp_server_host, LDHelpers.ftp_credential))
                    {

                        // begin connecting to the server
                        client.Connect();

                        var status = client.DownloadDirectory(local_directory, remote_directory);

                        if (status.ToString() != "Failed")
                            finished = true;
                    }
                }
            }
        }

        static public bool FTP_IsDirectoryExisted(string directory)
        {
            if (directory.Contains("\\"))
                throw new System.ArgumentException("Remote path can't contains \\");

            bool is_existed = false;

            lock (ftp_connect_string)
            {

                using (FtpClient client = new FtpClient(LDHelpers.ftp_server_host, LDHelpers.ftp_credential))
                {

                    // begin connecting to the server
                    client.Connect();

                    is_existed = client.DirectoryExists(directory);

                }
            }

            return is_existed;
        }

        static public string FTP_CreateDirectory(string directory_path)
        {
            if (directory_path.Contains("\\"))
                throw new System.ArgumentException("Remote path can't contains \\");

            lock (ftp_connect_string)
            {

                using (FtpClient client = new FtpClient(LDHelpers.ftp_server_host, LDHelpers.ftp_credential))
                {

                    // begin connecting to the server
                    client.Connect();

                    client.CreateDirectory(directory_path);

                }
            }

            return directory_path;
        }

        static public void FTP_DeleteDirectory(string remote_directory)
        {
            if (remote_directory.Contains("\\"))
                throw new System.ArgumentException("Remote path can't contains \\");

            lock (ftp_connect_string)
            {

                using (FtpClient client = new FtpClient(LDHelpers.ftp_server_host, LDHelpers.ftp_credential))
                {

                    // begin connecting to the server
                    client.Connect();

                    client.DeleteDirectory(remote_directory);

                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file_path"></param>
        /// <param name="location">support folder location (auto generate file name base on source || support file destination path</param>
        /// <param name="isremovelocal_after_upload"></param>
        static public void FTP_UploadFile(string file_path, string location, bool isremovelocal_after_upload = true)
        {
            if (location.Contains("\\"))
                throw new System.ArgumentException("Remote path can't contains \\");


            bool finished = false;

            while (!finished)
            {

                lock (ftp_connect_string)
                {
                    using (var client = new FtpClient(LDHelpers.ftp_server_host, LDHelpers.ftp_credential))
                    {

                        client.DataConnectionType = FtpDataConnectionType.PASV;

                        // begin connecting to the server
                        client.Connect();

                        //check if location if folder or path

                        string remote_path = location;



                        if (!location.Contains(Path.GetExtension(file_path))) //neu khong chua file extension, tuc no là folder
                        {
                            if (!location.EndsWith("/"))
                                location = location + "/";

                            remote_path = location + Path.GetFileName(file_path);
                        }

                        int retry = 0;
                    FTP_UploadFile:
                        var status = client.UploadFile(file_path, remote_path);

                        if (status.ToString() != "Success")
                        {
                            retry++;
                            if (retry > 3)
                                throw new System.ArgumentException("FTP Upload Failed|" + status.ToString());

                            Thread.Sleep(2000);
                            goto FTP_UploadFile;
                        }

                        finished = true;

                    }
                }

                Thread.Sleep(1000);
            }

            if (isremovelocal_after_upload)
                File.Delete(file_path);
        }

        static public void FTP_MultifileUpload(List<string> file_paths, string location, bool isremovelocal_after_upload = true)
        {
            if (location.Contains("\\"))
                throw new System.ArgumentException("Remote path can't contains \\");


            bool finished = false;

            while (!finished)
            {

                lock (ftp_connect_string)
                {

                    using (FtpClient client = new FtpClient(LDHelpers.ftp_server_host, LDHelpers.ftp_credential))
                    {

                        client.DataConnectionType = FtpDataConnectionType.PASV;

                        // begin connecting to the server
                        client.Connect();

                        //check if location if folder or path


                        if (!location.EndsWith("/"))
                            location = location + "/";

                        foreach (var f in file_paths)
                        {
                            string remote_path = location + Path.GetFileName(f);


                            int retry = 0;
                        FTP_UploadFile:
                            var status = client.UploadFile(f, remote_path);

                            if (status.ToString() != "Success")
                            {
                                retry++;
                                if (retry > 3)
                                    break;

                                Thread.Sleep(5000);
                                goto FTP_UploadFile;
                            }
                        }

                        finished = true;
                    }
                }
            }

            if (isremovelocal_after_upload)
            {
                foreach (var f in file_paths)
                {
                    File.Delete(f);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="remote_filepath"></param>
        /// <param name="local_path">must file path</param>
        static public void FTP_DownloadFile(string remote_filepath, string local_path)
        {

            if (remote_filepath.Contains("\\"))
                throw new System.ArgumentException("Remote path can't contains \\");

            bool finished = false;

            while (!finished)
            {

                lock (ftp_connect_string)
                {

                    using (FtpClient client = new FtpClient(LDHelpers.ftp_server_host, LDHelpers.ftp_credential))
                    {

                        // begin connecting to the server
                        client.Connect();

                        var status = client.DownloadFile(local_path, remote_filepath);

                        if (status.ToString() == "Success")
                            finished = true;

                    }
                }
            }
        }

        static public void FTP_DeleteFile(string remote_filepath)
        {
            if (remote_filepath.Contains("\\"))
                throw new System.ArgumentException("Remote path can't contains \\");

            lock (ftp_connect_string)
            {

                using (FtpClient client = new FtpClient(LDHelpers.ftp_server_host, LDHelpers.ftp_credential))
                {

                    // begin connecting to the server
                    client.Connect();

                    client.DeleteFile(remote_filepath);

                }
            }
        }

        static public string FTP_TextFileContent(string remote_filepath)
        {
            if (remote_filepath.Contains("\\"))
                throw new System.ArgumentException("Remote path can't contains \\");

            string text = null;

            lock (ftp_connect_string)
            {

                using (FtpClient client = new FtpClient(LDHelpers.ftp_server_host, LDHelpers.ftp_credential))
                {

                    // begin connecting to the server
                    client.Connect();

                    var local_temp_path = "Temp\\" + Guid.NewGuid().GetHashCode().ToString() + ".txt";

                    client.DownloadFile(local_temp_path, remote_filepath);

                    text = File.ReadAllText(local_temp_path);

                    File.Delete(local_temp_path);

                }
            }

            return text;
        }

        static public void FTP_WriteTextToFile(string text, string remote_path)
        {
            if (remote_path.Contains("\\"))
                throw new System.ArgumentException("Remote path can't contains \\");
            lock (ftp_connect_string)
            {

                using (FtpClient client = new FtpClient(LDHelpers.ftp_server_host, LDHelpers.ftp_credential))
                {

                    // begin connecting to the server
                    client.Connect();

                    var local_temp_path = "Temp\\" + Guid.NewGuid().GetHashCode().ToString() + ".txt";

                    File.WriteAllText(local_temp_path, text);

                    client.UploadFile(local_temp_path, remote_path);

                    File.Delete(local_temp_path);


                }
            }
        }

        #endregion
    }
}
