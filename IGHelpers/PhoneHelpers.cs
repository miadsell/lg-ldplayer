﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGHelpers
{
    static class PhoneHelpers
    {
        static public string RequestPhone()
        {
            RestClient client = new RestClient("http://textsms.net/");

            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;

            CookieContainer _cookieJar = new CookieContainer();
            client.CookieContainer = _cookieJar;

            var request_phone = new RestRequest("api_mimo/getnumber.php?token=3dac5ff21999b08ad5cb222ddfafa0a5&filter=Instagram", Method.GET);

            var query_phone = client.Execute(request_phone);

            var phone = query_phone.Content;

            return phone.Replace("\"", "");
        }

        static public string GetSms(string phone)
        {
            RestClient client = new RestClient("http://textsms.net/");

            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;

            CookieContainer _cookieJar = new CookieContainer();
            client.CookieContainer = _cookieJar;

            int retry = 0;
        RetrySms:


            var request_sms = new RestRequest("api_mimo/getsms.php?token=3dac5ff21999b08ad5cb222ddfafa0a5&filter=Instagram&number=" + phone, Method.GET);

            var query_sms = client.Execute(request_sms);

            var sms = query_sms.Content;

            if (sms.Contains("NULL"))
            {
                retry++;
                if (retry > 5)
                    return "NULL";
                Thread.Sleep(20000);
                goto RetrySms;
            }

            Regex rg = new Regex("(\\d+)(.*?)(?= is your Instagram code)");

            string confirm_code = rg.Match(sms).Value.Replace(" ", "");

            return confirm_code;
        }

        static private string rentcode_apikey = "k4eb3bOND2hae2KftRUv4crlDeMN36EIY7EZxTa08hYv";

        static int VIETTEL = 1;
        static int VINAPHONE = 2;
        static int MOBIPHONE = 3;

        static public dynamic RequestPhone_RentCode()
        {
            List<int> networks = new List<int>() { VIETTEL, VINAPHONE, MOBIPHONE };

            int n = (new Random()).Next(networks.Count);

            RequestOrder:
            RestClient restClient = new RestClient("https://api.rentcode.net/");

            //Get list service

            RestRequest request_Services = new RestRequest("api/v2/available-services", Method.GET);


            request_Services.AddParameter("apiKey", rentcode_apikey);


            var query_services = restClient.Execute(request_Services);

            var query_json = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(query_services.Content);

            string id_service = "";

            foreach (var item in query_json["results"])
            {
                if (item["name"].Value.ToString().Contains("instagram"))
                {
                    id_service = item["id"].Value.ToString();
                }
            }

            //Request phone

            var request_neworder = new RestRequest("api/v2/order/request", Method.GET);


            request_neworder.AddParameter("apiKey", rentcode_apikey);
            request_neworder.AddParameter("ServiceProviderId", id_service);
            request_neworder.AddParameter("MaximumSms", "1");
            request_neworder.AddParameter("networkProvider", n);

            var query_neworder = restClient.Execute(request_neworder);

            var orderid = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(query_neworder.Content)["id"].Value.ToString();

            string phonenum = "";

            Stopwatch watch_getnum = new Stopwatch();
            watch_getnum.Start();

            GetPhoneNum:
            while (true)
            {
                var request_getnum = new RestRequest("api/v2/order/" + orderid + "/check", Method.GET);

                request_getnum.AddParameter("apiKey", rentcode_apikey);

                var query_getnum = restClient.Execute(request_getnum);

                if(query_getnum.StatusCode.ToString()=="NotFound")
                {
                    Thread.Sleep(2000);

                    if (watch_getnum.Elapsed.Minutes > 1)
                        return null;
                    continue;
                }

                if (Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(query_getnum.Content)["phoneNumber"].Value != null)
                {
                    phonenum = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(query_getnum.Content)["phoneNumber"].Value.ToString();
                    break;
                }

                Thread.Sleep(2000);

                if (watch_getnum.Elapsed.TotalMinutes > 1)
                    goto RequestOrder;

            }

            if (orderid != "" && phonenum == "")
            {
                n++;
                if (n == 3)
                    n = 0;
                goto GetPhoneNum;
            }

            return new
            {
                orderid,
                phonenum
            };

        }

        static public string GetSMS_RentCode(string orderid, int wait_minutes)
        {
            RestClient restClient = new RestClient("https://api.rentcode.net/");


            string message = "";

            Stopwatch watch_waitsms = new Stopwatch();
            watch_waitsms.Start();

            GetSMS_RentCode:
            while (true)
            {
                var request_getnum = new RestRequest("api/v2/order/" + orderid + "/check", Method.GET);

                request_getnum.AddParameter("apiKey", rentcode_apikey);

                var query_getnum = restClient.Execute(request_getnum);

                if(query_getnum.StatusCode.ToString()=="NotFound")
                {
                    Thread.Sleep(2000);

                    if (watch_waitsms.Elapsed.Minutes > wait_minutes)
                        return null;
                    continue;
                }

                if (Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(query_getnum.Content)["message"].Value != null)
                {
                    message = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(query_getnum.Content)["message"].Value.ToString();
                    break;
                }

                Thread.Sleep(5000);

                if (watch_waitsms.Elapsed.Minutes > wait_minutes)
                    return null;
            }

            if (message == "")
                goto GetSMS_RentCode;

            Regex rg = new Regex("(\\d+)(.*?)(?= to verify your Instagram account)");

            string confirm_code = rg.Match(message).Value.Replace(" ", "");

            if (confirm_code != "")
                return confirm_code;

            rg = new Regex("(\\d+)(.*?)(?= is your Instagram code)");

            confirm_code = rg.Match(message).Value.Replace(" ", "");

            return confirm_code;
        }


        static public string RequestPhone_Reuse(string phonenumber)
        {

            phonenumber = "0" + phonenumber;

            RestClient restClient = new RestClient("https://api.rentcode.net/");

            

            //Request phone

            var request_neworder = new RestRequest("api/v2/order/request-reuse", Method.GET);


            request_neworder.AddParameter("apiKey", rentcode_apikey);
            request_neworder.AddParameter("RequestPhoneNumber", phonenumber);

            var query_neworder = restClient.Execute(request_neworder);

            if (Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(query_neworder.Content)["success"].Value == false)
                return null;

            var orderid = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(query_neworder.Content)["id"].Value.ToString();


            return orderid;
        }
    }
}
