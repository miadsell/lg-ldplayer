﻿
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGHelpers
{
    static class Settings
    {
        static public dynamic Settings_GetSettingByName(string accountId, string setting)
        {
            //get setting from db
            using (SqlConnection conn = new SqlConnection(DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select aio_settings from account_info where [accountId]=" + accountId, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        dataReader.Read();

                        if (dataReader["aio_settings"] == null)
                            return null;
                        return Newtonsoft.Json.JsonConvert.DeserializeObject(dataReader["aio_settings"].ToString());
                    }
                }
            }
        }

        static private string Settings_UpdateByMerge(string original_json, string merge_json)
        {
            JObject originalObj = JObject.Parse(original_json);

            JObject mergeObj = JObject.Parse(merge_json);

            originalObj.Merge(mergeObj);

            var new_json = originalObj.ToString(Newtonsoft.Json.Formatting.None);

            return new_json;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usernames"></param>
        /// <param name="update_jsons"></param>
        /// <param name="data_type">user|id</param>
        static public void Settings_UpdateByUsername(List<string> datas, List<string> update_jsons, string data_type)
        {

            List<string> usernames = new List<string>();

            switch (data_type)
            {
                case "user":
                    usernames = datas;
                    break;
                case "id":
                    using (SqlConnection conn = new SqlConnection(DBHelpers.db_connection))
                    {
                        conn.Open();

                        foreach(var item in datas)
                        {
                            var command_query = "select [user] from ig_account where [id] = @id";

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = item;
                                using(var dataReader=command.ExecuteReader())
                                {
                                    dataReader.Read();
                                    usernames.Add(dataReader["user"].ToString());
                                }
                            }
                        }

                    }
                    break;
                default:
                    throw new System.ArgumentException("Not recognize this data type");

            }

            using (SqlConnection conn = new SqlConnection(DBHelpers.db_connection))
            {
                conn.Open();

                foreach (var item in usernames)
                {
                    //get current json
                    string current_json = "{}";

                    using (var command = new SqlCommand("select aio_settings from account_info where [accountId]=(select [id] from ig_account where [user]='" + item + "')", conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            dataReader.Read();

                            if (dataReader["aio_settings"] != null && dataReader["aio_settings"].ToString()!="")
                                current_json = dataReader["aio_settings"].ToString();
                        }
                    }

                    foreach (var json in update_jsons)
                    {
                        current_json = Settings_UpdateByMerge(current_json, json);
                    }

                    //update to db
                    using (var command = new SqlCommand("update account_info set [aio_settings]=@aio_settings where [accountId]=(select [id] from ig_account where [user]='" + item + "')", conn))
                    {
                        command.Parameters.Add("@aio_settings", System.Data.SqlDbType.NText).Value = current_json;
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="datas"></param>
        /// <param name="j_path">path to remove</param>
        /// <param name="data_type"></param>
        static public void Settings_RemoveSettingsByUsername(List<string> datas, string j_path, string data_type)
        {
            List<string> usernames = new List<string>();

            switch (data_type)
            {
                case "user":
                    usernames = datas;
                    break;
                case "id":
                    using (SqlConnection conn = new SqlConnection(DBHelpers.db_connection))
                    {
                        conn.Open();

                        foreach (var item in datas)
                        {
                            var command_query = "select [user] from ig_account where [id] = @id";

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = item;
                                using (var dataReader = command.ExecuteReader())
                                {
                                    dataReader.Read();
                                    usernames.Add(dataReader["user"].ToString());
                                }
                            }
                        }

                    }
                    break;
                default:
                    throw new System.ArgumentException("Not recognize this data type");

            }

            using (SqlConnection conn = new SqlConnection(DBHelpers.db_connection))
            {
                conn.Open();

                foreach (var item in usernames)
                {
                    //get current json
                    string current_json = "{}";

                    using (var command = new SqlCommand("select aio_settings from account_info where [accountId]=(select [id] from ig_account where [user]='" + item + "')", conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            dataReader.Read();

                            if (dataReader["aio_settings"] != null && dataReader["aio_settings"].ToString() != "")
                                current_json = dataReader["aio_settings"].ToString();
                        }
                    }

                    current_json = Settings_RemoveSettings(current_json, j_path);

                    //update to db
                    using (var command = new SqlCommand("update account_info set [aio_settings]=@aio_settings where [accountId]=(select [id] from ig_account where [user]='" + item + "')", conn))
                    {
                        command.Parameters.Add("@aio_settings", System.Data.SqlDbType.NText).Value = current_json;
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        #region --helper--

        /// <summary>
        /// remove all childs of jpath node (not remove jpath node, only childs)
        /// </summary>
        /// <param name="json"></param>
        /// <param name="jpath"></param>
        /// <returns></returns>
        static private string Settings_RemoveSettings(string json, string jpath)
        {
            JObject j_obj = JObject.Parse(json);
            //"$..Task_MassDM"
            var childs = j_obj.SelectTokens(jpath).Children();//.ToList();


            List<JToken> removeList = new List<JToken>();
            foreach (var item in childs)
            {
                removeList.Add(item);
            }

            foreach (var item in removeList)
            {
                item.Remove();
            }

            return j_obj.ToString();
        }


        static public List<string> Settings_ListUnderChilds_Name(string json, string jpath)
        {
            JObject j_obj = JObject.Parse(json);
            //"$..Task_MassDM"
            var childs = j_obj.SelectTokens(jpath).Children();//.ToList();

            List<string> childs_name = new List<string>();

            foreach (var item in childs)
            {
                JProperty jProperty = item.ToObject<JProperty>();
                string propertyName = jProperty.Name;

                childs_name.Add(propertyName);
            }

            return childs_name;
        }

        #endregion
    }
}
