﻿using InstagramLDPlayer.Class;
using InstagramLDPlayer.IGThreads;
using OpenQA.Selenium.Remote;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGHelpers
{
    static class DBHelpers
    {
        static public string db_connection = @"Data Source=@server_ip;Initial Catalog=@instagramdb;User ID=sa;Password=123456789kdL;Pooling=true;Max Pool Size=150;Min Pool Size=50;Connect Timeout=30;Connection Lifetime=300;ConnectRetryCount=3";
        static public string db_connection_igtoolkit = @"Data Source=@server_ip;Initial Catalog=@igtoolkit;User ID=sa;Password=123456789kdL;Pooling=false;Max Pool Size=150;Min Pool Size=50;Connect Timeout=30;Connection Lifetime=300;ConnectRetryCount=3";

        static public dynamic DynamicIGAccountByID(string accountId)
        {
            dynamic ig_account = null;

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                var command_query =
                    @"select *
                    from ig_account
                    join account_info on ig_account.id=account_info.accountId where [id]=@id";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = accountId;
                    using (var dataReader = command.ExecuteReader())
                    {
                        dataReader.Read();
                        string id = dataReader["id"].ToString();
                        string user = dataReader["user"].ToString();
                        string fullname = dataReader["fullname"].ToString();
                        string phone_device = dataReader["phone_device"].ToString();
                        string type = dataReader["type"] == null ? "" : dataReader["type"].ToString();
                        string mode = dataReader["mode"].ToString();
                        dynamic aio_settings = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(dataReader["aio_settings"].ToString());
                        ig_account = new
                        {
                            id,
                            fullname,
                            phone_device,
                            type,
                            mode,
                            user,
                            aio_settings
                        };
                    }
                }
            }

            return ig_account;
        }

        //static public dynamic GetDataToReg()
        //{
        //    dynamic data = null;
        //    Mutex m_reg = new Mutex(false, "ig_datareg");
        //    m_reg.WaitOne();

        //    using (SqlConnection conn = new SqlConnection(db_connection))
        //    {
        //        conn.Open();

        //        using (var command = new SqlCommand("ig_GetAccountToRegister", conn))
        //        {
        //            command.CommandType = System.Data.CommandType.StoredProcedure;
        //            command.Parameters.Add("@pc_devices", System.Data.SqlDbType.VarChar).Value = Program.form.pc_devices;

        //            using (var dataReader = command.ExecuteReader())
        //            {
        //                if (dataReader.HasRows)
        //                {
        //                    dataReader.Read();

        //                    string id = dataReader["id"].ToString();
        //                    string fullname = dataReader["fullname"].ToString();
        //                    string phone_device = dataReader["phone_device"].ToString();
        //                    string register = dataReader["register"].ToString();
        //                    data = new
        //                    {
        //                        id,
        //                        fullname,
        //                        phone_device,
        //                        register
        //                    };
        //                }
        //            }
        //        }
        //    }

        //    m_reg.ReleaseMutex();

        //    return data;
        //}

        //static public dynamic GetAccountToEngage()
        //{
        //    dynamic data = null;
        //    Mutex m_reg = new Mutex(false, "ig_account_engage");
        //    m_reg.WaitOne();

        //    using (SqlConnection conn = new SqlConnection(db_connection))
        //    {
        //        conn.Open();

        //        using (var command = new SqlCommand("ig_GetAccountToEngage", conn))
        //        {
        //            command.CommandType = System.Data.CommandType.StoredProcedure;
        //            command.Parameters.Add("@pc_devices", System.Data.SqlDbType.VarChar).Value = Program.form.pc_devices;

        //            using (var dataReader = command.ExecuteReader())
        //            {
        //                if (dataReader.HasRows)
        //                {
        //                    dataReader.Read();

        //                    string id = dataReader["id"].ToString();
        //                    string time_per_session = dataReader["time_per_session"].ToString();
        //                    string user = dataReader["user"].ToString();
        //                    string fullname = dataReader["fullname"].ToString();
        //                    string phone_device = dataReader["phone_device"].ToString();
        //                    string register = dataReader["register"].ToString();
        //                    string type = dataReader["type"] == null ? "" : dataReader["type"].ToString();
        //                    string mode = dataReader["mode"].ToString();
        //                    data = new
        //                    {
        //                        id,
        //                        time_per_session,
        //                        user,
        //                        fullname,
        //                        phone_device,
        //                        register,
        //                        type,
        //                        mode
        //                    };
        //                }
        //            }
        //        }
        //    }

        //    m_reg.ReleaseMutex();

        //    return data;
        //}

        //static public dynamic GetAccountToQueueTask()
        //{
        //    dynamic data = null;
        //    Mutex m_reg = new Mutex(false, "ig_account_queuetask");
        //    m_reg.WaitOne();

        //    using (SqlConnection conn = new SqlConnection(db_connection))
        //    {
        //        conn.Open();

        //        using (var command = new SqlCommand("ig_GetAccountToDoQueueTask", conn))
        //        {
        //            command.CommandType = System.Data.CommandType.StoredProcedure;
        //            command.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = Program.form.pc_devices;

        //            using (var dataReader = command.ExecuteReader())
        //            {
        //                if (dataReader.HasRows)
        //                {
        //                    dataReader.Read();

        //                    string id = dataReader["id"].ToString();
        //                    string fullname = dataReader["fullname"].ToString();
        //                    string phone_device = dataReader["phone_device"].ToString();
        //                    string register = dataReader["register"].ToString();
        //                    string type = dataReader["type"] == null ? "" : dataReader["type"].ToString();
        //                    string mode = dataReader["mode"].ToString();
        //                    string user = dataReader["user"].ToString();
        //                    data = new
        //                    {
        //                        id,
        //                        fullname,
        //                        phone_device,
        //                        register,
        //                        type,
        //                        mode,
        //                        user
        //                    };
        //                }
        //            }
        //        }
        //    }

        //    m_reg.ReleaseMutex();

        //    Program.form.RefreshForm();

        //    return data;
        //}

        //static public dynamic GetAccountDoNormalUserTask()
        //{
        //    dynamic data = null;
        //    Mutex m_reg = new Mutex(false, "ig_account_normalusertask");
        //    m_reg.WaitOne();

        //    using (SqlConnection conn = new SqlConnection(db_connection))
        //    {
        //        conn.Open();

        //        PLAutoHelper.SQLHelper.Execute(() =>
        //        {
        //            using (var command = new SqlCommand("igdb_GetAccountDoNormalUserTask", conn))
        //            {
        //                command.CommandType = System.Data.CommandType.StoredProcedure;
        //                command.Parameters.Add("@pc_devices", System.Data.SqlDbType.VarChar).Value = Program.form.pc_devices;

        //                using (var dataReader = command.ExecuteReader())
        //                {
        //                    if (dataReader.HasRows)
        //                    {
        //                        dataReader.Read();

        //                        string id = dataReader["id"].ToString();
        //                        string fullname = dataReader["fullname"].ToString();
        //                        string phone_device = dataReader["phone_device"].ToString();
        //                        string register = dataReader["register"].ToString();
        //                        string type = dataReader["type"] == null ? "" : dataReader["type"].ToString();
        //                        string mode = dataReader["mode"].ToString();
        //                        string user = dataReader["user"].ToString();
        //                        dynamic aio_settings = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(dataReader["aio_settings"].ToString());
        //                        data = new
        //                        {
        //                            id,
        //                            fullname,
        //                            phone_device,
        //                            register,
        //                            type,
        //                            mode,
        //                            user,
        //                            aio_settings
        //                        };
        //                    }
        //                }
        //            }
        //        }, "ig_account");
        //    }

        //    m_reg.ReleaseMutex();

        //    return data;
        //}

        //static public dynamic GetAccountDoSpecificTasks(string taskname)
        //{
        //    //if (ADBSupport.LDHelpers.LDPlayer_mode == "switch")
        //    //{
        //    //    switch (taskname)
        //    //    {
        //    //        case "Backup":
        //    //            return null;
        //    //    }
        //    //}
        //    dynamic device = null;
        //    Mutex m_createdevice = new Mutex(false, "ig_dospecific_task");
        //    m_createdevice.WaitOne();
        //    using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
        //    {
        //        conn.Open();

        //        using (var command = new SqlCommand("igtoolkit_DoSpecificTask_Account", conn))
        //        {
        //            command.CommandType = System.Data.CommandType.StoredProcedure;
        //            command.Parameters.Add("@pc_devices", System.Data.SqlDbType.VarChar).Value = Program.form.pc_devices;
        //            command.Parameters.Add("@taskname", System.Data.SqlDbType.VarChar).Value = taskname;
        //            using (var dataReader = command.ExecuteReader())
        //            {
        //                if (dataReader.HasRows)
        //                {
        //                    dataReader.Read();
        //                    {
        //                        string id = dataReader["id"].ToString();
        //                        string user = dataReader["user"].ToString();
        //                        string fullname = dataReader["fullname"].ToString();
        //                        string phone_device = dataReader["phone_device"].ToString();
        //                        string type = dataReader["type"] == null ? "" : dataReader["type"].ToString();
        //                        string mode = dataReader["mode"].ToString();
        //                        device = new
        //                        {
        //                            id,
        //                            fullname,
        //                            phone_device,
        //                            type,
        //                            mode,
        //                            user
        //                        };
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    m_createdevice.ReleaseMutex();

        //    Program.form.RefreshForm();

        //    return device;
        //}

        //static public dynamic GetAccountToLogin(string mode)
        //{
        //    dynamic data = null;
        //    Mutex m_reg = new Mutex(false, "ig_account_engage");
        //    m_reg.WaitOne();

        //    using (SqlConnection conn = new SqlConnection(db_connection))
        //    {
        //        conn.Open();

        //        using (var command = new SqlCommand("ig_GetNewAccountToLogin", conn))
        //        {
        //            command.CommandType = System.Data.CommandType.StoredProcedure;
        //            command.Parameters.Add("@pc_devices", System.Data.SqlDbType.VarChar).Value = Program.form.pc_devices;
        //            command.Parameters.Add("@mode", System.Data.SqlDbType.VarChar).Value = mode;

        //            using (var dataReader = command.ExecuteReader())
        //            {
        //                if (dataReader.HasRows)
        //                {
        //                    dataReader.Read();

        //                    string id = dataReader["id"].ToString();
        //                    string user = dataReader["user"].ToString();
        //                    string password = dataReader["password"].ToString();
        //                    string phone_device = dataReader["phone_device"].ToString();
        //                    string phonenum = dataReader["phonenum"].ToString();
        //                    data = new
        //                    {
        //                        id,
        //                        user,
        //                        password,
        //                        phone_device,
        //                        phonenum
        //                    };
        //                }
        //            }
        //        }
        //    }

        //    m_reg.ReleaseMutex();

        //    return data;
        //}

        

        

        static public dynamic GetDeviceByName(string deviceName)
        {
            dynamic deviceinfo = null;
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select * from ldplayers where ld_name='" + deviceName + "'", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        dataReader.Read();

                        deviceinfo = new
                        {
                            manufacturer = dataReader["manufacturer"].ToString(),
                            model = dataReader["model"].ToString(),
                            imei = dataReader["imei"].ToString(),
                            imsi = dataReader["imsi"].ToString(),
                            simserial = dataReader["simserial"].ToString(),
                            androidid = dataReader["androidid"].ToString(),
                            hard_serial = dataReader["hard_serial"].ToString(),
                        };
                    }
                }
            }

            return deviceinfo;
        }

        #region --Engage Support--

        /// <summary>
        /// lay thoi gian gan nhat da thuc hien action
        /// </summary>
        static public string GetLatestActionDate(string deviceName, string actionlog_name)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using(var command=new SqlCommand("ig_LatestActionLog", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    command.Parameters.Add("@deviceName", System.Data.SqlDbType.VarChar).Value = deviceName;
                    command.Parameters.Add("@actionname", System.Data.SqlDbType.VarChar).Value = actionlog_name;

                    using(var dataReader=command.ExecuteReader())
                    {
                        if(dataReader.HasRows)
                        {
                            dataReader.Read();

                            return (dataReader["do_at"].ToString());
                        }
                    }
                }
            }

            return null;
        }


        #endregion

        #region --Account manage--

        static public void UpdateRegisterStatus(string id, string status, string password = "", string phonenum = "")
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("update ig_account set [password]='" + password + "',phonenum='" + phonenum + "',register='" + status + "',[status]='' where id=" + id, conn))
                {
                    command.ExecuteNonQuery();
                }
            }


            
        }

        static public void UpdateStatusofIG(string id = null, string status = "", string pc_devices=null)
        {

            if (pc_devices == null)
                pc_devices = Program.form.pc_devices;

            string command_query = "";

            //AIO devices

            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                if (id == null)
                {
                    //set for all accounts claimed by pc_devices

                    command_query =
                        @"update ig_account set [status]=@status
	                    where
		                    [status] like '%|By " + pc_devices + "%'";
                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@status", System.Data.SqlDbType.VarChar).Value = status;

                        command.ExecuteNonQuery();
                    }
                }
                else
                {
                    command_query =
                        @"update ig_account set [status]=@status
	                    where [id]=@id";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@status", System.Data.SqlDbType.VarChar).Value = status;
                        command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        static public void UpdateModeofIG(string accountId, string mode)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("update ig_account set [mode]='" + mode + "' where [id]='" + accountId + "'", conn))
                {
                    sqlCommand.ExecuteNonQuery();
                }
            }
        }


        static public bool IsAccountState(string deviceName, string check_state)
        {
            var sql = "select top 1 [do_at] from actionlog join ig_account on actionlog.accountid = ig_account.id where ig_account.phone_device = '" + deviceName + "' and[action] = '"+check_state+"' order by actionlog.id desc";
            using (SqlConnection conn=new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command=new SqlCommand(sql,conn))
                {
                    var return_value = command.ExecuteScalar();
                    if(return_value!=null)
                    {
                        var do_at = DateTime.Parse(return_value.ToString());
                        int sleep_time = 0;
                        switch (check_state)
                        {
                            case "Action Blocked":
                                sleep_time = IGHelpers.Helpers.actionblocked_sleep;
                                break;
                            case "Swipe Suggested For You":
                                sleep_time = IGHelpers.Helpers.swipesuggest_sleep;
                                break;
                        }
                        if (DateTime.Now.Subtract(do_at).TotalHours > sleep_time)
                            return false;
                        return true;
                    }
                }
            }

            return false;
        }

        static public void UpdateStateofIG(string deviceName, string state)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var sqlCommand = new SqlCommand("update ig_account set [state]='" + state + "' where phone_device='" + deviceName + "'", conn))
                {
                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        //static public void ImportAccountFromFirefox()
        //{
        //    using (SqlConnection conn = new SqlConnection(db_connection))
        //    {
        //        conn.Open();

        //        using (var sqlCommand = new SqlCommand("ig_ImportAccountFromFirefox", conn))
        //        {

        //            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
        //            sqlCommand.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = Program.form.pc_devices;

        //            sqlCommand.ExecuteNonQuery();
        //        }
        //    }
        //}

        #endregion

        #region --Device Manage--


        static public void AddDevice(string deviceName, string google_account)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("insert into ldplayers (ld_name,google_account) values ('" + deviceName + "','" + google_account + "')", conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }


        //static public void RemoveUnusedDevice(List<string> devices)
        //{
        //    using (SqlConnection conn = new SqlConnection(db_connection))
        //    {
        //        conn.Open();

        //        foreach (var device in devices)
        //        {

        //            using (var command = new SqlCommand("delete ldplayers where [ld_name]='" + device + "'", conn))
        //            {
        //                command.ExecuteNonQuery();
        //            }
        //        }
        //    }
        //}

        #endregion

        #region --Handle Exception & LOG--

        static public string AddLogException(string deviceName, string message, string stack_trace)
        {
            string log_id = "";
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand("insert into log_exception (deviceName,message,stack_trace) output inserted.id values ('" + deviceName + "','" + message.Replace("'", "") + "','" + stack_trace + "')", conn))
                {
                    log_id = command.ExecuteScalar().ToString();
                }
            }

            return log_id;
        }

        static public void AddActionLog(string deviceName, string log, string value="",string target_account="")
        {
            string accountid = null;
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select [id] from ig_account where phone_device='" + deviceName + "'", conn))
                {
                    accountid = command.ExecuteScalar().ToString();
                }

                var final_command = "insert into actionlog (accountid,[action],[value],[target_account]) values (" + accountid + ",'" + log + "','" + value + "','" + target_account + "')";


                using (var command = new SqlCommand(final_command, conn))
                {
                    command.ExecuteNonQuery();
                }

            }
        }

        static public void AddActionLogByID(string id, string log, string value = "", string target_account = "")
        {
            string accountid = null;
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                var final_command = "insert into actionlog (accountid,[action],[value],[target_account]) values (" + id + ",'" + log + "','" + value + "','" + target_account + "')";


                using (var command = new SqlCommand(final_command, conn))
                {
                    command.ExecuteNonQuery();
                }

            }
        }

        #endregion

        #region --IGTask Support--

        #region --ScanRequired-Specific Task--

        /// <summary>
        /// Add important action if not do yet
        /// </summary>
        static public void ScanRequiredTask()
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("ig_ScanRequiredTask", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    command.ExecuteNonQuery();
                }
            }
        }

        static public void ScanSpecificTask()
        {
            var command_query = "";

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();


                /*Insert ig_account that aren't added to specifictask*/

                command_query =
                    @"insert into specifictasks ([accountId])
                    select [id] from ig_account
                    where
	                    [id] not in (select [accountId] from specifictasks)";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region --Post Task--


        static public dynamic GetPostToPublish(string accountId, string include_in_settings=null)
        {
            dynamic post = null;

            var sql = "ig_GetPostToPublish";

            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand(sql, conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;

                    if (include_in_settings == null)
                        command.Parameters.Add("@settings", System.Data.SqlDbType.VarChar).Value = DBNull.Value;
                    else
                        command.Parameters.Add("@settings", System.Data.SqlDbType.VarChar).Value = include_in_settings;


                    using (var dataReader=command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();

                            var id = dataReader["id"].ToString();
                            var content = dataReader["content"].ToString();
                            var image = dataReader["image"].ToString();
                            var hashtag = dataReader["hashtag"].ToString();
                            var settings = dataReader["settings"].ToString();


                            post = new
                            {
                                id,
                                content,
                                image,
                                hashtag,
                                settings
                            };
                        }
                    }
                }
            }

            return post;
        }



        #endregion

        static public void AddQueueTask(string phone_device, string action)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("AddQueueTask", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.Add("@phone_device", System.Data.SqlDbType.VarChar).Value = phone_device;
                    command.Parameters.Add("@action", System.Data.SqlDbType.VarChar).Value = action;

                    command.ExecuteNonQuery();
                }
            }
        }

        static public void ClearQueueTask(bool clear_all = false, string pc_devices = null)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                string command_query = "update queuetasks set [status]='' where [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText(pc_devices) + @"'";

                if (clear_all)
                {
                    command_query = "update queuetasks set [status]='' where [status]='Claimed'";
                }

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        static public void UpdateEngageInfo(string id, double minutes)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("ig_UpdateAccountEngage", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = int.Parse(id);
                    command.Parameters.Add("@minutes", System.Data.SqlDbType.Float).Value = minutes;

                    command.ExecuteNonQuery();
                }
            }
        }

        static public void IsNeedGenerateTask()
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("ig_GenerateStats", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.ExecuteNonQuery();
                }
            }
        }

        static public void IsNeedGenerateOFFDay()
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("ig_GenerateOffDay", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.ExecuteNonQuery();
                }
            }
        }

        static public dynamic GetRandomSearchTerms(string accountId)
        {
            dynamic search_term = null;
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select count(*) from actionlog where [action]='Search People' and [accountid]=404 and DATEDIFF(day,do_at,getdate())<2", conn))
                {
                    if (int.Parse(command.ExecuteScalar().ToString()) > 0)
                        return null;
                }

                using (var command = new SqlCommand("select top 1 * from searchterms order by NEWID()", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        dataReader.Read();

                        search_term = new
                        {
                            term = dataReader["term"].ToString(),
                            type = dataReader["type"].ToString(),
                        };
                    }
                }
            }

            return search_term;
        }

        #endregion

        #region --Manage PC--

        static public string CheckDevice()
        {
            var mac_address = IGHelpers.Helpers.MacAddressOfDevice();

            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select [device_id] from devices where macaddress='" + mac_address + "'", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();
                            return dataReader[0].ToString();
                        }
                    }
                }

                using (var command = new SqlCommand("insert into devices (macaddress,[name]) OUTPUT inserted.[device_id] values ('" + mac_address + "','" + Environment.MachineName + "')", conn))
                {
                    var device_id = command.ExecuteScalar().ToString();
                    return device_id;
                }
            }
        }

        static public void SettingUpLDConstants()
        {
            dynamic devices;
            var pc_devices = IGHelpers.DBHelpers.CheckDevice();

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("select * from devices where device_id=" + pc_devices, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        dataReader.Read();

                        devices = new
                        {
                            LDPlayer_path = dataReader["ldplayer_path"].ToString(),
                            LDPlayer_data_path = dataReader["ldplayer_data_path"].ToString(),
                            LDPlayer_Origin_name = dataReader["ld_original_name"].ToString(),
                            LDPlayer_Threads = int.Parse(dataReader["threads"].ToString()),
                            LDPlayer_FocusEngage_Threads = int.Parse(dataReader["focus_engage_threads"].ToString()),
                            LDPlayer_FocusMassAction_Threads_Count = int.Parse(dataReader["focus_mass_threads"].ToString()),
                            LDPlayer_mode = dataReader["mode"].ToString(),
                            //LDPlayer_switch_manual = dataReader["manual_switch_index"].ToString(),
                            //LDPlayer_manual_proxy = dataReader["manual_proxy"].ToString(),
                            LDPlayer_permission = dataReader["permission"] == null ? "" : dataReader["permission"].ToString(),
                            scraper_cookie_location = dataReader["scraper_cookie_location"].ToString(),
                            sleep_between_launch = dataReader["sleep_between_launch"].ToString(),
                            ftp_username = dataReader["ftp_username"].ToString(),
                            ftp_password = dataReader["ftp_password"].ToString()
                        };
                    }
                }
            }

            ADBSupport.LDHelpers.UpdateLDConstants(devices);
            //IGTasks.FFPCSwitch.SettingFFPCSwitch_Constants(devices);
        }

        #endregion

        #region --Do specific task--

        static public object FollowingUser_TargetAccount = new object();

        static public dynamic GetTargetAccount_SpecificTasks(string taskname, string accountId = null)
        {
            dynamic profile = null;


        //Mutex m = null;

        //if (taskname == "FollowingUser")
        //{
        //    m = new Mutex(false, "igdb_DoSpecificTask_ScrapeUser_FollowingUser");
        //    m.WaitOne();
        //}

        GetTargetAccount_SpecificTasks:
            try
            {
                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var command = new SqlCommand("igdb_DoSpecificTask_ScrapeUser", conn))
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.Add("@tasktype", System.Data.SqlDbType.VarChar).Value = taskname;

                        if (accountId != null)
                            command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;

                        command.CommandTimeout = 300;

                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                dataReader.Read();

                                var pk = long.Parse(dataReader["pk"].ToString());
                                var username = dataReader["UserName"].ToString();

                                profile = new
                                {
                                    pk,
                                    username
                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("was deadlocked on lock resources with another process and has been chosen as the deadlock victim") && taskname == "FollowingUser")
                {
                    lock (FollowingUser_TargetAccount)
                    {
                        goto GetTargetAccount_SpecificTasks;
                    }
                }
                throw;
            }

            //if (taskname == "FollowingUser")
            //{
            //    m.ReleaseMutex();
            //}

            return profile;
        }

        //run when first start function relate to igtoolkit db
        static public void ClearStatusOfUserScrape(bool clear_all = false, string pc_devices = null)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                while (true)
                {

                    using (var sqlTrans = conn.BeginTransaction())
                    {
                        try
                        {

                            string command_query = "update scrapeusers set [status]='' where [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText(pc_devices) + @"' and [state]='READY'";

                            if (clear_all)
                            {
                                command_query = "update scrapeusers set [status]='' where [status]='Claimed' and [state]='READY'";
                            }

                            using (var command = new SqlCommand(command_query, conn, sqlTrans))
                            {
                                command.ExecuteNonQuery();
                            }
                            sqlTrans.Commit();
                            break;
                        }
                        catch (Exception ex)
                        { }
                    }

                }

            }
        }

        static public void ClearStatusOfCommentPost(bool clear_all = false, string pc_devices = null)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                while (true)
                {

                    using (var sqlTrans = conn.BeginTransaction())
                    {
                        try
                        {
                            string command_query =
                                @"update comment_posts
                                set [status]='' where [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText(pc_devices) + @"'";

                            if (clear_all)
                            {
                                command_query =
                                @"update comment_posts
                                set [status]='' where [status]='Claimed'";
                            }

                            using (var command = new SqlCommand(command_query, conn, sqlTrans))
                            {
                                command.ExecuteNonQuery();
                            }
                            sqlTrans.Commit();
                            break;
                        }
                        catch (Exception ex)
                        { }
                    }

                }

            }
        }

        static public void ClearStatusOfCommentPost_ForScrapeUsers(string pc_devices=null)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                while (true)
                {

                    using (var sqlTrans = conn.BeginTransaction())
                    {
                        try
                        {
                            string command_query =
                                @"update scrapeuser_comment_posts
                                set [status]='' where [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText(pc_devices) + @"'";


                            using (var command = new SqlCommand(command_query, conn, sqlTrans))
                            {
                                command.ExecuteNonQuery();
                            }
                            sqlTrans.Commit();
                            break;
                        }
                        catch (Exception ex)
                        { }
                    }

                }

            }
        }

        static public void UpdateStatusIgToolKitUser(long pk, string col, System.Data.SqlDbType sqlDbType, string colvalue)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                PLAutoHelper.SQLHelper.Execute(() =>
                {
                    using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                    {
                        var command_query = @"update scrapeusers set [" + col + "]=@colvalue where [pk]=@pk";

                        using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                        {
                            command.Parameters.Add("@pk", System.Data.SqlDbType.Float).Value = pk;
                            command.Parameters.Add("@colvalue", sqlDbType).Value = colvalue;
                            command.ExecuteNonQuery();
                        }
                        sqlTransaction.Commit();
                    }
                }, "scrapeusers");


                //using (var command = new SqlCommand("igtoolkit_UpdateScrapeUser", conn))
                //{
                //    command.CommandType = System.Data.CommandType.StoredProcedure;

                //    command.Parameters.Add("@pk_val", System.Data.SqlDbType.Float).Value = pk;
                //    command.Parameters.Add("@colname", System.Data.SqlDbType.VarChar).Value = col;
                //    command.Parameters.Add("@colvalue_val", System.Data.SqlDbType.VarChar).Value = colvalue;

                //    command.ExecuteNonQuery();
                //}
            }
        }

        #endregion


        static public List<dynamic> GetQueueTask(string accountId)
        {
            List<dynamic> queuetasks = new List<dynamic>();
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("ig_GetQueueTask", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;

                    using (var dataReader = command.ExecuteReader())
                    {
                        while(dataReader.Read())
                        {
                            var taskid = dataReader["taskid"].ToString();
                            var action = dataReader["action"].ToString();
                            var value = dataReader["value"] == null ? "NULL" : dataReader["value"].ToString();

                            queuetasks.Add(new
                            {
                                taskid,
                                action,
                                value
                            });
                        }
                    }
                }
            }

            return queuetasks;
        }

        #region --NormalUserTask--

        static public string GetNormalUserTask(string accountId)
        {
            string taskname = null;

            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("igdb_GetNormalUserTask", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;

                    using (var dataReader = command.ExecuteReader())
                    {
                        if(dataReader.HasRows)
                        {
                            dataReader.Read();
                            taskname = dataReader[0].ToString();
                        }
                    }
                }
            }

            return taskname;
        }


        static public bool IsPublishedThisPost(string accountId, string postId)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("select count(*) from actionlog where [accountid]="+accountId+" and [action]='published_post' and [value]='"+postId+"'", conn))
                {
                    int count = int.Parse(command.ExecuteScalar().ToString());
                    if (count == 0)
                        return false;
                }
            }


            return true;
        }

        static public bool IsPublishedThisStory(string accountId, string storyID)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("select count(*) from actionlog where [accountid]=" + accountId + " and [action]='published_stories' and [value]='" + storyID + "'", conn))
                {
                    int count = int.Parse(command.ExecuteScalar().ToString());
                    if (count == 0)
                        return false;
                }
            }

            return true;
        }

        static public string GetSourceUser(string accountId)
        {
            string source_user = null;
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("select [source_username] from source_normaluser where ([source_status]!='error' or [source_status] is null) and [accountId]=" +accountId, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();
                            source_user = dataReader["source_username"].ToString();
                        }
                    }
                }
            }

            if (source_user == null)
            {
                throw new System.ArgumentException("Not found source user of " + accountId);
            }

            return source_user;
        }

        static public void UpdateSourceUser(string sourceuser, string status)
        {
            string source_user = null;
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("update source_normaluser set [source_status]='" + status + "' where [source_username]='" + sourceuser + "'", conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        static public void UpdateSourceUserByAccountID(string accountId, string status)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("update source_normaluser set [source_status]='" + status + "' where [accountId]='" + accountId + "'", conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        static public List<string> GetSourceUser_Error()
        {
            List<string> source_users = new List<string>();
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("select * from source_normaluser where [source_status]='error'", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                source_users.Add(dataReader["source_username"].ToString());
                            }
                        }
                    }
                }
            }

            return source_users;
        }

        static public bool IsPublishedPostInLastXMinutes(string accountId, int minutes = 10)
        {
            var command_query =
                @"SELECT count(*)
                FROM actionlog
                WHERE[action] = 'published_post'
                  AND datediff(MINUTE, do_at, getdate())< @minutes
                  AND[accountid] = @accountId";

            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                    command.Parameters.Add("@minutes", System.Data.SqlDbType.Int).Value = minutes;

                    var count = int.Parse(command.ExecuteScalar().ToString());

                    if (count > 0)
                        return true;
                }
            }

            return false;
        }



        #endregion


        #region --SemiAutoComment--

        static public dynamic GetSemiAutoCommentTask(string postId)
        {
            dynamic comment = null;
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("select * from semi_autocomment_task where [postId]='" + postId + "'", conn))
                {

                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();
                            var link = "https://www.instagram.com/p/" + dataReader["postId"].ToString() + "/";
                            var text = dataReader["text"].ToString();
                            var id = dataReader["id"].ToString();

                            comment = new
                            {
                                id,
                                link,
                                text
                            };
                        }
                    }
                }
            }

            return comment;
        }

        #endregion


        #region --Switch Device--

        static public dynamic GetDeviceInformation(string deviceName)
        {
            dynamic deviceinfo = null;
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("select * from ldplayers where [ld_name]='" + deviceName + "'", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        dataReader.Read();
                        deviceinfo = new
                        {
                            manufacturer = dataReader["manufacturer"].ToString(),
                            model = dataReader["model"].ToString(),
                            imei = dataReader["imei"].ToString(),
                            imsi = dataReader["imsi"].ToString(),
                            simserial = dataReader["simserial"].ToString(),
                            androidid = dataReader["androidid"].ToString(),
                            mac = dataReader["mac"] == null ? "" : dataReader["mac"].ToString(),
                            hard_serial = dataReader["hard_serial"],
                            pnumber = dataReader["pnumber"] == null ? "" : dataReader["pnumber"].ToString().Replace("+84", "").Replace(" ", "")
                        };
                    }
                }
            }

            return deviceinfo;
        }
        static public bool IsDeviceFree(string index)
        {
            bool isfree = false;
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("select * from ldplayers_switch where [pc_device]=" + Program.form.pc_devices + " and [ld_index]=" + index, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        dataReader.Read();
                        if (dataReader["status"].ToString() == "")
                            isfree = true;
                    }
                }

                if (isfree)
                {
                    using (var command = new SqlCommand("update ldplayers_switch set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"' where [pc_device]=" + Program.form.pc_devices + " and [ld_index]=" + index, conn))
                    {
                        command.ExecuteNonQuery();
                    }

                    return true;
                }
            }

            return false;
        }

        static public void SetSwitchDeviceStatus(string index, string status)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("update ldplayers_switch set [status]='" + status + "' where [pc_device]="+Program.form.pc_devices+" and [ld_index]=" + index, conn))
                {
                    command.ExecuteNonQuery();
                }

            }
        }

        static public void AddSwitchDevice(List<dynamic> switch_ldplayers)
        {
            //clear old devices
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("delete ldplayers_switch where pc_device=" + Program.form.pc_devices, conn))
                {
                    command.ExecuteNonQuery();
                }

                foreach (var device in switch_ldplayers)
                {
                    using (var command = new SqlCommand("insert into ldplayers_switch ([ld_index],[ld_name],[pc_device]) values (" + device.index + ",'" + device.name + "'," + Program.form.pc_devices + ")", conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }


            }

        }

        #endregion

        #region --Check Message--

        static public bool IsReallyNewMessage(string accountId, Bitmap current_bmp, string screen)
        {
            //screen: new: latest_message_screen -- request: latest_request_screen

            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select top 1 [value] from actionlog where [action]='" + screen + "' and [accountid]=" + accountId + " order by id desc", conn))
                {
                    using(var dataReader=command.ExecuteReader())
                    {
                        if(dataReader.HasRows)
                        {
                            dataReader.Read();

                            var base64 = dataReader[0].ToString();

                            var latest_screen_bmp = Action.ActionHelpers.BitmapFromBase64Str(base64);

                            var find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(latest_screen_bmp, current_bmp, 0.8);
                            if (find_point != null)
                                return false;
                        }
                    }
                }

            }

            //really news -> add to actionlog

            var base64_current = Action.ActionHelpers.SaveBitmapToBase64Str(current_bmp);

            IGHelpers.DBHelpers.AddActionLogByID(accountId, screen, base64_current);

            return true;
        }

        static public bool IsCheckRequestMessageInLatest8Hours(string accountId)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select count(*) from actionlog where [accountid]=" + accountId + " and [action]='check_request_message' and DATEDIFF(hour,do_at,getdate())<8", conn))
                {
                    var count = int.Parse(command.ExecuteScalar().ToString());

                    if (count > 0)
                        return true;
                }


            }

            return false;
        }

        static public void UpdateHasMessage(string accountId, string value, string message_base64)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("update account_info set [has_message]='" + value + "',[message_base64]='" + message_base64 + "' where [accountId]=" + accountId, conn))
                {
                    command.ExecuteNonQuery();
                }

            }
        }

        #endregion


        #region --Monitor all pc devices--

        static public void UpdateLastRunning()
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("update devices set [last_check_running]=getdate() where [device_id]=" + Program.form.pc_devices, conn))
                {
                    command.ExecuteNonQuery();
                }

            }
        }

        static public void ClearStatusOfAccountsClaimedByStopDevice()
        {
            //Clear status of account Claimed Manual By device_id in device that aren't running
            List<string> devices = new List<string>();
            //get list devices not running in last x minutes
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select device_id from devices where ([last_check_running] is not null and DATEDIFF(minute, last_check_running, getdate()) > 11)", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                devices.Add(dataReader["device_id"].ToString());
                            }
                        }
                    }
                }


                foreach (var item in devices)
                {
                    using (var command = new SqlCommand("update ig_account set [status]='' where [status] like '%By " + item + "%'", conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }

            }

        }


        #endregion

        #region --AIO Thread--

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="times">in minutes</param>
        static public void AddTimeToCurrent_TimeSpend(string accountId, double times)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("update account_info set [current_timespend]=[current_timespend]+@times where [accountId]=" + accountId, conn))
                {
                    command.Parameters.Add("@times", System.Data.SqlDbType.Float).Value = times;
                    command.ExecuteNonQuery();
                }

            }
        }

        static public int ErrorTimesOfThisRun(string accountId, string error_logname)
        {
            int error_times = 0;
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select count(*) from actionlog where [action]='"+error_logname+ "' and DATEDIFF(day,do_at,getdate())=0 and accountid="+accountId, conn))
                {
                    error_times = int.Parse(command.ExecuteScalar().ToString());
                }

            }

            return error_times;
        }


        #endregion


        #region --Form - TaskTab - Import--

        static public void AddTasks(List<dynamic> tasks, bool is_schedule = false)
        {
            var sql = "insert into queuetasks (accountid, [action], [value], [schedule_time]) values (@accountId,@action,@value,NULL)";

            if (is_schedule)
            {
                sql = "insert into queuetasks (accountid, [action], [value], [schedule_time]) values (@accountId, @action, @value, DATEADD(second,abs(CHECKSUM(NEWID())) % 3600,getdate()))"; //random in next 1 hour
            }

            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                foreach (var t in tasks)
                {

                    using (var command = new SqlCommand(sql, conn))
                    {
                        command.Parameters.Add("@accountid", System.Data.SqlDbType.Int).Value = t.accountId;
                        command.Parameters.Add("@action", System.Data.SqlDbType.VarChar).Value = t.taskname;
                        command.Parameters.Add("@value", System.Data.SqlDbType.VarChar).Value = ADBSupport.ADBHelpers.Base64Encode(t.value);

                        command.ExecuteNonQuery();
                    }
                }

            }
        }

        #endregion


        #region --AutoCommentTask--

        /// <summary>
        /// use when launch tool
        /// </summary>
        static public void ClearStatusOfSemiAutoTask(bool clear_all = false, string pc_devices = null)
        {
            if (pc_devices == null)
                pc_devices = Program.form.pc_devices;
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                string command_query = "update semi_autocomment_task set [status]='' where ([accountId] is not null and [accountId]!='') and [accountId] in (select [id] from ig_account where [pc_devices]=" + pc_devices + ") and ([posted] is null or [posted]='')";

                if (clear_all)
                {
                    command_query = "update semi_autocomment_task set [status]=''";
                }

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        static public dynamic GetAutoCommentTask(string accountId)
        {
            dynamic comment = null;
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("ig_GetAutoCommentTask", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;

                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();
                            var link = "https://www.instagram.com/p/" + dataReader["postId"].ToString() + "/";
                            var text = dataReader["text"].ToString();
                            var id = dataReader["id"].ToString();

                            comment = new
                            {
                                id,
                                link,
                                text
                            };
                        }
                    }
                }
            }

            return comment;
        }

        static public string GetAutoCommentText()
        {
            string text = "";
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("select top 1 * from data_resources where [name]='autocomment' order by newid()", conn))
                {

                    using (var dataReader = command.ExecuteReader())
                    {
                        
                            dataReader.Read();
                            text = dataReader["content"].ToString();
                    }
                }
            }

            return text;
        }

        #endregion


        #region --Add Posts--

        static public void AddPost(dynamic post)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("insert into ig_posts ([content],[image],[hashtag],[settings],[state]) values (@content, @image, @hashtag,@settings,@state)", conn))
                {

                    command.Parameters.Add("@content", System.Data.SqlDbType.NVarChar).Value = post.content;
                    command.Parameters.Add("@image", System.Data.SqlDbType.VarChar).Value = post.image_base64;
                    command.Parameters.Add("@hashtag", System.Data.SqlDbType.NVarChar).Value = post.hashtag;
                    command.Parameters.Add("@settings", System.Data.SqlDbType.VarChar).Value = post.settings;
                    command.Parameters.Add("@state", System.Data.SqlDbType.VarChar).Value = post.state;

                    command.ExecuteNonQuery();
                }
            }
        }


        #endregion


        #region -RealPhone Register--

        static public dynamic RealPhoneReg_GetAccountToReg()
        {
            dynamic account = null;

            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("ig_GetAccountRealPhoneReg", conn))
                {
                    using(var dataReader=command.ExecuteReader())
                    {
                        if(dataReader.HasRows)
                        {
                            dataReader.Read();
                            var id = dataReader["id"].ToString();
                            var fullname = dataReader["fullname"].ToString();
                            var user = dataReader["user"].ToString();

                            account = new
                            {
                                id,
                                fullname,
                                user
                            };
                        }
                    }
                }
            }

            return account;
        }

        static public void RealPhoneReg_AddFullname(List<string> fullnames)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                foreach(var u in fullnames)
                {
                    using (var command = new SqlCommand("insert into account_reg (fullname) values (@fullname)", conn))
                    {
                        command.Parameters.Add("@fullname", System.Data.SqlDbType.NVarChar).Value = u;
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        static public void RealPhoneReg_ClearStatus()
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("update account_reg set [status]=''", conn))
                {

                    command.ExecuteNonQuery();
                }
            }
        }

        static public void RealPhoneReg_Success_UpdateInformation(dynamic account)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("update account_reg set [user]=@user,[password]='123456789kdL',[phonenum]=@phone,[reg_date]=getdate(),[reg_status]='Success',[status]='' where [id]=@id", conn))
                {
                    command.Parameters.Add("@user", System.Data.SqlDbType.VarChar).Value = account.username;
                    command.Parameters.Add("@phone", System.Data.SqlDbType.VarChar).Value = account.phone;
                    command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = account.id;

                    command.ExecuteNonQuery();
                }
            }
        }


        #endregion


        #region --FFPCSwitch--

        static public List<string> FFPC_ProfilesToBackup()
        {
            List<string> profiles = new List<string>();

            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("select * from ig_account where [already_hasprofile]='yes'", conn))
                {
                    using(var dataReader=command.ExecuteReader())
                    {
                        while(dataReader.Read())
                        {
                            profiles.Add(dataReader["id"].ToString());
                        }
                    }
                }
            }

            return profiles;
        }

        static public bool IsNewestBackupTime(string accountId, DateTime newest_time)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("select top 1 * from actionlog where [accountid]="+accountId+" and [action]='backup_ffpc' order by id desc", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        if(dataReader.HasRows)
                        {
                            dataReader.Read();
                            var do_at = dataReader["do_at"].ToString();
                            if (newest_time.Subtract(DateTime.Parse(do_at)).TotalSeconds < 0)
                                return false;
                        }
                    }
                }
            }

            return true;
        }

        static public void SetAlready_HasProfile(string accountId)
        {
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("update ig_account set [already_hasprofile]='yes' where [id]=" + accountId, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }


        #endregion

        #region --Slave's Comments Helpers--

        static public dynamic GetSlaveAccountToScrape()
        {
            dynamic slave = null;

            Mutex m = new Mutex(false, "slave_account");
            m.WaitOne();
            using (SqlConnection conn = new SqlConnection(db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("ig_GetSlavesComment_Account", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.Add("@pc_devices", System.Data.SqlDbType.Int).Value = Program.form.pc_devices;

                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();

                            var accountId = dataReader["id"].ToString();
                            var user = dataReader["user"].ToString();

                            slave = new
                            {
                                user,
                                accountId
                            };
                        }
                    }
                }
            }



            m.ReleaseMutex();

            return slave;
        }



        #endregion

        #region --Helpers for quick launch testing--


        static public dynamic QuickLaunch_IGAccount(string deviceName)
        {
            dynamic device = null;
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("update ig_account set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"' where [phone_device]='" + deviceName + "' and ([status] is null or [status]='')", conn))
                {
                    var affected_row = command.ExecuteNonQuery();
                    if (affected_row == 0)
                    {
                        //device is using
                        return null;
                    }
                }

                using (var command = new SqlCommand("select * from ig_account join account_info on ig_account.id=account_info.accountId where [phone_device]='" + deviceName + "'", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        dataReader.Read();
                        {
                            string id = dataReader["id"].ToString();
                            string user = dataReader["user"].ToString();
                            string password = dataReader["password"].ToString();
                            string fullname = dataReader["fullname"].ToString();
                            string phone_device = dataReader["phone_device"].ToString();
                            string type = dataReader["type"] == null ? "" : dataReader["type"].ToString();
                            string mode = dataReader["mode"].ToString();
                            dynamic aio_settings = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(dataReader["aio_settings"].ToString());
                            device = new
                            {
                                id,
                                user,
                                password,
                                fullname,
                                phone_device,
                                type,
                                mode,
                                aio_settings
                            };
                        }
                    }
                }
            }

            return device;
        }


        #endregion


        #region --UnsualActivity--

        static public bool IsSolveUnsualActivityRecently(string accountId)
        {
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select top 1 * from actionlog where [action]='solved_unsualactivity' and [accountid]=" + accountId + " order by id desc", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();

                            //check if new_login

                            var value = dataReader["value"].ToString();
                            if (value == "new_login")
                                return true;
                            else return false;
                        }
                    }
                }
            }

            return true;
        }


        static public void UpdateValueSolvedActivityRecently(string accountId)
        {
            string logid = null;
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select top 1 * from actionlog where [action]='solved_unsualactivity' and [accountid]=" + accountId + " order by id desc", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        dataReader.Read();
                        logid = dataReader["id"].ToString();
                    }
                }

                using (var command = new SqlCommand("update actionlog set [value]='new_login' where [id]=" + logid, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }


        #endregion

        #region --ig_account_external--

        static public string IG_Account_External_ValueByAccountID(string accountId, string name)
        {
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select [" + name + "] from ig_account_external where [accountId]=" + accountId, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();
                            return dataReader[name].ToString();
                        }
                    }
                }
            }

            return null;
        }

        static public string ReplaceValueWithExtInfo(dynamic ig_account, string text)
        {
            var spin_content = SharpSpin.Spinner.Spin(text);

            spin_content = spin_content.Replace("@user", "@" + ig_account.user);

            try
            {
                var replace_link = ((string)IG_Account_External_ValueByAccountID(ig_account.id, "link")).Replace("http://", "");
                if (replace_link[replace_link.Length - 1] == '/')
                    replace_link = replace_link.Remove(replace_link.Length - 1);

                spin_content = spin_content.Replace("@link", replace_link);
            }
            catch { }

            return spin_content;

        }

        #endregion

        
    }
}
