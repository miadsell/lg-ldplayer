﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGHelpers
{
    static class ManageOLVHelpers
    {

        static public List<Class.IGAccountItem> LoadItems()
        {
            List<Class.IGAccountItem> accounts = new List<Class.IGAccountItem>();

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select * from ig_account where [pc_devices]=" + Program.form.pc_devices, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var id = dataReader["id"].ToString();
                            var user = dataReader["user"].ToString();
                            var niche = dataReader["niche"].ToString();
                            var fullname = dataReader["fullname"].ToString();
                            var note = dataReader["note"].ToString();
                            var state = dataReader["state"].ToString();
                            var status = dataReader["status"].ToString();

                            accounts.Add(new Class.IGAccountItem(id, niche, user, fullname, note, state, status));

                        }
                    }
                }
            }

            return accounts;
        }

        static public List<Class.IGAccountItem> LoadItems_AIO()
        {
            List<Class.IGAccountItem> accounts = new List<Class.IGAccountItem>();

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select * from ig_account", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var id = dataReader["id"].ToString();
                            var user = dataReader["user"].ToString();
                            var niche = dataReader["niche"].ToString();
                            var fullname = dataReader["fullname"].ToString();
                            var note = dataReader["note"].ToString();
                            var state = dataReader["state"].ToString();
                            var status = dataReader["status"].ToString();

                            accounts.Add(new Class.IGAccountItem(id, niche, user, fullname, note, state, status));

                        }
                    }
                }
            }

            return accounts;
        }


        static public List<dynamic> LoadItemsDynamic(string phone_device = null, string has_message_filter = "", string monitor_comment_filter = "", string monitor_message_filter = "")
        {
            List<dynamic> accounts = new List<dynamic>();

            //string filter = "";

            var default_command = "select * from ig_account join account_info on ig_account.id=account_info.accountId where [type]='slave'"; //where [pc_devices]=" + Program.form.pc_devices;

            //List<string> filters = new List<string>();

            //if(has_message_filter!="")

            //if (has_message_filter!="" || monitor_comment_filter!="" || monitor_message_filter!="")
            //{
            //    //(has_message like '%" + has_message_filter + "%' or monitor_comment like '%" + monitor_comment_filter + "%' or monitor_message like '%" + monitor_message_filter + "%') and
            //}



            if (phone_device != null)
            {
                default_command = "select * from ig_account join account_info on ig_account.id=account_info.accountId where [phone_device] like '%" + phone_device + "%'";
            }

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand(default_command, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var Id = dataReader["id"].ToString();
                            var User = dataReader["user"].ToString();
                            var Niche = dataReader["niche"].ToString();
                            //var Fullname = dataReader["fullname"].ToString();
                            var Note = dataReader["note"].ToString();
                            //var State = dataReader["state"].ToString();
                            var Status = dataReader["status"].ToString();
                            var PhoneDevice = dataReader["phone_device"].ToString();
                            var has_message = dataReader["has_message"].ToString();
                            var monitor_message = dataReader["monitor_message"].ToString();
                            var monitor_comment = dataReader["monitor_comment"].ToString();
                            var message_base64 = dataReader["message_base64"].ToString();
                            var already_hasprofile = dataReader["already_hasprofile"].ToString();

                            accounts.Add(new
                            {
                                Id,
                                Niche,
                                User,
                                //Fullname,
                                //Note,
                                PhoneDevice,
                                has_message,
                                Status,
                                monitor_comment,
                                monitor_message,
                                message_base64,
                                already_hasprofile
                                //State
                            });

                        }
                    }
                }
            }

            return accounts;
        }

        static public bool IsDeviceFree(string phone_device)
        {
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("update ig_account set [status]='" + AIOHelper.DBLogic.HandleClaimed.TakeClaimedText() + @"' output inserted.id where [status]='' and [phone_device]='" + phone_device + "'", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                            return true;
                    }
                }
            }
            return false;
        }

        static public void MakeDeviceFree(string phone_device)
        {
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("update ig_account set [status]='' where [phone_device]='" + phone_device + "'", conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        #region --Monitor Comment and Message--


        static public void UpdateClearMessage(string accountId)
        {
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("update account_info set [has_message]='', [message_base64]='' where [accountId]=" + accountId, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        static public void UpdateMonitorMessage(string accountId, string value)
        {
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("update account_info set [monitor_message]='" + value + "' where [accountId]=" + accountId, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        static public void UpdateMonitorComment(string accountId, string value)
        {
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("update account_info set [monitor_comment]='" + value + "' where [accountId]=" + accountId, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region --Manage Comments--

        static public List<dynamic> LoaddAllSlavesComment()
        {
            List<dynamic> slavesComments = new List<dynamic>();
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                using (var command = new SqlCommand("select * from slaves_comment", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var Id = dataReader["id"].ToString();
                            var AccountID = dataReader["accountId"].ToString();
                            var SlaveUser = dataReader["slave_user"].ToString();
                            var PostUrl = dataReader["post_url"].ToString();
                            var Comment_Text = dataReader["comment_text"].ToString();
                            var Comment_At = dataReader["comment_at"].ToString();
                            var Scraped_At = dataReader["scraped_at"].ToString();

                            slavesComments.Add(new { Id, AccountID, SlaveUser, PostUrl, Comment_Text, Comment_At, Scraped_At });
                        }
                    }
                }
            }

            return slavesComments;
        }

        #endregion
    }
}
