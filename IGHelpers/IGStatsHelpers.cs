﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGHelpers
{
    static class IGStatsHelpers
    {
        static public void FollowingTask()
        {
            List<dynamic> following_task_logs = new List<dynamic>();
            //get following_task actionlog

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("select * from actionlog where[action] = 'following_task' order by id desc", conn))
                {
                    using(var dataReader=command.ExecuteReader())
                    {
                        while(dataReader.Read())
                        {
                            if (dataReader["target_account"] == null)
                                continue;

                            var id = dataReader["id"].ToString();
                            var accountid = dataReader["accountid"].ToString();
                            var target_account = dataReader["target_account"].ToString();
                            var do_at = DateTime.Parse(dataReader["do_at"].ToString());

                            following_task_logs.Add(new
                            {
                                id,
                                accountid,
                                target_account,
                                do_at
                            });
                        }
                    }
                }
            }

            //distinc to get just accountid list

            var accountIds = following_task_logs.Select(c => c.accountid.ToString()).ToList().Distinct().ToList().OrderBy(c=>int.Parse(c));

            List<dynamic> stats = new List<dynamic>();

            foreach(var accountid in accountIds)
            {
                List<dynamic> accountid_stats = new List<dynamic>();
                for(int i=1;i<=20;i++) //get stats from this day to 20 days ago
                {
                    var date = DateTime.Now.Subtract(TimeSpan.FromDays(i - 1));
                    var following_count = following_task_logs.Where(f => DateTime.Now.Subtract((DateTime)f.do_at).TotalDays <= i && DateTime.Now.Subtract((DateTime)f.do_at).TotalDays > (i-1) && f.accountid == accountid).ToList().Count;

                    accountid_stats.Add(new
                    {
                        date = date.ToShortDateString(),
                        count = following_count
                    });
                }

                stats.Add(new
                {
                    accountId = accountid,
                    stats = accountid_stats
                });
            }

        }
    }
}
