﻿using InstagramLDPlayer.ADBSupport;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.IGHelpers
{
    static class ExifHelpers
    {

        static public string OptimizeImage(string deviceName, string imagepath)
        {

            string deviceImage_path = Path.GetFullPath(LDHelpers.LDPlayer_dnplayerdata) + deviceName + "\\";

            if (Path.GetExtension(imagepath) != ".jpg")
            {
                //save to .jpg

                Bitmap bmp = (Bitmap)System.Drawing.Image.FromFile(imagepath);

                imagepath = deviceImage_path + "temp_img\\" + Path.GetFileNameWithoutExtension(imagepath) + ".jpg";

                ADBSupport.ADBHelpers.CreatePath(Directory.GetParent(imagepath).FullName);

                bmp.Save(imagepath, System.Drawing.Imaging.ImageFormat.Jpeg);

                //throw new System.ArgumentException("Image must be in .jpg format");
            }


            //Check if default exif is generate, if not -> generate

            //string ftp_default_exif_path = ADBSupport.LDHelpers.ftp_dnplayerdata + deviceName + "/system/default_exif.txt";


            //if (!IGHelpers.Helpers.FTP_IsFileExisted(ftp_default_exif_path))
            //{
            //    var deviceinfo = IGHelpers.DBHelpers.GetDeviceByName(deviceName);
            //    string brand = deviceinfo.manufacturer;
            //    string model = deviceinfo.model;


            //    //generate default exif
            //    GenerateDefaultExifForDevice(brand, model, local_default_exif_path);

            //    //Push file to ftp_server
            //    IGHelpers.Helpers.FTP_UploadFile(local_default_exif_path, ftp_default_exif_path, false);
            //}
            //else
            //{
            //    IGHelpers.Helpers.FTP_DownloadFile(ftp_default_exif_path, local_default_exif_path);
            //}

            var local_default_exif_path = deviceImage_path + "system\\default_exif.txt";

            if(!File.Exists(local_default_exif_path))
            {
                var deviceinfo = IGHelpers.DBHelpers.GetDeviceByName(deviceName);
                string brand = deviceinfo.manufacturer;
                string model = deviceinfo.model;


                //generate default exif
                GenerateDefaultExifForDevice(brand, model, local_default_exif_path);
            }

            var temp_filename_crop = deviceImage_path + "temp_img\\"+ DateTime.Now.Ticks.ToString() + "__crop" + Path.GetExtension(imagepath);
            var temp_filenameresize = deviceImage_path + "temp_img\\" + DateTime.Now.Ticks.ToString() + "_resize" + Path.GetExtension(imagepath);

            //Random crop and resize
            var cropvalue = GenerateCropSize(imagepath);

            CropImage(imagepath, temp_filename_crop, cropvalue.focus_x, cropvalue.focus_y, cropvalue.width, cropvalue.height);

            ResizeImage(temp_filename_crop, temp_filenameresize, 700);

            var guid = Guid.NewGuid().GetHashCode().ToString();
            var des_path = deviceImage_path + "temp_img\\" + guid.Substring(guid.Length-6) + Path.GetExtension(imagepath);
            //Generate random exif
            ModifyExif(local_default_exif_path, temp_filenameresize, des_path);

            return des_path;
        }

        static public string OptimizeImage_FTPServer(string deviceName, string imagepath)
        {

            string ftp_devicePath = "/" + deviceName + "/";
            string temp_local_path = "Temp\\" + deviceName + "\\";
            ADBHelpers.CreatePath(temp_local_path);

            if (Path.GetExtension(imagepath) != ".jpg")
            {
                //save to .jpg

                Bitmap bmp = (Bitmap)System.Drawing.Image.FromFile(imagepath);


                imagepath = "Temp\\" + deviceName + "\\" + Path.GetFileNameWithoutExtension(imagepath) + ".jpg";

                ADBSupport.ADBHelpers.CreatePath(Directory.GetParent(imagepath).FullName);

                bmp.Save(imagepath, System.Drawing.Imaging.ImageFormat.Jpeg);

                //throw new System.ArgumentException("Image must be in .jpg format");
            }

            var ftpserver_default_exif_path = ftp_devicePath + "system/default_exif.txt";

            if (!PLAutoHelper.FTPHelper.FTP_IsFileExisted(LDHelpers.pLFTP, ftpserver_default_exif_path))
            {
                var deviceinfo = IGHelpers.DBHelpers.GetDeviceByName(deviceName);
                string brand = deviceinfo.manufacturer;
                string model = deviceinfo.model;


                //generate default exif
                GenerateDefaultExifForDevice_FTPServer(brand, model, ftpserver_default_exif_path);
            }

            var temp_filename_crop = temp_local_path + "temp_img\\" + DateTime.Now.Ticks.ToString() + "__crop" + Path.GetExtension(imagepath);
            var temp_filenameresize = temp_local_path + "temp_img\\" + DateTime.Now.Ticks.ToString() + "_resize" + Path.GetExtension(imagepath);

            //Random crop and resize
            var cropvalue = GenerateCropSize(imagepath);

            CropImage(imagepath, temp_filename_crop, cropvalue.focus_x, cropvalue.focus_y, cropvalue.width, cropvalue.height);

            ResizeImage(temp_filename_crop, temp_filenameresize, 700);

            var guid = Guid.NewGuid().GetHashCode().ToString();
            var des_path = temp_local_path + "temp_img\\" + guid.Substring(guid.Length - 6) + Path.GetExtension(imagepath);
            //Generate random exif
            ModifyExif_ExifToolCMD_FTPServer(ftpserver_default_exif_path, temp_filenameresize, des_path);

            return des_path;
        }

        #region --Exif--

        static private void ModifyExif(string default_exif_path, string filepath, string des_path)
        {
            //filepath = @"F:\DOWNLOAD\vaytrang9.jpg";
            // download_des = @"F:\DOWNLOAD\";

            RestClient client = new RestClient("https://www.thexifer.net/");

            CookieContainer _cookieJar = new CookieContainer();
            client.CookieContainer = _cookieJar;

            client.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0";

            var request_exif = new RestRequest("", Method.GET);

            request_exif.AddHeader("Host", "www.thexifer.net");
            request_exif.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0");
            request_exif.AddHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            request_exif.AddHeader("Accept-Language", "en-US,en;q=0.5");
            //request_exif.AddHeader("Accept-Encoding", "gzip, deflate, br");
            request_exif.AddHeader("Connection", "keep-alive");
            request_exif.AddHeader("Upgrade-Insecure-Requests", "1");
            request_exif.AddHeader("Content-Type", "application/x-www-form-urlencoded");

            var query_exif = client.Execute(request_exif);

            //get form upload action

            var htmlDoc_load = new HtmlAgilityPack.HtmlDocument();
            htmlDoc_load.LoadHtml(query_exif.Content);

            var form_upload = htmlDoc_load.DocumentNode.SelectSingleNode("//form[contains(@action,'upload.php?folder=uploads/')]").Attributes["action"].Value;


            //Upload image

            var request_upload = new RestRequest(form_upload, Method.POST);

            request_upload.AddHeader("Host", "www.thexifer.net");
            request_upload.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0");
            request_upload.AddHeader("Accept", "application/json");
            request_upload.AddHeader("Accept-Language", "en-US,en;q=0.5");
            request_upload.AddHeader("Accept-Encoding", "gzip, deflate, br");
            request_upload.AddHeader("Cache-Control", "no-cache");
            request_upload.AddHeader("X-Requested-With", "XMLHttpRequest");
            request_upload.AddHeader("Origin", "https://www.thexifer.net");
            request_upload.AddHeader("Connection", "keep-alive");
            request_upload.AddHeader("Referer", "https://www.thexifer.net/");

            request_upload.AddFile("file", filepath, "image/jpeg");

            var query_upload = client.Execute(request_upload);


            //Write exif

            var request_write_exif = new RestRequest("exif.php", Method.POST);

            request_write_exif.AddHeader("Host", "www.thexifer.net");
            request_write_exif.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0");
            request_write_exif.AddHeader("Accept", "*/*");
            request_write_exif.AddHeader("Accept-Language", "en-US,en;q=0.5");
            request_write_exif.AddHeader("Accept-Encoding", "gzip, deflate, br");
            request_write_exif.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request_write_exif.AddHeader("X-Requested-With", "XMLHttpRequest");
            request_write_exif.AddHeader("Origin", "https://www.thexifer.net");
            request_write_exif.AddHeader("Connection", "keep-alive");
            request_write_exif.AddHeader("Referer", "https://www.thexifer.net/");

            List<dynamic> exif_generator = new List<dynamic>();

            //GENERATE RANDOM DEFAULT EXIF


            exif_generator = GenerateRandomExif(default_exif_path);

            foreach (var item in exif_generator)
            {
                request_write_exif.AddParameter(item.key, item.value);
            }

            request_write_exif.AddParameter("targetfiles[]", form_upload.Replace("upload.php?folder=", "") + "/" + Path.GetFileName(filepath));

            var query_write_exif = client.Execute(request_write_exif);

            //Download image after write exif

            var request_exif_download = new RestRequest(form_upload.Replace("upload.php?folder=", "") + "/" + Path.GetFileName(filepath), Method.GET);

            request_exif_download.AddHeader("Host", "www.thexifer.net");
            request_exif_download.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0");
            request_exif_download.AddHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            request_exif_download.AddHeader("Accept-Language", "en-US,en;q=0.5");
            request_exif_download.AddHeader("Accept-Encoding", "gzip, deflate, br");
            request_exif_download.AddHeader("Connection", "keep-alive");
            request_exif_download.AddHeader("Upgrade-Insecure-Requests", "1");
            request_exif_download.AddHeader("Content-Type", "application/x-www-form-urlencoded");


            var query_exif_download = client.ExecuteAsync(request_exif_download);

            var fileBytes = query_exif_download.Result.RawBytes;

            File.WriteAllBytes(des_path, fileBytes);
        }

        static private void ModifyExif_ByThexifer_FTPServer(string default_exif_path, string filepath, string des_path)
        {
            //filepath = @"F:\DOWNLOAD\vaytrang9.jpg";
            // download_des = @"F:\DOWNLOAD\";

            RestClient client = new RestClient("https://www.thexifer.net/");

            CookieContainer _cookieJar = new CookieContainer();
            client.CookieContainer = _cookieJar;

            client.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0";

            var request_exif = new RestRequest("", Method.GET);

            request_exif.AddHeader("Host", "www.thexifer.net");
            request_exif.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0");
            request_exif.AddHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            request_exif.AddHeader("Accept-Language", "en-US,en;q=0.5");
            //request_exif.AddHeader("Accept-Encoding", "gzip, deflate, br");
            request_exif.AddHeader("Connection", "keep-alive");
            request_exif.AddHeader("Upgrade-Insecure-Requests", "1");
            request_exif.AddHeader("Content-Type", "application/x-www-form-urlencoded");

            var query_exif = client.Execute(request_exif);

            //get form upload action

            var htmlDoc_load = new HtmlAgilityPack.HtmlDocument();
            htmlDoc_load.LoadHtml(query_exif.Content);

            var form_upload = htmlDoc_load.DocumentNode.SelectSingleNode("//form[contains(@action,'upload.php?folder=uploads/')]").Attributes["action"].Value;


            //Upload image

            var request_upload = new RestRequest(form_upload, Method.POST);

            request_upload.AddHeader("Host", "www.thexifer.net");
            request_upload.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0");
            request_upload.AddHeader("Accept", "application/json");
            request_upload.AddHeader("Accept-Language", "en-US,en;q=0.5");
            request_upload.AddHeader("Accept-Encoding", "gzip, deflate, br");
            request_upload.AddHeader("Cache-Control", "no-cache");
            request_upload.AddHeader("X-Requested-With", "XMLHttpRequest");
            request_upload.AddHeader("Origin", "https://www.thexifer.net");
            request_upload.AddHeader("Connection", "keep-alive");
            request_upload.AddHeader("Referer", "https://www.thexifer.net/");

            request_upload.AddFile("file", filepath, "image/jpeg");

            var query_upload = client.Execute(request_upload);


            //Write exif

            var request_write_exif = new RestRequest("exif.php", Method.POST);

            request_write_exif.AddHeader("Host", "www.thexifer.net");
            request_write_exif.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0");
            request_write_exif.AddHeader("Accept", "*/*");
            request_write_exif.AddHeader("Accept-Language", "en-US,en;q=0.5");
            request_write_exif.AddHeader("Accept-Encoding", "gzip, deflate, br");
            request_write_exif.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request_write_exif.AddHeader("X-Requested-With", "XMLHttpRequest");
            request_write_exif.AddHeader("Origin", "https://www.thexifer.net");
            request_write_exif.AddHeader("Connection", "keep-alive");
            request_write_exif.AddHeader("Referer", "https://www.thexifer.net/");

            List<dynamic> exif_generator = new List<dynamic>();

            //GENERATE RANDOM DEFAULT EXIF


            exif_generator = GenerateRandomExif_FTPServer(default_exif_path);

            foreach (var item in exif_generator)
            {
                request_write_exif.AddParameter(item.key, item.value);
            }

            request_write_exif.AddParameter("targetfiles[]", form_upload.Replace("upload.php?folder=", "") + "/" + Path.GetFileName(filepath));

            var query_write_exif = client.Execute(request_write_exif);

            //Download image after write exif

            var request_exif_download = new RestRequest(form_upload.Replace("upload.php?folder=", "") + "/" + Path.GetFileName(filepath), Method.GET);

            request_exif_download.AddHeader("Host", "www.thexifer.net");
            request_exif_download.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0");
            request_exif_download.AddHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            request_exif_download.AddHeader("Accept-Language", "en-US,en;q=0.5");
            request_exif_download.AddHeader("Accept-Encoding", "gzip, deflate, br");
            request_exif_download.AddHeader("Connection", "keep-alive");
            request_exif_download.AddHeader("Upgrade-Insecure-Requests", "1");
            request_exif_download.AddHeader("Content-Type", "application/x-www-form-urlencoded");


            var query_exif_download = client.ExecuteAsync(request_exif_download);

            var fileBytes = query_exif_download.Result.RawBytes;

            File.WriteAllBytes(des_path, fileBytes);
        }


        static private void ModifyExif_ExifToolCMD_FTPServer(string default_exif_path, string filepath, string des_path)
        {

            //check if exiftool is install -> if not, download and set environment path

            var command = ADBHelpers.ExecuteCMDTask("exiftool", 10);

            if(!command.Contains("exiftool - Read and write meta information in files"))
            {
                throw new System.ArgumentException("Not install exiftool!");
            }

            List<dynamic> exif_generator = new List<dynamic>();

            //GENERATE RANDOM DEFAULT EXIF


            exif_generator = GenerateRandomExif_FTPServer(default_exif_path);

            var EXIFMake = exif_generator.Where(e => e.key == "EXIFMake").First().value;
            var EXIFModel = exif_generator.Where(e => e.key == "EXIFModel").First().value;
            var EXIFSoftware = exif_generator.Where(e => e.key == "EXIFSoftware").First().value;

            var EXIFDateTimeOriginal = exif_generator.Where(e => e.key == "EXIFDateTimeOriginal").First().value;

            var EXIFGPSLatitude = exif_generator.Where(e => e.key == "EXIFGPSLatitude").First().value;
            var EXIFGPSLongitude = exif_generator.Where(e => e.key == "EXIFGPSLongitude").First().value;

            var cmd = "exiftool -model=\"@model\" -make=\"@make\" -software=\"@software\" -AllDates=\"@AllDates\" -GPSLongitudeRef=E -GPSLongitude=@GPSLongitude -GPSLatitudeRef=N -GPSLatitude=@GPSLatitude -o \"@des_path\" \"@filepath\"";

            cmd = cmd.Replace("@model", EXIFModel);
            cmd = cmd.Replace("@make", EXIFMake);
            cmd = cmd.Replace("@software", EXIFSoftware);
            cmd = cmd.Replace("@AllDates", EXIFDateTimeOriginal);
            cmd = cmd.Replace("@GPSLongitude", EXIFGPSLongitude);
            cmd = cmd.Replace("@GPSLatitude", EXIFGPSLatitude);
            cmd = cmd.Replace("@filepath", Path.GetFullPath(filepath));
            cmd = cmd.Replace("@des_path", Path.GetFullPath(des_path));

            command = ADBHelpers.ExecuteCMDTask(cmd, 10);

            //check if file exists

            Thread.Sleep(2000);

            if (!File.Exists(des_path))
                throw new System.ArgumentException("Modify exif: FAILED");
        }

        static public double lat_to = 10.898716;
        static public double lat_from = 10.888939;
        static public double long_to = 106.730749;
        static public double long_from = 106.719766;

        static private void GenerateDefaultExifForDevice(string brand, string model, string default_path)
        {
            List<dynamic> exif_dynamics = new List<dynamic>();

            exif_dynamics.Add(new
            {
                key = "EXIFMake",
                value = brand
            });

            exif_dynamics.Add(new
            {
                key = "EXIFModel",
                value = model
            });

            var software = model.Remove(0, 3) + MakeExifSoftware(8);

            exif_dynamics.Add(new
            {
                key = "EXIFSoftware",
                value = software
            });

            var lat_point = ((new Random()).NextDouble() * (lat_to - lat_from) + lat_from).ToString("F6");
            var long_point = ((new Random()).NextDouble() * (long_to - long_from) + long_from).ToString("F6");

            exif_dynamics.Add(new
            {
                key = "EXIFGPSLatitude",
                value = lat_point
            });

            exif_dynamics.Add(new
            {
                key = "EXIFGPSLongitude",
                value = long_point
            });

            var exif_lists = "";

            foreach (var item in exif_dynamics)
            {
                exif_lists += item.key + "|" + item.value + "\r\n";
            }

            File.WriteAllText(default_path, exif_lists);
        }

        static private void GenerateDefaultExifForDevice_FTPServer(string brand, string model, string default_path)
        {
            List<dynamic> exif_dynamics = new List<dynamic>();

            exif_dynamics.Add(new
            {
                key = "EXIFMake",
                value = brand
            });

            exif_dynamics.Add(new
            {
                key = "EXIFModel",
                value = model
            });

            var software = model.Remove(0, 3) + MakeExifSoftware(8);

            exif_dynamics.Add(new
            {
                key = "EXIFSoftware",
                value = software
            });

            var lat_point = ((new Random()).NextDouble() * (lat_to - lat_from) + lat_from).ToString("F6");
            var long_point = ((new Random()).NextDouble() * (long_to - long_from) + long_from).ToString("F6");

            exif_dynamics.Add(new
            {
                key = "EXIFGPSLatitude",
                value = lat_point
            });

            exif_dynamics.Add(new
            {
                key = "EXIFGPSLongitude",
                value = long_point
            });

            var exif_lists = "";

            foreach (var item in exif_dynamics)
            {
                exif_lists += item.key + "|" + item.value + "\r\n";
            }

            PLAutoHelper.FTPHelper.FTP_WriteTextToFile(LDHelpers.pLFTP, exif_lists, default_path);
        }

        static private string MakeExifSoftware(int length)
        {
            var result = "";
            var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var charactersLength = characters.Length;
            for (var i = 0; i < length; i++)
            {
                result += characters[(new Random()).Next(characters.Length)];
                Thread.Sleep(200);
            }
            return result;
        }

        static private List<dynamic> GenerateRandomExif(string device_exif_path)
        {
            List<dynamic> exif_generator = new List<dynamic>();

            string exifnumber = "";

            string created_date = GetRandomDate();

            //Read default value of device

            var default_exif = File.ReadAllLines(device_exif_path);

            foreach (var item in default_exif)
            {
                string key = item.Split('|')[0];
                string value = item.Split('|')[1];

                exif_generator.Add(new
                {
                    key,
                    value
                });
            }

            var exif_file = File.ReadAllLines("exif_template.txt");

            foreach (var item in exif_file)
            {
                string key = item.Split('|')[0];
                List<string> value_list = new List<string>();

                for (int i = 1; i < item.Split('|').Length; i++)
                {
                    value_list.Add(item.Split('|')[i]);
                }

                string value = "";

                if (value_list.Count > 0)
                {
                    value = value_list[(new Random()).Next(value_list.Count)];
                }


                if (key == "EXIFFNumber")
                    exifnumber = value;

                if (key == "EXIFApertureValue" || key == "EXIFMaxApertureValue")
                    value = exifnumber;
                if (key == "EXIFCreateDate" || key == "EXIFDateTimeOriginal" || key == "EXIFModifyDate")
                    value = created_date;

                exif_generator.Add(new
                {
                    key,
                    value
                });
            }

            return exif_generator;
        }

        static private List<dynamic> GenerateRandomExif_FTPServer(string remote_device_exif_path)
        {
            List<dynamic> exif_generator = new List<dynamic>();

            string exifnumber = "";

            string created_date = GetRandomDate();

            //Read default value of device


            //download device_exif_path

            var device_exif_path = "Temp\\" + PLAutoHelper.NormalFuncHelper.RandomGUID(5) + "_" + Path.GetFileName(remote_device_exif_path);

            PLAutoHelper.FTPHelper.FTP_DownloadFile(LDHelpers.pLFTP, remote_device_exif_path, device_exif_path);

            var default_exif = File.ReadAllLines(device_exif_path);


            foreach (var item in default_exif)
            {
                string key = item.Split('|')[0];
                string value = item.Split('|')[1];

                exif_generator.Add(new
                {
                    key,
                    value
                });
            }

            var exif_file = File.ReadAllLines("exif_template.txt");

            foreach (var item in exif_file)
            {
                string key = item.Split('|')[0];
                List<string> value_list = new List<string>();

                for (int i = 1; i < item.Split('|').Length; i++)
                {
                    value_list.Add(item.Split('|')[i]);
                }

                string value = "";

                if (value_list.Count > 0)
                {
                    value = value_list[(new Random()).Next(value_list.Count)];
                }


                if (key == "EXIFFNumber")
                    exifnumber = value;

                if (key == "EXIFApertureValue" || key == "EXIFMaxApertureValue")
                    value = exifnumber;
                if (key == "EXIFCreateDate" || key == "EXIFDateTimeOriginal" || key == "EXIFModifyDate")
                    value = created_date;

                exif_generator.Add(new
                {
                    key,
                    value
                });
            }

            return exif_generator;
        }

        static private string GetRandomDate()
        {
            Random rnd = new Random();
            var to = DateTime.Now;
            var from = to.Subtract(TimeSpan.FromDays(5));

            var range = to - from;

            var randTimeSpan = new TimeSpan((long)(rnd.NextDouble() * range.Ticks));

            var return_Date = from + randTimeSpan;

            var random_hour = (new Random()).Next(7, 22);

            var d = new DateTime(return_Date.Year, return_Date.Month, return_Date.Day, random_hour, return_Date.Minute, return_Date.Second);

            return d.ToString("yyyy:MM:dd HH:mm:ss");
        }

        #endregion


        #region --Crop, Resize--

        static private dynamic GenerateCropSize(string imagepath)
        {
            var focus_x = (new Random()).Next(50);
            var focus_y = (new Random()).Next(50);

            Bitmap source = new Bitmap(imagepath);

            int img_width = source.Width;
            int img_height = source.Height;

            var width = img_width - focus_x - (new Random()).Next(50);
            var height = img_height - focus_y - (new Random()).Next(50);

            return new
            {
                focus_x,
                focus_y,
                width,
                height
            };
        }

        static private void CropImage(string imagepath, string des_path, int focus_x, int focus_y, int width, int height)
        {
            Bitmap source = new Bitmap(imagepath);

            int img_width = source.Width;
            int img_height = source.Height;
            //Focus meant no cut focus

            int crop_x = (new Random()).Next(focus_x);
            int crop_y = (new Random()).Next(focus_y);

            int crop_width = (focus_x + width) + (new Random(Guid.NewGuid().GetHashCode())).Next(img_width - focus_x - width) - crop_x;
            int crop_height = (focus_y + height) + (new Random(Guid.NewGuid().GetHashCode())).Next(img_height - focus_y - height) - crop_y;

            Bitmap CroppedImage = source.Clone(new System.Drawing.Rectangle(crop_x, crop_y, crop_width, crop_height), source.PixelFormat);

            ADBSupport.ADBHelpers.CreatePath(Directory.GetParent(des_path).FullName);

            CroppedImage.Save(des_path, System.Drawing.Imaging.ImageFormat.Jpeg);

        }

        static public void ResizeImage(string imageFile, string outputFile, int target_width)
        {

            using (var srcImage = Image.FromFile(imageFile))
            {
                double scaleFactor = ((double)target_width) / srcImage.Width;
                var newWidth = (int)(srcImage.Width * scaleFactor);
                var newHeight = (int)(srcImage.Height * scaleFactor);
                using (var newImage = new Bitmap(newWidth, newHeight))
                using (var graphics = Graphics.FromImage(newImage))
                {
                    graphics.SmoothingMode = SmoothingMode.AntiAlias;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    graphics.DrawImage(srcImage, new Rectangle(0, 0, newWidth, newHeight));
                    newImage.Save(outputFile, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
            }
        }

        #endregion
    }
}
