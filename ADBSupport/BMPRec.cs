﻿using InstagramLDPlayer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.ADBSupport
{
    static class BMPRec
    {
        static public ClickRange IconApp = new ClickRange(16, 16);
        static public ClickRange LikeCommentBtn = new ClickRange(14, 15);
        static public ClickRange LikeButton = new ClickRange(14, 15);
        static public ClickRange MoreCaption = new ClickRange(21, 7);
        static public ClickRange Suggested_Follow = new ClickRange(46, 12);
        static public ClickRange StoryElement = new ClickRange(10, 10);
        static public ClickRange Login_LoginBtn = new ClickRange(27, 8);
        static public ClickRange Login_PhoneEmailUser = new ClickRange(167, 8);
        static public ClickRange Login_Password = new ClickRange(50, 8);
        static public ClickRange SelfComment_CommentBtn = new ClickRange(11, 9);
        static public ClickRange SelfComment_PostComment = new ClickRange(18, 5);
        static public ClickRange ReadNotice_HeartBtn = new ClickRange(16, 12);


        static public ClickRange SemiAutoComment_CommentBtn = new ClickRange(11, 9);
        static public ClickRange SemiAutoComment_PostComment = new ClickRange(18, 5);



        static public ClickRange LaunchManual_PhoneNumber = new ClickRange(308, 27);
        static public ClickRange LaunchManual_Password = new ClickRange(308, 27);
        static public ClickRange LaunchManual_LoginBtn = new ClickRange(204, 27);

        static public ClickRange WriteACaption = new ClickRange(103, 16);


        static public ClickRange Message_HasRequestMessage = new ClickRange(37, 9);


        static public ClickRange Seeding_LikeBtn = new ClickRange(8, 6);
        static public ClickRange Seeding_ViewComment = new ClickRange(35, 7);


        static public ClickRange UpdateFullname_UpdateProfileTick = new ClickRange(13, 11);

        static public ClickRange SelfStories_ShareStoriesBtn = new ClickRange(12, 9);
        static public ClickRange SelfStories_AddPostToYourStory = new ClickRange(103, 5);


    }
}
