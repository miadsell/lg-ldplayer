﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.ADBSupport
{
    static class TitaniumBackupHelpers
    {
        /// <summary>
        /// backup to folder data in local
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="remotepath"></param>
        /// <param name="username"></param>
        static public void BackUpIG(string deviceID, string backup_path, string username, bool check_correctaccount = true)
        {
            //Check if IsCorrectAccount => if not throw exception
            if (check_correctaccount)
            {
                if (!IGThreads.ThreadHelpers.IsCorrectAccount(deviceID, username))
                {
                    throw new System.ArgumentException("Can't Backup Because Account Isn't CORRECT");
                }
            }

            ADBHelpers.CreatePath(backup_path);

            //Clear old titanium backup
            var command = ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell \"su -c 'rm -r sdcard/TitaniumBackup/*'\"", 20);

            //Close app

            ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell pm disable com.instagram.android");

            //Re-enable

            ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell pm enable com.instagram.android");

            LaunchTitaniumBackup(deviceID);

            //Hit backup and restore
            try

            {
                ADBHelpers.ClickByImage(deviceID, BMPSource.Backup_Restore);
            }
            catch { }

            //Wait instagram

            ADBHelpers.WaitScreenByImage(deviceID, BMPSource.InstagramBackup, 3, 5, 3, 0.7);


            //Click Instagram

            ADBHelpers.ClickandWait(deviceID, BMPSource.InstagramBackup, BMPSource.WaitPopUp, 1);

            //Click backup

            ADBHelpers.ClickByImage(deviceID, BMPSource.BackupTitanium);

            Thread.Sleep(5000);

            //Wait backing up finish

            while (true)
            {
                try
                {
                    ADBHelpers.WaitScreenByImage(deviceID, BMPSource.WaitBackingUp, 1, 0, 0);
                    Thread.Sleep(4000);
                }
                catch
                {
                    break;
                }
            }

            //var temp_path = AppDomain.CurrentDomain.BaseDirectory + "Temp\\" + Guid.NewGuid().GetHashCode().ToString();
            //ADBHelpers.CreatePath(temp_path);


            //Pull to local
            ADBHelpers.ExecuteCMDTask("adb -s \"" + deviceID + "\" pull /sdcard/TitaniumBackup \"" + backup_path + "\"", 50);

            //Upload to remote
            //foreach (var item in Directory.GetFiles(temp_path))
            //{
            //    IGHelpers.Helpers.FTP_UploadFile(item, remotepath);
            //}

            //IGHelpers.Helpers.FTP_MultifileUpload(Directory.GetFiles(temp_path).ToList(), remotepath);
        }

        static Semaphore semaphore_restore_ig = new Semaphore(5, 5);

        static public void RestoreIG(string deviceID, string datafolder, bool remove_folder_after = true)
        {
            if (datafolder == null)
                return;

            if (datafolder.EndsWith("\\"))
            {
                datafolder = datafolder.Remove(datafolder.Length - 1, 1);
            }

            if (!System.IO.Path.IsPathRooted(datafolder))
            {
                datafolder = AppDomain.CurrentDomain.BaseDirectory + datafolder;
            }

            //Clear app data
            ADBSupport.ADBHelpers.ClearAPPData(deviceID, "com.instagram.android");

            //Clear titanium backup
            var command = ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell \"su -c 'rm -r sdcard/TitaniumBackup/*'\"", 20);

            semaphore_restore_ig.WaitOne();

            try
            {
                //Pull to local
                command = ADBHelpers.ExecuteCMDTask("adb -s \"" + deviceID + "\" push \"" + datafolder + "\" /sdcard/TitaniumBackup", 60);
            }
            finally
            {
                semaphore_restore_ig.Release();
            }

            LaunchTitaniumBackup(deviceID);

            //Close ig

            //Close app

            ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell pm disable com.instagram.android");

            //Re-enable

            ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell pm enable com.instagram.android");

            //Hit backup and restore

            try
            {
                ADBHelpers.ClickByImage(deviceID, BMPSource.Backup_Restore);
            }
            catch { }

            ADBHelpers.WaitScreenByImage(deviceID, BMPSource.InstagramBackup, 3, 5, 3);

            //ADBHelpers.ClickandWait(deviceID, BMPSource.Backup_Restore, BMPSource.InstagramBackup, 3);


            //Click Instagram

            ADBHelpers.ClickandWait(deviceID, BMPSource.InstagramBackup, BMPSource.WaitPopUp, 1);

            //Click restore

            ADBHelpers.ClickandWait(deviceID, BMPSource.ExecRestore, BMPSource.DataOnly, 1);

            //Click cancel update

            ADBHelpers.ClickandWait(deviceID, BMPSource.DataOnly, BMPSource.FinishRestore, 6);

            if (remove_folder_after)
            {
                Directory.Delete(datafolder, true);
            }
        }

        static private void LaunchTitaniumBackup(string deviceID)
        {
            ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell monkey -p com.keramidas.TitaniumBackup -c android.intent.category.LAUNCHER 1");

            try
            {
                //Wait TitaniumBackupAddon ask

                ADBHelpers.WaitScreenByImage(deviceID, BMPSource.TitaniumBackupAddOn, 2, 5, 3);

                //Click cancel
                ADBHelpers.ClickandWait(deviceID, BMPSource.CancelAddOn, BMPSource.Backup_Restore, 1);
            }
            catch
            { }


            try
            {
                //Wait ask install Addon

                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.First_TitaniumAddOn, 1, 0, 0);
                KAutoHelper.ADBHelper.TapByPercent(deviceID, 65.5, 95.0);
            }
            catch { }

            //Keep newid
            try
            {
                //Wait ask install Addon

                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Titanium_IgnoreKeepNewId, 1, 1, 1);

                ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.Titanium_Ok_IgnoreKeepNewId);
            }
            catch { }
        }
    }
}
