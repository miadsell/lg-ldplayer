﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.ADBSupport
{
    static class IGScreen
    {
        static public List<dynamic> screens = new List<dynamic>();

        #region --Screen--

        //static public Bitmap ProfileScreen = (Bitmap)Bitmap.FromFile(@"ImageSource\Global\Goto_EditProfileScreen.png");
        //static public Bitmap EditProfileScreen = (Bitmap)Bitmap.FromFile(@"ImageSource\Global\WaitEditProfileScreen.png");
        //static public Bitmap HomeScreen = (Bitmap)Bitmap.FromFile(@"ImageSource\Global\WaitHome.png");

        #endregion

        #region --Click This Image To Go To Target Screen--

        //static public void Goto_HomeScreen(Class.IGDevice iGDevice)
        //{
        //    ADBHelpers.GoToIntentView(iGDevice.deviceID, "android.intent.action.VIEW", "https://ig.me", "com.instagram.android");
        //    ADBHelpers.WaitScreenByImage(iGDevice.deviceID, HomeScreen);
        //}

        //static public void Goto_ProfileScreen(Class.IGDevice iGDevice)
        //{
        //    ADBSupport.ADBHelpers.ClickandWait(iGDevice.deviceID, ADBSupport.BMPSource.Global_GoTo_ProfileScreen, ProfileScreen);
        //}

        //static public void Goto_EditProfileScreen(Class.IGDevice iGDevice)
        //{
        //    ADBSupport.ADBHelpers.ClickandWait(iGDevice.deviceID, ADBSupport.BMPSource.Global_Goto_EditProfileScreen, EditProfileScreen);
        //}

        //static public void Goto_UpdateBioScreen(Class.IGDevice iGDevice)
        //{

        //}

        #endregion

        static public void LoadScreen()
        {
            var fields = typeof(IGScreen).GetFields();

            foreach (var field in fields)
            {
                if (field.FieldType.Name == "Bitmap")
                {
                    screens.Add(new
                    {
                        screen_name = field.Name,
                        bitmap_screen = (Bitmap)field.GetValue(null)
                    });
                }
            }
        }

        static public void GoToScreen(Class.IGDevice iGDevice, List<string> screens)
        {
            var current_screen = ADBSupport.IGScreen.CurrentScreen(iGDevice);

            int start_from_screen = 0;

            //if(screens.Contains(current_screen))
            //{

            //}

            if (start_from_screen == 0)
            {
                //Open homepage ig app
            }

            //Navigate screen

            for (int s = 1; s < screens.Count; s++)
            {
                typeof(ADBSupport.IGScreen).GetMethods();
            }
        }

        static public string CurrentScreen(Class.IGDevice iGDevice)
        {
            var current_screen= ADBSupport.ADBHelpers.ScreenShoot(iGDevice.deviceID);

            foreach (var item in screens)
            {
                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(iGDevice.deviceID, item.bitmap_screen, 1, 0, 0);
                    return item.screen_name;
                }
                catch
                {

                }
            }

            return null;
        }
    }
}
