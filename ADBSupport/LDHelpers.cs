﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.ADBSupport
{
    static class LDHelpers
    {
        #region - LD Constants--
        static public string LDPlayer_path = @"C:\ChangZhi\LDPlayer";
        static private string LDPlayer_Origin_name = "LDPlayer-ORIGIN";

        /// <summary>
        /// backup device info and instagram apk and data
        /// </summary>
        //static public string LDPlayerDATA_path = AppDomain.CurrentDomain.BaseDirectory + "LDData";
        static public string ftp_server_host = "192.168.1.7";
        static public string ftp_username;
        static public string ftp_password;
        static public NetworkCredential ftp_credential;
        static public string ftp_dnplayerdata = "";
        static public PLAutoHelper.FTPHelper.PLFTP pLFTP;

        static public int LDPlayerThread_Count = 5;
        static public int LDPlayer_FocusEngage_Threads_Count = 0;
        static public int LDPlayer_FocusMassAction_Threads_Count = 0;
        static public int LDPlayer_sleep_between_launch = 0;
        static public string LDPlayer_Switch_ThreadName = "igaio_switch";
        static public string LDPlayer_mode = "1-1";
        static public string LDPlayer_dnplayerdata = "";
        static public string switch_ldname = "switch";
        static public int LDPlayer_switch_manual;
        static public string  LDPlayer_manual_proxy, LDPlayer_proxifier_path;
        static public string LDPlayer_permission;
        static public LocalDataStoreSlot _ld_index = Thread.AllocateNamedDataSlot("ld_index");
        static public string lock_copy = "lock_copy";
        static public List<string> proxy_server_ips = new List<string>();
        static public string gdrive_folderId = "1xjTgI1UQnROuNMBgXXzYnyzjbH2woLvZ";

        static public void UpdateLDConstants(dynamic settings)
        {
            LDPlayer_path = settings.LDPlayer_path;
            LDPlayer_dnplayerdata=settings.LDPlayer_data_path;
            LDPlayer_Origin_name = settings.LDPlayer_Origin_name;
            //ftp_dnplayerdata = settings.LDPlayerDATA_path;
            LDPlayerThread_Count = settings.LDPlayer_Threads;
            LDPlayer_FocusEngage_Threads_Count = settings.LDPlayer_FocusEngage_Threads;
            LDPlayer_FocusMassAction_Threads_Count = settings.LDPlayer_FocusMassAction_Threads_Count;
            LDPlayer_mode = settings.LDPlayer_mode;
            LDPlayer_sleep_between_launch = int.Parse(settings.sleep_between_launch);

            ftp_username = settings.ftp_username;
            ftp_password = settings.ftp_password;
            ftp_credential = new NetworkCredential(ftp_username, ftp_password);
            pLFTP = new PLAutoHelper.FTPHelper.PLFTP(ftp_server_host, ftp_credential);

            //LDPlayer_switch_manual = int.Parse(settings.LDPlayer_switch_manual.ToString() == "" ? "-1" : settings.LDPlayer_switch_manual.ToString());
            //LDPlayer_manual_proxy = settings.LDPlayer_manual_proxy;
            LDPlayer_permission = settings.LDPlayer_permission;

            IGHelpers.Helpers.UpdateResourceConstants();
        }

        static private List<dynamic> phone = new List<dynamic>()
        {
            new {manu="samsung",model="SM-G977N"},
            new {manu="samsung",model="SM-G973N"},
            new {manu="samsung",model="SM-G975N"},
            new {manu="samsung",model="SM-G970N"},
            new {manu="samsung",model="SM-N971N"},
            new {manu="samsung",model="SM-N976N"},
            new {manu="samsung",model="SM-N960N"},
            new {manu="samsung",model="SM-G965N"},
            new {manu="samsung",model="SM-G960N"},
            new {manu="samsung",model="SM-A908N"},
            new {manu="samsung",model="SM-A805N"},
            new {manu="huawei",model="HMA-L29"},
            new {manu="huawei",model="EVR-L29"},
            new {manu="huawei",model="LYA-L29"},
            new {manu="huawei",model="LIO-L29"},
            new {manu="huawei",model="TAS-L29"},
            new {manu="huawei",model="VTR-L29"},
            new {manu="huawei",model="WAS-LX2"},
            new {manu="huawei",model="CLT-L29"},
            new {manu="huawei",model="EML-L29C"},
            new {manu="huawei",model="ANE-LX2"},
            new {manu="huawei",model="VOG-L04"},
            new {manu="huawei",model="ELE-L044"},
            new {manu="google",model="G020A"},
            new {manu="google",model="G020F"},
            new {manu="google",model="G020M"},
            new {manu="xiaomi",model="M1807E8A"},
            new {manu="xiaomi",model="M1808D2TG"},
            new {manu="xiaomi",model="M1906G7G"},
            new {manu="xiaomi",model="M1910F4G"},
            new {manu="xiaomi",model="M1910F4S"},
            new {manu="xiaomi",model="M1910F4G"},
            new {manu="xiaomi",model="M2003J6B2G"},
            new {manu="xiaomi",model="M2003J15SS"},
            new {manu="xiaomi",model="M2003J6A1G"},
            new {manu="xiaomi",model="M2003J6B1I"},
            new {manu="xiaomi",model="M1906G7G"},
            new {manu="xiaomi",model="M1908C3JI"},
            new {manu="oppo",model="CPH2069"},
            new {manu="oppo",model="CPH2059"},
            new {manu="oppo",model="CPH2005"},
            new {manu="oppo",model="CPH2023"},
            new {manu="oppo",model="CPH2025"},
            new {manu="oppo",model="CPH2043"},
            new {manu="oppo",model="CPH2036"},
            new {manu="oppo",model="CPH1943"},
            new {manu="oppo",model="CPH2021"},
            new {manu="oppo",model="CPH1938"}
        };

        static private List<string> samsung_model = new List<string>() { "SM-G977N", "SM-G973N", "SM-G975N", "SM-G970N", "SM-N971N", "SM-N976N", "SM-N960N", "SM-G965N", "SM-G960N", "SM-A908N", "SM-A805N" };

        #endregion

        static public void CloneDevice(string new_deviceName = "")
        {
            while (true)
            {
                //remove old device same name if has

                var command_delete = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole remove --name \"" + new_deviceName + "\"");

                if (command_delete.Contains("Usage"))
                    break;
                else
                    continue;
            }

            Mutex clone_m = new Mutex(false, "clone_device");
            clone_m.WaitOne();

            Thread.Sleep(5000);

            clone_m.ReleaseMutex();

            string command = "";

            if (new_deviceName == "")
            {
                command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole copy --from \"" + LDPlayer_Origin_name + "\"", 480);
            }


            else command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole copy --name \"" + new_deviceName + "\" --from \"" + LDPlayer_Origin_name + "\"", 480);

        }

        static public void RemoveDevice(string deviceName, bool removedata = false)
        {
            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole remove " + NameIndex(deviceName) + "");



            //remove folder in LDData
            if (removedata)
            {

                string ftp_remove_path = ftp_dnplayerdata + "/" + deviceName;

                IGHelpers.Helpers.FTP_DeleteDirectory(ftp_remove_path);
            }

        }

        static public void RebootDevice(string deviceName)
        {
            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole reboot " + NameIndex(deviceName) + "");

        }

        static public void InstallAPK(string deviceName, string apk_path)
        {
            ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole installapp " + NameIndex(deviceName) + " --filename " + apk_path);
        }

        static private object lock_launch = new object();

        static public string LaunchLDPlayer(string deviceName, bool useproxy = true)
        {
            bool checkxposed = false;
        LaunchLDPlayer:
            lock (lock_launch)
            {
                ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + ADBSupport.LDHelpers.LDPlayer_path + " \r\n ldconsole modify " + NameIndex(deviceName) + " --resolution 270,480,120"); Thread.Sleep(3000);
                ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole launch " + NameIndex(deviceName) + "");


                //check if minimize ldplayer
                if (Program.form.IsMinimizeLDPlayer())
                {
                    Stopwatch watch_minimize = new Stopwatch();
                    watch_minimize.Start();
                    while (true)
                    {
                        if (watch_minimize.Elapsed.TotalSeconds > 10)
                            break;

                        //var hwnd = KAutoHelper.AutoControl.FindWindowHandlesFromProcesses("LDPlayerMainFrame", null);

                        var hwnd = KAutoHelper.AutoControl.FindHandles(IntPtr.Zero, "LDPlayerMainFrame", null);

                        if (hwnd.Count > 0)
                        {
                            foreach (var item in hwnd)
                            {
                                IGHelpers.ViewDeviceHelpers.MinimizeWindow(item, 2);

                            }
                        }
                        Thread.Sleep(20);
                    }
                }

                Thread.Sleep(TimeSpan.FromSeconds(LDPlayer_sleep_between_launch));
            }

        //LoadDeviceToPanel:
        //    try
        //    {
        //        IGHelpers.ViewDeviceHelpers.LoadDeviceToPanel(deviceName);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.Message == "Not Found Device Window")
        //        {
        //            //Check if device is existed
        //            if (!IsDeviceExisted(deviceName))
        //            {
        //                //restore device
        //                IGThreads.ThreadHelpers.DoRestore(deviceName);
        //            }
        //        }
        //        else
        //            throw;
        //    }

        WaitDeviceOpen:
            var deviceID = WaitDeviceOpen(deviceName);

            if (deviceID == null)
            {
                ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole quit " + NameIndex(deviceName) + ""); Thread.Sleep(3000);
                //IGHelpers.ViewDeviceHelpers.RemovePanel(deviceName);
                if (!IsDeviceExisted(deviceName) && LDPlayer_mode != "switch")
                {
                    //restore device
                    IGThreads.ThreadHelpers.DoRestore(deviceName);
                }
                goto LaunchLDPlayer;
            }

            //log device ID
            if(!Thread.CurrentThread.Name.Contains("igaddaccount"))
                IGHelpers.DBHelpers.AddActionLog(deviceName, "deviceID", deviceID);

            //check if already has titaniumbackup

            if (!IsTitaniumBackupInstalled(deviceID))
            {
                ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole quit " + NameIndex(deviceName) + ""); Thread.Sleep(3000);

                //remove device
                var command_remove = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole remove " + NameIndex(deviceName) + ""); Thread.Sleep(3000);

                //restore device
                IGThreads.ThreadHelpers.DoRestore(deviceName);

                goto LaunchLDPlayer;
            }


            //active xposed

            //if (!checkxposed)
            //{
            //    IsXposedRunning(deviceName, deviceID);
            //    checkxposed = true;
            //    //IGHelpers.ViewDeviceHelpers.RemovePanel(deviceName);
            //    goto WaitDeviceOpen;
            //}

            //Set proxy for ld

            if (useproxy)
            {
            //Get and reset proxy
            SetProxy:
                string proxy = null;

                //if (Thread.CurrentThread.Name.Contains("igregister"))
                //    proxy = IGHelpers.ProxyHelpers.GetProxyReg();
                //else 
                //proxy = IGHelpers.ProxyHelpers.GetProxyOthers_v1(deviceName);

                proxy = IGHelpers.ProxyHelpers.GetProxyOthers_v1_AIO(deviceName);

                //Set proxy
                var setproxy = ADBSupport.ADBHelpers.SetProxy(deviceID, proxy);
                if (setproxy.Contains("IP not changed correctly"))
                {
                    ADBSupport.LDHelpers.HandleException(deviceName, "setproxy|" + setproxy, "");
                    IGHelpers.ProxyHelpers.ClearLogProxy();
                    goto SetProxy;
                }

            }

            //Check if error application has stopped, if yes click ok

            try
            {
                ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_ClickHasStopped_Ok, 1, 3, 3);
                ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Global_ClickHasStopped_Ok);
            }
            catch { }

            KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_HOME);

            //remove pictures in shared folder
            var command = ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell \"su -c 'rm -r sdcard/Pictures/Instagram/*'\"", 20);


            return deviceID;
        }

        static public void QuitAllDevices()
        {
            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole quitall");
            Thread.Sleep(2000);

        }

        static private bool IsDeviceExisted(string deviceName)
        {
            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole list");

            if (command.Contains("LDPlayer") && !command.Contains(deviceName))
            {
                return false;
            }

            return true;
        }

        static public void GenerateDeviceInfo(string deviceName)
        {
            //var manufacturer = "samsung";
            //var model = samsung_model[(new Random()).Next(samsung_model.Count)];

            //ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole modify " + NameIndex(deviceName)+" --manufacturer " + manufacturer + " --model " + model + " --imei auto --imsi auto --simserial auto --androidid auto --mac auto ");

            //choose random model

            var choiced_model = phone[(new Random()).Next(phone.Count)];

            ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole modify " + NameIndex(deviceName) + " --manufacturer " + choiced_model.manu + " --model " + choiced_model.model + " --imei auto --imsi auto --simserial auto --androidid auto --mac auto ");
        }

        //static public dynamic GetDeviceInfoFromFile(string deviceName)
        //{
        //    string file = LDPlayerDATA_path + "\\" + deviceName + "\\system\\deviceinfo.ig";

        //    //test

        //    //string file = @"F:\DOWNLOAD\deviceinfo.ig";

        //    var lines = File.ReadAllLines(file).ToList<string>();

        //    dynamic deviceinfo = new
        //    {
        //        manufacturer = GetValueOfDeviceInformation(lines, "manufacturer"),
        //        model = GetValueOfDeviceInformation(lines, "model"),
        //        imei = GetValueOfDeviceInformation(lines, "imei"),
        //        imsi = GetValueOfDeviceInformation(lines, "imsi"),
        //        simserial = GetValueOfDeviceInformation(lines, "simserial"),
        //        androidid = GetValueOfDeviceInformation(lines, "androidid"),
        //        mac = GetValueOfDeviceInformation(lines, "mac"),
        //        hard_serial = GetValueOfDeviceInformation(lines, "hard_serial")
        //    };

        //    return deviceinfo;
        //}

        static private string GetValueOfDeviceInformation(List<string> info, string key)
        {
            foreach (var item in info)
            {
                if (item.Split('|')[0] == key)
                    return item.Split('|')[1];
            }

            if (key == "mac")
                return "";

            throw new System.ArgumentException("Not found value");
        }

        static public string GetLatestBackupByPL(string deviceName)
        {
            string backup_folder = LDHelpers.LDPlayer_dnplayerdata + deviceName + "\\system\\backup";

            //check if folder exists

            if (!Directory.Exists(backup_folder))
                return null;


            List<string> choice_files = new List<string>();

            //Get from daily first

            var backup_daily_folder = backup_folder + "\\daily";


            if (!Directory.Exists(backup_daily_folder))
                return null;

            var daily_backup_bypl = Directory.GetFiles(backup_daily_folder).Where(s => Path.GetFileName(s).Contains("_bkpl.tar.gz")).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();

            if (daily_backup_bypl.Count > 0)
            {
                return daily_backup_bypl[0];
            }

            return null;
        }
        static public string GetLatestBackupByPL_FromFTPServer(string deviceName)
        {
            string backup_folder = "/"+ deviceName + "/system/backup";

            //check if folder exists

            if (!PLAutoHelper.FTPHelper.FTP_IsDirectoryExisted(pLFTP, backup_folder))
                return null;


            List<string> choice_files = new List<string>();

            //Get from daily first

            var backup_daily_folder = backup_folder + "/daily";


            if (!PLAutoHelper.FTPHelper.FTP_IsDirectoryExisted(pLFTP, backup_daily_folder))
                return null;

            //var daily_backup_bypl = Directory.GetFiles(backup_daily_folder).Where(s => Path.GetFileName(s).Contains("_bkpl.tar.gz")).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();

            var daily_backup_bypl = PLAutoHelper.FTPHelper.FTP_GetFileListing(pLFTP, backup_daily_folder, "*_bkpl.tar.gz").OrderByDescending(d => d.Modified).ToList();

            if (daily_backup_bypl.Count > 0)
            {
                foreach (var item in daily_backup_bypl)
                {
                    //check if file size=0
                    if(PLAutoHelper.FTPHelper.FTP_GetFileSize(pLFTP, item.FullName)==0)
                    {
                        PLAutoHelper.FTPHelper.FTP_DeleteFile(pLFTP, item.FullName);
                        continue;
                    }

                    return item.FullName;
                }
            }

            return null;
        }

        static public string GetLatestTitaniumBackupFolder(string deviceName)
        {
            string backup_folder = LDHelpers.LDPlayer_dnplayerdata + deviceName + "\\system\\backup";

            //check if folder exists

            if (!Directory.Exists(backup_folder))
                return null;


            List<string> choice_files = new List<string>();

            //Get from daily first

            var backup_daily_folder = backup_folder + "\\daily";

            if (Directory.Exists(backup_daily_folder))
            {

                var daily_property_files = Directory.GetFiles(backup_daily_folder).Where(s => Path.GetFileName(s).Contains(".properties")).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();


                foreach (var p in daily_property_files)
                {
                    var temp = backup_daily_folder + "\\" + Path.GetFileNameWithoutExtension(p) + ".tar";
                    if (File.Exists(backup_daily_folder + "\\" + Path.GetFileNameWithoutExtension(p) + ".tar.lzop"))
                    {
                        choice_files.Add(p);
                        choice_files.Add(backup_daily_folder + "\\" + Path.GetFileNameWithoutExtension(p) + ".tar.lzop");
                        break;
                    }

                    if (File.Exists(backup_daily_folder + "\\" + Path.GetFileNameWithoutExtension(p) + ".tar.gz"))
                    {
                        choice_files.Add(p);
                        choice_files.Add(backup_daily_folder + "\\" + Path.GetFileNameWithoutExtension(p) + ".tar.gz");
                        break;
                    }
                }
            }

            if (choice_files.Count != 0)
                goto CopyToLocal;

            //get backup file tu cac folder date

            var backup_directories = Directory.GetDirectories(backup_folder).OrderByDescending(d => (new DirectoryInfo(d)).LastWriteTime).ToList();


            for (int f = 0; f < backup_directories.Count; f++)
            {
                //get properties files and order by new to old
                var property_files = Directory.GetFiles(backup_directories[f]).Where(s => Path.GetFileName(s).Contains(".properties")).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();


                foreach (var p in property_files)
                {
                    var temp = backup_directories[f] + "\\" + Path.GetFileNameWithoutExtension(p) + ".tar";
                    if (File.Exists(backup_directories[f] + "\\" + Path.GetFileNameWithoutExtension(p) + ".tar.lzop"))
                    {
                        choice_files.Add(p);
                        choice_files.Add(backup_directories[f] + "\\" + Path.GetFileNameWithoutExtension(p) + ".tar.lzop");
                        break;
                    }

                    if (File.Exists(backup_directories[f] + "\\" + Path.GetFileNameWithoutExtension(p) + ".tar.gz"))
                    {
                        choice_files.Add(p);
                        choice_files.Add(backup_directories[f] + "\\" + Path.GetFileNameWithoutExtension(p) + ".tar.gz");
                        break;
                    }
                }

                if (choice_files.Count != 0)
                    break;
            }

            if (choice_files.Count == 0)
                return null;

            CopyToLocal:

            string local_latest_backup = "Temp\\" + deviceName + "\\backup\\";

            if (Directory.Exists(local_latest_backup))
                Directory.Delete(local_latest_backup, true);

            Directory.CreateDirectory(local_latest_backup);

            foreach(var f in choice_files)
            {
                File.Copy(f, local_latest_backup + Path.GetFileName(f));
            }

            //Copy apk backup - no need apk
            //File.Copy("com.instagram.android-4062eb78ce2849518bc4b41c5c7b83c2.apk.gz", local_latest_backup + "com.instagram.android-4062eb78ce2849518bc4b41c5c7b83c2.apk");

            return AppDomain.CurrentDomain.BaseDirectory + local_latest_backup;
        }

        static public void RemoveTitaniumBackupByName(string deviceName, string backupname)
        {
            string backup_folder = LDHelpers.LDPlayer_dnplayerdata + deviceName + "\\system\\backup";

            //check if folder exists

            if (!Directory.Exists(backup_folder))
                return;


            List<string> choice_files = new List<string>();

            //check from daily first

            var backup_daily_folder = backup_folder + "\\daily";

            if (Directory.Exists(backup_daily_folder))
            {

                var daily_files = Directory.GetFiles(backup_daily_folder).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();


                foreach (var p in daily_files)
                {
                    if (Path.GetFileName(p).Contains(backupname))
                        File.Delete(p);
                }
            }

            //get backup file tu cac folder date

            var backup_directories = Directory.GetDirectories(backup_folder).OrderByDescending(d => (new DirectoryInfo(d)).LastWriteTime).ToList();


            for (int f = 0; f < backup_directories.Count; f++)
            {
                var backup_files = Directory.GetFiles(backup_directories[f]).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();


                foreach (var p in backup_files)
                {
                    if (Path.GetFileName(p).Contains(backupname))
                        File.Delete(p);
                }
            }
        }

        static public string GetLatestTitaniumBackupFolder_FTPServer(string deviceName)
        {
            string backup_folder = "/" + deviceName + "/system/backup";

            //check if folder exists

            if (!PLAutoHelper.FTPHelper.FTP_IsDirectoryExisted(LDHelpers.pLFTP, backup_folder))
                return null;


            List<string> choice_files = new List<string>();

            //Get from daily first

            var backup_daily_folder = backup_folder + "/daily";

            if (PLAutoHelper.FTPHelper.FTP_IsDirectoryExisted(LDHelpers.pLFTP, backup_daily_folder))
            {

                //var daily_property_files = Directory.GetFiles(backup_daily_folder).Where(s => Path.GetFileName(s).Contains(".properties")).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();

                var daily_property_files = PLAutoHelper.FTPHelper.FTP_GetFileListing(LDHelpers.pLFTP, backup_daily_folder, "*.properties").OrderByDescending(d => d.Modified).ToList();


                foreach (var p in daily_property_files)
                {
                    var temp = backup_daily_folder + "/" + Path.GetFileNameWithoutExtension(p.FullName) + ".tar";
                    if (PLAutoHelper.FTPHelper.FTP_IsFileExisted(pLFTP, backup_daily_folder + "/" + Path.GetFileNameWithoutExtension(p.FullName) + ".tar.lzop"))
                    {
                        choice_files.Add(p.FullName);
                        choice_files.Add(backup_daily_folder + "/" + Path.GetFileNameWithoutExtension(p.FullName) + ".tar.lzop");
                        break;
                    }

                    if (PLAutoHelper.FTPHelper.FTP_IsFileExisted(pLFTP, backup_daily_folder + "/" + Path.GetFileNameWithoutExtension(p.FullName) + ".tar.gz"))
                    {
                        choice_files.Add(p.FullName);
                        choice_files.Add(backup_daily_folder + "/" + Path.GetFileNameWithoutExtension(p.FullName) + ".tar.gz");
                        break;
                    }
                }
            }

            if (choice_files.Count != 0)
                goto CopyToLocal;

            //get backup file tu cac folder date

            //var backup_directories = Directory.GetDirectories(backup_folder).OrderByDescending(d => (new DirectoryInfo(d)).LastWriteTime).ToList();

            var backup_directories = PLAutoHelper.FTPHelper.GetDirectoryListing(LDHelpers.pLFTP, backup_folder).OrderByDescending(d => d.Modified).ToList();


            for (int f = 0; f < backup_directories.Count; f++)
            {
                //get properties files and order by new to old
                //var property_files = Directory.GetFiles(backup_directories[f]).Where(s => Path.GetFileName(s).Contains(".properties")).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();

                var property_files = PLAutoHelper.FTPHelper.FTP_GetFileListing(LDHelpers.pLFTP, backup_directories[f].FullName, "*.properties").OrderByDescending(d => d.Modified).ToList();


                foreach (var p in property_files)
                {
                    var temp = backup_directories[f] + "\\" + Path.GetFileNameWithoutExtension(p.FullName) + ".tar";
                    if (PLAutoHelper.FTPHelper.FTP_IsFileExisted(pLFTP, backup_directories[f] + "\\" + Path.GetFileNameWithoutExtension(p.FullName) + ".tar.lzop"))
                    {
                        choice_files.Add(p.FullName);
                        choice_files.Add(backup_directories[f] + "\\" + Path.GetFileNameWithoutExtension(p.FullName) + ".tar.lzop");
                        break;
                    }

                    if (PLAutoHelper.FTPHelper.FTP_IsFileExisted(pLFTP, backup_directories[f] + "\\" + Path.GetFileNameWithoutExtension(p.FullName) + ".tar.gz"))
                    {
                        choice_files.Add(p.FullName);
                        choice_files.Add(backup_directories[f] + "\\" + Path.GetFileNameWithoutExtension(p.FullName) + ".tar.gz");
                        break;
                    }
                }

                if (choice_files.Count != 0)
                    break;
            }

            if (choice_files.Count == 0)
                return null;

            CopyToLocal:

            string local_latest_backup = "Temp\\" + deviceName + "\\backup\\";

            if (Directory.Exists(local_latest_backup))
                Directory.Delete(local_latest_backup, true);

            Directory.CreateDirectory(local_latest_backup);

            foreach (var f in choice_files)
            {
                //File.Copy(f, local_latest_backup + Path.GetFileName(f));
                PLAutoHelper.FTPHelper.FTP_DownloadFile(LDHelpers.pLFTP, f, PLAutoHelper.NormalFuncHelper.OptimizePathEndBackSlash(local_latest_backup) + Path.GetFileName(f));
            }

            //Copy apk backup - no need apk
            //File.Copy("com.instagram.android-4062eb78ce2849518bc4b41c5c7b83c2.apk.gz", local_latest_backup + "com.instagram.android-4062eb78ce2849518bc4b41c5c7b83c2.apk");

            return AppDomain.CurrentDomain.BaseDirectory + local_latest_backup;
        }

        static public void RemoveTitaniumBackupByName_FTPServer(string deviceName, string backupname)
        {
            string backup_folder ="/" + deviceName + "/system/backup";

            //check if folder exists


            if (!PLAutoHelper.FTPHelper.FTP_IsDirectoryExisted(LDHelpers.pLFTP, backup_folder))
                return;


            List<string> choice_files = new List<string>();

            //check from daily first

            var backup_daily_folder = backup_folder + "/daily";

            if (PLAutoHelper.FTPHelper.FTP_IsDirectoryExisted(LDHelpers.pLFTP, backup_daily_folder))
            {

                //var daily_files = Directory.GetFiles(backup_daily_folder).OrderByDescending(s => (new FileInfo(s)).LastWriteTime).ToList();


                var daily_files = PLAutoHelper.FTPHelper.FTP_GetFileListing(LDHelpers.pLFTP, backup_daily_folder).OrderByDescending(d => d.Modified).ToList();

                foreach (var p in daily_files)
                {
                    if (Path.GetFileName(p.FullName).Contains(backupname))
                        PLAutoHelper.FTPHelper.FTP_DeleteFile(LDHelpers.pLFTP, p.FullName);
                }
            }

            //get backup file tu cac folder date

            //var backup_directories = Directory.GetDirectories(backup_folder).OrderByDescending(d => (new DirectoryInfo(d)).LastWriteTime).ToList();



            var backup_directories = PLAutoHelper.FTPHelper.GetDirectoryListing(LDHelpers.pLFTP, backup_folder).OrderByDescending(d => d.Modified).ToList();

            for (int f = 0; f < backup_directories.Count; f++)
            {
                var backup_files = PLAutoHelper.FTPHelper.FTP_GetFileListing(LDHelpers.pLFTP, backup_directories[f].FullName).OrderByDescending(d => d.Modified).ToList();

                foreach (var p in backup_files)
                {
                    if (Path.GetFileName(p.FullName).Contains(backupname))
                        PLAutoHelper.FTPHelper.FTP_DeleteFile(LDHelpers.pLFTP, p.FullName);
                }
            }
        }

        static public bool IsTitaniumBackupInstalled(string deviceID)
        {
            var command_packages =  ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell pm list packages");

            if (!command_packages.Contains("com.keramidas.TitaniumBackup"))
            {
                return false;
            }

            return true;
        }

        static public void GetFullDeviceInfo(string deviceName)
        {
            var imei = Regex_ExtractDeviceInfo(ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole getprop " + NameIndex(deviceName) + " --key phone.imei"), "phone.imei");
            var imsi = Regex_ExtractDeviceInfo(ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole getprop " + NameIndex(deviceName) + " --key phone.imsi"), "phone.imsi");
            var simserial = Regex_ExtractDeviceInfo(ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole getprop " + NameIndex(deviceName) + " --key phone.simserial"), "phone.simserial");
            var androidid = Regex_ExtractDeviceInfo(ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole getprop " + NameIndex(deviceName) + " --key phone.androidid"), "phone.androidid");
        }

        static public string Regex_ExtractDeviceInfo(string command_res, string device_info)
        {
            var value = (new Regex("(?<=" + device_info + "\r\n)(.*?)(?=\r)")).Match(command_res).Value;

            return value;
        }

        static public string GetIMEI(string deviceName)
        {
            var imei_str = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole getprop " + NameIndex(deviceName) + " --key phone.imei");

            var imei = (new Regex("(?<=phone.imei\r\n)(.*?)(?=\r)")).Match(imei_str).Value;

            return imei;
        }

        static public void SetPhoneNumber(string deviceName, string phone)
        {
            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole modify " + NameIndex(deviceName) + " --pnumber " + phone);

            Thread.Sleep(2000);
        }


        static public bool IsDeviceOff(string deviceName)
        {
        WaitDeviceOff:
            int wait = 0;
            while (true)
            {
                var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole isrunning " + NameIndex(deviceName) + "");

                if (command.Contains("stop"))
                    break;
                wait++;
                if (wait > 50)
                {
                    ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole quit " + NameIndex(deviceName) + "");
                    Thread.Sleep(5000);
                    goto WaitDeviceOff;
                }


                Thread.Sleep(200);
            }

            return true;
        }

        static public void UninstallApp(string deviceName, string packagename)
        {
            ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole uninstallapp " + NameIndex(deviceName) + " --packagename " + packagename);
        }

        static public string WaitDeviceOpen(string deviceName)
        {
            bool isopen = false;

            int wait = 0;

            //check if device is open
            Stopwatch watch_device_open = new Stopwatch();
            watch_device_open.Start();
            while (true)
            {
                var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole isrunning " + NameIndex(deviceName) + "");

                if (command.Contains("running"))
                    break;

                if (watch_device_open.Elapsed.TotalMinutes > 2)
                    return null;

                Thread.Sleep(2000);
            }

            //wait android started
            watch_device_open.Restart();
            while (true)
            {

                var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole getprop " + NameIndex(deviceName) + "", 15);

                if (!command.Contains("error: device not found"))
                    break;

                if (watch_device_open.Elapsed.TotalMinutes > 2)
                    return null;


                Thread.Sleep(5000);
            }

            //Wait until home screen visible
            string deviceID = ADBSupport.ADBHelpers.GetDeviceByName(deviceName);

            if (deviceID == null)
                return null;

            //go home

            watch_device_open.Restart();
            while (true)
            {
                KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.KEYCODE_HOME);

                Thread.Sleep(2000);

                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_WaitDeviceOpen);
                    break;
                }
                catch (Exception ex)
                {
                    if (ex.Message == "Can't wait element")
                    {
                        if (watch_device_open.Elapsed.TotalMinutes > 0.5)
                            return null;

                        Thread.Sleep(2000);
                    }

                }
            }

            return deviceID;
        }

        static public void CloseDevice(string deviceName)
        {
            ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole quit " + NameIndex(deviceName) + "");

            Thread.Sleep(5000);
        }

        static public void IsXposedRunning(string deviceName, string deviceID)
        {

            //exec cmd
            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell \"su -c 'cd sdcard/Download/xposed;sh flash-script.sh'\"");

            //reboot
            ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole reboot " + NameIndex(deviceName) + "");

        }

        static public string NameIndex(string deviceName)
        {
            string return_device = "";
            //if (Thread.CurrentThread.Name.Contains(LDPlayer_Switch_ThreadName))
            if (Thread.GetData(_ld_index) != null)
            {
                //string index = Thread.CurrentThread.Name.Replace("igaio_switch_", "");
                var index = Thread.GetData(_ld_index);
                return_device= "--index " + index;
            }
            else
                return_device= "--name \"" + deviceName + "\"";

            return return_device;
        }



        #region --Manual Support--

        static public void OpenIGProfile(string deviceName, string ig_profile_link)
        {

            string username = ig_profile_link.Replace("https://www.instagram.com/", "").Replace("/", "");

            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole adb " + NameIndex(deviceName) + " --command \"shell am start -a android.intent.action.VIEW -d instagram://user?username=" + username + " com.instagram.android\"");
        }

        static public void OpenIGMediaByID(string deviceName, string ig_media_url)
        {
            string media_id = ig_media_url.Replace("https://www.instagram.com/p/", "").Replace("/", "");

            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole adb " + NameIndex(deviceName) + " --command \"shell am start -a android.intent.action.VIEW -d instagram://media?id=" + media_id + " com.instagram.android\"");
        }

        #endregion

        #region --Backup and Restore--

        //static public void BackUpApp(string deviceName, string package_name)
        //{
        //    string ftp_backup_folder = IGHelpers.Helpers.FTP_CreateDirectory(ftp_dnplayerdata + deviceName + "/system/ldbackup");

        //    string ftp_path = ftp_backup_folder + "/ig" + DateTime.Now.Ticks.ToString() + ".vmdk";

        //    string local_path = "Temp\\" + Path.GetFileName(ftp_path);

        //    var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole backupapp " + NameIndex(deviceName) + " --packagename \"" + package_name + "\" --file \"" + local_path + "\"", 120);

        //    if (!command.Contains("dnplayer Command Line Management Interface"))
        //        throw new System.ArgumentException("Not backup correctly");


        //    Thread.Sleep(10000);

        //    //Upload file to ftp server
        //    IGHelpers.Helpers.FTP_UploadFile(local_path, ftp_path, true);

        //}

        static public void RestoreApp(string deviceName, string package_name, string deviceID)
        {
            var ftp_restore_data_path = "";
            //Get latest backup file in system folder

            var ftp_system_path = ftp_dnplayerdata + deviceName + "/system/ldbackup";

            var ftp_igdata = IGHelpers.Helpers.FTP_GetFileListing(ftp_system_path, ".vmdk").OrderByDescending(x => x.Modified).ToList();

            //var igdata = Directory.GetFiles(system_path, "*.vmdk", SearchOption.TopDirectoryOnly).OrderByDescending(d => new FileInfo(d).CreationTime).ToList<string>();

            foreach (var data in ftp_igdata)
            {
                var length_mb = Math.Round(Convert.ToDecimal(data.Size / 1024), 0);

                if (length_mb < 5) //neu <5mb, co the bi loi, next
                {
                    //remove file
                    IGHelpers.Helpers.FTP_DeleteFile(data.FullName);

                    continue;
                }


                ftp_restore_data_path = data.FullName;

                var temp_local_path = "Temp\\" + deviceName + Path.GetFileName(ftp_restore_data_path);

                IGHelpers.Helpers.FTP_DownloadFile(ftp_restore_data_path, temp_local_path);

                var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\nldconsole restoreapp " + NameIndex(deviceName) + " --packagename \"" + package_name + "\" --file \"" + temp_local_path + "\"");

                Thread.Sleep(5000);

                File.Delete(temp_local_path);

                //Check if app is restore -- open app info of IG and check if it has data

                ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole adb " + NameIndex(deviceName) + " --command \"shell am start -a android.settings.APPLICATION_DETAILS_SETTINGS -d package:com.instagram.android\"");

                ADBSupport.ADBHelpers.WaitScreenByImage(ADBSupport.ADBHelpers.GetDeviceByName(deviceName), ADBSupport.BMPSource.AppInfo_DataAvail, 1, 1);

                //Check if login
                IsLogin:
                var command_openig = ADBSupport.ADBHelpers.GoToIntentView(deviceID, "android.intent.action.VIEW", "https://instagram.com", "com.instagram.android");
                try
                {
                    //Wait home

                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Global_WaitHome, 2, 10, 5);
                    break;
                }
                catch
                {
                    //check if instagram has stopped

                    try
                    {
                        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.ModeSwitch_IGStopped, 1, 1, 1);
                        //hit ok

                        ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Global_ClickHasStopped_Ok);

                        //file data bi loi =>delete

                        IGHelpers.Helpers.FTP_DeleteFile(data.FullName);


                    }
                    catch
                    {

                    }

                    //Check if supicious login
                    try
                    {
                        ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Login_ThisWasMe, 1, 1, 1);
                        //hit ok

                        ADBSupport.ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.Login_ThisWasMe);

                        goto IsLogin;
                    }
                    catch
                    {

                    }

                    //clear ig app data
                    ADBSupport.ADBHelpers.ClearAPPData(deviceID, "com.instagram.android");
                }
            }

        }

        //static public void SwapIGAccount(string target_deviceID, string target_deviceName, dynamic deviceinfo, string origindevice_name)
        //{
        //    ///Clear device

        //    //Clear app data -- adb -s 127.0.0.1:5631 shell pm clear com.instagram.android

        //    ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + target_deviceID + " shell pm clear com.instagram.android");

        //    //Change device information

        //    RestoreDevice(target_deviceID, deviceinfo);

        //    //Restore device masker information

        //    var generate_device_info = new Action.GenerateRandomDeviceInfo(target_deviceID, target_deviceName, "", ADBSupport.LDHelpers.LDPlayerDATA_path + "\\" + origindevice_name + "\\system" + origindevice_name + ".txt");
        //    generate_device_info.DoAction();

        //    //Load app data to device

        //    //Get latest backup file in system folder

        //    var system_path = LDPlayerDATA_path + "\\" + origindevice_name + "\\system";

        //    var igdata = Directory.GetFiles(system_path, "*.vmdk", SearchOption.TopDirectoryOnly).OrderByDescending(d => new FileInfo(d).CreationTime).ToList<string>();

        //    RestoreApp(target_deviceName, "com.instagram.android", igdata[0]);

        //    //Close and reopen device
        //}

        static public void RestoreDevice(string deviceName, dynamic deviceinfo)
        {


            var manufacturer = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole modify " + NameIndex(deviceName) + " --manufacturer \"" + deviceinfo.manufacturer + "\"");

            var model = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole modify " + NameIndex(deviceName) + " --model \"" + deviceinfo.model + "\"");

            var imei = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole modify " + NameIndex(deviceName) + " --imei \"" + deviceinfo.imei + "\"");

            var imsi = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole modify " + NameIndex(deviceName) + " --imsi \"" + deviceinfo.imsi + "\"");

            var simserial = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole modify " + NameIndex(deviceName) + " --simserial \"" + deviceinfo.simserial + "\"");

            var androidid = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole modify " + NameIndex(deviceName) + " --androidid \"" + deviceinfo.androidid + "\"");

            if (deviceinfo.mac != "")
            {
                var mac = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole modify " + NameIndex(deviceName) + " --mac \"" + deviceinfo.mac + "\"");
            }


            var pnumber = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole modify " + NameIndex(deviceName) + " --pnumber \"" + deviceinfo.pnumber + "\"");

            //var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole modify " + NameIndex(deviceName)+" --key ro.serialno --value \"" + deviceinfo.hard_serial + "\"");

        }

        static public void RestoreLDOnline(string deviceName, string key, string value)
        {
            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole modify " + NameIndex(deviceName)+" --key " + key + " --value \"" + value + "\"");
        }

        static public dynamic BackUpDevice(string deviceName)
        {
            //var ftp_system_devicepath = ADBSupport.LDHelpers.ftp_dnplayerdata + deviceName + "/system";

            //IGHelpers.Helpers.FTP_CreateDirectory(ftp_system_devicepath);

            //extract manufacturer

            var manufacturer = Regex_ExtractDeviceInfo(ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole getprop " + NameIndex(deviceName) + " --key ro.product.manufacturer"), "ro.product.manufacturer");

            //extract model

            var model = Regex_ExtractDeviceInfo(ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole getprop " + NameIndex(deviceName) + " --key ro.product.model"), "ro.product.model");

            //extract pnum
            var pnumber = Regex_ExtractDeviceInfo(ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole getprop " + NameIndex(deviceName) + " --key phone.pnumber"), "phone.pnumber");

            //extract imei
            var imei = Regex_ExtractDeviceInfo(ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole getprop " + NameIndex(deviceName) + " --key phone.imei"), "phone.imei");

            //extract imsi
            var imsi = Regex_ExtractDeviceInfo(ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole getprop " + NameIndex(deviceName) + " --key phone.imsi"), "phone.imsi");

            //extract simserial
            var simserial = Regex_ExtractDeviceInfo(ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole getprop " + NameIndex(deviceName) + " --key phone.simserial"), "phone.simserial");

            //extract androidid
            var androidid = Regex_ExtractDeviceInfo(ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole getprop " + NameIndex(deviceName) + " --key phone.androidid"), "phone.androidid");

            //extract mac
            string mac = "";

            try
            {
                var command_mac = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole adb " + NameIndex(deviceName) + " --command \"shell netcfg | grep eth0\"");

                var eth0_line = command_mac.Split(new string[] { "\r\n" }, StringSplitOptions.None).Where(s => s.Contains("eth0 ")).ToList<string>()[0];

                mac = eth0_line.Substring(eth0_line.LastIndexOf(" ") + 1).Replace("\r", "").Replace(":", "");
            }
            catch { }

            //extract hardware serial ro.serialno

            var hard_serial = Regex_ExtractDeviceInfo(ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole getprop " + NameIndex(deviceName) + " --key ro.serialno"), "ro.serialno");

            //extract device build finger print 

            var fingerprint = Regex_ExtractDeviceInfo(ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\n ldconsole getprop " + NameIndex(deviceName) + " --key ro.build.fingerprint"), "ro.build.fingerprint");


//            var ftp_path = ftp_system_devicepath + "/" + "deviceinfo.ig";

//            var deviceinfo = "manufacturer|" + manufacturer + "\r\nmodel|" + model + "\r\npnumber|" + pnumber + "\r\nimei|" + imei + "\r\nimsi|" + imsi + "\r\nsimserial|" + simserial + "\r\nandroidid|" + androidid + "\r\nhard_serial|" + hard_serial + "\r\nfingerprint|" + fingerprint;

////            File.WriteAllText(path, deviceinfo);

//            IGHelpers.Helpers.FTP_WriteTextToFile(deviceinfo, ftp_path);

            //Backup IG data

            //ADBSupport.LDHelpers.BackUpApp(deviceName, "com.instagram.android");

            return new
            {
                manufacturer,
                model,
                pnumber,
                imei,
                imsi,
                simserial,
                androidid,
                mac,
                hard_serial,
                fingerprint
            };

        }


        static public void PushFile(string deviceName, string local_path, string remote_path)
        {
            if (!System.IO.Path.IsPathRooted(local_path))
            {
                local_path = AppDomain.CurrentDomain.BaseDirectory + local_path;
            }

            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\nldconsole push " + NameIndex(deviceName) + " --remote \"" + remote_path + "\" --local \"" + local_path + "\"");
        }


        #endregion


        #region --Handle Ex--


        static public void HandleException(string deviceName, string message, string stack_trace)
        {

            string deviceID = ADBSupport.ADBHelpers.GetDeviceByName(deviceName);


            //Save current screen to db

            var error_path = "";

            if (deviceID != null)
            {
                //log trace and screen of ldplayer to db
                var screenshoot = ADBSupport.ADBHelpers.ScreenShoot(deviceID);

                if (screenshoot != null)
                {

                    error_path = "Temp\\error_" + Guid.NewGuid().GetHashCode().ToString().Replace("-", "") + ".jpg";

                    screenshoot.Save(error_path, ImageFormat.Jpeg);

                    //upload file to ftpserver

                    if (!PLAutoHelper.FTPHelper.FTP_IsDirectoryExisted(LDHelpers.pLFTP, "/Temp/"))
                        PLAutoHelper.FTPHelper.FTP_CreateDirectory(LDHelpers.pLFTP, "/Temp/");

                    try
                    {
                        PLAutoHelper.FTPHelper.FTP_UploadFile(LDHelpers.pLFTP, error_path, "/Temp/", false);
                    }
                    catch { }
                }

            }

            string log_id = IGHelpers.DBHelpers.AddLogException(deviceName, message + "|" + error_path, stack_trace);
        }

        #endregion


        #region --Switch LD Helpers--

        /// <summary>
        /// scan and add switch device to switch_ldplayers
        /// </summary>
        static public void ScanSwitchDevice()
        {
            //get list2 devices

            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\nldconsole list2");

            List<dynamic> switch_ldplayers = new List<dynamic>();

            var sr = new StringReader(command);
            string line = null;

            while ((line = sr.ReadLine()) != null)
            {
                if (line.Contains("," + switch_ldname))
                {
                    switch_ldplayers.Add(new
                    {
                        index = line.Split(',')[0],
                        name = line.Split(',')[1],
                        isrunning = line.Split(',')[2] != "0" ? true : false
                    });
                }
            }

            //clear old devices in previous session and add new devices

            IGHelpers.DBHelpers.AddSwitchDevice(switch_ldplayers);
        }

        static public string get_switch_device = "get_switch_device";

        /// <summary>
        /// get index of free device
        /// </summary>
        static public dynamic ReturnSwitchDeviceAvailable()
        {
            //get list2 devices
            lock (get_switch_device)
            {
                var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + LDPlayer_path + " \r\nldconsole list2");

                List<dynamic> switch_ldplayers = new List<dynamic>();

                var sr = new StringReader(command);
                string line = null;

                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Contains("," + switch_ldname))
                    {
                        switch_ldplayers.Add(new
                        {
                            index = line.Split(',')[0],
                            name = line.Split(',')[1],
                            isrunning = line.Split(',')[2] != "0" ? true : false
                        });
                    }
                }

                switch_ldplayers = switch_ldplayers.OrderBy(x => Guid.NewGuid()).ToList();

                //check device that is not running and make sure it's also not claim in database

                dynamic choiced_switch_ld = null;

                foreach (var device in switch_ldplayers)
                {
                    if (device.isrunning)
                        continue;

                    //check in db
                    if (IGHelpers.DBHelpers.IsDeviceFree(device.index))
                    {
                        choiced_switch_ld = device;
                        break;
                    }
                }

                if (choiced_switch_ld == null)
                    throw new System.ArgumentException("Not found free switch device");

                return choiced_switch_ld;
            }
        }


        #endregion
    }
}
