﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.ADBSupport
{
    static class ImageHelpers
    {
        static public Image cropImage(Bitmap bmpImage, Rectangle cropArea)
        {
            return bmpImage.Clone(cropArea, bmpImage.PixelFormat);
        }

        static public void AddWaterImage(string source_path, string watermark_path, string output_path, Point watermark_point, int watermark_width = 0)
        {
            Image imageBackground = Image.FromFile(source_path);

            Image imageOverlay = null;

            string resize_path = null;


            //if (watermark_width != 0)
            //{
            //    //resize_path = ResizeImage(watermark_path, watermark_width);
            //    //imageOverlay = Image.FromFile(resize_path);
            //    imageOverlay = KAutoHelper.CaptureHelper.ResizeImage(Image.FromFile(watermark_path), 400, 161);

            //}
            //else
            imageOverlay = Image.FromFile(watermark_path);


            int watermark_height = (int)(watermark_width * imageOverlay.Height / imageOverlay.Width);

            Bitmap img = new Bitmap(imageBackground.Width, imageBackground.Height);
            img.SetResolution(imageBackground.HorizontalResolution, imageBackground.VerticalResolution);

            using (Graphics gr = Graphics.FromImage(img))
            {
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.CompositingQuality = CompositingQuality.AssumeLinear;
                gr.DrawImage(imageBackground, new Point(0, 0));
                gr.DrawImage(imageOverlay, new Rectangle(watermark_point, new Size(watermark_width, watermark_height)));
            }
            img.Save(output_path, ImageFormat.Png);

            imageOverlay.Dispose();

            if (resize_path != null)
            {
                //delete temp
                File.Delete(resize_path);
            }
        }


        static public string ResizeImage(string imageFile, int target_width)
        {

            using (var srcImage = Image.FromFile(imageFile))
            {
                double scaleFactor = ((double)target_width) / srcImage.Width;
                var newWidth = (int)(srcImage.Width * scaleFactor);
                var newHeight = (int)(srcImage.Height * scaleFactor);
                using (var newImage = new Bitmap(newWidth, newHeight))
                using (var graphics = Graphics.FromImage(newImage))
                {
                    graphics.SmoothingMode = SmoothingMode.AntiAlias;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    graphics.DrawImage(srcImage, new Rectangle(0, 0, newWidth, newHeight));

                    var guid = Guid.NewGuid().GetHashCode().ToString();

                    newImage.Save("Temp\\resize_" + guid.Substring(guid.Length - 6) + ".png", ImageFormat.Png);
                    return "Temp\\resize_" + guid.Substring(guid.Length - 6) + ".png";
                }
            }
        }
    }
}
