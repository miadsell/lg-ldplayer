﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramLDPlayer.ADBSupport
{
    static class ADBHelpers
    {

        #region --ADB Screen Simulation--


        public static Bitmap ScreenShoot(
      string deviceID = null,
      bool isDeleteImageAfterCapture = true,
      string fileName = "screenShoot.png")
        {
            //var screenshoot_bmp = KAutoHelper.ADBHelper.ScreenShoot(deviceID, isDeleteImageAfterCapture, fileName);

            //return screenshoot_bmp;

            if (string.IsNullOrEmpty(deviceID))
            {
                List<string> devices = KAutoHelper.ADBHelper.GetDevices();
                if (devices == null || devices.Count <= 0)
                    return (Bitmap)null;
                deviceID = devices.First<string>();
            }
            string str1 = deviceID;
            try
            {
                str1 = deviceID.Split(':')[1];
            }
            catch
            {
            }
            string str2 = Path.GetFileNameWithoutExtension(fileName) + str1 + "_" + PLAutoHelper.NormalFuncHelper.RandomGUID(4) + Path.GetExtension(fileName);
            while (true)
            {
                if (File.Exists(str2))
                {
                    try
                    {
                        File.Delete(str2);
                        break;
                    }
                    catch
                    {
                    }
                }
                else
                    break;
            }


            var local = AppDomain.CurrentDomain.BaseDirectory + "Temp\\";
            int retry_capture = 0;
            CaptureScreen:
            //var command = ADBHelpers.ExecuteCMDTask(string.Format("adb -s {0} shell screencap -p \"{1}\"", (object)deviceID, (object)("/sdcard/" + str2)) + Environment.NewLine + string.Format("adb -s {0} pull \"{1}\" \"{2}\"", (object)deviceID, (object)("/sdcard/" + str2), local + str2) + Environment.NewLine + string.Format("adb -s {0} shell rm -f \"{1}\"", (object)deviceID, (object)("/sdcard/" + str2)) + Environment.NewLine, 20);

            var command = ADBHelpers.ExecuteCMDTask(string.Format("adb -s {0} shell screencap -p \"{1}\"", (object)deviceID, (object)("/sdcard/" + str2)), 20);

            command = ADBHelpers.ExecuteCMDTask(string.Format("adb -s {0} pull \"{1}\" \"{2}\"", (object)deviceID, (object)("/sdcard/" + str2), local + str2), 20);

            command = ADBHelpers.ExecuteCMDTask(string.Format("adb -s {0} shell rm -f \"{1}\"", (object)deviceID, (object)("/sdcard/" + str2)), 20);

            if (command == null)
                return null;
            if (!File.Exists(local + str2))
            {
                retry_capture++;
                if (retry_capture > 5)
                {
                    return null;
                    //throw new System.ArgumentException("Unknown error when capture");
                }
                Thread.Sleep(2000);
                goto CaptureScreen;
            }

            Bitmap bitmap1;
            try
            {
                using (Bitmap bitmap2 = new Bitmap(local + str2))
                    bitmap1 = new Bitmap((Image)bitmap2);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Parameter is not valid"))
                {
                    retry_capture++;
                    if (retry_capture > 5)
                    {
                        return null;
                        //throw new System.ArgumentException("Unknown error when capture");
                    }
                    goto CaptureScreen;
                }

                throw;
            }

            if (isDeleteImageAfterCapture)
            {
                try
                {
                    File.Delete(local + str2);
                }
                catch
                {
                }
            }
            return bitmap1;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="find_bmp"></param>
        /// <param name="start_after">cho bao nhieu s truoc khi bat dau check</param>
        /// <param name="wait_second_loop">thoi gian cho giua cac lan check</param>
        static public void WaitScreenByImage(string deviceID, Bitmap find_bmp, int count_retry = 5, int start_after = 5, int wait_second_loop = 5, double percent = 0.9)
        {
            Thread.Sleep(TimeSpan.FromSeconds(start_after));

            //var path = CreatePath("DeviceID\\" + deviceID.Replace(":","").Replace(".",""));

            //var wait_path = path + "\\" + DateTime.Now.Ticks.ToString() + ".png";

            //find_bmp.Save(wait_path, System.Drawing.Imaging.ImageFormat.Png);

            //var temp = AppDomain.CurrentDomain.BaseDirectory + wait_path;

            //var point = KAutoHelper.ADBHelper.FindImage(deviceID, AppDomain.CurrentDomain.BaseDirectory + wait_path, wait_second_loop * 1000, count_retry);

            //if (point == null)
            //    throw new System.ArgumentException("Can't wait element");


            int retry = 0;

            while (true)
            {
                var path = CreatePath("DeviceID\\" + deviceID.Replace(":", "").Replace(".", ""));

                var screenshot_temp = "screenshoot_" + DateTime.Now.Ticks.ToString();

                var guid = "";
                while (guid.Length <= 6)
                    guid = Guid.NewGuid().GetHashCode().ToString();


                var screen = ScreenShoot(deviceID, true, "screenShoot_" + guid.Substring(guid.Length - 6) + ".png");

                //screen.Save(path +"\\" + screenshot_temp + ".png", System.Drawing.Imaging.ImageFormat.Png);

                Point? find_point = null;

                DoFindPoint:
                Bitmap temp_bmp = null;

                lock (find_bmp)
                {
                    temp_bmp = new Bitmap((Image)find_bmp);
                }

                //temp_bmp = new Bitmap(find_bmp.Width, find_bmp.Height, find_bmp.PixelFormat);
                //using (Graphics g = Graphics.FromImage(temp_bmp))
                //{
                //    g.DrawImage(find_bmp, Point.Empty);
                //    g.Flush();
                //}

                try
                {
                    find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, percent);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("Object is currently in use elsewhere") || ex.Message.Contains("Bitmap region is already locked"))
                    {
                        Thread.Sleep((new Random()).Next(2000, 10000));
                        goto DoFindPoint;
                    }
                    throw;
                }

                if (find_point == null)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(wait_second_loop));
                    retry++;
                    if (retry > count_retry)
                        throw new System.ArgumentException("Can't wait element");

                    //Check if has program has stopped
                    lock (ADBSupport.BMPSource.ErrorScreen_UnsualActivity)
                    {
                        temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.ErrorScreen_UnsualActivity);
                    }
                    var find_point_unsual_activity = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp);

                    if (find_point_unsual_activity != null)
                    {
                        throw new System.ArgumentException("UnsualActivity");
                    }

                    //Check if has program has stopped
                    lock (ADBSupport.BMPSource.Global_ClickHasStopped_Ok)
                    {
                        temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.Global_ClickHasStopped_Ok);
                    }
                    var find_point_stopped = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp);

                    if (find_point_stopped != null)
                    {

                        KAutoHelper.ADBHelper.Tap(deviceID, find_point_stopped.Value.X, find_point_stopped.Value.Y);
                        ADBSupport.ADBHelpers.Delay(2, 3);
                    }

                    //Check if ask instagram rating

                    lock (ADBSupport.BMPSource.Rating_NoThanksOption)
                    {
                        temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.Rating_NoThanksOption);
                    }
                    var find_point_rating = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp);
                    if (find_point_rating != null)
                    {
                        ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.Rating_NoThanksOption);
                        ADBSupport.ADBHelpers.Delay(2, 3);
                    }

                    //Check if post removed nudity and click OK
                    lock (ADBSupport.BMPSource.ErrorScreen_PostRemoved)
                    {
                        temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.ErrorScreen_PostRemoved);
                    }
                    var find_point_postremoved = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.9);
                    if (find_point_postremoved != null)
                    {
                        ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.ErrorScreen_PostRemoved_OK, 0.8);
                        ADBSupport.ADBHelpers.Delay(6, 10);
                    }

                    //Check if story removed nudity and click OK
                    lock (ADBSupport.BMPSource.ErrorScreen_StoryRemoved)
                    {
                        temp_bmp = new Bitmap((Image)ADBSupport.BMPSource.ErrorScreen_StoryRemoved);
                    }
                    var find_point_storyremoved = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.9);
                    if (find_point_storyremoved != null)
                    {
                        ADBHelpers.ClickByImageRandom(deviceID, ADBSupport.BMPSource.ErrorScreen_PostRemoved_OK, 0.8);
                        ADBSupport.ADBHelpers.Delay(6, 10);
                    }

                    //check if restrict
                    lock (BMResource.ActionBlocked.ActionBlocked_Restrictv1)
                    {
                        temp_bmp = new Bitmap((Image)BMResource.ActionBlocked.ActionBlocked_Restrictv1);
                    }
                    var find_point_restrict = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, 0.9);
                    if (find_point_restrict != null)
                    {
                        ADBHelpers.ClickByImageRandom(deviceID, BMResource.ActionBlocked.ActionBlocked_TellUsv1, 0.8);
                        var deviceName = Thread.GetData(Thread.GetNamedDataSlot("deviceName")).ToString();
                        IGHelpers.DBHelpers.AddActionLog(deviceName, "Action Blocked");
                        ADBSupport.ADBHelpers.Delay(6, 10);

                        if(Thread.GetData(Thread.GetNamedDataSlot("thread_type")).ToString()== "action_mass")
                        {
                            throw new System.ArgumentException("Action Blocked");
                        }
                    }


                }
                else break;
            }
        }

        static public void WaitImageInsivible(string deviceID, Bitmap expected_hide_bmp, int timeout_seconds = 30)
        {
            Stopwatch watch_invisible = new Stopwatch();
            watch_invisible.Start();

            while (true)
            {
                if (!IsImageExisted(deviceID, expected_hide_bmp, 1, 0, 0))
                {
                    return;
                }

                Thread.Sleep(5000);

                if (watch_invisible.Elapsed.TotalSeconds > timeout_seconds)
                    throw new System.ArgumentException("Can't wait element invisible");
            }
        }

        static public bool IsImageExisted(string deviceID, Bitmap find_bmp, int count_retry = 5, int start_after = 5, int wait_second_loop = 5, double percent = 0.9)
        {
            try
            {
                WaitScreenByImage(deviceID, find_bmp, count_retry, start_after, wait_second_loop, percent);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="click_bmp"></param>
        /// <param name="wait_bmp"></param>
        /// <param name="timeout">timeout in minutes</param>
        static public void ClickandWait(string deviceID, Bitmap click_bmp, Bitmap wait_bmp, int timeout = 6, string bitmap_name = null)
        {
            var current_screen = ScreenShoot(deviceID);

            Stopwatch watch_process = new Stopwatch();
            watch_process.Start();

            //Wait bmp
            while (true)
            {
                //Click bitmap


                ClickByImage(deviceID, click_bmp);
            ContinueWait:

                if (watch_process.Elapsed.TotalMinutes > timeout)
                    throw new System.ArgumentException("Failed to click and wait");


                try
                {
                    WaitScreenByImage(deviceID, wait_bmp, 5, 2, 1, 0.8);
                    break;
                }
                catch
                {
                    //Check if current screen still visible

                    if(IsImageExisted(deviceID,click_bmp))
                    {
                        ClickByImage(deviceID, click_bmp);
                    }

                    goto ContinueWait;
                }
            }
        }

        static public void ClickByImage(string deviceID, Bitmap find_bmp, string bitmap_name = null)
        {
            //var path = CreatePath("DeviceID\\" + deviceID.Replace(":", ""));

            //var click_path = path + "\\" + DateTime.Now.Ticks.ToString() + ".png";

            //KAutoHelper.ADBHelper.FindImageAndClick(deviceID, click_path);


            WaitScreenByImage(deviceID, find_bmp, 5, 1, 1);

        ClickByImage:
            var screen = ScreenShoot(deviceID);

            Bitmap temp_bmp = null;

            lock (find_bmp)
            {
                temp_bmp = new Bitmap((Image)find_bmp);
            }

            //temp_bmp = new Bitmap(find_bmp.Width, find_bmp.Height, find_bmp.PixelFormat);
            //using (Graphics g = Graphics.FromImage(temp_bmp))
            //{
            //    g.DrawImage(find_bmp, Point.Empty);
            //    g.Flush();
            //}

            var find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp);

            RandomTapImage(deviceID, find_point.Value.X, find_point.Value.Y, bitmap_name);

            //if (find_point != null)
            //{
            //    //KAutoHelper.ADBHelper.Tap(deviceID, find_point.Value.X, find_point.Value.Y);

            //    RandomTapImage(deviceID, find_point.Value.X, find_point.Value.Y, bitmap_name);
            //}
            //else
            //{
            //    try
            //    {
            //        var find_point_stopped = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, ADBSupport.BMPSource.Global_ClickHasStopped_Ok);
            //        KAutoHelper.ADBHelper.Tap(deviceID, find_point_stopped.Value.X, find_point_stopped.Value.Y);
            //        ADBSupport.ADBHelpers.Delay(2, 3);
            //        goto ClickByImage;
            //    }
            //    catch
            //    {
            //        throw new System.ArgumentException("Not found image");
            //    }

            //}
        }

        /// <summary>
        /// click random in image, auto get size of image to determine click range
        /// </summary>
        static public void ClickByImageRandom(string deviceID, Bitmap find_bmp, double percent = 0.9)
        {
            ClickByImage:
            var screen = ScreenShoot(deviceID);

            Bitmap temp_bmp = null;

            lock (find_bmp)
            {
                temp_bmp = new Bitmap((Image)find_bmp);
            }

            //temp_bmp = new Bitmap(find_bmp.Width, find_bmp.Height, find_bmp.PixelFormat);
            //using (Graphics g = Graphics.FromImage(temp_bmp))
            //{
            //    g.DrawImage(find_bmp, Point.Empty);
            //    g.Flush();
            //}

            var find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp, percent);

            if (find_point != null)
            {

                var clicked_x = find_point.Value.X + RandomIntValue(0, find_bmp.Width);
                var clicked_y = find_point.Value.Y + RandomIntValue(0, find_bmp.Height);


                KAutoHelper.ADBHelper.Tap(deviceID, clicked_x, clicked_y);
            }
            else
            {
                try
                {
                    var find_point_stopped = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, ADBSupport.BMPSource.Global_ClickHasStopped_Ok);
                    KAutoHelper.ADBHelper.Tap(deviceID, find_point_stopped.Value.X, find_point_stopped.Value.Y);
                    ADBSupport.ADBHelpers.Delay(2, 3);
                    goto ClickByImage;
                }
                catch
                {
                    throw new System.ArgumentException("Not found image");
                }

            }
        }


        static public void ClickandWaitRandom(string deviceID, Bitmap click_bmp, Bitmap wait_bmp, int timeout = 6)
        {
            var current_screen = ScreenShoot(deviceID);

            Stopwatch watch_process = new Stopwatch();
            watch_process.Start();

            //Wait bmp
            while (true)
            {
                //Click bitmap


                ClickByImageRandom(deviceID, click_bmp);
            ContinueWait:


                if (watch_process.Elapsed.TotalMinutes > timeout)
                    throw new System.ArgumentException("Failed to click and wait");

                try
                {
                    WaitScreenByImage(deviceID, wait_bmp, 1, 2, 2, 0.8);
                    break;
                }
                catch
                {
                    //Check if current screen still visible

                    var now_screen = ScreenShoot(deviceID);
                    try
                    {
                        WaitScreenByImage(deviceID, click_bmp, 1, 0, 3);
                        ClickByImage(deviceID, click_bmp);
                    }
                    catch
                    {

                    }
                    goto ContinueWait;
                }

            }
        }

        /// <summary>
        /// Click relative in percentage
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="tagBmp">bitmap cua vi tri neo</param>
        /// <param name="point_demotagBmp">vi tri khi test -> dung de tinh khoang cach giua vi tri can click voi vi tri neo</param>
        /// <param name="point_demoA">vi tri test - click A trong rectangle ABCD</param>
        /// <param name="point_demoC">vi tri test - click C trong rectangle ABCD</param>
        static public void ClickRelative(string deviceID, Bitmap tagBmp, dynamic point_demotagBmp, dynamic point_demoA, dynamic point_demoC)
        {
            //same XClickRelative uivision, tim điểm neo, click vị trí cach vị trị neo 1 khoảng cách cố định

            var point_tagBmp = FindPointByPercentage(deviceID, tagBmp);

            var pointA_x = point_demoA.x - point_demotagBmp.x + point_tagBmp.x;
            var pointA_y = point_demoA.y - point_demotagBmp.y + point_tagBmp.y;

            var rec_click_width = point_demoC.x - point_demoA.x;
            var rec_click_height = point_demoC.y - point_demoA.y;


            TapRecTanglePercentage(deviceID, new Class.RecPercentage(new { x = pointA_x, y = pointA_y }, rec_click_width, rec_click_height));

        }

        static public void LongPressRelative(string deviceID, Bitmap tagBmp, dynamic point_demotagBmp, dynamic point_demoA, dynamic point_demoC, int duration = 100)
        {
            //same XClickRelative uivision, tim điểm neo, click vị trí cach vị trị neo 1 khoảng cách cố định

            var point_tagBmp = FindPointByPercentage(deviceID, tagBmp);

            var pointA_x = point_demoA.x - point_demotagBmp.x + point_tagBmp.x;
            var pointA_y = point_demoA.y - point_demotagBmp.y + point_tagBmp.y;

            var rec_click_width = point_demoC.x - point_demoA.x;
            var rec_click_height = point_demoC.y - point_demoA.y;

            double tap_width = ((new Random()).NextDouble()) * rec_click_width;
            double tap_height = ((new Random()).NextDouble()) * rec_click_height;

            var press_x = ConvertPercentageToPoint(deviceID, "X", pointA_x + tap_width);
            var press_y = ConvertPercentageToPoint(deviceID, "Y", pointA_y + tap_height);

            KAutoHelper.ADBHelper.LongPress(deviceID, (int)press_x, (int)press_y, duration);


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="tagBmp">bitmap cua vi tri neo</param>
        /// <param name="point_demotagBmp">vi tri khi test -> dung de tinh khoang cach giua vi tri can click voi vi tri neo</param>
        /// <param name="point_demoA">vi tri test - click A trong rectangle ABCD</param>
        /// <param name="point_demoC">vi tri test - click C trong rectangle ABCD</param>
        static public System.Drawing.Image CropScreenshotRelative(string deviceID, Bitmap tagBmp, dynamic point_demotagBmp, dynamic point_demoA, dynamic point_demoC)
        {
            //same XClickRelative uivision, tim điểm neo, click vị trí cach vị trị neo 1 khoảng cách cố định

            var point_tagBmp = FindPointByPercentage(deviceID, tagBmp);

            var pointA_x = point_demoA.x - point_demotagBmp.x + point_tagBmp.x;
            var pointA_y = point_demoA.y - point_demotagBmp.y + point_tagBmp.y;

            var rec_click_width = point_demoC.x - point_demoA.x;
            var rec_click_height = point_demoC.y - point_demoA.y;


            var screen = ScreenShoot(deviceID);

            int int_x = (int)(screen.Width * pointA_x / 100);
            int int_y = (int)(screen.Height * pointA_y / 100);

            int int_rec_width= (int)(screen.Width * rec_click_width / 100);
            int int_rec_height = (int)(screen.Height * rec_click_height / 100);

            var img=ADBSupport.ImageHelpers.cropImage(screen, new Rectangle(int_x, int_y, int_rec_width, int_rec_height));

            return img;

        }

        static public void RandomScrollScreen(string deviceID, int scroll_time = 1, int delay_between = 5, double scroll_start_y = 38.4, double scroll_stop_y = 78.7, int duration = 100)
        {
            double scroll_x = IGHelpers.Helpers.RandomScroll_X();

            for (int s = 0; s < scroll_time; s++)
            {
                try
                {
                    KAutoHelper.ADBHelper.SwipeByPercent(deviceID, scroll_x, scroll_start_y, scroll_x, scroll_stop_y, 100);
                }
                catch
                {
                    throw new System.ArgumentException("Not found device");
                }
            }

            Thread.Sleep(500);
        }

        static public void ScrollDownToImage(string deviceID, double point_start_Y, Bitmap to_bmp, double scroll_distance = 15, int duration = 100)
        {
            Stopwatch watch_scroll = new Stopwatch();
            watch_scroll.Start();
            while (true)
            {
                //KAutoHelper.ADBHelper.SwipeByPercent(deviceID, 52, point_start_Y, 52.6, point_start_Y - scroll_distance, duration);

                SwipeByPercent_RandomTouch(deviceID, "doc", "22-86", point_start_Y, point_start_Y - scroll_distance, duration);

                try
                {
                    WaitScreenByImage(deviceID, to_bmp, 0, 0, 0);
                    break;
                }
                catch
                {

                }

                if (watch_scroll.Elapsed.TotalMinutes > 1)
                    throw new System.ArgumentException("Not found image to scroll");
            }
        }

        static public void ScrollUpToImage(string deviceID, double point_start_Y, Bitmap to_bmp, double scroll_distance = 15, int duration = 100)
        {
            Stopwatch watch_scroll = new Stopwatch();
            watch_scroll.Start();

            while (true)
            {
                //KAutoHelper.ADBHelper.SwipeByPercent(deviceID, 52, point_start_Y, 52.6, point_start_Y + scroll_distance, duration);

                SwipeByPercent_RandomTouch(deviceID, "doc", "22-86", point_start_Y, point_start_Y + scroll_distance, duration);

                try
                {
                    WaitScreenByImage(deviceID, to_bmp, 0, 0, 0);
                    break;
                }
                catch
                {

                }

                if (watch_scroll.Elapsed.TotalMinutes > 2)
                {
                    throw new System.ArgumentException("Can't scroll up to image");
                }
            }
        }

        static public void ScrollImageToPosByPercent(string deviceID, Bitmap need_move_bmp, double point_destination_Y)
        {
            var screen = ScreenShoot(deviceID);

            var find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, need_move_bmp);

            var distance = point_destination_Y - ConvertPointToPercentage(deviceID, "Y", find_point.Value.Y);
            if (distance < 0)
                distance = distance * (-1);
            if (distance < 4)
                return;

            //Check if vi tri hien tai so voi vi tri dich qua nho thi bo qua, vi co the hieu nham thanh click

            //KAutoHelper.ADBHelper.SwipeByPercent(deviceID, 51, ConvertPointToPercentage(deviceID, "Y", find_point.Value.Y), 51, point_destination_Y, 1200);

            SwipeByPercent_RandomTouch(deviceID, "doc", "20-72", ConvertPointToPercentage(deviceID, "Y", find_point.Value.Y), point_destination_Y, 1200);
        }

        #endregion


        #region --Simulate Random Point--

        // Tương tự các hàm simulate point nhưng thay vi handle tại 1 số vị trí đã set, có thể thể random điểm chạm để tự nhiên hơn

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="vector"></param>
        /// <param name="percentage_range">khoảng cho phep để random x hoặc y tuy theo chiều dọc hoặc ngang</param>
        /// <param name="b_from"></param>
        /// <param name="b_to"></param>
        /// <param name="duration"></param>
        static public void SwipeByPercent_RandomTouch(string deviceID, string vector, string percentage_range, double b_from, double b_to, int duration)
        {
            int range_from = int.Parse(percentage_range.Split('-')[0]);
            int range_to = int.Parse(percentage_range.Split('-')[1]);

            switch (vector)
            {
                case "doc": //swipe dọc

                    var x_from = (new Random().Next(range_from, range_to));

                    var x_to = x_from + (new Random()).Next(-5, +5);

                    KAutoHelper.ADBHelper.SwipeByPercent(deviceID, x_from, b_from, x_to, b_to, duration);

                    break;
                case "ngang": //swipe ngang

                    var y_from = (new Random().Next(range_from, range_to));

                    var y_to = y_from + (new Random()).Next(-5, +5);

                    KAutoHelper.ADBHelper.SwipeByPercent(deviceID, b_from, y_from, b_to, y_to, duration);

                    break;
                default:
                    throw new System.ArgumentException("not understand your vector");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="x">point x of bitmap in screen pixel</param>
        /// <param name="y">point x of bitmap in screen pixel</param>
        /// <param name="bitmap_name">name of bitmap to get width height clicked range in pixel</param>
        static public void RandomTapImage(string deviceID, int x, int y, string bitmap_name)
        {
            if (bitmap_name == null)
            {
                KAutoHelper.ADBHelper.Tap(deviceID, x, y);
                return;
            }
            var bmp_rec_variables = typeof(ADBSupport.BMPRec).GetFields()
                            .Where(field => field.Name == bitmap_name)
                            .ToList().First();
            Class.ClickRange clickRange = (Class.ClickRange)bmp_rec_variables.GetValue(null);

            ////Convert to percentage
            //var width_per = ConvertPointToPercentage(deviceID, "X", clickRange.width);
            //var height_per = ConvertPointToPercentage(deviceID, "Y", clickRange.height);


            //Select point to click in percentage

            var clicked_x = x + RandomIntValue(0, clickRange.width);
            var clicked_y = y + RandomIntValue(0, clickRange.height);


            KAutoHelper.ADBHelper.Tap(deviceID, clicked_x, clicked_y);
        }

        static public void RandomTapImageByPercentage(string deviceID, double x, double y, string bitmap_name)
        {
            if (bitmap_name == null)
            {
                KAutoHelper.ADBHelper.TapByPercent(deviceID, x, y);
                return;
            }
            var bmp_rec_variables = typeof(ADBSupport.BMPRec).GetFields()
                            .Where(field => field.Name == bitmap_name)
                            .ToList().First();
            Class.ClickRange clickRange = (Class.ClickRange)bmp_rec_variables.GetValue(null);

            ////Convert to percentage
            var width_per = ConvertPointToPercentage(deviceID, "X", clickRange.width);
            var height_per = ConvertPointToPercentage(deviceID, "Y", clickRange.height);


            //Select point to click in percentage

            var clicked_x = x + RandomDoubleValue(0, width_per);
            var clicked_y = y + RandomDoubleValue(0, height_per);


            KAutoHelper.ADBHelper.TapByPercent(deviceID, clicked_x, clicked_y);
        }

        static public double RandomDoubleValue(double minimum, double maximum)
        {
            var random_double = (new Random()).NextDouble() * (maximum - minimum) + minimum;

            return random_double;
        }

        static public int RandomIntValue(int minimum, int maximum)
        {
            var random_int = (new Random()).Next(minimum, maximum);

            return random_int;
        }

        #endregion

        #region --ADB Backend Support --

        static public void CopyTextFromEsEditor(string deviceID, string filepath)
        {
        //Push to sdcard
        PushFileToDevice:
            ADBSupport.ADBHelpers.PushFileToPhone(deviceID, filepath, "/sdcard/Download");

            //Check if content is avail in device

            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell \"su -c 'cd sdcard/Download;ls'\"");
            if (!command.Contains(Path.GetFileName(filepath)))
                goto PushFileToDevice;

            //Open file on device and use clipboard manager to copy

            ADBHelpers.OpenFileIntentEsEditor(deviceID, "sdcard/Download/" + Path.GetFileName(filepath));


            //Hit edit

            ADBHelpers.ClickandWait(deviceID, BMPSource.Es_Menu, BMPSource.Es_Edit, 1);

            ADBHelpers.ClickByImage(deviceID, BMPSource.Es_Edit); Thread.Sleep(4000);


            //Long press to show top bard
            Stopwatch watch_longpress = new Stopwatch();
            watch_longpress.Start();

            while (true)
            {

                if (watch_longpress.Elapsed.TotalMinutes > 1)
                    throw new System.ArgumentException("Can't Copy Text - EsEditor");

                KAutoHelper.ADBHelper.LongPress(deviceID, 8, 68, 1000);

                //Check if topbar is show => if yes, break
                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Es_SelectAll, 1, 2, 1);
                    break;
                }
                catch { }

            }


            //Hit select all

            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Es_SelectAll); Thread.Sleep(2000);

            //Hit copy

            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.Es_Copy);

        }

        static public void CopyTextFromFile(string deviceID, string filepath)
        {
        //Push to sdcard
        PushFileToDevice:
            ADBSupport.ADBHelpers.PushFileToPhone(deviceID, filepath, "/sdcard/Download");

            //Check if content is avail in device

            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell \"su -c 'cd sdcard/Download;ls'\"");
            if (!command.Contains(Path.GetFileName(filepath)))
                goto PushFileToDevice;

            //Open file on device and use clipboard manager to copy

            ADBSupport.ADBHelpers.OpenFileIntentHtmlViewer(deviceID, "sdcard/Download/" + Path.GetFileName(filepath));

            //Wait open (check comment.txt)

            ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.WaitHtmlViewer);


            //Long press to show top bard
            int start_x = 4;
            while (true)
            {
                KAutoHelper.ADBHelper.LongPress(deviceID, 10, 70, 1000);

                //Check if topbar is show => if yes, break
                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.HtmlViewerSelect, 1, 2, 1);
                    break;
                }
                catch { }

                start_x++;
            }

            //Hit select all

            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.HtmlViewerSelect); Thread.Sleep(2000);

            //Hit copy

            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.HtmlViewerCopy);
        }

        static public void SendUnicodeTextByADBKeyboard(string deviceID, string deviceName, string spin_text, bool realdevice = false)
        {
            var text = SharpSpin.Spinner.Spin(spin_text);
            bool first_install = false;
        //get list keyboards
        ListKeyboards:
            var command_keyboards = ExecuteCMDTask("adb -s " + deviceID + " shell ime list -s");

            var command_keyboards_line = ReadLinesFromStr(command_keyboards);


            List<string> keyboards = new List<string>();

            foreach (var item in command_keyboards_line)
            {
                if (item.Contains("com.android"))
                    keyboards.Add(item);
            }

            if (keyboards.Count == 1 && !realdevice) //chua install ADBKeyboard
            {
                //install adbkeyboard apk
                if (first_install)
                {
                    throw new System.ArgumentException("ADBKeyboard wasn't installed correctly");
                }

                //Check if it's installed

                var command_packages = ExecuteCMDTask("adb -s " + deviceID + " shell pm list packages");

                if (command_packages.Contains("com.android.adbkeyboard"))
                    goto EnableKeyboard;

                var adbkeyboard_path ="Temp\\" + deviceName + "\\system\\adbkeyboard.apk";
                File.Copy("adbkeyboard.apk", adbkeyboard_path, true);

                var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + ADBSupport.LDHelpers.LDPlayer_path + " \r\n ldconsole installapp " + ADBSupport.LDHelpers.NameIndex(deviceName) + " --filename \"" + adbkeyboard_path + "\"");
                Thread.Sleep(10000);
                first_install = true;

            //enable ADBKeyboard
            EnableKeyboard:
                var command_enable = ExecuteCMDTask("adb -s " + deviceID + " shell ime enable com.android.adbkeyboard/.AdbIME");

                if (!command_enable.Contains(": now enabled"))
                {
                    throw new System.ArgumentException("ADBKeyboard wasn't enabled correctly");
                }

                goto ListKeyboards;
            }

            //set adbkeyboard to default
            var command_set_keyboard= ExecuteCMDTask("adb -s " + deviceID + " shell ime set com.android.adbkeyboard/.AdbIME "); Thread.Sleep(2000);

            //Convert text to base64


            List<string> lines = ReadLinesFromStr(text);


            //List<string> lines = text.Split(new string[] { mode }, StringSplitOptions.None).ToList();

            for (int i = 0; i < lines.Count; i++)
            {
                var line_text = lines[i];

                if (i != lines.Count - 1)
                {
                    line_text += "\r\n";
                }

                //split line_text by .

                var dot_splits = line_text.Split('.').ToList();

                for (int d = 0; d < dot_splits.Count; d++)
                {
                    var dot_line = dot_splits[d];

                    if (d != dot_splits.Count - 1)
                    {
                        dot_line += ".";
                    }

                    var base64str = Base64Encode(dot_line);

                    //Send unicode text

                    var command_input_base64 = ExecuteCMDTask("adb -s " + deviceID + " shell am broadcast -a ADB_INPUT_B64 --es msg '" + base64str + "'", 60);

                    if (!command_input_base64.Contains("Broadcast completed: result=0"))
                    {
                        if (command_input_base64.Contains("Broadcasting")) //tuc la da hien broadcasting nhung k complete
                        {
                            throw new System.ArgumentException("Text wasn't sent correctly|" + command_input_base64);
                        }
                        //neu khong phai thi do text qua dai
                    }

                    ADBHelpers.Delay(0.3, 0.7);
                }
            }
        }

        static public string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            var base64str = System.Convert.ToBase64String(plainTextBytes);
            return base64str;
        }

        static public string Base64Decode(string base64_str)
        {
            var plainTextBytes = Convert.FromBase64String(base64_str);

            string plain_text = System.Text.Encoding.UTF8.GetString(plainTextBytes);

            return plain_text;
        }

        static public List<string> ReadLinesFromStr(string text)
        {
            List<string> lines = new List<string>();
            string aLine = null;
            StringReader strReader = new StringReader(text);
            while (true)
            {
                aLine = strReader.ReadLine();

                if (aLine == null)
                    break;
                lines.Add(aLine);
            }

            return lines;
        }

        static public void PushImageandScanGallery(string deviceID, string imagepath)
        {
            var filename = Path.GetFileName(imagepath);
            //push image
            PushFileToPhone(deviceID, imagepath, "sdcard/Download/" + filename);

            //remove launch ad

            var command_remove_launchad = ExecuteCMDTask("adb -s " + deviceID + " shell \"su -c 'rm -r sdcard/launcher/ad'\"");

            //scan gallery
            //WARNING: neu scan ngay luc nay, thi se bi dong bo voi folder share tren pc, tuc la se co cac hinh anh o cac device khac shown len
            var command_scan = ExecuteCMDTask("adb -s " + deviceID + " shell am broadcast -a android.intent.action.MEDIA_MOUNTED -d file:///sdcard");

            if(!command_scan.Contains("Broadcast completed: result=0"))
            {
                throw new System.ArgumentException("Not scan gallery correctly");
            }

            //check if file exist

            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell \"su -c 'cd sdcard/Download;ls'\"");
            if(!command.Contains(filename))
            {
                throw new System.ArgumentException("File not push to device correctly");
            }
        }

        static public void ScannGallery(string deviceID)
        {

            //scan gallery
            var command_scan = ExecuteCMDTask("adb -s " + deviceID + " shell am broadcast -a android.intent.action.MEDIA_MOUNTED -d file:///sdcard");
            Thread.Sleep(3000);
        }

        static public void DownloadImage(Class.IGDevice iGDevice, string image_link)
        {
            string deviceID = iGDevice.deviceID;
        DownloadImage:
            //Download image
            Thread.Sleep(10000);
            ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s \"" + deviceID + "\" shell am start -a android.intent.action.VIEW -d " + image_link, 1);

            //Longpress to show menu
            Stopwatch watch_longpress = new Stopwatch();
            watch_longpress.Start();

            while (true)
            {
                if (watch_longpress.Elapsed.TotalMinutes > 0.5)
                    goto DownloadImage;
                KAutoHelper.ADBHelper.LongPress(deviceID, 150, 106, 2000);

                try
                {
                    ADBSupport.ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.SaveImage, 1, 2, 0);
                    break;
                }
                catch { }

                try
                {

                    ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.ReloadImage);
                }
                catch
                {

                }
            }

            //Click save image and sleep 5000
            ADBSupport.ADBHelpers.ClickByImage(deviceID, ADBSupport.BMPSource.SaveImage);

            Thread.Sleep(5000);

            //Use su to check if it has already been downloaded
            var command = ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell \"su -c 'cd sdcard/Download;ls'\"");
            if (!command.Contains(Path.GetFileName(image_link)))
                goto DownloadImage;
        }

        static public string ContentFromClipBoard(string deviceID)
        {
            var clipboard_content = ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell am broadcast -a clipper.get");

            var content = (new Regex("(?<=Broadcast completed: result=-1, data=\")(.*?)(?=\"\r)")).Match(clipboard_content).Value;

            return content;
        }

        static public string GoToIntentView(string deviceID, string intent, string scheme, string package)
        {
            var command = ExecuteCMDTask("adb -s " + deviceID + " shell am start -a " + intent + " -d \"" + scheme + "\" " + package);
            return command;
        }

        static public string SetProxy(string deviceID, string proxy)
        {
            var host = proxy.Split(':')[0];
            var port = proxy.Split(':')[1];

            //ExecuteCMDTask("adb -s " + deviceID + " shell settings put global http_proxy " + proxy);

            //ExecuteCMDTask("adb -s " + deviceID + " shell settings put global http_proxy " + proxy);

            //Open proxy droid
            LaunchDroid:
            var command_launch = ExecuteCMDTask("adb -s " + deviceID + " shell monkey -p org.proxydroid -c android.intent.category.LAUNCHER 1");

            //Wait proxy droid
            //try
            //{
            WaitScreenByImage(deviceID, ADBSupport.BMPSource.ProxyDroidPort);


            //Check if IsOn

            try
            {
                WaitScreenByImage(deviceID, ADBSupport.BMPSource.ProxyOn, 1, 3, 0, 0.95);

                //If yes, turn off
                //Click off
                ClickandWait(deviceID, ADBSupport.BMPSource.ProxyOn, ADBSupport.BMPSource.ProxyOff, 1);
            }
            catch
            {

            }
            //ClearPort:
            //    ADBHelpers.ClickandWait(deviceID, ADBSupport.BMPSource.ProxyDroidPort, ADBSupport.BMPSource.PortPopupEdit, 1);

            Stopwatch watch_clearport = new Stopwatch();
            watch_clearport.Start();
            ClearPort:
            ADBHelpers.ClickByImageRandom(deviceID, BMResource.ProxyDroid.ProxyDroid_ProxyDroidPort);

            if (!ADBHelpers.IsImageExisted(deviceID, BMResource.ProxyDroid.ProxyDroid_PortPopupEdit, 5, 2, 1))
            {
                if (watch_clearport.Elapsed.TotalMinutes > 1)
                {
                    throw new System.ArgumentException("Can't Set Proxy");
                }
                ADBHelpers.ClickByImageRandom(deviceID, BMResource.ProxyDroid.ProxyDroid_PAC_Ticked); Thread.Sleep(1000);
                goto ClearPort;
            }

            //Clear port popup

            int try_clearedport = 0;

            while (true)
            {
                for (int i = 0; i < 6; i++)
                {
                    ExecuteCMDTask("adb -s " + deviceID + " shell input keyevent KEYCODE_FORWARD_DEL");
                    //KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.keycode_bac);
                    Thread.Sleep(100);
                }

                //Check if port cleared

                if (IsImageExisted(deviceID, ADBSupport.BMPSource.ClearedPort, 1, 3, 0))
                    break;

                if (try_clearedport > 2)
                {
                    throw new System.ArgumentException("Can't cleard proxy port");
                }

                try_clearedport++;
            }

            //Fill new port

            ClickByImageRandom(deviceID, ADBSupport.BMPSource.ClearedPort);

            SendUnicodeTextByADBKeyboard(deviceID, null, port);

            //InputTextWithDelay(deviceID, port);

            //Click ok

            ClickandWait(deviceID, ADBSupport.BMPSource.EditPortOK, ADBSupport.BMPSource.ProxyOff, 1);

            ClearHost:

            ADBHelpers.ClickandWait(deviceID, BMResource.ProxyDroid.ProxyDroid_HostTitle, BMResource.ProxyDroid.ProxyDroid_HostTitle_Popup, 1);

            //Clear host popup

            for (int retry = 0; retry < 2; retry++)
            {

                for (int i = 0; i < 20; i++)
                {
                    ExecuteCMDTask("adb -s " + deviceID + " shell input keyevent KEYCODE_FORWARD_DEL");
                    //KAutoHelper.ADBHelper.Key(deviceID, KAutoHelper.ADBKeyEvent.keycode_bac);
                    Thread.Sleep(100);
                }


                //Check if host cleared
                if (IsImageExisted(deviceID, BMResource.ProxyDroid.ProxyDroid_HostCleared, 1, 3, 0))
                    break;

                if (retry == 1)
                    throw new System.ArgumentException("Can't cleard proxy host");
            }


            //Fill new host


            ClickByImageRandom(deviceID, BMResource.ProxyDroid.ProxyDroid_HostCleared);

            SendUnicodeTextByADBKeyboard(deviceID, null, host);

            //InputTextWithDelay(deviceID, host);

            //Click ok

            ClickandWait(deviceID, BMResource.ProxyDroid.ProxyDroid_Host_OkBtn, ADBSupport.BMPSource.ProxyOff, 1);

            //Check if socks5 is choiced

            //try
            //{
            //    ADBHelpers.WaitScreenByImage(deviceID, BMPSource.VerifySetSocks5, 1, 2);
            //}
            //catch
            //{

            //    //Scroll to see proxy type

            //    ScrollDownToImage(deviceID, 70.9, ADBSupport.BMPSource.ProxyTypeTitle);

            //    ScrollImageToPosByPercent(deviceID, ADBSupport.BMPSource.ProxyTypeTitle, 80.1);

            //    //if not set socks5
            //    ADBHelpers.ClickandWait(deviceID, BMPSource.ProxyTypeTitle, BMPSource.Socks5Option, 1);

            //    ADBHelpers.ClickandWait(deviceID, BMPSource.Socks5Option, BMPSource.VerifySetSocks5);
            //}

            ScrollUpToImage(deviceID, 34.9, ADBSupport.BMPSource.ProxySwitchTitle);

        //Turn on

        //ClickByImageRandom(deviceID, ADBSupport.BMPSource.ProxyOff);

        //WaitScreenByImage(deviceID, ADBSupport.BMPSource.ProxyOn, 5, 2, 1);

        //demoTag: 6.1,21.3 --demoA: 84.6,23.3 --demoC: 88.5, 24.5
        TurnOnProxy:
            ClickRelative(deviceID, ADBSupport.BMPSource.ProxySwitchTitle, new { x = 6.1, y = 21.3 }, new { x = 84.6, y = 23.3 }, new { x = 88.5, y = 24.5 });


            //Add proxy ip to db

            if (Thread.CurrentThread.Name.Contains("register"))
            {

                RestClient client = new RestClient("https://ifconfig.me/");
                var request = new RestRequest("", Method.GET);
                client.Proxy = new WebProxy(proxy.Split(':')[0], int.Parse(proxy.Split(':')[1]));
                var query = client.Execute(request);

                var htmlDoc_load = new HtmlAgilityPack.HtmlDocument();
                htmlDoc_load.LoadHtml(query.Content);

                var ip = query.Content;

                using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
                {
                    conn.Open();

                    using (var command = new SqlCommand("insert into temp_reglog (deviceID,ipaddress) values ('" + deviceID + "','" + ip + "')", conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }

            //Check if proxy is changed

            Thread.Sleep(2000);
            int retry_check = 0;
        IsProxyChangedCorrectly:
            var pc_ip = (new System.Text.RegularExpressions.Regex("(?<=ifconfig.me\\/ip\\r\\n)(.*?)(?=\\r\\n)")).Match(ADBSupport.ADBHelpers.ExecuteCMDTask("curl ifconfig.me/ip")).Value;
            //ifconig.me is currently error so use ifconfig.co
            //var pc_ip = ADBSupport.ADBHelpers.ExtractProxyFromCurl_Ifconfig_Co(ADBSupport.ADBHelpers.ExecuteCMDTask("curl ifconfig.co"));

            string ldplayer_request = null;

            Stopwatch watch = new Stopwatch();
            watch.Start();
            while (true)
            {

                ldplayer_request = ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell curl ifconfig.me/ip");
                //ifconig.me is currently error so use ifconfig.co

                //ldplayer_request = ADBSupport.ADBHelpers.ExecuteCMDTask("adb -s " + deviceID + " shell curl ifconfig.co");

                if (ldplayer_request != null)
                {
                    break;
                }

                Thread.Sleep(2000);

                if (watch.Elapsed.TotalSeconds > 30)
                {
                    throw new System.ArgumentException("Can't confirm ip change");
                }
            }


            var ldplayer_ip = (new System.Text.RegularExpressions.Regex("(?<=ifconfig.me\\/ip\\r\\n)(.*?)(?=\\r\\n)")).Match(ldplayer_request).Value;

            //var ldplayer_ip = ADBSupport.ADBHelpers.ExtractProxyFromCurl_Ifconfig_Co(ldplayer_request);

            if (pc_ip.Contains(".") && ldplayer_ip.Contains("."))
            {
                if (pc_ip != ldplayer_ip)
                {

                }
                else
                {
                    retry_check++;
                    if (retry_check > 5)
                    {
                        return "IP not changed correctly pc_ip: " + pc_ip.ToString() + " ldplayer_ip: " + ldplayer_ip.ToString();
                    }
                    goto TurnOnProxy;
                }
            }
            else
            {
                Thread.Sleep(5000);
                retry_check++;
                if (retry_check > 5)
                {
                    return "IP not changed correctly pc_ip: " + pc_ip.ToString() + " ldplayer_ip: " + ldplayer_ip.ToString();
                }
                goto IsProxyChangedCorrectly;
            }

            return "";

        }

        static public string ExtractProxyFromCurl_Ifconfig_Co(string command_value)
        {
            string aLine;
            StringReader strReader = new StringReader(command_value);
            while (true)
            {
                aLine = strReader.ReadLine();
                if (aLine != null)
                {
                    if(aLine.Contains("ifconfig.co"))
                    {
                        return strReader.ReadLine();
                    }
                }
            }
        }

        static public double ConvertPointToPercentage(string deviceID, string type, double value)
        {
            var screen_size = KAutoHelper.ADBHelper.GetScreenResolution(deviceID);
            switch (type)
            {
                case "X":
                    return value / screen_size.X * 100;
                case "Y":
                    return value / screen_size.Y * 100;
            }

            return 0;
        }

        static public int ConvertPercentageToPoint(string deviceID, string type, double value)
        {
            var screen_size = KAutoHelper.ADBHelper.GetScreenResolution(deviceID);
            switch (type)
            {
                case "X":
                    return (int)(value / 100 * screen_size.X);
                case "Y":
                    return (int)(value / 100 * screen_size.Y);
            }

            return 0;
        }

        static public dynamic FindPointByPercentage(string deviceID, Bitmap need_found_bmp)
        {
            var screen = ScreenShoot(deviceID);

            if (screen == null)
            {
                throw new System.ArgumentException("Can't capture screen");
            }

            Bitmap temp_bmp = null;

            lock (need_found_bmp)
            {
                temp_bmp = new Bitmap((Image)need_found_bmp);
            }

            var find_point = KAutoHelper.ImageScanOpenCV.FindOutPoint(screen, temp_bmp);

            if (find_point == null)
                return null;

            var x = ADBSupport.ADBHelpers.ConvertPointToPercentage(deviceID, "X", find_point.Value.X);
            var y = ADBSupport.ADBHelpers.ConvertPointToPercentage(deviceID, "Y", find_point.Value.Y);

            return new
            {
                x,
                y
            };

        }

        static public void TapByPercentage(string deviceID, double x, double y, Bitmap wait_bmp, int timeout=1)
        {
            var current_screen = ScreenShoot(deviceID);

            Stopwatch watch_process = new Stopwatch();
            watch_process.Start();

            //Wait bmp
            while (true)
            {
                //Click bitmap


                KAutoHelper.ADBHelper.TapByPercent(deviceID, x, y);

                if (wait_bmp == null)
                    break;

                ContinueWait:


                if (watch_process.Elapsed.TotalMinutes > timeout)
                    throw new System.ArgumentException("Failed to click and wait");

                try
                {
                    WaitScreenByImage(deviceID, wait_bmp, 1, 2, 2, 0.8);
                    break;
                }
                catch
                {
                    //Check if current screen still visible

                    var now_screen = ScreenShoot(deviceID);
                    try
                    {
                        WaitScreenByImage(deviceID, current_screen, 1, 0, 3, 0.7);
                        KAutoHelper.ADBHelper.TapByPercent(deviceID, x, y);
                    }
                    catch
                    {

                    }
                    goto ContinueWait;
                }

            }
        }

        static public void TapRecTanglePercentage(string deviceID, Class.RecPercentage recPercentage, Bitmap wait_bmp = null, int tap_time = 1, double timeout = 2)
        {
            double tap_width = ((new Random()).NextDouble()) * recPercentage.width;
            double tap_height = ((new Random()).NextDouble()) * recPercentage.height;

            var current_screen = ScreenShoot(deviceID);

            Stopwatch watch_process = new Stopwatch();
            watch_process.Start();

            //Wait bmp
            while (true)
            {
                //Click bitmap


                KAutoHelper.ADBHelper.TapByPercent(deviceID, recPercentage.point.x + tap_width, recPercentage.point.y + tap_height, tap_time);

                if (wait_bmp == null)
                    break;

            ContinueWait:


                if (watch_process.Elapsed.TotalMinutes > timeout)
                    throw new System.ArgumentException("Failed to click and wait");

                try
                {
                    WaitScreenByImage(deviceID, wait_bmp, 1, 2, 2, 0.8);
                    break;
                }
                catch
                {
                    //Check if current screen still visible

                    var now_screen = ScreenShoot(deviceID);
                    try
                    {
                        WaitScreenByImage(deviceID, current_screen, 1, 0, 3, 0.7);
                        KAutoHelper.ADBHelper.TapByPercent(deviceID, recPercentage.point.x + tap_width, recPercentage.point.y + tap_height, tap_time);
                    }
                    catch
                    {

                    }
                    goto ContinueWait;
                }

            }



        }

        static public void PushFileToPhone(string deviceID, string file_path, string device_path)
        {
            if (!System.IO.Path.IsPathRooted(file_path))
            {
                file_path = AppDomain.CurrentDomain.BaseDirectory + file_path;
            }

            var command = ExecuteCMDTask("adb -s " + deviceID + " push \"" + file_path + "\" " + device_path);
        }

        static public void PullFileToLocal(string deviceID, string file_path, string local_path)
        {
            if (!System.IO.Path.IsPathRooted(local_path))
            {
                local_path = AppDomain.CurrentDomain.BaseDirectory + local_path;
            }

            ExecuteCMDTask("adb -s " + deviceID + " pull \"" + file_path + "\" \"" + local_path + "\"");
        }

        /// <summary>
        /// Crop and get value from crop rectangle (abcd) from screen
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="A"></param>
        /// <param name="C"></param>
        /// <returns></returns>
        static public void ValueFromScreenByPercentage(string deviceID, string device_name, Class.RecPercentage rec, bool issave = false, string filename = null)
        {
            var temp_path = "Temp\\" + device_name + "\\temp_img";
            CreatePath(temp_path);
            var screen = ScreenShoot(deviceID);

            //Get device size

            //var command_size = ExecuteCMDTask("adb -s " + deviceID + " shell wm size");

            var command_size = ExecuteCMDTask("adb -s " + deviceID + " shell wm size");

            var size_str = (new Regex("(?<=Physical size: )(.*?)(?=\r\r\n)")).Match(command_size).Value;

            if (size_str == "")
                size_str = "270x480";

            var width = int.Parse(size_str.Split('x')[0]);
            var height = int.Parse(size_str.Split('x')[1]);

            //Convert recpercentage to Rectangle class

            var rectangle = rec.ConvertToRectangle(width, height);

            var cropimg = ImageHelpers.cropImage(screen, rectangle);

            string temp_img_path = temp_path + "\\" + "temp_crop.jpeg";


            cropimg.Save(temp_img_path, System.Drawing.Imaging.ImageFormat.Jpeg);

            if (issave)
            {
                var igprofile_path = "Temp\\" + device_name + "\\" + "igprofile_img\\";
                CreatePath(igprofile_path);
                screen.Save(igprofile_path + filename + ".jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);

                IGHelpers.Helpers.FTP_UploadFile(igprofile_path, LDHelpers.ftp_dnplayerdata + device_name + "/igprofile_img/");
            }


            //var task = Task.Run(async () => await IGHelpers.OCRSpaceHelpers.OCRActionAsync(temp_img_path));
            //task.Wait();

            //return task.Result;
        }

        static public string GetDeviceByName(string ld_player_name)
        {
            string imei = "";
            Stopwatch watch_device = new Stopwatch();
            watch_device.Start();

            while (true)
            {
                imei = ADBSupport.LDHelpers.GetIMEI(ld_player_name);

                if (imei.Contains("device not found") || imei.Contains("adb.exe: device"))
                {
                    Thread.Sleep(5000);

                    if (watch_device.Elapsed.TotalMinutes > 2)
                        return null;

                }
                else break;
            }


            while (true)
            {

                var listDevice = KAutoHelper.ADBHelper.GetDevices();


                for (int i = listDevice.Count - 1; i >= 0; i--)
                {
                    string adb_imei = "";
                    //var imei_str = ExecuteCMDTask("adb -s " + listDevice[i] + " shell service call iphonesubinfo 1");
                    var imei_str = ExecuteCMDTask("adb -s " + listDevice[i] + " shell service call iphonesubinfo 1");

                    if (imei_str == null)
                        return null;

                    var matches = (new Regex("(?<=')(.*?)(?=')")).Matches(imei_str);
                    foreach (var item in matches)
                    {
                        adb_imei += item.ToString();
                    }

                    adb_imei = adb_imei.Replace(".", "").Replace(" ", "");

                    if (adb_imei == imei)
                    {
                        return listDevice[i];
                    }
                }

                if (watch_device.Elapsed.TotalMinutes > 2)
                    return null;
            }
        }

        //static public string GetProxySettings(string deviceID)
        //{
        //    var proxy_str = ExecuteCMDTask("adb -s " + deviceID + " shell settings get global http_proxy ");

        //    foreach (var line in proxy_str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
        //    {
        //        if (line.Contains(ADBSupport.LDHelpers.proxy_server_ip))
        //            return line;
        //    }

        //    return null;
        //}

        static public void SetLocaleRegion(string deviceID)
        {
            var command = ExecuteCMDTask("adb -s " + deviceID + " shell setprop ro.product.locale.region VN");
        }

        static public void ClearAPPData(string deviceID, string package)
        {
            var command = ExecuteCMDTask("adb -s " + deviceID + " shell pm clear " + package);
        }

        static public void InputTextWithDelay(string deviceID, string text)
        {
            //foreach (char c in text)
            //{
            //    KAutoHelper.ADBHelper.InputText(deviceID, c.ToString());
            //    //Delay

            //    Thread.Sleep((new Random()).Next(120, 220));
            //}

            KAutoHelper.ADBHelper.InputText(deviceID, text);
        }

        static public string CreatePath(string folder)
        {
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            return folder;
        }

        static public void RestartADB()
        {
            ExecuteCMDTask("adb kill-server"); Thread.Sleep(10000);
            ExecuteCMDTask("adb start-server"); Thread.Sleep(20000);
        }

        static Semaphore Semaphore_ExecuteCMDTask = new Semaphore(10, 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="timeout">in seconds</param>
        /// <returns></returns>
        static public string ExecuteCMDTask(string command, int timeout = 10)
        {
            Semaphore_ExecuteCMDTask.WaitOne();

            try
            {
                int execute_time = 0;
                ExecuteCMDTask:
                var task = Task.Run(() =>
                {
                    return ExecuteCMD(command, timeout);
                });

                bool isCompletedSuccessfully = task.Wait(TimeSpan.FromSeconds(timeout));

                if (isCompletedSuccessfully)
                {
                    return task.Result;
                }
                else
                {
                    execute_time++;
                    if (execute_time > 10)
                        return null;
                    Thread.Sleep(1000);
                    goto ExecuteCMDTask;
                }
            }
            finally
            {
                Semaphore_ExecuteCMDTask.Release();
            }
        }

        static private string ExecuteCMD(string cmdCommand, int timeout)
        {
            try
            {
                Process process = new Process();
                ProcessStartInfo info = new ProcessStartInfo
                {
                    WorkingDirectory=ADBSupport.LDHelpers.LDPlayer_path,
                    FileName = "cmd.exe",
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true
                };
                process.StartInfo = info;
                process.Start();
                process.StandardInput.WriteLine(cmdCommand);
                process.StandardInput.Flush();
                process.StandardInput.Close();
                process.WaitForExit(timeout * 1000 - 5000);
                return process.StandardOutput.ReadToEnd();
            }
            catch
            {
                return null;
            }
        }

        static public void UnInstallApp(string deviceID, string packagename)
        {
            var command = ExecuteCMDTask("adb -s " + deviceID + " uninstall " + packagename);

            //delete data

            command = ExecuteCMDTask("adb -s " + deviceID + " shell rm -rf /data/data/" + packagename);

        }

        static public void StartClipperService(string deviceID, string deviceName)
        {
            //Check if clipper is installed
            //Check if it's installed
            bool first_install = false;
            EnableClipper:
            var command_packages = ExecuteCMDTask("adb -s " + deviceID + " shell pm list packages");

            if (!command_packages.Contains("ca.zgrs.clipper"))
            {
                if(first_install)
                {
                    throw new System.ArgumentException("Clipper isn't installed correctly");
                }

                var clipper_path = AppDomain.CurrentDomain.BaseDirectory + "Temp\\" + deviceName + "\\clipper.apk";
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "Temp\\" + deviceName);
                File.Copy("clipper.apk", clipper_path, true);

                var command = ADBSupport.ADBHelpers.ExecuteCMDTask("cd " + ADBSupport.LDHelpers.LDPlayer_path + " \r\n ldconsole installapp " + ADBSupport.LDHelpers.NameIndex(deviceName) + " --filename \"" + clipper_path + "\"");
                Thread.Sleep(10000);

                first_install = true;

                goto EnableClipper;
            }

            var command_startservice = ExecuteCMDTask("adb -s \"" + deviceID + "\" shell am startservice ca.zgrs.clipper/.ClipboardService");
        }

        static public void OpenFileIntentHtmlViewer(string deviceID, string path_to_file)
        {
            ExecuteCMDTask("adb -s \"" + deviceID + "\" shell am start -n com.android.htmlviewer/.HTMLViewerActivity -d file:///" + path_to_file + " -t \"text / html\"");
        }


        static public void OpenFileIntentEsEditor(string deviceID, string path_to_file)
        {
            ExecuteCMDTask("adb -s \"" + deviceID + "\" shell am start -n com.estrongs.android.pop/.app.editor.PopNoteEditor -d file:///" + path_to_file + " --activity-clear-task");

            //wait

            ADBHelpers.WaitScreenByImage(deviceID, ADBSupport.BMPSource.Es_Menu, 5, 5, 5);


            //Set Unicode

            ADBHelpers.ClickandWait(deviceID, BMPSource.Es_Menu, BMPSource.Es_Encoding, 1);

            //Click unicode

            ADBHelpers.ClickandWait(deviceID, BMPSource.Es_Encoding, BMPSource.Es_UTF8, 1);


            //Click utf8

            ADBHelpers.ClickandWait(deviceID, BMPSource.Es_UTF8, BMPSource.Es_Menu, 1);
        }

        static public bool VerifyDeviceIDOnline(string deviceID)
        {
            var command_packages = ExecuteCMDTask("adb devices");

            if (command_packages.Contains(deviceID))
                return true;
            return false;
        }

        static public void BlockedNotificatioin(string deviceID, string packageName = "com.instagram.android")
        {
            ExecuteCMDTask("adb -s " + deviceID + " shell appops set " + packageName + " POST_NOTIFICATION ignore");
        }

        #endregion

        #region --ADB Backup and Restore By PL--

        static public void BackupByPL(string deviceID, string packageName, string local_folder_path, string device_backup_path = "/sdcard/PLIG_Backup")
        {
            string command = null;
            //force close

            //create device_backup_path
            command = ExecuteCMDTask("adb -s " + deviceID + " shell mkdir " + device_backup_path);

            //remove all contents under folder
            command = ExecuteCMDTask("adb -s " + deviceID + " shell rm -r " + device_backup_path + "/*");

            //zip folder /data/data/packageName to device_backup_path
            string backup_name = "instagram_" + PLAutoHelper.NormalFuncHelper.RandomGUID(4) + "_bkpl.tar.gz";

            //remove lib-zstd

            command = ExecuteCMDTask("adb -s " + deviceID + " shell rm -r " + "/data/data/" + packageName + "/lib-zstd/*");

            command = ExecuteCMDTask("adb -s " + deviceID + " shell tar -czvf " + device_backup_path + "/" + backup_name + " -C /data/data/" + packageName + " .", 20);

            //get sum remote
            var md5sum_remote = CheckSum_OnDevice(deviceID, device_backup_path + "/" + backup_name);

            //pull to local
            var temp_local_backup = PLAutoHelper.NormalFuncHelper.OptimizePathEndBackSlash(Path.GetFullPath("Temp")) + backup_name;

            if (File.Exists(temp_local_backup))
                File.Delete(temp_local_backup);

            command = ExecuteCMDTask("adb -s \"" + deviceID + "\" pull " + device_backup_path + "/" + backup_name + " \"" + temp_local_backup + "\"", 50);

            //get sum local
            var md5sum_local = CheckSum_PC(temp_local_backup);


            //compare sum
            if (md5sum_remote != md5sum_local)
            {
                throw new System.ArgumentException("Error when pull backup file from server");
            }
            else
            {
                ADBHelpers.CreatePath(PLAutoHelper.NormalFuncHelper.OptimizePathEndBackSlash(local_folder_path));
                File.Copy(temp_local_backup, PLAutoHelper.NormalFuncHelper.OptimizePathEndBackSlash(local_folder_path) + backup_name, true);

                Thread.Sleep(2000);

                //remove temp
                File.Delete(temp_local_backup);
            }
        }

        static public void RestoreByPL(string deviceID, string packageName, string backup_gz_path, bool clear_cache = true, string device_backup_path = "/sdcard/PLIG_Backup")
        {
            string command = null;
            //force close
            ExecuteCMDTask("adb -s " + deviceID + " shell am force-stop " + packageName);


            //create device_backup_path
            command = ExecuteCMDTask("adb -s " + deviceID + " shell mkdir " + device_backup_path);

            //remove all contents under folder
            command = ExecuteCMDTask("adb -s " + deviceID + " shell rm -r " + device_backup_path + "/*");

            //upload backup to device_backup_path
            command = ADBHelpers.ExecuteCMDTask("adb -s \"" + deviceID + "\" push \"" + backup_gz_path + "\" " + device_backup_path + "/" + Path.GetFileName(backup_gz_path), 50);

            //check sum


            //get sum local
            var md5sum_local = CheckSum_PC(backup_gz_path);

            var md5sum_remote = CheckSum_OnDevice(deviceID, device_backup_path + "/" + Path.GetFileName(backup_gz_path));

            //compare sum
            if (md5sum_remote != md5sum_local)
            {
                throw new System.ArgumentException("Error when push backup file from server");
            }

            //if match sum, extract to /data/data/packageName

            ExecuteCMDTask("adb -s " + deviceID + " shell tar -xzvf " + device_backup_path + "/" + Path.GetFileName(backup_gz_path) + " -C /data/data/" + packageName);

            //remove folder cache if clear_cache=true
            if (clear_cache)
            {
                //remove folder cache
                command = ExecuteCMDTask("adb -s " + deviceID + " shell rm -r " + "/data/data/" + packageName + "/cache/*");
            }

        }

        static public void BackupByPL_FTPServer(string deviceID, string packageName, string ftp_server_path, string device_backup_path = "/sdcard/PLIG_Backup")
        {
            string command = null;
            //force close

            //create device_backup_path
            command = ExecuteCMDTask("adb -s " + deviceID + " shell mkdir " + device_backup_path);

            //remove all contents under folder
            command = ExecuteCMDTask("adb -s " + deviceID + " shell rm -r " + device_backup_path + "/*");

            //zip folder /data/data/packageName to device_backup_path
            string backup_name = "instagram_" + PLAutoHelper.NormalFuncHelper.RandomGUID(4) + "_bkpl.tar.gz";

            //remove lib-zstd

            command = ExecuteCMDTask("adb -s " + deviceID + " shell rm -r " + "/data/data/" + packageName + "/lib-zstd/*");

            command = ExecuteCMDTask("adb -s " + deviceID + " shell tar -czvf " + device_backup_path + "/" + backup_name + " -C /data/data/" + packageName + " .", 20);

            //get sum remote
            var md5sum_remote = CheckSum_OnDevice(deviceID, device_backup_path + "/" + backup_name);

            //pull to local
            var temp_local_backup = PLAutoHelper.NormalFuncHelper.OptimizePathEndBackSlash(Path.GetFullPath("Temp")) + backup_name;

            if (File.Exists(temp_local_backup))
                File.Delete(temp_local_backup);

            command = ExecuteCMDTask("adb -s \"" + deviceID + "\" pull " + device_backup_path + "/" + backup_name + " \"" + temp_local_backup + "\"", 50);

            //get sum local
            var md5sum_local = CheckSum_PC(temp_local_backup);


            //compare sum
            if (md5sum_remote != md5sum_local)
            {
                throw new System.ArgumentException("Error when pull backup file from server");
            }
            else
            {
                if (!PLAutoHelper.FTPHelper.FTP_IsDirectoryExisted(LDHelpers.pLFTP, ftp_server_path))
                    PLAutoHelper.FTPHelper.FTP_CreateDirectory(LDHelpers.pLFTP, ftp_server_path);

                PLAutoHelper.FTPHelper.FTP_UploadFile(LDHelpers.pLFTP, temp_local_backup, ftp_server_path, false);

                Thread.Sleep(2000);

                //remove temp
                File.Delete(temp_local_backup);
            }
        }

        static public void RestoreByPL_FTPServer(string deviceID, string packageName, string ftp_server_backup_gz_path, bool clear_cache = true, string device_backup_path = "/sdcard/PLIG_Backup")
        {
            string command = null;
            //force close
            ExecuteCMDTask("adb -s " + deviceID + " shell am force-stop " + packageName);


            //create device_backup_path
            command = ExecuteCMDTask("adb -s " + deviceID + " shell mkdir " + device_backup_path);

            //remove all contents under folder
            command = ExecuteCMDTask("adb -s " + deviceID + " shell rm -r " + device_backup_path + "/*");

            string local_backup_gz_path= PLAutoHelper.NormalFuncHelper.OptimizePathEndBackSlash(Path.GetFullPath("Temp")) + Path.GetFileName(ftp_server_backup_gz_path);

            PLAutoHelper.FTPHelper.FTP_DownloadFile(LDHelpers.pLFTP, ftp_server_backup_gz_path, local_backup_gz_path);

            //upload backup to device_backup_path
            command = ADBHelpers.ExecuteCMDTask("adb -s \"" + deviceID + "\" push \"" + local_backup_gz_path + "\" " + device_backup_path + "/" + Path.GetFileName(local_backup_gz_path), 50);

            //check sum


            //get sum local
            var md5sum_local = CheckSum_PC(local_backup_gz_path);

            var md5sum_remote = CheckSum_OnDevice(deviceID, device_backup_path + "/" + Path.GetFileName(local_backup_gz_path));

            //compare sum
            if (md5sum_remote != md5sum_local)
            {
                throw new System.ArgumentException("Error when push backup file from server");
            }

            //if match sum, extract to /data/data/packageName

            ExecuteCMDTask("adb -s " + deviceID + " shell tar -xzvf " + device_backup_path + "/" + Path.GetFileName(local_backup_gz_path) + " -C /data/data/" + packageName);

            //remove folder cache if clear_cache=true
            if (clear_cache)
            {
                //remove folder cache
                command = ExecuteCMDTask("adb -s " + deviceID + " shell rm -r " + "/data/data/" + packageName + "/cache/*");
            }
        }

        static public string CheckSum_OnDevice(string deviceID, string path)
        {
            var command = ExecuteCMDTask("adb -s " + deviceID + " shell md5sum " + path);

            //read all lines from return command

            string sum_str = null;
            StringReader strReader = new StringReader(command);
            while (true)
            {
                string line = strReader.ReadLine();
                if (line != null)
                {
                    if (line.Contains(path) && !line.Contains("adb -s"))
                    {
                        sum_str = line;
                        break;
                    }
                }
                else
                {
                    break;
                }
            }

            var md5sum = sum_str.Split(' ')[0];

            return md5sum;
        }

        static public string CheckSum_PC(string path_to_file)
        {
            var command = ExecuteCMDTask("CertUtil -hashfile " + path_to_file + " MD5");
            
            if(command.Contains("ERROR_FILE_INVALID") || command.Contains("The process cannot access the file because it is being used by another process"))
            {
                return null;
            }

            string md5sum = null;
            StringReader strReader = new StringReader(command);
            while (true)
            {
                string line = strReader.ReadLine();
                if (line != null)
                {
                    if (line.Contains("MD5 hash of"))
                    {
                        md5sum = strReader.ReadLine();
                        md5sum = md5sum.Replace(" ", "");
                        break;
                    }
                }
            }

            return md5sum;
        }

        #endregion

        static public void Delay(double seconds_from = 2, double seconds_to = 5)
        {
            Thread.Sleep((new Random()).Next((int)seconds_from * 1000, (int)seconds_to * 1000));
        }

        static public object GetDynamicProperty(object obj, string name)
        {
            return obj.GetType().GetProperty(name).GetValue(obj, null);
        }

        static public void KillAllADBProcesses()
        {

            //close all adb
            var processes = Process.GetProcessesByName("adb");

            foreach (Process p in processes)
            {
                try
                {
                    string fullpath = p.MainModule.FileName;
                    //if (fullpath.Contains(ADBSupport.LDHelpers.LDPlayer_path))
                    //    p.Kill();
                    p.Kill();
                }
                catch
                { }
            }
        }

    }
}
