﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramLDPlayer.ADBSupport
{
    static class BMPSource
    {

        #region --Global--

        static public Bitmap Global_ClickHasStopped_Ok = BMResource.Global.Global_ClickHasStopped_Ok;
        static public Bitmap Global_Goto_EditProfileScreen = BMResource.Global.Global_Goto_EditProfileScreen;
        static public Bitmap Global_Goto_PostScreenBtn = BMResource.Global.Global_Goto_PostScreenBtn;
        static public Bitmap Global_GoTo_ProfileScreen = BMResource.Global.Global_GoTo_ProfileScreen;
        static public Bitmap Global_isOnTopHome = BMResource.Global.Global_isOnTopHome;
        static public Bitmap Global_NewPosts = BMResource.Global.Global_NewPosts;
        static public Bitmap Global_WaitDeviceOpen = BMResource.Global.Global_WaitDeviceOpen;
        static public Bitmap Global_WaitEditProfileScreen = BMResource.Global.Global_WaitEditProfileScreen;
        static public Bitmap Global_WaitHome = BMResource.Global.Global_WaitHome;
        static public Bitmap Global_WaitProfileScreen = BMResource.Global.Global_WaitProfileScreen;
        static public Bitmap Global_WaitSearchTab = BMResource.Global.Global_WaitSearchTab;
        static public Bitmap Global_IconApp = BMResource.Global.Global_IG_Icon;
        static public Bitmap Global_LoadNewPosts = BMResource.Global.Global_NewPosts;

        #endregion

        #region --ErrorScreen--

        static public Bitmap ErrorScreen_UnsualActivity = BMResource.ErrorScreen.ErrorScreen_UnsualActivity;
        static public Bitmap ErrorScreen_PostRemoved = BMResource.ErrorScreen.ErrorScreen_PostRemoved;
        static public Bitmap ErrorScreen_StoryRemoved = BMResource.ErrorScreen.ErrorScreen_StoryRemoved;
        static public Bitmap ErrorScreen_PostRemoved_OK = BMResource.ErrorScreen.ErrorScreen_PostRemoved_OK;
        static public Bitmap ErrorScreen_TemporarilyLocked = BMResource.ErrorScreen.ErrorScreen_TemporarilyLocked;
        static public Bitmap ErrorScreen_VerifyYourIdentity = BMResource.ErrorScreen.ErrorScreen_VerifyYourIdentity;
        static public Bitmap ErrorScreen_NextBtn = BMResource.ErrorScreen.ErrorScreen_NextBtn;
        static public Bitmap ErrorScreen_YourAccountMayDeleted = BMResource.ErrorScreen.ErrorScreen_YourAccountMayDeleted;
        static public Bitmap ErrorScreen_Ok_1 = BMResource.ErrorScreen.ErrorScreen_Ok_1;
        static public Bitmap ErrorScreen_YourAccountMayDeleted_1 = BMResource.ErrorScreen.ErrorScreen_YourAccountMayDeleted_1;


        #endregion

        #region --ProxyDroid--


        static public Bitmap ProxyOff = BMResource.ProxyDroid.ProxyDroid_ProxyOff;
        static public Bitmap ProxyOn = BMResource.ProxyDroid.ProxyDroid_ProxyOn;
        static public Bitmap ProxyDroidPort = BMResource.ProxyDroid.ProxyDroid_ProxyDroidPort;
        static public Bitmap PortPopupEdit = BMResource.ProxyDroid.ProxyDroid_PortPopupEdit;
        static public Bitmap ClearedPort = BMResource.ProxyDroid.ProxyDroid_ClearedPort;
        static public Bitmap EditPortOK = BMResource.ProxyDroid.ProxyDroid_EditPortOK;
        static public Bitmap Socks5Option = BMResource.ProxyDroid.ProxyDroid_Socks5Option;
        static public Bitmap ProxyType = BMResource.ProxyDroid.ProxyDroid_ProxyType;
        static public Bitmap VerifySetSocks5 = BMResource.ProxyDroid.ProxyDroid_VerifySetSocks5;
        static public Bitmap ProxyTypeTitle = BMResource.ProxyDroid.ProxyDroid_ProxyTypeTitle;
        static public Bitmap ProxySwitchTitle = BMResource.ProxyDroid.ProxyDroid_ProxySwitchTitle;

        #endregion

        #region --Register Instagram

        static public Bitmap CreateNewAccount = BMResource.Register.Register_IG_Button_CreateNewAccount;
        static public Bitmap Wait_FillPhoneScreen = BMResource.Register.Register_Wait_FillPhoneScreen;
        static public Bitmap Next = BMResource.Register.Register_IG_Button_Next;
        static public Bitmap Option84 = BMResource.Register.Register_IG_Option_84VN;
        static public Bitmap WaitConfirmCodeBox = BMResource.Register.Register_IG_Text_WaitConfirmCodeBox;
        static public Bitmap WaitNamePass = BMResource.Register.Register_IG_Text_WaitNamePass;
        static public Bitmap WaitBirthday = BMResource.Register.Register_IG_Text_WaitBirthday;
        static public Bitmap FollowButton = BMResource.Register.Register_FollowButton;
        static public Bitmap WaitSuggestion = BMResource.Register.Register_WaitSuggestion;
        static public Bitmap SuggestionArrow = BMResource.Register.Register_SuggestionArrow;
        static public Bitmap Profile = BMResource.Register.Register_Profile;
        static public Bitmap WaitPrivacy = BMResource.Register.Register_IG_WaitPrivacy;
        static public Bitmap WaitFindFBFriends = BMResource.Register.Register_IG_WaitFindFBFriends;
        static public Bitmap WaitAddPhoto = BMResource.Register.Register_IG_WaitAddPhoto;
        static public Bitmap WaitGoBackFillPhone = BMResource.Register.Register_WaitGoBackFillPhone;


        //---Verify Human---

        static public Bitmap VerifyHuman = BMResource.Register.Register_ErrorScreen_VerifyHuman;
        static public Bitmap AddPhoneNumber = BMResource.Register.Register_ErrorScreen_AddPhoneNumber;
        static public Bitmap SendConfirmation = BMResource.Register.Register_ErrorScreen_SendConfirmation;
        static public Bitmap EnterCode = BMResource.Register.Register_ErrorScreen_EnterCode;
        static public Bitmap SubmitCode = BMResource.Register.Register_ErrorScreen_SubmitCode;
        static public Bitmap WelcomeToIG = BMResource.Register.Register_ErrorScreen_WelcomeToIG;
        static public Bitmap NextBtn = BMResource.Register.Register_ErrorScreen_NextBtn;
        static public Bitmap BackToInstagram = BMResource.Register.Register_ErrorScreen_BackToInstagram;

        #endregion

        #region --UserName


        static public Bitmap User_ProfileButton = BMResource.User.User_ProfileButton;
        static public Bitmap User_WaitEditProfile = BMResource.User.User_WaitEditProfile;
        static public Bitmap User_WaitUserNameLabel = BMResource.User.User_WaitUserNameLabel;

        #endregion

        #region --UpdateBio--


        static public Bitmap SwitchAccount = BMResource.UpdateBio.UpdateBio_ClickSwitchAccount;
        static public Bitmap WaitBioInput = BMResource.UpdateBio.UpdateBio_WaitBioInput;
        static public Bitmap ClickUpdateBio = BMResource.UpdateBio.UpdateBio_ClickUpdateBio;
        static public Bitmap EmptyBio = BMResource.UpdateBio.UpdateBio_EmptyBio;
        static public Bitmap BioSelectAll = BMResource.UpdateBio.UpdateBio_BioSelectAll;
        static public Bitmap BioPaste = BMResource.UpdateBio.UpdateBio_BioPaste;
        static public Bitmap WaitHtmlViewer = BMResource.UpdateBio.UpdateBio_WaitHtmlViewer;


        #endregion

        #region --UpdateProfileImage--

        static public Bitmap ClickChangeProfilePhoto = BMResource.UpdateProfileImage.UpdateProfileImage_ClickChangeProfilePhoto;
        static public Bitmap ClickNewProfilePhoto = BMResource.UpdateProfileImage.UpdateProfileImage_ClickNewProfilePhoto;
        static public Bitmap WaitGallery = BMResource.UpdateProfileImage.UpdateProfileImage_WaitGallery;
        static public Bitmap SaveImageProfile = BMResource.UpdateProfileImage.UpdateProfileImage_SaveImage;
        static public Bitmap StartingDownload = BMResource.UpdateProfileImage.UpdateProfileImage_StartingDownload;

        #endregion

        #region--Device masker--

        static public Bitmap CurrentNetworkCountry = BMResource.DeviceMasker.DeviceMasker_Wait_CurrentNetworkCountry;
        static public Bitmap CurrentNetworkType = BMResource.DeviceMasker.DeviceMasker_Wait_CurrentNetworkType;
        static public Bitmap CurrentGoogleEmail = BMResource.DeviceMasker.DeviceMasker_Wait_CurrentGoogleEmail;
        static public Bitmap RandomBtn = BMResource.DeviceMasker.DeviceMasker_RandomBtn;
        static public Bitmap ApplyAll = BMResource.DeviceMasker.DeviceMasker_ApplyAll;
        static public Bitmap Reboot = BMResource.DeviceMasker.DeviceMasker_Reboot;
        static public Bitmap ClickSettings = BMResource.DeviceMasker.DeviceMasker_ClickSettings;
        static public Bitmap ImportSettings = BMResource.DeviceMasker.DeviceMasker_ImportSettings;
        static public Bitmap WaitSettingsPage = BMResource.DeviceMasker.DeviceMasker_WaitSettingsPage;
        static public Bitmap DeviceIDMaskerLite = BMResource.DeviceMasker.DeviceMasker_DeviceIDMaskerLite;
        static public Bitmap ClickSettingButton = BMResource.DeviceMasker.DeviceMasker_ClickSettingButton;

        #endregion

        #region --PostEngage--

        #region --Post--

        static public Bitmap Post_DropDownMenu = BMResource.Post.Post_DropDownMenu;
        static public Bitmap LikeButton = BMResource.Post.Post_LikeButton;
        static public Bitmap Liked_Button = BMResource.Post.Post_Liked_Button;
        static public Bitmap CommentButton = BMResource.Post.Post_CommentButton;
        static public Bitmap SaveCollectionButton = BMResource.Post.Post_SaveCollectionButton;
        static public Bitmap ViewCommentLink = BMResource.Post.Post_ViewCommentLink;
        static public Bitmap MoreCaption = BMResource.Post.Post_MoreCaption;
        static public Bitmap AddACommentBox = BMResource.Post.Post_AddACommentBox;
        static public Bitmap AddCommentButton = BMResource.Post.Post_AddCommentButton;
        static public Bitmap Post_TimeAgo = BMResource.Post.Post_TimeAgo;

        static public Bitmap WaitViewPostPage = BMResource.Post.Post_WaitViewPostPage;

        static public Bitmap ExploreTab = BMResource.Post.Post_ExploreTab;
        static public Bitmap NextDropDown = BMResource.Post.Post_NextDropDown;

        #endregion


        #region --SwipePost--

        //Swipe post

        static public Bitmap SwipeStartDot = BMResource.Post.Post_SwipeHorizontal_SwipeStartDot;
        static public Bitmap SwipeLastDot = BMResource.Post.Post_SwipeHorizontal_SwipeLastDot;

        #endregion


        #region --Comment--

        //Comment

        static public Bitmap LikeCommentBtn = BMResource.Post.Post_Comment_LikeCommentBtn;
        static public Bitmap ClickPost = BMResource.Post.Post_Comment_ClickPost;
        static public Bitmap PasteBtn = BMResource.Post.Post_Comment_PasteBtn;
        static public Bitmap ViewReplies = BMResource.Post.Post_Comment_ViewReplies;
        static public Bitmap ViewPreviousReplies = BMResource.Post.Post_Comment_ViewPreviousReplies;
        static public Bitmap SelectAllEsEditor = BMResource.Post.Post_Comment_SelectAllEsEditor;
        static public Bitmap TapSettingEsEditor = BMResource.Post.Post_Comment_TapSettingEsEditor;
        static public Bitmap EditEsEditor = BMResource.Post.Post_Comment_EditEsEditor;
        static public Bitmap CopyEsEditor = BMResource.Post.Post_Comment_CopyEsEditor;
        static public Bitmap WaitEsEditor = BMResource.Post.Post_Comment_WaitEsEditor;
        static public Bitmap CopyLink = BMResource.Post.Post_Comment_CopyLink;
        static public Bitmap WaitLoading_CopyLink = BMResource.Post.Post_Comment_WaitLoading_CopyLink;
        static public Bitmap ConfirmYesEsEditor = BMResource.Post.Post_Comment_ConfirmYesEsEditor;
        static public Bitmap BackEsEditor = BMResource.Post.Post_Comment_BackEsEditor;
        static public Bitmap HtmlViewerOpenTxt = BMResource.Post.Post_Comment_HtmlViewerOpenTxt;
        static public Bitmap HtmlViewerSelect = BMResource.Post.Post_Comment_HtmlViewerSelect;
        static public Bitmap HtmlViewerCopy = BMResource.Post.Post_Comment_HtmlViewerCopy;

        #endregion


        //Video Dark Feed

        static public Bitmap ExploreVideoFeed = BMResource.Explore.Explore_ExploreVideoFeed;
        static public Bitmap LikeBtn_Dark = BMResource.Explore.Explore_LikeBtn_Dark;

        #endregion

        #region --Swipe Suggested For You--

        static public Bitmap SuggestedForYou = BMResource.SuggestedForYou.SuggestedForYou_SuggestedForYou;
        static public Bitmap Suggested_Follow = BMResource.SuggestedForYou.SuggestedForYou_FollowBtn;


        #endregion

        #region --Swipe Stories--

        static public Bitmap StoryElement = BMResource.SimulateStories.SimulateStories_StoryElement;
        static public Bitmap StoryElementViewed = BMResource.SimulateStories.SimulateStories_StoryElementViewed;
        static public Bitmap YourStory = BMResource.SimulateStories.SimulateStories_YourStory;
        static public Bitmap DirectMess = BMResource.SimulateStories.SimulateStories_DirectMess;

        #endregion

        #region --Search tab--

        static public Bitmap SearchBox = BMResource.SearchTab.SearchTab_SearchBox;
        static public Bitmap Tab_Accounts = BMResource.SearchTab.SearchTab_Tab_Accounts;
        static public Bitmap Tab_Tags = BMResource.SearchTab.SearchTab_Tab_Tags;
        static public Bitmap Tab_Places = BMResource.SearchTab.SearchTab_Tab_Places;
        static public Bitmap Tab_Top = BMResource.SearchTab.SearchTab_Tab_Top;
        static public Bitmap BlueTick = BMResource.SearchTab.SearchTab_BlueTick;

        #endregion

        #region --Follow Action--

        static public Bitmap Follow1Btn = BMResource.FollowPeople.FollowPeople_Follow1Btn;
        static public Bitmap Follow2Btn = BMResource.FollowPeople.FollowPeople_Follow2Btn;
        static public Bitmap Following_UserNotFound = BMResource.FollowPeople.FollowPeople_UserNotFound;


        #endregion

        #region --Publish--

        static public Bitmap SaveImage = BMResource.Publish.Publish_SaveImage;
        static public Bitmap AddPost = BMResource.Publish.Publish_AddPost;
        static public Bitmap LightBtn = BMResource.Publish.Publish_LightBtn;
        static public Bitmap WaitNewPostEdit = BMResource.Publish.Publish_WaitNewPostEdit;
        static public Bitmap WriteACaption = BMResource.Publish.Publish_WriteACaption;
        static public Bitmap ShareBtn = BMResource.Publish.Publish_ShareBtn;
        static public Bitmap WaitFinishingUp = BMResource.Publish.Publish_WaitFinishingUp;
        static public Bitmap Publish_NextBtn = BMResource.Publish.Publish_Publish_NextBtn;
        static public Bitmap ReloadImage = BMResource.Publish.Publish_ReloadImage;
        static public Bitmap MultiClicked = BMResource.Publish.Publish_MultiClicked;
        static public Bitmap PublishPost_BackBtn = BMResource.Publish.Publish_BackBtn;



        #endregion

        #region --Titanium Backup--

        static public Bitmap TitaniumBackupAddOn = BMResource.TitaniumBackup.TitaniumBackup_TitaniumBackupAddOn;
        static public Bitmap CancelAddOn = BMResource.TitaniumBackup.TitaniumBackup_CancelAddOn;
        static public Bitmap Backup_Restore = BMResource.TitaniumBackup.TitaniumBackup_Backup_Restore;
        static public Bitmap InstagramBackup = BMResource.TitaniumBackup.TitaniumBackup_Instagram;
        static public Bitmap WaitPopUp = BMResource.TitaniumBackup.TitaniumBackup_WaitPopUp;
        static public Bitmap ExecRestore = BMResource.TitaniumBackup.TitaniumBackup_ExecRestore;
        static public Bitmap App_Data = BMResource.TitaniumBackup.TitaniumBackup_App_Data;
        static public Bitmap DataOnly = BMResource.TitaniumBackup.TitaniumBackup_DataOnly;
        static public Bitmap AskInstallUpdate = BMResource.TitaniumBackup.TitaniumBackup_AskInstallUpdate;
        static public Bitmap FinishRestore = BMResource.TitaniumBackup.TitaniumBackup_FinishRestore;
        static public Bitmap BackupTitanium = BMResource.TitaniumBackup.TitaniumBackup_Backup;
        static public Bitmap WaitBackingUp = BMResource.TitaniumBackup.TitaniumBackup_WaitBackingUp;
        static public Bitmap TitaniumIcon = BMResource.TitaniumBackup.TitaniumBackup_TitaniumIcon;
        static public Bitmap Titanium_IgnoreKeepNewId = BMResource.TitaniumBackup.TitaniumBackup_IgnoreKeepNewId;
        static public Bitmap Titanium_Ok_IgnoreKeepNewId = BMResource.TitaniumBackup.TitaniumBackup_Ok_IgnoreKeepNewId;

        static public Bitmap Remember_choice_forever = BMResource.TitaniumBackup.TitaniumBackup_Remember_choice_forever;
        static public Bitmap AllowRoot = BMResource.TitaniumBackup.TitaniumBackup_AllowRoot;
        static public Bitmap First_TitaniumAddOn = BMResource.TitaniumBackup.TitaniumBackup_First_TitaniumAddOn;
        static public Bitmap FirstStart_Ok = BMResource.TitaniumBackup.TitaniumBackup_FirstStart_Ok;
        static public Bitmap First_ChangeLog = BMResource.TitaniumBackup.TitaniumBackup_First_ChangeLog;
        static public Bitmap First_Warning = BMResource.TitaniumBackup.TitaniumBackup_First_Warning;
        static public Bitmap First_Allow = BMResource.TitaniumBackup.TitaniumBackup_First_Allow;

        static public Bitmap First_EditFilter = BMResource.TitaniumBackup.TitaniumBackup_First_EditFilter;
        static public Bitmap First_CreateLabel = BMResource.TitaniumBackup.TitaniumBackup_First_CreateLabel;
        static public Bitmap First_WaitLabelPopup = BMResource.TitaniumBackup.TitaniumBackup_First_WaitLabelPopup;
        static public Bitmap First_WaitClearBox = BMResource.TitaniumBackup.TitaniumBackup_First_WaitClearBox;
        static public Bitmap First_Instagram138 = BMResource.TitaniumBackup.TitaniumBackup_First_Instagram138;
        static public Bitmap AddRemoveElements = BMResource.TitaniumBackup.TitaniumBackup_AddRemoveElements;


        #endregion


        #region --Privacy--

        static public Bitmap AccountPrivacy = BMResource.AccountPrivacy.AccountPrivacy_AccountPrivacy;
        static public Bitmap WaitPrivateAccount = BMResource.AccountPrivacy.AccountPrivacy_WaitPrivateAccount;
        static public Bitmap PrivateOff = BMResource.AccountPrivacy.AccountPrivacy_PrivateOff;
        static public Bitmap PrivateOn = BMResource.AccountPrivacy.AccountPrivacy_PrivateOn;

        static public Bitmap PrivateOff_1 = BMResource.AccountPrivacy.AccountPrivacy_PrivateOff_1;
        static public Bitmap PrivateOn_1 = BMResource.AccountPrivacy.AccountPrivacy_PrivateOn_1;

        static public Bitmap OkPopup = BMResource.AccountPrivacy.AccountPrivacy_OkPopup;
        static public Bitmap SettingBtn = BMResource.AccountPrivacy.AccountPrivacy_SettingBtn;
        static public Bitmap SettingsText = BMResource.AccountPrivacy.AccountPrivacy_SettingsText;
        static public Bitmap PrivacyMenu = BMResource.AccountPrivacy.AccountPrivacy_PrivacyMenu;
        #endregion


        #region --Es Editor--
        static public Bitmap Es_Menu = BMResource.EsEditor.EsEditor_MenuBtn;
        static public Bitmap Es_Edit = BMResource.EsEditor.EsEditor_Edit;
        static public Bitmap Es_SelectAll = BMResource.EsEditor.EsEditor_SelectAll;
        static public Bitmap Es_Copy = BMResource.EsEditor.EsEditor_Copy;
        static public Bitmap Es_Encoding = BMResource.EsEditor.EsEditor_Encoding;
        static public Bitmap Es_UTF8 = BMResource.EsEditor.EsEditor_UTF8;


        #endregion

        #region --ACTION BLOCK--

        static public Bitmap ActionBlocked = BMResource.ActionBlocked.ActionBlocked_ActionBlocked;
        static public Bitmap TellUs = BMResource.ActionBlocked.ActionBlocked_TellUs;
        static public Bitmap RestrictActivity = BMResource.ActionBlocked.ActionBlocked_RestrictActivity;

        #endregion


        #region --Sponsored--

        static public Bitmap Sponsored_SponsoredText = BMResource.Sponsored.Sponsored_SponsoredText;
        static public Bitmap Sponsored_LoadWebsiteX = BMResource.Sponsored.Sponsored_LoadWebsiteX;
        static public Bitmap Sponsored_LoadProfileBack = BMResource.Sponsored.Sponsored_LoadProfileBack;

        #endregion

        #region --SelfComment--

        static public Bitmap SelfComment_EditProfile = BMResource.SelfComment.SelfComment_EditProfileBtn;
        static public Bitmap SelfComment_CommentBtn = BMResource.SelfComment.SelfComment_CommentBtn;
        static public Bitmap SelfComment_AddACommentBox = BMResource.SelfComment.SelfComment_AddACommentBox;
        static public Bitmap SelfComment_PostComment = BMResource.SelfComment.SelfComment_PostComment;

        #endregion


        #region --LikeUserPostTask--


        static public Bitmap LikeUserPost_followbtn = BMResource.LikeUserPostTask.LikeUserPostTask_followbtn;
        static public Bitmap LikeUserPost_PostTabIcon = BMResource.LikeUserPostTask.LikeUserPostTask_PostTabIcon;
        static public Bitmap LikeUserPost_WaitPosts = BMResource.LikeUserPostTask.LikeUserPostTask_WaitPosts;



        #endregion

        #region --Login To Instagram--

        static public Bitmap Login_LoginBtn = BMResource.LogIn.LogIn_LoginBtn;
        static public Bitmap Login_PhoneEmailUser = BMResource.LogIn.LogIn_PhoneEmailUser;
        static public Bitmap Login_Password = BMResource.LogIn.LogIn_Password;
        static public Bitmap Login_LoginBtnBlue = BMResource.LogIn.LogIn_LoginBtnBlue;
        static public Bitmap Login_Disabled = BMResource.LogIn.LogIn_Disabled;
        static public Bitmap Login_ThisWasMe = BMResource.LogIn.LogIn_ThisWasMe;
        static public Bitmap Login_ErrorRequest = BMResource.LogIn.LogIn_ErrorRequest;
        static public Bitmap Login_AccountIsDisabled = BMResource.LogIn.LogIn_AccountIsDisabled;

        #endregion

        #region --Install IG--

        static public Bitmap InstallIG_InstagramAPK = BMResource.Install_IG.Install_IG_InstagramAPK;
        static public Bitmap InstallIG_InstallBtn = BMResource.Install_IG.Install_IG_InstallBtn;
        static public Bitmap InstallIG_Next = BMResource.Install_IG.Install_IG_Next;
        static public Bitmap InstallIG_ConfirmInstall = BMResource.Install_IG.Install_IG_ConfirmInstall;
        static public Bitmap InstallIG_Installing = BMResource.Install_IG.Install_IG_Installing;
        static public Bitmap InstallIG_AppInstalled = BMResource.Install_IG.Install_IG_AppInstalled;
        static public Bitmap InstallIG_OpenIG = BMResource.Install_IG.Install_IG_OpenIG;
        static public Bitmap InstallIG_EsFileAskUpdate = BMResource.Install_IG.Install_IG_EsFileAskUpdate;
        static public Bitmap InstallIG_EsFileAskUpdate_Cancel = BMResource.Install_IG.Install_IG_EsFileAskUpdate_Cancel;

        #endregion

        #region --Rating--
        static public Bitmap Rating_NoThanksOption = BMResource.Rating.Rating_NoThanksOption;

        #endregion


        #region --Read Notification--

        static public Bitmap ReadNotice_NoticationAlert = BMResource.ReadNotification.ReadNotification_NoticationAlert;
        static public Bitmap ReadNotice_Activity = BMResource.ReadNotification.ReadNotification_Activity;
        static public Bitmap ReadNotice_HeartBtn = BMResource.ReadNotification.ReadNotification_HeartBtn;

        #endregion

        #region --SimulateStoriesTask--

        static public Bitmap SimulateStoriesTask_Stories_Round = BMResource.SimulateStoriesTask.SimulateStoriesTask_Stories_Round;

        static public Bitmap SimulateStoriesTask_Stories_End = BMResource.SimulateStoriesTask.SimulateStoriesTask_Stories_End;

        static public Bitmap SimulateStoriesTask_Tag_BackArrow = BMResource.SimulateStoriesTask.SimulateStoriesTask_Tag_BackArrow;

        #endregion


        #region --SemiAutoComment--

        static public Bitmap SemiAutoComment_AddACommentBox = BMResource.SemiAutoComment.SemiAutoComment_AddACommentBox;
        static public Bitmap SemiAutoComment_CommentBtn = BMResource.SemiAutoComment.SemiAutoComment_CommentBtn;
        static public Bitmap SemiAutoComment_PostComment = BMResource.SemiAutoComment.SemiAutoComment_PostComment;
        static public Bitmap SemiAutoComment_Tag_HeartBtn = BMResource.SemiAutoComment.SemiAutoComment_Tag_HeartBtn;
        static public Bitmap SemiAutoComment_Posting = BMResource.SemiAutoComment.SemiAutoComment_Posting;

        #endregion

        #region --Publish Stories--

        static public Bitmap PublishStories_TagCameraBtn = BMResource.PublishStories.PublishStories_TagCameraBtn;
        static public Bitmap PublishStories_AddToYourStory = BMResource.PublishStories.PublishStories_AddToYourStory;
        static public Bitmap PublishStories_ShootBtn = BMResource.PublishStories.PublishStories_ShootBtn;
        static public Bitmap PublishStories_Gallery = BMResource.PublishStories.PublishStories_Gallery;
        static public Bitmap PublishStories_TagSendToBtn = BMResource.PublishStories.PublishStories_TagSendToBtn;
        static public Bitmap PublishStories_AskStoriesArchive = BMResource.PublishStories.PublishStories_AskStoriesArchive;
        static public Bitmap PublishStories_OK_StoriesArchive = BMResource.PublishStories.PublishStories_OK_StoriesArchive;

        #endregion

        #region --AppInfo--

        static public Bitmap AppInfo_DataCleared = BMResource.AppInfo.AppInfo_DataCleared;
        static public Bitmap AppInfo_DataAvail = BMResource.AppInfo.AppInfo_DataAvail;

        #endregion

        #region --Mode Switch--

        static public Bitmap ModeSwitch_IGStopped = BMResource.ModeSwitch.ModeSwitch_IGStopped;

        #endregion

        #region --Login Instagram Manually Firefox--

        static public Bitmap LaunchManual_PhoneNumber = BMResource.LaunchManual.LaunchManual_PhoneNumber;
        static public Bitmap LaunchManual_Password = BMResource.LaunchManual.LaunchManual_Password;
        static public Bitmap LaunchManual_LoginBtn = BMResource.LaunchManual.LaunchManual_LoginBtn;

        #endregion

        #region --Message--


        static public Bitmap Message_NoMessage = BMResource.Message.Message_NoMessage;
        static public Bitmap Message_HasRequestMessage = BMResource.Message.Message_HasRequestMessage;
        static public Bitmap Message_TagInstagram = BMResource.Message.Message_TagInstagram;
        static public Bitmap Message_DirectMessage = BMResource.Message.Message_DirectMessage;
        static public Bitmap Message_TagCameraIcon = BMResource.Message.Message_TagCameraIcon;
        static public Bitmap Message_TagMessage = BMResource.Message.Message_TagMessage;
        static public Bitmap Message_TagRequests = BMResource.Message.Message_TagRequests;


        #endregion

        #region --Seeding--

        static public Bitmap Seeding_LikeBtn = BMResource.Seeding.Seeding_LikeBtn;
        static public Bitmap Seeding_PostOption = BMResource.Seeding.Seeding_PostOption;
        static public Bitmap Seeding_ViewComment = BMResource.Seeding.Seeding_ViewComment;
        static public Bitmap Seeding_NoMoreComment = BMResource.Seeding.Seeding_NoMoreComment;


        #endregion


        #region --Update Fullname--

        static public Bitmap UpdateFullname_TagTitleName = BMResource.UpdateFullname.UpdateFullname_TagTitleName;
        static public Bitmap UpdateFullname_SelectAllBtn = BMResource.UpdateFullname.UpdateFullname_SelectAllBtn;
        static public Bitmap UpdateFullname_EmptyFullname = BMResource.UpdateFullname.UpdateFullname_EmptyFullname;
        static public Bitmap UpdateFullname_UpdateProfileTick = BMResource.UpdateFullname.UpdateFullname_UpdateProfileTick;

        #endregion

        #region --Self Share Stories--


        static public Bitmap SelfStories_ShareStoriesBtn = BMResource.SelfStories.SelfStories_ShareStoriesBtn;
        static public Bitmap SelfStories_AddPostToYourStory = BMResource.SelfStories.SelfStories_AddPostToYourStory;
        static public Bitmap SelfStories_SendToBtn = BMResource.SelfStories.SelfStories_SendToBtn;

        #endregion

        #region --CaptureUsername--

        static public Bitmap CaptureUsername_TagChooseProfile = BMResource.CaptureUsername.CaptureUsername_TagChooseProfile;
        static public Bitmap CaptureUsername_EmptySpace = BMResource.CaptureUsername.CaptureUsername_EmptySpace;
        static public Bitmap CaptureUsername_TagSwitchAccount = BMResource.CaptureUsername.CaptureUsername_TagSwitchAccount;

        #endregion

        #region --IsAccountCorrect--

        static public Bitmap IsAccountCorrect_TagUsername = BMResource.IsAccountCorrect.IsAccountCorrect_TagUsername;
        static public Bitmap IsAccountCorrect_CopyBtn = BMResource.IsAccountCorrect.IsAccountCorrect_CopyBtn;
        static public Bitmap IsAccountCorrect_TagUsernameScreen = BMResource.IsAccountCorrect.IsAccountCorrect_TagUsernameScreen;
        static public Bitmap IsAccountCorrect_SelectAllBtn = BMResource.IsAccountCorrect.IsAccountCorrect_SelectAllBtn;

        #endregion

    }
}
