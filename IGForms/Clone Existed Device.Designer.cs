﻿namespace InstagramLDPlayer.IGForms
{
    partial class Clone_Existed_Device
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_devices = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_start_clone = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tb_devices
            // 
            this.tb_devices.Location = new System.Drawing.Point(12, 60);
            this.tb_devices.Multiline = true;
            this.tb_devices.Name = "tb_devices";
            this.tb_devices.Size = new System.Drawing.Size(434, 308);
            this.tb_devices.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Paste list devices below";
            // 
            // btn_start_clone
            // 
            this.btn_start_clone.Location = new System.Drawing.Point(183, 374);
            this.btn_start_clone.Name = "btn_start_clone";
            this.btn_start_clone.Size = new System.Drawing.Size(75, 23);
            this.btn_start_clone.TabIndex = 2;
            this.btn_start_clone.Text = "Start Cloning";
            this.btn_start_clone.UseVisualStyleBackColor = true;
            this.btn_start_clone.Click += new System.EventHandler(this.btn_start_clone_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label2.Location = new System.Drawing.Point(12, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(417, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nếu để trống, soft sẽ clone tiếp các device đã added từ trước và chưa clone thành" +
    " công";
            // 
            // Clone_Existed_Device
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_start_clone);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_devices);
            this.Name = "Clone_Existed_Device";
            this.Text = "Clone_Existed_Device";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_devices;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_start_clone;
        private System.Windows.Forms.Label label2;
    }
}