﻿namespace InstagramLDPlayer.IGForms
{
    partial class IGInputShopName
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tb_inputshopname_input = new System.Windows.Forms.TextBox();
            this.inputshopname_btn_import = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.inputshopname_cb_niches = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(297, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tips: dán list tên shop vào ô bên dưới, mỗi tên shop là 1 dòng";
            // 
            // tb_inputshopname_input
            // 
            this.tb_inputshopname_input.Location = new System.Drawing.Point(15, 48);
            this.tb_inputshopname_input.Multiline = true;
            this.tb_inputshopname_input.Name = "tb_inputshopname_input";
            this.tb_inputshopname_input.Size = new System.Drawing.Size(294, 342);
            this.tb_inputshopname_input.TabIndex = 1;
            // 
            // inputshopname_btn_import
            // 
            this.inputshopname_btn_import.Location = new System.Drawing.Point(120, 439);
            this.inputshopname_btn_import.Name = "inputshopname_btn_import";
            this.inputshopname_btn_import.Size = new System.Drawing.Size(75, 23);
            this.inputshopname_btn_import.TabIndex = 2;
            this.inputshopname_btn_import.Text = "Import";
            this.inputshopname_btn_import.UseVisualStyleBackColor = true;
            this.inputshopname_btn_import.Click += new System.EventHandler(this.inputshopname_btn_import_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 404);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Chủ đề";
            // 
            // inputshopname_cb_niches
            // 
            this.inputshopname_cb_niches.FormattingEnabled = true;
            this.inputshopname_cb_niches.Location = new System.Drawing.Point(60, 401);
            this.inputshopname_cb_niches.Name = "inputshopname_cb_niches";
            this.inputshopname_cb_niches.Size = new System.Drawing.Size(121, 21);
            this.inputshopname_cb_niches.TabIndex = 4;
            // 
            // IGInputShopName
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 474);
            this.Controls.Add(this.inputshopname_cb_niches);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.inputshopname_btn_import);
            this.Controls.Add(this.tb_inputshopname_input);
            this.Controls.Add(this.label1);
            this.Name = "IGInputShopName";
            this.Text = "Input Account Name";
            this.Load += new System.EventHandler(this.IGInputShopName_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_inputshopname_input;
        private System.Windows.Forms.Button inputshopname_btn_import;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox inputshopname_cb_niches;
    }
}