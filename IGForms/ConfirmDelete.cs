﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.IGForms
{
    public partial class ConfirmDelete : Form
    {
        public ConfirmDelete()
        {
            InitializeComponent();
        }

        private void confirmdelete_btn_ok_Click(object sender, EventArgs e)
        {
            if(confirmdelete_tb_delete.Text.ToLower()=="delete")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                confirmdelete_lb_error.Visible = true;
            }
        }

        private void confirmdelete_btn_cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
