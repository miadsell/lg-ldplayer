﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.IGForms
{
    public partial class Tool_Settings : Form
    {
        public Tool_Settings()
        {
            InitializeComponent();
            LoadSettingsToForm();
        }

        string pc_devices;

        private void LoadSettingsToForm()
        {
            pc_devices = IGHelpers.DBHelpers.CheckDevice();

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("select * from devices where device_id=" + pc_devices, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        dataReader.Read();

                        tool_settings_tb_name.Text = dataReader["ld_original_name"].ToString();
                        tool_settings_tb_ldplayer.Text = dataReader["ldplayer_path"].ToString();
                        tool_settings_tb_lddata.Text = dataReader["ldplayer_data_path"].ToString();
                        tool_settings_tb_threads.Text = dataReader["threads"].ToString();
                        tool_settings_tb_focus_engage.Text = dataReader["focus_engage_threads"].ToString();
                        tool_settings_tb_sleep_between_launch.Text = dataReader["sleep_between_launch"].ToString();
                        tb_ftp_username.Text = dataReader["ftp_username"].ToString();


                    }
                }
            }
        }

        private void tool_settings_btn_browse_ldplayer_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    tool_settings_tb_ldplayer.Text = fbd.SelectedPath;
                }
            }
        }

        private void tool_settings_btn_browse_lddata_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    tool_settings_tb_lddata.Text = fbd.SelectedPath;
                }
            }
        }

        private void tool_settings_btn_update_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();
                using (var command = new SqlCommand("update devices set ld_original_name='" + tool_settings_tb_name.Text + "', ldplayer_path='" + tool_settings_tb_ldplayer.Text + "', ldplayer_data_path='" + tool_settings_tb_lddata.Text + "', threads=" + tool_settings_tb_threads.Text + ", [focus_engage_threads]=" + tool_settings_tb_focus_engage.Text + ",[sleep_between_launch]=" + tool_settings_tb_sleep_between_launch.Text + ", [ftp_username]='" + tb_ftp_username.Text + "' where device_id=" + pc_devices, conn))
                {
                    command.ExecuteNonQuery();
                }
            }

            IGHelpers.DBHelpers.SettingUpLDConstants();

            MessageBox.Show("Cập nhật thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void tool_settings_btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_browse_proxifier_Click(object sender, EventArgs e)
        {
            using (var fd = new OpenFileDialog())
            {
                DialogResult result = fd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fd.FileName))
                {
                    tb_proxifier_path.Text = fd.FileName;
                }
            }
        }
    }
}
