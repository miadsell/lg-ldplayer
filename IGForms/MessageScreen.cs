﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.IGForms
{
    public partial class MessageScreen : Form
    {
        string message_base64;
        public MessageScreen(string message_base64)
        {
            this.message_base64 = message_base64;

            var base64_screens = message_base64.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            Bitmap message_screen = null, request_screen = null;

            InitializeComponent();

            if (base64_screens.Count == 1)
            {
                message_screen = Action.ActionHelpers.BitmapFromBase64Str(base64_screens[0]);
            }
            else
            {

                message_screen = Action.ActionHelpers.BitmapFromBase64Str(base64_screens[0]);

                request_screen = Action.ActionHelpers.BitmapFromBase64Str(base64_screens[1]);
            }


            if (message_screen != null)
            {
                pictureBox_message.Image = message_screen;
            }

            if (request_screen != null)
            {
                pictureBox_request.Image = request_screen;
            }

        }

        private void button_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
