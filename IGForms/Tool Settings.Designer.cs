﻿namespace InstagramLDPlayer.IGForms
{
    partial class Tool_Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tool_settings_tb_ldplayer = new System.Windows.Forms.TextBox();
            this.tool_settings_tb_lddata = new System.Windows.Forms.TextBox();
            this.tool_settings_btn_browse_ldplayer = new System.Windows.Forms.Button();
            this.tool_settings_btn_browse_lddata = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tool_settings_tb_threads = new System.Windows.Forms.TextBox();
            this.tool_settings_btn_update = new System.Windows.Forms.Button();
            this.tool_settings_btn_close = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tool_settings_tb_name = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tool_settings_tb_focus_engage = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label18 = new System.Windows.Forms.Label();
            this.tool_settings_tb_sleep_between_launch = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_browse_proxifier = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_proxifier_path = new System.Windows.Forms.TextBox();
            this.tb_manual_proxy = new System.Windows.Forms.TextBox();
            this.tb_switch_index = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.tb_launchff_pc_backup = new System.Windows.Forms.TextBox();
            this.tb_launchff_pc_profile_location = new System.Windows.Forms.TextBox();
            this.tb_launchff_pc_original = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label19 = new System.Windows.Forms.Label();
            this.tb_ftp_username = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Đường dẫn folder LDPlayer";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Đường dẫn folder LD Data";
            // 
            // tool_settings_tb_ldplayer
            // 
            this.tool_settings_tb_ldplayer.Location = new System.Drawing.Point(178, 60);
            this.tool_settings_tb_ldplayer.Name = "tool_settings_tb_ldplayer";
            this.tool_settings_tb_ldplayer.Size = new System.Drawing.Size(230, 20);
            this.tool_settings_tb_ldplayer.TabIndex = 2;
            // 
            // tool_settings_tb_lddata
            // 
            this.tool_settings_tb_lddata.Location = new System.Drawing.Point(178, 112);
            this.tool_settings_tb_lddata.Name = "tool_settings_tb_lddata";
            this.tool_settings_tb_lddata.Size = new System.Drawing.Size(230, 20);
            this.tool_settings_tb_lddata.TabIndex = 3;
            // 
            // tool_settings_btn_browse_ldplayer
            // 
            this.tool_settings_btn_browse_ldplayer.Location = new System.Drawing.Point(444, 58);
            this.tool_settings_btn_browse_ldplayer.Name = "tool_settings_btn_browse_ldplayer";
            this.tool_settings_btn_browse_ldplayer.Size = new System.Drawing.Size(75, 23);
            this.tool_settings_btn_browse_ldplayer.TabIndex = 4;
            this.tool_settings_btn_browse_ldplayer.Text = "Browse";
            this.tool_settings_btn_browse_ldplayer.UseVisualStyleBackColor = true;
            this.tool_settings_btn_browse_ldplayer.Click += new System.EventHandler(this.tool_settings_btn_browse_ldplayer_Click);
            // 
            // tool_settings_btn_browse_lddata
            // 
            this.tool_settings_btn_browse_lddata.Location = new System.Drawing.Point(444, 110);
            this.tool_settings_btn_browse_lddata.Name = "tool_settings_btn_browse_lddata";
            this.tool_settings_btn_browse_lddata.Size = new System.Drawing.Size(75, 23);
            this.tool_settings_btn_browse_lddata.TabIndex = 5;
            this.tool_settings_btn_browse_lddata.Text = "Browse";
            this.tool_settings_btn_browse_lddata.UseVisualStyleBackColor = true;
            this.tool_settings_btn_browse_lddata.Click += new System.EventHandler(this.tool_settings_btn_browse_lddata_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(135, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Luồng";
            // 
            // tool_settings_tb_threads
            // 
            this.tool_settings_tb_threads.Location = new System.Drawing.Point(178, 157);
            this.tool_settings_tb_threads.Name = "tool_settings_tb_threads";
            this.tool_settings_tb_threads.Size = new System.Drawing.Size(130, 20);
            this.tool_settings_tb_threads.TabIndex = 7;
            // 
            // tool_settings_btn_update
            // 
            this.tool_settings_btn_update.Location = new System.Drawing.Point(192, 341);
            this.tool_settings_btn_update.Name = "tool_settings_btn_update";
            this.tool_settings_btn_update.Size = new System.Drawing.Size(75, 23);
            this.tool_settings_btn_update.TabIndex = 8;
            this.tool_settings_btn_update.Text = "Update";
            this.tool_settings_btn_update.UseVisualStyleBackColor = true;
            this.tool_settings_btn_update.Click += new System.EventHandler(this.tool_settings_btn_update_Click);
            // 
            // tool_settings_btn_close
            // 
            this.tool_settings_btn_close.Location = new System.Drawing.Point(290, 340);
            this.tool_settings_btn_close.Name = "tool_settings_btn_close";
            this.tool_settings_btn_close.Size = new System.Drawing.Size(75, 23);
            this.tool_settings_btn_close.TabIndex = 9;
            this.tool_settings_btn_close.Text = "Close";
            this.tool_settings_btn_close.UseVisualStyleBackColor = true;
            this.tool_settings_btn_close.Click += new System.EventHandler(this.tool_settings_btn_close_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(79, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "LD Original Name";
            // 
            // tool_settings_tb_name
            // 
            this.tool_settings_tb_name.Location = new System.Drawing.Point(178, 18);
            this.tool_settings_tb_name.Name = "tool_settings_tb_name";
            this.tool_settings_tb_name.Size = new System.Drawing.Size(130, 20);
            this.tool_settings_tb_name.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 205);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Focus Engage Threads";
            // 
            // tool_settings_tb_focus_engage
            // 
            this.tool_settings_tb_focus_engage.Location = new System.Drawing.Point(178, 202);
            this.tool_settings_tb_focus_engage.Name = "tool_settings_tb_focus_engage";
            this.tool_settings_tb_focus_engage.Size = new System.Drawing.Size(130, 20);
            this.tool_settings_tb_focus_engage.TabIndex = 13;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(547, 322);
            this.tabControl1.TabIndex = 14;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.tool_settings_tb_sleep_between_launch);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.tool_settings_tb_focus_engage);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.tool_settings_tb_name);
            this.tabPage1.Controls.Add(this.tool_settings_tb_ldplayer);
            this.tabPage1.Controls.Add(this.tool_settings_tb_lddata);
            this.tabPage1.Controls.Add(this.tool_settings_btn_browse_ldplayer);
            this.tabPage1.Controls.Add(this.tool_settings_btn_browse_lddata);
            this.tabPage1.Controls.Add(this.tool_settings_tb_threads);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(539, 296);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Global";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(324, 244);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 13);
            this.label18.TabIndex = 16;
            this.label18.Text = "in seconds";
            // 
            // tool_settings_tb_sleep_between_launch
            // 
            this.tool_settings_tb_sleep_between_launch.Location = new System.Drawing.Point(178, 241);
            this.tool_settings_tb_sleep_between_launch.Name = "tool_settings_tb_sleep_between_launch";
            this.tool_settings_tb_sleep_between_launch.Size = new System.Drawing.Size(130, 20);
            this.tool_settings_tb_sleep_between_launch.TabIndex = 15;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(54, 244);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(118, 13);
            this.label17.TabIndex = 14;
            this.label17.Text = "Sleep Between Launch";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btn_browse_proxifier);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.tb_proxifier_path);
            this.tabPage2.Controls.Add(this.tb_manual_proxy);
            this.tabPage2.Controls.Add(this.tb_switch_index);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(539, 296);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Launch Manual";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btn_browse_proxifier
            // 
            this.btn_browse_proxifier.Location = new System.Drawing.Point(290, 100);
            this.btn_browse_proxifier.Name = "btn_browse_proxifier";
            this.btn_browse_proxifier.Size = new System.Drawing.Size(75, 23);
            this.btn_browse_proxifier.TabIndex = 9;
            this.btn_browse_proxifier.Text = "Browser";
            this.btn_browse_proxifier.UseVisualStyleBackColor = true;
            this.btn_browse_proxifier.Click += new System.EventHandler(this.btn_browse_proxifier_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Brown;
            this.label11.Location = new System.Drawing.Point(379, 105);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "**proxifier.exe path";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Brown;
            this.label10.Location = new System.Drawing.Point(214, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(195, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "**index of ldplayer use to launch manual";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Brown;
            this.label9.Location = new System.Drawing.Point(287, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(187, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "**must match proxy settings in Proxifier";
            // 
            // tb_proxifier_path
            // 
            this.tb_proxifier_path.Location = new System.Drawing.Point(108, 102);
            this.tb_proxifier_path.Name = "tb_proxifier_path";
            this.tb_proxifier_path.Size = new System.Drawing.Size(173, 20);
            this.tb_proxifier_path.TabIndex = 5;
            // 
            // tb_manual_proxy
            // 
            this.tb_manual_proxy.Location = new System.Drawing.Point(108, 64);
            this.tb_manual_proxy.Name = "tb_manual_proxy";
            this.tb_manual_proxy.Size = new System.Drawing.Size(173, 20);
            this.tb_manual_proxy.TabIndex = 4;
            // 
            // tb_switch_index
            // 
            this.tb_switch_index.Location = new System.Drawing.Point(108, 26);
            this.tb_switch_index.Name = "tb_switch_index";
            this.tb_switch_index.Size = new System.Drawing.Size(100, 20);
            this.tb_switch_index.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(69, 67);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Proxy";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(58, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Proxifier";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(34, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Switch Index";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.tb_launchff_pc_backup);
            this.tabPage3.Controls.Add(this.tb_launchff_pc_profile_location);
            this.tabPage3.Controls.Add(this.tb_launchff_pc_original);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(539, 296);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Launch FF on PC";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(394, 114);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 13);
            this.label16.TabIndex = 7;
            this.label16.Text = "** must end with /";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(394, 70);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "** must end with /";
            // 
            // tb_launchff_pc_backup
            // 
            this.tb_launchff_pc_backup.Location = new System.Drawing.Point(135, 111);
            this.tb_launchff_pc_backup.Name = "tb_launchff_pc_backup";
            this.tb_launchff_pc_backup.Size = new System.Drawing.Size(234, 20);
            this.tb_launchff_pc_backup.TabIndex = 5;
            // 
            // tb_launchff_pc_profile_location
            // 
            this.tb_launchff_pc_profile_location.Location = new System.Drawing.Point(135, 67);
            this.tb_launchff_pc_profile_location.Name = "tb_launchff_pc_profile_location";
            this.tb_launchff_pc_profile_location.Size = new System.Drawing.Size(234, 20);
            this.tb_launchff_pc_profile_location.TabIndex = 4;
            // 
            // tb_launchff_pc_original
            // 
            this.tb_launchff_pc_original.Location = new System.Drawing.Point(135, 30);
            this.tb_launchff_pc_original.Name = "tb_launchff_pc_original";
            this.tb_launchff_pc_original.Size = new System.Drawing.Size(234, 20);
            this.tb_launchff_pc_original.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(45, 114);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Backup Path";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(19, 70);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "FF Profile Location";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(57, 33);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Original FF";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tb_ftp_username);
            this.tabPage4.Controls.Add(this.label19);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(539, 296);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "FTP Server";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(19, 19);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(73, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Ftp Username";
            // 
            // tb_ftp_username
            // 
            this.tb_ftp_username.Location = new System.Drawing.Point(98, 16);
            this.tb_ftp_username.Name = "tb_ftp_username";
            this.tb_ftp_username.Size = new System.Drawing.Size(176, 20);
            this.tb_ftp_username.TabIndex = 1;
            // 
            // Tool_Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 375);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.tool_settings_btn_close);
            this.Controls.Add(this.tool_settings_btn_update);
            this.Name = "Tool_Settings";
            this.Text = "Tool Settings";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tool_settings_tb_ldplayer;
        private System.Windows.Forms.TextBox tool_settings_tb_lddata;
        private System.Windows.Forms.Button tool_settings_btn_browse_ldplayer;
        private System.Windows.Forms.Button tool_settings_btn_browse_lddata;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tool_settings_tb_threads;
        private System.Windows.Forms.Button tool_settings_btn_update;
        private System.Windows.Forms.Button tool_settings_btn_close;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tool_settings_tb_name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tool_settings_tb_focus_engage;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btn_browse_proxifier;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_proxifier_path;
        private System.Windows.Forms.TextBox tb_manual_proxy;
        private System.Windows.Forms.TextBox tb_switch_index;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox tb_launchff_pc_backup;
        private System.Windows.Forms.TextBox tb_launchff_pc_profile_location;
        private System.Windows.Forms.TextBox tb_launchff_pc_original;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tool_settings_tb_sleep_between_launch;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox tb_ftp_username;
        private System.Windows.Forms.Label label19;
    }
}