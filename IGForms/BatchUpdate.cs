﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.IGForms
{
    public partial class BatchUpdate : Form
    {
        public DataGridView dataGridView_igaccount;
        List<int> selected_row;

        public BatchUpdate()
        {
            InitializeComponent();
        }

        public BatchUpdate(DataGridView dataGridView, List<int> selected_row, List<string> niche_filters)
        {
            InitializeComponent();
            dataGridView_igaccount = dataGridView;
            this.selected_row = selected_row;
            
            foreach(var item in niche_filters)
            {
                batchupdate_cb_niche.Items.Add(item);
            }
        }

        private void batch_btn_update_Click(object sender, EventArgs e)
        {
            if(batchupdate_cb_niche.Text!="")
            {
                //batch update niche
                Update("niche", batchupdate_cb_niche.Text);
            }


            if(batchupdate_cb_state.Text!="")
            {
                //batch update state
                Update("state", batchupdate_cb_state.Text);
            }

            MessageBox.Show("Cập nhật thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        private void Update(string key, string value)
        {
            foreach (var index in selected_row)
            {
                dataGridView_igaccount.Rows[index].Cells[key].Value = value;
            }
        }

        private void batch_btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
