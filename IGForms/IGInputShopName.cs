﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.IGForms
{
    public partial class IGInputShopName : Form
    {
        List<string> niches;
        public IGInputShopName(List<string> niches)
        {
            InitializeComponent();
            this.niches = niches;
        }

        private void inputshopname_btn_import_Click(object sender, EventArgs e)
        {
            if(inputshopname_cb_niches.Text=="")
            {
                MessageBox.Show("Lỗi", "Vui lòng chọn chủ đề", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var niche = inputshopname_cb_niches.Text;

            var pc_devices = Program.form.pc_devices;

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                var reader = new StringReader(tb_inputshopname_input.Text);
                string line;
                while (null != (line = reader.ReadLine()))
                {
                    using (var command = new SqlCommand("insert into ig_account (fullname,niche,pc_devices) values ('" + line + "','" + niche + "','" + pc_devices + "')", conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }

            this.Close();
        }

        private void IGInputShopName_Load(object sender, EventArgs e)
        {
            foreach(var item in niches)
            {
                inputshopname_cb_niches.Items.Add(item);
            }
        }
    }
}
