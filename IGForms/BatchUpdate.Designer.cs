﻿namespace InstagramLDPlayer.IGForms
{
    partial class BatchUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.batchupdate_cb_state = new System.Windows.Forms.ComboBox();
            this.batchupdate_cb_niche = new System.Windows.Forms.ComboBox();
            this.batch_btn_update = new System.Windows.Forms.Button();
            this.batch_btn_close = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Niche";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "State";
            // 
            // batchupdate_cb_state
            // 
            this.batchupdate_cb_state.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.batchupdate_cb_state.FormattingEnabled = true;
            this.batchupdate_cb_state.Items.AddRange(new object[] {
            "ACTIVE",
            "PAUSED",
            "MANUAL"});
            this.batchupdate_cb_state.Location = new System.Drawing.Point(53, 85);
            this.batchupdate_cb_state.Name = "batchupdate_cb_state";
            this.batchupdate_cb_state.Size = new System.Drawing.Size(121, 21);
            this.batchupdate_cb_state.TabIndex = 2;
            // 
            // batchupdate_cb_niche
            // 
            this.batchupdate_cb_niche.FormattingEnabled = true;
            this.batchupdate_cb_niche.Location = new System.Drawing.Point(53, 39);
            this.batchupdate_cb_niche.Name = "batchupdate_cb_niche";
            this.batchupdate_cb_niche.Size = new System.Drawing.Size(121, 21);
            this.batchupdate_cb_niche.TabIndex = 3;
            // 
            // batch_btn_update
            // 
            this.batch_btn_update.Location = new System.Drawing.Point(53, 154);
            this.batch_btn_update.Name = "batch_btn_update";
            this.batch_btn_update.Size = new System.Drawing.Size(75, 23);
            this.batch_btn_update.TabIndex = 4;
            this.batch_btn_update.Text = "Update";
            this.batch_btn_update.UseVisualStyleBackColor = true;
            this.batch_btn_update.Click += new System.EventHandler(this.batch_btn_update_Click);
            // 
            // batch_btn_close
            // 
            this.batch_btn_close.Location = new System.Drawing.Point(169, 154);
            this.batch_btn_close.Name = "batch_btn_close";
            this.batch_btn_close.Size = new System.Drawing.Size(75, 23);
            this.batch_btn_close.TabIndex = 5;
            this.batch_btn_close.Text = "Close";
            this.batch_btn_close.UseVisualStyleBackColor = true;
            this.batch_btn_close.Click += new System.EventHandler(this.batch_btn_close_Click);
            // 
            // BatchUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 276);
            this.Controls.Add(this.batch_btn_close);
            this.Controls.Add(this.batch_btn_update);
            this.Controls.Add(this.batchupdate_cb_niche);
            this.Controls.Add(this.batchupdate_cb_state);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "BatchUpdate";
            this.Text = "BatchUpdate";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox batchupdate_cb_state;
        private System.Windows.Forms.ComboBox batchupdate_cb_niche;
        private System.Windows.Forms.Button batch_btn_update;
        private System.Windows.Forms.Button batch_btn_close;
    }
}