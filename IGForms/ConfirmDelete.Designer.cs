﻿namespace InstagramLDPlayer.IGForms
{
    partial class ConfirmDelete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.confirmdelete_tb_delete = new System.Windows.Forms.TextBox();
            this.confirmdelete_btn_ok = new System.Windows.Forms.Button();
            this.confirmdelete_btn_cancel = new System.Windows.Forms.Button();
            this.confirmdelete_lb_error = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.OrangeRed;
            this.label1.Location = new System.Drawing.Point(46, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(386, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bạn có chắc chắn muốn xóa profile này? Thao tác này không thể hoàn lại được";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(137, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Gõ DELETE và nhấn OK để xóa";
            // 
            // confirmdelete_tb_delete
            // 
            this.confirmdelete_tb_delete.Location = new System.Drawing.Point(161, 100);
            this.confirmdelete_tb_delete.Name = "confirmdelete_tb_delete";
            this.confirmdelete_tb_delete.Size = new System.Drawing.Size(100, 20);
            this.confirmdelete_tb_delete.TabIndex = 2;
            // 
            // confirmdelete_btn_ok
            // 
            this.confirmdelete_btn_ok.Location = new System.Drawing.Point(122, 126);
            this.confirmdelete_btn_ok.Name = "confirmdelete_btn_ok";
            this.confirmdelete_btn_ok.Size = new System.Drawing.Size(75, 23);
            this.confirmdelete_btn_ok.TabIndex = 3;
            this.confirmdelete_btn_ok.Text = "OK";
            this.confirmdelete_btn_ok.UseVisualStyleBackColor = true;
            this.confirmdelete_btn_ok.Click += new System.EventHandler(this.confirmdelete_btn_ok_Click);
            // 
            // confirmdelete_btn_cancel
            // 
            this.confirmdelete_btn_cancel.Location = new System.Drawing.Point(224, 126);
            this.confirmdelete_btn_cancel.Name = "confirmdelete_btn_cancel";
            this.confirmdelete_btn_cancel.Size = new System.Drawing.Size(75, 23);
            this.confirmdelete_btn_cancel.TabIndex = 4;
            this.confirmdelete_btn_cancel.Text = "Cancel";
            this.confirmdelete_btn_cancel.UseVisualStyleBackColor = true;
            this.confirmdelete_btn_cancel.Click += new System.EventHandler(this.confirmdelete_btn_cancel_Click);
            // 
            // confirmdelete_lb_error
            // 
            this.confirmdelete_lb_error.AutoSize = true;
            this.confirmdelete_lb_error.ForeColor = System.Drawing.Color.OrangeRed;
            this.confirmdelete_lb_error.Location = new System.Drawing.Point(287, 103);
            this.confirmdelete_lb_error.Name = "confirmdelete_lb_error";
            this.confirmdelete_lb_error.Size = new System.Drawing.Size(81, 13);
            this.confirmdelete_lb_error.TabIndex = 5;
            this.confirmdelete_lb_error.Text = "Sai lệnh. Gõ lại.";
            this.confirmdelete_lb_error.Visible = false;
            // 
            // ConfirmDelete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 265);
            this.Controls.Add(this.confirmdelete_lb_error);
            this.Controls.Add(this.confirmdelete_btn_cancel);
            this.Controls.Add(this.confirmdelete_btn_ok);
            this.Controls.Add(this.confirmdelete_tb_delete);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ConfirmDelete";
            this.Text = "Confirm Delete";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox confirmdelete_tb_delete;
        private System.Windows.Forms.Button confirmdelete_btn_ok;
        private System.Windows.Forms.Button confirmdelete_btn_cancel;
        private System.Windows.Forms.Label confirmdelete_lb_error;
    }
}