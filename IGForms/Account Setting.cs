﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.IGForms
{
    public partial class Account_Setting : Form
    {
        public DataGridView DataGridView;
        public int rowindex;
        string phone_device, datadata_path, path_account;
        bool ischange_profile = false, ischange_bio = false;
        public DataGridViewRow dataGridViewRow;
        public Account_Setting()
        {
            InitializeComponent();
        }

        public Account_Setting(DataGridView dataGridView, int rowindex)
        {
            InitializeComponent();
            this.DataGridView = dataGridView ?? throw new ArgumentNullException(nameof(dataGridView));
            this.rowindex = rowindex;

            LoadSettings();
            
        }

        private void LoadSettings()
        {
            dataGridViewRow = DataGridView.Rows[rowindex];

            this.phone_device = dataGridViewRow.Cells["phonedevice"].Value.ToString();

            this.datadata_path = ADBSupport.LDHelpers.ftp_dnplayerdata + this.phone_device;
            this.path_account = datadata_path + "/account/";
            //Tao folder path account
            ADBSupport.ADBHelpers.CreatePath(path_account);


            this.accountsetting_tb_user.Text = dataGridViewRow.Cells["user"].Value.ToString();
            this.accountsetting_lb_fullname.Text = dataGridViewRow.Cells["fullname"].Value.ToString();
            this.accountsetting_tb_birthday.Text = dataGridViewRow.Cells["birthday"].Value.ToString();
            this.accountsetting_tb_bio.Text = dataGridViewRow.Cells["bio"].Value.ToString();

            //Load profile img
            var image = path_account + DataGridView.Rows[rowindex].Cells["profile_image"].Value.ToString();
            if (image != "")
            {
                //Load image
                picturebox_profile.ImageLocation = image;
            }
        }

        private void accountsetting_btn_update_Click(object sender, EventArgs e)
        {
            DataGridView.Rows[rowindex].Cells["user"].Value = this.accountsetting_tb_user.Text;

            if (this.accountsetting_tb_birthday.Text != "")
                DataGridView.Rows[rowindex].Cells["birthday"].Value = this.accountsetting_tb_birthday.Text;


            if (ischange_bio)
            {
                IGHelpers.DBHelpers.AddQueueTask(phone_device, "updatebio");
                DataGridView.Rows[rowindex].Cells["bio"].Value = this.accountsetting_tb_bio.Text;
                ischange_bio = false;
            }

            //Update image_path

            if (picturebox_profile.ImageLocation != "")
            {

                if (ischange_profile)
                {

                    IGHelpers.DBHelpers.AddQueueTask(phone_device, "updateprofile");

                    DataGridView.Rows[rowindex].Cells["profile_image"].Value = Path.GetFileName(this.picturebox_profile.ImageLocation);
                    ischange_profile = false;
                }
            }

            MessageBox.Show("Cập nhật thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void accountsetting_calendar_birthday_DateChanged(object sender, DateRangeEventArgs e)
        {
            accountsetting_tb_birthday.Text = accountsetting_calendar_birthday.SelectionStart.ToShortDateString();
        }



        private void accountsetting_tb_birthday_MouseClick(object sender, MouseEventArgs e)
        {
            if (accountsetting_tb_birthday.Text != "")
                accountsetting_calendar_birthday.SetDate(DateTime.Parse(accountsetting_tb_birthday.Text));
            accountsetting_calendar_birthday.Visible = true;

            var path = datadata_path + "\\igprofile_img\\birthday.jpeg";
            SetAccountSetting_ImagePos(path);
        }

        private void accountsetting_tb_birthday_Leave(object sender, EventArgs e)
        {
        }

        private void tab_profile_Click(object sender, EventArgs e)
        {
            accountsetting_calendar_birthday.SendToBack();
            accountsetting_calendar_birthday.Visible = false;

            if (accountSetting_Image != null)
                accountSetting_Image.Dispose();
        }

        private void tab_profile_btn_changeprofile_Click(object sender, EventArgs e)
        {
            ShowImagePopupForm();
        }

        private void ShowImagePopupForm()
        {
            OpenFileDialog imageDialog = new OpenFileDialog();
            imageDialog.Filter = "Image files (.jpg,.png,.jpeg)|*.jpg;*.png;*.jpeg";
            imageDialog.Title = "Choose Profile Image";
            imageDialog.ShowDialog();
            string file_image = imageDialog.FileName;


            if (file_image != "")
            {
                //Copy file to LDData folder tuong ung voi device

                var filename = Path.GetFileName(file_image);

                //Check if current file is existed on LDData
                while (true)
                {
                    if (File.Exists(path_account + filename))
                    {
                        var tick_str = DateTime.Now.Ticks.ToString();
                        filename = Path.GetFileNameWithoutExtension(file_image) + "_" +tick_str.Substring(tick_str.Length-3) + Path.GetExtension(file_image);
                        continue;
                    }
                    break;
                }

                File.Copy(file_image, path_account + filename);

                ischange_profile = true;

                //Set new value for pictureBox profile imagelocation

                picturebox_profile.ImageLocation = path_account + filename;
            }
        }
        IGForms.AccountSetting_Image accountSetting_Image;


        private void SetAccountSetting_ImagePos(string path)
        {//Set location accountsetting image

            if (accountSetting_Image == null)
            {
                accountSetting_Image = new AccountSetting_Image();
            }
            else if (accountSetting_Image.IsDisposed == true)
            {
                accountSetting_Image = new AccountSetting_Image();
            }


            if (accountSetting_Image.Visible)
            {
                accountSetting_Image.Visible = false;
            }
            accountSetting_Image.SetImage(path);
            accountSetting_Image.Show();

            
        }

        private void accountsetting_tb_bio_TextChanged(object sender, EventArgs e)
        {
            ischange_bio = true;
        }

        private void accountsetting_tb_user_MouseClick(object sender, MouseEventArgs e)
        {
            var path = datadata_path + "\\igprofile_img\\username.jpeg";
            SetAccountSetting_ImagePos(path);
        }

        private void Account_Setting_Resize(object sender, EventArgs e)
        {
            if(WindowState == FormWindowState.Minimized)
            {
                accountSetting_Image.WindowState = FormWindowState.Minimized;
            }
            if(WindowState == FormWindowState.Normal)
            {
                accountSetting_Image.WindowState = FormWindowState.Normal;
            }
        }

        private void picturebox_profile_Click(object sender, EventArgs e)
        {
            ShowImagePopupForm();
        }
    }
}
