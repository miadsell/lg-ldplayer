﻿namespace InstagramLDPlayer.IGForms
{
    partial class PhoneHelpers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.phone_tb_phonenumber = new System.Windows.Forms.TextBox();
            this.phone_btn_number = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.phone_btn_copy = new System.Windows.Forms.Button();
            this.phone_tb_confirmcode = new System.Windows.Forms.TextBox();
            this.phone_lb_status = new System.Windows.Forms.Label();
            this.phone_lb_statuscode = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nhập số cũ";
            // 
            // phone_tb_phonenumber
            // 
            this.phone_tb_phonenumber.Location = new System.Drawing.Point(109, 46);
            this.phone_tb_phonenumber.Name = "phone_tb_phonenumber";
            this.phone_tb_phonenumber.Size = new System.Drawing.Size(139, 20);
            this.phone_tb_phonenumber.TabIndex = 1;
            // 
            // phone_btn_number
            // 
            this.phone_btn_number.Location = new System.Drawing.Point(109, 89);
            this.phone_btn_number.Name = "phone_btn_number";
            this.phone_btn_number.Size = new System.Drawing.Size(75, 23);
            this.phone_btn_number.TabIndex = 2;
            this.phone_btn_number.Text = "Get number";
            this.phone_btn_number.UseVisualStyleBackColor = true;
            this.phone_btn_number.Click += new System.EventHandler(this.phone_btn_number_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(276, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "* (nếu để trống sẽ lấy số mới)";
            // 
            // phone_btn_copy
            // 
            this.phone_btn_copy.Location = new System.Drawing.Point(109, 218);
            this.phone_btn_copy.Name = "phone_btn_copy";
            this.phone_btn_copy.Size = new System.Drawing.Size(75, 23);
            this.phone_btn_copy.TabIndex = 4;
            this.phone_btn_copy.Text = "Copy";
            this.phone_btn_copy.UseVisualStyleBackColor = true;
            // 
            // phone_tb_confirmcode
            // 
            this.phone_tb_confirmcode.Location = new System.Drawing.Point(109, 172);
            this.phone_tb_confirmcode.Name = "phone_tb_confirmcode";
            this.phone_tb_confirmcode.ReadOnly = true;
            this.phone_tb_confirmcode.Size = new System.Drawing.Size(139, 20);
            this.phone_tb_confirmcode.TabIndex = 5;
            // 
            // phone_lb_status
            // 
            this.phone_lb_status.AutoSize = true;
            this.phone_lb_status.Location = new System.Drawing.Point(213, 94);
            this.phone_lb_status.Name = "phone_lb_status";
            this.phone_lb_status.Size = new System.Drawing.Size(52, 13);
            this.phone_lb_status.TabIndex = 6;
            this.phone_lb_status.Text = "No status";
            // 
            // phone_lb_statuscode
            // 
            this.phone_lb_statuscode.AutoSize = true;
            this.phone_lb_statuscode.Location = new System.Drawing.Point(276, 175);
            this.phone_lb_statuscode.Name = "phone_lb_statuscode";
            this.phone_lb_statuscode.Size = new System.Drawing.Size(52, 13);
            this.phone_lb_statuscode.TabIndex = 7;
            this.phone_lb_statuscode.Text = "No status";
            // 
            // PhoneHelpers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 342);
            this.Controls.Add(this.phone_lb_statuscode);
            this.Controls.Add(this.phone_lb_status);
            this.Controls.Add(this.phone_tb_confirmcode);
            this.Controls.Add(this.phone_btn_copy);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.phone_btn_number);
            this.Controls.Add(this.phone_tb_phonenumber);
            this.Controls.Add(this.label1);
            this.Name = "PhoneHelpers";
            this.Text = "Lấy số thủ công";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox phone_tb_phonenumber;
        private System.Windows.Forms.Button phone_btn_number;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button phone_btn_copy;
        private System.Windows.Forms.TextBox phone_tb_confirmcode;
        private System.Windows.Forms.Label phone_lb_status;
        private System.Windows.Forms.Label phone_lb_statuscode;
    }
}