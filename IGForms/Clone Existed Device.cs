﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.IGForms
{
    public partial class Clone_Existed_Device : Form
    {
        public Clone_Existed_Device()
        {
            InitializeComponent();
        }

        private void btn_start_clone_Click(object sender, EventArgs e)
        {
            var pc_devices = Program.form.pc_devices;

            using (SqlConnection conn = new SqlConnection(IGHelpers.DBHelpers.db_connection))
            {
                conn.Open();

                var reader = new StringReader(tb_devices.Text);
                string line;
                while (null != (line = reader.ReadLine()))
                {
                    using (var command = new SqlCommand("update specifictasks set clonexisted='Do' where accountId=(select [id] from ig_account where phone_device='" + line + "' and pc_devices!=" + pc_devices + ")", conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }

            this.Close();
        }
    }
}
