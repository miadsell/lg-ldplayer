﻿namespace InstagramLDPlayer.IGForms
{
    partial class MessageScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox_message = new System.Windows.Forms.PictureBox();
            this.pictureBox_request = new System.Windows.Forms.PictureBox();
            this.button_close = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_message)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_request)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox_message
            // 
            this.pictureBox_message.Location = new System.Drawing.Point(11, 39);
            this.pictureBox_message.Name = "pictureBox_message";
            this.pictureBox_message.Size = new System.Drawing.Size(270, 480);
            this.pictureBox_message.TabIndex = 0;
            this.pictureBox_message.TabStop = false;
            // 
            // pictureBox_request
            // 
            this.pictureBox_request.Location = new System.Drawing.Point(322, 39);
            this.pictureBox_request.Name = "pictureBox_request";
            this.pictureBox_request.Size = new System.Drawing.Size(270, 480);
            this.pictureBox_request.TabIndex = 1;
            this.pictureBox_request.TabStop = false;
            // 
            // button_close
            // 
            this.button_close.Location = new System.Drawing.Point(262, 525);
            this.button_close.Name = "button_close";
            this.button_close.Size = new System.Drawing.Size(75, 23);
            this.button_close.TabIndex = 2;
            this.button_close.Text = "Close";
            this.button_close.UseVisualStyleBackColor = true;
            this.button_close.Click += new System.EventHandler(this.button_close_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(84, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 26);
            this.label1.TabIndex = 3;
            this.label1.Text = "Messages";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(409, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 26);
            this.label2.TabIndex = 4;
            this.label2.Text = "Requests";
            // 
            // MessageScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 555);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_close);
            this.Controls.Add(this.pictureBox_request);
            this.Controls.Add(this.pictureBox_message);
            this.Name = "MessageScreen";
            this.Text = "MessageScreen";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_message)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_request)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_message;
        private System.Windows.Forms.PictureBox pictureBox_request;
        private System.Windows.Forms.Button button_close;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}