﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.IGForms
{
    public partial class AccountSetting_Image : Form
    {
        public AccountSetting_Image()
        {
            InitializeComponent();

        }


        public void SetImage(string path)
        {
            pictureBox1.ImageLocation = path;
        }

        private void pictureBox1_VisibleChanged(object sender, EventArgs e)
        {
        }
    }
}
