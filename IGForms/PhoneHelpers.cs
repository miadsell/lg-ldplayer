﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramLDPlayer.IGForms
{
    public partial class PhoneHelpers : Form
    {
        public PhoneHelpers()
        {
            InitializeComponent();
        }

        private void FreezeForm()
        {
            this.phone_btn_number.Enabled = false;
            this.phone_btn_copy.Enabled = false;
        }


        private void UnfreezeForm()
        {
            this.phone_btn_number.Enabled = true;
            this.phone_btn_copy.Enabled = true;
        }

        private void phone_btn_number_Click(object sender, EventArgs e)
        {
            FreezeForm();
            if (phone_tb_phonenumber.Text == "")
            {
                //lay so moi
                Thread thread = new Thread(() => GetNewNumber());
                thread.IsBackground = true;
                thread.Start();
            }
            else
            {
                //test xem co the goi lai k
                Thread thread = new Thread(() => GetOldNumber(phone_tb_phonenumber.Text));
                thread.IsBackground = true;
                thread.Start();
            }
        }


        private void GetNewNumber()
        {
            this.Invoke((MethodInvoker)delegate
            {
                this.phone_lb_status.Text = "Đang lấy số...";
                this.phone_lb_status.ForeColor = Color.Green;
            });
                var phone = IGHelpers.PhoneHelpers.RequestPhone_RentCode();
            this.Invoke((MethodInvoker)delegate
            {

                phone_tb_phonenumber.Text = phone.phonenum;
                this.phone_lb_status.Text = "Lấy số thành công. Submit bên IG và chờ lấy code";
                this.phone_lb_status.ForeColor = Color.Green;


                this.phone_lb_statuscode.Text = "Đang lấy code";
                this.phone_lb_statuscode.ForeColor = Color.Red;
            });


            GetSms(phone.orderid);
        }


        private void GetOldNumber(string phonenum)
        {
            this.Invoke((MethodInvoker)delegate
            {
                this.phone_lb_status.Text = "Đang thử lấy lại số";
                this.phone_lb_status.ForeColor = Color.Green;
            });
            var orderid = IGHelpers.PhoneHelpers.RequestPhone_Reuse(phonenum);
            if (orderid == null)
            {
                return;
            }

            this.Invoke((MethodInvoker)delegate
            {

                this.phone_lb_status.Text = "Số còn tồn tại. Submit bên IG và chờ lấy code";
                this.phone_lb_status.ForeColor = Color.Green;


                this.phone_lb_statuscode.Text = "Đang lấy code";
                this.phone_lb_statuscode.ForeColor = Color.Red;
            });

            GetSms(orderid);
        }


        private void GetSms(string orderId)
        {
            var sms = IGHelpers.PhoneHelpers.GetSMS_RentCode(orderId,2);

            if (sms == null)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    this.phone_lb_status.Text = "";
                    this.phone_lb_status.ForeColor = Color.Green;


                    this.phone_lb_statuscode.Text = "Lỗi -> Không lấy được code";
                    this.phone_lb_statuscode.ForeColor = Color.Red;
                });

                return;
            }
            else
            {
                this.Invoke((MethodInvoker)delegate
                {
                    this.phone_lb_status.Text = "";
                    this.phone_lb_status.ForeColor = Color.Green;


                    this.phone_lb_statuscode.Text = "Thành công";
                    this.phone_lb_statuscode.ForeColor = Color.Green;
                });
            }

            phone_tb_confirmcode.Text = sms;

            //Clear number

            phone_tb_phonenumber.Clear();

            //Unfreeze form
            this.Invoke((MethodInvoker)delegate
            {
                UnfreezeForm();
            });
        }

        bool isstop = false;
        private void phone_btn_stop_Click(object sender, EventArgs e)
        {
            isstop = true;
        }
    }
}
