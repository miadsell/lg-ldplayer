﻿namespace InstagramLDPlayer.IGForms
{
    partial class Account_Setting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.accountsetting_tb_user = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.accountsetting_tb_birthday = new System.Windows.Forms.TextBox();
            this.accountsetting_lb_fullname = new System.Windows.Forms.Label();
            this.accountsetting_btn_update = new System.Windows.Forms.Button();
            this.accountsetting_calendar_birthday = new System.Windows.Forms.MonthCalendar();
            this.accountsetting_lb_status = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.accountsetting_tb_bio = new System.Windows.Forms.TextBox();
            this.picturebox_profile = new System.Windows.Forms.PictureBox();
            this.tab_profile_btn_changeprofile = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tab_profile = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.picturebox_profile)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tab_profile.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "User";
            // 
            // accountsetting_tb_user
            // 
            this.accountsetting_tb_user.Location = new System.Drawing.Point(92, 36);
            this.accountsetting_tb_user.Name = "accountsetting_tb_user";
            this.accountsetting_tb_user.Size = new System.Drawing.Size(155, 20);
            this.accountsetting_tb_user.TabIndex = 1;
            this.accountsetting_tb_user.MouseClick += new System.Windows.Forms.MouseEventHandler(this.accountsetting_tb_user_MouseClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Fullname";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Birthday";
            // 
            // accountsetting_tb_birthday
            // 
            this.accountsetting_tb_birthday.Location = new System.Drawing.Point(92, 105);
            this.accountsetting_tb_birthday.Name = "accountsetting_tb_birthday";
            this.accountsetting_tb_birthday.Size = new System.Drawing.Size(155, 20);
            this.accountsetting_tb_birthday.TabIndex = 4;
            this.accountsetting_tb_birthday.MouseClick += new System.Windows.Forms.MouseEventHandler(this.accountsetting_tb_birthday_MouseClick);
            this.accountsetting_tb_birthday.Leave += new System.EventHandler(this.accountsetting_tb_birthday_Leave);
            // 
            // accountsetting_lb_fullname
            // 
            this.accountsetting_lb_fullname.AutoSize = true;
            this.accountsetting_lb_fullname.Location = new System.Drawing.Point(92, 73);
            this.accountsetting_lb_fullname.Name = "accountsetting_lb_fullname";
            this.accountsetting_lb_fullname.Size = new System.Drawing.Size(41, 13);
            this.accountsetting_lb_fullname.TabIndex = 5;
            this.accountsetting_lb_fullname.Text = "{name}";
            // 
            // accountsetting_btn_update
            // 
            this.accountsetting_btn_update.Location = new System.Drawing.Point(112, 236);
            this.accountsetting_btn_update.Name = "accountsetting_btn_update";
            this.accountsetting_btn_update.Size = new System.Drawing.Size(102, 23);
            this.accountsetting_btn_update.TabIndex = 6;
            this.accountsetting_btn_update.Text = "Update";
            this.accountsetting_btn_update.UseVisualStyleBackColor = true;
            this.accountsetting_btn_update.Click += new System.EventHandler(this.accountsetting_btn_update_Click);
            // 
            // accountsetting_calendar_birthday
            // 
            this.accountsetting_calendar_birthday.Location = new System.Drawing.Point(460, 36);
            this.accountsetting_calendar_birthday.Name = "accountsetting_calendar_birthday";
            this.accountsetting_calendar_birthday.TabIndex = 7;
            this.accountsetting_calendar_birthday.Visible = false;
            this.accountsetting_calendar_birthday.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.accountsetting_calendar_birthday_DateChanged);
            // 
            // accountsetting_lb_status
            // 
            this.accountsetting_lb_status.AutoSize = true;
            this.accountsetting_lb_status.ForeColor = System.Drawing.SystemColors.Highlight;
            this.accountsetting_lb_status.Location = new System.Drawing.Point(92, 285);
            this.accountsetting_lb_status.Name = "accountsetting_lb_status";
            this.accountsetting_lb_status.Size = new System.Drawing.Size(0, 13);
            this.accountsetting_lb_status.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(64, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Bio";
            // 
            // accountsetting_tb_bio
            // 
            this.accountsetting_tb_bio.Location = new System.Drawing.Point(92, 140);
            this.accountsetting_tb_bio.Multiline = true;
            this.accountsetting_tb_bio.Name = "accountsetting_tb_bio";
            this.accountsetting_tb_bio.Size = new System.Drawing.Size(155, 82);
            this.accountsetting_tb_bio.TabIndex = 10;
            this.accountsetting_tb_bio.TextChanged += new System.EventHandler(this.accountsetting_tb_bio_TextChanged);
            // 
            // picturebox_profile
            // 
            this.picturebox_profile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picturebox_profile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picturebox_profile.ErrorImage = null;
            this.picturebox_profile.ImageLocation = "";
            this.picturebox_profile.Location = new System.Drawing.Point(284, 36);
            this.picturebox_profile.Name = "picturebox_profile";
            this.picturebox_profile.Size = new System.Drawing.Size(136, 136);
            this.picturebox_profile.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picturebox_profile.TabIndex = 11;
            this.picturebox_profile.TabStop = false;
            this.picturebox_profile.Click += new System.EventHandler(this.picturebox_profile_Click);
            // 
            // tab_profile_btn_changeprofile
            // 
            this.tab_profile_btn_changeprofile.Location = new System.Drawing.Point(310, 196);
            this.tab_profile_btn_changeprofile.Name = "tab_profile_btn_changeprofile";
            this.tab_profile_btn_changeprofile.Size = new System.Drawing.Size(86, 23);
            this.tab_profile_btn_changeprofile.TabIndex = 12;
            this.tab_profile_btn_changeprofile.Text = "Change Profile";
            this.tab_profile_btn_changeprofile.UseVisualStyleBackColor = true;
            this.tab_profile_btn_changeprofile.Click += new System.EventHandler(this.tab_profile_btn_changeprofile_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tab_profile);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(815, 366);
            this.tabControl1.TabIndex = 13;
            // 
            // tab_profile
            // 
            this.tab_profile.Controls.Add(this.accountsetting_tb_user);
            this.tab_profile.Controls.Add(this.tab_profile_btn_changeprofile);
            this.tab_profile.Controls.Add(this.label1);
            this.tab_profile.Controls.Add(this.picturebox_profile);
            this.tab_profile.Controls.Add(this.label2);
            this.tab_profile.Controls.Add(this.accountsetting_tb_bio);
            this.tab_profile.Controls.Add(this.label3);
            this.tab_profile.Controls.Add(this.label4);
            this.tab_profile.Controls.Add(this.accountsetting_tb_birthday);
            this.tab_profile.Controls.Add(this.accountsetting_lb_status);
            this.tab_profile.Controls.Add(this.accountsetting_lb_fullname);
            this.tab_profile.Controls.Add(this.accountsetting_calendar_birthday);
            this.tab_profile.Controls.Add(this.accountsetting_btn_update);
            this.tab_profile.Location = new System.Drawing.Point(4, 22);
            this.tab_profile.Name = "tab_profile";
            this.tab_profile.Padding = new System.Windows.Forms.Padding(3);
            this.tab_profile.Size = new System.Drawing.Size(807, 340);
            this.tab_profile.TabIndex = 0;
            this.tab_profile.Text = "Profile";
            this.tab_profile.UseVisualStyleBackColor = true;
            this.tab_profile.Click += new System.EventHandler(this.tab_profile_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(807, 340);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Engage";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // Account_Setting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 366);
            this.Controls.Add(this.tabControl1);
            this.Name = "Account_Setting";
            this.Text = "Account_Setting";
            this.Resize += new System.EventHandler(this.Account_Setting_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.picturebox_profile)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tab_profile.ResumeLayout(false);
            this.tab_profile.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox accountsetting_tb_user;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox accountsetting_tb_birthday;
        private System.Windows.Forms.Label accountsetting_lb_fullname;
        private System.Windows.Forms.Button accountsetting_btn_update;
        private System.Windows.Forms.MonthCalendar accountsetting_calendar_birthday;
        private System.Windows.Forms.Label accountsetting_lb_status;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox accountsetting_tb_bio;
        private System.Windows.Forms.PictureBox picturebox_profile;
        private System.Windows.Forms.Button tab_profile_btn_changeprofile;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tab_profile;
        private System.Windows.Forms.TabPage tabPage2;
    }
}